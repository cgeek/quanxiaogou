define(function(require, exports, module) {
	var $ = require('jquery');
	require('bootstrap');

	require('jquery-placeholder');
	$('input, textarea').placeholder();

	$('.dropdown-toggle').dropdown();

	$('*[data-toggle=tooltip]').tooltip();
});
