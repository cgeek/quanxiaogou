define(function(require, exports, module) {
	var $ = require('jquery');
	require('jquery-validate');
	require('json');

	require('uploadify');

	require('jquery-ui-widget');
	require('fileupload');

	var Mustache = require('mustache');

	var $form = $("#hotel_edit_form");

	require('gmap3');
	
	require('jquery-rte');
	require('jquery-rte-css');
	var EditToolBar = require('jquery-rte-tb');
	var contentRTE;

	return {
		init:function() {
			var that = this;
			that.validate();
			that.initUploadCoverImage();
			that.initDeleteBtn();
			that.initAgodaSync();
			that.initMapBox();

			contentRTE = $('#html_content_Text').rte({
				controls_rte: EditToolBar.rte_toolbar,
				controls_html: EditToolBar.html_toolbar,
				width: 700,
				height:500
			});

			//字段隐藏，显示
			$('.show-more-form-groups').click(function(){
				$('.hidden-form-groups').show();
				$('.show-more-form-groups').hide();
			});

			$('.hide-more-form-groups').click(function(){
				$('.hidden-form-groups').hide();
				$('.show-more-form-groups').show();
			});

			$('#gps_check').click(function(){
				var lat = $('#lat').val(), lon = $('#lon').val();
				var q = (lat != '' && lon != '')?'q=' + lat + ',' + lon : '';
				var openURL = "https://maps.google.com/?" + q;
				window.open(openURL, '_blank');
			});
			$('#cancel-btn').click(function(){
				window.history.back();	
			});


		},
		initAgodaSync: function() {
			$('#agoda_sync_btn').click(function(){
				var btn = $(this);
				var url = $('input[name=agoda_url]').val();
				if(url == '' || url == undefined) {
					alert('请输入agoda网址,注意使用中文网址');
					return false;
				}
				$.ajax({
					url : '/admin/hotel/crawlingAgoda',
					type : 'Post',
					data: {url: url},
					beforeSend: function() {
						btn.html('正在同步，请稍等。。。');
						btn.attr('disabled', true);
					},
					success: function(result){
						var hotel = result.data;
						$('input[name=title]').val(hotel.title);
						$('input[name=address]').val(hotel.address);
						$('input[name=hotel_name_english]').val(hotel.hotel_name_english);
						$('input[name=lat]').val(hotel.lat);
						$('input[name=lon]').val(hotel.lon);
						$('input[name=area]').val(hotel.area);
						$('textarea[name=desc]').val(hotel.desc);
						$("select[name=city_id] option[text='"+hotel.city_name+"']").attr('selected', true);

						$('input[name=cover_image]').val(hotel.cover_image);
						$('.poi-cover-image-box img').attr('src', hotel.cover_image);
						btn.html('同步成功!');
						btn.attr('disabled', false);
					},
					error: function(){
						btn.html('同步失败!');
						btn.attr('disabled', false);
					}

				});
			});
		},
		initMapBox:function() {
			$('.show_map_btn').click(function(){
				var lat = $('input[name=lat]').val();
				var lon = $('input[name=lon]').val();
				var city_name = $("select[name=city_id] option[selected]").text();
				$('#modalView').modal('show');

				if(!$('#modalView .modal-content').html()) {
				
				} else {
					var html =Mustache.to_html($('#map_box_tpl').html(), {lat:lat, lon:lon});
					$('#modalView .modal-content').html(html);

				}


				var map_opt = {address:city_name, options:{zoom:12}};
				if(lon > 0 && lat >0) {
					map_opt = {options: {zoom:15, center:[lat, lon]}};
				}

				$('#map_box').height('300px').gmap3({
					map : map_opt,
					marker:{
						values: [{latLng:[lat, lon]}],
						options:{
							draggable:true 
						},
						events:{
							dragstart: function(marker, event, context) {
							},
							drag: function(marker) {
								$('.lat_lon_span').html(marker.position.toString())
							},
							dragend: function(marker, event, content) {
								lat = marker.position.lat();
								lon = marker.position.lng();
							}
						}
						
					}
				});


				$('#save_lat_btn').click(function(){
					$('input[name=lat]').val(lat);
					$('input[name=lon]').val(lon);

					$('#modalView').modal('hide');
				});
			});
		},

		initDeleteBtn: function() {
			$(document).delegate('#delete-hotel-btn', 'click', function(){
				var hotel_id = $('input[name=hotel_id]').val();
				if(hotel_id == undefined || hotel_id <= 0) {
					alert('无法删除');
					return false;
				}
				if(confirm("确定要删除吗!")) {
					$.ajax({
						url : '/admin/hotel/deleteHotel',
						data : {hotel_id: hotel_id},
						type: 'post',
						dataType : 'JSON',
						success: function(result) {
							if(result.code == 200) {
								window.location.href = '/admin/hotel';
							} else {
								alert(result.message);
							}
						},
						error: function() {
							alert('删除失败！请联系管理员');
						}
					});
				}
			});
		},
		initUploadCoverImage: function(){
			var poi_id = $('input[name=poi_id]').val();
			//上传封面图片
			$('#fileupload').fileupload({
				url : '/admin/image/uploadify',
				dataType: 'json',
				acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
				maxFileSize: 5000000, // 5 MB
				formData:{object:"hotel", object_id: poi_id},
				send: function(e, data) {
					$('.poi-cover-image-box img').hide();
					$('.poi-cover-image-box .loading').show();
				},
				done: function(e, data) {
					var result = data.result;
					if(result.code == 200) {
						image_hash = result.data['image_hash'];
						$('input[name=cover_image]').val(image_hash);
						$('.poi-cover-image-box img').attr('src', result.data['image_url_medium']).show();
						$('.poi-cover-image-box .loading').hide();
					} else {
							alert(result.message);
					}
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progress .progress-bar').css('width',progress + '%');
				},
				fail: function(e, data) {
					$('.poi-cover-image-box .loading').html('上传失败');
				}
			}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
		},

		validate:function() {
			var _self = this;
			$form.validate({
				debug:false,
				submitHandler: function(e) {
					/*
					var htmlcontent = contentRTE.html_content_Text.get_content();
					$('textarea[name=html_content]').html(htmlcontent);
					*/
					var submitBtn = $('#submit-btn');
					$.ajax({
						url:'/admin/hotel/saveHotel',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function() {
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								if(result.data['poi_id'] > 0) {
									$('input[name=poi_id]').val(result.data['poi_id']); 
								}
								if(result.data['hotel_id'] > 0) {
									$('input[name=hotel_id]').val(result.data['hotel_id']); 
								}
								submitBtn.addClass('btn-success').html('保存成功');
								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存').attr('disabled', false);
									if(result.data['new_poi'] == 'ok' && result.data['poi_id'] > 0) {
										window.location.href = '/admin/hotel/edit?poi_id=' + result.data['poi_id'];
									}
								}, 2000);
							} else {
								submitBtn.addClass('btn-error').html('保存失败，请联系喵喵');
								alert(result.message);
								setTimeout(function() {
									submitBtn.removeClass('btn-error');
									submitBtn.text('保存').attr('disabled', false);
								}, 3000);
							}
						},
						error: function(e){
							submitBtn.text('保存').attr('disabled', false);
						}
					});
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					city_id:{
						required: true
					},
					title:{
						required: true
					}
				},
				messages:{
					city_id:{
						required:"请选择所在城市"
					},
					title:{
						required:"标题不能为空"
					}
				}
			});
		}
	};
});
