define(function(require, exports, module) {
	var $ = require('jquery');
	require('jquery-validate');
	require('json');

	require('uploadify');

	require('jquery-ui-widget');
	require('fileupload');

	var Mustache = require('mustache');

	var $form = $("#room_edit_form");
	
	return {
		init:function() {
			var that = this;
			that.initRoomList();
			that.validate();
			that.initMultiUpload();
			that.initUploadCoverImage();
			that.initDeleteBtn();
		},
		initRoomList : function() {
		},
		initDeleteBtn: function() {
			$(document).delegate('#delete-customer-btn', 'click', function(){
				var room_id = $('input[name=room_id]').val();
				if(room_id == undefined || room_id <= 0) {
					alert('无法删除');
					return false;
				}
				if(confirm("确定要删除吗!")) {
					$.ajax({
						url : '/admin/package/deleteRoom',
						data : {room_id: room_id},
						type: 'post',
						dataType : 'JSON',
						success: function(result) {
							if(result.code == 200) {
								window.location.href = '/admin/package/rooms?package_id=' + result.data['package_id'];
							} else {
								alert(result.message);
							}
						},
						error: function() {
							alert('删除失败！请联系喵喵');
						}
					});
				}
			});
		},
		initUploadCoverImage: function(){
			var room_id = $('input[name=room_id]').val();
			//上传封面图片
			$('#fileupload').fileupload({
				url : '/admin/image/uploadify',
				dataType: 'json',
				acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
				maxFileSize: 5000000, // 5 MB
				formData:{object:"room", object_id: room_id},
				send: function(e, data) {
					$('.cover-image-box img').hide();
					$('.cover-image-box .loading').show();
				},
				done: function(e, data) {
					var result = data.result;
					if(result.code == 200) {
						image_hash = result.data['image_hash'];
						$('input[name=cover_image]').val(image_hash);
						$('.cover-image-box img').attr('src', result.data['image_url_small']).show();

						$('.cover-image-box .loading').hide();
						//上传的图片同时添加到相册
						var tpl = $('#multimage-gallery-item-tpl').html();
						if(tpl) {
							var url = result.data['image_url_small'];
							var html = Mustache.to_html(tpl , {url:url, desc:'', id: result.data['id']});
							$('.multimage-gallery ul').append(html);
						}
					} else {
							alert(result.message);
					}
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progress .progress-bar').css('width',progress + '%');
				},
				fail: function(e, data) {
					$('.cover-image-box .loading').html('上传失败');
				}
			}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

			//从相册选择
			$(document).delegate('.select-cover-image-btn', 'click', function(){
				var tpl = $('#cover-image-select-tpl').html();
				
				$.ajax({
					url: '/admin/package/imageList',
					type: 'POST',
					data: {object: 'room', object_id:room_id},
					dataType: 'json',
					success: function(result) {
						if(result.code == 200) {
							var uploadHtml = Mustache.to_html(tpl, result.data);
							$('body').append(uploadHtml).addClass('no-scroll');
						} else {
							alert(result.message);
						}
					},
					error:function(e) {

					}
				});
			});
			$(document).delegate('.coverpicwrap ul li', 'click', function() {
				$('.coverpicwrap ul li .selected').removeClass('show');

				var image_hash = $(this).attr('image_hash');
				var image_url_small = $(this).find('img').attr('src');
				$(this).find('.selected').addClass('show');

				$('.coverpicwrap #set-cover-image-btn').attr('image_hash', image_hash);
				$('.coverpicwrap #set-cover-image-btn').attr('url', image_url_small);
			});
			$(document).delegate('#set-cover-image-btn', 'click', function() {
				var image_hash = $('.coverpicwrap #set-cover-image-btn').attr('image_hash');
				var cover_image_url = $('.coverpicwrap #set-cover-image-btn').attr('url');
				if(!image_hash || image_hash == undefined ) {
					return;
				}
				$('input[name=cover_image]').val(image_hash);
				$('.cover-image-box img').attr('src', cover_image_url);

				$('.coverpicwrap-pop').remove();
				$('body').removeClass('no-scroll');
			});
			$(document).delegate('.coverpicwrap .close', 'click', function(){
				$('.coverpicwrap-pop').remove();
				$('body').removeClass('no-scroll');
			});

		},
		initMultiUpload: function() {
			//相册操作
			$(document).delegate('.multimage-gallery li', 'mouseenter mouseleave', function(event){
				if(event.type == 'mouseenter') {
					$(this).find('.operate').show();
				} else {
					$(this).find('.operate').hide();
				}
			});
			$(document).delegate('.multimage-gallery li .operate i', 'click', function(e) {
				var image_id = $(this).parents('li').attr('image_id');
				var operate = $(this).attr('class');
				if(image_id == undefined || image_id <= 0 || operate == undefined) {
					alert('参数不正确，没有image id 或者没有 operate');
					return ;
				}
				var that = $(this);

				if(operate == 'del') {
					$.ajax({
						url : '/admin/image/delete',
						data: {image_id:image_id},
						dataType: 'json',
						success: function(result) {
							if(result.code == 200) {
								that.parents('li').remove();
							} else {
								alert(result.message);
							}
						},
						error: function(e) {
							alert('删除失败!');
						}
					});
				}
			});

			$(document).delegate('#upload-pop-btn', 'click', function(){
				var tpl = $('#multiuploadpic-tpl').html();
				var uploadHtml = Mustache.to_html(tpl, {});
				
				$('body').append(uploadHtml).addClass('no-scroll');

				var room_id = $('input[name=room_id]').val();

				var selectFirst = true;

				var successTpl = '<li image_id="{{id}}" class="clearfix"><span class="pic"><img src="{{image_url_small}}" original-src="{{image_url}}" /></span><span class="text"><textarea class="txt" maxlength="100" placeholder="添加照片描述" />{{desc}}</textarea></span><a href="javascript:void(0);" class="delete-pic">\u5220\u9664</a></li>';

				$('#uploadBtn').uploadify({
					'debug':		false,
					'formData'      : {'object' : 'room', 'object_id' : room_id},
					'buttonText': 	'',
					'height': 		35,
					'width':		200,
					'multi': 		true,
					'preventCaching' : true,
					'queueID': 	'fileQueue',
					'fileObjName': 'Filedata',
					'fileSizeLimit': '10MB',
					'queueSizeLimit' : 10,
					'removeCompleted' : false,
					'progressData' :	'percentage',
					'folder': 		'UploadFile',
					'uploader': 	'/admin/image/uploadify',
					'swf' : 		'/images/uploadify.swf',
					'itemTemplate' : '<li class="picitem clearfix" id="${fileID}"><span class="name">${fileName}</span><span class="byte">${fileSize}</span><span class="progress"><b style="width:0%"></b><em></em></span></li>',
					'onSelect': function(file) {
						if(selectFirst) {
							selectFirst = false;
							$('.swfupload-btn').addClass('append-upload-status').removeClass('swfupload-btn');
							$('.multiupload-wrap').css({'left':'-888px'});
						}
					},
					'onCancel' : function(file) {
					},
					'onDialogClose' : function(queueData) {
						$('.multiupload-wrap .listtip').html('已经上传（0/ '+ queueData.filesQueued +'）');
					},
					'onQueueComplete' : function(queueData) {
						$('.multiupload-wrap .listtip').html('已经上传（'+ queueData.uploadsSuccessful +'/ '+ queueData.filesQueued +'）');
					},
					'onSelectError' : function(file, errorCode, errorMsg) {
						alert(errorMsg);
					},
					'onUploadComplete' : function(file) {
					},
					'onUploadError' : function(file, errorCode, errorMsg, errorString) {
						alert(errorMsg);
					},
					'onUploadProgress' : function(file, bytesUploaded, bytesTotal, totalBytesUploaded, totalBytesTotal) {
						$('.piclist #' + file.id).addClass('uploading');
						var per =  bytesUploaded/bytesTotal * 100;
						$('.piclist #' + file.id + ' .progress b').css({'width': per + '%'});
						$('.piclist #' + file.id + ' .progress em').html('正在上传...' + per + '%');
					},
					'onUploadStart' : function(file) {
						$('.piclist #' + file.id + ' .progress em').html('开始上传');
						$('.complete-list').animate({opacity: 1}, 1000);
					},
					'onUploadSuccess' : function(file, data, response) {
						result = $.parseJSON(data);
						if(response && result.code == 200) {
							var html = Mustache.to_html(successTpl, result.data);
							$('.complete-list ul').append(html);
						
							$('.piclist #' + file.id).removeClass('uploading').addClass('success');
							$('.piclist #' + file.id + ' .progress em').html('上传成功');
						} else {
							$('.piclist #' + file.id).removeClass('uploading').addClass('fail');
							$('.piclist #' + file.id + ' .progress em').html('上传失败');
						}
					},
				});
			});
			$(document).delegate('.multiuploadPic-pop .close', 'click', function(){
				$('.multiuploadPic-pop').remove();
				$('body').removeClass('no-scroll');
			});
			$(document).delegate('.multiuploadPic-pop .delete-pic', 'click', function() {
				var image_id = $(this).parent('li').attr('image_id');
				var that = $(this);
				if(!image_id || image_id == undefined) {
					alert('无法删除');
					return ;
				}
				$.ajax({
					url : '/admin/image/delete',
					data: {image_id:image_id},
					dataType: 'json',
					success: function(result) {
						if(result.code == 200) {
							that.parent('li').remove();
						} else {
							alert(result.message);
						}
					},
					error: function(e) {
						alert('删除失败!');
					}

				});
			});
			$(document).delegate('#saveUploadBtn', 'click', function(){
				var tpl = $('#multimage-gallery-item-tpl').html();
				$('.complete-list li').each(function(){
					var image = $(this).find('img').attr('src');
					var desc = $(this).find('textarea').val();
					
					var html = Mustache.to_html(tpl , {url:image, desc:desc });
					$('.multimage-gallery ul').append(html);
					//colse pop
					$('.multiuploadPic-pop').remove();
					$('body').removeClass('no-scroll');
				});
			});

		},
		validate:function() {
			var _self = this;
			$form.validate({
				debug:false,
				submitHandler: function(e) {
					var submitBtn = $('#submit-btn');
					$.ajax({
						url:'/admin/package/saveRoom',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function(){
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								if(result.data['room_id'] > 0) {
									$('input[name=room_id]').val(result.data['room_id']); 
								}
								submitBtn.addClass('btn-success').html('保存成功');

								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存').attr('disabled', false);
									if(result.data['new_room'] == 'ok' && result.data['room_id'] > 0) {
										window.location.href = '/admin/package/rooms?package_id=' + result.data['package_id'] + '&room_id=' + result.data['room_id'];
									}
								}, 2000);
							} else {
								submitBtn.addClass('btn-error').html('保存失败，请联系喵喵');
								alert(result.message);
								setTimeout(function() {
									submitBtn.removeClass('btn-error');
									submitBtn.text('保存').attr('disabled', false);
								}, 3000);
							}
						},
						error: function(e){
							submitBtn.text('保存').attr('disabled', false);
						}
					});
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					name:{
						required:true
					},
					desc:{
						required: true
					}
				},
				messages:{
					name: {
						required:"房型名称不能为空"
					},
					desc:{
						required:"房型描述不能为空"
					}
				}
			});
		}
	};
});
