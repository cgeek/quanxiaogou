define(function(require, exports, module) {
	var $ = require('jquery');
	require('json');

	var $form = $("form");
	
	require('jquery-rte');
	require('jquery-rte-css');
	var EditToolBar = require('jquery-rte-tb');

	return {
		init:function() {
			var that = this;
			var contentRTE = $('#detailText').rte({
				controls_rte: EditToolBar.rte_toolbar,
				controls_html: EditToolBar.html_toolbar,
				width: 680,
				height:400
			});

			$(document).delegate('#submit-btn', 'click', function(e){
				e.preventDefault();
				var content = contentRTE.detailText.get_content();
				if(content.length <= 0) {
					alert('详情不能为空');
				}
				$('textarea[name=detail]').html(content);

				var submitBtn = $('#submit-btn');
					$.ajax({
						url:'/admin/package/saveActivity',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function(){
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								submitBtn.addClass('btn-success').html('保存成功');
								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存').attr('disabled', false);
								}, 2000);
							}
						},
						error: function(e) {
							submitBtn.text('保存').attr('disabled', false);
						}
					});

			});
		}
	}
});
