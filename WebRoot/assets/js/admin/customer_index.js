define(function(require, exports, module) {
	var $ = require('jquery');
	require('bootstrap')
	require('bootstrap-datepicker');

	var Mustache = require('mustache');
	return {
		init: function() {
			var that = this;
			that.initCheckList();

			$(document).delegate('.claim-btn', 'click', function(){
				var btn = $(this);
				var customer_id = $(this).parents('tr').attr('customer_id');
				if(customer_id == undefined || customer_id == '') {
					alert('参数错误');
					return ;
				}
				$.ajax({
					url : '/admin/customer/claim?customer_id=' + customer_id,
					type: 'GET',
					data: {},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
							alert('恭喜你，认领成功!');
							btn.parents('td').append('<p>[' + result.data + ']</p>');
							btn.remove();
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误');
					}
				});
			});
		},
		
		initCheckList:function() {
			$(document).delegate('.check_list_btn', 'click', function() {
				var tpl = $('#customer_check_list_modal_tpl').html();
				var customer_id = $(this).parents('tr').attr('customer_id');
				if(customer_id == undefined || customer_id == '') {
					alert('参数错误');
					return ;
				}
				$.ajax({
					url : '/admin/todo/checkList?customer_id=' + customer_id,
					type: 'GET',
					data: {},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
							$('#modalView .modal-content').html(Mustache.to_html(tpl, result.data));
							$('#modalView').modal('show');
							$('.date-picker').datetimepicker({
								format: "yyyy-mm-dd hh:ii",
				        		language:  'zh-CN',
				    		    weekStart: 1,
						        todayBtn:  0,
								autoclose: 1,
								todayHighlight: 1,
								startView: 2,
								minView: 1,
								forceParse: 0,
								minuteStep: 20,
								orientation: "bottom left"
							});
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误');
					}
				});
			});

			//点击文字可编辑
			$(document).delegate('.task-list .check-item p', 'click', function(){
				$('.task-list .check-item-text').removeClass('editing');
				$(this).parents('.check-item-text').addClass('editing');
			});
			//添加x，可取消
			$(document).delegate('.task-list .check-item .cancel', 'click', function(){
				$(this).parents('li').find('.check-item-text').removeClass('editing');
			});
			//点击删除，删除任务
			$(document).delegate('.task-list .check-item .delete', 'click', function() {
				var item = $(this).parents('li');
				var check_item_id = $(this).parents('li').attr('item-id');
				if(check_item_id == undefined || check_item_id == '') {
					alert('参数错误');
					return ;
				}

				$.ajax({
					url : '/admin/todo/closeCheckItem',
					type: 'POST',
					data: {'check_item_id': check_item_id},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
							item.remove();
							$('.progress-bar').css('width', result.data.progress + '%');
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误');
					}
				});
			});

			//取消,添加新任务
			$(document).delegate('.new-task .cancel', 'click', function(){
				$('.new-task textarea').val('');
				$('.new-task').removeClass('focus');
			});
			$(document).delegate('.new-task textarea','click', function(){
				$(this).parents('.new-task').addClass('focus');
			});
			//点击保存修改，保存修改任务
			$(document).delegate('.task-list .check-item .save-btn', 'click', function(e) {
				var item = $(this).parents('li');
				var title = item.find('textarea').val();
				var due_date = item.find('input').val();
				if(title == undefined || title =='') {
					item.find('textarea').focus();
					return ;
				}
				var check_item_id = $(this).parents('li').attr('item-id');
				if(check_item_id == undefined || check_item_id == '') {
					alert('参数错误');
					return ;
				}

				$.ajax({
					url : '/admin/todo/updateCheckItem',
					type: 'POST',
					data: {'check_item_id': check_item_id, 'title': title, 'due_date': due_date},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
							item.find('.check-item-text').removeClass('editing');
							item.find('p').html(title);
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误, 请联系喵喵');
					}
				});
			});

			//添加新任务
			$(document).delegate('.new-task .add-checklist-item', 'click', function(e) {
				var item = $(this).parents('.new-task');
				var title = item.find('textarea').val();
				var due_date = item.find('input').val();
				if(title == undefined || title =='') {
					item.find('textarea').focus();
					return ;
				}
				var customer_id = item.attr('customer_id');
				if(customer_id == undefined ) {
					alert('参数不正确');
					return ;
				}
				var task_list_item_tpl = $('#task-list-item-tpl').html();
				$.ajax({
					url : '/admin/todo/newCheckItem',
					type: 'POST',
					data: {'customer_id': customer_id, 'title': title, 'due_date': due_date},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
							$('.task-list').append(Mustache.to_html(task_list_item_tpl, result.data.check_item));
							$('.new-task textarea').val('');
							$('.new-task input').val('');
							$('.new-task').removeClass('focus');
							$('.progress-bar').css('width', result.data.progress + '%');
							$('.date-picker').datetimepicker({
								format: "yyyy-mm-dd hh:ii",
				        		language:  'zh-CN',
				    		    weekStart: 1,
						        todayBtn:  0,
								autoclose: 1,
								todayHighlight: 1,
								startView: 2,
								minView: 1,
								forceParse: 0,
								minuteStep: 20,
								orientation: "bottom left"
							});
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误, 请联系喵喵');
					}
				});
			});

			//update check status
			$(document).delegate('.task-list .check-item .checkbox', 'click', function(e) {
				e.preventDefault();
				var item = $(this).parents('li');
				var check_item_id = $(this).parents('li').attr('item-id');
				var status = 1;
				if(item.hasClass('complete')) {
					status = 0;
				}
				if(check_item_id == undefined || check_item_id == '') {
					alert('参数错误');
					return ;
				}
				$.ajax({
					url : '/admin/todo/updateCheckItemStatus',
					type: 'POST',
					data: {'check_item_id': check_item_id, 'status': status},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
							if(item.hasClass('complete')) {
								item.removeClass('complete');
							} else {
								item.addClass('complete');
							}
							$('.progress-bar').css('width', result.data.progress + '%');
						} else {
							alert(result.message);
						}
					},
					error: function() {
						alert('服务器错误, 请联系喵喵');
					}
				});
			});
			
			//添加动态
			$(document).delegate('.new-comment .cancel', 'click', function(){
				$('.new-comment textarea').val('');
				$('.new-comment').removeClass('focus');
			});
			$(document).delegate('.new-comment textarea','click', function(){
				$(this).parents('.new-comment').addClass('focus');
			});

			//保存评论
			$(document).delegate('.new-comment  .add-comment-btn ', 'click', function(e) {
				var btn = $(this);
				var item = $(this).parents('.new-comment');
				var content = item.find('textarea').val();
				if(content == undefined || content =='') {
					item.find('textarea').focus();
					return ;
				}
				var customer_id = item.attr('customer_id');
				var admin_id = $('body').attr('admin_id');
				if(customer_id == undefined || customer_id == '') {
					alert('参数错误');
					return ;
				}

				$.ajax({
					url : '/admin/comment/save',
					type: 'POST',
					data: {'object_id': customer_id, 'object': 'customer-log', 'admin_id': admin_id, 'content': content},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
							//item.find('.check-item-text').removeClass('editing');
							var html = '<li>' + content +'<p style="font-size:12px; color:#ccc;">1秒前</p></li>';
							$('.activity-list').prepend(html);
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误, 请联系喵喵');
					}
				});
			});

		}
	}
});
