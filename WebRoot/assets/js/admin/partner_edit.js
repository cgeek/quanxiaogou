define(function(require, exports, module) {
	var $ = require('jquery');
	require('jquery-validate');
	require('json');
	require('bootstrap')
	require('bootstrap-datepicker');
	var Mustache = require('mustache');

	var $form = $("#partner_edit_form");

	return {
		init:function() {
			var that = this;
			that.validate();

			$form.find(':input[value=""]:first').select();

            //指派地接
            $(document).delegate('.user_list li', 'click', function(){
                var user_id = $(this).attr('user_id');
                $('input[name=user_id]').val(user_id);
            });


			$('.date-picker').datetimepicker({
				format: "yyyy-mm-dd",
        		language:  'zh-CN',
    		    weekStart: 1,
		        todayBtn:  0,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				minView: 2,
				minuteStep: 20,
				orientation: "bottom left"
			});

		},
		showError:function(error) {
			//$form.find('.page-account-alert-message').show('fast').addClass('alert alert-error').fadeOut('fast', function() {$(this).html(error)}).fadeIn('fast');
		},
		validate:function() {
			var _self = this;
			$form.validate({
				debug:true,
				submitHandler: function(e) {
					var submitBtn = $('#submit-btn');
					$.ajax({
						url:'/admin/partner/savePartner',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function(){
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								$('input[name=id]').val(result.data['partner_id']);
								submitBtn.addClass('btn-success').html('保存成功');
								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存修改').attr('disabled', false);
								}, 2000);
							} else {
								submitBtn.addClass('btn-error').html('保存失败');
								alert(result.message);
								setTimeout(function() {
									submitBtn.removeClass('btn-error');
									submitBtn.text('保存修改').attr('disabled', false);
								}, 2000);
							}
						},
						error: function(e){
							submitBtn.text('保存修改').attr('disabled', false);
						}
					});
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					contact:{
						required:true
					},
					destination:{
						required: true
					},
					remark:{
						required: true
					},
					start_date: {
						required:true
					}
				},
				messages:{
					contact: {
						required:"联系人不能为空"
					},
					remark:{
						required:"备注信息不能为空"
					},
					destination:{
						required:"目的地不能为空"
					},
					start_date: {
						required: "出发日期不能为空"
					}
				}
			});
		}
	};
});
