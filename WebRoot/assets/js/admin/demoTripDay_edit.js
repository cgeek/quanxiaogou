define(function(require, exports, module) {
	var $ = require('jquery');
	require('jquery-validate');
	require('json');

	var Mustache = require('mustache');

	var $form = $("#demoTripDay_form");
	
	return {
		init:function() {
			var that = this;
			that.validate();
		},
		initUploadCoverImage: function(){
			var city_id = $('input[name=city_id]').val();
			//上传封面图片
			$('#fileupload').fileupload({
				url : '/admin/image/uploadify',
				dataType: 'json',
				acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
				maxFileSize: 5000000, // 5 MB
				formData:{object:"city", object_id: city_id},
				send: function(e, data) {
					$('.cover-image-box img').hide();
					$('.cover-image-box .loading').show();
				},
				done: function(e, data) {
					var result = data.result;
					if(result.code == 200) {
						image_hash = result.data['image_hash'];
						$('input[name=cover_image]').val(image_hash);
						$('.cover-image-box img').attr('src', result.data['image_url_small']).show();

						$('.cover-image-box .loading').hide();
						//上传的图片同时添加到相册
						var tpl = $('#multimage-gallery-item-tpl').html();
						if(tpl) {
							var url = result.data['image_url_small'];
							var html = Mustache.to_html(tpl , {url:url, desc:'', id: result.data['id']});
							$('.multimage-gallery ul').append(html);
						}
					} else {
							alert(result.message);
					}
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progress .progress-bar').css('width',progress + '%');
				},
				fail: function(e, data) {
					$('.cover-image-box .loading').html('上传失败');
				}
			}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

		},
		validate:function() {
			var _self = this;
			$form.validate({
				debug:false,
				submitHandler: function(e) {
					var submitBtn = $('#submit-btn');
					$.ajax({
						url:'/admin/demoTripDay/save',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function(){
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								$('input[name=city_id]').val(result.data['city_id']); 
								submitBtn.addClass('btn-success').html('保存成功');
								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存').attr('disabled', false);
									if(result.data['new_hotel'] == 'ok' && result.data['hotel_id'] > 0) {
										window.location.href = '/admin/city/edit?city_id=' + result.data['hotel_id'];
									}
								}, 2000);
							} else {
								submitBtn.addClass('btn-error').html('保存失败，请联系喵喵');
								alert(result.message);
								setTimeout(function() {
									submitBtn.removeClass('btn-error');
									submitBtn.text('保存').attr('disabled', false);
								}, 3000);
							}
						},
						error: function(e){
							submitBtn.text('保存').attr('disabled', false);
						}
					});
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					title:{
						required:true
					},
					desc:{
						required: true
					}
				},
				messages:{
					title: {
						required:"标题"
					},
					desc:{
						required:"行程描述"
					}
				}
			});
		}
	};
});
