define(function(require, exports, module) {
	var $ = require('jquery');

    require('bootstrap-editable');
    require('bootstrap-editable-css');
	
	require('jquery-ui-widget');
	require('fileupload');
	
	
	return {
		init: function() {
			var that = this;
			that.initUploadFile();
		    that.initCommentList();
            //$.fn.editable.defaults.mode = 'inline';
            $('.editable-input').editable({
                url: '/admin/trade/updateTravelInfo',
                params: function(params) {
                    params.infoType = $(this).attr('info-type');
                    return params;
                },
                title: 'Enter username'
            });

            $(document).delegate('.view-item-info-btn','click', function(){
                var item_id = $(this).attr('item_id');
                var ele = $('#info-' + item_id);
                if(ele.is(':hidden')) {
                    ele.slideDown();
                } else {
                    ele.slideUp();
                }
            });

            $(document).delegate('.update-status-btn', 'click', function(){
                var item_id = $(this).attr('item_id');
                var trade_id = $(this).attr('trade_id');
                var status = $(this).attr('status');
                if(item_id == undefined || item_id <= 0) {
                    alert('参数错误，无法删除');
                    return false;
                }
                if(confirm("请确定更新状态前，已经填写备注原因!")) {
                    $.ajax({
                        url : '/admin/trade/updateStatus',
                        data : {item_id: item_id, status:status, trade_id:trade_id},
                        type: 'post',
                        dataType : 'JSON',
                        success: function(result) {
                            if(result.code == 200) {
                                alert(result.message);
                                //window.location.reload();
                            } else {
                                alert(result.message);
                            }
                        },
                        error: function() {
                            alert('操作失败！请联系喵喵');
                        }
                    });
                }
            });

            $(document).delegate('.update-remind-status-btn', 'click', function(){
                var item_id = $(this).attr('item_id');
                var trade_id = $(this).attr('trade_id');
                var status = $(this).attr('status');
                if(item_id == undefined || item_id <= 0) {
                    alert('参数错误');
                    return false;
                }
                if(confirm("请确定更新状态前，已经填写备注原因!")) {
                    $.ajax({
                        url : '/admin/trade/updateRemindStatus',
                        data : {item_id: item_id, status:status, trade_id:trade_id},
                        type: 'post',
                        dataType : 'JSON',
                        success: function(result) {
                            if(result.code == 200) {
                                alert(result.message);
                                //window.location.reload();
                            } else {
                                alert(result.message);
                            }
                        },
                        error: function() {
                            alert('操作失败！请联系喵喵');
                        }
                    });
                }
            });
        },
		initUploadFile: function(){
            //上传封面图片
            $('.fileupload').bind('fileuploadsubmit', function (e, data) {
                    var lineItemId =$(e.target).parents('.fileinput-button').find('input[name=lineItemId]').val();
                    data.formData = {'itemId': lineItemId};
            });
            $('.fileupload').fileupload({
                    url : '/admin/trade/uploadFile',
                    dataType: 'json',
                    acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                    maxFileSize: 5000000, // 5 MB
                    formData:{'itemId':'11'},
                    send: function(e, data) {
                    },
                    done: function(e, data) {
                            var result = data.result;
                            if(result.code == 200) {
                                alert('上传成功');
                                window.location.reload();
                            } else {
                                alert('上传失败，请重新上传!');
                            }
                    },
                    progressall: function (e, data) {
                    },
                    fail: function(e, data) {
                        console.log(data);
                    }
            }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
		},
		initCommentList: function(){
			var btn = $('#comment-submit-btn');
			var form = $('#comment-form');
			$(document).delegate('#comment-submit-btn', 'click', function(e) {
				e.preventDefault();
				var content = $('textarea[name=content]').val();
				if(content == '') {
					$('textarea[name=content]').focus();
					return ;
				}
				btn.disabled = true;
				btn.html('正在提交...');
				$.ajax({
					url : '/admin/trade/saveNote',
					data: form.serialize(),
					dataType : 'json',
					success: function(result){
						if(result.code == 200) {
							window.location.reload();
						} else {
							alert(result.message);
							btn.disabled = false;
							btn.html('提交');
						}
					},
					error: function() {
						alert('提交失败！请联系喵喵');
						btn.html('提交');
						btn.disabled = false;
					}
				});

			});
		}

    }
});
