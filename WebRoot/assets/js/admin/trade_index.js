define(function(require, exports, module) {
	var $ = require('jquery');

	return {
		init: function() {
			var that = this;
			
			$(document).delegate('.claim-btn', 'click', function(){
				var trade_id = $(this).parents('tr').attr('trade_id');
                $.ajax({
                    url : '/admin/trade/claimTrade',
                    data : { trade_id:trade_id},
                    type: 'post',
                    dataType : 'JSON',
                    success: function(result) {
                        if(result.code == 200) {
                            alert('认领成功！');
                            window.location.reload();
                        } else {
                            alert(result.message);
                        }
                    },
                    error: function() {
                        alert('操作失败！请联系喵喵');
                    }
                });
			});

		}
	}
});