define(function(require, exports, module) {
	var $ = require('jquery');
	require('bootstrap')

	return {
		init: function() {
			var that = this;

			$(document).delegate('.delete_demo_btn', 'click', function(e){

				var r = confirm("真的要删除吗？谨慎删除哦");
				if(!r) {
					return false;
				}

				var btn = $(this);
				var demo_id = $(this).parents('li').attr('demo_id');
				var line = $(this).parents('li');
				if(demo_id == undefined || demo_id == '') {
					alert('参数错误');
					return ;
				}
				$.ajax({
					url : '/admin/demoTripDay/save',
					type: 'POST',
					data: {'id': demo_id, 'status' : '-1'},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
							line.remove();
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误');
					}
				});
			});

		}
	}
});
