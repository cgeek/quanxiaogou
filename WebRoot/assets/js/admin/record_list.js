define(function(require, exports, module) {
	var $ = require('jquery');
	require('bootstrap')

	require('jquery-ui-widget');
	require('fileupload');

	return {
		init: function() {
			var that = this;
			that.initUpload();

			$(document).delegate('.record_customer_id_input', 'blur', function(e){
				var btn = $(this);
				var customer_id = $(e.target).val();
				if(customer_id == 0) {
					return ;
				}
				var record_id = $(this).parents('tr').attr('record_id');
				if(record_id == undefined || record_id == '') {
					alert('参数错误');
					return ;
				}
				$.ajax({
					url : '/admin/order/updateAlipayRecord',
					type: 'POST',
					data: {'record_id': record_id, 'customer_id' : customer_id},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误');
					}
				});
			});

			$(document).delegate('.delete_record_btn', 'click', function(e){
				var btn = $(this);
				var record_id = $(this).parents('tr').attr('record_id');
				var line = $(this).parents('tr');
				if(record_id == undefined || record_id == '') {
					alert('参数错误');
					return ;
				}
				$.ajax({
					url : '/admin/order/updateAlipayRecord',
					type: 'POST',
					data: {'record_id': record_id, 'status' : '-1'},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
							line.remove();
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误');
					}
				});
			});

		},
		initUpload: function(){
			//上传交易记录
			$('#fileupload').fileupload({
				url : '/admin/order/importAlipayRecord',
				dataType: 'json',
				acceptFileTypes: /(\.|\/)(txt|zip)$/i,
				maxFileSize: 5000000, // 5 MB
				formData:{},
				send: function(e, data) {
				},
				done: function(e, data) {
					var result = data.result;
					if(result.code == 200) {
						alert(result.message);
					} else {
						alert(result.message);
					}
				},
				progressall: function (e, data) {
				},
				fail: function(e, data) {
					$('.cover-image-box .loading').html('上传失败');
				}
			}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

		}
	}
});
