define(function(require, exports, module) {
	var $ = require('jquery');
	require('jquery-validate');
	require('json');

	require('uploadify');

	require('jquery-ui-widget');
	require('fileupload');

	var Mustache = require('mustache');

	var $form = $("#city_edit_form");
	
	return {
		init:function() {
			var that = this;
			that.validate();
			that.initUploadCoverImage();
			that.initDeleteBtn();

		},
		initDeleteBtn: function() {
			$(document).delegate('#delete-customer-btn', 'click', function(){
				var hotel_id = $('input[name=hotel_id]').val();
				if(hotel_id == undefined || hotel_id <= 0) {
					alert('无法删除');
					return false;
				}
				if(confirm("确定要删除吗!")) {
					$.ajax({
						url : '/admin/city/deletePackage',
						data : {city_id: hotel_id},
						type: 'post',
						dataType : 'JSON',
						success: function(result) {
							if(result.code == 200) {
								window.location.href = '/admin/city?type=hotel&status=0';
							} else {
								alert(result.message);
							}
						},
						error: function() {
							alert('删除失败！请联系喵喵');
						}
					});
				}
			});
		},
		initUploadCoverImage: function(){
			var city_id = $('input[name=city_id]').val();
			//上传封面图片
			$('#fileupload').fileupload({
				url : '/admin/image/uploadify',
				dataType: 'json',
				acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
				maxFileSize: 5000000, // 5 MB
				formData:{object:"city", object_id: city_id},
				send: function(e, data) {
					$('.cover-image-box img').hide();
					$('.cover-image-box .loading').show();
				},
				done: function(e, data) {
					var result = data.result;
					if(result.code == 200) {
						image_hash = result.data['image_hash'];
						$('input[name=cover_image]').val(image_hash);
						$('.cover-image-box img').attr('src', result.data['image_url_small']).show();

						$('.cover-image-box .loading').hide();
						//上传的图片同时添加到相册
						var tpl = $('#multimage-gallery-item-tpl').html();
						if(tpl) {
							var url = result.data['image_url_small'];
							var html = Mustache.to_html(tpl , {url:url, desc:'', id: result.data['id']});
							$('.multimage-gallery ul').append(html);
						}
					} else {
							alert(result.message);
					}
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progress .progress-bar').css('width',progress + '%');
				},
				fail: function(e, data) {
					$('.cover-image-box .loading').html('上传失败');
				}
			}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');

		},
		validate:function() {
			var _self = this;
			$form.validate({
				debug:false,
				submitHandler: function(e) {
					var submitBtn = $('#submit-btn');
					$.ajax({
						url:'/admin/city/save',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function(){
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								$('input[name=city_id]').val(result.data['city_id']); 
								submitBtn.addClass('btn-success').html('保存成功');
								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存').attr('disabled', false);
									if(result.data['new_hotel'] == 'ok' && result.data['hotel_id'] > 0) {
										window.location.href = '/admin/city/edit?city_id=' + result.data['hotel_id'];
									}
								}, 2000);
							} else {
								submitBtn.addClass('btn-error').html('保存失败，请联系喵喵');
								alert(result.message);
								setTimeout(function() {
									submitBtn.removeClass('btn-error');
									submitBtn.text('保存').attr('disabled', false);
								}, 3000);
							}
						},
						error: function(e){
							submitBtn.text('保存').attr('disabled', false);
						}
					});
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					name:{
						required:true
					},
					name_english:{
						required: true
					},
					country_name: {
						required: true
					}
				},
				messages:{
					name: {
						required:"名称不能为空"
					},
					name_english:{
						required:"英文名不能为空"
					},
					country_name: {
						required:"国家不能为空"
					}
				}
			});
		}
	};
});
