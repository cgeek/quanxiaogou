define(function(require, exports, module) {
	var $ = require('jquery');
	require('json');

	var $form = $("form");
	
	require('jquery-rte');
	require('jquery-rte-css');
	var EditToolBar = require('jquery-rte-tb');

	require('jquery-ui-widget');
	require('fileupload');

	var Mustache = require('mustache');

	return {
		init:function() {
			var that = this;
			var contentRTE = $('#detailText').rte({
				controls_rte: EditToolBar.rte_toolbar,
				controls_html: EditToolBar.html_toolbar,
				width: 340,
				height:300
			});

			that.initUploadCoverImage();

			$(document).delegate('#submit-btn', 'click', function(e){
				e.preventDefault();
				var content = contentRTE.detailText.get_content();
				if(content.length <= 0) {
					alert('详情不能为空');
				}
				$('textarea[name=content]').val(content);

				var submitBtn = $('#submit-btn');
					$.ajax({
						url:'/admin/weixin/saveContent',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function(){
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								submitBtn.addClass('btn-success').html('保存成功');
								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存').attr('disabled', false);
								}, 2000);
							}
						},
						error: function(e) {
							submitBtn.text('保存').attr('disabled', false);
						}
					});

			});
		},
		initUploadCoverImage: function(){
			var content_id = $('input[name=id]').val();
			//上传封面图片
			$('#fileupload').fileupload({
				url : '/admin/image/uploadify',
				dataType: 'json',
				acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
				maxFileSize: 5000000, // 5 MB
				formData:{object:"weixin_content", object_id: content_id},
				send: function(e, data) {
					$('.weixin-cover-image-box img').hide();
					$('.weixin-cover-image-box .loading').show();
				},
				done: function(e, data) {
					var result = data.result;
					if(result.code == 200) {
						image_hash = result.data['image_hash'];
						$('input[name=cover_image]').val(image_hash);
						$('.weixin-cover-image-box img').attr('src', result.data['image_url']).show();

						$('.weixin-cover-image-box .loading').hide();
					} else {
						alert(result.message);
					}
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progress .progress-bar').css('width',progress + '%');
				},
				fail: function(e, data) {
					$('.weixin-cover-image-box .loading').html('上传失败');
				}
			}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
		}
	}
});
