define(function(require, exports, module) {
	var $ = require('jquery');
	require('jquery-validate');
	require('json');
	require('bootstrap-datepicker');

	var Mustache = require('mustache');

	var $form = $("#order_edit_form");
	return {
		init:function() {
			var that = this;
			that.initForm();
			that.initTaobaoTrade();
			that.validate();

			$('.date-picker').datetimepicker({
				format: "yyyy-mm-dd",
        		language:  'zh-CN',
    		    weekStart: 1,
		        todayBtn:  0,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				minView: 2,
				forceParse: 0
			});
		},
		initForm: function() {
			//支付方式
			var paymentType =$('input[name=payment_type]:checked').val();
			if(paymentType == 'all') {
				$('.down_payment_input').hide();
			}
			$('input[name=payment_type]').change(function(){
				var type = $(this).val();
				if(type == 'down') {
					$('.down_payment_input').show();
				} else {
					$('.down_payment_input').hide();
				}
			});
			//订购方式
			
			var bookingType =$('input[name=booking_type]:checked').val();
			if(bookingType == 'agent') {
				$('.booking_url').hide();
			}
			$('input[name=booking_type]').change(function(){
				var type = $(this).val();
				if(type == 'agent') {
					$('.booking_url').hide();
				} else {
					$('.booking_url').show();
				}
			});
		},
		initTaobaoTrade:function() {
			$(document).delegate('.show-source-url-btn', 'click', function(){
				$('.source_url_input').show();
			});
			$(document).delegate('.taobao-trade-btn', 'click', function(){
				var tpl = $('#taobao_tredes_tpl').html();
					$.ajax({
						url : '/admin/order/taobao',
						data : {},
						type: 'post',
						dataType : 'JSON',
						success: function(result){
							if(result.code == 200) {
								$('#modalView .modal-content').html(Mustache.to_html(tpl, result.data));
								$('#modalView').modal('show');
							} else {
								alert(result.message);
							}
						},
						error: function() {
							alert('删除失败！请联系喵喵');
						}
					});
			});
			$(document).delegate('.taobao_trade_item .btn', 'click', function(){
				var trade_elem = $(this).parents('tr');
				var taobao_trade_id = trade_elem.attr('trade_id');
				var total_fee = trade_elem.attr('total_fee');
				var nick_name = trade_elem.attr('buyer_nick');

				$('input[name=income]').val(total_fee);
				$('input[name=payment_nick_name]').val(nick_name);
				$('input[name=payment_trade_id]').val(taobao_trade_id);
				$('#modalView').modal('hide');
			});
		},
		validate:function() {
			var _self = this;
			$form.validate({
				debug:true,
				submitHandler: function(e) {
					var submitBtn = $('#submit-btn');
					$.ajax({
						url:'/admin/order/saveOrder',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function(){
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								$('input[name=order_id]').val(result.data['order_id']); 
								submitBtn.addClass('btn-success').html('保存成功');
								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存修改').attr('disabled', false);
								}, 2000);
							} else {
								submitBtn.addClass('btn-error').html('保存失败，请联系喵喵');
								alert(result.message);
								setTimeout(function() {
									submitBtn.removeClass('btn-error');
									submitBtn.text('保存修改').attr('disabled', false);
								}, 3000);
							}
						},
						error: function(e){
							submitBtn.text('保存修改').attr('disabled', false);
						}
					});
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					title:{
						required: true
					},
					start_date:{
						required:true
					},
					payment_type:{
						required:true
					},
					total_fee:{
						required:true
					},
					payment:{
						required:true
					},
					remark: {
						required:true
					}
				},
				messages:{
					title:{
						required:"标题不能为空"
					},
					start_date:{
						required:"开始时间不能为空"
					},
					payment_type:{
						required:"支付类型得选"
					},
					total_fee:{
						required:"金额不能为空"
					},
					payment:{
						required:"必填,不需支出填0"
					},
					remark: {
						required:"备注写点什么吧"
					}
				}
			});
		}
	};
});
