define(function(require, exports, module) {
	var $ = require('jquery');
	require('jquery-validate');
	require('json');
	require('bootstrap-datepicker');

	var $form = $("#traveler_edit_form");
	return {
		init:function() {
			var that = this;
			that.validate();

			$form.find(':input[value=""]:first').select();

			$('.date-picker').datetimepicker({
				format: "yyyy-mm-dd",
        		language:  'zh-CN',
    		    weekStart: 1,
		        todayBtn:  0,
				autoclose: 1,
				todayHighlight: 1,
				startView: 2,
				minView: 2,
				forceParse: 0
			});
		},
		showError:function(error) {
			//$form.find('.page-account-alert-message').show('fast').addClass('alert alert-error').fadeOut('fast', function() {$(this).html(error)}).fadeIn('fast');
		},
		validate:function() {
			$.validator.addMethod("birthdayFormat", function (value, element) { return value.match(/^\d\d\d\d\-\d\d\-\d\d$/); },"请输入正确日期格式：1987-10-01");
			var _self = this;
			$form.validate({
				debug:true,
				submitHandler: function(e) {
					var submitBtn = $('#submit-btn');
					$.ajax({
						url:'/admin/traveler/saveTraveler',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function(){
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								$('input[name=traveler_id]').val(result.data['traveler_id']); 
								submitBtn.addClass('btn-success').html('保存成功');
								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存修改').attr('disabled', false);
								}, 2000);
							} else {
								submitBtn.addClass('btn-error').html('保存失败，请联系喵喵');
								alert(result.message);
								setTimeout(function() {
									submitBtn.removeClass('btn-error');
									submitBtn.text('保存修改').attr('disabled', false);
								}, 3000);
							}
						},
						error: function(e){
							submitBtn.text('保存修改').attr('disabled', false);
						}
					});
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					name:{
						required:true
					},
					last_name: {
						required:true
					},
					first_name: {
						required:true
					},
					birthday: {
						birthdayFormat: true
					}
				},
				messages:{
					name: {
						required:"联系人不能为空"
					},
					last_name: {
						required:"姓不能空"
					},
					first_name: {
						required:"名字不能空"
					}
				}
			});
		}
	};
});
