define(function(require, exports, module) {
	var $ = require('jquery');
	require('jquery-validate');
	require('jquery-form');
	require('json');
	var cookie = require('cookie');

	var $form = $("#collect-form");
	
	return {
		init:function() {
			var that = this;
			that.initCookie();
			that.validate();

			$form.find(':input[value=""]:first').focus();
		},
		showError:function(error) {
			//$form.find('.page-account-alert-message').show('fast').addClass('alert alert-error').fadeOut('fast', function() {$(this).html(error)}).fadeIn('fast');
		},
		initCookie:function() {
			var customer = $.cookie('customer_form');
			if(customer != '' && customer != undefined) {
				customer = JSON.parse(customer);
				$.each(customer, function(i) {
					var field = customer[i];
					$('[name=' + field.name +']').val(field.value);
				});
			}
		},
		validate:function() {
			var _self = this;
			$.validator.addMethod("isPhone", function(value,element) {
				var length = value.length;
				var tel = /^1[3|4|5|8|7][0-9]\d{4,8}$/;
				return this.optional(element) || (tel.test(value));  
			}, "请正确填写您的联系电话"); 

			$form.validate({
				debug:false,
				submitHandler: function(e) {
					$.cookie('customer_form', JSON.stringify($form.serializeArray()));
					var submitBtn = $('#submit-btn');


					$.ajax({
						url:'/customer/collect',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function(){
							submitBtn.attr('disabled', true);
							submitBtn.html('正在提交...');
						},
						success: function(result) {
							if(result.code == 200) {
								$.removeCookie('customer_form'); 
								$('#online-contact-form').hide();
								$('#post-success-box').show();
							
								$('input[name=contact]').val('');
                                $('input[name=mobile]').val('');
								$('input[name=origin]').val('');
                                $('input[name=destination]').val('');
                                $('input[name=num_of_people]').val('');
								$('input[name=weixin]').val('');
                                $('input[name=email]').val('');
								$('textarea[name=remark]').val('');

                                $('.post-success-box').show();
                                $('#collect-form').hide();


								submitBtn.text('恭喜你，需求提交成功！');
								setInterval(function(){
									submitBtn.text('提交申请');
									submitBtn.attr('disabled', false);
									
								}, 5000);
							} else {
								submitBtn.text('提交失败！请联系客服');
								setInterval(function(){
									submitBtn.text('好了，给我来个管家');
									submitBtn.attr('disabled', false);
									
								}, 5000);
							}
						},
						error: function(e){
							submitBtn.text('提交');
							submitBtn.attr('disabled', false);
						},
						complete: function(){
						}
					});
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					contact:{
						required:true
					},
					mobile: {
						required:true,
						isPhone:true
					},
					remark: {
						required:true
					}
				},
				messages:{
					contact: {
						required:"请填写姓名"
					},
					mobile:{
						required:"请填写手机号",
						isPhone:"手机格式不正确"
					},
					remark: {
						required:"请填写需求"
					}
				}
			});
		}
	};
});
