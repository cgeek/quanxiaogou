define(function(require, exports, module) {
	var $ = require('jquery');
	require('bootstrap')

	return {
		init: function() {
			var that = this;
			$(document).delegate('.trip_customer_id_input', 'blur', function(e){
				var btn = $(this);
				var customer_id = $(e.target).val();
				if(customer_id == 0) {
					return ;
				}
				var trip_id = $(this).parents('tr').attr('trip_id');
				if(trip_id == undefined || trip_id == '') {
					alert('参数错误');
					return ;
				}
				$.ajax({
					url : '/trip/updateTripCustomer',
					type: 'POST',
					data: {'trip_id': trip_id, 'customer_id' : customer_id},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误');
					}
				});
			});

			$(document).delegate('.delete_trip_btn', 'click', function(e){

				var r = confirm("真的要删除吗？谨慎删除哦");
				if(!r) {
					return false;
				}
				var btn = $(this);

                var trip_id = $(this).parents('.tripActions').attr('trip_id');
                var line = $(this).parents('.tripCard');

				if(trip_id == undefined || trip_id == '') {
					alert('参数错误');
					return ;
				}
				$.ajax({
					url : '/trip/deleteTrip',
					type: 'POST',
					data: {'trip_id': trip_id, 'status' : '-1'},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
							line.remove();
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误');
					}
				});
			});

            $(document).delegate('.create-trip-btn', 'click', function(e) {
                var type = $(this).attr('type');
                if(type == 'demo') {
                    $('.modal-create-trip #typeDemo').attr('checked', true);
                } else {
                    $('.modal-create-trip #typeCustomer').attr('checked', true);
                }
                $('.modal-create-trip').modal('show');
            });

            $(document).delegate('.create-trip-submit-btn', 'click', function(e){
                var title = $('#create-trip-form').find('input[name=title]').val();
                var type = $('#create-trip-form').find('input[name=type]:checked').val();
                if(title == '' || title == undefined) {
                    $('#create-trip-form').find('input[name=title]').focus();
                    return false;
                }
                window.location.href = '/trip/quickCreate?type='+ type +'&title=' + title;

            })

            $(document).delegate('.copy-btn', 'click', function(e) {
				var btn = $(this);
				var trip_id = $(this).parents('.tripActions').attr('trip_id');
				var line = $(this).parents('.tripCard');
				if(trip_id == undefined || trip_id == '') {
					alert('参数错误');
					return ;
				}
                $('.modal-copy-trip').modal('show');
                $('#copy-form input[name=sourceId]').val(trip_id);
            });

            $(document).delegate('.copy-submit-btn', 'click', function(){
                var sourceId = $('#copy-form input[name=sourceId]').val();
                var targetCustomerId = $('#copy-form input[name=targetCustomerId]').val();
                var targetTitle= $('#copy-form input[name=targetTitle]').val();

                $.ajax({
                    url: '/trip/copy',
					type: 'POST',
					data: {'sourceId': sourceId, 'targetCustomerId' : targetCustomerId, 'targetTitle':targetTitle},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
                            window.location.href = '/trip';
						} else {
							alert(result.message);
						}
                        $('.modal-copy-trip').modal('hide');
					},
					error: function(){
						alert('服务器错误');
                        $('.modal-copy-trip').modal('hide');
					}
                });
            });

            $(document).delegate('.tripCard', 'click', function(e){
                if(e.target.tagName == 'DIV') {
                    var trip_id = $(this).attr('trip_id');
                    window.location.href= '/trip/edit?id=' + trip_id;
                    return false;
                }

            });
		}
	}
});
