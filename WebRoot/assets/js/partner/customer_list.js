define(function(require, exports, module) {
	var $ = require('jquery');
	require('bootstrap')

	return {
		init: function() {
			var that = this;

			$(document).delegate('.delete_customer_btn', 'click', function(e){

				var r = confirm("真的要删除吗？谨慎删除哦");
				if(!r) {
					return false;
				}
				var btn = $(this);

                var customer_id = $(this).attr('customer_id');
                var line = $(this).parents('.customerCard');

				if(customer_id == undefined || customer_id == '') {
					alert('参数错误');
					return false;
				}

				$.ajax({
					url : '/customer/delete',
					type: 'POST',
					data: {'customer_id': customer_id, 'status' : '-1'},
					dataType:'JSON',
					success:function(result) {
						if(result.code == 200) {
							line.remove();
						} else {
							alert(result.message);
						}
					},
					error: function(){
						alert('服务器错误');
					}
				});
			});

		}
	}
});
