define(function(require, exports, module) {
	var $ = require('jquery');
	require('jquery-validate');
	require('json');



    require('jquery-ui-widget');
    require('fileupload');

	var $form = $("#userSettingsForm");
	return {
		init:function() {
			var that = this;
			that.validate();

			$form.find(':input[value=""]:first').select();

            that.initUploadHeadImg();
		},
		showError:function(error) {
		},
        initUploadHeadImg: function(){

            //上传封面图片
            $('#fileupload').fileupload({
                url : '/image/uploadify',
                dataType: 'json',
                acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
                maxFileSize: 5000000, // 5 MB
                formData:{object:"userAvatar", object_id: '', action:'changeUserAvatar'},
                send: function(e, data) {
                    $('.companyLogo').hide();
                    $('.poi-cover-image-box .loading').show();
                },
                done: function(e, data) {
                    var result = data.result;
                    if(result.code == 200) {
                        image_hash = result.data['image_hash'];
                        console.log(result.data['image_url_thumb']);
                        $('input[name=cover_image]').val(image_hash);
                        $('#companyLogo').attr('src', result.data['image_url_thumb']).show();
                        $('.profile-userpic img').attr('src', result.data['image_url_thumb']);
                        $('.poi-cover-image-box .loading').hide();
                    } else {
                        alert(result.message);
                    }
                },
                progressall: function (e, data) {
                    var progress = parseInt(data.loaded / data.total * 100, 10);
                    $('#progress .progress-bar').css('width',progress + '%');
                },
                fail: function(e, data) {
                    $('.poi-cover-image-box .loading').html('上传失败');
                }
            }).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
        },
		validate:function() {
			var _self = this;
			$form.validate({
				debug:true,
				submitHandler: function(e) {
					var submitBtn = $('#submit-btn');
					$.ajax({
						url:'/user/settings',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function(){
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								submitBtn.addClass('btn-success').html('修改成功');
								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存修改').attr('disabled', false);
								}, 2000);
							} else {
								alert(result.message);
								setTimeout(function() {
									submitBtn.removeClass('btn-error');
									submitBtn.text('保存修改').attr('disabled', false);
								}, 3000);
							}
						},
						error: function(e){
							submitBtn.text('保存修改').attr('disabled', false);
						}
					});
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					nick_name:{
						required:true
					}
				},
				messages:{
					nick_name: {
						required:"请输入昵称"
					}
				}
			});
		}
	};
});
