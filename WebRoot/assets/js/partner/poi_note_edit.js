define(function(require, exports, module) {
	var $ = require('jquery');
	require('jquery-validate');
	require('json');

	require('uploadify');

	require('jquery-ui-widget');
	require('fileupload');

	var Mustache = require('mustache');

	var $form = $("#note_edit_form");
	
	return {
		init:function() {
			var that = this;
			that.validate();
			that.initUploadCoverImage();
			that.initDeleteBtn();

			$('#cancel-btn').click(function(){
				window.history.back();	
			});
		},
		initDeleteBtn: function() {
			$(document).delegate('#delete-note-btn', 'click', function(){
				var id = $('input[name=id]').val();
				if(id == undefined || id <= 0) {
					alert('无法删除');
					return false;
				}
				if(confirm("确定要删除吗!")) {
					$.ajax({
						url : '/note/deleteNote',
						data : {id: id},
						type: 'post',
						dataType : 'JSON',
						success: function(result) {
							if(result.code == 200) {
								window.location.href = '/note';
							} else {
								alert(result.message);
							}
						},
						error: function() {
							alert('删除失败！请联系管理员');
						}
					});
				}
			});
		},
		initUploadCoverImage: function(){
			var id = $('input[name=id]').val();
			//上传封面图片
			$('#fileupload').fileupload({
				url : '/image/uploadify',
				dataType: 'json',
				acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
				maxFileSize: 5000000, // 5 MB
				formData:{object:"note", object_id: id},
				send: function(e, data) {
					$('.poi-cover-image-box img').hide();
					$('.poi-cover-image-box .loading').show();
				},
				done: function(e, data) {
					var result = data.result;
					if(result.code == 200) {
						image_hash = result.data['image_hash'];
						$('input[name=cover_image]').val(image_hash);
						$('.poi-cover-image-box img').attr('src', result.data['image_url_medium']).show();
						$('.poi-cover-image-box .loading').hide();
					} else {
							alert(result.message);
					}
				},
				progressall: function (e, data) {
					var progress = parseInt(data.loaded / data.total * 100, 10);
					$('#progress .progress-bar').css('width',progress + '%');
				},
				fail: function(e, data) {
					$('.poi-cover-image-box .loading').html('上传失败');
				}
			}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
		},

		validate:function() {
			var _self = this;
			$form.validate({
				debug:false,
				submitHandler: function(e) {
					var submitBtn = $('#submit-btn');
					$.ajax({
						url:'/note/saveNote',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function() {
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								if(result.data['id'] > 0) {
									$('input[name=id]').val(result.data['id']); 
								}
								submitBtn.addClass('btn-success').html('保存成功');
								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存').attr('disabled', false);
									if(result.data['new_poi'] == 'ok' && result.data['id'] > 0) {
										window.location.href = '/note/edit?id=' + result.data['id'];
									}
								}, 2000);
							} else {
								submitBtn.addClass('btn-error').html('保存失败，请联系客服');
								alert(result.message);
								setTimeout(function() {
									submitBtn.removeClass('btn-error');
									submitBtn.text('保存').attr('disabled', false);
								}, 3000);
							}
						},
						error: function(e){
							submitBtn.text('保存').attr('disabled', false);
						}
					});
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					city_id:{
						required: true
					},
					title:{
						required: true
					}
				},
				messages:{
					city_id:{
						required:"请选择所在城市"
					},
					title:{
						required:"标题不能为空"
					}
				}
			});
		}
	};
});
