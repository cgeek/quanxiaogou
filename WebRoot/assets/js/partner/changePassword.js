define(function(require, exports, module) {
	var $ = require('jquery');
	require('jquery-validate');
	require('json');

	var $form = $("#changePasswordForm");
	return {
		init:function() {
			var that = this;
			that.validate();

			$form.find(':input[value=""]:first').select();

		},
		showError:function(error) {
		},
		validate:function() {
			var _self = this;
			$form.validate({
				debug:true,
				submitHandler: function(e) {
					var submitBtn = $('#submit-password-btn');
					$.ajax({
						url:'/user/changePassword',
						type: 'POST',
						data: $form.serialize(),
						beforeSend: function(){
							submitBtn.text('正在提交...').attr('disabled', true);
						},
						success: function(result){
							if(result.code == 200) {
								submitBtn.addClass('btn-success').html('修改成功');
								setTimeout(function() {
									submitBtn.removeClass('btn-success');
									submitBtn.text('保存修改').attr('disabled', false);
								}, 2000);
							} else {
								alert(result.message);
								setTimeout(function() {
									submitBtn.removeClass('btn-error');
									submitBtn.text('保存修改').attr('disabled', false);
								}, 3000);
							}
						},
						error: function(e){
							submitBtn.text('保存修改').attr('disabled', false);
						}
					});
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					old_password:{
						required:true
					},
					new_password:{
						required: true
					},
					check_new_password:{
						required: true,
						equalTo: '#inputNewPassword'
					}
				},
				messages:{
					old_password: {
						required:"原密码不能为空"
					},
					new_password:{
						required:"新密码不能为空"
					},
					check_new_password: {
						required:"确认密码不能为空",
						equalTo: '两次输入不一致'
					}
				}
			});
		}
	};
});
