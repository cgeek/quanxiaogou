define(function(require, exports, module) {
	var $ = require('jquery');

	require('jquery-validate');
	var cookie = require('cookie');

	var $form = $(".content form");
	
	return {
		init:function(){
			var _self = this;
			if($form.attr('id') == 'register_form') {
				_self.validate_reg();
			}
			$form.find(':input[value=""]:first').select();
		},
		showError:function(error) {
			//$form.find('.page-account-alert-message').show('fast').addClass('alert alert-error').fadeOut('fast', function() {$(this).html(error)}).fadeIn('fast');
		},
		validate_reg:function(form) {
			var _self = this;
			$form.validate({
				debug:true,
				submitHandler: function(form) {
					form.submit();
				},
				validClass:'success',
				highlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(errorClass).removeClass(validClass);
				},
				unhighlight: function(element, errorClass, validClass) {
					$(element).closest('.clearfix').addClass(validClass).removeClass(errorClass);
				},
				errorPlacement: function(error, element) {
					error.appendTo( element.next("span") );
				},
				errorElement: "span",
				rules:{
					username:{
						required:true,
						minlength: 3,
						maxlength: 16,
						remote: {
							url:'/user.php?act=is_registered&username=' + $('input[name=username]').val(),
							type:"GET"
						}
					},
					email:{
						required:true,
						email: true,
						remote: {
							url : 'user.php?act=check_email&email=' + $('input[name=email]').val(),
							type:"GET"
						}
					},
					captcha: {
						required:true
					},
					password: {
						required:true,
						minlength: 6,
						maxlength: 32
					},
					confirm_password: {
						required:true,
						equalTo: "#password"
					}
				},
				messages:{
					email:{
						required:"请填写邮箱",
						email:"邮箱是不合法的邮箱",
						remote: "该邮箱已经被注册"
					},
					username: {
						required:"请填写用户名",
						remote: "用户名已经存在",
						minlength:$.format("用户名最少 {0} 位"),
						maxlength:$.format("用户名最多 {0} 位")
					},
					captcha: {
						required:"请填写验证码"
					},
					password: {
						required:"请填写密码",
						minlength:$.format("密码最少 {0} 位"),
						maxlength:$.format("用户名最多 {0} 位")
					},
					confirm_password: {
						required: "请确认密码",
						equalTo: "两次输入的密码不一致"
					}
				}
			});
		}
	};
});
