define(function(require, exports, module) {
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache'),
		cookie = require('cookie');
		
	require('json');
	
	var Create = require('create').Create;
	var citylist = require('planCityList').citylist;
	var CityModel = require('planCityList').CityModel;
	citylist.initStep3Page();
	
	Create.SteptwoArgs = Backbone.Model.extend({
		defaults: {
			'type': 1,
			'id': 0,
			'page': 1,
			'nextto': true,
			'name': null
		}
	});
	
	Create.SteptwoView = Backbone.View.extend({
		el: 'body',
		listel: '#_jscitylist',
		middleware: '#_jsmiddleware',
		events: {
			'click .addnewcity': 'addNewCity',
			'click #_jstostep3': 'gotoStep3',
			'click #_jstostep1': 'gotoStep1'
		},
		initialize: function (data) {
			this.wordcountry = "%s的热门城市";
			this.wordcity = "去过%s的还会去";
			_.bindAll(this, 'render', 'getCity', 'loadCity', 'changeWord', 'addNewCity', 'gotoStep1', 'gotoStep3');
			this.model = new Create.SteptwoArgs();
			this.model.on("change:id", this.loadCity);
			this.model.on("change:name", this.changeWord);
			if (data) this.setModel(data);
				this.render();
		},
		render: function () {
			var self = this;

			//滚动翻页
			Create.scroll(60, function (callback) {
				var page = self.model.get('page');
				if (page <= 1) self.model.set('page', 2);
					self.getCity(callback);
			});
			
			$(".pl_found_mask").fadeIn(2000);
		},
		changeWord: function () {
			var word;
			if (this.model.get('type') == 2) {
				word = this.wordcity.replace("%s", this.model.get('name'));
			} else {
				word = this.wordcountry.replace("%s", this.model.get('name'));
			}
			$("#_jswordtitle").html(word);
		},
		gotoStep1: function (e) {
			var url = "/trip/create?step=1";
			window.location.href = url;
		},
		gotoStep3: function (e) {
			var that = e.currentTarget;
			if ($(that).hasClass('button_disabled')) {
				return false;
			}
			var url = "/trip/create?step=3";
			var uri = Create.geturi();

            uri = uri && uri.replace('step=2', '');

			if (uri) {
				url += '&' + uri;
			}
			window.location.href = url;
		},
		loadCity: function () {
			this.getCity();
		},
		setModel: function (data) {
			this.model.set(data);
		},
		getCity: function (callback) {
			var self = this;
			$("#_jscityloading").show();
			if (!self.model.get('nextto')) {
				$("#_jscityloading").hide();
				return;
			}
			var type = self.model.get('type');
			var id = self.model.get('id');
			var page = self.model.get('page')
			$.get("/api/getCityList", {
				type: type,
				status: 1,
				country_id: id,
				page: page
			}, function (result) {
				if (result && result.code == 200) {
					self.getCityed(result);
					if (callback) callback();
				}
			$("#_jscityloading").hide();
			})
		},
		getCityed: function (result) {
			if (result.data && result.data.city_list && result.data.city_list.length > 0) {
				var page = this.model.get('page');

				var tpl = $('#city-list-tpl').html();
				var html = Mustache.to_html(tpl, result.data);
				if (page > 1) {
					$(this.listel).append(html);
				} else {
					$(this.listel).html(html);
				}
				this.model.set({
					page: page + 1,
					nextto: true
				});
			} else {
				this.model.set('nextto', false);
			}
		},
		citymaskchange: function()
		{
			var cityh = $(".pl_found_mask .list_two_hang");
			if(cityh.height()>30){
				cityh.css({"padding":"0"});
			}
		},
		checkNextButton: function () {
			if (!citylist.checkList()) {
				$("#_jstostep3").addClass('button_disabled');
			} else {
				$("#_jstostep3").removeClass("button_disabled");
			}
		},
		addCity: function (data) {
			if (citylist.length >= 30) {
				alert("最多可添加30个城市！");
				return;
			}
			citylist.push(new CityModel(data));
			this.citymaskchange();
		},
		addNewCity: function (e) {
			if (citylist.length >= 30) {
				alert("最多可添加30个城市！");
				return;
			}
			$("._jsnochoosetags").hide();
			$("._jschoosetags").show();
			var that = e.currentTarget;
			var cityname = $(that).attr('data-cityname');
			var cityid = $(that).attr('data-cityid');
			var curlist = $(that).parents('div .list');
			var html = "<div class='list'>" + curlist.html() + "</div>";
			var city_list = $(".list_two_hang").find('.list_tag:last');
			city_list = city_list[0] ? city_list : $(".list_two_hang");
			var priwidth = $(this.middleware).width();
			var that = this;
			
			$(this.middleware).html(html).show().css({
				top: curlist.offset().top,
				left: curlist.offset().left
			}).animate({
				top: city_list.offset().top,
				left: city_list.offset().left,
				width: 0,
				height: 0
			}, 500, function () {
				
				//添加到cityList
				that.addCity({
					days: 1,
					city_id: cityid,
					city_name: cityname,
					type: 1
				});
				
				$(that.middleware).hide().css({
					top: 0,
					left: 0,
					width: priwidth,
					height: 'auto'
				});
			});
		}
	});

	return {
		init : function(){
			new Create.SteptwoView(window.countryInfo);
		}
	}


});
