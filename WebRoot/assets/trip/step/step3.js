define(function(require, exports, module) {
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache'),
		cookie = require('cookie');
		
	require('json');
	require('jquery-ui-sortable');
	require('jquery-ui-datepicker');
	require('gmap');
	
	var autoThink = require('autoThink').autoThink;
	
	var Create = require('create').Create;
	var citylist = require('planCityList').citylist;
	
	return {
		init : function(){
			this.initViewAction();
			this.initAutoThink();
		},
		initViewAction : function() {
			//添加新城市
			$(document).delegate("#jsplans_addnewcity", "click", function(){
				$("#jsplans_addnewcity").hide();
				$("#jsplans_searchaddcity").show().focus();
			});
			//进到下一步
			$(document).delegate("#jsplan_step3_gonext", "click", function(){
				if($(this).hasClass("action_button_disabled")){
					return;
				}
				var days = citylist.getCurListTotalDays();
				if(days>50){
					$("#jsplan_step3_gonext").addClass("action_button_disabled");
					qyerUI.message({msg:"行程最多支持50天", type:"warn"});
					return ;
				}
				if(citylist.length>30){
					$("#jsplan_step3_gonext").addClass("action_button_disabled");
					qyerUI.message({msg:"最多可添加30个城市", type:"warn"});
					return ;
				}
                var uri = Create.geturi();
                uri = uri && uri.replace('step=3', '');
				location.href = "/trip/create?step=4&"+uri;
			});
			
			$("#datepicker").datepicker({
				changeYear: true, dateFormat: 'yy-mm-dd'
			});
			$("#datepicker").on("change", function(){
				var v = $("#datepicker").val();
				citylist.setTriptime(v);
			});
			
			
			$("#jsplan_citylist_step3").sortable({
				start:function(event, ui){
				},
				stop:function(event, ui){
				},
				update:function(){
					citylist.updateSort();
				}
			});
			
			$("#gmap").gmap3({
				map:{
					options:{
						center:[18.50, 98.50],
						zoom: 6
					}
				}
			});
			
		},
		initAutoThink : function() {
			var searchLeaveCity = function(val, type, dataid){
				$("#plans_step3_startcity").val(val);
				console.log(val);
				citylist.setStartCity(val, dataid);
				var endval = $("#plans_step3_endcity").val();
				endval = $.trim(endval);
				if(!endval){
					$("#plans_step3_endcity").val(val);
					citylist.setEndCity(val, dataid);
				}
			};
			var startcity = new autoThink({
				inputel:"#plans_step3_startcity",
				listel:"#plans_step3_startcity_list",
				itemel:"#plans_step3_startcity_list a",
				autoClearInput:false,
				//searchCountry:0,
				//posturl:"http://plan.qyer.com/planapi.php?action=selectcity",
				posturl:"/api/searchCity",
				entercb: function(arg)
				{
					var obj = arg.obj;
					var size = $(obj).size();
					
					if(size>0){
						var val = $(obj).attr("data-title");
						var type = $(obj).attr("data-type");
						var dataid = $(obj).attr("data-dataid");
					} else {
						var val = arg.val;
						var type = "";
						var dataid = "";
					}
					searchLeaveCity(val, type, dataid);
				},
				postdata: {
					isstep3:1
				},
				clickcb: function(e){
					console.log(e);
					var obj = $(e.currentTarget);
					var val = obj.attr("data-title");
					var type = obj.attr("data-type");
					var dataid = obj.attr("data-dataid");

					console.log(val);
					console.log(type);
					console.log(dataid);
					searchLeaveCity(val, type, dataid);
				}
			});
			
			var searchCityEnter = function(obj){
				//var type = $(obj).attr("data-route-type");
				var dataid = $(obj).attr("data-id");
				var title = $(obj).attr("data-name");
				var fundays = $(obj).attr("data-fundays");
				fundays = parseInt(fundays);
				var iscanadd = citylist.isCanAddCity(true);
				if(iscanadd){
					citylist.add({days:fundays, name:title, dataid:dataid, xsort:0, type:1}); 
				}
				$("#jsplans_addnewcity").show();
				$("#jsplans_searchaddcity").hide();
			};

			var searchEndCity = function(val, type, dataid){
				$("#plans_step3_endcity").val(val);
				citylist.setEndCity(val, dataid);
			};
			
			//添加城市
			var addSearchCity = new autoThink({
				inputel:"#jsplans_searchaddcity",
				posturl:"/api/searchCity",
				entercb: function(arg){
					var size = $(arg.obj).size();
					if(size<=0) return;
					searchCityEnter(arg.obj);
				},
				postdata: {
					source:1,
					searchCountry:0
				},
				clickcb: function(e){
					var obj = e.currentTarget;
					searchCityEnter(obj);
				}
			});
			
			var endcity = new autoThink({
					inputel:"#plans_step3_endcity",
					listel:"#plans_step3_endcity_list",
					itemel:"#plans_step3_endcity_list a",
					autoClearInput:false,
					searchCountry:0,
					posturl:"/api/searchCity",
					entercb: function(arg) {
						var obj = arg.obj;
						var size = $(obj).size();
			
						if(size>0){
							var val = $(obj).attr("data-title");
							var type = $(obj).attr("data-type");
							var dataid = $(obj).attr("data-dataid");
						}else{
							var val = arg.val;
							var type = "";
							var dataid = "";
						}
						searchEndCity(val, type, dataid);
					},
					postdata: {
						isstep3:1
					},
					clickcb: function(e){
						var obj = $(e.currentTarget);
						var val = obj.attr("data-title");
						var type = obj.attr("data-type");
						var dataid = obj.attr("data-dataid");
						searchEndCity(val, type, dataid);
					}
				});
			
			
			
			citylist.isStep3 = function(){return true;};
			citylist.initStep3Page();
		}
	}
	
});
