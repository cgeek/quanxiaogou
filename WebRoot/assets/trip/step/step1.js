define(function(require, exports, module) {
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');
		
	var Create = require('create').Create;
	Create.SteponeArgs = Backbone.Model.extend({
		defaults: {
			'continentid': 0,
			'page': 1,
			'nextto': true
		}
	});
	Create.Stepone = Backbone.View.extend({
		el: 'body',
		events: {
			'click ._jschgcontinent': 'changeContinent',
			'click ._jsgotostep2': 'chooseCountry',
			'click #_jstostep3': 'gotoStep3'
		},
		listel: '#_jscountrylist',
		initialize: function () {
			_.bindAll(this, 'changeContinent', 'chooseCountry');
			this.model = new Create.SteponeArgs();
			this.model.on('change:continentid', this.loadCountry);
			this.render();
		},
		render: function () {
			var self = this;
			this.renderCountry();
			
			Create.scroll(60, function (callback) {
				var page = self.model.get('page');
				if (page <= 1) self.model.set('page', 2);
				self.getCountry(self, callback);
			});
		},
		chooseCountry: function (e) {
			this.gotoStep2(e.currentTarget);
		},
		renderCountry: function () {
			this.getCountry(this);
		},
		loadCountry: function () {
			var self = Create.stepone;
			self.getCountry(self);
		},
		getCountry: function (self, callback) {
			$("#_jscountryloading").show();
			if (!self.model.get('nextto')) {
				$("#_jscountryloading").hide();
				return;
			}
			var continentid = self.model.get('continentid');
			var page = self.model.get('page');
			$.get("/api/getCountryList", {
				'continentid': continentid,
				'page': page
			}, function (result) {
				if (result && result.code == 200) {
					self.getCountryed(result);
					if (callback) callback();
				}
				$("#_jscountryloading").hide();
			});
		},
		getCountryed: function (result) {
			if (result.data) {
				var page = this.model.get('page');

				var tpl = $('#country-list-tpl').html();
				var html = Mustache.to_html(tpl, result.data);
				if (page > 1) {
					$(this.listel).append(html);
				} else {
					$(this.listel).html(html);
				}
				this.model.set({
					page: page + 1,
					nextto: true
				});
			} else {
				this.model.set('nextto', false);
			}
		},
		gotoStep2: function (thiz) {
			var type = parseInt($(thiz).attr('data-type'));
			var id = $(thiz).attr('data-id');
			var url = "/trip/create/?step=2";
			if (type == 1) {
				url += "&country_id=" + id
			} else if (type == 2) {
				url += "&city_id=" + id;
			}
			if (!id) {
				return false;
			}
				setTimeout(function () {
					window.location.href = url;
				}, 50);
		},
		gotoStep3: function (e) {
			var thiz = e.currentTarget;
			if ($(thiz).hasClass('button_disabled')) {
				return false;
			}
			var url = "/trip/create?step=3";
				var uri = Create.geturi();
			if (uri) {
				url += '?' + uri;
			}
			window.location.href = url;
		},
		changeContinent: function (e) {
			var that = e.currentTarget;
			var id = parseInt($(that).attr('data-id'));
			if (isNaN(id)) id = 0;
			this.model.set({
				continentid: id,
				page: 1,
				nextto: true
			});
			Create.openScroll();
			$(that).parents('div').find('a').addClass('_jschgcontinent').removeClass('current');
			$(that).addClass('current').removeClass('_jschgcontinent');
		}
	});

	return {
		init : function(){
			new Create.Stepone();
			
			//TODO 城市搜索自动提示
		}
	}


});
