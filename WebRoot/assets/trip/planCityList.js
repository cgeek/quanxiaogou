define(function(require, exports, module) {
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		cookie = require('cookie');
	
	var tripStoreCookie = require('tripStoreCookie').tripStoreCookie;
	
	var CityModel, CityList, Step2CityView, Step3CityView, getcityinfos;
	
	getcityinfos = function (ids, callback) {
		var data, url;
		url = "/api/getCityInfos";
		data = {
			ids: ids
		};
		return $.post(url, data, function (strjson) {
			var json;
			json = strjson;
			return typeof callback === "function" ? callback(json) : void 0;
		});
	};
	
	CityModel = Backbone.Model.extend({
		defaults: {
			days: 1,
			city_name: "",
			city_id: 0,
			xsort: 0,
			type: 1
		},
		addDays: function (day, ok_callback, error_callback) {
			var days, objview;
			days = day + this.get("days");
			this.set({
				days: days
			});
			objview = this.get("objview");
			if (objview != null) {
				objview.render(false);
			}
			return typeof ok_callback === "function" ? ok_callback() : void 0;
		},
		getDataid: function () {
			return this.get("type") + "|" + this.get("city_id") + "|" + this.get("days");
		},
		info2JSON: function () {
			var k, ret, _i, _len, _ref;
			ret = {};
			_ref = ["days", "city_name", "city_id", "type"];
			for (_i = 0, _len = _ref.length; _i < _len; _i++) {
				k = _ref[_i];
				ret[k] = this.get(k);
			}
			return ret;
		}
	});
	
	
	CityList = Backbone.Collection.extend({
		model: CityModel,
		initialize: function () {
			var citynum, countrynum;
			_.bindAll(this, "addone", "refreshdata", "renderall","setStartCity", "setEndCity");
			this.bind("add", this.addone);
			this.bind("remove", this.removeone);
			this.bind("remove", this.checkCanNext);
			this.bind("add", this.checkCanNext);
			countrynum = 0;
			citynum = 0;
			return this.store = new tripStoreCookie();
		},
		setStartCity: function (title, cityid) {
			console.log(title);
			console.log(this.store);
			return this.store.saveStartCity(title, cityid);
		},
		setEndCity: function (title, cityid) {
			return this.store.saveEndCity(title, cityid);
		},
		setTriptime: function (time) {
			return this.store.saveTriptime(time);
		},
		//更新cookie的info
		refreshdata: function () {
			var json;
			json = this.collection2JSON();
			return this.store.saveCity(json);
		},
		//将collection里的model列表转化成json
		collection2JSON: function () {
			var v;
			v = [];
			this.each(function (model) {
				return v.push(model.info2JSON());
			});
			return v;
		},
		isStep3: function () {
			return false;
		},
		//在step2和step3里，添加后的view不一样
		getObjview: function (citymodel) {
			var cityView;
			if (this.isStep3()) {
				cityView = new Step3CityView({
					model: citymodel
				});
			} else {
				cityView = new Step2CityView({
					model: citymodel
				});
			}
			citymodel.set({
				objview: cityView
			});
			return cityView;
		},
		addone: function (citymodel, opts, usropt) {
			var cityView, updateMap, that = this;
			citymodel.on("change", function () {
				return that.refreshdata();
			});
			cityView = this.getObjview(citymodel);
			//this.refreshdata();
			cityView.render();
			updateMap = !(usropt != null ? usropt.not_updatemap : void 0);
			this.renderAllView(updateMap);
			//检查是否
			return this.checkCanNext();
		},
		removeone: function () {
			this.refreshdata();
			return this.renderAllView(true);
		},
		comparator: function (a, b) {
			var v1, v2;
			v1 = a.get("xsort");
			v2 = b.get("xsort");
			if (v1 > v2) {
				return -1;
			}
			if (v1 === v2) {
				return 0;
			}
			return 1;
		},
		checkCanNext: function () {
			var curdays;
			if (this.length <= 0 || this.length > 30) {
				$("._jschoosetags").hide();
				$("._jsnochoosetags").show();
				$("#_jstostep3").addClass('button_disabled');
			} else {
				$("._jschoosetags").show();
				$("._jsnochoosetags").hide();
				$("#_jstostep3").removeClass("button_disabled");
			}
			curdays = this.getCurListTotalDays();
			if ((curdays <= 50 && curdays >= 1) || this.length > 30) {
				return $("#jsplan_step3_gonext").removeClass("action_button_disabled");
			} else {
				return $("#jsplan_step3_gonext").addClass("action_button_disabled");
			}
		},
		getDataids: function () {
			var v;
			v = [];
			this.each(function (m) {
				return v.push(m.getDataid());
			});
			return v;
		},
		getUnknownCityCount: function () {
			var c;
			c = 0;
			this.each(function (m) {
				var cityid;
				cityid = m.get("dataid");
				if (cityid <= 0 || cityid === '0' || cityid === '') {
					return c++;
				}
			});
			return c;
		},
		getCityCount: function () {
			var c;
			c = 0;
			this.each(function (m) {
				var type;
				type = m.get("type");
				if (type === 1) {
					return c++;
				}
			});
			return c;
		},
		getCountryCount: function () {
			var c;
			c = 0;
			this.each(function (m) {
				var type;
				type = m.get("type");
				if (type === 2) {
					return c++;
				}
			});
			return c;
		},
		getCityIDS: function () {
			var k, u, v, _results;
			u = {};
			this.each(function (m) {
				var cityid;
				cityid = m.get("city_id");
				if (cityid > 0 && cityid !== '0' && m.get("type") === 1) {
					return u[cityid] = 1;
				}
			});
			_results = [];
			for (k in u) {
				v = u[k];
				_results.push(k);
			}
			return _results;
		},
		updateDays: function (at, day, okcb, errcb) {
			var model, msg;
			if (at == null) {
				at = 0;
			}
			if (day == null) {
				day = 1;
			}
			model = this.at(at);
			if (!model) {
				return false;
			}
			if (day >= 20) {
				return false;
			}
			if (!this.isCanAddDay(day)) {
				if (typeof errcb === "function") {
					errcb();
				}
				alert("行程最多支持50天");
				return;
			}
			model.addDays(day, okcb, errcb);
			return this.updateDaysTips();
		},
		getCurListTotalDays: function () {
			var day;
			day = 0;
			this.each(function (m) {
				return day += m.get("days");
			});
			return day;
		},
		isCanAddCity: function (isalert) {
			var msg, size;
			size = this.size();
			if (size >= 30) {
				if (isalert) {
					alert("行程最多支持30个城市");
				}
				return false;
			}
			return true;
		},
		isCanAddDay: function (day) {
			var curday;
			curday = this.getCurListTotalDays();
			return curday + day <= 50;
		},
		updateMap: function () {
			var ids;
			ids = this.getDataids();
			if (!this.isStep3()) {
				return;
			}
			//return plans_initMap(ids, true);
		},
		updateSort: function () {
			var list, mythiz, size, _this = this;
			list = $("#jsplan_citylist_step3 .list");
			size = $(list).size();
			mythiz = this;
			$.each(list, function (k, v) {
				var cid, m;
				cid = $(v).attr("data-cid");
				m = mythiz.get(cid);
				return m.set("xsort", size - k, {
					silent: true
				});
			});
			this.sort();
			this.refreshdata();
			return this.updateMap();
		},
		recomputecount: function () {
			var list;
			list = this.qyer2JSON();
			this.countrynum = 0;
			this;
			return $.each(list);
		},
		updateDaysTips: function () {
			var citynum, country, days, tpl;
			this.countrynum = this.getCountryCount();
			this.citynum = this.getCityCount();
			country = this.countrynum || 0;
			citynum = this.citynum + this.getUnknownCityCount();
			if (isNaN(citynum) || typeof citynum !== 'number') {
				citynum = 0;
			}
			days = this.getCurListTotalDays();
			tpl = "总计：" + days + " 天，" + country + "个国家，" + citynum + "个城市";
			return $("#jsplan_daystips").html(tpl);
		},
		renderall: function () {
			var list, m, _i, _len, _results;
			list = this.store.getCity();
			if ((list != null ? list.length : void 0) <= 0) {
				return false;
			}
			_results = [];
			for (_i = 0, _len = list.length; _i < _len; _i++) {
				m = list[_i];
				_results.push(this.push(new CityModel(m), {
					not_updatemap: true
				}));
			}
			return _results;
		},
		renderAllView: function (isupdatemap) {
			var i, size;
			size = this.length;
			i = 0;
			this.each(function (m) {
				i++;
				return m.get("objview").render(false, i === size);
			});
			if (isupdatemap) {
				return this.updateMap(isupdatemap);
			}
		},
		initStep3Page: function () {
			var cityids, endcid, endinfo, startcid, startinfo, triptime, _this = this;
			this.renderall();
			startcid = '#plans_step3_startcity';
			if ($(startcid).size() > 0) {
				startinfo = this.store.getStartCity();
				if (startinfo != null ? startinfo.n : void 0) {
					$(startcid).val(startinfo.n);
				}
			}
			endcid = '#plans_step3_endcity';
			if ($(endcid).size() > 0) {
				endinfo = this.store.getEndCity();
				if (endinfo != null ? endinfo.n : void 0) {
					$(endcid).val(endinfo.n);
				}
			}
			cityids = this.getCityIDS();
			this.checkCanNext();
			if ((cityids != null ? cityids.length : void 0) <= 0) {
				return false;
			}
			triptime = this.store.getTriptime();
			$("#datepicker").val(triptime);
			
			return getcityinfos(cityids, function (json) {
				var cityinfo, days, i, size;
				cityinfo = json.data.citys;
				size = _this.length;
				i = 0;
				_this.countrynum = json.data.countrys.length;
				_this.citynum = cityids.length;
				_this.each(function (m) {
					var dd, info, tmpcid, _ref;
					tmpcid = m.get("dataid");
					info = cityinfo["cid" + tmpcid];
					if (info) {
						dd = info.fundays;
						if (dd <= 0) {
							dd = 1;
						}
						m.set({
							"city_name": info.cityname,
							days: dd
						});
					} else {
						m.set({
							days: 1
						});
					}
					i++;
					if ((_ref = m.get("objview")) != null) {
						_ref.render(false, i === size);
					}
					return _this.updateDaysTips();
				});
				days = _this.getCurListTotalDays();
				if (days > 50 || size > 30) {
					alert("行程最多支持50天");
				}
			});
			
		}
	});
	Step2CityView = Backbone.View.extend({
		tagName: "span",
		className: "list_tag",
		initialize: function () {},
		events: {
			"click .del": "click_del"
		},
		render: function (isappend, islast) {
			var json, tpl;
			if (isappend == null) {
				isappend = true;
			}
			json = this.model.toJSON();
			tpl = "<span class=\"list_tag\">\n <a href=\"javascript:;\">" + json.city_name + "</a>\n <span class=\"del\" title=\"删除\"></span>\n</span>";
			if (!islast) {
				tpl += '<span class="list_jt">箭头</span>';
			}
			$(this.el).html(tpl);
			$(this.el).find(".del").attr("data-bn-ipg", "plancreate-bottom-delete");
			if (isappend) {
				return $(this.el).appendTo($(".list_two_hang"));
			}
		},
		click_del: function () {
			this.remove();
			return citylist.remove(this.model);
		}
	});
	
	Step3CityView = Backbone.View.extend({
		tagName: "div",
		className: "list",
		initialize: function () {},
		events: {
			"click .reduce": "reduce_num",
			"click .add": "add_num",
			"click .close": "rm_element"
		},
		render: function (isappend) {
			var cid, curdays, days, json, tpl;
			if (isappend == null) {
				isappend = true;
			}
			cid = this.model.cid;
			json = this.model.toJSON();
			days = this.model.get("days") || 1;
			tpl = "<strong>" + json.city_name + "</strong>\n<div class=\"select\">\n <span class=\"reduce\" title=\"减\" data-bn-ipg=\"plancreate-bottom-delete\"></span>\n <span class=\"number\">" + json.days + "天</span>\n <span class=\"add\" title=\"加\"></span>\n</div>\n<span class=\"close\" title=\"删除城市\"></span>";
			$(this.el).html(tpl);
			if (isappend) {
				$(this.el).appendTo($("#jsplan_citylist_step3"));
			}
			$(this.el).attr("data-cid", cid);
			$(this.el).attr("title", "可以拖拽排序");
			if (days <= 0.5) {
				$(this.el).find(".reduce").addClass("reduce_disabled");
			}
			if (days >= 20) {
				$(this.el).find(".add").addClass("add_disabled");
			}
			citylist.updateDaysTips();
			curdays = citylist.getCurListTotalDays();
			if (curdays <= 50) {
				return $("#jsplan_step3_gonext").removeClass("action_button_disabled");
			} else {
				return $("#jsplan_step3_gonext").addClass("action_button_disabled");
			}
		},
		reduce_num: function () {
			var days, num;
			num = -0.5;
			days = this.model.get("days");
			if (days === 0.5) {
				return false;
			}
			if (days > 1) {
				num = -1;
			}
			this.model.addDays(num);
			return this.render(false);
		},
		add_num: function () {
			var curdays, days, num;
			num = 0.5;
			days = this.model.get("days");
			if (days >= 20) {
				return false;
			}
			if (days >= 1) {
				num = 1;
			}
			curdays = citylist.getCurListTotalDays();
			if (curdays + num > 50) {
				qyerUI.message({
					msg: "行程最多支持50天",
					type: "warn"
				});
				return false;
			}
			this.model.addDays(num);
			return this.render(false);
		},
		rm_element: function () {
			citylist.remove(this.model);
			this.remove();
			return this.render(false);
		}
	});
	
	
	exports.citylist = window.citylist = new CityList();
	exports.CityModel = CityModel;
});

