define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');
	
	var OnedayEvent = require('../modules/oneday_event.js?v=20123');
	var FormViews = require('../views/form_views.js?v=2013');
	var Traffic = require('../modules/traffic.js?v=20131231');
	
	//autocomplete
	var AutoThink = require('autoThink').autoThink;
	
	exports.Views = {};
	//中间区域对每一天计划的操作
	exports.Views.XcdCntView = Backbone.View.extend({
		el: ".pl_nday_main",
        searchcityAuto : false,
		initialize: function () {
			var that = this;
			this.rendertimer = 0;
			this.curoneday = null;
			this.initPoiForm();

		},
		events: {
			"click #js_plan_add_hotplace_city a" : "add_city",
			"click #jsplan_go_prev": "jsplan_go_prev",
			"click #jsplan_go_next": "jsplan_go_next",
			"click #js_plan_addcity_btn": "add_more_city",
			"click .add_hotel_btn": "addHotel",
            "click .add_poi_btn": "addPoi",
            "click .add_desc_btn": "addDesc",
            "blur .js_trip_info_update": "blur_update",
			"click #_js_set_demo_trip": "setDemoTrip"
			//"click #_js_upload_trip_day_image" : "uploadTripDayImage",
			//"click #js_remove_trip_day_image" : "removeTripDayImage"
		},
        addDesc: function() {
            var form = this.getDayDescForm();
            form.render();
        },
		addHotel: function(event){
			var form = this.getHotelForm();
			var isedit = false;
			form.render(isedit);
		},
        addPoi: function(event) {
            var form = this.getPoiForm();
            form.showAddForm();
        },
		setDemoTrip: function(e) {
			var that = this;
			var city = null;
			var city_list = this.curoneday.get("city_list");
			if(city_list.length > 0) {
				city = city_list.at(city_list.length - 1);
			}
			if(city == null) {
				alert('请先添加城市');
				return false;
			}

            var trip_day_id = this.curoneday.get('xid');
			
			var button = $('#_js_set_demo_trip');
			button.prop('disabled', true);

			$.post("/api/setDemoTripDay", {trip_day_id:trip_day_id}, function(result){
				if(result.code == 200) {
					button.find('i').removeClass('fa-star-o').addClass('fa-star');
				} else {
					button.prop('disabled', false).html('保存失败');
				}
			});
		},
		uploadTripDayImage: function(e) {
			var that = this;
			var curoneday = this.curoneday;
			var trip_day_id = curoneday.get('xid');
			if(trip_day_id == undefined || trip_day_id <= 0) {
				alert('上传初始化失败！');
				return false;
			}
			//上传贴士图片
			$('#tripDayImageFile').fileupload({
				url : '/admin/image/uploadify',
				dataType: 'json',
				acceptFileTypes: /(\.|\/)(gif|jpe?g|png)$/i,
				maxFileSize: 5000000, // 5 MB
				formData:{object:"trip_day_image", object_id: trip_day_id},
				send: function(e, data) {
					$('.set_demo_trip_help').html('正在上传...').show();
				},
				done: function(e, data) {
					var result = data.result;
					if(result.code == 200) {
						image_hash = result.data['image_hash'];
						
						var images = image_hash + ',';
						$('.trip_day_image_box ul li').each(function() {
							console.log($(this).attr('image_hash'));
							var hash = $(this).attr('image_hash');
							if(hash != undefined) {
								images += hash + ',';
							}
						});
						
						that.curoneday.get("objectView").updateDayInfo('image', images);
						$('.trip_day_image_box ul').prepend('<li image_hash="'+ image_hash + '"><img src="' + result.data['image_url'] + '"><span id="js_remove_trip_day_image">x</span></li>');
						$('.set_demo_trip_help').html('上传成功！').hide();
						
					} else {
						$('.set_demo_trip_help').html('上传失败！请重试').show();
					}
				},
				progressall: function (e, data) {
					//var progress = parseInt(data.loaded / data.total * 100, 10);
					//$('#progress .progress-bar').css('width',progress + '%');
				},
				fail: function(e, data) {
					$('.set_demo_trip_help').html('上传失败！请重试').show();
				}
			}).prop('disabled', !$.support.fileInput).parent().addClass($.support.fileInput ? undefined : 'disabled');
			
		},
		removeTripDayImage: function(e){
			
			$(e.target).parents('li').remove();
			var images = '';
			$('.trip_day_image_box ul li').each(function(){
				console.log($(this).attr('image_hash'));
				var image_hash = $(this).attr('image_hash');
				if(image_hash != undefined) {
					images += image_hash + ',';
				}
			});
			
			this.curoneday.get("objectView").updateDayInfo('image', images);

		},
		
		blur_update: function(e){
			var that = this;
            var value = $(e.target).val();
			var key = $(e.target).attr('name');
            $(e.target).removeClass("textarea");
            that.curoneday.get("objectView").updateDayInfo(key, value);
		},
		render: function(args) {
			var that = this;
			if(that.rendertimer > 0) {
				clearTimeout(that.rendertimer);
			}
			var delayExec = function() {
				var onedaymodel = args.oneday;
				that.curoneday = onedaymodel;
				var onedayView = onedaymodel.get("objectView");

				that.renderCitylist({
					'type' : 'init'
				});

				onedayView.fetchall_event(function () {});
				//refreshXCDTitle
				that.refreshXCDTitle(onedaymodel);
				$("#js_plan_nday").show();
				$("#js_plan_prepare").hide();
				var alldayView = window.app.colA.getView("xcd");
				var size = alldayView.allday.size();
				var curidx = alldayView.__getIndexByCid(onedaymodel);
				if (curidx > 0) {
					$("#jsplan_go_prev").removeClass("n_prev_disabled");
				} else {
					$("#jsplan_go_prev").addClass("n_prev_disabled");
				}
				if (curidx < (size - 1)) {
					$("#jsplan_go_next").removeClass("n_next_disabled");
				} else {
					$("#jsplan_go_next").addClass("n_next_disabled");
				}

				//add traffic list
				window.app.getAlldayView().onedayTraffic.addTrafficList();
				//trafficView.render();
				//onedayView.fet
				
			    $("#js_plan_taglist").sortable({
			        start: function (event, ui) {},
			        stop: function (event, ui) {},
			        update: function () {
			            var oneday = window.app.getCurrentOnedayModel();
			            var view = oneday.get("objectView");
			            view.resetDragOrderForCitylist();
			            view.fixXCDCityInputPosition();
			        }
			    });
			};
			that.rendertimer = setTimeout(delayExec, 250);

		},
		renderCitylist: function(options) {

			var onedaymodel = this.curoneday;
			var onedayView = onedaymodel.get("objectView");

			var taglistdiv = $(this.$el).find("#js_plan_addtag");
			var cbfunc = function () {};
			//如果是第一次render城市ids，执行callback函数，设置素材列表默认城市id
			if(options != undefined && options.type == 'init') {
				cbfunc = function() {
					var city_list = onedaymodel.get("city_list");

					if (!city_list) return;
					var size = _(city_list).size();
					if (size <= 0) return;
					var citylist = city_list.collection2JSON();
					if (!citylist) return;
					$.each(citylist, function (k, v) {
						if (v.city_id > 0) {
							window.app.poiListView.setDefaultCity({
								city_id: v.city_id,
								city_name: v.city_name
							});

                            window.app.demoListView.setDefaultCity({
                                city_id: v.city_id,
                                city_name: v.city_name
                            });
							return false;
						}
					});

                    window.app.poiListView.renderCityList(citylist);
				}
			}
			onedayView.renderall_citylist(taglistdiv, options, cbfunc);

		},
		refreshXCDTitle: function(model) {
			var title = $(this.$el).find("#xcd-oneday-title");
			var starttime = $.trim(window.app.tripInfo["starttime"]);
			var idx = window.app.colA.getView("xcd").__getIndexByCid(model);
			var strtitle = "";
			var strdate = "";
			if ("" != starttime) {
				var arrdate = starttime.split(/-/);
				var sDate = new Date();
				var y = parseInt(arrdate[0].replace(/^0+(\d+)/, "$1")) - 0;
				var m = parseInt(arrdate[1].replace(/^0+(\d+)/, "$1")) - 1;
				var d = parseInt(arrdate[2].replace(/^0+(\d+)/, "$1")) - 0;
				sDate.setFullYear(y, m, d);
				if (idx > 0) {
					sDate.setDate(d + idx);
				}
				strdate = (sDate.getMonth() + 1) + "月" + sDate.getDate() + "日";
				var week = sDate.getDay();
				var weekcn = {
					0: '日',
					1: "一",
					2: "二",
					3: "三",
					4: "四",
					5: "五",
					6: "六"
				};
				strdate = "（" + strdate + " 星期" + weekcn[week] + "） ";
			}
			var n = idx + 1;
			strtitle = "第<em>" + n + "</em>天";
			$(title).find(".day").html(strtitle);
			$(title).find(".date").html(strdate);
		},

		jsplan_go_prev: function() {
			var idx = this.__getCurOnedayIDX();
			if (idx <= 0) return;
				idx--;
			if (idx <= 0) idx = 0;
				var alldayview = window.app.colA.getView("xcd");
			alldayview.allday.at(idx).get("objectView").switch2modeledit();
		},
		jsplan_go_next: function () {
			var idx = this.__getCurOnedayIDX();
			var alldayview = window.app.colA.getView("xcd");
			var size = alldayview.allday.size();
			if (idx >= (size - 1)) return;
				idx++;
			if (idx >= (size - 1)) {
				idx = size - 1;
			}
			alldayview.allday.at(idx).get("objectView").switch2modeledit();
		},
		__getCurOnedayIDX: function() {
			var curoneday = this.curoneday;
			var alldayview = window.app.colA.getView("xcd");
			var size = alldayview.allday.size();
			return alldayview.__getIndexByCid(curoneday);
		},
		add_city : function(e) {
			var elem = $(e.target);
			var data = {
				city_name : elem.attr('data-cityname'),
				city_id : elem.attr('data-cityid')
			};
			$("#js_plan_addcity_list").hide();

			this.curoneday.get("objectView").add_city(data);
			this.renderCityList();

            window.app.poiListView.setDefaultCity({
                city_id: data.city_id,
                city_name: data.city_name
            });

        },
		add_more_city: function(e){
			var that = this;
			var taglist = this.curoneday.get("city_list");
			
			var layer = $("#js_plan_addtag .csearch_list");
            var btn = $(e.target);
            var btnL = btn.offset().left;
            var mainL = $(".pl_nday_main").offset().left;
            var chaL = btnL - mainL;
            var mainW = $(".pl_nday_main").width();
            var chaR = mainW - chaL;
            if (chaL < 200) {
                if (chaR > 240) {
                    layer.css({
                        "left": "-10px",
                        "right": ""
                    });
                    layer.find(".arrowbig").css({
                        "left": "38px",
                        "right": ""
                    });
                    layer.find(".arrow").css({
                        "left": "39px",
                        "right": ""
                    });
                } else {
                    var left = -(chaL - 10);
                    layer.css({
                        "left": left,
                        "right": ""
                    });
                    layer.find(".arrowbig").css({
                        "left": (-left + 32),
                        "right": ""
                    });
                    layer.find(".arrow").css({
                        "left": (-left + 33),
                        "right": ""
                    });
                }
            } else {
                layer.css({
                    "left": "",
                    "right": ""
                });
                layer.find(".arrowbig").css({
                    "left": "",
                    "right": ""
                });
                layer.find(".arrow").css({
                    "left": "",
                    "right": ""
                });
            }
			$('#js_plan_addcity_list').show();
			$("#xcd-taginput").focus();


            if(!this.searchcityAuto ) {
                this.searchcityAuto  = new AutoThink({
                    inputel: "#xcd-city-input",
                    listel: "#xcd-city-list",
                    itemel: "#xcd-city-list a",
                    autoClearInput: false,
                    posturl: "/api/searchCity",
                    entercb: function (arg) {
                        that.searchEntercb(arg);
                    },
                    postdata: {
                        fromway: 'plan'
                    },
                    clickcb: function (e) {
                        that.searchClickcb(e);
                    }
                });
            }

			
		},
	    searchEntercb: function (arg) {
	        this.searchCityCallback(arg.obj);
	    },
	    searchClickcb: function (e) {
	        this.searchCityCallback(e.currentTarget);
	    },
	    searchCityCallback: function (thiz) {
	        var city_id = $(thiz).attr("data-dataid");
	        var city_name = $(thiz).attr("data-title");
	      	
			if(!city_id || !city_name) {
				return false;
			}
			var data = {
				city_name : city_name,
				city_id : city_id
			};
			$("#js_plan_addcity_list").hide();

			this.curoneday.get("objectView").add_city(data);

			this.renderCityList();

            window.app.poiListView.setDefaultCity({
                city_id: city_id,
                city_name: city_name
            });
	    },
		
		//contentView添加 poi or hotel to 这一天的行程中
		addPOIEvent: function(domObject, callback){
			var tmptype = typeof domObject;
			var initdata = {};
			var li = $(domObject);

			var that = this;
			var city_id, city_name;
			initdata.xid = 0;
			initdata.poi_id = $(li).attr('data-poi_id');
			initdata.poi_name = $(li).attr("data-poi_name");
			initdata.type = $(li).attr('data-type');
			initdata.city_id = $(li).attr('data-city_id');
			initdata.city_name = $(li).attr('data-city_name');
			initdata.desc = $(li).attr("data-desc");
			initdata.address = $(li).attr("data-address");
			initdata.cover_image = $(li).attr("data-cover_image");
			initdata.href = $(li).attr("data-href");
			initdata.lat = $(li).attr("data-lat");
			initdata.lon = $(li).attr("data-lon");

            var event = new OnedayEvent.Models.OnedayeventModel(initdata);
			var dayView = this.curoneday.get("objectView");

			dayView.addPOIEvent(event);
		},
	    initPoiForm: function () {
	        var self = this;
	        this.poiform = new FormViews.Views.OnedayPoiForm({
	            views: self
	        });
            this.poiform.xcdCntView = this;
	    },
	    getPoiForm: function () {
	        return this.poiform;
	    },
	    getHotelForm: function () {
	        var form = new FormViews.Views.OnedayHotelForm();
	        form.xcdCntView = this;
	        return form;
	    },
        getDayDescForm: function () {
            var form = new FormViews.Views.OnedayDescForm();
            form.xcdCntView = this;
            return form;
        },
	    renderCityList: function (opts) {

	        var onedaymodel = this.curoneday;
	        var onedayview = onedaymodel.get("objectView");
	        var taglistdiv = $(this.$el).find("#js_plan_addtag");
	        var cbfunc = function () {};
			//更新右边poi list

	        if (typeof opts != 'undefined' && opts.qyeropt == 'init') {
	            cbfunc = function () {
	                var citylist = onedaymodel.get("city_list");
	                if (!citylist) return;
	                var size = _(citylist).size();
	                if (size <= 0) return;
	                var jsontaglist = citylist.collection2JSON();
	                if (!jsontaglist) return;
	                $.each(jsontaglist, function (k, v) {
	                    if (v.city_id > 0) {
	                        window.app.poiListView.setDefaultCity({
	                            city_id: v.city_id,
	                            city_name: v.city_name
	                        });
	                        return false;
	                    }
	                });
	            };
	        }
            //console.log(onedaymodel)
	        onedayview.renderall_citylist(taglistdiv, opts, cbfunc);

	    }

	});
});

