define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');
	
	//具体某一天
	var Oneday = require('../modules/oneday');
	//具体一天行程内容
	var TripContentView = require('./trip_content_view');
	//一天里的交通模块
	var Traffic = require('../modules/traffic');
	
	exports.Views = {};
	exports.Views.TripAllDayView = Backbone.View.extend({
		el : ".menu_cnt",
	    events: {
	        "click #xcd_addoneday_last a": "click_addoneday"
	    },
		initialize: function() {
			this.container = ".menu_cnt .xcd_item";
			this.xcdid = ".menu_cnt";
			this.current_onedaycid = "";
			this.current_oneday = null;
			
			_.bindAll(this, "appendOneday", "trigger_change_starttime","trigger_change_poilist_result", "refresh_xcd_delete", "insert_oneday");
			this.init_allday_collection();

            $("#plans_datepicker").on("change", function () {
                var sdate = $("#plans_datepicker").val();
                var reg = /^(\d{4})(-|\/)(\d{1,2})\2(\d{1,2})$/;
                if (sdate.match(reg) == null) {
                    $("#plans_datepicker").val("");
                    sdate = "";
                }
                window.app.setStartTime(sdate);
            });

            //初始化一天的交通的View
			this.onedayTraffic = new Traffic.Views.OnedayTrafficView();

			//new 出第二栏的Poi列表的 view
			window.app.colB.addView("xcd_poievent_list", function () {
				return new TripContentView.Views.XcdCntView();
			});

		},
		init_allday_collection: function() {
			if (typeof this.allday != 'undefined') {
				delete this.allday;
			}
			//所有天数
			this.allday = new Oneday.Collections.TripAllDayList();

			this.allday.bind("add", this.appendOneday);
	        //this.allday.bind("remove", this.refresh_xcd_delete);
			this.allday.bind("add remove", this.trigger_change_starttime);
			//绑定每天变换后，poilist的变换（城市列表变了）
			this.allday.bind("add remove", this.trigger_change_poilist_result);

		},
	    trigger_change_poilist_result: function () {
			//window.app.sync_add_remove_citys_callback();
	    },
		render: function(callback) {
			var that = this;
			callback = callback || function() {
				var m = that.get_current_oneday();
				if(m) m.get("objectView").switch2modeledit();
			}
			$(".xcd_list").remove();
			//reset
			that.allday.reset(false);
			this.fetchData(function(){
				callback();
			});
		},
		fetchData: function(callback){
			var that = this;
			//TODO trip_id get
			var trip_id = window.app.getTripId();
			callback = callback || function(){};
			$.post('/api/fetchAllDays', {"trip_id": trip_id}, function(result){
				if(result.code == 200) {
					for(i=0; i< result.data.length; i++) {
						var day = result.data[i];
						var newOneday = new Oneday.Models.TripOnedayModel(day, {position:"last"});
						if(that.current_onedaycid == "") {
							that.current_onedaycid = newOneday.cid;
						}
						that.allday.push(newOneday, {position: 'init'});
					}
				} else {
					alert(result.message);
				}
				callback();
			});
		},
		get_current_oneday: function() {
			var cid = this.current_onedaycid;
			if (cid == '') cid = 1;
			return this.allday.get(cid);
		},
	    click_addoneday: function () {
			var that = this;
			var model = new Oneday.Models.TripOnedayModel();
			var okcallback = okcallback || function(){
				model.get("objectView").switch2modeledit();
	            var prevM = that.__getPrevModelByCid(model, -1);
				if (prevM) {
	            	var city_list = prevM.get("city_list");
					
	                var citysjson = city_list.collection2JSON();

                    if (citysjson) {
	                    var citylen = citysjson.length;

	                    if (citylen >= 1) {
	                        var tag = citysjson[citylen - 1];
	                        model.get("objectView").add_city(tag);
	                        window.app.colB.getView("xcd_poievent_list").renderCityList({
	                            option: 'init'
	                        });
	                    }
	                }

				}
			};
	        this.allday.push(model, {
	            position: "last",
	            okcb: okcallback,
	            golast: true
	        });
	    },
		insert_oneday: function(srcmodel, okcallback) {
			var that = this;
			var model = new Oneday.Models.TripOnedayModel();
			var at = this.__getIndexByCid(srcmodel);
			var okcallback = okcallback || function(){
				model.get("objectView").switch2modeledit();
				//如果是两个城市？？？
				//Plan.fixresize("insert_oneday");
			};
			this.allday.push(model, {
				"position" : "before",
				srcmodel: srcmodel,
				at: at,
				okcb: okcallback
			});
		},
		appendOneday: function(model, unkonwnxx, options) {
			var where = options.position;
			var size = this.allday.size();
			var onedayView = new Oneday.Views.TripOnedayView({model:model});
			
			//处理回调函数
			if(typeof options.okcb != 'undefined') {
				var okcb = options.okcb;
			} else {
				var okcb = null;
			}
			okcb = okcb || function () {};
			if (typeof okcb != 'function') {
				okcb = function () {};
			}
			
			var el = onedayView.render();
			model.set({objectView: onedayView});
			
			if('init' == where) {
				$(el).appendTo($(this.container));
			} else if("before" == where) {
				var srcmodel = options.srcmodel;
				$(el).insertBefore(srcmodel.get("objectView").$el).hide().slideDown("slow");
			} else if("last" == where) {
				var container = $(this.container);
				this.scrollToTop();
				$(el).appendTo($(this.container));
			}	
			var xid = model.get('xid') || 0;
			xid = parseInt(xid);
			if (xid > 0) {
				okcb();
				return ;
			}
			var index = this.__getIndexByCid(model);
			//保存到服务器
			model.save2server(index, okcb);
		},
		refresh_xcd_delete: function(x, y, z){
			var delidx = z.index;
			var len = this.allday.size();
			if (len <= 0) {
				var m = this.new_onedaymodel();
				this.allday.push(m, {
					qyer: "last"
				});
				Plan.fixresize("add_oneday");
			}
			if (delidx == len) delidx = len - 1;
			if (delidx <= 0) delidx = 0;
			var m = this.allday.at(delidx);
			m.get("objectView").switch2modeledit();
		},
		trigger_change_starttime: function(){
			var size = this.allday.size();
			$('#js_plan_totalday').html(size == 0 ? "--": size);
			
			var sdate = window.app.getStarttime();
			if ("" != sdate) {
				var arrdate = sdate.split("-");
				var sDate = new Date();
				var y = parseInt(arrdate[0].replace(/^0+(\d+)/, "$1")) - 0;
				var m = parseInt(arrdate[1].replace(/^0+(\d+)/, "$1")) - 1;
				var d = parseInt(arrdate[2].replace(/^0+(\d+)/, "$1")) - 0;
				sDate.setFullYear(y, m, d);
				var day;
				var week = ["星期日", "星期一", "星期二", "星期三", "星期四", "星期五", "星期六"];
				this.allday.each(function (onedaymodel, idx) {
					day = sDate.getDay();
					onedaymodel.set({
						"datedesc": "D" + (idx + 1),
						timedesc: (sDate.getMonth() + 1) + "月" + sDate.getDate() + "日 " + week[day]
					});
					sDate.setDate(sDate.getDate() + 1);
				});
			} else {
				this.allday.each(function(onedayModel, idx){
					onedayModel.set({"datedesc": "D" + (idx + 1)});
				});
			}
			
		},
		__getPrevModelByCid: function (onedaymodel, num) {
			var curIdx = this.__getIndexByCid(onedaymodel);
			if (curIdx > 0) curIdx = curIdx + num;
				else curIdx = -1; if (curIdx < 0) return;
					return this.allday.at(curIdx);
		},
		__getIndexByCid: function (onedaymodel) {
			var cid = onedaymodel.cid;
			var ooidx = -1;
			this.allday.each(function (m, idx) {
				if (m.cid == cid) {
					ooidx = idx;
					return false;
				}
			});
			return ooidx;
		},
		resetForDragOrder: function() {

			this.allday.resetForDragOrder();
			this.trigger_change_starttime();
			//var current_oneday = window.app.getCurrentOnedayModel();

		},
	    scrollToTop: function () {
	        $(".xcd_cnt_scroll").scrollTop(0);
	    }
	});
	
});
