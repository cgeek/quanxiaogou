define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');

	var AutoThink = require('autoThink').autoThink;
	var xiaogouUI = require('../modules/ui').UI;
	var commonStyle = require('../modules/style').commonStyle;
	var OnedayEvent = require('../modules/oneday_event');
		
	exports.Views = {};
	exports.Views.OnedayPoiForm = Backbone.View.extend({
	    el: "body",
	    elform: "#poiform",
	    events: {
	        'click #_jstabpoititle': 'tabToTitleInput',
	        'click #_jssubmitonedaypoi': "submitForm",
	        'keyup #poititleinput': 'submitButtonStatus',
	        'keyup #poiform #poinote': 'showNoteTip',
	        'focus #poiform #poinote': 'showNoteTip',
	        "click #_jsaddpoibtn": 'showAddForm',
	        'click #_jscancelonedaypoi': 'closeForm',
	        'click #_jspoiticketcount li': 'countSpend',
	        'keyup #poiform #_jspoispendinput': 'countSpend'
	    },
	    initialize: function (args) {

	        if (args.views) {
	            this.views = args.views;
	        }
	        var self = this;

	        new AutoThink({
	            inputel: "#poititleinput",
	            listel: "#_jstitlesearchlist",
	            itemel: "#_jstitlesearchlist a",
	            autoClearInput: false,
	            posturl: "/api/searchPoi",
                template: '#tpl_searchlivepoi',
	            entercb: function (arg) {
	                self.searchEntercb(arg);
	            },
	            postdata: {
	                fromway: 'plan'
	            },
	            clickcb: function (e) {
	                self.searchClickcb(e);
	            }
	        });
			_.bindAll(this, 'editPoi', 'getCurItem');
	        this.data = {};
	        this.notemaxlen = 1000;
	    },
	    countSpend: function () {

	    },
	    submitForm: function () {
	        this.getData();

	        if (!this.chksubmit()) {
	            return false;
	        }

	        if (this.data.doaction == 'add') {
	            this.addPoi();
	        } else if (this.data.doaction == 'edit') {
	            this.editPoi();
	        }
	    },
        get_xcdview: function () {
            return this.xcdCntView;
        },
	    addPoi: function () {
	        delete this.data.doaction;

            /*

            var tmptype = typeof domObject;
            var initdata = {};
            var li = $(domObject);

            var that = this;
            var city_id, city_name;
            initdata.xid = 0;
            initdata.poi_id = $(li).attr('data-poi_id');
            initdata.poi_name = $(li).attr("data-poi_name");
            initdata.type = $(li).attr('data-type');
            initdata.city_id = $(li).attr('data-city_id');
            initdata.city_name = $(li).attr('data-city_name');
            initdata.desc = $(li).attr("data-desc");
            initdata.address = $(li).attr("data-address");
            initdata.cover_image = $(li).attr("data-cover_image");
            initdata.href = $(li).attr("data-href");
            initdata.lat = $(li).attr("data-lat");
            initdata.lon = $(li).attr("data-lon");
            */



            var that = this;
            var thiz = this.get_xcdview();
            var event = new OnedayEvent.Models.OnedayeventModel(this.data);
            var dayview = thiz.curoneday.get("objectView");
            dayview.addPOIEvent(event, function () {
                xiaogouUI.popup.pupclose();
            } );

	    },
	    editPoi: function () {
	        delete this.data.doaction;
	        var curitem = this.getCurItem();
	        var thiz = this;
	        curitem.editEvent(this.data, function () {
	            thiz.closeForm();
	        });
	    },
	    chksubmit: function () {
	        if (this.data.title.length < 1) {
	            return false;
	        }
	        var tlen = util.getwordlen(this.data['note']);
	        var tmax = this.notemaxlen;
	        if (tlen > (tmax * 2)) {
	            commonStyle.showErrorLayer($("textarea[name=note]", this.elform), '请填写正确的备注信息！');
	            return false;
	        }
	        return true;
	    },
	    submitButtonStatus: function () {

	        var title = commonStyle.getVal($("input[name=title]", this.elform));

	        if (title && title.length > 0) {
	            $("#_jssubmitonedaypoi").removeClass("ui_button_disabled").addClass("ui_buttonB").attr("disabled", false);
	        } else {
	            $("#_jssubmitonedaypoi").addClass("ui_button_disabled").removeClass("ui_buttonB").attr("disabled", true);
	        }
	    },
	    getData: function () {
			var getarr = ['doaction', 'title', 'type', 'poi_id','poi_name', 'cover_image', 'desc'];
	        for (var i = 0; i < getarr.length; i++) {
	            var iname = getarr[i];
	            this.data[iname] = commonStyle.getVal($("input[name=" + iname + "]", this.elform));
	        }


			this.data['note'] = commonStyle.getVal($("textarea[name=note]", this.elform));
	    },
	    showNoteTip: function (e) {
	        var thiz = e.currentTarget;
	        commonStyle.showFontTip(thiz, this.notemaxlen);
	        return;
	    },
	    searchEntercb: function (arg) {
	        this.searchcb(arg.obj);
	    },
	    searchClickcb: function (e) {
	        this.searchcb(e.currentTarget);
	    },
	    searchcb: function (thiz) {

	        this.data.pic = $(thiz).attr('data-cover_image');
	        this.data.type = $(thiz).attr('data-type');
            this.data.poi_id = parseInt($(thiz).attr('data-dataid'));
	        this.data.title = $(thiz).attr("data-title");

	        var cnname = $(thiz).attr('data-title');
	        $("#_jspoicoverimg").attr('src', this.data.pic);
	        $("#_jstabpoititle").html("<strong>" + cnname + "</strong>");
	        $("input[name=type]", this.elform).val(this.data.type);
	        $("input[name=poi_id]", this.elform).val(this.data.poi_id);
	        $("input[name=title]", this.elform).val(cnname);
            $("input[name=poi_name]", this.elform).val(cnname);
            $("input[name=cover_image]", this.elform).val(this.data.pic);
	        if (this.data.type != '') {
	            $("#_jscatetypecontainer").hide();
	        } else {
	            $("#_jscatetypecontainer").show();
	        }
	        this.tabToTitle();
	    },
	    tabToTitle: function () {
	        $("#_jstabpoititle").show();
	        $("#poititleinput").hide();
	    },
	    tabToTitleInput: function () {
	        $("#poititleinput").show();
	        $("#_jstabpoititle").hide();
	    },
	    ajaxForm: function (data) {
			var _url = "/api/PoiForm?";
	        var trip_id = window.app.getTripId();
	        var trip_day_id = window.app.getCurrentOnedayModel().get("xid");
	        _url += "trip_id=" + trip_id + "&trip_day_id=" + trip_day_id;

            if (typeof data == 'object') {
	            for (var k in data) {
	                _url += "&" + k + "=" + data[k];
	            }
	        }

	        xiaogouUI.popup.ajax({
	            url: _url,
	            width: 600
	        });
	    },
	    showAddForm: function () {

	        this.ajaxForm();
	    },
	    showEditForm: function () {
	        var curmodel = this.getCurModel();
			console.log(curmodel);
	        var id = curmodel.get('xid')
	        if (!id) {
	            alert('数据还未同步，请稍等或者重载页面');
	            return false;
	        }
	        this.ajaxForm({
	            id: id
	        });
	    },
	    doEdit: function (item) {
	        this.registerCurItem(item);
	        this.showEditForm();
	    },
	    closeForm: function () {
	        xiaogouUI.popup.pupclose();
	    },
	    registerCurItem: function (item) {
	        this.curitem = item;
	    },
	    getCurItem: function () {
	        return this.curitem;
	    },
	    getCurModel: function () {
	        var item = this.getCurItem();
	        return item.model;
	    }
	});
	
	
	exports.Views.OnedayHotelForm = Backbone.View.extend({
	    el: "body",
	    elform: "#jsplans_pophotel_form",
	    events: {
	        'keyup #jsSignlePrice': 'countSpend',
	        'click #_jspoiticketcount li': 'countSpend'
	    },
	    countSpend: function () {
	        setTimeout(function () {
	            var spend = commonStyle.getVal($("*[name=spend]", this.elform));
	            var ticketcount = parseInt(commonStyle.getVal($("*[name=ticketcount]", this.elform)));
	            var v;
	            if (isNaN(spend) || isNaN(ticketcount)) {
	                v = '';
	            } else {
	                v = parseFloat(spend * ticketcount).toFixed(2);
	            }
	            $("#_jspoicountspend").html(v);
	        }, 100);
	    },
	    initialize: function () {
	        this.xcdCntView = false;
	        this.eventview = false;
	        var thiz = this;
			
	        this.formdata = {};
	        this.notemaxlen = 1000;
	        this.searchTimer = 0;
	        this.hotelEventPlusbtnDiv = "#js_plan_addhotelevent_plusbtn_div";
	        this.hotelInputBtn = "#js_plans_addhotelevent";
	        this.hotelEventResultID = "#js_plans_searchhotel_result";
	        this.hotelEventInputDiv = "#js_plan_addhotelevent_input_div";
	        this.hotelEventPopPic = "#js_plans_pophotel_pic";
	        this.hotelEventNote = "#js_plans_pophotel_note";
	        this.close_hotelevent_result = function () {
	            $(this.hotelEventResultID).hide();
	            $(this.hotelInputBtn).val("");
	            this.update_inputvalue();
	        };
	        this.update_inputvalue = function () {
	            var data = this.formdata;
	            var ipt = $(this.hotelInputBtn);
	            var tmpt = "";
	            tmpt = data.title;
	            $(ipt).val(tmpt);
	        };
	        this.save_hotel_info = function (json) {
				var showtitle = "";
	            if (json.iscustom) {
	                thiz.formdata = $.extend(thiz.formdata, json);
	                thiz.formdata.pic = "";
	            } else {
	                thiz.formdata = $.extend(this.formdata, json);
	            }
				
				showtitle = thiz.formdata.title;
	            thiz.formdata.iscustom = json.iscustom;
	            $(thiz.hotelEventPopPic).attr("src", thiz.formdata.cover_image);
	            var title = _.escape(showtitle);
	            $(thiz.hotelInputBtn).hide();
	            $(thiz.hotelInputBtn + "_show strong").html(title);
	            $(thiz.hotelInputBtn + "_show").show();
	            thiz.close_hotelevent_result();
	        };
	        this.form_id = "#jsplans_pophotel_form";
	        this.close_popup = function () {
	            xiaogouUI.popup.pupclose();
	        };
	        this.getFormData = function () {
	            var formobj = $("#jsplans_pophotel_form");
	            var noteobj = $("#js_plans_pophotel_note");
				
	            var spendobj = $("*[name=spend]", formobj);
	            var currencyobj = $("*[name=currency]", formobj);
	            var inDayObj = $('*[name=inDay]', formobj);
	            var outDayObj = $('*[name=outDay]', formobj);
	            var ticketcountObj = $('*[name=ticketcount]', formobj);
				
	            var data = {};
	            data.spend = $(spendobj).val();
	            data.currency = $(currencyobj).val();
	            data.note = $(noteobj).val();
	            data.inDay = inDayObj.val();
	            data.outDay = outDayObj.val();
	            data.ticketcount = ticketcountObj.val();
	            data.type = 'hotel';
	            var tmpd = {};
	            tmpd = $.extend(this.formdata, data);
	            return tmpd;
	        };
	    },
		search_idx : function (where, list) {
		    var a = list;
		    var idx = -1;
		    $(a).each(function (k, v) {
		        if ($(v).hasClass("current")) {
		            idx = k;
		        }
		    });
		    var size = $(a).size() - 1;
		    if ("down" == where) {
		        idx++;
		        if (idx >= size) {
		            idx = size;
		        }
		    } else if ("up" == where) {
		        idx--;
		        if (idx <= 0) idx = 0;
		    }
		    $(a).removeClass("current");
		    $(a).eq(idx).addClass("current");
		},
	    catch_event_input: function (event, cacth_enter) {
	        var code = event.keyCode;
	        var thiz = this;
	        var list = list = $(this.hotelEventResultID + " ul li a");
	        if (40 == code) {
	            thiz.search_idx("down", list);
	            return;
	        } else if (38 == code) {
	            thiz.search_idx("up", list);
	            return;
	        }
	        var xcdview = this.get_xcdview();
	        var input = $(this.hotelInputBtn);
	        var title = $(input).val() || "";
	        title = title.gbtrim(50);
	        title = $.trim(title);
	        if (typeof code == 'undefined') {
	            code = 13;
	        }
	        if ("13" == code && "cacth-enter" == cacth_enter) {
	            if ("" == title) return;
	            var objdom = $(list).filter(".current").parent("li");
	            var ddata;
                var ddata = thiz._getdatafrom_dom($(objdom));
                ddata = $.extend(this.formdata, ddata);
                thiz.save_hotel_info(ddata);
	            return ddata;
	        }
	        this.result_search(title);
	    },
	    result_search: function (kw) {
	        var type = "hotel";
	        var result_hotel = this.hotelEventResultID;
	        var hide_result_func = function () {
	            $(result_hotel).hide();
	        };
	        if (typeof kw == 'undefined') {
	            hide_result_func();
	            return;
	        }
	        if ("" == kw) {
	            hide_result_func();
	            return;
	        }
	        var data = {
	            kw: kw
	        };
	        data.format = "html";
	        data.version = "plan_edit_v2.3";
	        $.post("/api/searchHotel", data, function (result) {
	            if (result.code == 200) {
					var tpl = '<ul>{{#hotel_list}}<li cover_image="{{cover_image}}" title="{{title}}" desc="{{desc}}" hotel_id="{{hotel_id}}"><a href="javascript:void(0);">{{title}}({{city_name}})</a></li>{{/hotel_list}}</ul>';
					var html = Mustache.to_html(tpl, result.data);
	                $(result_hotel).find(".title_list").html(html);
	                $(result_hotel).show();
	            }
	        });
	    },
	    _getdatafrom_dom: function (domobj) {
	        var initdata = {};
            var li = $(domobj);
            initdata.title = $(li).attr("title");
			initdata.cover_image = $(li).attr("cover_image");
			initdata.hotel_id = $(li).attr("hotel_id");
			initdata.desc = $(li).attr('desc');
			initdata.type = 'hotel';
	        return initdata;
	    },
	    get_xcdview: function () {
            console.log(this.xcdCntView);
	        return this.xcdCntView;
	    },
	    bind_events: function () {
	        var thiz = this;
	        var form = $(this.form_id);
	        var hideerror = function (t) {
	            $(t).next("div._jserrorlayer").hide();
	        };
	        $("textarea[name=note]").focus(function () {
	            hideerror(this);
	        });
	        $("input[name=spend]").focus(function () {
	            hideerror(this);
	        });
	        $(thiz.hotelInputBtn).focus(function () {
	            hideerror(this);
	        });
			$(document).delegate(this.hotelEventResultID + " ul li", 'click', function(e){
				var data = {};
				data = thiz._getdatafrom_dom($(this));
				thiz.save_hotel_info(data);
				
	            e.stopPropagation();
	        });
	        $(this.hotelInputBtn).click(function (e) {}).keyup(function (event) {
	            if (thiz.searchTimer > 0) {
	                clearTimeout(thiz.searchTimer);
	                thiz.searchTimer = 0;
	            }
	            var code = event.keyCode;
	            if (40 == code || 38 == code) {
	                var searchdata = thiz.catch_event_input(event, "cacth-enter");
	            } else {
	                thiz.searchTimer = setTimeout(function () {
	                    var searchdata = thiz.catch_event_input(event, "cacth-enter");
	                }, 500);
	            }
	        });
	        $(this.hotelEventNote).on('keyup', function () {
	            commonStyle.showFontTip(this, thiz.notemaxlen);
	        });
	        $("#js_plans_addhotelevent_show").click(function () {
	            var title = $(this).text();
	            var input = $(thiz.hotelInputBtn);
	            $(input).val(title).show();
	            $(this).hide();
	            $(input).focus();
	        });
	        $("#js_plans_pophotel_save").click(function () {
	            var data = thiz.getFormData();
	            if (data['title'] == undefined || !data['title']) {
	                data['title'] = $("#js_plans_addhotelevent").val();
	            }
	            var cb = function () {
	                thiz.close_popup();
	            };
	            var elform = $(thiz.form_id);
	            var inputtitle = $("#js_plans_addhotelevent");
	            var laytitle = $("#js_plans_addhotelevent_show");
	            if ($.trim(data['title']) == '') {
	                laytitle.hide();
	                commonStyle.showErrorLayer(inputtitle, '请输入酒店的名字！');
				    return;
	            }
	            var note = data.note;
	            note = $.trim(note);
	            if (note) {
	                var tlen = util.getwordlen(note);
	                var tmax = thiz.notemaxlen;
	                if (tlen > (tmax * 2)) {
	                    commonStyle.showErrorLayer($("textarea[name=note]", elform), '请填写正确的备注信息！');
	                    return false;
	                }
	            }
				
	            if (thiz.eventview) {
	                thiz.eventview.editEvent(thiz.formdata, cb);
	            } else {

	                thiz.addEvent(thiz.formdata, cb);
	            }
	        });
	        $("#js_plans_pophotel_cancel").click(function () {
	            thiz.close_popup();
	        });
	    },
		//点击编辑酒店，根据id，调用api，渲染出酒店编辑form
	    render: function (isedit, eventView) {
	        var _url = "/api/hotelForm";
	        var thiz = this;
	        var trip_id = window.app.getTripId();
	        var trip_day_id = window.app.getCurrentOnedayModel().get("xid");
	        _url += "?trip_id=" + trip_id + "&trip_day_id=" + trip_day_id;
	        if (isedit) {
	            var data = eventView.model.model2JSON();
	            this.formdata = data || {};
	            _url += "&eventid=" + this.formdata.xid;
	            this.eventview = eventView;
	        } else {
	            if (!this.formdata.id)
	                this.formdata = {};
	            this.eventview = false;
	            $(thiz.hotelInputBtn).focus();
	        }
	        var cb = function () {
	            thiz.bind_events();
	            var currText = $('input[name=inDay]').next().find('.contents').find('a[data-value="' + trip_day_id + '"]').text();
	            var currDayId = trip_day_id;
	            var isFirst = false;
	            if (currText == undefined || !currText) {
	                currText = '&nbsp;';
	                currDayId = 0;
	                isFirst = true;
	            }
	            $('input[name=inDay]').next().find('.titles span').html(currText);
	            $('input[name=inDay]').val(currDayId);
	            var ckObj = $('input[name=outDay]').next().find('.contents').find('a[data-value="' + trip_day_id + '"]');
	            if (ckObj.length > 0) {
	                var obj = ckObj.parent().next().find('a');
	            } else {
	                var obj = $('input[name=outDay]').next().find('.contents').find('li:first').find('a');
	            }
	            currText = obj.text();
	            if (currText == undefined || !currText) currText = '&nbsp;';
	            $('input[name=outDay]').next().find('.titles span').html(currText);
	            $('input[name=outDay]').val(obj.attr('data-value'));
	            var priceInput = $('input[name=spend]');
	            if (thiz.formdata.lower_price) {
	                priceInput.val(thiz.formdata.lower_price)
	            }
	            var numberInput = $('input[name=ticketcount]');
	            if (parseInt(numberInput.val()) == 0 || (undefined == thiz.formdata.ticketcount || !thiz.formdata.ticketcount)) {
	                numberInput.val(1);
	            } else if (!numberInput.val()) {
	                numberInput.val(thiz.formdata.ticketcount);
	            }
	            numberInput.next().find('.titles span').text(numberInput.val());
	            if (numberInput.val() && (priceInput != undefined && priceInput.val()))
	                thiz.countSpend();
	            $('#js_plans_addhotelevent').val(thiz.formdata.en_name);
	            currencyInput = $('input[name=currency]');
	            if (thiz.formdata.currency_code) {
	                currencyInput.val(thiz.formdata.currency_code);
	                var currencyText = currencyInput.next().find('a[data-value=' + thiz.formdata.currency_code + ']').text();
	                if (currencyText)
	                    currencyInput.next().find('.titles span').text(currencyText);
	            }
	            if (thiz.formdata.pic)
	                $('.title img', thiz.elform).attr('src', thiz.formdata.pic);
	            if (thiz.formdata.en_name) {
	                $('#js_plans_addhotelevent_show', thiz.elform).text(thiz.formdata.en_name + ',' + thiz.formdata.cn_name).show();
	                $('#js_plans_addhotelevent', thiz.elform).hide();
	            }
	        }
	        xiaogouUI.popup.ajax({
	            url: _url,
	            width: 600,
	            callback: cb
	        });
	    },
	    addEvent: function (initdata, cb) {
			var that = this;
	        var thiz = this.get_xcdview();
	        if (!thiz) return;
	        cb = cb || function () {};
	        if ("请输入酒店名" == initdata.title || "" == initdata.title) {
	            return;
	        }

	        var event = new OnedayEvent.Models.OnedayeventModel(initdata);
	        var dayview = thiz.curoneday.get("objectView");
	        dayview.addHotelEvent(event, function () {
				cb();
	            $(".xcd_cnt_scroll").animate({
	                scrollTop: $('.day_item')[0].offsetHeight + $('.day_city')[0].offsetHeight
	            });
	        });
			cb();
	    }
	});


    exports.Views.OnedayDescForm = Backbone.View.extend({
        el: "body",
        elform: "#jsplans_popdesc_form",
        events: {
        },
        initialize: function () {
            this.xcdCntView = false;
            this.eventview = false;
            var thiz = this;

            this.formdata = {};
            this.notemaxlen = 1000;
            this.searchTimer = 0;

            this.form_id = "#jsplans_popdesc_form";
            this.close_popup = function () {
                xiaogouUI.popup.pupclose();
            };
        },

        _getdatafrom_dom: function (domobj) {
            var initdata = {};
            var li = $(domobj);
            initdata.desc = $(li).attr('desc');
            return initdata;
        },
        get_xcdview: function () {
            return this.xcdCntView;
        },
        bind_events: function () {
            var thiz = this;
            var form = $(this.form_id);
            var hideerror = function (t) {
                $(t).next("div._jserrorlayer").hide();
            };
            $("textarea[name=desc]").focus(function () {
                hideerror(this);
            });
            $("#js_plans_popdesc_save").click(function () {
                var dayview = thiz.get_xcdview().curoneday.get("objectView");
                var trip_title = $('input[name=trip_title]').val();
                var desc = $('textarea[name=desc]').val();
                var traffic_note = $('textarea[name=traffic_note]').val();

                dayview.updateDayInfos({desc: desc, traffic_note: traffic_note, title: trip_title});
                //dayview.updateDayInfo('desc', desc);
                //dayview.updateDayInfo('traffic_note', traffic_note);
                //dayview.updateDayInfo('trip_title', trip_title);

                if(desc) {
                    $(".xcd_note").find('.trip_day_desc').html('<strong>行程简介：</strong><br />' + desc);
                } else {
                    $(".xcd_note").find('.trip_day_desc').html('');
                }
                if(traffic_note) {
                    $(".xcd_note").find('.trip_day_traffic_note').html('<strong>交通说明：</strong>' +  traffic_note);
                } else {
                    $(".xcd_note").find('.trip_day_traffic_note').html('');
                }

                if(trip_title) {
                    $(".xcd_title input").val(trip_title);
                } else {
                    $(".xcd_title input").val('');
                }

                thiz.close_popup();
            });
            $("#js_plans_popdesc_cancel").click(function () {
                thiz.close_popup();
            });
        },
        //点击编辑酒店，根据id，调用api，渲染出酒店编辑form
        render: function (isedit, eventView) {
            var _url = "/api/dayDescForm";
            var thiz = this;
            var trip_id = window.app.getTripId();
            var trip_day_id = window.app.getCurrentOnedayModel().get("xid");
            _url += "?trip_id=" + trip_id + "&trip_day_id=" + trip_day_id;
            if (isedit) {
                var data = eventView.model.model2JSON();
                this.formdata = data || {};
                _url += "&eventid=" + this.formdata.xid;
                this.eventview = eventView;
            } else {
                if (!this.formdata.id)
                    this.formdata = {};
                this.eventview = false;
            }
            var cb = function () {
                thiz.bind_events();
            }
            xiaogouUI.popup.ajax({
                url: _url,
                width: 600,
                callback: cb
            });
        }
    });


});

