define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone');

	exports.Views = {};
	
	//ColA 是最左栏的行程按日期的导航栏
	exports.Views.ColA = Backbone.View.extend({
		el: '.pl_left',
		coll: {}, //记录A栏里面有得块
		current_selected_idx: 0, //当前选中的id
		initialize: function() {
		},
		render : function(name, args) {
			name = $.trim(name) || "xcd";
			args = args || "";
			if( typeof this.coll[name] == 'undefined') 
				return ;
			var obj = this.coll[name];
			if(typeof obj == 'function') {
				obj = (this.coll[name])();
				this.coll[name] = obj;
			}

            obj.render(args);
			this.current_selected_idx = name;
			var that = this;

			//初始化出发日期
			$("#plans_datepicker").datepicker({
				changeYear: true,
				dateFormat: 'yy-mm-dd'
			});
			$("#plans_datepicker").on("change", function(){
				var v = $("#datepicker").val();
			});

			$(".xcd_item").sortable({
				update: function () {
					var alldayView = that.getView('xcd');
					alldayView.resetForDragOrder();
				},
				start: function (event, ui) {
					$(ui.item).find(".insert").hide();
				},
				stop: function (event, ui) {
					$(ui.item).find(".insert").show();
				}
			});
		},
		addView: function(name, view) {
			if(typeof this.coll[name] != 'undefined') 
				delete this.coll[name];
			this.coll[name] = view;
		},
		getView: function(name) {
			return this.coll[name] || false;
		}
	});

	exports.Views.ColB = Backbone.View.extend({
		el: '.pl_rleft',
		coll: {},
		current_selected_idx: 0,
		initialize: function () {},
		render: function (name, args) {
			name = $.trim(name) || "xcd";
			args = args || "";
			if (typeof this.coll[name] == 'undefined') return;
				var obj = this.coll[name];
			if (typeof obj == 'function') {
				obj = (this.coll[name])();
				this.coll[name] = obj;
			} else {
				obj = this.coll[name];
			}
			if (obj) obj.render(args);
				this.current_selected_idx = name;

			$("#js_plan_activites").sortable({
				items: '.day_list',
				start: function (event, ui) {},
				update: function () {
					var oneday = window.app.getCurrentOnedayModel();
					var view = oneday.get("objectView");

					view.resetDragOrderForPOIEvent();

					$('#js_plan_activites').children().css({
						'position': '',
						'z-index': ''
					});
				},
				stop: function (event, ui) {}
			});
		},
		addView: function (name, v) {
			if (typeof this.coll[name] != 'undefined') delete this.coll[name];
			this.coll[name] = v;
		},
		getView: function (name) {
			return this.coll[name] || false;
		}
	});

	exports.Views.ColC = Backbone.View.extend({
		el: '.pl_rright',
		coll: {},
		cur_selected_idx: 0,
		initialize: function () {
            //console.log(window.app);
		},
		render: function (name) {
			name = $.trim(name) || "xcd";
			console.log("now, render view {" + name + "} .");
			if (name == this.cur_selected_idx) return;
            if (typeof this.coll[name] == 'undefined') return;
            var obj = this.coll[name];
    		if (obj) obj.render();
			this.cur_selected_idx = name;
		    window.app.alldayView = obj;
    	},
		addView: function (name, v) {
			if (typeof this.coll[name] != 'undefined') delete this.coll[name];
			this.coll[name] = v;
		},
        getView: function (name) {
            return this.coll[name] || false;
        }
	});
	
});

