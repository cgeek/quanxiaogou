String.prototype.gbtrim = function (len, s) {
	var str = '';
	var sp = s || '';
	var len2 = 0;
	for (var i = 0; i < this.length; i++) {
		if (this.charCodeAt(i) > 127 || this.charCodeAt(i) == 94) {
			len2 += 2;
		} else {
			len2++;
		}
	}
	if (len2 <= len) {
		return this;
	}
	len2 = 0;
	len = (len > sp.length) ? len - sp.length : len;
	for (var i = 0; i < this.length; i++) {
		if (this.charCodeAt(i) > 127 || this.charCodeAt(i) == 94) {
			len2 += 2;
		} else {
			len2++;
		}
		if (len2 > len) {
			str += sp;
			break;
		}
		str += this.charAt(i);
	}
	return str;
};

var util = {
	getwordlen: (function () {
		var byteLength = function (b) {
			if (typeof b == "undefined") {
				return 0
			}
			var a = b.match(/[^\x00-\x80]/g);
			return (b.length + (!a ? 0 : a.length))
		};
		var doublebyte = function (str) {
			return str.replace(/[^\x00-\xff]/g, '*');
		}
		return function (q, g) {
			if (g) {
				q = doublebyte(q);
			}
			return byteLength(jQuery.trim(q));
		}
	})()
};
