define(function(require, exports, module) {
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache'),
		cookie = require('cookie');

	var CreateFun = function(){};
		
	CreateFun.prototype.geturi = function () {
		var href = window.location.href;
		var re = new RegExp("\\?(.+)", "i");
		var garr = re.exec(href);
		uri = garr && garr.length > 0 ? garr[1] : '';
		return uri;
	}
	CreateFun.prototype.scroll = function (limit, callback, item) {
		var h, top;
		var self = this;
		self.openScroll();
		item = item || window;
		$(item).scroll(function () {
			h = typeof item.open == 'object' || item instanceof Window ? document.documentElement.offsetHeight : item.scrollHeight;
			top = $(item).scrollTop() + $(item).height();
			if (!needle) {
				return;
			}
			if (h - top < limit) {
				self.closeScroll();
				callback(function () {
					self.openScroll();
				})
			}
		});
	};
	CreateFun.prototype.openScroll = function () {
		window.needle = true;
	}
	CreateFun.prototype.closeScroll = function () {
		window.needle = false;
	}
	
	exports.Create  = new CreateFun();
});