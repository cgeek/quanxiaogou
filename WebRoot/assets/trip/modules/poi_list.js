define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');
	
	var Create = require('create').Create;
	var City = require('./city');
	
	exports.Models = {};
	exports.Models.DefaultCityModel = Backbone.Model.extend({
		defaults: {
			city_id: null,
			city_name: null,
			tab: 1,
			poi_type: 'sight',
			page: 0,
			nextto: true,
			tabmaplist: 'list',
			searchkey: ''
		},
		initialize: function () {}
	});

	exports.Views = {};
	exports.Views.PoiListView = Backbone.View.extend({
		el: ".pl_rright",
		listitem: "#_jsdefaultcitylist",
		poicontainer: '.pl_poi_cnt',
		events: {
			"click .rtag_jt": "showCityList",
			"click #_jsrighthotcity a": "chgDefaultCity",
			"click #_jsdefaultcitylist a": "chgCityByList",
			"click ._jstabdefaultcity": "tabToDefaultCity",
			"click ._jstabnoteclip": "tabToNoteClip",
			"click #_jsPoiTypeSelect": "switchPoiType",
            "click #_jsSearchPoi" : "searchPoiFromKeyword",
            "submit #_jsSearchPoiFrom" : "searchPoiFromKeyword",
			"click ._jsrightaddpoi": "rightAddPoi",	//添加poi
			"click ._jsrightaddhotel" : "rightAddHotel", //添加酒店
			"click ._jsrightadd_demotripday": "rightAddDemoTripDay", //添加经典行程
			"click ._jsrightaddrecommendhotel" : "addRecommendHotel",
			"click #_jstabtomap": "tabToMap",
			"click #_jstabtolist": "tabToList"
			
		},
		initialize: function(){
			_.bindAll(this, 'render', 'getPoiList', 'changedCity', 'rightAddPoi', 'rightAddDemoTripDay','switchPoiType', 'searchPoiFromKeyword', 'chgDefaultCity','tabToDefaultCity','renderCityList', 'showCityList');

            this.collection = new City.Collections.DefaultCityList();
	        this.collection.on('add remove', this.addCity);
        	
			this.model.on("change:city_id", this.changedCity);
			this.model.on("change:tabmaplist", this.chgedMaplist);
			this.model.on("change:cityid", this.chghotcitys);
			this.model.on("change:city_name", this.chgedDefaultCityname);

			this.render();
		},
		render: function() {
			var that = this;
			//绑定滚动事件,searchPoiList
			Create.scroll(30, function (cb) {
				that.toLoading();
				that.searchPoiList(that.model, cb);
			}, $(that.poicontainer)[0]);

            //console.log(this.model);
            //this.renderCityList();
		},
	    initModel: function () {
	        this.model.set({
	            page: 1,
	            nextto: true
	        });
	        Create.openScroll();
	    },

		switchPoiType: function(e) {
			var type = $(e.target).find('input[name=type]').val();
			$(e.target).siblings().removeClass('active');
			$(e.target).addClass('active')
			
			this.toLoading();
			this.model.set({
				poi_type : type,
				page: 1,
				nextto: true
			});
			this.searchPoiList(this.model);
			Create.openScroll();
			return false;
		},
        searchPoiFromKeyword: function(e) {
            var searchKey = $('#_jsSearchKeyword').val();
            var type = $(e.target).find('input[name=type]').val();
            $(e.target).siblings().removeClass('active');
            $(e.target).addClass('active')

            this.toLoading();
            this.model.set({
                searchkey: searchKey,
                page: 1,
                nextto: true
            });
            this.searchPoiList(this.model);
            Create.openScroll();
            return false;
        },
		getPoiList: function(model, argv) {
			this.toLoading();
			$('#_jssearchkey').val('');
			$('#_jssearchclose').hide();
			model.set({
				searchkey: '',
				searchrange: 1
			});
			this.searchPoiList(model);
		},
		changedCity: function(model){
			this.getPoiList(model);
		},
		toLoading: function(){
			var page = this.model.get('page');
			var tabmaplist = this.model.get('tabmaplist');
			$(".pl_rcity", this.$el).removeClass('pl_rcity_bg');
			if (page == 1) {
				$(".pl_rcity", this.$el).addClass('pl_loadcity');
				
				$(this.poicontainer, this.$el).hide();
			} else {
				$("._jsloadmorepoihotel").show();
			}
		},
		removeLoading: function(){
			var page = this.model.get('page');
			if (page == 1) {
				$(".pl_rcity", this.$el).removeClass('pl_loadcity');
			}
			$("._jsloadmorepoihotel").hide();
		},
		searchPoiList: function(model, callback) {

			if (!model.get('nextto')) {
				return false;
			}
			
			var that = this;
			var data = {
				'type' : this.model.get('poi_type'),
				'city_id' : this.model.get('city_id'),
				'page' : this.model.get('page')
			};
			
			if (model.get('searchkey') != '') {
				data['searchkey'] = model.get('searchkey');
			}
			
			$.get("/api/poiList", data, function(result) {
				if(result.code == 200) {
					that.renderSearchPoiList(result, callback);
				}
			});
			
		},
		renderSearchPoiList: function(result, cb) {
			var poiitem = $("#_jspoilist", this.$el);
			var page = this.model.get('page');
			var poiContainer = $(this.poicontainer, this.$el);
			var cityname = this.model.get('city_name');
			//todo view list
			
			var tpl = $('#poiItemView-tpl').html();
			if(result.data.type && result.data.type == 'note') {
				tpl = $('#noteItemView-tpl').html();
			} else if (result.data.type && result.data.type == 'demo_trip_day') {
				tpl = $('#demoTripDayView-tpl').html();
			}
			
			var html = Mustache.to_html(tpl, result.data);
			if(page > 1 ) {
				poiitem.append(html);
			} else {
                poiitem.html(html);
                if(result.data.poi_list == null || result.data.poi_list.length <= 0) {
                    poiitem.html('<p style="padding:10px;">没有找到属于该城市的地点，可以自己添加一个地点<a href="/poi/new?city_id='+ this.model.get('city_id') + '" target="_blank">添加</a></p>');
                }

			}

			poiContainer.show();
			
	        if (result.data ) {
                if((result.data.demo_trip_day_list && result.data.demo_trip_day_list.length > 0) || (result.data.poi_list && result.data.poi_list.length > 0)) {
				    if(cb) cb();
	                this.model.set('nextto', true);
	                $(".pl_rcity", this.$el).removeClass('pl_rcity_bg');
                } else {
	                this.model.set('nextto', false);
                }
	        } else {
	            this.model.set('nextto', false);
	            if (page == 1) {
	                if (this.model.get('searchkey') == '') {
	                    $(".pl_rcity", this.$el).addClass('pl_rcity_bg');
	                }
	            }
	        }
			
			this.removeLoading();
			this.model.set('page', page + 1);
			if(cityname) {
				$("#_jsdefaultcityname").html(cityname);
			}
			this.model.set('nextto', true);
			window.app.windowResize();
		},
		renderCityList: function(citys, isset){
	        var cityselect = $("#_jsdefaultcitylist");
	        var tpl = $("#citylist_select").html();
	        var html = Mustache.to_html(tpl, {
	            city_list: citys,
	            city_id: this.model.get('city_id')
	        });

	        if (isset) {
	            if (citys.length > 0) {
	                var curtag = citys[0];
	                this.model.set({
	                    cityid: curtag.city_id,
	                    cityname: curtag.city_name
	                });
	            } else {
	                this.model.set({
	                    cityid: null,
	                    cityname: null
	                });
	            }
	        }
	        cityselect.html(html);
		},
	    showCityList: function (e) {
	        if (!this.TAB_SHOW_CITYLIST) {
	            return;
	        }
			var listitem = $(".pl_rright .csearch_list");
			var ishidden = (listitem.css('display') == 'none') ? true : false;
			if (ishidden) {
				listitem.show();
			} else {
				listitem.hide();
			}
			e.stopPropagation();
		},
	    tabToNoteClip: function (e) {
	        var self = e.currentTarget;
	        $('.pl_rcity').hide();
	        $('.pl_rnote').show();
	        $('.pl_rtag li').removeClass('current');
	        $(self).addClass('current');
	        this.TAB_SHOW_CITYLIST = false;
	    },
	    tabToDefaultCity: function (e) {
			var self = e.currentTarget;
	        $('.pl_rnote').hide();
            $('.pl_rdemo').hide();
	        $('.pl_rcity').show();

            $('.pl_rright .nav-tabs li').removeClass('active');

            $(self).addClass('active');
	        this.TAB_SHOW_CITYLIST = true;
	    },
		//设置列表页面的默认城市id
		setDefaultCity: function(object) {
			if (!object.city_id || !object.city_name) {
				return false;
			}
			this.model.set({
				city_id: object.city_id,
				city_name: object.city_name,
				page: 1,
				nextto: true
			});
			Create.openScroll();
		},
	    addDefaultCity: function (tag) {
	        var itemtag = new City.Models.TripCityModel();
	        itemtag.set(tag);
	        this.collection.add(itemtag);
	    },
	    chgCityByList: function (e) {
	        this.chgDefaultCity(e);
	        $(e.currentTarget).addClass('select_current');
	        $(".csearch_list").hide();
	    },
	    chgDefaultCity: function (e) {
			var cityid = $(e.currentTarget).attr('data-cityid');
	        var cityname = $(e.currentTarget).attr('data-cityname');
	        this.setDefaultCity({
	            city_id: cityid,
	            city_name: cityname
	        });
	        this.unmakeSearch();
	        $("#_jsdefaultcitylist a").removeClass('select_current');
	        $(".csearch_list").hide();
	    },
	    unmakeSearch: function () {
	        $("._jssearchcitylist", this.$el).html('').parent().hide();
	        $("._jssearchcity", this.$el).val('');
	    },
		rightAddPoi: function(e) {
			window.app.colB.getView("xcd_poievent_list").addPOIEvent(e.currentTarget);
		},
		rightAddHotel: function(e) {
			window.app.colB.getView("xcd_poievent_list").addHotelEvent(e.currentTarget);
		},
		rightAddDemoTripDay: function(e) {
			$('.xcd_cnt_day *[name=title]').val($(e.currentTarget).attr('data-title')).focus();
			$('.xcd_cnt_day *[name=desc]').val($(e.currentTarget).attr('data-desc'));
			$('.xcd_cnt_day *[name=desc]').focus();
			$('.xcd_cnt_day *[name=traffic_note]').val($(e.currentTarget).attr('data-traffic_note'));
			$('.xcd_cnt_day *[name=traffic_note]').focus();
			$('.xcd_cnt_day *[name=traffic_note]').blur();
		},
		addRecommendHotel : function(e) {
			
			
		}
	});
});
