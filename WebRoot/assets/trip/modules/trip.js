define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');
	
	
	exports.Models = {};
	exports.Models.TripModel = Backbone.Model.extend({
		defaults : {
			start_time: '',
			city_id: '',
			xsort: 0
		}
		
	});

});
