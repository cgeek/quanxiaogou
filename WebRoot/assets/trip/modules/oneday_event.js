define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');
	
	exports.Models = {};
	exports.Models.OnedayeventModel = Backbone.Model.extend({
		defaults: {
			xid: 0,
			event_id: 0,
			hotel_id: 0,
			poi_id: 0,
			poi_name: '',
			title: '',
			desc: '',
			cover_image: '',
			type: '',
			address: "",
			city_id: "",
			city_name:"",
			phone: "",
			traffic: "",
			href: "",
			lat:0,
			lon:0,
			objectView: null,
			xsort: 0,
			ticketcount: 1,
			spend: 0,
			currency:"",
			note: ""
		},
		initialize: function(argv) {
			this.needattributes = ['xid', 'event_id', 'title', 'hotel_id', 'poi_id', 'poi_name', 'desc',
			 'cover_image', 'type', 'address', 'phone', 'href', 'lat', 'lon', 'city_id', 'city_name',
			 'spend', 'ticketcount', 'note', 'currency'];
		},
		model2JSON: function() {
			var ret = {};
			var needattributes = this.needattributes;
			var thiz = this;
			_(needattributes).each(function (k) {
				ret[k] = thiz.get(k);
			});
			return ret;
		},
	    editit: function (data, cb) {
	        var postdata = this.toJSON();
	        postdata = $.extend(postdata, data);
	        cb = cb || function (issucc, data) {};
	        var xid = postdata.xid;
	        if (xid <= 0) {
	            return;
	        }
	        var thiz = this;
	        var pdata = {};
	        $.each(this.needattributes, function (k, v) {
	            pdata[v] = postdata[v];
	        });
	        pdata.eventid = xid;
	        pdata.trip_id = window.app.getTripId();
	        pdata.onedayid = window.app.getCurrentOnedayModel().get("xid");
	        $(".ui_popup .ui_buttonB").addClass("ui_button_disabled").removeClass("ui_buttonB").attr("disabled", true);
			
	        var url = "/api/saveEvent";
	        $.post(url, pdata, function (result) {
	            $(".ui_popup .ui_button_disabled").removeClass("ui_button_disabled").addClass("ui_buttonB").attr("disabled", false);
	            if (result.code == 200) {
	                //pdata.coordinate = json.data.coordinate;
	                thiz.set(pdata, {
	                    silent: true
	                });
	                var oneday = window.app.getCurrentOnedayModel();
	                var poilist = oneday.get("poi_eventlist");
	            }
	            cb(true, result.data);
	        });
	    }
	
	});

	exports.Collections = {};
	exports.Collections.OnedayeventList = Backbone.Collection.extend({
		model: exports.Models.OnedayeventModel,
		traffics: {},
		initialize: function () {
			_.bindAll(this, "addEvent", "saveEvent");
			this.bind("add", this.addEvent);
			this.bind("remove", this.removeEvent);
		},
		addEvent:function (eventmodel, unknownxx, opts) {
			var that = this;
			var eventview = new exports.Views.OnedayeventView({model: eventmodel});
			eventmodel.set({"objectView":eventview});
			eventview.render();
		},
		removeEvent: function() {
		
		},
		saveEvent: function(eventmodel, callback) {
			var that = this;
			var eventdata = eventmodel.toJSON();
			var poi_id = eventdata.poi_id;
			var trip_id = window.app.getTripId();
			var trip_day_id = window.app.getCurrentOnedayModel().get("xid");
			
			var postdata = {};
//			alert('xtcn');
//			alert(eventdata.hotel_id);
//			alert(eventdata.poi_id);
			if(eventdata.type == 'hotel') {
				postdata = {
					trip_id: trip_id,
					trip_day_id: trip_day_id,
					type: eventdata.type,
					id: eventdata.hotel_id,
					title: eventdata.title,
					in_day: eventdata.inDay,
					out_day: eventdata.outDay,
					note: eventdata.note,
					spend: eventdata.spend,
					ticketcount: eventdata.ticketcount,
					currency: eventdata.currency
				}
			} else {
				postdata = {
					trip_id: trip_id,
					trip_day_id: trip_day_id,
					type: eventdata.type,
					id: eventdata.poi_id,
					note: eventdata.note,
					spend: eventdata.spend,
					ticketcount: eventdata.ticketcount,
					currency: eventdata.currency
				};
			}
			$.post("/api/addEvent", postdata, function (result) {
                callback(result);
			});
		},
		removeEvent: function (onedayevent) {
			this.remove(onedayevent);
			onedayevent.get("objectView").remove();
		},
		renderall: function (taglistdiv) {
			var that = this;
			this.each(function (v) {
				var view = v.get("objectView");
				//undelegateEvents and delegateEvents
				view.render();
			});
		},
		renderlist: function () {
			var models = this.models;
			_.each(models, function (v, k) {
				var view = v.get('objectView');
				view.render();
			});
		},
		resetForDragOrder: function(listClass, type){
			var list = $(listClass);
			var that = this;
			var size = $(list).size();
			$(list).each(function(k, v) {
				var cid = $(v).attr('data-xcid');
				var model = that.get(cid);
				model.set("xsort", size - k);
			});
			that.sort();
			if ("poi" == type) {
				$(listClass).find(".list_none_bg").remove();
				$("<div class='list_none_bg'></div>").appendTo($(listClass + ":first"));
				window.app.fixedEventLineCss(size);
				that.renderlist();
			}
			
		},
		comparator: function (a, b) {
			var v1 = a.get("xsort"),
			v2 = b.get("xsort");
			return v1 > v2 ? -1 : (v1 == v2 ? 0 : 1);
		}
											    
	});

	exports.Views = {};
	exports.Views.OnedayeventView = Backbone.View.extend({
		tagName: "div",
		events: {
			"click .jsplan_del": "plans_del_event",
			"click .jsplan_edit": "plans_edit_event",
			"click .jsplan_move li": "plans_move_event"
		},
		initialize: function(){
			_.bindAll(this, "destroyModel", "editEvent");
			this.bind("remove", this.destroyModel);

		},
		isPoi: function() {
			return this.model.get('type') != 'hotel';
		},
		isHotel: function(){
			return this.model.get('type') == 'hotel';
		},
		destroyModel: function () {
			this.model.destroy();
		},
		render: function(isedit) {
			var pdiv = {
				t1: $("#js_plan_activites"),
				t2: $("#js_plan_hotels")
			};
			var cid = this.model.cid;
			var itemtype = this.model.get("type");
			if(itemtype != 'hotel') {
				itemtype = 1;
			} else {
				itemtype = 2;
			}
			var key = "t" + itemtype;
			var eventdiv = pdiv[key];
			var tpl = $("#onedayevent-" + key).html();
			var dd = this.model.toJSON();
			html = Mustache.to_html(tpl, dd);
			$(this.$el).html(html);
			if ("t1" == key) {
				$(this.$el).addClass("day_list");
			} else if ("t2" == key) {
				$(this.$el).addClass("hotel_list");
			}
			$(this.$el).attr("data-xcid", cid);
			if (!isedit) {
				$(this.$el).appendTo($(eventdiv));
			}

			$(this.$el).hover(function () {
				$(this).addClass("hover");
			}, function () {
				$(this).removeClass("hover");
			});
			$(".p_list_seting").on('mouseover', function () {
				$(this).children("ul").show();
			}).on('mouseout', function () {
				$(this).children("ul").hide();
			});
		},
		plans_move_event: function (event) {
			var that = this;
			var trip_day_poi_id = $(event.currentTarget).attr('data-idx');
			var curoneday = window.app.getCurrentOnedayModel();
			console.log(trip_day_poi_id);
			
			
		},
	    editEvent: function (data, cb) {
	        var thiz = this;
	        cb = cb || function () {};
	        this.model.editit(data, function (issucc, json) {
	            if (issucc) {
	                var isedit = true;
	                thiz.render(isedit);
	                cb(json);
	            }
	        });
	    },
		plans_edit_event: function () {
			var xcdView = window.app.colB.getView("xcd_poievent_list");
	        if (this.isHotel()) {
	            var form = xcdView.getHotelForm();
	            form.render(true, this);
	        }
	        if (this.isPoi()) {
	            var form = xcdView.getPoiForm();
	            form.doEdit(this);
	        }
		},
		plans_del_event: function (opts) {
			var that = this;
			var onedayevent = this.model;

			var oneday = window.app.getCurrentOnedayModel();
			var poilist = oneday.get("poi_eventlist");
			poilist.removeEvent(onedayevent);
			
            var xid = onedayevent.get("xid");
            var type = onedayevent.get("type");
			
            var tirp_day_id = oneday.get("xid");
            var poilist = oneday.get("poi_eventlist");
            var hotellist = oneday.get("hotel_eventlist");
            var poimodel, poiview;
            var cb = function () {
                if (1 == type) {
                    poilist.removeEvent(onedayevent);
                } else if (2 == type) {
					hotellist.removeEvent(onedayevent);
				}
            };
            
			if (xid <= 0) {
                cb();
                return;
            }
            var postdata = {
                tirp_day_id: tirp_day_id,
                event_id: xid,
                type: type
            };
            $.post("/api/removeEvent", postdata, function (result) {
                if (result.code == 200) {
                    cb();
					/*
                    setTimeout(function () {
                        window.app.showCurDayMap();
                    }, 1000);
					*/
                } else {
					alert(result.message);
				}
            });
			
			
		}

	});
	
});
