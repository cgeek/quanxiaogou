define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');

	//需要city相关
	var City = require('./city');
	var OnedayEvent = require('./oneday_event');
	var xiaogouUI = require('../modules/ui').UI;
	
	exports.Models = {};
	exports.Models.TripOnedayModel = Backbone.Model.extend({
		defaults: {
			xid : 0,
			city_list: null,
			title :'',
			desc: '',
			traffic_note: '',
			poi_eventlist : null,
			hotel_eventlist: null,
			traffic_eventlist : null,
			objectView : null,
			image: '',
			image_url: '',
			xsort: 0
		},
		initialize: function(argv) {
			_.bindAll(this, "afterRemove");
			//焦点离开note的textarea的时候会触发
			this.bind('change:title', this.updateInfo);
			this.bind('change:desc', this.updateInfo);
			this.bind('change:traffic_note', this.updateInfo);
			
			this.bind('change:image', this.updateInfo);
			
			this.bind("remove", this.afterRemove);
			//初始化一天的要去的所有的城市列表
			var city_list = new City.Collections.TripCityList();
			this.set("city_list", city_list);
			
			//如果创建TripOneDayModel的时候，参数里面有city_list，则添加进去
			if(typeof argv != 'undefined' && typeof argv.city_list != 'undefined') {
				city_list.reset(argv.city_list);
			}
			
			//初始化一天里面的所有poi列表和hotel列表
			var poiEvents = new OnedayEvent.Collections.OnedayeventList();
			var hotelEvents = new OnedayEvent.Collections.OnedayeventList();
			this.set("poi_eventlist", poiEvents);
			this.set("hotel_eventlist", hotelEvents);
		},
		afterRemove: function () {
			this.get("objectView").remove();
			var data = {
				trip_id : window.app.getTripId(),
				trip_day_id : this.get('xid')
			};
			$.post('/api/deleteOneday', data, function(result) {
				
			});

		},
		model2JSON: function() {
			var result = {};
			var needattributes = ['desc', 'xid'];
			var that = this;
			_(needattributes).each(function(key){
				result[key] = that.get(key);
			});
			var city_list = this.get("city_list");
			if(city_list) {
				result['city_list'] = city_list.collection2JSON();
			} else {
				result['city_list'] = [];
			}
			//TODO poi list hotel list
			return result;
		},
		save2server : function(index, callback) {
			callback = callback || function () {};
			var that = this;
			var trip_id = window.app.getTripId();
			index += 1;
			var data = {
				'index' : index,
				'trip_id': trip_id
			};
			$.post("/api/addBlankOneday", data, function(result){
				if(result.code == 200) {
					that.set("xid", result.data.id);
					callback();
				} else {
					alert(result.message);
				}
			});
		},
	    updateInfo: function (model, data) {
            //console.log(data);
			var key = data.key;
			var value = data.value;
	        //if (model.previous(key) == value) return;
	        var trip_day_id = model.get("xid");
	        var postdata = {
	            trip_id: window.app.getTripId(),
	            trip_day_id: trip_day_id
	        };
			postdata[key] = value;

	        $.post("/api/updateOneday", postdata, function (result) {
				if(result.code == 200) {
					
				} else {
					alert(result.message);
				}
	        });
	    },
	    getOneCity: function (i) {
	        i = i ? i : 0;
	        var city_list = this.model2JSON().city_list;
	        return city_list[i];
	    }
	});
	
	exports.Views = {};
	exports.Views.TripOnedayView = Backbone.View.extend({
		tagName : "li",
		className : "xcd_list",
		desc_length: 58, //描述字数限制
		events: {
			"click .insert": "insert_oneday",
			"click .remove" : "remove_oneday",
			"click" : "switch2modeledit"
		},
		initialize: function() {
			_.bindAll(this, "render", "switch2modeledit", "insert_oneday", "add_city");
			this.model.get("city_list").bind("add remove change", this.render);
			
			this.model.bind("destroy", function () {
				$(this.$el).slideUp("slow", function () {
					if (window.app.colA.getView("xcd").allday.size() == 0) {
						window.app.colA.getView("xcd").click_addoneday();
					}
					Plan.fixresize();
				});
			}, this);
			this.model.bind("change", this.render);
		},
		render: function(){
			var tpl = $('#onedayView-tpl').html();
			var timedesc = this.model.get('timedesc');

            $('#js_xcd_layer_loading').show();

			var citylist = [];
			var count = _( this.model.get("city_list").models).size();
			for(var i = 0; i < count; i++ ) {
				var city_model = this.model.get("city_list").models[i];
                //console.log(city_model.get('city_name'));
				if(i ==0) {
					citylist.push({'city_name' :city_model.get('city_name'), 'first':true});
				} else {
					citylist.push({'city_name' :city_model.get('city_name')});
				}
			}

			var data = {
				city_list: citylist, 
				datedesc: this.model.get("datedesc") || "", 
				timedesc: timedesc, 
				notedesc: this.noteDesc()
			};
			if(citylist.length > 0) {
				data.has_city = true;
			}
			var html = Mustache.to_html(tpl, data);
			this.$el.attr('data-cid', this.model.cid);
			this.$el.html(html);
			$(this.$el).hover(function(){
				$(this).addClass("hover");
			}, function(){
				$(this).removeClass("hover");
			});
			$(this.$el).attr('title', "可以拖拽换序");
			return this.$el;
		},
		cityDesc: function () {
			var citylist = [];
			_(this.model.get("city_list").models).each(function(city_model) {
				citylist.push({'city_name' :city_model.get('city_name')});
			});
			return citylist;
		},
		noteDesc: function () {
			var desc = this.model.get("desc").data;
			desc = desc || "";
			if (null == desc) return "";
			if ('null' == desc) return "";
			return desc.gbtrim(this.desc_length, '...');
		},
	    add_city: function (city_model) {

	        var city_list = this.model.get("city_list");
	        city_list.addCity(city_model);
	    },
		insert_oneday: function(e) {
			window.app.colA.getView("xcd").insert_oneday(this.model);
		},
		remove_oneday: function(e) {
			if ((window.app.colA.getView("xcd").allday.size() <= 1)) {
				return ;
			}
			var that = this;
			var oneday = that.model;
			if(confirm("确定要删除该天行程吗？")) {
				window.app.colA.getView("xcd").allday.remove(oneday);
			}
			e.stopPropagation();
			return;
		},
		switch2modeledit: function() {
			var that = this;
			var model = this.model;
			var alldayView = window.app.colA.getView('xcd');
			var allday = alldayView.allday;
			this.addCurrent();
			
			$('#js_plan_note_btn').removeClass('current');
			
			//TODO 更新title note 等字段
			var oldcid = alldayView.current_onedaycid;
			if(oldcid) {
				var oldmodel = allday.get(oldcid);
				if(typeof oldmodel == "object") {
					oldmodel.get("objectView").unCurrent();

				}
			}
			alldayView.current_onedaycid = model.cid;
			model.get("objectView").addCurrent();
			
			window.app.colB.render("xcd_poievent_list", {oneday : model});
			
		},
		unCurrent: function() {
			this.$el.removeClass("current");
		},
		addCurrent: function() {
			this.$el.addClass("current");

		},
		fetchall_event: function (callback) {
			var that = this;
			callback = callback || function () {};
			var poilist = this.model.get("poi_eventlist");
			var hotellist = this.model.get("hotel_eventlist");
			var data = {
				"trip_id" : window.app.getTripId(),
				"trip_day_id" : this.model.get("xid")
			};
			$.post("/api/fetchAllEvents", data, function (result) {
				if(result.code != 200) {
					return ;
				};
				poilist.reset(null);
				hotellist.reset(null);
				that.clearView();
				var data = result.data;

                //显示为安排
                if(data.desc != '' || data.traffic_note != '' || data.poi_list.length > 0 || data.hotel_list.length > 0) {
                    $('#js_xcd_layer_loading').hide();
                } else {
                    $('#js_xcd_layer_loading').show();
                }

                if(data.desc) {
                    $(".xcd_note").find('.trip_day_desc').html('<strong>行程简介：</strong><br />' + data.desc);
                } else {
                    $(".xcd_note").find('.trip_day_desc').html('');
                }
                if(data.traffic_note) {
                    $(".xcd_note").find('.trip_day_traffic_note').html('<strong>交通说明：</strong>' +  data.traffic_note);
                } else {
                    $(".xcd_note").find('.trip_day_traffic_note').html('');
                }
				$(".xcd_title").find('input[name=title]').val(data.title);
				
				$(".trip_day_image_box ul").html('');
				if(data.images != 'undefined' && data.images.length > 0) {
					$.each(data.images, function(k, v) {
						$(".trip_day_image_box ul").append('<li image_hash="' + v.image_hash + '"><img src="' + v.image_url + '"><span id="js_remove_trip_day_image">x</span></li>');
					});
				}
				
				xiaogouUI.autoheight($("#js_plan_onedayDescTextarea"), {
					fontSize: "14px",
					lineHeight: "24px"
				});
				
				var pois = data.poi_list;
				if( typeof pois != 'undefined' && pois) {
					$.each(pois, function (k, v) {
						var eventmodel = new OnedayEvent.Models.OnedayeventModel(v);
						poilist.add(eventmodel);
					});
				}
				var hotels = data.hotel_list;
				if(hotels) {
					$.each(hotels, function(k, v) {
						var eventmodel = new OnedayEvent.Models.OnedayeventModel(v);
						//console.log(eventmodel.toJSON());
						hotellist.add(eventmodel);
					});
				}

			});
		},
		clearView: function () {
			$(".day_list").remove();
			$(".hotel_list").remove();
			$(".j_poi_traffic_info").remove();
		},
		renderall_citylist: function (citylistdiv, opts, cbfunc) {
			var citylist = this.model.get("city_list");
			citylist.renderAll(citylistdiv, opts);
			var cbfunc = cbfunc || function () {};
			cbfunc();
		},
		renderall_event: function () {
			var poilist = this.model.get("poi_eventlist");
			var hotellist = this.model.get("hotel_eventlist");
			
			poilist.renderall();
			hotellist.renderall();
			
			window.app.fixedEventLineCss(poilist.length);
		},
		addPOIEvent: function(eventmodel, callback){

			var poilist = this.model.get("poi_eventlist");
			//判断是否可以添加
			
			//判断是否已经添加过
			var poi_id = parseInt(eventmodel.get('poi_id'));
			if (poi_id > 0) {
				for (var i = 0; i < poilist.models.length; i++) {
					pid = parseInt(poilist.models[i].get('poi_id'));
					if (poi_id == pid) {
						alert('你已经添加过了，不能重复添加哦~');
						return ;
					}
				}
			}

			poilist.saveEvent(eventmodel, function(r){
                eventmodel.set('xid',r.data.xid);
                eventmodel.set('event_id',r.data.xid);

				poilist.push(eventmodel, {
					okcb: callback
				});
                callback && callback(r);
			});

		},
	    addHotelEvent: function (eventmodel, cb) {
	        var hotellist = this.model.get("hotel_eventlist");
	        hotellist.saveEvent(eventmodel, function () {
	            hotellist.push(eventmodel, {
	                okcb: cb
	            });
	        });
	    },
	    updateDayInfo: function (key, value) {	
			var data = {key: key, value: value};
	        this.model.set(key,data);
	    },
        updateDayInfos: function(data) {
            var trip_day_id = this.model.get("xid");
            var postdata = {
                trip_id: window.app.getTripId(),
                trip_day_id: trip_day_id
            };
            $.each(data, function (k, v) {
                postdata[k] = v;
            });

            $.post("/api/updateOneday", postdata, function (result) {
                if(result.code == 200) {

                } else {
                    alert(result.message);
                }
            });
        },
	    resetDragOrderForCitylist: function () {
	        var taglist = this.model.get("city_list");
	        if (!taglist) return;
	        taglist.resetForDragOrder();
	        this.render();
	    },
		resetDragOrderForPOIEvent: function() {
			var list = this.model.get("poi_eventlist");
			if (!list) return;

			list.resetForDragOrder(".day_list", "poi");
		},
	    fixXCDCityInputPosition: function () {
	        var inputele = $("#js_plan_addcity_btn").parent("div");
	        var nextele = $(inputele).next("span");
	        if (!$(nextele).hasClass("list_tag")) {
	            return;
	        }
	        var placelist = $("#js_plan_taglist");
	        var clearele = $(placelist).find(".clear");
	        var p = clearele.get(0).parentNode;
	        p.insertBefore(inputele.get(0), clearele.get(0));
	    }
	});
	
	exports.Collections = {};
	exports.Collections.TripAllDayList = Backbone.Collection.extend({
		model : exports.Models.TripOnedayModel,
		initialize : function(args) {},
		collection2JSON: function(){
			var result = [];
			this.each(function(onedayModel) {
				
				result.push(onedayModel.model2JSON());
			});
			return result;
		},
	    getAllCitys: function () {
	        var ret = this.collection2JSON();
	        if (!ret) return [];
	        var tags = [];
	        var uniqkeys = {};
	        $.each(ret, function (k, v) {
	            if (v) {
	                var tmpcitys = v.city_list;
	                if (tmpcitys) {
	                    $.each(tmpcitys, function (tagk, tagv) {
	                       	var uniqk = tagv.city_id + ":" + tagv.city_id;
							if (typeof uniqkeys[uniqk] == 'undefined') {
								tags.push(tagv);
								uniqkeys[uniqk] = 1;
							}
							
	                    });
	                }
	            }
	        });
	        return tags;
	    },
	    getAllCityList: function () {
	        var ret = this.collection2JSON();
	        if (!ret) return [];
	        var tags = [];
	        $.each(ret, function (k, v) {
	            if (v) {
	                var tmptag = v.city_list;
	                if (tmptag) {
	                    $.each(tmptag, function (tagk, tagv) {
	                        tags.push(tagv);
	                    });
	                }
	            }
	        });
	        return tags;
	    },
		sync_onedaylist2server: function () {
			//列表同步到服务器
			//var current_oneday = window.app.colA.getView('xcd').get_current_oneday();
			//console.log(current_oneday);
			var json = this.collection2JSON();
			var ids = [];
			$.each(json, function (k, v) {
				if (v.xid > 0) {
					ids.push(v.xid);
				}
			});
			var trip_id = window.app.getTripId();
			var data = {
				trip_id: trip_id,
				new_order: ids.join(",")
			};
			$.post("/api/resetOnedayOrder", data, function(result){
				//TODO
				if(result.code != 200) {
					alert(result.message);
				}
			});
		},
		resetForDragOrder:function() {

            //alert('reset drag order');
			var that = this;
			var list = $(".xcd_list");
			that.cidOrder = [];
			var size = $(list).size();
			$(list).each(function (key, value) {
				var cid = $(list[key]).attr("data-cid");
				that.get(cid).set("xsort", size - key);
			});

			that.sort();
			this.sync_onedaylist2server();
		},
		comparator: function (a, b) {
			var v1 = a.get("xsort"),
			v2 = b.get("xsort");
			return v1 > v2 ? -1 : (v1 == v2 ? 0 : 1);
		},
		renderall: function(){
		
		}
	});
	
});

