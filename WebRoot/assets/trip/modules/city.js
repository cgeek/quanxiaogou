define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');
	
	exports.Models = {};
	//计划中每日的城市model
	exports.Models.TripCityModel = Backbone.Model.extend({
		defaults : {
			city_name: '',
			city_id: '',
			xsort: 0
		},
		model2JSON: function(){
			return {
				city_name: this.get("city_name"), 
				city_id: this.get('city_id')
			};
		}
	});
	
	
	exports.Views = {};
	exports.Views.TripCityView = Backbone.View.extend({
		tagName: "span",
		className: "list_city",
		initialize: function() {
			_.bindAll(this, "removethis");
		},
		events: {
			"click .list_close": "removethis"
		},
		render: function(tagListDiv) {
			var cid = this.model.cid;
			var tpl = '{{city_name}}<span class="list_close" title="删除"></span>';
			var dd = this.model.toJSON();
			var html = Mustache.to_html(tpl, dd);
			this.$el.html(html);
			$(this.$el).attr("data-xcid", cid);
			$(this.$el).insertBefore($(tagListDiv));
		},
		removethis: function() {
			this.remove();
			this.model.destroy();
		}
	});
	
	exports.Collections = {};
	exports.Collections.TripCityList = Backbone.Collection.extend({
		model: exports.Models.TripCityModel,
		initialize: function(){
			this.cidOrder = [];
			_.bindAll(this, "sync_citys", "sync_citys2server", "renderAll");
			this.bind("add remove", this.sync_citys);
		},
		sync_citys: function() {

			window.app.sync_add_remove_citys_callback();
			this.sync_citys2server();
		},
		sync_citys2server : function(){
	        var curoneday = window.app.colA.getView("xcd").get_current_oneday();
	        var json = curoneday.model2JSON();
		    var city_list = json.city_list;
	        var trip_id = window.app.getTripId();
	        var trip_day_id = json.xid;
	        var postdata = {
	            trip_id: trip_id,
	            trip_day_id: trip_day_id,
	            city_list: city_list
	        };
			
			$.post('/api/updateOnedayCitys', postdata, function(result){
				if(result.code == 200) {
					
				} else {
					alert(result.message);
				}
			});
		},
		addCity: function(citymodel, opts) {
			if (!(citymodel instanceof exports.Models.TripCityModel)) {
	            citymodel = new exports.Models.TripCityModel(citymodel);
	        }
			var view = new exports.Views.TripCityView({
	            model: citymodel
	        });
	        citymodel.set({
	            objectView: view
	        });
	        view.render();
	        this.add(citymodel);
		},
		renderAll: function (taglistdiv) {
			$(".list_city").remove();
			this.each(function (tagmodel) {
				var v = new exports.Views.TripCityView({
					model: tagmodel
				});
				tagmodel.set({
					objectView: v
				});
				v.render(taglistdiv);
			});
		},
		collection2JSON: function() {
			if(this.size() <= 0) return null;
			var result = [];
			var that = this;
			var size = $(this.cidOrder).size();
			if(size <= 0) {
				this.each(function(model){
					if(null != model && typeof model == 'object') {
						result.push(model.model2JSON());
					}
				});
			} else {
				$(this.cidOrder).each(function (k, v) {
					var tagmodel = that.get(v);
					if (null != tagmodel && typeof tagmodel == 'object') {
						result.push(tagmodel.model2JSON());
					}
				});
			}

			return result;
		},
	    resetForDragOrder: function () {
	        var list = $(".list_city");
	        var thiz = this;
	        thiz.cidOrder = [];
	        var size = $(list).size();
	        $(list).each(function (k, v) {
	            var cid = $(v).attr("data-xcid");
	            thiz.get(cid).set("xsort", size - k, {
	                silent: true
	            });
	        });
	        thiz.sort();
	        this.sync_citys2server();
	    },
	    comparator: function (a, b) {
	        var v1 = a.get("xsort"),
	            v2 = b.get("xsort");
	        return v1 > v2 ? -1 : (v1 == v2 ? 0 : 1);
	    }
	});
	
	
	exports.Models.DefaultCityModel = Backbone.Model.extend({
	    defaults: {
	        cityid: null,
	        cityname: null,
	        tab: 1,
	        islock: false,
	        poicateid: 0,
	        page: 1,
	        nextto: true,
	        hotelstar: 0,
	        hotelorderway: 'grade',
	        tabmaplist: 'list',
	        searchkey: '',
	        searchrange: 1
	    },
	    initialize: function () {},
	    getHotelArgs: function () {
	        return {
	            tab: this.get('tab'),
	            cityid: this.get('cityid'),
	            cityname: this.get('cityname'),
	            hotelstar: this.get('hotelstar'),
	            hotelorderway: this.get('hotelorderway')
	        };
	    },
	    getPoiArgs: function () {
	        return {
	            tab: this.get('tab'),
	            cityid: this.get('cityid'),
	            cityname: this.get('cityname'),
	            poicateid: this.get('poicateid')
	        };
	    },
	    getTab: function () {
	        return this.get('tab');
	    },
	    BIND_EVENT: true,
	    stopBind: function () {
	        this.BIND_EVENT = false;
	    },
	    beginBind: function () {
	        this.BIND_EVENT = true;
	    },
	    chkBind: function () {
	        return this.BIND_EVENT;
	    }
	});
	
	exports.Collections.DefaultCityList = Backbone.Collection.extend({
	    model: exports.Models.TripCityModel
	});;
	
});
