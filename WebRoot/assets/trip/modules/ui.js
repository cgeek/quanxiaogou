define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');

		
	var xiaogouUI = {
 	   version: "0.1"
	};
	
	var popup = {};
    var initial = true;
    var pupcode = '<div class="ui_popup_bg"><div class="ui_popup"><p class="ui_popup_close" title="关闭"></p><div class="ui_popup_main"></div></div></div>';

    function pupClose() {
        $(".ui_popup_bg").fadeOut(300);
    }
    popup.pupclose = pupClose;

    function pupStart(width, isclose) {
        if (initial) {
            $("body").append(pupcode);
            $(".ui_popup_close").on("click", function () {
                pupClose();
            });
        }
        initial = false;
        width = parseInt(width, 10);
        $(".ui_popup_bg").css({
            "height": $(document).height()
        }).stop(true, true).fadeIn(300);
        $(".ui_popup").css({
            "width": width,
            "top": $(document).scrollTop()
        });
        $(".ui_popup_main").text("");
        $(".ui_popup_close").hide();
        if (isclose == "show") {
            $(".ui_popup_close").show();
        }
    }
    popup.start = pupStart;

    function pupFinish() {
        var winH = $(window).height();
        var boxH = $(".ui_popup").height();
        var boxT = (winH - boxH) / 2;
        boxT = (boxT < 20) ? 20 : boxT;
        $(".ui_popup").css("margin-top", boxT);
    }
    popup.ajax = function (obj) {
        if (typeof obj == "object") {
            var url = obj.url;
            var width = obj.width || 500;
            var isclose = obj.isclose || "show";
            var callback = obj.callback || false;
        }
        pupStart(width, isclose);
        $.get(url, function (data) {
            $(".ui_popup_main").html(data);
            if (typeof callback == 'function') {
                callback();
            }
            pupFinish();
        });
    }
    popup.show = function (obj) {
        if (typeof obj == "object") {
            var id = obj.id;
            var width = obj.width || 500;
            var isclose = obj.isclose || "show";
        }
        pupStart(width, isclose);
        var idtext = $("#" + id).html();
        $(".ui_popup_main").html(idtext);
        pupFinish();
    }
    popup.showHtml = function (obj) {
        if (typeof obj == "object") {
            var html = obj.html;
            var width = obj.width || 500;
            var isclose = obj.isclose || "show";
        }
        pupStart(width, isclose);
        $(".ui_popup_main").html(html);
        pupFinish();
    };
    xiaogouUI.popup = popup;
	
	
    var isfirst = true;

    function autoheight(target, css) {
        if (isfirst) {
            $("body").append("<textarea id='js_autoheight_clone'></textarea>");
            isfirst = false;
        }
        var clone = $("#js_autoheight_clone");
        clone.width($(target).width());
        clone.val($(target).val());
        if (typeof css) {
            if (typeof css.fontSize) {
                clone.css("font-size", css.fontSize);
            }
            if (typeof css.lineHeight) {
                clone.css("line-height", css.lineHeight);
            }
        }
        setTimeout(function () {
            var height = clone.get(0).scrollHeight;
            $(target).height(height);
        }, 1);
    }
    xiaogouUI.autoheight = autoheight;


	exports.UI = xiaogouUI;
});