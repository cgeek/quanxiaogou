define(function(require,exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');

	var Common_Style = function () {
	    this.init.apply(this, arguments);
	};
	Common_Style.prototype.init = function () {
	    this.bindEvent();
	};
	Common_Style.prototype.bindEvent = function () {
	    var self = this;
		$(document).delegate('.tra_style_select .titles', 'click', function(e) {
	        var gclass = $(this).parent(".tra_style_select").attr("class");
	        if (/_jsselect/.test(gclass) == true) {
	            return;
	        }
	        if ($(this).next(".contents").is(":hidden")) {
	            $(".tra_select").find(".contents").hide();
	            $(".tra_select").removeClass("pr");
	            $(this).parents(".tra_style_select").addClass("pr");
	            $(this).next(".contents").show();
	        } else {
	            $(this).parents(".tra_style_select").removeClass("pr");
	            $(this).next(".contents").hide();
	        }
	        e.stopPropagation();
		});
		
		$(document).delegate('.tra_style_select .contents li a', 'click', function(e){
	        var gclass = $(this).parent(".tra_style_select").attr("class");
	        if (/_jsselect/.test(gclass) == true) {
	            return;
	        }
	        var selectTitle = $(this).text();
	        var selectClass = $(this).attr("class");
	        $(".tra_style_select .titles span").text(selectTitle);
	        $(".tra_style_select .titles span").attr("class", selectClass);
	        $(".tra_style_select .contents").hide();
	        self.selectAssign(this);
	    });
		$(document).delegate('.tra_select .titles', 'click', function(e){
	        var thiz = this;
	        var goselect = function () {
	            var gclass = $(thiz).parent(".tra_select").attr("class");
	            if (/_jsselect/.test(gclass) == true) {
	                return;
	            }
	            if ($(thiz).next(".contents").is(":hidden")) {
	                $(".tra_select").find(".contents").hide();
	                $(".tra_style_select").find(".contents").hide();
	                $(".tra_select").removeClass("pr");
	                $(thiz).parents(".tra_select").addClass("pr");
	                var tName = $(thiz).parent("div").prev().attr('name');
	                var inDayIsFirst = false;
	                var outDayIsFirst = false;
	                var inDayInput = $('input[name=inDay]');
	                var outDayInput = $('input[name=outDay]');
	                var theInDayEl = inDayInput.next().find('.contents').find('a[data-value="' + inDayInput.val() + '"]');
	                var theOutDayEl = outDayInput.next().find('.contents').find('a[data-value="' + outDayInput.val() + '"]');
	                if (theInDayEl[0] == inDayInput.next().find('.contents').find('li:first').find('a')[0])
	                    inDayIsFirst = true;
	                if (theOutDayEl[0] == outDayInput.next().find('.contents').find('li:first').find('a')[0])
	                    outDayIsFirst = true;
	                if ('inDay' == tName) {
	                    if (outDayInput.length >= 1 && parseInt(outDayInput.val()) > 0) {
	                        var inLi = $(thiz).next('.contents').find('li a');
	                        var isHide = false;
	                        for (var i = 0; i < inLi.length; i++) {
	                            var o = $(inLi[i]);
	                            if (o.attr('data-value') == outDayInput.val() && !outDayIsFirst) {
	                                isHide = true;
	                            }
	                            if (isHide)
	                                o.hide();
	                            else
	                                o.show();
	                        }
	                    }
	                } else if ('outDay' == tName) {
	                    if (inDayInput.length >= 1 && parseInt(inDayInput.val()) > 0) {
	                        var inLi = $(thiz).next('.contents').find('li a');
	                        var inDay = inDayInput.val();
	                        var isHide = (inDay != undefined && parseInt(inDay) > 0 && !inDayIsFirst) ? true : false;
	                        for (var i = 0; i < inLi.length; i++) {
	                            var o = $(inLi[i]);
	                            if (isHide)
	                                o.hide();
	                            else
	                                o.show(); if (o.attr('data-value') == inDayInput.val())
	                                isHide = false;
	                        }
	                    }
	                }
	                $(thiz).next(".contents").show();
	            } else {
	                $(thiz).parents(".tra_select").removeClass("pr");
	                $(thiz).next(".contents").hide();
	            }
	        }
	        setTimeout(goselect, 200);
	        e.stopPropagation();
	    });
		$(document).delegate('.tra_select .contents li a', 'click', function(e){
	        var gclass = $(this).parent(".tra_select").attr("class");
	        if (/_jsselect/.test(gclass) == true) {
	            return;
	        }
	        var selectTitles = $(this).text();
	        $(this).parents(".tra_select").find(".titles span").text(selectTitles);
	        $(this).parents(".tra_select").find(".contents").hide();
	        self.selectAssign(this);
	    });
	    $(document).click(function () {
	        self.closeSelectItem();
	    });
	    $("input, textarea").on('focus', function () {
	        self.clearErrorLayer(this);
	    });
	};
	Common_Style.prototype.closeSelectItem = function () {
	    $(".tra_style_select .contents, .tra_select .contents").hide();
	}, Common_Style.prototype.selectAssign = function (thiz) {
	    if (typeof thiz == 'undefined')
	        return false;
	    var val = $(thiz).attr("data-value");
	    $(thiz).parents(".contents").parent("div").prev().val(val);
	};
	Common_Style.prototype.showErrorLayer = function (thiz, msg, unlayer, type) {
	    this.clearErrorLayer(thiz);
	    type = type || 'textarea';
	    $(thiz).addClass('ui2_error')
	    if (type == 'textarea') {
	        var layer = '<div class="clearfix pr _jserrorlayer">';
	        layer += '<div class="ui2_error_layer">';
	        layer += '<p class="ui2_error_layer_arrow"></p>';
	        layer += '<p class="ui2_error_layer_arrow2"></p>';
	        layer += '<p class="ui2_error_layer_text">' + msg + '</p>';
	        layer += '</div>';
	        layer += '</div>';
	    } else if (type == 'input') {
	        var layer = '<div class="btm clearfix _jserrorlayer">';
	        layer += ' <div style="" class="ui2_error_layer">';
	        layer += ' <p class="ui2_error_layer_arrow"></p>';
	        layer += ' <p class="ui2_error_layer_arrow2"></p>';
	        layer += ' <p class="ui2_error_layer_text">' + msg + '</p>';
	        layer += '</div>';
	    }
	    if (!unlayer) $(thiz).after(layer);
	};
	Common_Style.prototype.getVal = function (item) {
	    if (!item) return '';
	    var placeholder = $(item).attr('placeholder');
	    var val = $(item).val().replace(/(^\s+)|(\s+$)/, '');
	    if (placeholder) {
	        var itemmatch = new RegExp(item.attr('placeholder'), 'i');
	        val = val.replace(itemmatch, '');
	    }
	    return val;
	};
	Common_Style.prototype.clearErrorLayer = function (thiz) {
	    $(thiz).removeClass('ui2_error').nextAll('._jserrorlayer').remove();
	};
	Common_Style.prototype.toClearErrorLayer = function (e) {
	    this.clearErrorLayer(e.currentTarget);
	};
	
	Common_Style.prototype.showFontTip = function (thiz, max) {

	    var tip = $($(thiz).attr('rel-tip'));
	    var val = $(thiz).val();
	    var tlen = util.getwordlen(val);
	    var maxlen = max || 200;
	    if (tlen > (maxlen * 2)) {
	        tip.html('<span class="sum" style="color:red">' + Math.ceil(tlen / 2) + '</span> / ' + maxlen + ' 字');
	        $(thiz).addClass("ui2_error");
	    } else {
	        tip.html('<span class="sum">' + Math.ceil(tlen / 2) + '</span> / ' + maxlen + ' 字');
	        $(thiz).removeClass("ui2_error");
	    }
	};
	
	exports.commonStyle = new Common_Style();
	
});
