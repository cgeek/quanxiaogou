define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');

		
	exports.Views = exports.Models = exports.Collections = {};
	
	exports.Models.NoteClipConfig = Backbone.Model.extend({
		defaults: {
			type: 0,
			identify: 0,
        	position: 'append'
    	}
	});
	exports.Models.NoteClipItemModel = Backbone.Model.extend({
		defaults: {
        	id: null,
			title: null,
			content: null,
			html: null
		}
	});
	exports.Collections.NoteClipCollection = Backbone.Collection.extend({
		model: exports.Models.NoteClipItemModel
	});
	
	exports.Views.NoteClipList = Backbone.View.extend({
	    el: '.pl_rright',
	    elmore: '._jsmorenoteclip',
	    elpager: '#_jsnoteclippager',
	    elload: '._jsmorenoteclipload',
	    ellist: '#_jsnotecliplist',
	    BACKWORD: '您还没有保存内容，确定要返回吗？',
	    submitlock: 'false',
	    initialize: function () {
	        _.bindAll(this, "getNoteClip", 'minusNoteClip');
	        this.curview = null;
	        this.collection = new exports.Collections.NoteClipCollection();
	        this.collection.on('add', this.appendNoteClipItem);
	        this.collection.on('remove', this.minusNoteClip);
	        this.model.on('change:type', this.buildNoteClip);
	    },
	    events: {
	        'click ._jstabnoteclip': 'tabToNoteClip',
			'click ._jsrightaddnote' : 'appendNote',
	        'click #leftrrr .dropdown dd a': 'chgType',
	        'click ._jsmorenoteclip': 'conMoreNoteClip',
	        'click #_jsaddnotebtn': 'noteAddForm',
	        'click #_jsnoteclipeditbtn': 'editNoteClip',
	        'click #_jsnoteclipeditback': 'confirmEdit',
	        'click #_jsnoteclipaddbtn': 'addNote',
	        'click #_jsnoteclipaddback': 'confirmAddNote',
	        'click #_jscliphelpbtn': 'showcliphelp'
	    },
		appendNote: function(e) {
			var title = $(e.target).attr('data-title');
			var html = $(e.target).attr('data-content');
			var note = window.app.contentRTE.tripNoteText.get_content();
			var new_html = note + '<h4>'+ title +'</h4>' + html;
			window.app.contentRTE.tripNoteText.set_content(new_html);			
		},
	    tabToNoteClip: function () {
			window.app.noteclip.buildNoteClip(this.model);
	    },
	    buildNoteClip: function (model) {
	        model.set('identify', 0);
	        window.app.noteclip.collection.reset();
	        $(window.app.noteclip.ellist, window.app.noteclip.$el).html('');
	        
			window.app.noteclip.getNoteClip();
	    },
	    getNoteClip: function () {
			if($('#js_plan_prepare').css('display') == 'none') {
				//alert('请先切换到小贴士编辑模式');
				return false;
			}
			
	        var planid = window.app.getTripId();
	        var type = this.model.get('type');
	        var offset = this.model.get('identify');
	        $("#_jsnoteclip").addClass('pl_loadcity');
	        $.post("/api/noteList", {
	            action: 'notecliplist',
	            type: type,
	            offset: offset
	        }, function (result) {
				if(result.code != 200) {
					alert('失败:' + result.message);
					return false;
				}
	            /*
				if (window.app.oper_tips(json)) {
	                window.noteclip.cbTabNoteClip(json);
	            } else {
	                window.noteclip.voidNoteClip(json);
	            }*/
				window.app.noteclip.cbTabNoteClip(result.data);
				
	        });
	    },
	    cbTabNoteClip: function (data) {
	        var tpl = $('#noteItemView-tpl').html();
			var html = Mustache.to_html(tpl, data);

			$(window.app.noteclip.ellist).append(html);
			
	        $("#_jsnoteclip").removeClass('pl_loadcity');
	    },
	    minusNoteClip: function () {
	        var identify = Plan.noteclip.model.get('identify');
	        this.model.set('identify', identify - 1);
	        if (this.collection.models.length < 1) {
	            this.voidNoteClip();
	        }
	    },
	    getNoteClipItem: function (data, html) {
	        var itemmodel = new exports.Models.NoteClipItemModel();
	        var infoobj = this.getInfoObj(data, html);
	        itemmodel.set(infoobj);
	        return itemmodel;
	    },
	    addNoteClipItem: function (data, html) {
	        this.model.set('position', 'append');
	        this.collection.add(this.getNoteClipItem(data, html));
	    },
	    unshiftNoteClipItem: function (data, html) {
	        this.model.set('position', 'prepend');
	        this.collection.unshift(this.getNoteClipItem(data, html));
	    },
	    conMoreNoteClip: function () {
	        Plan.noteclip.moreNoteClip();
	    },
	    moreNoteClip: function () {
	        $(this.elload, this.$el).show();
	        $(this.elmore, this.$el).hide();
	        this.getNoteClip();
	    },
	    
	    voidNoteClip: function () {
	        $("#_jsnoteclip").addClass('pl_rnote_bg');
	        $("#_jsnoteclip").removeClass('pl_loadcity');
	    },
	    getInfoObj: function (info, html) {
	        return {
	            id: info.id,
	            plan_id: info.plan_id,
	            title: info.title,
	            link: info.link,
	            note: info.note,
	            ctime: info.ctime,
	            utime: info.utime,
	            isnote: info.isnote,
	            html: html
	        };
	    },
	    appendNoteClipItem: function (item) {
	        var noteclipview = new exports.Views.NoteClipView({
	            model: item
	        });
	        var p = window.app.noteclip.model.get('position');
	        var html = noteclipview.render().el;
	        if ('append' == p) {
	            $(window.app.noteclip.ellist).append(html);
	        } else if ('prepend' == p) {
	            $(Plan.noteclip.ellist).prepend(html);
	        }
	        $("#_jsnoteclip").removeClass('pl_rnote_bg');
	    },
	    chgType: function (e) {
	        var val = parseInt($(e.currentTarget).parents('.dropdown').find(".value").html());
	        this.model.set('type', val);
	    },
	    noteAddForm: function (e) {
	        this.showNoteClipForm(0);
	    },
	    showNoteClipForm: function (id) {
	        if (!islogined()) {
	            planner.ajaxlogin();
	            return;
	        }
	        var plan_id = planner.getPlannerId();
	        $.post("/planapi.php", {
	            action: 'noteclipform',
	            id: id,
	            plan_id: plan_id
	        }, function (json) {
	            json = $.parseJSON(json);
	            if (Plan.app.oper_tips(json)) {
	                $("#rightrrr")[0].innerHTML = json.data.html;
	                $("#rightrrr")[0].style.display = 'block';
	                $("#leftrrr")[0].style.display = 'none';
	                qyerUI.autoheight($('.js_autoheight'), {
	                    fontSize: "14px",
	                    lineHeight: "24px"
	                });
	                qyerUI.placeholder();
	            }
	        });
	    },
	    registerCurView: function (obj) {
	        this.curview = obj;
	    },
	    destroyCurView: function () {
	        this.curview = null;
	    },
	    confirmEdit: function (e) {
	        var self = this;
	        tips.confirm(e, function () {
	            self.showList();
	        }, self.BACKWORD);
	    },
	    editNoteClip: function (e) {
	        var self = this;
	        var objform = $(e.currentTarget).parents('form');
	        if (data = this.chkform(objform)) {
	            $.post('/planapi.php?action=editnoteclip', data, function (json) {
	                json = $.parseJSON(json);
	                if (Plan.app.oper_tips(json)) {
	                    Plan.noteclip.cbEditNoteClip(json);
	                    self.showList();
	                }
	            });
	        }
	    },
	    confirmAddNote: function (e) {
	        var self = this;
	        var objform = $(e.currentTarget).parents('form');
	        var data = this.getTitleNote(objform);
	        if (data.title == '' && data.note == '') {
	            self.showList();
	            return;
	        }
	        tips.confirm(e, function () {
	            self.showList();
	        }, self.BACKWORD);
	    },
	    addNote: function (e) {
	        var objform = $(e.currentTarget).parents('form');
	        var self = this;
	        if (typeof objform.data('submitlock') != 'undefined') return false;
	        objform.data('submitlock', 1);
	        if (data = this.chkform(objform)) {
	            $.post('/planapi.php?action=addnote', data, function (json) {
	                json = $.parseJSON(json);
	                if (Plan.app.oper_tips(json)) {
	                    Plan.noteclip.cbAddNote(json);
	                    self.showList();
	                }
	            });
	        }
	    },
	    cbEditNoteClip: function (json) {
	        if (!json.data.info || !json.data.html || null == this.curview) {
	            return;
	        }
	        var infoobj = this.getInfoObj(json.data.info, json.data.html);
	        this.curview.model.set(infoobj);
	        this.destroyCurView();
	    },
	    cbAddNote: function (json) {
	        if (!json.data.info || !json.data.html) {
	            return;
	        }
	        var identify = this.model.get('identify');
	        this.model.set('identify', identify + 1);
	        if (this.model.get('type') == 2) {
	            this.model.set('type', 0);
	        }
	        this.unshiftNoteClipItem(json.data.info, json.data.html);
	    },
	    chkform: function (objform) {
	        var objid = $("input[name='id']", objform);
	        var objtitle = $("input[name='title']", objform);
	        var objnote = $("textarea[name='note']", objform);
	        var data = {};
	        data.routeid = planner.getPlannerId();
	        var tmatch = new RegExp(objtitle.attr('placeholder'), 'i');
	        data.title = objtitle.val().replace(/(^\s+)|(\s+$)/, '').replace(tmatch, '');
	        data.id = objid.val();
	        var nmatch = new RegExp(objnote.attr('placeholder'), 'i');
	        data.note = objnote.val().replace(/(^\s+)|(\s+$)/, '').replace(nmatch, '');
	        if (data.title == '') {
	            qyerUI.message({
	                msg: "标题不能为空！",
	                type: 'warn'
	            });
	            return '';
	        }
	        if (data.note == '') {
	            qyerUI.message({
	                msg: "正文不能为空！",
	                type: "warn"
	            });
	            return false;
	        }
	        var tlen = util.getwordlen(data.title);
	        var tmax = 32;
	        if (tlen > (tmax * 2)) {
	            qyerUI.message({
	                msg: "标题最多" + tmax + "个汉字！",
	                type: "warn"
	            });
	            data.title = data.title.gbtrim(tmax * 2);
	            return false;
	        }
	        var len = 1000;
	        var clen = util.getwordlen(data.note);
	        if (clen > len * 2) {
	            qyerUI.message({
	                msg: "正文最多" + len + "个汉字！",
	                type: "warn"
	            });
	            data.note = data.note.gbtrim(len * 2);
	            return false;
	        }
	        return data;
	    },
	    getTitleNote: function (objform) {
	        var data = {};
	        var objtitle = $("input[name='title']", objform);
	        var objnote = $("textarea[name='note']", objform);
	        var tmatch = new RegExp(objtitle.attr('placeholder'), 'i');
	        data.title = objtitle.val().replace(/(^\s+)|(\s+$)/, '').replace(tmatch, '');
	        var nmatch = new RegExp(objnote.attr('placeholder'), 'i');
	        data.note = objnote.val().replace(/(^\s+)|(\s+$)/, '').replace(nmatch, '');
	        return data;
	    },
	    showcliphelp: function () {
	        popup.show('_jscliphelptips', 640);
	    },
	    showList: function () {
	        $('#leftrrr').show();
	        $('#rightrrr').hide();
	    }
	});
	
	exports.Views.NoteClipView = Backbone.View.extend({
	    tagName: 'div',
	    initialize: function () {
	        _.bindAll(this, 'render', 'delnoteclip', 'removeitem', 'showEditForm');
	        this.model.on('remove', this.removeitem);
	        this.model.on('change', this.render);
	    },
	    events: {
	        "click .pl_rnote_list": "showEditForm",
	        "click .delete": "delnoteclip"
	    },
	    render: function () {
	        $(this.el).html(this.model.get('html'));
	        return this;
	    },
	    delnoteclip: function (e) {
	        var model = this.model;
	        var id = $(e.currentTarget).parents('.pl_rnote_list').attr('data-id');
	        var routeid = planner.getPlannerId();
	        tips.confirm(e, function () {
	            Plan.noteclip.collection.remove(model);
	            $.post("/planapi.php?action=delclip", {
	                id: id,
	                routeid: routeid
	            }, function (json) {
	                json = $.parseJSON(json);
	                if (Plan.app.oper_tips(json)) {
	                    Plan.noteclip.collection.remove(model);
	                }
	            });
	        });
	        e.stopPropagation();
	    },
	    showEditForm: function (e) {
	        var id = $(e.currentTarget).attr('data-id');
	        Plan.noteclip.showNoteClipForm(id);
	        Plan.noteclip.registerCurView(this);
	    },
	    removeitem: function () {
	        $(this.$el).remove();
	    }
	});;
	
	
});
