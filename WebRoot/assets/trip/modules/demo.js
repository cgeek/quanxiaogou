define(function(require, exports, module){
    var $ = require('jquery'),
        _ = require('underscore'),
        Backbone = require('backbone'),
        Mustache = require('mustache');

    var Create = require('create').Create;
    var City = require('./city');

    exports.Models = {};
    exports.Models.DefaultCityModel = Backbone.Model.extend({
        defaults: {
            city_id: null,
            city_name: null,
            tab: 1,
            poi_type: 'sight',
            page: 0,
            nextto: true,
            tabmaplist: 'list',
            searchkey: ''
        },
        initialize: function () {}
    });

    exports.Views = {};
    exports.Views.DemoListView = Backbone.View.extend({
        el: ".pl_rright",
        demoContainer: '.pl_demo_cnt',
        events: {
            "click ._jsTabDemoList": "tabDemoList",
            "click ._jsrightadd_demotripday": "rightAddDemoTripDay" //添加经典行程
        },
        initialize: function(){
            _.bindAll(this, 'render', 'getDemoList', 'rightAddDemoTripDay');
            this.collection = new City.Collections.DefaultCityList();
            //this.render();
        },
        render: function(){
            var that = this;
            that.toLoading();
            that.searchDemoList(that.model);


            Create.scroll(30, function (cb) {
                that.toLoading();
                that.searchDemoList(that.model, cb);

            }, $(that.demoContainer)[0]);

        },
        //设置列表页面的默认城市id
        setDefaultCity: function(object) {

            if (!object.city_id || !object.city_name) {
                return false;
            }
            this.model.set({
                city_id: object.city_id,
                city_name: object.city_name,
                page: 1,
                nextto: true
            });
            Create.openScroll();
        },
        initModel: function () {
            this.model.set({
                page: 1,
                nextto: true
            });
            Create.openScroll();
        },
        getDemoList: function(model, argv) {
            this.toLoading();
            $('#_jssearchkey').val('');
            $('#_jssearchclose').hide();
            model.set({
                searchkey: '',
                searchrange: 1
            });
            this.searchPoiList(model);
        },
        toLoading: function(){
            $("._jsLoadMoreDemo").show();
        },
        removeLoading: function(){
            $("._jsLoadMoreDemo").hide();
        },
        searchDemoList: function(model, callback) {

            if (!model.get('nextto')) {
                return false;
            }

            var that = this;
            var data = {
                'city_id' : this.model.get('city_id'),
                'page' : this.model.get('page')
            };

            $.get("/api/demoList", data, function(result) {
                if(result.code == 200) {
                    that.renderSearchDemoList(result, callback);
                }
            });

        },
        renderSearchDemoList: function(result, cb) {

            var poiitem = $("#_jsDemoList", this.$el);
            var page = this.model.get('page');
            var demoContainer = $(this.demoContainer, this.$el);
            var cityname = this.model.get('city_name');

            var tpl = tpl = $('#demoTripDayView-tpl').html();

            var html = Mustache.to_html(tpl, result.data);
            if(page > 1 ) {
                poiitem.append(html);
            } else {
                poiitem.html(html);
            }
            demoContainer.show();


            if (result.data ) {
                if(result.data.demo_trip_day_list && result.data.demo_trip_day_list.length > 0) {
                    if(cb) cb();
                    this.model.set('nextto', true);
                    $(".pl_rdemo", this.$el).removeClass('pl_rdemo_bg');
                } else {
                    this.model.set('nextto', false);
                }
            } else {
                this.model.set('nextto', false);
                if (page == 1) {
                    if (this.model.get('searchkey') == '') {
                        $(".pl_rdemo", this.$el).addClass('pl_rdemo_bg');
                    }
                }
            }

            this.removeLoading();
            this.model.set('page', page + 1);

            this.model.set('nextto', true);
            window.app.windowResize();
        },
        tabDemoList: function(e) {

            var self = e.currentTarget;

            $('.pl_rright .nav-tabs li').removeClass('active');

            $('.pl_rcity').hide();
            $('.pl_rdemo').show();

            $(self).addClass('active');
            this.TAB_SHOW_DEMOLIST = true;
            this.render();
        },
        unmakeSearch: function () {
            $("._jssearchcitylist", this.$el).html('').parent().hide();
            $("._jssearchcity", this.$el).val('');
        },
        rightAddDemoTripDay: function(e) {

            var trip_day_id = window.app.getCurrentOnedayModel().get('xid');
            var demo_id = $(e.currentTarget).attr('data-demo_id');

            $.post('/api/updateTripDayFromDemo', {trip_day_id:trip_day_id, demo_id: demo_id}, function(r){
                $('.trip_day_desc').html('<strong>行程简介：</strong><br />' + $(e.currentTarget).attr('data-desc'));
                $('.trip_day_traffic_note').html('<strong>交通说明：</strong>' + $(e.currentTarget).attr('data-traffic_note'));
                $('.xcd_title input[name=title]').val($(e.currentTarget).attr('data-title'));
            });




        },
        addRecommendHotel : function(e) {


        }
    });
});
