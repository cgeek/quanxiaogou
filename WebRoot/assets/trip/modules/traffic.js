define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');

	var xiaogouUI = require('./ui.js').UI;
	var autoThink = require('autoThink').autoThink;
	var commonStyle = require('./style.js').commonStyle;
	
	exports.Views = exports.Models = exports.Collections = {};

	exports.Models.OnedayTrafficModel = Backbone.Model.extend({
		    defaults: {
		        xid: null,
				title: null,
		        from_place: null,
		        to_place: null,
		        from_placeid: null,
				to_placeid: null,
		        days: null,
				mode: 1,
                mode_icon:null,
		        traffic_number: null,
				spend: null,
				start_hours: null,
				start_minutes: null,
				end_hours: null,
				end_minutes: null,
				currency: null,
		        ticketcount: null,
				note: null,
				readeronly: true
		    },
		    initialize: function () {},
			model2JSON: function() {
				var result = {};
				var needattributes = ['xid', 'title','from_place', 'to_place', 'days', 'traffic_number',
				 'ticketcount', 'note', 'start_hours', 'start_minutes', 'end_hours', 'end_minutes', 'spend', 'currency', 'mode', 'mode_icon'];
				var that = this;
				_(needattributes).each(function(key){
                    console.log(that.get(key))
					result[key] = that.get(key);
				});
				return result;
			},
		    addTraffic: function (cb) {
		        var cb = cb || function () {};
		        if (this.get('renderonly')) {
		            cb();
		            return;
		        }
		        var data = {};
		        data.trip_id = window.app.getTripId();
		        data.trip_day_id = window.app.getCurrentOnedayModel().get("xid");
                data = $.extend(data, this.attributes);
                console.log(data);
                data.mode_icon = this.getTripmodeIcon(data.mode);
                //data.xid = window.app.getCurrentOnedayModel().get("xid");
                delete data.id;
		        $(".ui_popup .ui_buttonB").addClass("ui_button_disabled").removeClass("ui_buttonB").attr("disabled", true);
		        var self = this;
		        $.post("/api/saveTraffic", data, function (json) {
		            $(".ui_popup .ui_button_disabled").removeClass("ui_button_disabled").addClass("ui_buttonB").attr("disabled", false);

					if(json.code == 200) {
			            self.set({
							renderonly: true,
							xid: json.data.xid,
                            mode_icon : data.mode_icon
						});
						cb();
					} else {
						alert(json.message);
					}
		            
		        });
		    },
            getTripmodeIcon:function (mode) {

                if(mode == 1) {
                    return 'icon_plane';
                } else if(mode == 2) {
                    return 'icon_train';
                }else if(mode == 3) {
                    return 'icon_bus';
                } else if(mode == 4) {
                    return 'icon_car';
                } else if(mode == 5) {
                    return 'icon_steamer';
                } else if(mode == 6) {
                    return 'icon_other';
                }

            },
		    editTraffic: function (itemdata, cb) {
		        var cb = cb || function () {};
		        if (itemdata.renderonly) {
		            cb();
		            return;
		        }
		        $(".ui_popup .ui_buttonB").addClass("ui_button_disabled").removeClass("ui_buttonB").attr("disabled", true);
		        var data = {};
		        data.trip_id = window.app.getTripId();
		        data.trip_day_id = window.app.getCurrentOnedayModel().get("xid");
		        data = $.extend(data, this.attributes);
		        data = $.extend(data, itemdata);
		        data.xid = data.xid;
		        $.post("/api/saveTraffic", data, function (result) {
		            $(".ui_popup .ui_button_disabled").removeClass("ui_button_disabled").addClass("ui_buttonB").attr("disabled", false);
		            if (result.code == 200) {
		                cb();
		            } else {
						alert(result.message);
					}
		        });
		    },
		    delTraffic: function () {
		        if (this.get('renderonly')) {
		            return;
		        }
		        var id = this.get('xid');
		        var trip_id = window.app.getTripId();
		        var trip_day_id = window.app.getCurrentOnedayModel().get("xid");
		        $.post("/api/removeTraffic", {
		            traffic_id: id,
		            trip_id: trip_id,
		            trip_day_id: trip_day_id
		        }, function () {
		        });
		    }
		});
		
		exports.Collections.OnedayTrafficCollection = Backbone.Collection.extend({
		    model: exports.Models.OnedayTrafficModel,
		    list: false,
		    initialize: function () {},
		    getList: function (cb) {
		        var trip_id = window.app.getTripId();
		        var oneday_id = window.app.getCurrentOnedayModel().get("xid");
		        var self = this;
		        $.post("/api/trafficList", {
		            trip_id: trip_id,
		            trip_day_id: oneday_id
		        }, function (result) {
					if(result.code == 200) {
		               cb(result.data);
				   }
		        });
		        return;
		    }
		});
		
		exports.Views.OnedayTrafficView = Backbone.View.extend({
		    el: "#_jstrafficcontainer",
		    listel: "#_jstrafficlist",
		    events: {
		        'click #_jsshowtrafficform': 'showForm'
		    },
		    initialize: function () {
		        _.bindAll(this, 'appendTrafficItem', 'resetItem');
		        this.form = new exports.Views.TrafficForm();
		        this.collection = new exports.Collections.OnedayTrafficCollection();
		        this.collection.on('add', this.appendTrafficItem);
		        this.collection.on('reset', this.resetItem);
		        this.render();
		    },
		    render: function () {},
		    resetItem: function () {
		        $("#_jstrafficlist").html('');
		    },
			//添加交通列表，外部调用这个方法，用于添加交通list到view
			addTrafficList: function () {
		        var that = this;
		        that.collection.reset();
				//从api获取list后添加转化成model，add到collection里，触发appendTrafficItem
		        that.collection.getList(function (data) {
					var list = data.traffic_list;
		            if (!list) {
		                return;
		            }
		            for (var t = 0; t < list.length; t++) {
		                var curdata = list[t];
						curdata.renderonly = true;
		                that.addTrafficModel(curdata);
		            }
		        });
		    },
		    addTrafficModel: function (data) {
		        var tmodel = new exports.Models.OnedayTrafficModel();
				tmodel.set(data);
				var that = this;
		        var cbfun = function () {
					//会触发，appendTrafficItem
		            that.collection.add(tmodel);
		            that.form.closeForm();
		        }
		        tmodel.addTraffic(cbfun);
		    },
		    appendTrafficItem: function (itemmodel) {
		        var trafficitem = new exports.Views.TrafficItem({
		            model: itemmodel
		        });
		        $(this.listel).append(trafficitem.render().el);
		    },
		    showForm: function () {
		        var self = this;
		        this.ajaxForm(false, function () {
		            self.form.setDefaultFromCity();
		            self.form.setDefaultToCity();
		            self.form.showTrafficTips();
		        });
		    },
		    ajaxForm: function (data, cb) {
		        var self = this;
		        var _url = "/api/trafficform?";
		        if (typeof data == 'object') {
		            for (var k in data) {
		                _url += "&" + k + "=" + data[k];
		            }
		        }
		       // _url = Plan.checkAjaxUrl(_url);
		        this.form.initData();
				xiaogouUI.popup.ajax({
		            url: _url,
		            width: 600,
		            callback: function () {
		                self.form.showTrafficTips();
		                if (typeof cb == 'function') cb();
		            }
		        });
		    },
		    changeTrafficModel: function (data) {
		        var mod = this.curmodel;
		        if (!mod) {
		            return;
		        }
		        data.mode_icon = this.getTripmodeIcon(data.mode);
                console.log(data);
		        var thiz = this;
		        var cbfun = function () {
		            thiz.form.closeForm();
		            mod.model.set(data);
		        }
		        mod.model.editTraffic(data, cbfun);
		    },
		    registerCurView: function (self) {
		        this.curmodel = self;
		    },
            getTripmodeIcon:function (mode) {

                if(mode == 1) {
                    return 'icon_plane';
                } else if(mode == 2) {
                    return 'icon_train';
                }else if(mode == 3) {
                    return 'icon_bus';
                } else if(mode == 4) {
                    return 'icon_car';
                } else if(mode == 5) {
                    return 'icon_steamer';
                } else if(mode == 6) {
                    return 'icon_other';
                }

            }
		});
		
		exports.Views.TrafficItem = Backbone.View.extend({
		    tagName: "div",
		    events: {
		        'click ._jsdelonetraffic': 'delTraffic',
		        'click ._jseditonetraffic': 'showTrafficForm'
		    },
		    initialize: function () {
		        _.bindAll(this, 'removeItem', 'delTraffic', 'changeItem');
		        this.tpl = $("#tpl_onedaytraffic").html();
		        this.model.on('remove', this.removeItem);
		        this.model.on('change', this.changeItem);
		    },
		    render: function () {
		        var data = this.model.model2JSON();
		        var html = Mustache.to_html(this.tpl, data);
		        $(this.el).html(html);
		        return this;
		    },
		    showTrafficForm: function () {
		        var id = this.model.get('xid');
		        if (!id) {
		            alert('数据还未同步，请稍等');
		            return;
		        }
		        window.app.getAlldayView().onedayTraffic.ajaxForm({
		            id: id
		        });
		        window.app.getAlldayView().onedayTraffic.registerCurView(this);
		    },
		    delTraffic: function (e) {
		        var model = this.model;
				if(confirm('确认要删除此交通')) {
		            model.set('renderonly', false);
		            window.app.getAlldayView().onedayTraffic.collection.remove(model);
		            model.delTraffic();
				}
		    },
		    removeItem: function () {
		        $(this.$el).remove();
		    },
		    changeItem: function () {
		        this.render();
		    }
		});
		
		exports.Views.TrafficForm = Backbone.View.extend({
		    el: 'body',
		    elform: "#trafficform",
		    events: {
		        'click #_jssubmittraffic': 'submitData',
		        'click #trafficform ._jscloseform': 'closeForm',
		        'keyup #trafficform #trafficnote': 'showNoteTip',
		        'focus #trafficform #trafficnote': 'showNoteTip',
		        'keyup #trafficform #traffic_number': 'showNumberTip',
		        'click #_jsselectendhours li, #_jsselectendminutes li, #_jsselectstarthours li, #_jsselectstartminutes li': 'countDays',
		        'click #_jstrafficdays li': 'selectDaysStatus',
		        'keyup #trafficform ._jsspendinput': 'countSpend',
		        'click #trafficform ._jsticketcountlist  li': 'countSpend',
		        'click #trafficform .pl_traffic_box .tips .title': 'slideTrafficTips',
		        'blur #trafficfromplace, #traffictoplace': 'showTrafficTips'
		    },
		    initialize: function () {
		        this.data = {};
		        this.initBind();
		        this.$elform = this.elform;
		        var self = this;
				
				var from_place = new autoThink({
		            inputel: "#trafficfromplace",
		            listel: "#trafficfromplacelist",
		            itemel: "#trafficfromplacelist a",
		            autoClearInput: false,
		            posturl: "/api/searchCity",
		            entercb: function (arg) {
		                self.searchEntercb('from', arg);
		            },
		            postdata: {
		                style: 'style1'
		            },
		            clickcb: function (e) {
		                self.searchClickcb('from', e);
		            },
		            voidexec: function () {
		                self.defaultSchFromCity();
		            }
		        });
				
		        var to_place = new autoThink({
		            inputel: "#traffictoplace",
		            listel: "#traffictoplacelist",
		            itemel: "#traffictoplacelist a",
		            autoClearInput: false,
		            posturl: "/api/searchCity",
		            entercb: function (arg) {
		                self.searchEntercb('to', arg);
		            },
		            postdata: {
		                style: 'style1'
		            },
		            clickcb: function (e) {
		                self.searchClickcb('to', e);
		            },
		            voidexec: function () {
		                self.defaultSchToCity();
		            }
		        });
		        this.notemaxlen = 1000;
		    },
		    initData: function (data) {
		        this.data = {};
		    },
		    slideTrafficTips: function (e) {
		        var title = $(e.currentTarget);
		        if (title.next(".list").is(":hidden")) {
		            title.find(".arrow").addClass("arrow1");
		            title.attr('data-bn-ipg', 'planedit-col2-citytraffic-unfold').next(".list").slideDown();
		        } else {
		            title.attr('data-bn-ipg', 'planedit-col2-citytraffic-fold').next(".list").slideUp();
		            title.find(".arrow").removeClass("arrow1");
		        }
		    },
		    showTrafficTips: function () {
		        var getarr = ['from_place', 'from_placeid', 'to_place', 'to_placeid'];
		        var gdata = {};
		        for (var i = 0; i < getarr.length; i++) {
		            var iname = getarr[i];
		            gdata[iname] = this.getVal($("input[name=" + iname + "]", this.$elform));
		        }
		        if (gdata['from_place'] == '' || gdata['to_place'] == '') {
		            $("#_jsrecommendtraffic").html('').hide();
		            return;
		        }
				/*
		        $.post("/api/recommendTraffic", gdata, function (json) {
		            $("#_jsrecommendtraffic").html(json.data.html).show();
		        });
				*/
		    },
		    setDefaultFromCity: function () {
		        var city = window.app.getCurrentOnedayModel().getOneCity(0);
		        if (typeof city != 'object') {
		            return;
		        }
		        $("input[name=from_place]", this.$elform).val(city.city_name);
		        $("input[name=from_placeid]", this.$elform).val(city.city_id);
		    },
		    defaultSchFromCity: function () {
		        var thiz = $("#trafficfromplace");
		        var val = this.getVal(thiz);
		        if (val != '') {
		            return;
		        }
		        var html = this.defaultSchHtml();
				console.log(html);
		        $("#trafficfrom_placelist").html(html).show();
		    },
		    setDefaultToCity: function () {
		        var tag = window.app.getCurrentOnedayModel().getOneCity(1);
		        if (typeof tag != 'object') {
		            return;
		        }
		        $("input[name=to_place]", this.$elform).val(tag.name);
		        $("input[name=totype]", this.$elform).val(tag.type);
		        $("input[name=to_placeid]", this.$elform).val(tag.tagid);
		    },
		    defaultSchToCity: function () {
		        var thiz = $("#traffictoplace");
		        var val = this.getVal(thiz);
		        if (val != '') {
		            return;
		        }
		        var html = this.defaultSchHtml();
		        $("#traffictoplacelist").html(html).show();
		    },
		    defaultSchHtml: function () {
		        var tpl = $("#tpl_searchlivecity").html();
		        var mod = window.app.getCurrentOnedayModel();
		        var taglist = mod.get('city_list').collection2JSON();
		        var buf = {};
		        for (var i = 0; i < taglist.length; i++) {
		            var n = taglist[i].name;
		            buf[n] = taglist[i];
		        }
		        var citylist = {};
		        var j = 0;
		        for (cityk in buf) {
		            citylist[j] = buf[cityk];
		            j++;
		        }
		        return Mustache.to_html(tpl, {
		            citylist: citylist
		        });
		    },
		    selectDaysStatus: function () {
		        this.daysStatus = false;
		    },
		    countSpend: function () {
		        setTimeout(function () {
		            var spend = commonStyle.getVal($("[name=spend]", this.elform));
		            var ticketcount = parseInt(commonStyle.getVal($("[name=ticketcount]", this.elform)));
		            var v;
		            if (isNaN(spend) || isNaN(ticketcount)) {
		                v = '';
		            } else {
		                v = spend * ticketcount;
		            }
		            $("#_jstrafficcountspend").html(v);
		        }, 100)
		    },
		    countDays: function (e) {
		        var doaction = $('input[name=doaction]', this.$elform).val();
		        var sh = parseInt($('input[name=starthours]', this.$elform).val());
		        var sm = parseInt($('input[name=startminutes]', this.$elform).val());
		        var eh = parseInt($('input[name=endhours]', this.$elform).val());
		        var em = parseInt($('input[name=endminutes]', this.$elform).val());
		        var days = parseInt($("input[name=days]", this.$elform).val());
		        if (days < 2 && sh > -1 && eh > -1) {
		            if ((sh * 60 + sm) >= (eh * 60 + em)) {
		                $("input[name=days]", this.$elform).val(1);
		                $("#_jstrafficdays .titles span").html('次日');
		            }
		        }
		    },
		    showNumberTip: function (e) {
		        var thiz = e.currentTarget;
		        var v = this.getVal($(thiz));
		        if (/^(.{0,20})$/.test(v) === false) {
		            $(thiz).addClass('ui2_error');
		        } else {
		            $(thiz).removeClass('ui2_error');
		        }
		        e.stopPropagation();
		    },
		    showNoteTip: function (e) {
		        var thiz = e.currentTarget;
		        commonStyle.showFontTip(thiz, this.notemaxlen);
		        return;
		    },
		    searchEntercb: function (way, arg) {
		        var place, type, placeid;
		        if (arg.obj.length > 0) {
		            place = $(arg.obj).attr('data-title');
		            type = $(arg.obj).attr('data-type');
		            placeid = $(arg.obj).attr('data-dataid');
		        } else {
		            place = arg.val;
		            type = 0;
		            placeid = 0;
		        }
		        this.searchedCity(way, {
		            place: place,
		            type: type,
		            placeid: placeid
		        });
		    },
		    searchClickcb: function (way, e) {
                obj = e.currentTarget;
		        var place = $(obj).attr('data-title');
		        var type = $(obj).attr('data-type');
		        var placeid = $(obj).attr('data-dataid');
		        this.searchedCity(way, {
		            place: place,
		            type: type,
		            placeid: placeid
		        });
		    },
		    searchedCity: function (way, data) {
		        if (way == 'from') {
		            $("input[name=from_place]", this.$elform).val(data.place);
		            $("input[name=fromtype]", this.$elform).val(data.type);
		            $("input[name=from_placeid]", this.$elform).val(data.placeid);
		        } else if (way == 'to') {
		            $("input[name=to_place]", this.$elform).val(data.place);
		            $("input[name=totype]", this.$elform).val(data.type);
		            $("input[name=to_placeid]", this.$elform).val(data.placeid);
		        }
		        this.showTrafficTips();
		    },
		    closeForm: function () {
		        var self = this;
		        setTimeout(function () {
		            xiaogouUI.popup.pupclose();
		            $(self.elform).remove();
		        }, 500);
		    },
		    submitData: function () {
		        if (!this.chkSubmit()) {
		            return false;
		        }
		        this.getData();
		        if (this.data.doaction == 'add') {
		            this.addTraffic();
		        } else if (this.data.doaction == 'edit') {
		            this.editTraffic();
		        }
		    },
		    getData: function () {
		        var getarr = ['id', 'doaction', 'from_place', 'from_placeid', 'to_place', 'to_placeid',
					 'mode', 'traffic_number', 'start_hours', 'start_minutes', 'end_hours', 'end_minutes',
					 'days', 'spend', 'currency', 'ticketcount'];
		        for (var i = 0; i < getarr.length; i++) {
		            var iname = getarr[i];
		            this.data[iname] = this.getVal($("input[name=" + iname + "]", this.$elform));
		        }
		        if (parseInt(this.data['from_placeid']) == 0) {
		            this.data['fromtype'] = 0;
		        }
		        if (parseInt(this.data['to_placeid']) == 0) {
		            this.data['totype'] = 0;
		        }
		        this.data['note'] = this.getVal($("textarea[name=note]", this.$elform));
		    },
		    addTraffic: function () {
		        delete this.data.id;
		        delete this.data.doaction;
		        this.data.renderonly = false;
				
		        if (window.app.getAlldayView().onedayTraffic.collection.length > 9) {
		            alert('最多添加10个交通信息！');
		            return false;
		        }
		        window.app.getAlldayView().onedayTraffic.addTrafficModel(this.data);
		    },
		    editTraffic: function () {
		        delete this.data.doaction;
		        this.data.renderonly = false;
		        var self = this;
		        window.app.getAlldayView().onedayTraffic.changeTrafficModel(this.data);
		    },
		    chkSubmit: function () {
		        var from_placeitem = $("input[name=from_place]", this.$elform);
		        this.data['from_place'] = this.getVal(from_placeitem);
		        if (!this.chkfrom_place()) {
		            this.showErrorLayer(from_placeitem, '请填写正确的出发城市！');
		            return false;
		        }
		        var to_placeitem = $("input[name=to_place]", this.$elform);
		        this.data['to_place'] = this.getVal(to_placeitem);
		        if (!this.chkto_place()) {
		            this.showErrorLayer(to_placeitem, '请填写正确的到达城市！');
		            return false;
		        }
		        var noteitem = $("textarea[name=note]", this.$elform);
		        this.data['note'] = this.getVal(noteitem);
		        if (!this.chknote()) {
		            this.showErrorLayer(noteitem, '请填写正确的备注！');
		            return false;
		        }
		        var spenditem = $("input[name=spend]", this.$elform);
		        
		        return true;
		    },
		    chknote: function () {
		        var tlen = util.getwordlen(this.data['note']);
		        var tmax = this.notemaxlen;
		        if (tlen > (tmax * 2)) {
		            return false;
		        }
		        return true;
		    },
		    chkfrom_place: function () {
		        if (this.data['from_place'].length < 1) {
		            return false;
		        }
		        return true;
		    },
		    chkto_place: function () {
		        if (this.data['to_place'].length < 1) {
		            return false;
		        }
		        return true;
		    },
		    chkspend: function () {
		        var mod = /^[0-9]{0,8}([.]{1}[0-9]{1,2})?$/;
		        return mod.test(this.data['spend']);
		    },
		    getVal: function (item) {
		        if (!item) return '';
		        var placeholder = item.attr('placeholder');
		        //var val = $(item).val().replace(/(^\s+)|(\s+$)/, '');
				var val = $(item).val();
		        if (placeholder) {
		            //var itemmatch = new RegExp(item.attr('placeholder'), 'i');
		            //val = val.replace(itemmatch, '');
		        }
		        return val;
		    },
		    showErrorLayer: function (thiz, msg, unlayer) {
		        var layer = '<div class="clearfix pr _jserrorlayer">';
		        layer += '<div class="ui2_error_layer">';
		        layer += '<p class="ui2_error_layer_arrow"></p>';
		        layer += '<p class="ui2_error_layer_arrow2"></p>';
		        layer += '<p class="ui2_error_layer_text">' + msg + '</p>';
		        layer += '</div>';
		        layer += '</div>';
		        $(thiz).addClass('ui2_error')
		        if (!unlayer) $(thiz).after(layer);
		    },
		    clearErrorLayer: function (thiz) {
		        $(thiz).removeClass('ui2_error').nextAll('._jserrorlayer').remove();
		    },
		    toClearErrorLayer: function (e) {
		        this.clearErrorLayer(e.currentTarget);
		    },
		    initBind: function () {}
		});
	


});
