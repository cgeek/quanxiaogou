define(function(require, exports, module) {
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		cookie = require('cookie');
		
	exports.tripStoreCookie = Backbone.Model.extend({
		namespace: "planinfo",
		cookie_opt: {
			expires: 365,
			path: '/',
			domain: "xiaogoulvxing.com"
		},
		cleanup: function () {
			$.cookie(this.namespace, "{}", this.cookie_opt);
			var keys = ["triptitle", "citylist", "starttime","customer_id"];
			var thiz = this;
			_.each(keys, function (v, k) {
				thiz.unset(v);
			});
		},
		initialize: function (opt) {
			this.iscookie = true;
			this.on("change", function () {
				var data = JSON.stringify(this.toJSON());
				$.cookie(this.namespace, data, this.cookie_opt);
			});
            console.log('cookie init');
            var ret = $.cookie(this.namespace);
            console.log('cookie init');
            console.log(ret);

            if (!ret) ret = '';
				var json = "";
			try {
				json = $.parseJSON(ret);
			} catch (e) {}
			if (json) {
				this.set(json, {
					silent: true
				});
			}
		},
		saveCity: function (ret) {
			this.set({
				citylist: ret
			});
		},
		saveStartCity: function (name, id) {
			console.log(name);
			this.set({
				scity: {
					n: name,
					i: id
				}
			});
		},
		getStartCity: function () {
			return this.get("scity");
		},
		saveEndCity: function (name, id) {
			this.set({
				ecity: {
					n: name,
					i: id
				}
			});
		},
		getEndCity: function () {
			return this.get("ecity");
		},
		saveTriptime: function (time) {
			this.set({
				starttime: time
			});
		},
		getTriptime: function () {
			return this.get("starttime") || "";
		},
		getCity: function () {
			var v = this.get("citylist");
			if (!_.isArray(v)) v = [];
				return v;
		},
		info2JSON: function () {
			var strjson = $.cookie(this.namespace);
			var json = json = $.parseJSON(strjson);
			if (typeof json == "undefined" || typeof json == "null") {
				json = {};
			}
			return json;
		}
	});
	
});
