define(function(require, exports, module){
	var $ = require('jquery'),
		_ = require('underscore'),
		Backbone = require('backbone'),
		Mustache = require('mustache');
		require('jquery-ui-sortable');
		require('jquery-ui-datepicker');
		require('fileupload');
		require('jquery-rte');
		require('jquery-rte-css');

    var EditToolBar = require('jquery-rte-tb');

	var Plan = Plan || {};
	
	//三栏的View
	var TripCols = require('./views/trip_cols_view');
	
	var PoiList = require('./modules/poi_list');
	
	var TripAllday = require('./views/trip_allday_view');
	
	var Note = require('./modules/note');

    var DemoList = require('./modules/demo');

	Plan.APP = Backbone.View.extend({
		el: "body",
		tripInfo: null,
		poiListView: null,
        demoListView: null,
		noteclip: null,
		max_onedayTag_num : 10, //每天最多去的城市数量
		max_oneday_num : 50,  	//最多去的天数
		max_poievent_num : 20,  //每天最多去的poi
		max_hotelevent_num : 5,  //每天最多住得hotel
		events: {
			'click ._jsplanpoidetail': 'showPoiDetail',
			'click #_jsplanpoidetail .close': 'closePoiDetail'
		},
		initialize: function (info) {
			_.bindAll(this, 'windowResize');

			//tripinfo 还没有模型化，先使用传进来的info
			this.tripInfo = info;

			//ColA 是最左栏的行程按日期的导航栏
			this.colA = new TripCols.Views.ColA();
			//ColB 是右侧的区域
			this.colB = new TripCols.Views.ColB();
			//ColC 是右侧的右侧区域
			this.colC = new TripCols.Views.ColC();
			
			//往ColA里添加具体每一天
			this.colA.addView("xcd", function(){
				return new TripAllday.Views.TripAllDayView();
			});
			
			//初始化PoiList的view
			this.poiListView = new PoiList.Views.PoiListView({
				model: new PoiList.Models.DefaultCityModel()
			});

            //推荐行程列表
            this.demoListView = new DemoList.Views.DemoListView({
                model: new DemoList.Models.DefaultCityModel()
            });

		},
		render: function() {
			var that = this;
			this.colA.render("xcd");

			//添加删除城市以后，回调修改
			this.sync_add_remove_citys_callback();
			
			//修改标题，有空改成用backbone， 临时放在这里
			$(document).delegate('#trip-title', 'click', function(){
				$(this).hide();
				$('#trip-title-ipt').show();
				$('#js_trip_title_input').focus();
			});
			$(document).delegate("#js_trip_title_input", 'blur', function(e){
				var title = $(e.target).val();
				if(title == '') {
					alert('标题不能为空');
				}
				var trip_id = that.tripInfo['tripId'];
				var postdata = {
					trip_id : trip_id,
					title: title
				};
				$.post("/api/updateTrip", postdata, function (result) {
					$('#trip-title-ipt').hide();
					$('#trip-title span').html(title);
					$('#trip-title').show();
				});
			});

			
		},
		getTripId: function() {
			return this.tripInfo['tripId'];
		},
		setStartTime: function(start_time){
			//修改时间
			var trip_id = this.tripInfo['tripId'];
			var postdata = {
				trip_id : trip_id,
				start_time: start_time
			};
			$.post("/api/updateTrip", postdata, function (result) {
				//callback();
				window.location.reload();
			});
		},
		getStarttime: function() {
			return this.tripInfo['starttime'];
		},
	    getAlldayView: function () {
	        return window.app.colA.getView("xcd");
	    },
		getCurrentOnedayModel: function () {
			return window.app.colB.getView("xcd_poievent_list").curoneday;
		},
		fixedEventLineCss: function (poieventnum) {
			if (poieventnum > 0) {
				$("#js_plan_activites").parent("div").removeClass("day_item_fff");
			} else {
				$("#js_plan_activites").parent("div").addClass("day_item_fff");
			}
		},
	    sync_add_remove_citys_callback: function (isaddtag) {
            var xcdView = window.app.colA.getView("xcd");
            if(xcdView.allday) {
                var tags = window.app.colA.getView("xcd").allday.getAllCitys();
                window.app.poiListView.renderCityList(tags);
                //window.app.demoListView.renderByCity(tags);
            }
	    },
		isCanAddAnotherOneday: function() {
		},
		isCanAddAnotherCitytag: function (onedaytaglist, ishide) {
			var size = onedaytaglist.size();
			if (size >= this.max_onedayTag_num) {
				alert("最多一天可以添加" + this.max_onedayTag_num + "个城市");
				return false;
			}
			return true;
		},
		windowResize: function() {
			var bodyHeight = $(window).height();
			var topHeight = $("#topbar").height() + 110;
			$(".pl_left ,.pl_rleft ,.pl_rright").height(bodyHeight - topHeight);
			var mRleftHeight = $(".pl_rleft").height();
			var mRrightHeight = $(".pl_rright").height();
			var nTopHeight = $(".pl_left .menu_tit").outerHeight();
			$(".pl_left .menu_cnt").height(bodyHeight - topHeight - nTopHeight);
			var mTopHeight = $(".pl_rleft .xcd_cnt_tit").outerHeight();
			$(".pl_rleft .xcd_cnt_day").outerHeight(mRleftHeight - mTopHeight);

			var mCityHeight = $(".pl_rright .pl_rtag").outerHeight();
			var jCityHeight = $(".pl_rcity .head").outerHeight();
			$(".pl_poi_cnt,.pl_stay_cnt,.pl_rnote_map").height(mRrightHeight - mCityHeight - jCityHeight);

            $(".pl_rright .pl_rnote").outerHeight(mRrightHeight - mCityHeight);
            $(".pl_rright .pl_rdemo").outerHeight(mRrightHeight - mCityHeight);
		}
		
	});
	return {
		init: function(){
			var that = this;
			window.app = new Plan.APP(tripInfo);
			app.render();
			
			app.windowResize();
			$(window).resize(function () {
				app.windowResize();
			});
		}
		
	}

});
