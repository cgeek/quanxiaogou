<?php
// uncomment the following to define a path alias
// Yii::setPathOfAlias('local','path/to/local-folder');

// This is the main Web application configuration. Any writable
// CWebApplication properties can be configured here.
return array(
	'basePath'=>dirname(__FILE__).DIRECTORY_SEPARATOR.'..',
	'name'=>'妙妙行程管家',

	// preloading 'log' component
	'preload'=>array('log'),

	// autoloading model and component classes
	'import'=>array(
		'application.models.*',
		'application.components.*',
		'ext.phpword.*'
	),

	'modules'=>array(
		// uncomment the following to enable the Gii tool
		'gii'=>array(
			'class'=>'system.gii.GiiModule',
			'password'=>'miaomiao',
			// If removed, Gii defaults to localhost only. Edit carefully to taste.
			'ipFilters'=>array('127.0.0.1','192.168.*', '::1'),
		),
		'admin',
		'client',
		'api',
		'plan',
		'partners'
	),

	// application components
	'components'=>array(
		'user'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class'=>'WebUser',
			'loginUrl'=>array('user/login'),
		),
		'adminUser'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class'=>'WebUser',
			'stateKeyPrefix'=>'admin_',
			'loginUrl'=>array('/admin/user/login'),
		),
		'partner'=>array(
			// enable cookie-based authentication
			'allowAutoLogin'=>true,
			'class'=>'WebUser',
			'stateKeyPrefix'=>'partner_',
			'loginUrl'=>array('/partners/user/login'),
		),
		// uncomment the following to enable URLs in path-format
		'urlManager'=>array(
			'urlFormat'=>'path',
			'showScriptName'=>false,
            'caseSensitive' => false,
			'rules'=>array(
                'http://dev.miaomiao.cc<_q:.*>/*'=> 'partners<_q>',
                //'http://dev.xiaogoulvxing.com' => 'partners',
                //'http://dev.xiaogoulvxing.com' => 'partners',
				'<controller:\w+>/<id:\d+>'=>'<controller>/detail',
				'<controller:\w+>/<action:\w+>/<id:\d+>'=>'<controller>/<action>',
			),
		),
		/*
		'db'=>array(
			'connectionString' => 'sqlite:'.dirname(__FILE__).'/../data/testdrive.db',
		),
		*/
		// uncomment the following to use a MySQL database
		'db'=>array(
			'connectionString' => 'mysql:host=127.0.0.1;dbname=xiaogoulvxing',
			'emulatePrepare' => true,
			'username' => 'root',
			'password' => '123456',
			'charset' => 'utf8',
			'enableParamLogging' => true,
		),
		'errorHandler'=>array(
			// use 'site/error' action to display errors
			'errorAction'=>'error/error',
		),
        'cache'=>array( 'class'=>'CFileCache'),
        'log'=>array(
			'class'=>'CLogRouter',
			'routes'=>array(
				array(
					'class'=>'CFileLogRoute',
					'levels'=>'error, warning, log',
					'categories' => 'system.db.*',
					'logFile' => 'sql.log',
				),
				// uncomment the following to show log messages on web pages
				/*
				array(
					'class'=>'CWebLogRoute',
				),
				 */
			),
		),
		'upyun' => array(
			'class' => 'ext.upyun.QUpyun',
			'user' => 'cgeek', 
			'password' => 'miaomiao'
		),
		'taobao' => array(
			'class' => 'ext.taobao.Taobao',
			'appkey' => '21686155',
			'secretKey' => '35f5a7a2c72f5c80c09f2104cd0f67f1',
			//'appkey' => '21105797',
			//'secretKey' => 'b200948628f47b0ff1042d46670ccce0',
		),
		'curl' => array(
			'class' => 'ext.Curl'
		)
	),

	// application-level parameters that can be accessed
	// using Yii::app()->params['paramName']
	'params'=>array(
		// this is used in contact page
		'adminEmail'=>'cgeek.share@gmail.com',
	),
	'defaultController' => 'site',
);
