<?php

/**
 * This is the model class for table "weixin_message".
 *
 * The followings are the available columns in table 'weixin_message':
 * @property integer $id
 * @property string $ToUserName
 * @property string $FromUserName
 * @property integer $CreateTime
 * @property string $MsgType
 * @property string $Content
 * @property string $MsgId
 * @property string $PicUrl
 * @property string $MediaId
 * @property string $Format
 * @property string $ThumbMediaId
 * @property string $Location_X
 * @property string $Location_Y
 * @property string $Scale
 * @property string $Label
 * @property string $Title
 * @property string $Description
 * @property string $Url
 * @property integer $status
 * @property integer $is_admin
 */
class WeixinMessage extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return WeixinMessage the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'weixin_message';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ToUserName, FromUserName', 'required'),
			array('CreateTime, status, is_admin', 'numerical', 'integerOnly'=>true),
			array('ToUserName, FromUserName, MsgType, MsgId, MediaId, Format, ThumbMediaId, Location_X, Location_Y, Scale, Label, Title, Description', 'length', 'max'=>255),
			array('Content, PicUrl, Url', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ToUserName, FromUserName, CreateTime, MsgType, Content, MsgId, PicUrl, MediaId, Format, ThumbMediaId, Location_X, Location_Y, Scale, Label, Title, Description, Url, status, is_admin', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ToUserName' => 'To User Name',
			'FromUserName' => 'From User Name',
			'CreateTime' => 'Create Time',
			'MsgType' => 'Msg Type',
			'Content' => 'Content',
			'MsgId' => 'Msg',
			'PicUrl' => 'Pic Url',
			'MediaId' => 'Media',
			'Format' => 'Format',
			'ThumbMediaId' => 'Thumb Media',
			'Location_X' => 'Location X',
			'Location_Y' => 'Location Y',
			'Scale' => 'Scale',
			'Label' => 'Label',
			'Title' => 'Title',
			'Description' => 'Description',
			'Url' => 'Url',
			'status' => 'Status',
			'is_admin' => 'Is Admin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ToUserName',$this->ToUserName,true);
		$criteria->compare('FromUserName',$this->FromUserName,true);
		$criteria->compare('CreateTime',$this->CreateTime);
		$criteria->compare('MsgType',$this->MsgType,true);
		$criteria->compare('Content',$this->Content,true);
		$criteria->compare('MsgId',$this->MsgId,true);
		$criteria->compare('PicUrl',$this->PicUrl,true);
		$criteria->compare('MediaId',$this->MediaId,true);
		$criteria->compare('Format',$this->Format,true);
		$criteria->compare('ThumbMediaId',$this->ThumbMediaId,true);
		$criteria->compare('Location_X',$this->Location_X,true);
		$criteria->compare('Location_Y',$this->Location_Y,true);
		$criteria->compare('Scale',$this->Scale,true);
		$criteria->compare('Label',$this->Label,true);
		$criteria->compare('Title',$this->Title,true);
		$criteria->compare('Description',$this->Description,true);
		$criteria->compare('Url',$this->Url,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('is_admin',$this->is_admin);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}