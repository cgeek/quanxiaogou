<?php

/**
 * This is the model class for table "user".
 *
 * The followings are the available columns in table 'user':
 * @property integer $user_id
 * @property string $user_name
 * @property string $password
 * @property string $nick_name
 * @property string $group
 * @property integer $client_id
 * @property integer $shop_id
 * @property string $parent_uid
 * @property string $email
 * @property string $phone
 * @property string $taobao_url
 * @property string $weibo
 * @property string $weibo_uid
 * @property string $weixin
 * @property string $weixin_uid
 * @property string $website
 * @property string $location
 * @property string $province
 * @property string $description
 * @property string $avatar
 * @property integer $ctime
 * @property string $mtime
 * @property string $last_login_time
 * @property integer $status
 */
class User extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return User the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('group, ctime', 'required'),
			array('client_id, shop_id, ctime, status', 'numerical', 'integerOnly'=>true),
			array('user_name, password', 'length', 'max'=>64),
			array('nick_name, group, email, phone, taobao_url, weibo, weibo_uid, weixin, weixin_uid, website, location, province, description, avatar', 'length', 'max'=>255),
			array('parent_uid', 'length', 'max'=>20),
			array('mtime, last_login_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('user_id, user_name, password, nick_name, group, client_id, shop_id, parent_uid, email, phone, taobao_url, weibo, weibo_uid, weixin, weixin_uid, website, location, province, description, avatar, ctime, mtime, last_login_time, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'user_id' => 'User',
			'user_name' => 'User Name',
			'password' => 'Password',
			'nick_name' => 'Nick Name',
			'group' => 'Group',
			'client_id' => 'Client',
			'shop_id' => 'Shop',
			'parent_uid' => 'Parent Uid',
			'email' => 'Email',
			'phone' => 'Phone',
			'taobao_url' => 'Taobao Url',
			'weibo' => 'Weibo',
			'weibo_uid' => 'Weibo Uid',
			'weixin' => 'Weixin',
			'weixin_uid' => 'Weixin Uid',
			'website' => 'Website',
			'location' => 'Location',
			'province' => 'Province',
			'description' => 'Description',
			'avatar' => 'Avatar',
			'ctime' => 'Ctime',
			'mtime' => 'Mtime',
			'last_login_time' => 'Last Login Time',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('user_name',$this->user_name,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('nick_name',$this->nick_name,true);
		$criteria->compare('group',$this->group,true);
		$criteria->compare('client_id',$this->client_id);
		$criteria->compare('shop_id',$this->shop_id);
		$criteria->compare('parent_uid',$this->parent_uid,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('taobao_url',$this->taobao_url,true);
		$criteria->compare('weibo',$this->weibo,true);
		$criteria->compare('weibo_uid',$this->weibo_uid,true);
		$criteria->compare('weixin',$this->weixin,true);
		$criteria->compare('weixin_uid',$this->weixin_uid,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('location',$this->location,true);
		$criteria->compare('province',$this->province,true);
		$criteria->compare('description',$this->description,true);
		$criteria->compare('avatar',$this->avatar,true);
		$criteria->compare('ctime',$this->ctime);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('last_login_time',$this->last_login_time,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}