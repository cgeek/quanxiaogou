<?php

/**
 * This is the model class for table "package".
 *
 * The followings are the available columns in table 'package':
 * @property integer $id
 * @property string $title
 * @property string $subtitle
 * @property string $type
 * @property string $detail
 * @property string $cover_image
 * @property double $market_price
 * @property double $price
 * @property string $selling_points
 * @property string $hotel_facilities
 * @property string $address
 * @property double $lat
 * @property double $lon
 * @property string $mtime
 * @property string $ctime
 * @property integer $status
 */
class Package extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Package the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'package';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, type, mtime, ctime', 'required'),
			array('status', 'numerical', 'integerOnly'=>true),
			array('market_price, price, lat, lon', 'numerical'),
			array('title, subtitle, type, cover_image, address', 'length', 'max'=>255),
			array('detail, selling_points, hotel_facilities', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, subtitle, type, detail, cover_image, market_price, price, selling_points, hotel_facilities, address, lat, lon, mtime, ctime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'subtitle' => 'Subtitle',
			'type' => 'Type',
			'detail' => 'Detail',
			'cover_image' => 'Cover Image',
			'market_price' => 'Market Price',
			'price' => 'Price',
			'selling_points' => 'Selling Points',
			'hotel_facilities' => 'Hotel Facilities',
			'address' => 'Address',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'mtime' => 'Mtime',
			'ctime' => 'Ctime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('subtitle',$this->subtitle,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('detail',$this->detail,true);
		$criteria->compare('cover_image',$this->cover_image,true);
		$criteria->compare('market_price',$this->market_price);
		$criteria->compare('price',$this->price);
		$criteria->compare('selling_points',$this->selling_points,true);
		$criteria->compare('hotel_facilities',$this->hotel_facilities,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('lat',$this->lat);
		$criteria->compare('lon',$this->lon);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}