<?php

/**
 * This is the model class for table "trip_day_traffic".
 *
 * The followings are the available columns in table 'trip_day_traffic':
 * @property integer $id
 * @property string $title
 * @property integer $trip_id
 * @property integer $trip_day_id
 * @property string $from_place
 * @property integer $from_placeid
 * @property string $to_place
 * @property integer $to_placeid
 * @property integer $start_hours
 * @property integer $start_minutes
 * @property integer $end_hours
 * @property integer $end_minutes
 * @property integer $days
 * @property integer $spend
 * @property string $currency
 * @property integer $ticketcount
 * @property string $traffic_number
 * @property string $mode
 * @property string $note
 * @property string $mtime
 * @property string $ctime
 * @property integer $status
 */
class TripDayTraffic extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TripDayTraffic the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trip_day_traffic';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('spend, ctime', 'required'),
			array('trip_id, trip_day_id, from_placeid, to_placeid, start_hours, start_minutes, end_hours, end_minutes, days, spend, ticketcount, status', 'numerical', 'integerOnly'=>true),
			array('title, from_place, to_place, currency, traffic_number, mode', 'length', 'max'=>255),
			array('note, mtime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, trip_id, trip_day_id, from_place, from_placeid, to_place, to_placeid, start_hours, start_minutes, end_hours, end_minutes, days, spend, currency, ticketcount, traffic_number, mode, note, mtime, ctime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'trip_id' => 'Trip',
			'trip_day_id' => 'Trip Day',
			'from_place' => 'From Place',
			'from_placeid' => 'From Placeid',
			'to_place' => 'To Place',
			'to_placeid' => 'To Placeid',
			'start_hours' => 'Start Hours',
			'start_minutes' => 'Start Minutes',
			'end_hours' => 'End Hours',
			'end_minutes' => 'End Minutes',
			'days' => 'Days',
			'spend' => 'Spend',
			'currency' => 'Currency',
			'ticketcount' => 'Ticketcount',
			'traffic_number' => 'Traffic Number',
			'mode' => 'Mode',
			'note' => 'Note',
			'mtime' => 'Mtime',
			'ctime' => 'Ctime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('trip_id',$this->trip_id);
		$criteria->compare('trip_day_id',$this->trip_day_id);
		$criteria->compare('from_place',$this->from_place,true);
		$criteria->compare('from_placeid',$this->from_placeid);
		$criteria->compare('to_place',$this->to_place,true);
		$criteria->compare('to_placeid',$this->to_placeid);
		$criteria->compare('start_hours',$this->start_hours);
		$criteria->compare('start_minutes',$this->start_minutes);
		$criteria->compare('end_hours',$this->end_hours);
		$criteria->compare('end_minutes',$this->end_minutes);
		$criteria->compare('days',$this->days);
		$criteria->compare('spend',$this->spend);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('ticketcount',$this->ticketcount);
		$criteria->compare('traffic_number',$this->traffic_number,true);
		$criteria->compare('mode',$this->mode,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}