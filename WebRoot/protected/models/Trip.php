<?php

/**
 * This is the model class for table "trip".
 *
 * The followings are the available columns in table 'trip':
 * @property integer $id
 * @property string $title
 * @property integer $customer_id
 * @property integer $admin_id
 * @property integer $user_id
 * @property integer $type
 * @property string $start_time
 * @property integer $days
 * @property integer $start_city_id
 * @property integer $end_city_id
 * @property integer $from_trip_id
 * @property string $mtime
 * @property string $ctime
 * @property integer $status
 * @property string $hash_id
 */
class Trip extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Trip the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trip';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, mtime', 'required'),
			array('customer_id, admin_id, user_id, type, days, start_city_id, end_city_id, from_trip_id, status', 'numerical', 'integerOnly'=>true),
			array('title, hash_id', 'length', 'max'=>255),
			array('start_time, ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, customer_id, admin_id, user_id, type, start_time, days, start_city_id, end_city_id, from_trip_id, mtime, ctime, status, hash_id', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'customer_id' => 'Customer',
			'admin_id' => 'Admin',
			'user_id' => 'User',
			'type' => 'Type',
			'start_time' => 'Start Time',
			'days' => 'Days',
			'start_city_id' => 'Start City',
			'end_city_id' => 'End City',
			'from_trip_id' => 'From Trip',
			'mtime' => 'Mtime',
			'ctime' => 'Ctime',
			'status' => 'Status',
			'hash_id' => 'Hash',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('admin_id',$this->admin_id);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('type',$this->type);
		$criteria->compare('start_time',$this->start_time,true);
		$criteria->compare('days',$this->days);
		$criteria->compare('start_city_id',$this->start_city_id);
		$criteria->compare('end_city_id',$this->end_city_id);
		$criteria->compare('from_trip_id',$this->from_trip_id);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('status',$this->status);
		$criteria->compare('hash_id',$this->hash_id,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}