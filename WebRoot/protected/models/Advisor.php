<?php

/**
 * This is the model class for table "advisor".
 *
 * The followings are the available columns in table 'advisor':
 * @property integer $id
 * @property integer $ctrip_id
 * @property string $nickName
 * @property string $name
 * @property integer $gender
 * @property string $email
 * @property string $birthday
 * @property string $tagList
 * @property string $districtList
 * @property string $registeTime
 * @property string $contactPhone
 * @property integer $isCitizensInChina
 * @property string $coverImgUrl
 * @property string $logoImgUrl
 * @property string $mobilePhone
 * @property string $weixin
 * @property string $address
 * @property string $locationProvinceName
 * @property string $locationCityName
 * @property string $locationRegionName
 * @property string $sign
 * @property integer $serveCount
 * @property integer $commentScore
 * @property integer $commentCount
 * @property string $providerName
 * @property string $brandName
 * @property string $source_json
 */
class Advisor extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Advisor the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'advisor';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('ctrip_id, gender, isCitizensInChina, serveCount, commentScore, commentCount', 'numerical', 'integerOnly'=>true),
			array('nickName, name, email, birthday, tagList, contactPhone, coverImgUrl, logoImgUrl, mobilePhone, weixin, address, locationProvinceName, locationCityName, locationRegionName, sign, providerName, brandName', 'length', 'max'=>255),
			array('districtList, registeTime, source_json', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, ctrip_id, nickName, name, gender, email, birthday, tagList, districtList, registeTime, contactPhone, isCitizensInChina, coverImgUrl, logoImgUrl, mobilePhone, weixin, address, locationProvinceName, locationCityName, locationRegionName, sign, serveCount, commentScore, commentCount, providerName, brandName, source_json', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'ctrip_id' => 'Ctrip',
			'nickName' => 'Nick Name',
			'name' => 'Name',
			'gender' => 'Gender',
			'email' => 'Email',
			'birthday' => 'Birthday',
			'tagList' => 'Tag List',
			'districtList' => 'District List',
			'registeTime' => 'Registe Time',
			'contactPhone' => 'Contact Phone',
			'isCitizensInChina' => 'Is Citizens In China',
			'coverImgUrl' => 'Cover Img Url',
			'logoImgUrl' => 'Logo Img Url',
			'mobilePhone' => 'Mobile Phone',
			'weixin' => 'Weixin',
			'address' => 'Address',
			'locationProvinceName' => 'Location Province Name',
			'locationCityName' => 'Location City Name',
			'locationRegionName' => 'Location Region Name',
			'sign' => 'Sign',
			'serveCount' => 'Serve Count',
			'commentScore' => 'Comment Score',
			'commentCount' => 'Comment Count',
			'providerName' => 'Provider Name',
			'brandName' => 'Brand Name',
			'source_json' => 'Source Json',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('ctrip_id',$this->ctrip_id);
		$criteria->compare('nickName',$this->nickName,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('gender',$this->gender);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('tagList',$this->tagList,true);
		$criteria->compare('districtList',$this->districtList,true);
		$criteria->compare('registeTime',$this->registeTime,true);
		$criteria->compare('contactPhone',$this->contactPhone,true);
		$criteria->compare('isCitizensInChina',$this->isCitizensInChina);
		$criteria->compare('coverImgUrl',$this->coverImgUrl,true);
		$criteria->compare('logoImgUrl',$this->logoImgUrl,true);
		$criteria->compare('mobilePhone',$this->mobilePhone,true);
		$criteria->compare('weixin',$this->weixin,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('locationProvinceName',$this->locationProvinceName,true);
		$criteria->compare('locationCityName',$this->locationCityName,true);
		$criteria->compare('locationRegionName',$this->locationRegionName,true);
		$criteria->compare('sign',$this->sign,true);
		$criteria->compare('serveCount',$this->serveCount);
		$criteria->compare('commentScore',$this->commentScore);
		$criteria->compare('commentCount',$this->commentCount);
		$criteria->compare('providerName',$this->providerName,true);
		$criteria->compare('brandName',$this->brandName,true);
		$criteria->compare('source_json',$this->source_json,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}