<?php

/**
 * This is the model class for table "weixin_user".
 *
 * The followings are the available columns in table 'weixin_user':
 * @property integer $id
 * @property string $openid
 * @property string $nickname
 * @property integer $sex
 * @property string $city
 * @property string $country
 * @property string $province
 * @property string $language
 * @property string $headimgurl
 * @property integer $subscribe_time
 * @property integer $subscribe
 * @property string $remark
 * @property string $unionid
 */
class WeixinUser extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return WeixinUser the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'weixin_user';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('openid, nickname', 'required'),
			array('sex, subscribe_time, subscribe', 'numerical', 'integerOnly'=>true),
			array('openid, nickname, city, country, province, language, unionid', 'length', 'max'=>255),
			array('headimgurl, remark', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, openid, nickname, sex, city, country, province, language, headimgurl, subscribe_time, subscribe, remark, unionid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'openid' => 'Openid',
			'nickname' => 'Nickname',
			'sex' => 'Sex',
			'city' => 'City',
			'country' => 'Country',
			'province' => 'Province',
			'language' => 'Language',
			'headimgurl' => 'Headimgurl',
			'subscribe_time' => 'Subscribe Time',
			'subscribe' => 'Subscribe',
			'remark' => 'Remark',
			'unionid' => 'Unionid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('openid',$this->openid,true);
		$criteria->compare('nickname',$this->nickname,true);
		$criteria->compare('sex',$this->sex);
		$criteria->compare('city',$this->city,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('province',$this->province,true);
		$criteria->compare('language',$this->language,true);
		$criteria->compare('headimgurl',$this->headimgurl,true);
		$criteria->compare('subscribe_time',$this->subscribe_time);
		$criteria->compare('subscribe',$this->subscribe);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('unionid',$this->unionid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}