<?php

/**
 * This is the model class for table "traveler".
 *
 * The followings are the available columns in table 'traveler':
 * @property integer $id
 * @property string $name
 * @property string $english_name
 * @property string $first_name
 * @property string $last_name
 * @property string $passport_no
 * @property string $passport_validity
 * @property string $birthday
 * @property string $sex
 * @property integer $age
 * @property string $id_no
 * @property integer $customer_id
 * @property integer $is_customer
 * @property string $flight_info
 * @property string $mtime
 * @property string $ctime
 * @property integer $status
 */
class Traveler extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Traveler the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'traveler';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, mtime, ctime', 'required'),
			array('age, customer_id, is_customer, status', 'numerical', 'integerOnly'=>true),
			array('name, english_name, first_name, last_name, passport_no, sex, id_no', 'length', 'max'=>255),
			array('passport_validity, birthday, flight_info', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, english_name, first_name, last_name, passport_no, passport_validity, birthday, sex, age, id_no, customer_id, is_customer, flight_info, mtime, ctime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'english_name' => 'English Name',
			'first_name' => 'First Name',
			'last_name' => 'Last Name',
			'passport_no' => 'Passport No',
			'passport_validity' => 'Passport Validity',
			'birthday' => 'Birthday',
			'sex' => 'Sex',
			'age' => 'Age',
			'id_no' => 'Id No',
			'customer_id' => 'Customer',
			'is_customer' => 'Is Customer',
			'flight_info' => 'Flight Info',
			'mtime' => 'Mtime',
			'ctime' => 'Ctime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('english_name',$this->english_name,true);
		$criteria->compare('first_name',$this->first_name,true);
		$criteria->compare('last_name',$this->last_name,true);
		$criteria->compare('passport_no',$this->passport_no,true);
		$criteria->compare('passport_validity',$this->passport_validity,true);
		$criteria->compare('birthday',$this->birthday,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('age',$this->age);
		$criteria->compare('id_no',$this->id_no,true);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('is_customer',$this->is_customer);
		$criteria->compare('flight_info',$this->flight_info,true);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}