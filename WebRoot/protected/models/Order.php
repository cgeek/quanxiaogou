<?php

/**
 * This is the model class for table "order".
 *
 * The followings are the available columns in table 'order':
 * @property integer $id
 * @property string $title
 * @property string $desc
 * @property string $channel
 * @property string $booking_type
 * @property string $booking_url
 * @property string $type
 * @property integer $customer_id
 * @property string $remark
 * @property string $start_date
 * @property string $end_date
 * @property double $income
 * @property double $total_fee
 * @property double $payment
 * @property string $payment_type
 * @property double $down_payment
 * @property double $balance_payment
 * @property string $payment_deadline
 * @property string $payment_path
 * @property string $source
 * @property string $agent
 * @property string $agent_url
 * @property string $commission
 * @property string $agent_order_id
 * @property string $payout_trade_id
 * @property string $payment_trade_id
 * @property string $payment_nick_name
 * @property integer $admin_id
 * @property string $mtime
 * @property string $ctime
 * @property integer $status
 */
class Order extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Order the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'order';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('title, type, mtime, ctime', 'required'),
			array('customer_id, admin_id, status', 'numerical', 'integerOnly'=>true),
			array('income, total_fee, payment, down_payment, balance_payment', 'numerical'),
			array('title, channel, booking_type, type, payment_type, payment_path, source, agent, agent_url, commission, agent_order_id, payout_trade_id, payment_trade_id, payment_nick_name', 'length', 'max'=>255),
			array('desc, booking_url, remark, start_date, end_date, payment_deadline', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, title, desc, channel, booking_type, booking_url, type, customer_id, remark, start_date, end_date, income, total_fee, payment, payment_type, down_payment, balance_payment, payment_deadline, payment_path, source, agent, agent_url, commission, agent_order_id, payout_trade_id, payment_trade_id, payment_nick_name, admin_id, mtime, ctime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'title' => 'Title',
			'desc' => 'Desc',
			'channel' => 'Channel',
			'booking_type' => 'Booking Type',
			'booking_url' => 'Booking Url',
			'type' => 'Type',
			'customer_id' => 'Customer',
			'remark' => 'Remark',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'income' => 'Income',
			'total_fee' => 'Total Fee',
			'payment' => 'Payment',
			'payment_type' => 'Payment Type',
			'down_payment' => 'Down Payment',
			'balance_payment' => 'Balance Payment',
			'payment_deadline' => 'Payment Deadline',
			'payment_path' => 'Payment Path',
			'source' => 'Source',
			'agent' => 'Agent',
			'agent_url' => 'Agent Url',
			'commission' => 'Commission',
			'agent_order_id' => 'Agent Order',
			'payout_trade_id' => 'Payout Trade',
			'payment_trade_id' => 'Payment Trade',
			'payment_nick_name' => 'Payment Nick Name',
			'admin_id' => 'Admin',
			'mtime' => 'Mtime',
			'ctime' => 'Ctime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('channel',$this->channel,true);
		$criteria->compare('booking_type',$this->booking_type,true);
		$criteria->compare('booking_url',$this->booking_url,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('income',$this->income);
		$criteria->compare('total_fee',$this->total_fee);
		$criteria->compare('payment',$this->payment);
		$criteria->compare('payment_type',$this->payment_type,true);
		$criteria->compare('down_payment',$this->down_payment);
		$criteria->compare('balance_payment',$this->balance_payment);
		$criteria->compare('payment_deadline',$this->payment_deadline,true);
		$criteria->compare('payment_path',$this->payment_path,true);
		$criteria->compare('source',$this->source,true);
		$criteria->compare('agent',$this->agent,true);
		$criteria->compare('agent_url',$this->agent_url,true);
		$criteria->compare('commission',$this->commission,true);
		$criteria->compare('agent_order_id',$this->agent_order_id,true);
		$criteria->compare('payout_trade_id',$this->payout_trade_id,true);
		$criteria->compare('payment_trade_id',$this->payment_trade_id,true);
		$criteria->compare('payment_nick_name',$this->payment_nick_name,true);
		$criteria->compare('admin_id',$this->admin_id);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}