<?php

/**
 * This is the model class for table "customer".
 *
 * The followings are the available columns in table 'customer':
 * @property integer $id
 * @property string $contact
 * @property string $sex
 * @property string $origin
 * @property string $destination
 * @property string $start_date
 * @property string $end_date
 * @property string $num_of_people
 * @property integer $has_booked_airline_ticket
 * @property integer $has_booked_hotel
 * @property integer $no_booking
 * @property string $remark
 * @property string $mobile
 * @property string $qq
 * @property string $weixin
 * @property string $weixin_uuid
 * @property string $weibo
 * @property string $alipay
 * @property string $wangwang
 * @property string $email
 * @property integer $service_fee
 * @property double $order_balance
 * @property double $alipay_balance
 * @property string $taobao_trade_id
 * @property string $journey_url
 * @property string $qq_log
 * @property string $source
 * @property string $platform
 * @property integer $check_item_count
 * @property integer $check_item_complete_count
 * @property integer $admin_id
 * @property integer $user_id
 * @property integer $trip_id
 * @property string $ctime
 * @property string $mtime
 * @property integer $status
 */
class Customer extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Customer the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'customer';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('contact, ctime', 'required'),
			array('has_booked_airline_ticket, has_booked_hotel, no_booking, service_fee, check_item_count, check_item_complete_count, admin_id, trip_id, status', 'numerical', 'integerOnly'=>true),
			array('order_balance, alipay_balance', 'numerical'),
			array('contact, sex, origin, destination, num_of_people, mobile, qq, weixin, weixin_uuid, weibo, alipay, wangwang, email, taobao_trade_id, source, platform', 'length', 'max'=>255),
			array('start_date, end_date, remark, journey_url, qq_log, mtime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, contact, sex, origin, destination, start_date, end_date, num_of_people, has_booked_airline_ticket, has_booked_hotel, no_booking, remark, mobile, qq, weixin, weixin_uuid, weibo, alipay, wangwang, email, service_fee, order_balance, alipay_balance, taobao_trade_id, journey_url, qq_log, source, platform, check_item_count, check_item_complete_count, admin_id, trip_id, ctime, mtime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'contact' => 'Contact',
			'sex' => 'Sex',
			'origin' => 'Origin',
			'destination' => 'Destination',
			'start_date' => 'Start Date',
			'end_date' => 'End Date',
			'num_of_people' => 'Num Of People',
			'has_booked_airline_ticket' => 'Has Booked Airline Ticket',
			'has_booked_hotel' => 'Has Booked Hotel',
			'no_booking' => 'No Booking',
			'remark' => 'Remark',
			'mobile' => 'Mobile',
			'qq' => 'Qq',
			'weixin' => 'Weixin',
			'weixin_uuid' => 'Weixin Uuid',
			'weibo' => 'Weibo',
			'alipay' => 'Alipay',
			'wangwang' => 'Wangwang',
			'email' => 'Email',
			'service_fee' => 'Service Fee',
			'order_balance' => 'Order Balance',
			'alipay_balance' => 'Alipay Balance',
			'taobao_trade_id' => 'Taobao Trade',
			'journey_url' => 'Journey Url',
			'qq_log' => 'Qq Log',
			'source' => 'Source',
			'platform' => 'Platform',
			'check_item_count' => 'Check Item Count',
			'check_item_complete_count' => 'Check Item Complete Count',
			'admin_id' => 'Admin',
            'user_id' => 'User',
			'trip_id' => 'Trip',
			'ctime' => 'Ctime',
			'mtime' => 'Mtime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('contact',$this->contact,true);
		$criteria->compare('sex',$this->sex,true);
		$criteria->compare('origin',$this->origin,true);
		$criteria->compare('destination',$this->destination,true);
		$criteria->compare('start_date',$this->start_date,true);
		$criteria->compare('end_date',$this->end_date,true);
		$criteria->compare('num_of_people',$this->num_of_people,true);
		$criteria->compare('has_booked_airline_ticket',$this->has_booked_airline_ticket);
		$criteria->compare('has_booked_hotel',$this->has_booked_hotel);
		$criteria->compare('no_booking',$this->no_booking);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('mobile',$this->mobile,true);
		$criteria->compare('qq',$this->qq,true);
		$criteria->compare('weixin',$this->weixin,true);
		$criteria->compare('weixin_uuid',$this->weixin_uuid,true);
		$criteria->compare('weibo',$this->weibo,true);
		$criteria->compare('alipay',$this->alipay,true);
		$criteria->compare('wangwang',$this->wangwang,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('service_fee',$this->service_fee);
		$criteria->compare('order_balance',$this->order_balance);
		$criteria->compare('alipay_balance',$this->alipay_balance);
		$criteria->compare('taobao_trade_id',$this->taobao_trade_id,true);
		$criteria->compare('journey_url',$this->journey_url,true);
		$criteria->compare('qq_log',$this->qq_log,true);
		$criteria->compare('source',$this->source,true);
		$criteria->compare('platform',$this->platform,true);
		$criteria->compare('check_item_count',$this->check_item_count);
		$criteria->compare('check_item_complete_count',$this->check_item_complete_count);
		$criteria->compare('admin_id',$this->admin_id);
        $criteria->compare('user_id',$this->user_id);
		$criteria->compare('trip_id',$this->trip_id);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}