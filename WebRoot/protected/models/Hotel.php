<?php

/**
 * This is the model class for table "hotel".
 *
 * The followings are the available columns in table 'hotel':
 * @property integer $id
 * @property integer $city_id
 * @property integer $country_id
 * @property string $city_name
 * @property string $title
 * @property string $type
 * @property string $hotel_name_english
 * @property string $hotel_name_pinyin
 * @property string $cover_image
 * @property string $desc
 * @property string $html_content
 * @property string $phone
 * @property string $website
 * @property string $email
 * @property string $address
 * @property string $address_local
 * @property string $price
 * @property integer $rank
 * @property double $rating
 * @property string $intro
 * @property string $tags
 * @property string $open_time
 * @property string $traffic
 * @property string $lat
 * @property string $lon
 * @property string $area
 * @property string $recommend
 * @property string $agoda_url
 * @property integer $agoda_price
 * @property string $booking_url
 * @property integer $booking_price
 * @property integer $qgl_pin_id
 * @property string $ctime
 * @property string $mtime
 * @property integer $status
 */
class Hotel extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Hotel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'hotel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('city_id, country_id, title, type, mtime, status', 'required'),
			array('city_id, country_id, rank, agoda_price, booking_price, qgl_pin_id, status', 'numerical', 'integerOnly'=>true),
			array('rating', 'numerical'),
			array('city_name, title, type, hotel_name_english, hotel_name_pinyin, cover_image, phone, website, email, address, address_local, price, tags, open_time, traffic, area, recommend, agoda_url, booking_url', 'length', 'max'=>255),
			array('lat, lon', 'length', 'max'=>16),
			array('desc, html_content, intro, ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, city_id, country_id, city_name, title, type, hotel_name_english, hotel_name_pinyin, cover_image, desc, html_content, phone, website, email, address, address_local, price, rank, rating, intro, tags, open_time, traffic, lat, lon, area, recommend, agoda_url, agoda_price, booking_url, booking_price, qgl_pin_id, ctime, mtime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'city_id' => 'City',
			'country_id' => 'Country',
			'city_name' => 'City Name',
			'title' => 'Title',
			'type' => 'Type',
			'hotel_name_english' => 'Hotel Name English',
			'hotel_name_pinyin' => 'Hotel Name Pinyin',
			'cover_image' => 'Cover Image',
			'desc' => 'Desc',
			'html_content' => 'Html Content',
			'phone' => 'Phone',
			'website' => 'Website',
			'email' => 'Email',
			'address' => 'Address',
			'address_local' => 'Address Local',
			'price' => 'Price',
			'rank' => 'Rank',
			'rating' => 'Rating',
			'intro' => 'Intro',
			'tags' => 'Tags',
			'open_time' => 'Open Time',
			'traffic' => 'Traffic',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'area' => 'Area',
			'recommend' => 'Recommend',
			'agoda_url' => 'Agoda Url',
			'agoda_price' => 'Agoda Price',
			'booking_url' => 'Booking Url',
			'booking_price' => 'Booking Price',
			'qgl_pin_id' => 'Qgl Pin',
			'ctime' => 'Ctime',
			'mtime' => 'Mtime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('city_name',$this->city_name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('hotel_name_english',$this->hotel_name_english,true);
		$criteria->compare('hotel_name_pinyin',$this->hotel_name_pinyin,true);
		$criteria->compare('cover_image',$this->cover_image,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('html_content',$this->html_content,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('email',$this->email,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('address_local',$this->address_local,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('rank',$this->rank);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('intro',$this->intro,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('open_time',$this->open_time,true);
		$criteria->compare('traffic',$this->traffic,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('area',$this->area,true);
		$criteria->compare('recommend',$this->recommend,true);
		$criteria->compare('agoda_url',$this->agoda_url,true);
		$criteria->compare('agoda_price',$this->agoda_price);
		$criteria->compare('booking_url',$this->booking_url,true);
		$criteria->compare('booking_price',$this->booking_price);
		$criteria->compare('qgl_pin_id',$this->qgl_pin_id);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}