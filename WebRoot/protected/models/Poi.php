<?php

/**
 * This is the model class for table "poi".
 *
 * The followings are the available columns in table 'poi':
 * @property integer $poi_id
 * @property integer $city_id
 * @property integer $country_id
 * @property string $city_name
 * @property string $poi_name
 * @property string $type
 * @property string $poi_name_english
 * @property string $poi_name_local
 * @property string $poi_name_pinyin
 * @property string $cover_image
 * @property string $desc
 * @property string $html_content
 * @property string $phone
 * @property string $website
 * @property string $address
 * @property string $address_local
 * @property string $price
 * @property integer $rank
 * @property double $rating
 * @property string $intro
 * @property string $tags
 * @property string $open_time
 * @property string $traffic
 * @property string $lat
 * @property string $lon
 * @property string $area
 * @property string $recommend
 * @property string $tips
 * @property integer $qgl_pin_id
 * @property integer $is_private
 * @property string $source_url
 * @property string $ctime
 * @property string $mtime
 * @property integer $status
 */
class Poi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Poi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'poi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('poi_name, type, mtime, status', 'required'),
			array('city_id, country_id, rank, qgl_pin_id, is_private, status', 'numerical', 'integerOnly'=>true),
			array('rating', 'numerical'),
			array('city_name, poi_name, type, poi_name_english, poi_name_local, poi_name_pinyin, cover_image, phone, website, address, address_local, price, tags, open_time, traffic, area, recommend, source_url', 'length', 'max'=>255),
			array('lat, lon', 'length', 'max'=>16),
			array('desc, html_content, intro, tips, ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('poi_id, city_id, country_id, city_name, poi_name, type, poi_name_english, poi_name_local, poi_name_pinyin, cover_image, desc, html_content, phone, website, address, address_local, price, rank, rating, intro, tags, open_time, traffic, lat, lon, area, recommend, tips, qgl_pin_id, is_private, source_url, ctime, mtime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'poi_id' => 'Poi',
			'city_id' => 'City',
			'country_id' => 'Country',
			'city_name' => 'City Name',
			'poi_name' => 'Poi Name',
			'type' => 'Type',
			'poi_name_english' => 'Poi Name English',
			'poi_name_local' => 'Poi Name Local',
			'poi_name_pinyin' => 'Poi Name Pinyin',
			'cover_image' => 'Cover Image',
			'desc' => 'Desc',
			'html_content' => 'Html Content',
			'phone' => 'Phone',
			'website' => 'Website',
			'address' => 'Address',
			'address_local' => 'Address Local',
			'price' => 'Price',
			'rank' => 'Rank',
			'rating' => 'Rating',
			'intro' => 'Intro',
			'tags' => 'Tags',
			'open_time' => 'Open Time',
			'traffic' => 'Traffic',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'area' => 'Area',
			'recommend' => 'Recommend',
			'tips' => 'Tips',
			'qgl_pin_id' => 'Qgl Pin',
			'is_private' => 'Is Private',
			'source_url' => 'Source Url',
			'ctime' => 'Ctime',
			'mtime' => 'Mtime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('poi_id',$this->poi_id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('city_name',$this->city_name,true);
		$criteria->compare('poi_name',$this->poi_name,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('poi_name_english',$this->poi_name_english,true);
		$criteria->compare('poi_name_local',$this->poi_name_local,true);
		$criteria->compare('poi_name_pinyin',$this->poi_name_pinyin,true);
		$criteria->compare('cover_image',$this->cover_image,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('html_content',$this->html_content,true);
		$criteria->compare('phone',$this->phone,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('address_local',$this->address_local,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('rank',$this->rank);
		$criteria->compare('rating',$this->rating);
		$criteria->compare('intro',$this->intro,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('open_time',$this->open_time,true);
		$criteria->compare('traffic',$this->traffic,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('area',$this->area,true);
		$criteria->compare('recommend',$this->recommend,true);
		$criteria->compare('tips',$this->tips,true);
		$criteria->compare('qgl_pin_id',$this->qgl_pin_id);
		$criteria->compare('is_private',$this->is_private);
		$criteria->compare('source_url',$this->source_url,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}