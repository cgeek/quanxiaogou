<?php

/**
 * This is the model class for table "local_seller".
 *
 * The followings are the available columns in table 'local_seller':
 * @property string $id
 * @property string $version
 * @property string $logo
 * @property string $memo
 * @property string $name
 * @property string $area
 * @property string $contacts
 * @property string $contract_end
 * @property string $contract_start
 * @property string $date_created
 * @property string $introduce
 * @property string $last_updated
 * @property string $payment_account
 * @property string $payment_methods
 * @property string $problem
 * @property integer $sign_contract
 * @property integer $status_enum
 * @property string $tracker
 * @property string $type_enum
 * @property string $user_id
 * @property string $ways_of_cooperation
 * @property string $owner_id
 * @property string $background_image
 * @property string $localseller_introduce
 * @property string $localseller_weixin
 * @property string $local_seller_title
 * @property string $localsellerqq
 * @property string $bank_id
 * @property integer $evaluate_total_count
 * @property string $category
 * @property string $country
 * @property string $real_name
 * @property string $referee_name
 * @property string $referee_phone
 * @property string $youme_user_id
 * @property integer $agreement_status_enum
 */
class LocalSeller extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return LocalSeller the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'local_seller';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('version, name, date_created, last_updated, sign_contract, status_enum, type_enum, user_id', 'required'),
			array('sign_contract, status_enum, evaluate_total_count, agreement_status_enum', 'numerical', 'integerOnly'=>true),
			array('version, user_id, owner_id, bank_id, youme_user_id', 'length', 'max'=>20),
			array('logo, name, area, problem, tracker, type_enum, ways_of_cooperation, background_image, localseller_weixin, local_seller_title, localsellerqq, category, country, real_name, referee_name, referee_phone', 'length', 'max'=>255),
			array('contacts, introduce, payment_account, payment_methods', 'length', 'max'=>1000),
			array('memo, contract_end, contract_start, localseller_introduce', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, version, logo, memo, name, area, contacts, contract_end, contract_start, date_created, introduce, last_updated, payment_account, payment_methods, problem, sign_contract, status_enum, tracker, type_enum, user_id, ways_of_cooperation, owner_id, background_image, localseller_introduce, localseller_weixin, local_seller_title, localsellerqq, bank_id, evaluate_total_count, category, country, real_name, referee_name, referee_phone, youme_user_id, agreement_status_enum', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'version' => 'Version',
			'logo' => 'Logo',
			'memo' => 'Memo',
			'name' => 'Name',
			'area' => 'Area',
			'contacts' => 'Contacts',
			'contract_end' => 'Contract End',
			'contract_start' => 'Contract Start',
			'date_created' => 'Date Created',
			'introduce' => 'Introduce',
			'last_updated' => 'Last Updated',
			'payment_account' => 'Payment Account',
			'payment_methods' => 'Payment Methods',
			'problem' => 'Problem',
			'sign_contract' => 'Sign Contract',
			'status_enum' => 'Status Enum',
			'tracker' => 'Tracker',
			'type_enum' => 'Type Enum',
			'user_id' => 'User',
			'ways_of_cooperation' => 'Ways Of Cooperation',
			'owner_id' => 'Owner',
			'background_image' => 'Background Image',
			'localseller_introduce' => 'Localseller Introduce',
			'localseller_weixin' => 'Localseller Weixin',
			'local_seller_title' => 'Local Seller Title',
			'localsellerqq' => 'Localsellerqq',
			'bank_id' => 'Bank',
			'evaluate_total_count' => 'Evaluate Total Count',
			'category' => 'Category',
			'country' => 'Country',
			'real_name' => 'Real Name',
			'referee_name' => 'Referee Name',
			'referee_phone' => 'Referee Phone',
			'youme_user_id' => 'Youme User',
			'agreement_status_enum' => 'Agreement Status Enum',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);
		$criteria->compare('version',$this->version,true);
		$criteria->compare('logo',$this->logo,true);
		$criteria->compare('memo',$this->memo,true);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('area',$this->area,true);
		$criteria->compare('contacts',$this->contacts,true);
		$criteria->compare('contract_end',$this->contract_end,true);
		$criteria->compare('contract_start',$this->contract_start,true);
		$criteria->compare('date_created',$this->date_created,true);
		$criteria->compare('introduce',$this->introduce,true);
		$criteria->compare('last_updated',$this->last_updated,true);
		$criteria->compare('payment_account',$this->payment_account,true);
		$criteria->compare('payment_methods',$this->payment_methods,true);
		$criteria->compare('problem',$this->problem,true);
		$criteria->compare('sign_contract',$this->sign_contract);
		$criteria->compare('status_enum',$this->status_enum);
		$criteria->compare('tracker',$this->tracker,true);
		$criteria->compare('type_enum',$this->type_enum,true);
		$criteria->compare('user_id',$this->user_id,true);
		$criteria->compare('ways_of_cooperation',$this->ways_of_cooperation,true);
		$criteria->compare('owner_id',$this->owner_id,true);
		$criteria->compare('background_image',$this->background_image,true);
		$criteria->compare('localseller_introduce',$this->localseller_introduce,true);
		$criteria->compare('localseller_weixin',$this->localseller_weixin,true);
		$criteria->compare('local_seller_title',$this->local_seller_title,true);
		$criteria->compare('localsellerqq',$this->localsellerqq,true);
		$criteria->compare('bank_id',$this->bank_id,true);
		$criteria->compare('evaluate_total_count',$this->evaluate_total_count);
		$criteria->compare('category',$this->category,true);
		$criteria->compare('country',$this->country,true);
		$criteria->compare('real_name',$this->real_name,true);
		$criteria->compare('referee_name',$this->referee_name,true);
		$criteria->compare('referee_phone',$this->referee_phone,true);
		$criteria->compare('youme_user_id',$this->youme_user_id,true);
		$criteria->compare('agreement_status_enum',$this->agreement_status_enum);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}