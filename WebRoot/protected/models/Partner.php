<?php

/**
 * This is the model class for table "partner".
 *
 * The followings are the available columns in table 'partner':
 * @property integer $id
 * @property string $username
 * @property string $nick_name
 * @property string $password
 * @property string $company
 * @property string $company_intro
 * @property string $company_logo
 * @property string $remark
 * @property string $last_login_time
 * @property string $ctime
 * @property integer $mtime
 * @property integer $status
 */
class Partner extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Partner the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'partner';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('username, password', 'required'),
			array('mtime, status', 'numerical', 'integerOnly'=>true),
			array('username, nick_name, password, company, company_logo', 'length', 'max'=>255),
			array('company_intro, remark, last_login_time, ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, username, nick_name, password, company, company_intro, company_logo, remark, last_login_time, ctime, mtime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'username' => 'Username',
			'nick_name' => 'Nick Name',
			'password' => 'Password',
			'company' => 'Company',
			'company_intro' => 'Company Intro',
			'company_logo' => 'Company Logo',
			'remark' => 'Remark',
			'last_login_time' => 'Last Login Time',
			'ctime' => 'Ctime',
			'mtime' => 'Mtime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('username',$this->username,true);
		$criteria->compare('nick_name',$this->nick_name,true);
		$criteria->compare('password',$this->password,true);
		$criteria->compare('company',$this->company,true);
		$criteria->compare('company_intro',$this->company_intro,true);
		$criteria->compare('company_logo',$this->company_logo,true);
		$criteria->compare('remark',$this->remark,true);
		$criteria->compare('last_login_time',$this->last_login_time,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('mtime',$this->mtime);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}