<?php

/**
 * This is the model class for table "city".
 *
 * The followings are the available columns in table 'city':
 * @property integer $id
 * @property string $name
 * @property string $name_english
 * @property string $name_pinyin
 * @property integer $parent_id
 * @property integer $country_id
 * @property string $country_name
 * @property integer $type
 * @property string $cover_image
 * @property string $desc
 * @property string $lat
 * @property string $lon
 * @property string $mtime
 * @property string $ctime
 * @property integer $status
 */
class City extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return City the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'city';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, country_id', 'required'),
			array('parent_id, country_id, type, status', 'numerical', 'integerOnly'=>true),
			array('name, name_english, name_pinyin, country_name, cover_image', 'length', 'max'=>255),
			array('lat, lon', 'length', 'max'=>16),
			array('desc, mtime, ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, name, name_english, name_pinyin, parent_id, country_id, country_name, type, cover_image, desc, lat, lon, mtime, ctime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'name' => 'Name',
			'name_english' => 'Name English',
			'name_pinyin' => 'Name Pinyin',
			'parent_id' => 'Parent',
			'country_id' => 'Country',
			'country_name' => 'Country Name',
			'type' => 'Type',
			'cover_image' => 'Cover Image',
			'desc' => 'Desc',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'mtime' => 'Mtime',
			'ctime' => 'Ctime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('name_english',$this->name_english,true);
		$criteria->compare('name_pinyin',$this->name_pinyin,true);
		$criteria->compare('parent_id',$this->parent_id);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('country_name',$this->country_name,true);
		$criteria->compare('type',$this->type);
		$criteria->compare('cover_image',$this->cover_image,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}