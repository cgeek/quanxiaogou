<?php

/**
 * This is the model class for table "admin_push_token".
 *
 * The followings are the available columns in table 'admin_push_token':
 * @property string $id
 * @property string $push_token
 * @property string $admin
 */
class AdminPushToken extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @return AdminPushToken the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'admin_push_token';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('push_token', 'length', 'max'=>80),
			array('admin', 'length', 'max'=>20),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, push_token, admin', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'Id',
			'push_token' => 'Push Token',
			'admin' => 'Admin',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id,true);

		$criteria->compare('push_token',$this->push_token,true);

		$criteria->compare('admin',$this->admin,true);

		return new CActiveDataProvider('AdminPushToken', array(
			'criteria'=>$criteria,
		));
	}
}