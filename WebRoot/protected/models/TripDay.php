<?php

/**
 * This is the model class for table "trip_day".
 *
 * The followings are the available columns in table 'trip_day':
 * @property integer $id
 * @property integer $trip_id
 * @property string $date
 * @property string $title
 * @property string $desc
 * @property string $traffic_note
 * @property integer $city_id
 * @property string $city_ids
 * @property integer $user_id
 * @property string $note
 * @property integer $sort
 * @property integer $status
 */
class TripDay extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TripDay the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trip_day';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('date', 'required'),
			array('trip_id, city_id, user_id, sort, status', 'numerical', 'integerOnly'=>true),
			array('title, city_ids', 'length', 'max'=>255),
			array('desc, traffic_note, note', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, trip_id, date, title, desc, traffic_note, city_id, city_ids, user_id, note, sort, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'trip_id' => 'Trip',
			'date' => 'Date',
			'title' => 'Title',
			'desc' => 'Desc',
			'traffic_note' => 'Traffic Note',
			'city_id' => 'City',
			'city_ids' => 'City Ids',
			'user_id' => 'User',
			'note' => 'Note',
			'sort' => 'Sort',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('trip_id',$this->trip_id);
		$criteria->compare('date',$this->date,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('traffic_note',$this->traffic_note,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('city_ids',$this->city_ids,true);
		$criteria->compare('user_id',$this->user_id);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}