<?php

/**
 * This is the model class for table "note".
 *
 * The followings are the available columns in table 'note':
 * @property integer $id
 * @property integer $city_id
 * @property integer $country_id
 * @property string $city_name
 * @property string $title
 * @property string $type
 * @property string $cover_image
 * @property string $content
 * @property string $website
 * @property string $tags
 * @property string $lat
 * @property string $lon
 * @property integer $qgl_pin_id
 * @property string $ctime
 * @property string $mtime
 * @property integer $status
 */
class Note extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Note the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'note';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('city_id, country_id, title, type, mtime, status', 'required'),
			array('city_id, country_id, qgl_pin_id, status', 'numerical', 'integerOnly'=>true),
			array('city_name, title, type, cover_image, website, tags', 'length', 'max'=>255),
			array('lat, lon', 'length', 'max'=>16),
			array('content, ctime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, city_id, country_id, city_name, title, type, cover_image, content, website, tags, lat, lon, qgl_pin_id, ctime, mtime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'city_id' => 'City',
			'country_id' => 'Country',
			'city_name' => 'City Name',
			'title' => 'Title',
			'type' => 'Type',
			'cover_image' => 'Cover Image',
			'content' => 'Content',
			'website' => 'Website',
			'tags' => 'Tags',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'qgl_pin_id' => 'Qgl Pin',
			'ctime' => 'Ctime',
			'mtime' => 'Mtime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('country_id',$this->country_id);
		$criteria->compare('city_name',$this->city_name,true);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('cover_image',$this->cover_image,true);
		$criteria->compare('content',$this->content,true);
		$criteria->compare('website',$this->website,true);
		$criteria->compare('tags',$this->tags,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('qgl_pin_id',$this->qgl_pin_id);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}