<?php

/**
 * This is the model class for table "trip_day_poi".
 *
 * The followings are the available columns in table 'trip_day_poi':
 * @property integer $id
 * @property integer $trip_id
 * @property integer $trip_day_id
 * @property integer $poi_id
 * @property string $type
 * @property string $poi_name
 * @property string $desc
 * @property string $cover_image
 * @property string $price
 * @property string $currency
 * @property string $note
 * @property string $lat
 * @property string $lon
 * @property integer $sort
 * @property string $mtime
 * @property string $ctime
 * @property integer $status
 */
class TripDayPoi extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TripDayPoi the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trip_day_poi';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('poi_name, ctime', 'required'),
			array('trip_id, trip_day_id, poi_id, sort, status', 'numerical', 'integerOnly'=>true),
			array('type, poi_name, cover_image, price, currency', 'length', 'max'=>255),
			array('lat, lon', 'length', 'max'=>16),
			array('desc, note, mtime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, trip_id, trip_day_id, poi_id, type, poi_name, desc, cover_image, price, currency, note, lat, lon, sort, mtime, ctime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'trip_id' => 'Trip',
			'trip_day_id' => 'Trip Day',
			'poi_id' => 'Poi',
			'type' => 'Type',
			'poi_name' => 'Poi Name',
			'desc' => 'Desc',
			'cover_image' => 'Cover Image',
			'price' => 'Price',
			'currency' => 'Currency',
			'note' => 'Note',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'sort' => 'Sort',
			'mtime' => 'Mtime',
			'ctime' => 'Ctime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('trip_id',$this->trip_id);
		$criteria->compare('trip_day_id',$this->trip_day_id);
		$criteria->compare('poi_id',$this->poi_id);
		$criteria->compare('type',$this->type,true);
		$criteria->compare('poi_name',$this->poi_name,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('cover_image',$this->cover_image,true);
		$criteria->compare('price',$this->price,true);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}