<?php

/**
 * This is the model class for table "demo_trip_day".
 *
 * The followings are the available columns in table 'demo_trip_day':
 * @property integer $id
 * @property integer $source_trip_day_id
 * @property string $title
 * @property string $desc
 * @property string $traffic_note
 * @property integer $city_id
 * @property integer $admin_id
 * @property string $note
 * @property integer $sort
 * @property string $ctime
 * @property string $mtime
 * @property integer $status
 */
class DemoTripDay extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return DemoTripDay the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'demo_trip_day';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('source_trip_day_id, city_id, admin_id, sort, status', 'numerical', 'integerOnly'=>true),
			array('title', 'length', 'max'=>255),
			array('desc, traffic_note, note, ctime, mtime', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, source_trip_day_id, title, desc, traffic_note, city_id, admin_id, note, sort, ctime, mtime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'source_trip_day_id' => 'Source Trip Day',
			'title' => 'Title',
			'desc' => 'Desc',
			'traffic_note' => 'Traffic Note',
			'city_id' => 'City',
			'admin_id' => 'Admin',
			'note' => 'Note',
			'sort' => 'Sort',
			'ctime' => 'Ctime',
			'mtime' => 'Mtime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('source_trip_day_id',$this->source_trip_day_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('traffic_note',$this->traffic_note,true);
		$criteria->compare('city_id',$this->city_id);
		$criteria->compare('admin_id',$this->admin_id);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('sort',$this->sort);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}