<?php

/**
 * This is the model class for table "alipay_record".
 *
 * The followings are the available columns in table 'alipay_record':
 * @property integer $id
 * @property string $alipay_order_no
 * @property string $merchant_order_no
 * @property string $order_title
 * @property string $order_status
 * @property string $order_type
 * @property string $order_from
 * @property double $total_amount
 * @property double $service_charge
 * @property string $in_out_type
 * @property string $owner_user_id
 * @property string $create_time
 * @property string $payment_time
 * @property string $modified_time
 * @property string $ctime
 * @property integer $customer_id
 * @property integer $order_id
 * @property integer $status
 */
class AlipayRecord extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return AlipayRecord the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'alipay_record';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('alipay_order_no, ctime', 'required'),
			array('customer_id, order_id, status', 'numerical', 'integerOnly'=>true),
			array('total_amount, service_charge', 'numerical'),
			array('alipay_order_no, merchant_order_no, order_title, order_status, order_type, order_from, in_out_type, owner_user_id', 'length', 'max'=>255),
			array('create_time, payment_time, modified_time', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, alipay_order_no, merchant_order_no, order_title, order_status, order_type, order_from, total_amount, service_charge, in_out_type, owner_user_id, create_time, payment_time, modified_time, ctime, customer_id, order_id, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'alipay_order_no' => 'Alipay Order No',
			'merchant_order_no' => 'Merchant Order No',
			'order_title' => 'Order Title',
			'order_status' => 'Order Status',
			'order_type' => 'Order Type',
			'order_from' => 'Order From',
			'total_amount' => 'Total Amount',
			'service_charge' => 'Service Charge',
			'in_out_type' => 'In Out Type',
			'owner_user_id' => 'Owner User',
			'create_time' => 'Create Time',
			'payment_time' => 'Payment Time',
			'modified_time' => 'Modified Time',
			'ctime' => 'Ctime',
			'customer_id' => 'Customer',
			'order_id' => 'Order',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('alipay_order_no',$this->alipay_order_no,true);
		$criteria->compare('merchant_order_no',$this->merchant_order_no,true);
		$criteria->compare('order_title',$this->order_title,true);
		$criteria->compare('order_status',$this->order_status,true);
		$criteria->compare('order_type',$this->order_type,true);
		$criteria->compare('order_from',$this->order_from,true);
		$criteria->compare('total_amount',$this->total_amount);
		$criteria->compare('service_charge',$this->service_charge);
		$criteria->compare('in_out_type',$this->in_out_type,true);
		$criteria->compare('owner_user_id',$this->owner_user_id,true);
		$criteria->compare('create_time',$this->create_time,true);
		$criteria->compare('payment_time',$this->payment_time,true);
		$criteria->compare('modified_time',$this->modified_time,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('customer_id',$this->customer_id);
		$criteria->compare('order_id',$this->order_id);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}