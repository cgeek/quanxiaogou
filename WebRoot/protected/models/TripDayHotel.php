<?php

/**
 * This is the model class for table "trip_day_hotel".
 *
 * The followings are the available columns in table 'trip_day_hotel':
 * @property integer $id
 * @property integer $trip_id
 * @property integer $trip_day_id
 * @property integer $hotel_id
 * @property string $title
 * @property string $note
 * @property string $cover_image
 * @property string $currency
 * @property double $spend
 * @property integer $ticketcount
 * @property integer $type
 * @property string $desc
 * @property string $address
 * @property string $lat
 * @property string $lon
 * @property string $mtime
 * @property string $ctime
 * @property integer $status
 */
class TripDayHotel extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return TripDayHotel the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'trip_day_hotel';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('trip_day_id, title, mtime, ctime, status', 'required'),
			array('trip_id, trip_day_id, hotel_id, ticketcount, type, status', 'numerical', 'integerOnly'=>true),
			array('spend', 'numerical'),
			array('title, cover_image, currency, address', 'length', 'max'=>255),
			array('lat, lon', 'length', 'max'=>16),
			array('note, desc', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, trip_id, trip_day_id, hotel_id, title, note, cover_image, currency, spend, ticketcount, type, desc, address, lat, lon, mtime, ctime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'trip_id' => 'Trip',
			'trip_day_id' => 'Trip Day',
			'hotel_id' => 'Hotel',
			'title' => 'Title',
			'note' => 'Note',
			'cover_image' => 'Cover Image',
			'currency' => 'Currency',
			'spend' => 'Spend',
			'ticketcount' => 'Ticketcount',
			'type' => 'Type',
			'desc' => 'Desc',
			'address' => 'Address',
			'lat' => 'Lat',
			'lon' => 'Lon',
			'mtime' => 'Mtime',
			'ctime' => 'Ctime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('trip_id',$this->trip_id);
		$criteria->compare('trip_day_id',$this->trip_day_id);
		$criteria->compare('hotel_id',$this->hotel_id);
		$criteria->compare('title',$this->title,true);
		$criteria->compare('note',$this->note,true);
		$criteria->compare('cover_image',$this->cover_image,true);
		$criteria->compare('currency',$this->currency,true);
		$criteria->compare('spend',$this->spend);
		$criteria->compare('ticketcount',$this->ticketcount);
		$criteria->compare('type',$this->type);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('address',$this->address,true);
		$criteria->compare('lat',$this->lat,true);
		$criteria->compare('lon',$this->lon,true);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}