<?php

/**
 * This is the model class for table "room".
 *
 * The followings are the available columns in table 'room':
 * @property integer $id
 * @property integer $package_id
 * @property string $name
 * @property string $english_name
 * @property string $cover_image
 * @property integer $price
 * @property integer $agent_price
 * @property string $desc
 * @property string $facilities
 * @property string $offerings
 * @property integer $size
 * @property string $view
 * @property string $bed
 * @property string $mtime
 * @property string $ctime
 * @property integer $status
 */
class Room extends CActiveRecord
{
	/**
	 * Returns the static model of the specified AR class.
	 * @param string $className active record class name.
	 * @return Room the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'room';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('name, mtime, ctime', 'required'),
			array('package_id, price, agent_price, size, status', 'numerical', 'integerOnly'=>true),
			array('name, english_name, cover_image, desc, view, bed', 'length', 'max'=>255),
			array('facilities, offerings', 'safe'),
			// The following rule is used by search().
			// Please remove those attributes that should not be searched.
			array('id, package_id, name, english_name, cover_image, price, agent_price, desc, facilities, offerings, size, view, bed, mtime, ctime, status', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'id' => 'ID',
			'package_id' => 'Package',
			'name' => 'Name',
			'english_name' => 'English Name',
			'cover_image' => 'Cover Image',
			'price' => 'Price',
			'agent_price' => 'Agent Price',
			'desc' => 'Desc',
			'facilities' => 'Facilities',
			'offerings' => 'Offerings',
			'size' => 'Size',
			'view' => 'View',
			'bed' => 'Bed',
			'mtime' => 'Mtime',
			'ctime' => 'Ctime',
			'status' => 'Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 * @return CActiveDataProvider the data provider that can return the models based on the search/filter conditions.
	 */
	public function search()
	{
		// Warning: Please modify the following code to remove attributes that
		// should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('id',$this->id);
		$criteria->compare('package_id',$this->package_id);
		$criteria->compare('name',$this->name,true);
		$criteria->compare('english_name',$this->english_name,true);
		$criteria->compare('cover_image',$this->cover_image,true);
		$criteria->compare('price',$this->price);
		$criteria->compare('agent_price',$this->agent_price);
		$criteria->compare('desc',$this->desc,true);
		$criteria->compare('facilities',$this->facilities,true);
		$criteria->compare('offerings',$this->offerings,true);
		$criteria->compare('size',$this->size);
		$criteria->compare('view',$this->view,true);
		$criteria->compare('bed',$this->bed,true);
		$criteria->compare('mtime',$this->mtime,true);
		$criteria->compare('ctime',$this->ctime,true);
		$criteria->compare('status',$this->status);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}
}