<?php
date_default_timezone_set('Asia/Shanghai');
ini_set('upload_max_filesize', '10M');

defined('DS') or define('DS',DIRECTORY_SEPARATOR);
function app() 
{
	return Yii::app();
}

function request() 
{
	return Yii::app()->request;
}

function user() 
{
	return Yii::app()->user;
}
function search()
{
	return Yii::app()->search;
}
function taobao()
{
	return Yii::app()->taobao;
}

function h($text)
{
	return htmlspecialchars($text,ENT_QUOTES,Yii::app()->charset);
}

function l($text, $url = '#', $htmlOptions = array()) 
{
	return CHtml::link($text, $url, $htmlOptions);
}

function debug_json($data)
{
	echo json_encode($data);
	die();
}

/*
Utf-8、gb2312都支持的汉字截取函数
cut_str(字符串, 截取长度, 开始长度, 编码);
编码默认为 utf-8
开始长度默认为 0
*/
function cut_str($string, $sublen, $start = 0, $code = 'UTF-8')
{
	if($code == 'UTF-8')
	{
		$pa ="/[\x01-\x7f]|[\xc2-\xdf][\x80-\xbf]|\xe0[\xa0-\xbf][\x80-\xbf]|[\xe1-\xef][\x80-\xbf][\x80-\xbf]|\xf0[\x90-\xbf][\x80-\xbf][\x80-\xbf]|[\xf1-\xf7][\x80-\xbf][\x80-\xbf][\x80-\xbf]/";
		preg_match_all($pa, $string, $t_string);        if(count($t_string[0]) - $start > $sublen) return join('', array_slice($t_string[0], $start, $sublen))."...";
		return join('', array_slice($t_string[0], $start, $sublen));
	}
	else
	{
		$start = $start*2;
		$sublen = $sublen*2;
		$strlen = strlen($string);
		$tmpstr = '';        for($i=0; $i<$strlen; $i++)
	{
		if($i>=$start && $i<($start+$sublen))
		{
			if(ord(substr($string, $i, 1))>129)
			{
				$tmpstr.= substr($string, $i, 2);
			}
			else
			{
				$tmpstr.= substr($string, $i, 1);
			}
		}
		if(ord(substr($string, $i, 1))>129) $i++;
	}
		if(strlen($tmpstr)<$strlen ) $tmpstr.= "...";
		return $tmpstr;
	}
}

//format time to human
function human_time($small_ts, $large_ts=false) {
	if(!$large_ts) $large_ts = time();
	$n = $large_ts - $small_ts;
	
	if($n <= -(3600*24)) return round((-$n)/(3600*24)) . '天后';
	if($n <= 1) return '1 秒前';
	if($n < (60)) return $n . ' 秒前';
	if($n < (60*60)) { $minutes = round($n/60); return '' . $minutes . '分钟' .'前'; }
	if($n < (60*60*16)) { $hours = round($n/(60*60)); return '' . $hours . '小时' . '前'; }
		if($n < (time() - strtotime('yesterday'))) return '昨天';
	if($n < (60*60*24)) { $hours = round($n/(60*60)); return '' . $hours . '小时' . '前'; }
		if($n < (60*60*24*6.5)) return '' . round($n/(60*60*24)) . '天前';
	if($n < (time() - strtotime('last week'))) return '上周';
	if(round($n/(60*60*24*7))  == 1) return '一周前';
	if($n < (60*60*24*7*3.5)) return '' . round($n/(60*60*24*7)) . '周前';
	if($n < (time() - strtotime('last month'))) return '上个月';
	if(round($n/(60*60*24*7*4))  == 1) return '一个月前';
	if($n < (60*60*24*7*4*11.5)) return '' . round($n/(60*60*24*7*4)) . '月前';
	if($n < (time() - strtotime('last year'))) return '去年';
	if(round($n/(60*60*24*7*52)) == 1) return '一年以前';
	if($n >= (60*60*24*7*4*12)) return '' . round($n/(60*60*24*7*52)) . '年前'; 
	return false;
}
function human_time_admin_hp($ts) {

	$n = $ts - time();
	if($n > 60*3600*24) {
		if(date('Y',time()) == date('Y', $ts))
		{
			return '今年' . date('n', $ts) . '月';
		}
		else if(date('Y',time()) == (date('Y', $ts) - 1))
		{
			return '明年' . date('n', $ts) . '月';
		}
		return round($n/(3600*24)) . '天后';
	}
	if($n > 3600*24) return round($n/(3600*24)) . '天后';
	if(($n < 3600*24) && ($n > 0)) {
		$d = date('Y-m-d', $ts);
		$d2 = date('Y-m-d', time());
		if($d == $d2) {
			return '今天';
		} else {
			return '明天';
		}
	}
	if($n < 0)
	{
		return human_time($ts);
	}
	return '';
}

function filterHtml($str)
{
	$str=str_replace(" ", '', $str);
	$str=str_replace("\n", '', $str);
	$str=str_replace("\t", '', $str);
	$str=str_replace("::", ':', $str);
	$str=str_replace(" ", '', $str);
	return $str;
}

function objectToArray(&$object){ 
	$object = (array)$object;
	foreach ($object as $key => $value) {   
		if (is_object($value)||is_array($value)) {    
			objectToArray($value);    
			$object[$key] = $value;   }   
	}
	return $object;
}
function orderTypeCN($type) {
	$array = array(
		'hotel' => '酒店',
		'flight' => '机票',
		'chartered' => '包车',
		'activity' => '活动',
		'pickup' => '接机',
		'insure' => '保险'
	);
	if(isset($array[$type])) {
		return $array[$type];
	} else {
		return '';
	}
}

function customerStatus()
{
    /*
	$array = array(
		'0' => '未认领',
		'1' => '沟通需求中',
		'2' => '规划行程中',
		'3' => '细化行程中',
		'4' => '行程设计已完成',
		'5' => '已完成',
		'-1' => '已删除',
		'-2' => '已取消'
    );
    */
	$array = array(
		'0' => '未认领',
		'1' => '已认领',
		'2' => '第一次联系完成',
		'3' => '需求沟通完成',
		'4' => '初步行程完成',
		'5' => '详细行程完成',
		'6' => '反馈完成',
		'-1' => '已删除',
		'-2' => '已取消'
	);
	return $array;
}

function customerStatusCN($type) {
	$array = customerStatus();
	if(isset($array[$type])) {
		return $array[$type];
	} else {
		return '';
	}
}

function customerStatusColor($status)
{
	$array = array(
		'0' => 'worning',
		'1' => 'default',
		'2' => 'primary',
		'3' => 'info',
		'4' => 'success',
		'5' => 'success',
		'-1' => 'danger',
		'-2' => 'danger'
	);
	if(isset($array[$status])) {
		return $array[$status];
	} else {
		return '';
	}
}

function getTradeStatusName($status) 
{
	$array = array(
		'TRADE_NO_CREATE_PAY' => '没有创建支付宝交易',
		'WAIT_BUYER_PAY' => '等待买家付款',
		'WAIT_SELLER_SEND_GOODS' => '买家已付款',
		'WAIT_BUYER_CONFIRM_GOODS' => '等待买家确认收货',
		'TRADE_BUYER_SIGNED' => '买家已签收',
		'TRADE_FINISHED' => '交易成功',
		'TRADE_CLOSED' => '交易自动关闭',
		'TRADE_CLOSED_BY_TAOBAO' => '关闭交易',
	);

	if(isset($array[$status])) {
		return $array[$status];
	} else {
		return NULL;
	}
}

function ajax_response($code = 200, $message="",$data = array())
{
	header('Content-Type: application/json; charset=utf-8');
	$result['code'] = $code;
	//$result['success'] = $success;
	$result['message'] = $message;
	$result['data'] = $data;
	echo json_encode($result);
	exit();
}

function getURLParams($url)
{
	$result = array();
	$mr = preg_match_all('/(\?|&)(.+?)=([^&?]*)/i', $url, $matchs);

	if ($mr !== FALSE) {
		for ($i = 0; $i < $mr; $i++) {
			$result[$matchs[2][$i]] = $matchs[3][$i];
		}
	}
	return $result;
}

//判断是否是URL地址
function isValidURL($url) 
{
	if(empty($url)) {
		return FALSE;
	}
	$array = parse_url($url);
	if(!empty($array['host'])) {
		return TRUE;
	}
	return FALSE;
}
//判断是否是upyun的地址
function is_upyun_url($url)
{
	if(preg_match('/^http:\/\/xiaogou.b0.upaiyun.com\/*/i', $url)) {
		return TRUE;
	} else {
		return FALSE;
	}
}

function get_image_hash_from_up_url($url)
{
	preg_match('/^http:\/\/xiaogou.b0.upaiyun.com\/([a-f0-9]{32}).*.jpg$/i', $url, $matches);
	if(empty($matches))
		return FALSE;

	$image_hash = $matches[1];
	return $image_hash;
}


/*
function upimage($image_hash, $size = '160x160')
{
	if(empty($image_hash) || $image_hash == md5(''))
	{
		return '';
	}
	if(strlen($image_hash) != 32) {
		return $image_hash;
	}
	if($size == 'org') {
		return "http://xiaogoulvxing.b0.upaiyun.com/{$image_hash}";
	}
	return "http://xiaogoulvxing.b0.upaiyun.com/{$image_hash}_{$size}.jpg";
}
*/
function upimage($image_hash, $size = 'w160h120')
{
    if(empty($image_hash) || $image_hash == md5(''))
    {
        return '';
    }
    if(strlen($image_hash) != 32) {
        return $image_hash;
    }
    if($size == 'org') {
        return "http://miaomiao.b0.upaiyun.com/{$image_hash}.jpg";
    }
    return "http://miaomiao.b0.upaiyun.com/{$image_hash}.jpg!{$size}";
}

//获得图片信息
function get_image_info($image_str = '', $path = '')
{
	$image_info = array();
	if(!empty($image_str)) {
		$image_info = get_image_info_from_string($image_str);
	}

	if(!empty($path)) {
		$exif = get_image_exif($path);
		if(!empty($exif)) {
			$image_info = array_merge($image_info, $exif);
		}
	}
	return $image_info;
}

function get_image_info_from_string($image)
{
	if(empty($image)) {
		return NULL;
	}
	$image_hash = md5($image);
	$imgObj = imagecreatefromstring($image);
	if(false == $imgObj)
	{
		//e("get wrong img");
		return FALSE;
	}
	$width = imagesx($imgObj);
	$height = imagesy($imgObj);
	$image_info = array('image_hash' => $image_hash, 'width' => $width, 'height' => $height);

	return $image_info;
}

//取得exif信息
function get_image_exif($path) 
{
	$result = array();
	if(exif_imagetype($path) != 'jpg' || exif_imagetype($path) != 'jpeg') {
		return $result;
	}
	if(function_exists('exif_read_data')) {
		$exif = exif_read_data($path, 0, true);
		if(!empty($exif['GPS'])) {
			$result['lat'] = getGps($exif['GPS']['GPSLatitude'], $exif['GPS']['GPSLongitudeRef']);
			$result['lon'] = getGps($exif['GPS']['GPSLongitude'], $exif['GPS']['GPSLongitudeRef']);
		}
		$result['exif'] = serialize($exif);
	}
	return $result;
}

function getGps($exifCoord,$banqiu)
{
	$degrees= count($exifCoord) > 0 ? gps2Num($exifCoord[0]) : 0;
	$minutes= count($exifCoord) > 1 ? gps2Num($exifCoord[1]) : 0;
	$seconds= count($exifCoord) > 2 ? gps2Num($exifCoord[2]) : 0;
	//normalize
	$minutes+= 60 * ($degrees- floor($degrees));
	$degrees= floor($degrees);
	$seconds+= 60 * ($minutes- floor($minutes));
	$minutes= floor($minutes);
	if($seconds>= 60)
	{
		$minutes+= floor($seconds/60.0);
		$seconds-= 60*floor($seconds/60.0);
	}
	if($minutes>= 60)
	{
		$degrees+= floor($minutes/60.0);
		$minutes-= 60*floor($minutes/60.0);
	}
	$lng_lat = $degrees + $minutes/60 + $seconds/60/60;
	if(strtoupper($banqiu) == 'W' || strtoupper($banqiu) == 'S'){
		//如果是南半球 或者 西半球 乘以-1
		$lng_lat = $lng_lat * -1;
	}
	return $lng_lat;
	//return array('degrees'=> $degrees, 'minutes'=> $minutes, 'seconds'=> $seconds);
}
/*
取得EXIF的內容
分数 转 小数
 */
function gps2Num($coordPart)
{
	$parts= explode('/', $coordPart);
	if(count($parts) <= 0)
		return 0;
	if(count($parts) == 1)
		return $parts[0];
	return floatval($parts[0]) / floatval($parts[1]);
}

function guess_refer_from_url($url)
{
	$refer_patten = array(
		'baidu.com' => 'http://lvyou.baidu.com/notes/view/25dd0606a7048b25ce7182ad',
		'mafengwo' => 'http://www.mafengwo.cn',
		'sinaimg.cn' => 'http://blog.sina.com.cn/u/1300871220',
		'16fan.com' => 'http://www.16fan.com/',
	);
	foreach($refer_patten as $patten=>$refer)
	{
		if(strpos($url, $patten))
		{
			return $refer;
		}
	}

	$refer = parse_url($url);
	$refer = 'http://' . $refer['host']. "/";
	return $refer;
}

function curl_get($url, $refer='')
{
	$refer = (empty($refer)) ? guess_refer_from_url($url) : $refer;

	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"); 
	curl_setopt($ch, CURLOPT_REFERER,$refer); 
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION,TRUE); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
	curl_setopt($ch, CURLOPT_TIMEOUT, 30); 

	$data = curl_exec($ch); 

	if(empty($data))
	{
		//e("[CURL]can not curl $url, errno:" . curl_errno($ch). " error:" . curl_error($ch). " try using proxy ... ");
		//$data = curl_get_with_proxy($url, $refer);
	}
	curl_close($ch); 
	return $data;
}

function curl_post($url, $data, $refer='')
{
	$refer = (empty($refer)) ? guess_refer_from_url($url) : $refer;

	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $url); 
    curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"); 
    if(!empty($refer)) {
        curl_setopt($ch, CURLOPT_REFERER,$refer); 
    }
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
    curl_setopt($ch, CURLOPT_POST, true);  
    curl_setopt($ch, CURLOPT_POSTFIELDS, $data);

	$r = curl_exec($ch); 
	if(empty($r))
	{
	    //	var_dump("[CURL]can not curl $url, errno:" . curl_errno($ch). " error:" . curl_error($ch). " try using proxy ... ");
		//$data = curl_get_with_proxy($url, $refer);
	}
	curl_close($ch); 
	return $r;
}

function curl_get_with_proxy($url, $refer='')
{
	$crypted_url = my_encode($url);
	$crypted_refer = my_encode($refer);
	$url = "http://cgeek.org/image.php?url={$crypted_url}&refer={$crypted_refer}";

	$ch = curl_init(); 
	curl_setopt($ch, CURLOPT_URL, $url); 
	curl_setopt($ch, CURLOPT_USERAGENT, "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1)"); 
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, TRUE); 
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE); 
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 5); 
	curl_setopt($ch, CURLOPT_TIMEOUT, 30); 

	$data = curl_exec($ch); 

	if(empty($data))
	{
		//e("[CURL PROXY]can not curl $url, errno:" . curl_errno($ch). " error:" . curl_error($ch));
	}

	curl_close($ch); 
	return $data;
}

function birthday($birthday){
	$age = strtotime($birthday);
	if($age === false){
		return false;
	}
	list($y1,$m1,$d1) = explode("-",date("Y-m-d",$age));
	$now = strtotime("now");
	list($y2,$m2,$d2) = explode("-",date("Y-m-d",$now));
	$age = $y2 - $y1;
	return $age + 1;
}

/**
 * 短信发送接口（螺丝帽）
 *
 */
function sendSMS($mobile, $text)
{
	if(empty($mobile) || empty($text)) {
		//die('mobile or text null');
		return false;
	}
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL, "https://sms-api.luosimao.com/v1/send.json");
	curl_setopt($ch, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_1_0);
	curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 30);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
	curl_setopt($ch, CURLOPT_HEADER, FALSE);
	curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
	curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 2);
	curl_setopt($ch, CURLOPT_SSLVERSION , 3);
	curl_setopt($ch, CURLOPT_HTTPAUTH , CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD  , 'api:key-2fb13c989eea57f27e22d7a9769a3e3e');
	curl_setopt($ch, CURLOPT_POST, TRUE);
	curl_setopt($ch, CURLOPT_POSTFIELDS, array('mobile' => $mobile, 'message' => $text . '【妙妙行程管家】'));
	$res = curl_exec( $ch );
	//$res  = curl_error( $ch );
	curl_close( $ch );
	return true;
}

function getWeek($date1, $short = false)
{
	$datearr = explode("-",$date1);     //将传来的时间使用“-”分割成数组
	$year = $datearr[0];       //获取年份
	$month = sprintf('%02d',$datearr[1]);  //获取月份 
	$day = sprintf('%02d',$datearr[2]);      //获取日期
	$hour = $minute = $second = 0;   //默认时分秒均为0
	$dayofweek = mktime($hour,$minute,$second,$month,$day,$year);    //将时间转换成时间戳
	$shuchu = date("w",$dayofweek);      //获取星期值
	$weekarray=array("星期日","星期一","星期二","星期三","星期四","星期五","星期六");
    if($short) {
        $weekarray=array("周日","周一","周二","周三","周四","周五","周六");
    }

	return $weekarray[$shuchu]; 
}

function pushAdminMessage($message=NULL, $userInfo = NULL)
{
	$criteria = new CDbCriteria;
	$criteria->select = "push_token";
	$criteria->condition = "push_token is not NULL";
	$adminPushTokens = AdminPushToken::model()->findAll($criteria);

	foreach ($adminPushTokens as $admin) {
			$metaJob = new MetaJob;
			$metaJob->meta_action = 'send_weixin_alert';
			
			$metaJob->ctime = time();

			$metaJob->cron_time = date("y-m-d h:i:s", time() + 60);

			if(is_null($userInfo)) $userInfo = array();

			$params = array(
					"push_token" => $admin->push_token,
					"message" => $message,
					"client_name" => "QGLAdmin",
					"userInfo" => $userInfo,
					"badge" => 1
				);

			$metaJob->params = CJSON::encode($params);

			if (!$metaJob->save()) {
				echo $metaJob->getErrors();
			}

			$metaJob->save();
		}
}


function isMobile() {
    if(isset($_SERVER['HTTP_X_WAP_PROFILE'])) {
        return true;
    }
    if(isset ($_SERVER['HTTP_VIA'])) {
        //找不到为flase,否则为true
        return stristr($_SERVER['HTTP_VIA'], "wap") ? true : false;
    }
    if(isset($_SERVER['HTTP_USER_AGENT'])) {
        //此数组有待完善
        $clientkeywords = array (
        'nokia',
        'sony',
        'ericsson',
        'mot',
        'samsung',
        'htc',
        'sgh',
        'lg',
        'sharp',
        'sie-',
        'philips',
        'panasonic',
        'alcatel',
        'lenovo',
        'iphone',
        'ipod',
        'blackberry',
        'meizu',
        'android',
        'netfront',
        'symbian',
        'ucweb',
        'windowsce',
        'palm',
        'operamini',
        'operamobi',
        'openwave',
        'nexusone',
        'cldc',
        'midp',
        'wap',
        'mobile'
        );
        // 从HTTP_USER_AGENT中查找手机浏览器的关键字
        if(preg_match("/(" . implode('|', $clientkeywords) . ")/i", strtolower($_SERVER['HTTP_USER_AGENT']))) {
            return true;
        }
 
    }
 
    //协议法，因为有可能不准确，放到最后判断
    if (isset ($_SERVER['HTTP_ACCEPT'])) {
        // 如果只支持wml并且不支持html那一定是移动设备
        // 如果支持wml和html但是wml在html之前则是移动设备
        if ((strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') !== false) && (strpos($_SERVER['HTTP_ACCEPT'], 'text/html') === false || (strpos($_SERVER['HTTP_ACCEPT'], 'vnd.wap.wml') < strpos($_SERVER['HTTP_ACCEPT'], 'text/html')))) {
            return true;
        }
    }
     
	return false;
}