<div class="container">
	<form class="form-horizontal" role="form" action="/admin/user/changePassword" method="POST">
		<div class="form-group">
    		<label for="inputUserName" class="col-sm-2 control-label">用户名</label>
			<div class="col-sm-3">
				<input type="text" name="username" class="form-control" id="inputUserName" placeholder="<?=$admin['username'];?>"  disabled>
				<span class="help-block"></span>
			</div>
		</div>
		<div class="form-group">
    		<label for="inputNickName" class="col-sm-2 control-label">昵称</label>
			<div class="col-sm-3">
				<input type="text" name="nick_name" class="form-control" id="inputNickName" placeholder="" value=<?=$admin['nick_name'];?>>
				<span class="help-block"></span>
			</div>
		</div>
		<div class="form-group">
    		<label for="inputQQ" class="col-sm-2 control-label">QQ</label>
			<div class="col-sm-3">
				<input type="text" name="qq" class="form-control" id="inputQQ" placeholder="" value=<?=$admin['qq'];?>>
				<span class="help-block"></span>
			</div>
		</div>
		<div class="form-group">
    		<label for="inputWeixin" class="col-sm-2 control-label">微信</label>
			<div class="col-sm-3">
				<input type="text" name="weixin" class="form-control" id="inputWeixin" placeholder="" value=<?=$admin['weixin'];?>>
				<span class="help-block"></span>
			</div>
		</div>
		<div class="form-group">
    		<label for="inputMobile" class="col-sm-2 control-label">手机号</label>
			<div class="col-sm-3">
				<input type="text" name="mobile" class="form-control" id="inputMobile" placeholder="" value=<?=$admin['mobile'];?>>
				<span class="help-block"></span>
			</div>
		</div>
		<div class="form-group">
			<div class="col-sm-offset-2 col-sm-10">
				<button type="submit" id="submit-btn" class="btn btn-default">确认修改</button>
			</div>
		</div>
	</form>
</div>


<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/changeUserInfo');
	});
</script>
