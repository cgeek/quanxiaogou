<div class="container">

	<div class="col-lg-5 col-md-5 col-sm-5">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">最近出发</h3>
			</div>
			<!--div class="panel-body">
				以下是最近一个月内要出发的用户，记得最后发提醒一下他们注意事项哦！
			</div-->
			<!-- Table -->
			<table class="table table-striped">
<?php if(!empty($nearest_customer_list)):?>
				<thead>
					<tr>
						<th>姓名</th>
						<th>出发日期</th>
						<th>服务</th>
					</tr>
				</thead>
				<tbody>
	<?php foreach($nearest_customer_list as $customer):?>
					<tr>
						<td><a href="/admin/customer/detail?id=<?=$customer['id'];?>"><?=$customer['contact'];?></a> <?=isset($customer['admin']) ? ' [' .$customer['admin']['nick_name'] .']': '';?></td>
						<td><?=$customer['start_date'];?>（<?=human_time_admin_hp(strtotime($customer['start_date']));?>）</td>
						<td><?=$customer['service_fee'];?></td>
					</tr>
	<?php endforeach;?>
				</tbody>
<?php endif;?>
	    	</table>
		</div>
	</div>

	<div class="col-lg-7 col-md-7 col-sm-7">
	<?php if(!empty($nearest_checkitem_list)):?>
		<div class="panel panel-danger">
			<div class="panel-heading">
				<h3 class="panel-title">我的任务提醒</h3>
			</div>
			<!-- Table -->
			<table class="table table-striped">
				<thead>
					<tr>
						<th style="min-width:80px;">客户</th>
						<th>任务</th>
						<th style="min-width:100px;">提醒日期</th>
					</tr>
				</thead>
				<tbody>
	<?php foreach($nearest_checkitem_list as $checkitem):?>
					<tr>
						<td><a class="<?=$checkitem['class_expired']?>" href="/admin/customer/detail?id=<?=$checkitem['customer_id'];?>"><?=$checkitem['customer']['contact'];?></a></td>
						<td><a class="<?=$checkitem['class_expired']?>" href="/admin/customer/detail?id=<?=$checkitem['customer_id'];?>"><?=$checkitem['title'];?></a></td>
						<td class="<?=$checkitem['class_expired']?>">（<?=human_time_admin_hp(strtotime($checkitem['due_date']));?>）</td>
					</tr>
	<?php endforeach;?>
				</tbody>
	    	</table>
		</div>
<?php endif;?>
	</div>

	<div class="col-lg-3 col-md-3 col-sm-3">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">常用链接</h3>
			</div>
			<div class="panel-body">
				<ol>
					<li><a href="https://docs.google.com/spreadsheet/ccc?key=0AoHScF1EAHKNdFVpS1dtM0pzdGlJQmVzSWtvTVFETmc&usp=drive_web#gid=2" target="_blank">旅游产品价格</li>
					<li><a href="https://docs.google.com/spreadsheet/ccc?key=0AisPuzzNFcqjdDR1VmhaUnh3QlVCcWJNd3FjSjJFTWc" target="_blank">马来西亚行程单</a></li>
					<li><a href="https://docs.google.com/spreadsheet/ccc?key=0AisPuzzNFcqjdFlIcGFmdDkyY1B0bU8yRUkyM3BKT1E" target="_blank">泰国行程单</a></li>
					<li><a href="http://www.agoda.com.cn/?cid=1616993" target="_blank">返点Agoda</a></li>
					<li><a href="http://www.agoda.com/zh-cn/city/kota-kinabalu-my.html?cid=1616993" target="_blank">返点Agoda[亚庇酒店]</a></li>
					<li><a href="https://partners.agoda.com/zh-cn/login.html" target="_blank">Agoda合作伙伴后台</a></li>
					<li><a href="http://u.ctrip.com/union/CtripRedirect.aspx?TypeID=2&Allianceid=11795&sid=328252&OUID=&jumpUrl=http://www.ctrip.com" target="_blank">返点携程</a></li>
					<li><a href="http://www.airasia.com" target="_blank">亚航</a></li>

					<li><a href="/assets/airasia.crx">亚航Chrome插件0.1版本</a></li>
					<li><a href="http://luosimao.com" target="_blank">Luosimao短信平台</a></li>
					<li><a href="https://exmail.qq.com/login" target="_blank">企业邮箱</a></li>
					<li><a href="https://mp.weixin.qq.com/" target="_blank">微信公众平台</a></li>
					<li><a href="http://union.xyz.cn/" target="_blank">旅游保险(xiaogoulvxing)</a></li>
				</ol>
			</div>
		</div>
	</div>


	<div class="col-lg-4 col-md-4 col-sm-4">
		<div class="panel panel-info">
			<div class="panel-heading">
				<h3 class="panel-title">agoda返点代订</h3>
			</div>
			<div class="panel-body">
				<div id="SearchBox">&nbsp;</div>
				 <script src="http://ajaxsearch.partners.agoda.com/partners/SearchBox/Scripts/Agoda.SearchBoxV2.js" type="text/javascript"></script>
				 <script type="text/javascript">
						var AgodaSearch = new AgodaSearchBox({
							cid: 1616993,
							filterCityName: '亚庇',
							fixedCityName: false,
							fixedCityNameVisible:true,
							hotelID: '',
							checkInDateBefore:3,
							night:2,
							language:8,
							currencyCode: 'CNY',
							newWindow: true,
							header: '',
							footer: '',
							style: 'style4',
							Element: 'SearchBox'
					    });
					</script>
					<!-- Default,CmsLanding,Style1, Style2,Style3,style4, Style6-->
			</div>
		</div>
	</div>

</div>
<script>
	seajs.use('/assets/js/router.js', function(router){
	//	router.load('admin/login');
	});
</script>
