<div class="hotel-edit-page">
	<ul class="hotel-edit-nav container">
		<li class="col-lg-3 col-md-3 col-xs-3 active"><a href="javascript:void(0);">基本信息</a></li>
		<li class="col-lg-3 col-md-3 col-xs-3" ><a href="/admin/package/rooms?package_id=<?=$hotel['id'];?>">房型/套餐</a></li>
		<li class="col-lg-3 col-md-3 col-xs-3" ><a href="/admin/package/traffic?package_id=<?=$hotel['id'];?>">位置/交通</a></li>
		<li class="col-lg-3 col-md-3 col-xs-3" ><a href="/admin/package/overview?package_id=<?=$hotel['id'];?>">介绍/体验</a></li>
	</ul>
	<div class="hotel-detail container">
		<form class="form-horizontal" role="form" id="hotel_edit_form" method="post">
			<input type="hidden" name="hotel_id" value="<?=isset($hotel) ? $hotel['id']:'';?>">
				<div class="form-group">
					<label for="inputTitle" class="col-lg-2 control-label">标题</label>
					<div class="col-lg-5">
						<input type="text" name="title" class="form-control" id="inputTitle" placeholder="" value="<?=isset($hotel)? $hotel['title'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputSubtitle" class="col-lg-2 control-label">副标题</label>
					<div class="col-lg-8">
						<input type="text" name="subtitle" class="form-control" id="inputSubtitle" placeholder="" value="<?=isset($hotel)? $hotel['subtitle'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="coverImages" class="col-lg-2 control-label">酒店照片</label>
					<div class="col-lg-8 multimage" >
						<!-- Nav tabs -->
						<ul class="nav nav-tabs">
							<li class="active"><a href="#upload" data-toggle="tab">本地上传</a></li>
						</ul>
						<!-- Tab panes -->
						<div class="tab-content multimage-panels">
							<div class="tab-pane local-panel active" id="upload">
								<div class="upload-btn">
									选择图片：<a id="upload-pop-btn" href="javascript:void(0);" class="btn btn-default">本地上传图片</a>
									<div class="multimage-tips">
										<div class="tip-title">提示：</div>
										<ol>
											<li>建议上传高清大图</li>
											<li>文件大小限制，小于5M</li>
										</ol>
									</div>
								</div>
							</div>
						</div>
						<div class="multimage-gallery clearfix">
							<ul>
						<?php if(!empty($images)):?>
							<?php foreach($images as $image):?>
								<li id="image-<?=$image['id'];?>">
									<div class="info"></div>
									<div class="operate">
										<i class="toleft">左移</i>
										<i class="toright">右移</i>
										<i class="del">删除</i>
									</div>
									<div class="preview"><img src="<?=$image['image_url_small'];?>"></div>
								</li>
							<?php endforeach;?>
						<?php endif;?>
						<script type="text/template" id="multimage-gallery-item-tpl"> 
								<li>
									<div class="info"></div>
									<div class="operate">
										<i class="toleft">左移</i>
										<i class="toright">右移</i>
										<i class="del">删除</i>
									</div>
									<div class="preview"><img src="{{url}}"></div>
								</li>
						</script>
							</ul>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="inputSubtitle" class="col-lg-2 control-label">设置封面图片</label>
					<div class="col-lg-8">
						<input type="hidden" name="cover_image" value="<?=isset($hotel)? $hotel['cover_image'] : '';?>">
						<div class="cover-image-box">
						<?php if(isset($hotel) && !empty($hotel['cover_image_url'])) : ?>
							<img src="<?=$hotel['cover_image_url'];?>">
						<?php else:?>
							<img src="http://img03.taobaocdn.com/tps/i3/T1MNu0XdXnXXXXXXXX-210-210.png">
						<?php endif;?>
							<div class="loading"></div>
						</div>

						<!-- The fileinput-button span is used to style the file input field as button -->
						<span class="btn btn-success fileinput-button">
							 <i class="glyphicon glyphicon-plus"></i>
							 <span>选择图片</span>
							 <!-- The file input field used as target for the file upload widget -->
							<input id="fileupload" type="file" name="Filedata" multipart=false>
					    </span>
						或者 <a href="javascript:void(0);" class="select-cover-image-btn">从相册里选择</a>
					</div>
				</div>

				<div class="form-group">
					<label for="inputSellingPoints" class="col-lg-2 control-label">推荐理由（亮点）</label>
					<div class="col-lg-8">
						<textarea name="selling_points" placeholder="推荐理由，每条一行" rows=5 class="form-control" ><?=$hotel['selling_points'];?></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">保存</button>
						<a href="javascript:void(0);" id="delete-customer-btn" class="btn btn-danger">删除酒店</a>
						<a href="/admin/customer" class="btn btn-default">前台预览</a>
						<span class="help-block"></span>
					</div>
				</div>
		</form>
	</div>
</div>

<script type="text/template" id="multiuploadpic-tpl">
	<div class="multiuploadPic-pop">
		<div class="multiuploadpicwrap">
			<a href="javascript:void(0);" class="close">×</a>
			<div class="hd">上传图片</div>
			<div class="bd">
				<div class="multiupload-wrap">
					<div class="init-status">
						<div class="swfupload-btn">
							<div id="uploadBtn"></div>
							<a href="javascript:void(0);" class="uploadbtn" target="_self">添加的照片</a>
						</div>
					</div>
					<div class="upload-status emptypiclist">
						<dl>
							<dt>
								<span class="listtip"></span>
								<a href="#" class="clearlist">清空列表</a>
							</dt>
							<dd class="wait-upload">
								<ul class="piclist" id="fileQueue"></ul>
							</dd>
							<dd class="upload-tool">
								<span class="tntip"></span>
							</dd>
						</dl>
						<div class="complete-list">
							<div class="picedit">
								<ul class="piclist"></ul>
							</div>
							<div class="btmbtn">
								<a href="#" class="btn btn-primary" id="saveUploadBtn">保存图片</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</script>
<script type="text/template" id="cover-image-select-tpl">
<div class="coverpicwrap-pop">
	<div class="coverpicwrap">
		<a href="javascript:void(0);" class="close">×</a>
		<div class="hd">选择封面</div>
		<div class="bd">
			<ul>
			{{#image_list}}
				<li class="note-pic" image_hash={{image_hash}}>
					<div class="image">
						<img class="noteImage" src="{{image_url_small}}" style="width:120px;; height:auto">
					</div>
					<div class="selected "></div>
				</li>
			{{/image_list}}
			</ul>
			<div class="saveBtnWrap"><a id="set-cover-image-btn" class="btn btn-warning saveBtn" href="javascript:void(0);">设为封面</a></div>
		</div>
	</div>
</div>
</script>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/hotel_edit');
	});
</script>
