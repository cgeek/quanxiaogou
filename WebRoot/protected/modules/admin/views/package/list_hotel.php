<div class="hotel-list-page">
	<div class="hotel-list container">
<?php if(!empty($package_list)):?>
		<table class="table">
			<thead>
				<tr>
					<th>封面图片</th>
					<th>酒店名称</th>
					<th>价格</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach($package_list as $hotel):?>
				<tr>
					<td><img src="<?=upimage($hotel['cover_image']);?>"></td>
					<td><?=$hotel['title'];?></td>
					<td><?=$hotel['price'];?></td>
					<td><a href="/admin/package/edit?package_id=<?=$hotel['id'];?>" class="btn btn-default btn-xs">编辑</a></td>
				</tr>
	<?php endforeach;?>
			</tbody>
		</table>
<?php endif;?>
	</div>
</div>
