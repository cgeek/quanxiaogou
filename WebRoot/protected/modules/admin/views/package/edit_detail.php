<div class="activity-edit-page">
	<ul class="hotel-edit-nav container">
		<li class="col-lg-3 col-md-3 col-xs-3 "><a href="/admin/package/edit?package_id=<?=$activity['id'];?>">基本信息</a></li>
<?php if(!empty($activity)):?>
		<li class="col-lg-3 col-md-3 col-xs-3 active" ><a href="/admin/package/editDetail?package_id=<?=$activity['id'];?>">详细信息</a></li>
<?php else:?>
		<li class="col-lg-3 col-md-3 col-xs-3" ><a style="color:#ccc;" href="javascript:void(0);">详细信息</a></li>
<?php endif;?>
	</ul>
	<div class="activity-detail container">
		<form class="form-horizontal" role="form" id="activity_edit_form" method="post">
			<input type="hidden" name="activity_id" value="<?=isset($activity) ? $activity['id']:'';?>">
				<div class="form-group">
					<label for="inputSellingPoints" class="col-lg-2 control-label">详细信息</label>
					<div class="col-lg-8">
						<textarea name="detail"  style="display:none;"  rows=5 ><?=$activity['detail'];?></textarea>
						<textarea id="detailText" style="width:700px;"  rows=5 ><?=$activity['detail'];?></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">保存</button>
						<span class="help-block"></span>
					</div>
				</div>
		</form>
	</div>
</div>

<script type="text/template" id="multiuploadpic-tpl">
	<div class="multiuploadPic-pop">
		<div class="multiuploadpicwrap">
			<a href="javascript:void(0);" class="close">×</a>
			<div class="hd">上传图片</div>
			<div class="bd">
				<div class="multiupload-wrap">
					<div class="init-status">
						<div class="swfupload-btn">
							<div id="uploadBtn"></div>
							<a href="javascript:void(0);" class="uploadbtn" target="_self">添加的照片</a>
						</div>
					</div>
					<div class="upload-status emptypiclist">
						<dl>
							<dt>
								<span class="listtip"></span>
								<a href="#" class="clearlist">清空列表</a>
							</dt>
							<dd class="wait-upload">
								<ul class="piclist" id="fileQueue"></ul>
							</dd>
							<dd class="upload-tool">
								<span class="tntip"></span>
							</dd>
						</dl>
						<div class="complete-list">
							<div class="picedit">
								<ul class="piclist"></ul>
							</div>
							<div class="btmbtn">
								<a href="#" class="btn btn-primary" id="saveUploadBtn">保存图片</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</script>
<script type="text/template" id="cover-image-select-tpl">
<div class="coverpicwrap-pop">
	<div class="coverpicwrap">
		<a href="javascript:void(0);" class="close">×</a>
		<div class="hd">选择封面</div>
		<div class="bd">
			<ul>
			{{#image_list}}
				<li class="note-pic" image_hash={{image_hash}}>
					<div class="image">
						<img class="noteImage" src="{{image_url_small}}" style="width:120px;; height:auto">
					</div>
					<div class="selected "></div>
				</li>
			{{/image_list}}
			</ul>
			<div class="saveBtnWrap"><a id="set-cover-image-btn" class="btn btn-warning saveBtn" href="javascript:void(0);">设为封面</a></div>
		</div>
	</div>
</div>
</script>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/detail_edit');
	});
</script>
