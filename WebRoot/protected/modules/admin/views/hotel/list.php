<div class="hotel-list-page">
	<div class="hotel-list container">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/hotel">酒店</a></li>
	</ol>
	<form class="form-inline" role="form">
	<div class="pull-left">
		城市：
		<div class="form-group">
		<select name="city" class="form-control">
    		  <option value="0">--所有--</option>
	<?php foreach($city_list as $city):?>
  		  <option value="<?=$city['id'];?>"<?=$city['selected']?>><?=$city['name'];?></option>
  	<?php endforeach;?>
	  	</select>
		</div>
		&nbsp;关键词:
		<div class="form-group">
			<input type="text" name="filter_key" class="form-control" id="filter_key" placeholder="名称或描述" value="<?=$filter_key?>"/>
		</div>
		&nbsp;<button type="submit" class="btn btn-primary">过滤</button>
	</div>
	</form>
<?php if(!empty($hotel_list)):?>
		<table class="table">
			<thead>
				<tr>
					<th>封面图片</th>
					<th nowrap>所在城市</th>
					<th>标题</th>
					<th style="width:50%;">描述</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach($hotel_list as $hotel):?>
				<tr>
					<td><img src="<?=$hotel['cover_image_url'];?>" width="160"></td>
					<td><?=$hotel['city_name'];?></td>
					<td><?=$hotel['title'];?></td>
					<td><?=$hotel['desc'];?></td>
					<td><a href="/admin/hotel/edit?hotel_id=<?=$hotel['id'];?>" class="btn btn-default btn-xs">编辑</a></td>
				</tr>
	<?php endforeach;?>
			</tbody>
		</table>
		<?php
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
<?php endif;?>
	</div>
</div>
