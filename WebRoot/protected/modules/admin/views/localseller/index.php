<div class="trip_list clearfix">
    <ol class="breadcrumb">
        <li><a href="/admin">管理首页</a></li>
        <li><a href="/admin/localseller">商户列表</a></li>
    </ol>
    <div class="filter-bar clearfix">
        <form class="form-inline" role="form" action="/admin/localseller">
            <div class="clearfix">
                <div class="pull-left">
                    &nbsp;&nbsp;搜索:
                    <div class="form-group">
                        <input type="text" name="keyword" class="form-control" id="filter_key" placeholder="关键词" value="<?=isset($keyword) ? $keyword : '';?>"/>
                    </div>

                    <div class="form-group">
                        <select name="country" class="form-control">
                            <option value="">全部国家</option>
                            <option value="泰国" <?=isset($_GET['country']) && $_GET['country'] == '泰国' ? 'selected' :'';?>>泰国</option>
                            <option value="台湾" <?=isset($_GET['country']) && $_GET['country'] == '台湾' ? 'selected' :'';?>>台湾</option>
                            <option value="韩国" <?=isset($_GET['country']) && $_GET['country'] == '韩国' ? 'selected' :'';?>>韩国</option>
                            <option value="日本" <?=isset($_GET['country']) && $_GET['country'] == '日本' ? 'selected' :'';?>>日本</option>
                            <option value="中国" <?=isset($_GET['country']) && $_GET['country'] == '中国' ? 'selected' :'';?>>中国</option>
                            <option value="马来西亚" <?=isset($_GET['country']) && $_GET['country'] == '马来西亚' ? 'selected' :'';?>>马来西亚</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select  name="typeEnum" class="form-control" id="typeEnum">
                            <option value="all">全部合作方式</option>
                            <option value="DIRECT" <?=isset($_GET['typeEnum']) && $_GET['typeEnum'] == 'DIRECT' ? 'selected' :'';?>>直接供应商</option>
                            <option value="ITEM"  <?=isset($_GET['typeEnum']) && $_GET['typeEnum'] == 'ITEM' ? 'selected' :'';?>>旅游物品</option>
                            <option value="LOCAL_AGENT"  <?=isset($_GET['typeEnum']) && $_GET['typeEnum'] == 'LOCAL_AGENT' ? 'selected' :'';?>>当地代理</option>
                            <option value="LOCAL_NON_AGENT" <?=isset($_GET['typeEnum']) && $_GET['typeEnum'] == 'LOCAL_NON_AGENT' ? 'selected' :'';?>>非当地代理</option>
                            <option value="THIRD_PARTY" <?=isset($_GET['typeEnum']) && $_GET['typeEnum'] == 'THIRD_PARTY' ? 'selected' :'';?>>第三方供应商</option>
                            <option value="AGENT_SELLS_TERRACE" <?=isset($_GET['typeEnum']) && $_GET['typeEnum'] == 'AGENT_SELLS_TERRACE' ? 'selected' :'';?>>代理分销平台</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <select name="statusEnum" class="form-control" id="statusEnum">
                            <option value="all" selected="selected">全部状态</option>
                            <option value="0" <?=isset($_GET['statusEnum']) && $_GET['statusEnum'] == '0' ? 'selected' :'';?>>商谈中</option>
                            <option value="1" <?=isset($_GET['statusEnum']) && $_GET['statusEnum'] == '1' ? 'selected' :'';?>>已合作</option>
                            <option value="2" <?=isset($_GET['statusEnum']) && $_GET['statusEnum'] == '2' ? 'selected' :'';?>>已合作，未上商品</option>
                            <option value="3" <?=isset($_GET['statusEnum']) && $_GET['statusEnum'] == '3' ? 'selected' :'';?>>未合作</option>
                            <option value="4" <?=isset($_GET['statusEnum']) && $_GET['statusEnum'] == '4' ? 'selected' :'';?>>申请入驻</option>
                            <option value="5" <?=isset($_GET['statusEnum']) && $_GET['statusEnum'] == '5' ? 'selected' :'';?>>拒绝</option>
                        </select>
                    </div>

                    <button type="submit" class="btn btn-sm btn-primary">查找</button>
                    &nbsp;
                    <?php if(isset($count)):?>符合条件<?=$count;?>条<?php endif;?>
                </div>


            </div>
        </form>
    </div>

    <div class="table-responsive" >
        <table class="table table-bordered trip-list-table">
            <thead>
            <tr style="background:#eee;">
                <th>#</th>
                <th style="width:100px;">商户名称</th>
                <th style="width:60px;">国家</th>
                <th style="width:70px;">区域</th>
                <th style="width:70px;">类别</th>

                <th>介绍</th>
                <th style="width:120px;">联系方式</th>
                <th style="width:100px;">创建时间</th>
                <th style="width:100px;">修改时间</th>
                <th style="width:60px;">状态</th>
            </tr>
            </thead>
            <tbody>
            <?php if(!empty($localseller_list)): ?>
                <?php foreach($localseller_list as $localseller):?>
                    <tr trip_id="<?=$localseller['id'];?>">
                        <td><?=$localseller['id'];?></td>
                        <td><?=$localseller['name'];?></td>
                        <td><?=$localseller['country'];?></td>
                        <td><?=$localseller['area'];?></td>
                        <td><?=$localseller['category'];?></td>
                        <td><?=$localseller['introduce'];?></td>

                        <td>
                            邮箱：<?=$localseller['contacts'];?><br />
                            微信： <?=$localseller['localseller_weixin'];?><br />
                            更多：<?=$localseller['ways_of_cooperation'];?>
                        </td>
                        <td><?=date('Y-m-d', strtotime($localseller['date_created']));?></td>
                        <td><?=date('Y-m-d', strtotime($localseller['last_updated']));?></td>

                        <td>
                            <?=$localseller['status_enum'];?>
                            <?=$localseller['agreement_status_enum'];?>
                            <?=$localseller['type_enum'];?>
                        </td>
                    </tr>
                <?php endforeach;?>
            <?php else:?>
                <tr>
                    <td colspan=11>暂没有相关行程列表</td>
                </tr>
            <?php endif;?>
            </tbody>
        </table>


        <?php
            $this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false));
        ?>
    </div>

</div>
