
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/order/recordCustomers">最近交易过的客户</a></li>
	</ol>
<form class="form-inline" role="form" method="GET">
	<div class="form-group">
		<label class="sr-only" for="exampleInputEmail2">开始时间</label>
		<input name="start_date" type="text" class="form-control" id="exampleInputEmail2" placeholder="2014-02-01" value="<?=$start_date;?>">
	</div>
	<div class="form-group">
		<label class="sr-only" for="exampleInputPassword2">结束时间</label>
		<input name="end_date" type="text" class="form-control" id="exampleInputPassword2" placeholder="2014-02-30" value="<?=$end_date;?>">
	</div>
	<button type="submit" class="btn btn-default">搜索</button>
</form>

	<table class="table">
		<thead>
			<tr>
				<td>ID</td>
				<td>客户名字</td>
				<td>创建时间</td>
				<td>出发时间</td>
				<td>旅游顾问</td>
				<td>目的地</td>
				<td>代订结余</td>
				<td>支付宝结余</td>
			</tr>
		</thead>
		<tbody>
<?php foreach($customer_list as $customer):?>
			<tr <?php if($customer['alipay_balance'] - $customer['order_balance'] < -10):?>style="background:lightyellow;"<?php elseif($customer['alipay_balance'] - $customer['order_balance'] > 10):?> style="background:#C5FBBD;"<?php endif;?> >
				<td>
					<?=$customer['id'];?>
				</td>
				<td>
					<a target="_target" href="/admin/customer/detail?id=<?=$customer['id'];?>" ><?=$customer['contact'];?></a>
				</td>
				<td>
					<?=$customer['ctime'];?>
				</td>
				<td>
					<?=$customer['start_date'];?>
				</td>
				<td>
					<?=$customer['admin']['nick_name'];?>
				</td>
				<td>
					<?=$customer['destination'];?>
				</td>
				<td>
					<?=$customer['order_balance'];?>
				</td>
				<td>
					<?=$customer['alipay_balance'];?>
				</td>
			</tr>
<?php endforeach;?>
			<tr>
				<td></td>
				<td></td>
				<td></td>
				<td></td>
				<td><?=count($customer_list);?>个客人</td>
				<td><?=$order_balance_total;?></td>
				<td><?=$alipay_balance_total;?></td>
			</tr>
	</table>
