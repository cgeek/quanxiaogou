<div class="">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/order">代订列表</a></li>
		<?php if(isset($customer)):?>
		<li><a href="/admin/customer/detail?id=<?=$customer['id'];?>"><?=$customer['contact'];?>的资料</a></li>
		<?php endif;?>
		<li class="active">编辑代订信息</li>
	</ol>
	<div class="order-edit">
		<div class="order-info-form col-lg-12">
			<form class="form-horizontal" role="form" method="post" id="order_edit_form">
				<input type="hidden" name="customer_id" value="<?=isset($customer) ? $customer['id']:'';?>">
				<input type="hidden" name="order_id" value="<?=isset($order) ? $order['id']:'';?>">
				<div class="form-group">
					<label for="inputName" class="col-lg-2 control-label">联系人姓名</label>
					<div class="col-lg-3">
						<input type="text" name="contact" class="form-control" id="inputName" placeholder="联系人姓名" value="<?=isset($customer)? $customer['contact']:'';?>" disabled=true>
					</div>
				</div>
				<div class="form-group">
					<label for="inputType" class="col-lg-2 control-label required">代订类型</label>
					<div class="col-lg-3">
						<select class="form-control" name="type">
						  <option <?php if(isset($order) && $order['type'] == 'hotel'):?>selected<?php endif;?> value="hotel">酒店</option>
						  <option <?php if(isset($order) && $order['type'] == 'flight'):?>selected<?php endif;?> value="flight">机票</option>
						  <option <?php if(isset($order) && $order['type'] == 'chartered'):?>selected<?php endif;?> value="chartered">包车</option>
						  <option <?php if(isset($order) && $order['type'] == 'activity'):?>selected<?php endif;?> value="activity">活动</option>
						  <option <?php if(isset($order) && $order['type'] == 'pickup'):?>selected<?php endif;?> value="pickup">接机</option>
						  <option <?php if(isset($order) && $order['type'] == 'insure'):?>selected<?php endif;?> value="insure">保险</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputTitle" class="col-lg-2 control-label required">代订项目名称</label>
					<div class="col-lg-5">
						<input type="text" name="title" class="form-control" id="inputTitle" placeholder="建议格式：【代订来源】+  项目标题" value="<?=isset($order)? $order['title']:'';?>">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputStartDate" class="col-lg-2 control-label required">项目时间</label>
					<div class="col-lg-2">
						<div class="input-append date date-picker"  data-date="2014-01-01" data-date-format="yyyy-mm-dd">
							<input id="inputStartDate" placeholder="开始日期(必填)" name="start_date" class="form-control" style="display:inline-block" size="16" type="text" value="<?=isset($order) ? $order['start_date'] : '';?>">
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="input-append date date-picker"  data-date="2014-01-01" data-date-format="yyyy-mm-dd">
							<input placeholder="结束日期" name="end_date" class="form-control" style="display:inline-block" size="16" type="text" value="<?=isset($order) ? $order['end_date'] : '';?>">
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
				</div>

				<div class="form-group">
					<label for="inputAgent" class="col-lg-2 control-label required">代订代理</label>
					<div class="col-lg-3">
						<select class="form-control" id="inputAgent" name="agent">
							<option <?php if(isset($order) && $order['agent'] == '亚航'):?>selected<?php endif;?> value="亚航">亚航</option>
							<option <?php if(isset($order) && $order['agent'] == '恩典'):?>selected<?php endif;?> value="恩典">恩典</option>
							<option <?php if(isset($order) && $order['agent'] == 'agoda'):?>selected<?php endif;?> value="agoda">Agoda</option>
							<option <?php if(isset($order) && $order['agent'] == '携程'):?>selected<?php endif;?> value="携程">携程</option>
							<option <?php if(isset($order) && $order['agent'] == '淘宝卖家'):?>selected<?php endif;?> value="淘宝卖家">淘宝卖家</option>
							<option <?php if(isset($order) && $order['agent'] == '其他'):?>selected<?php endif;?> value="其他">其他</option>
						</select>
					</div>
					<div class="col-lg-2">
						<a href="javascript:void(0);" class="btn btn-default show-source-url-btn" data-toggle="tooltip"  data-placement="top" title="" data-original-title="如果是淘宝卖家等非合作伙伴，建议填写代订url，方便以后统计">填写代订URL</a>
					</div>
				</div>

				<div class="form-group source_url_input">
					<label for="inputSourceURL" class="col-lg-2 control-label">代订URL</label>
					<div class="col-lg-5">
						<input type="text" name="source_url" class="form-control" id="inputSourceURL" placeholder="通过淘宝等代订请填写URL" value="<?=isset($order)? $order['source_url']:'';?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPaymentType" class="col-lg-2 control-label required">预定方式</label>
					<div class="col-lg-5">
						<label class="radio-inline">
							<input <?php if(empty($order) || $order['payment_type'] == 'all'):?>checked<?php endif;?>  type="radio" name="payment_type" id="inputPaymentType" value="all"> 付全款
						</label>
						<label class="radio-inline">
							<input <?php if(isset($order) && $order['payment_type'] == 'down'):?>checked<?php endif;?> type="radio" name="payment_type" id="inputPaymentType" value="down"> 付定金
						</label>
						<label class="radio-inline">
							<input <?php if(empty($order) || $order['payment_type'] == 'margin'):?>checked<?php endif;?>  type="radio" name="payment_type" id="inputPaymentType" value="margin"> 到付（赚差额）
						</label>
						<span class="help-inline"></span>
					</div>
				</div>
			<div style="background:#ddd; padding:10px 0; margin-bottom:10px;">
				<div class="form-group down_payment_input" <?php if( empty($order) || $order['payment_type'] != 'down'):?>style="display:none;"<?php endif;?>>
					<label for="inputDownPayment" class="col-lg-2 control-label required">定金金额</label>
					<div class="col-lg-3">
						<input type="text" name="down_payment" class="form-control" id="inputDownPayment" placeholder="定金金额" value="<?=isset($order)? $order['down_payment']:'';?>">
					</div>
				</div>
				<div class="form-group down_payment_input" <?php if( empty($order) || $order['payment_type'] != 'down'):?>style="display:none;"<?php endif;?>>
					<label for="inputPaymentDeadline" class="col-lg-2 control-label required">余款期限</label>
					<div class="col-lg-3">
						<div class="input-append date date-picker"  data-date="2014-01-01" data-date-format="yyyy-mm-dd">
							<input id="inputPaymentDeadline" placeholder="余款期限" name="payment_deadline" class="form-control" style="display:inline-block" size="16" type="text" value="<?=isset($order) ? $order['payment_deadline'] : '';?>">
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="inputTotalFee" class="col-lg-2 control-label required">卖价(客户应付)</label>
					<div class="col-lg-2">
						<input type="text" name="total_fee" class="form-control" id="inputTotalFee" placeholder="" value="<?=isset($order)? $order['total_fee']:'';?>">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputRemark" class="col-lg-2 control-label">收入交易号</label>
					<div class="col-lg-4">
						<textarea name="payment_trade_id" class="form-control" rows="3" placeholder="填写支付宝交易号  2013100811001001730054729214                  一行一个"><?=isset($order)? $order['payment_trade_id'] :'';?></textarea>
						<span class="help-inline"></span>
					</div>
				</div>
			</div>

			<div style="background:#eee; padding:10px 0; margin-bottom:10px;">
				<div class="form-group">
					<label for="inputPay" class="col-lg-2 control-label required">进价（代理价)</label>
					<div class="col-lg-2">
						<input type="text" name="payment" class="form-control" id="inputPay" placeholder="" value="<?=isset($order) ? $order['payment']:'';?>">
						<span class="help-inline"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputRemark" class="col-lg-2 control-label">支出交易号</label>
					<div class="col-lg-4">
						<textarea name="payout_trade_id" class="form-control" rows="3" placeholder="如：2013100811001001730054729214          一行一个"><?=isset($order)? $order['payout_trade_id']:'';?></textarea>
						<span class="help-inline"></span>
					</div>
				</div>
			</div>
				<div class="form-group">
					<label for="inputRemark" class="col-lg-2 control-label required">代订备注</label>
					<div class="col-lg-6">
						<textarea name="remark" class="form-control" rows="9" placeholder="填写关于该代订相关的信息(确认单上的重要信息)，时间，人数，金额，遇到的情况，换行记录,不要嫌多，方便以后追溯"><?=isset($order)? $order['remark']:'';?></textarea>
						<span class="help-inline"></span>
					</div>
				</div>

				<div class="form-group">
					<label for="inputStatus" class="col-lg-2 control-label">代订状态</label>
					<div class="col-lg-6">
						<label class="radio-inline">
							<input type="radio" <?php if(empty($order) || $order['status'] == '' || ($order['status'] == '0')):?>checked<?php endif;?>   name="status" id="inputStatus" value="0">等待客户付款
						</label>
						<label class="radio-inline">
							<input type="radio" <?php if(isset($order) && $order['status'] == '1'):?>checked<?php endif;?>   name="status" id="inputStatus" value="1">客户已付款
						</label>
						<label class="radio-inline">
							<input type="radio" <?php if(isset($order) && $order['status'] == '2'):?>checked<?php endif;?>   name="status" id="inputStatus" value="2">等待支付尾款
						</label>
						<!--label class="radio-inline">
							<input type="radio" <?php if(isset($order) && $order['status'] == '5'):?>checked<?php endif;?>   name="status" id="inputStatus" value="5">已发确认单(已完成)
						</label-->
					</div>
				</div>

				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">保存信息</button>
						<a class="btn btn-primary" href="https://www.dropbox.com/home/QGL/%E4%BB%A3%E8%AE%A2%E6%9C%8D%E5%8A%A1/%E4%BB%A3%E8%AE%A2%E9%A1%B9%E7%9B%AE%E7%A1%AE%E8%AE%A4%E5%8D%95" target="_blank">上传确认单(需翻墙)</a>
						<a class="btn btn-default" href="/admin/order/add?customer_id=<?=$customer['id'];?>">添加新代订</a>
						<a href="/admin/customer/detail?id=<?=$customer['id'];?>" class="btn btn-default">返回用户信息</a>
					</div>
				</div>
			</form>
		</div>	
	</div>
</div>

<script type="text/template" id="taobao_tredes_tpl">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">淘宝交易列表</h4>
	</div>
	<div class="modal-body">
		<table class="table">
			<thead>
				<tr>
					<th>支付时间</th>
					<th>支付金额</th>
					<th>旺旺</th>
					<th>状态</th>
					<th>关联</th>
				</tr>
			</thead>
			<tbody>
			{{#trades}}
				<tr class="taobao_trade_item" buyer_nick={{buyer_nick}} total_fee={{total_fee}} trade_id={{tid}}>
					<td>{{pay_time}}</td>
					<td>{{total_fee}}</td>
					<td>{{buyer_nick}}</td>
					<td>{{status}}</td>
					<td><a href="javascript:void(0);" class="btn btn-xs btn-primary">选择</a></td>
				</tr>
			{{/trades}}
			</tbody>
		</table>
		<p class="alert alert-warning">如果交易列表为空或者不正确，请点击<a href="/admin/oauth/taobao" target="_blank">淘宝API授权</a>(每24小时会失效,建议安装数字证书)</p>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
	</div>
</script>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/order_edit');
	});
</script>
