<html>
<body>
	<table>
		<tbody>
			<tr>
				<td>预定人</td>
				<td>QGL</td>
			</tr>
			<tr>
				<td>联系电话</td>
				<td>13738173952</td>
			</tr>
			<tr>
				<td>联系邮箱</td>
				<td>xiaogoulvxing@gmail.com</td>
			</tr>
			<tr>
				<td>QQ号</td>
				<td>549569630</td>
			</tr>
		</tbody>
	</table>

	<h1>酒店预定单</h1>
	<table>
		<tbody>
			<tr>
				<td>酒店名称<br>HOTEL</td>
				<td colspan=3><?=$order['title'];?></td>
			</tr>
			<tr>
				<td>入住日期<br>CHECK IN</td>
				<td><?=$order['start_date'];?></td>
				<td>抵达航班时间</td>
				<td></td>
			</tr>
			<tr>
				<td>离开日期<br>CHECK OUT</td>
				<td><?=$order['end_date'];?></td>
				<td>离开航班时间</td>
				<td></td>
			</tr>
			<tr>
				<td>房间数<br>NO OF ROOM</td>
				<td></td>
				<td>房型ROOM TYPE</td>
				<td></td>
			</tr>
			<tr>
				<td>备注</td>
				<td colspan=3></td>
			</tr>
		</tbody>
	</table>


	<h1>客人资料</h1>
	<table>
		<thead>
			<tr>
				<th>编号</th>
				<th>中文姓名</th>
				<th>拼音 Name</th>
				<th>性别 Sex</th>
				<th>出生年月 DOB</th>
				<th>护照号码</th>
				<th>国家</th>
			</tr>
		</thead>
		<tbody>
<?php foreach($traveler_list as $key=>$traveler):?>
			<tr>	
				<td><?=$key+1;?></td>
				<td><?=$traveler['name'];?></td>
				<td><?=$traveler['last_name'];?> <?=$traveler['first_name'];?></td>
				<td><?=$traveler['sex'];?></td>
				<td><?=$traveler['birthday'];?></td>
				<td><?=$traveler['passport_no'];?></td>
				<td>中国</td>
			</tr>
<?php endforeach;?>
		</tbody>
	</table>
</body>
</html>

