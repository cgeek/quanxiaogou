<div class="order_list clearfix">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/order">代订列表</a></li>
		<li class="active"><?=isset($customer) ? $customer['contact'] . '的':'';?>代订信息</li>
	</ol>
	<div class="filter-bar">
		<form class="form-inline" role="form">
			<div class="form-group">
				<label >筛选条件：</label>
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputCustomerName">联系人姓名</label>
				<input type="text" name="customer_name" class="form-control" id="inputCustomerName" placeholder="联系人姓名" value="<?=isset($customer)? $customer['contact'] : '';?>">
			</div>
			<button type="submit" class="btn btn-primary">查找</button>
<?php if(isset($customer)):?>
				<a style="margin-bottom:10px;" href="/admin/order/add?customer_id=<?=$customer['id'];?>" class="btn btn-success pull-right">添加代订需求</a>
<?php endif;?>
		</form>
	</div>
	<div class="table-responsive" >
		<table class="table table-bordered order-list-table">
			<thead>
				<tr style="background:#eee;">
					<th>#</th>
					<th>客户</th>
					<th>类型</th>
					<th>代订项目名称</th>
					<th>代理</th>
					<th>卖价</th>
					<th>进价</th>
					<th>利润</th>
					<th>修改时间</th>
					<th>状态</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
<?php if(!empty($order_list)): ?>
	<?php foreach($order_list as $order):?>
				<tr>
					<td><?=$order['id'];?></td>
					<td style="min-width:80px;">
						<a href="/admin/order?customer_id=<?=$order['customer_id'];?>"><?=$order['customer']['contact'];?></a>
<?php if(!empty($order['customer']) && !empty($order['customer']['admin'])):?>
<p><?=$order['customer']['admin']['nick_name'];?></p>
<?php endif;?>
					</td>
					<td><?=orderTypeCN($order['type']);?></td>
					<td style="max-width:200px;"><?=$order['title'];?></td>
					<td><?=$order['agent'];?></td>
					<td><?php if(!empty($order['total_fee'])):?>￥<?=$order['total_fee'];?><?php endif;?></td>
					<td><?php if(empty($order['payment'])) :?>0<?php else:?>￥<?=$order['payment'];?><?php endif;?></td>
					<td>￥<?=round(($order['total_fee'] - $order['payment']), 2);?></td>

					<td><?=date('m-d H:i',strtotime($order['mtime']));?></td>
					<td><?=$order['status'];?>
						<?php if($order['status'] == '等待支付尾款'):?>
						<p><?=$order['payment_deadline'];?></p>
						<?php endif;?>
					</td>
					<td>
						<a class="btn btn-default btn-xs" href="/admin/order/edit/?id=<?=$order['id'];?>">修改</a>
						<a class="btn btn-default btn-xs" href="/admin/customer/detail?id=<?=$order['customer_id'];?>">客户资料</a>
					</td>
				</tr>
	<?php endforeach;?>
<?php else:?>
				<tr>
					<td colspan=11>暂没有相关代订列表</td>
				</tr>
<?php endif;?>
			</tbody>
		</table>
		<?php 
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
	</div>

</div>
	<?php if(isset($sum)):?>
		<div class=" alert alert-success">符合条件的代订笔数：<?=$sum['total_num'];?>  ,  代订总收入：￥<?=intval($sum['total_fee']);?>,  代订总支出：￥<?=intval($sum['total_payment']);?>, 总差额：￥<?=round(($sum['total_fee'] - $sum['total_payment']), 2);?></div>
	<?php endif;?>
<script>
	seajs.use('/assets/js/router.js', function(router){
	//	router.load('admin/order_index');
	});
</script>
