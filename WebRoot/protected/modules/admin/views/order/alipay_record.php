<div class="record_list clearfix">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/order/alipayRecord">支付宝交易列表</a></li>
	</ol>
	<div class="filter-bar">
		<form class="form-inline" role="form" action="/admin/order/alipayRecord">
			<div class="form-group">
				<label >筛选条件：</label>
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputCustomerName">customer_id</label>
				<input type="text" name="customer_id" class="form-control" id="inputCustomerName" placeholder="customer_id" value="<?=isset($customer)? $customer['contact'] : '';?>">
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputCustomerName">支付宝交易号</label>
				<input type="text" name="alipay_order_no" class="form-control"  placeholder="支付宝交易号" value="">
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputCustomerName">真实姓名</label>
				<input type="text" name="owner_user_id" class="form-control" placeholder="姓名" value="">
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputCustomerName">订单状态</label>
				<input type="text" name="order_status" class="form-control"  placeholder="订单状态" value="">
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputCustomerName">金额</label>
				<input type="text" name="total_amount" class="form-control"  placeholder="金额" value="">
			</div>
			<button type="submit" class="btn btn-primary">查找</button>
			<span class="btn btn-success fileinput-button pull-right">
				<i class="glyphicon glyphicon-plus"></i>
				<span>导入支付宝交易</span>
				<!-- The file input field used as target for the file upload widget -->
				<input id="fileupload" type="file" name="Filedata" multipart=false>
			</span>
		</form>
	</div>
	<div class="table-responsive" >
		<table class="table table-brecorded record-list-table table-striped">
			<thead>
				<tr style="background:#eee;">
					<th>支付宝交易号</th>
					<th>商品名称</th>
					<th>用户名</th>
					<th>支付时间</th>
					<th>客户id</th>
					<th>流向</th>
					<th>交易金额</th>
					<th>订单状态</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
<?php if(!empty($record_list)): ?>
	<?php foreach($record_list as $record):?>
				<tr record_id="<?=$record['id'];?>">
					<td><a href="https://lab.alipay.com/consume/queryTradeDetail.htm?tradeNo=<?=$record['alipay_order_no'];?>" target="_blank"><?=$record['alipay_order_no'];?></a></td>
					<td><?=$record['order_title'];?></td>
					<td style="width:100px;"><?=$record['owner_user_id'];?></td>
					<td><?=$record['payment_time'];?></td>
					<td style="width:80px;">
						<input type="text" class="record_customer_id_input" name="customer_id" value="<?=$record['customer_id'];?>" style="width:70px;text-align:center;">
					</td>
					<td style="width:50px;"><?=$record['in_out_type'];?></td>
					<td style="width:80px;">
						<?php if($record['in_out_type'] == '收入'):?>
							<span style="color:#53a000; font-weight:bold;">+<?=$record['total_amount'];?></span>
						<?php else:?>
							<span style="color:#f37800; font-weight:bold;">-<?=$record['total_amount'];?></span>
						<?php endif;?>
					</td>
					<td style="width:100px;"><?=$record['order_status'];?></td>
					<td ><a href="javascript:;" class="btn btn-xs btn-default delete_record_btn">删除</a></td>
				</tr>
	<?php endforeach;?>
<?php else:?>
				<tr>
					<td colspan=11>暂没有相关代订列表</td>
				</tr>
<?php endif;?>
			</tbody>
		</table>
		<?php 
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
	</div>

</div>
<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/record_list');
	});
</script>
