<div class="container">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/demoTripDay">经典行程列表</a></li>
		<li class="active">编辑</li>
	</ol>
	<div class="demoTripDay-detail container">
		<form role="form" id="demoTripDay_form" method="post">
			<input type="hidden" name="id" value="<?=isset($demoTripDay)? $demoTripDay['id'] :'';?>">
			<div class="form-group">
				<label for="inputTitle">标题：</label>
			    <input name="title" type="title" class="form-control" id="inputTitle" placeholder="标题" value="<?=isset($demoTripDay)? $demoTripDay['title'] : '';?>">
			</div>
			<div class="form-group">
				<label for="inputDesc">行程描述：</label>
				<textarea name="desc" class="form-control" rows=8><?=isset($demoTripDay)? $demoTripDay['desc'] : '';?></textarea>
			</div>
			<div class="form-group">
				<label for="inputTrafficNote">交通描述：</label>
				<textarea name="traffic_note" class="form-control" rows=4><?=isset($demoTripDay)? $demoTripDay['traffic_note'] : '';?></textarea>
			</div>
			<div class="form-group">
				<button id="submit-btn" class="btn btn-primary" type="submit">保存</button>
			</div>
		</form>
	</div>
</div>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/demoTripDay_edit');
	});
</script>
