<div class="container">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/demoTripDay">经典行程列表</a></li>
	</ol>
	<form class="form-inline" method="get" action="/admin/demoTripDay/">
		<div class="form-group">
			<label class="sr-only" for="keyword">关键词搜索</label>
			<select class="form-control" name="city_id">
				<option value="">所有城市</option>
				<?php if(!empty($city_list)):?>
					<?php foreach($city_list as $city):?>
						<option value="<?=$city['id'];?>" <?=$city['selected'] ? 'selected' : '';?>><?=$city['name'];?></option>
					<?php endforeach;?>
				<?php endif;?>
			</select>
		</div>
		<div class="form-group">
			<label class="sr-only" for="keyword">关键词搜索</label>
			<input type="text" class="form-control" name="keyword" placeholder="关键词" value="<?=isset($keyword)? $keyword : '';?>">
		</div>
		<button type="submit" class="btn btn-primary">筛选</button>
	</form>
<?php if(!empty($demo_list)):?>
	<ul class="demo_trip_day_list clearfix ">
	<?php foreach($demo_list as $demo):?>
		<li class="col-md-6" demo_id="<?=$demo['id'];?>">
			<div class="" style="border:1px solid #ccc; padding:10px;">
			<div class="title"><a href="/admin/demoTripDay/edit?id=<?=$demo['id'];?>"><?=$demo['title'];?></a></div>
			<div class="desc"><?=nl2br($demo['desc']);?></div>
			<div class="traffic_note"><?=nl2br($demo['traffic_note']);?></div>
			<div class="action clearfix">
				<div class="pull-right">
					<a href="/admin/demoTripDay/edit?id=<?=$demo['id'];?>" class="btn btn-xs btn-primary">修改</a>
					<a href="javascript:void(0);" class="btn btn-xs btn-default delete_demo_btn">删除</a>
				</div>
			</div>
			</div>
		</li>
	<?php endforeach;?>
	</ul>
<?php endif;?>

		<?php 
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
</div>


<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/demoTripDay_list.js');
	});
</script>
