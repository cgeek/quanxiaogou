<div class="customer_list clearfix">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li class="active">客户列表</li>
<?php if(isset($status)):?>
		<li class="active"><?=customerStatusCn($status);?>的客户</li>
<?php endif;?>
	</ol>
	<div class="filter-bar clearfix">
		<form class="form-inline" role="form">
			<!--div class="form-group">
				<label >筛选条件：</label>
			</div>
			<div class="btn-group">
  				<button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown">联系人姓名<span class="caret"></span></button>
		  		<ul class="dropdown-menu" role="menu">
					<li><a href="#">联系人姓名</a></li>
					<li><a href="#">QQ</a></li>
					<li><a href="#">手机号码</a></li>
					<li><a href="#">出发日期</a></li>
				</ul>
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputCustomerName">联系人姓名</label>
				<input type="text" name="customer_name" class="form-control" id="inputCustomerName" placeholder="联系人姓名">
			</div>
			<button type="submit" class="btn btn-primary">查找</button-->
			<div class="pull-left">
					状态过滤:
					<a href="/admin/customer?admin_id=<?=$admin_id?>&filter_key=<?=$filter_key?>" class="btn <?=$status0_btn_class?>" style="padding-top:3px; padding-bottom:3px;">所有</a>
					<a href="/admin/customer?admin_id=<?=$admin_id?>&filter_key=<?=$filter_key?>&status=1" class="btn <?=$status1_btn_class?>" style="padding-top:3px; padding-bottom:3px;">进行中</a>
					<a href="/admin/customer?admin_id=<?=$admin_id?>&filter_key=<?=$filter_key?>&status=5" class="btn <?=$status5_btn_class?>" style="padding-top:3px; padding-bottom:3px;">已结束</a>
					<a href="/admin/customer?admin_id=<?=$admin_id?>&filter_key=<?=$filter_key?>&status=-2" class="btn <?=$status_2_btn_class?>" style="padding-top:3px; padding-bottom:3px;">已取消</a>
					&nbsp;
					关键词搜索:
					<div class="form-group">
						<input type="text" name="filter_key" class="form-control" id="filter_key" placeholder="联系人姓名、QQ或旺旺" value="<?=$filter_key?>"/>
						<input type="hidden" name="status" value="<?=$status?>"/>
					</div>
					<button type="submit" class="btn btn-sm btn-primary">查找</button>
					&nbsp;
					<a href="/admin/customer?admin_id=<?=$admin_id?>&filter_key=<?=$filter_key?>&status=<?=$status?>&orderby=<?=$next_orderby?>" class="btn btn-sm btn-primary <?=$orderby_btn_class?>">按出发时间排序<?=$orderby_mark?></a>
			</div>
			
			<a href="/admin/customer/add" class="btn btn-success btn-sm pull-right">添加客户</a>
		<!--div class="btn-group">
			<button type="button" class="btn btn-primary">我的客户</button>
		    <button type="button" class="btn btn-primary">所有客户</button>
			<button type="button" class="btn btn-primary">已经完成客户</button>
		</div-->
		</form>
	</div>
	<div class="table-responsive clearfix" >
		<table class="table table-bordered customer-list-table ">
			<thead>
				<tr style="background:#eee;">
					<th>姓名</th>
					<th width="35%">行程需求</th>
					<th>联系方式</th>
					<th>出发</th>
					<th>目的</th>
					<th>修改时间</th>
					<th>来源</th>
				</tr>
			</thead>
			<tbody>
				<?php foreach($customer_list as $key=>$customer):?>
				<tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?> customer_id="<?=$customer['id'];?>">
					<td rowspan="2" style="vertical-align:middle;min-width:100px;">
						<a target="_blank" href="/admin/customer/detail?id=<?=$customer['id'];?>"><?=$customer['contact'];?></a>
						<?php if(empty($customer['admin'])):?>
						<a href="javascript:void(0);" class="btn btn-xs btn-warning claim-btn">认领</a>
						<?php else:?>
						<p>[<a style="color:#999; font-size:13px;" href="/admin/customer?admin_id=<?=$customer['admin_id'];?>"><?=$customer['admin']['nick_name'];?></a>]
                            <?php if(!empty($customer['user'])):?>
                               ==> <?=$customer['user']['nick_name'];?>
                                <?php endif;?>
						</p>
						<!--p class="<?=$customer['check_item_expires_warning_class'];?>" style="font-size:13px;"><a class="check_list_btn" href="javascript:void(0);"><?=customerStatusCn($customer['status']);?>&nbsp;<span class="<?=$customer['check_item_expires_warning_class'];?>" style=""><?=$customer['check_item_complete_count'];?>/<?=$customer['check_item_count']?></span></a></p-->
						<?php endif;?>
					</td>
					<td rowspan="2">
						<?php if(mb_strlen($customer['remark'], 'utf-8') > 150):?>
						<div id='c-<?=$customer['id'];?>-partial'>
							<?=nl2br(mb_substr($customer['remark'], 0, 150, 'utf-8'));?>...<br/>
							<a href="#", onClick="javascript:document.getElementById('c-<?=$customer['id'];?>-partial').style.display='none';document.getElementById('c-<?=$customer['id'];?>-all').style.display='';return false;">show all</a>
						</div>
						<div id='c-<?=$customer['id'];?>-all' style="display:none;">
							<?=nl2br($customer['remark']);?><br/>
							<a href="#", onClick="javascript:document.getElementById('c-<?=$customer['id'];?>-partial').style.display='';document.getElementById('c-<?=$customer['id'];?>-all').style.display='none';return false;">hide all</a>
						</div>
						<?php else:?>
						<?=nl2br($customer['remark']);?>
							<?php if(!empty($customer['trip_id'])):?>
							<br/>
								<a class="glyphicon glyphicon-pencil" target="_blank" href="/plan/create/edit?trip_id=<?=$customer['trip_id'];?>"></a>
								<a class="glyphicon glyphicon-file" target="_blank" href="/plan/export/excel?trip_id=<?=$customer['trip_id'];?>"></a>
							<?php endif;?>
						<?php endif;?>
					</td>
					<td rowspan="2" style="min-width:130px;font-family:Courier New;">
						M：<?=$customer['mobile'];?><br/>
						Q：<?=$customer['qq'];?><br/>
						W：<?=$customer['wangwang'];?><br/>

					</td>
					<td style="min-width:100px;"><?=$customer['origin'];?></td>
					<td><?=$customer['destination'];?></td>
					<td style="min-width:100px;"><?=date('m-d H:i', strtotime($customer['ctime']));?></td>
					<td><?=($customer['service_fee'] == 0) ? '未付款' : '￥' .$customer['service_fee'];?></td>
				</tr>
				<tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?>>
					<td>
						<?php if($customer['start_date'] !='0000-00-00'):?>
						<?=$customer['start_date'];?><br/>
						<span style="font-size:12px;color:#666;"><?=human_time_admin_hp(strtotime($customer['start_date']))?></span>
						<?php endif;?>
					</td>
					<td>
						<?php if($customer['end_date'] !='0000-00-00'):?>
						<?=$customer['end_date'];?><br/>
						<span style="font-size:12px;color:#666;"><?=human_time_admin_hp(strtotime($customer['end_date']))?>
						<?php if(strtotime($customer['start_date']) > strtotime($customer['end_date'])):?>
						<span class="text-danger">（!）</span></span>
						<?php endif;?>
						<?php endif;?>
					</td>
					<td><?=date('m-d H:i', strtotime($customer['mtime']));?></td>
					<td style="width:100px;word-break:break-all;"><?=$customer['platform'];?><p><?=$customer['source'];?></p></td>
				</tr>
	<?php endforeach;?>
			</tbody>
		</table>
					<!--


					<td></td>
					<td><?=$customer['qq'];?></td>
					<td><?=$customer['wangwang'];?></td>
					<td><?=$customer['origin'];?></td>
					<td style="min-width:120px;"><?=$customer['destination'];?></td>
					<td><?=$customer['start_date'];?></td>
					<td><?=($customer['service_fee'] == 0) ? '未付款' : '￥' .$customer['service_fee'];?></td>
					<td rowspan="2">
						<a class="btn btn-xs btn-primary" target="_blank" href="/admin/customer/detail?id=<?=$customer['id'];?>">详情</a>
					</td>
					<td></td>
				</tr>
				<tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?>>
					<td colspan="5" style="max-width:300px;"><?=nl2br($customer['remark']);?></td>
					<td><?=$customer['end_date'];?></td>
					<td></td>
					<td></td>
				</tr>
			</tbody>
		</table>
		-->
		<?php 
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
		
	</div>
	<div class=" alert alert-success">符合条件的客户数：<?=$count;?>个</div>
</div>
<?
$this->render('check_item');
?>
<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/customer_index');
	});
</script>
