<div class="customer_list">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/customer">客户列表</a></li>
		<li class="active">编辑</li>
	</ol>
	<div class="customer-detail container">
		<form class="form-horizontal" role="form" id="customer_edit_form">
			<input type="hidden" name="customer_id" value="<?=isset($customer) ? $customer['id']:'';?>">
				<div class="form-group">
					<label for="inputContact" class="col-lg-2 control-label">联系人姓名</label>
					<div class="col-lg-2">
						<input type="text" name="contact" class="form-control" id="inputContact" placeholder="" value="<?=isset($customer)? $customer['contact'] : '';?>">
						<span class="help-inline"></span>
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputContact" class="col-lg-2 control-label">性别</label>
					<div class="col-lg-2">
						<select name="sex" class="form-control">
							<option <?php if(isset($customer) && $customer['sex'] == 'female'):?> selected<?php endif;?> value="female">女士</option>
							<option <?php if(isset($customer) && $customer['sex'] == 'male'):?> selected<?php endif;?> value="male">先生</option>
						</select>
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputOrigin" class="col-lg-2 control-label">出发城市</label>
					<div class="col-lg-2">
						<input type="text" name="origin" class="form-control" id="inputOrigin" placeholder="" value="<?=isset($customer)? $customer['origin'] : '';?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inputDestination" class="col-lg-2 control-label">目的地</label>
					<div class="col-lg-3">
						<input type="text" name="destination" class="form-control" id="inputDestination" placeholder="多个用逗号隔开" value="<?=isset($customer) ? $customer['destination'] :'';?>">
						<span class="help-inline"></span>
					</div>
					<ul class="list-inline default-destination">
						<li>沙巴</li>
						<li>吉隆坡</li>
						<li>曼谷</li>
						<li>清迈</li>
						<li>普吉岛</li>
						<li>台湾</li>
					</ul>
				</div>
				<div class="form-group">
					<label for="inputStartDate" class="col-lg-2 control-label">旅行日期</label>
					<div class="col-lg-2">
						<div class="input-append date date-picker"  data-date="2014-01-01" data-date-format="yyyy-mm-dd">
							<input placeholder="出发日期" name="start_date" class="form-control" style="display:inline-block" size="16" type="text" value="<?=isset($customer) ? $customer['start_date'] : '';?>">
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
					<div class="col-lg-2">
						<div class="input-append date date-picker"  data-date="2014-01-01" data-date-format="yyyy-mm-dd">
							<input placeholder="回程日期" name="end_date" class="form-control" style="display:inline-block" size="16" type="text" value="<?=isset($customer) ? $customer['end_date'] : '';?>">
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
				</div>
				<div class="form-group">
					<label for="peopleNums" class="col-lg-2 control-label">出行人数</label>
					<div class="col-lg-3">
						<input type="text" name="num_of_people" class="form-control" id="peopleNums" placeholder="如：2大1小（小孩4岁）"  value="<?=isset($customer) ? $customer['num_of_people'] :'';?>">
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputRemark" class="col-lg-2 control-label">备注说明</label>
					<div class="col-lg-5">
						<textarea class="form-control" placeholder="填写关于该用户的需求描述 和任何与该用户相关的操作，备注" name="remark" id="inputRemark" rows="5"><?=isset($customer) ? $customer['remark'] :'';?></textarea>
						<span class="help-inline"></span>
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputMobile" class="col-lg-2 control-label">手机号码</label>
					<div class="col-lg-2">
						<input type="text" name="mobile" class="form-control" id="inputMobile" placeholder="" value="<?=isset($customer) ? $customer['mobile'] :'';?>">
					</div>
					<span class="help-block">必填</span>
				</div>
				<div class="form-group">
					<label for="inputQQ" class="col-lg-2 control-label">QQ</label>
					<div class="col-lg-2">
						<input type="text" name="qq" class="form-control" id="inputQQ" placeholder="" value="<?=isset($customer) ? $customer['qq'] :'';?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inputEmail" class="col-lg-2 control-label">邮箱</label>
					<div class="col-lg-3">
						<input type="text" name="email" class="form-control" id="inputEmail" placeholder="" value="<?=isset($customer) ? $customer['email'] :'';?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inputWeixin" class="col-lg-2 control-label">微信号</label>
					<div class="col-lg-2">
						<input type="text" name="weixin" class="form-control" id="inputWeixin" placeholder="" value="<?=isset($customer) ? $customer['weixin'] :'';?>">
					</div>
				</div>
				<!--div class="form-group">
					<label for="inputWangwang" class="col-lg-2 control-label">旺旺</label>
					<div class="col-lg-2">
						<input type="text" name="wangwang" class="form-control" id="inputWangwang" placeholder="" value="<?=isset($customer) ? $customer['wangwang'] :'';?>">
					</div>
					<span class="help-block">通过淘宝支付服务费的必填</span>
				</div>
				<div class="form-group">
					<label for="inputServiceFee" class="col-lg-2 control-label">服务费</label>
					<div class="col-lg-2">
						<input type="text" name="service_fee" class="form-control" id="inputServiceFee" placeholder="" value="<?=isset($customer) ? $customer['service_fee'] :'';?>">
					</div>
					<span class="help-block">0表示未付款，一般100</span>
				</div-->
				<!--div class="form-group">
					<label for="inputTaobao" class="col-lg-2 control-label">淘宝交易号</label>
					<div class="col-lg-3">
						<input type="text" name="taobao_trade_id" class="form-control" id="inputTaobao" placeholder="" value="<?=isset($customer) ? $customer['taobao_trade_id'] :'';?>">
					</div>
					<div class="col-lg-2">
						<a class="btn btn-default taobao-trade-btn" href="javascript:void(0);">点击获取淘宝交易</a>
					</div>
				</div>
				<div class="form-group">
					<label for="inputJourneyURL" class="col-lg-2 control-label">行程单URL</label>
					<div class="col-lg-5">
						<input type="text" name="journey_url" class="form-control" id="inputJourneyURL" placeholder="" value="<?=isset($customer) ? $customer['journey_url'] :'';?>">
					</div>
				</div-->
				<!--div class="form-group">
					<label for="inputTripID" class="col-lg-2 control-label">行程ID</label>
					<div class="col-lg-2">
						<input type="text" name="trip_id" class="form-control" id="inputTripID" placeholder="" value="<?=isset($customer) ? $customer['trip_id'] :'';?>">
					</div>
				</div-->
				<!--div class="form-group">
					<label for="inputSource" class="col-lg-2 control-label">来源</label>
					<div class="col-lg-2">
						<input type="text" name="source" class="form-control" id="inputSource" placeholder="" value="<?=isset($customer) ? $customer['source'] :'';?>">
					</div>
				</div-->
				<!--div class="form-group">
					<label for="inputAdminID" class="col-lg-2 control-label">客服ID</label>
					<div class="col-lg-2">
						<input type="text" name="admin_id" class="form-control" id="inputAdminID" placeholder="" value="<?=isset($customer) ? $customer['admin_id'] : Yii::app()->adminUser->id;?>">
					</div>
				</div-->

                <div class="form-group">
                    <label for="inputAdminID" class="col-lg-2 control-label">指派地接</label>
                    <div class="col-lg-2">
                        <input type="text" name="user_id" class="form-control" id="inputAdminID" placeholder="" value="<?=isset($customer) ? $customer['user_id'] : Yii::app()->adminUser->id;?>">
                    </div>
                    <ul class="list-inline user_list">
                        <li user_id="1">台湾凤姐</li>
                        <li user_id="2">日本丹丹</li>
                        <li user_id="3">日本甘姐</li>
                        <li user_id="4">普吉岛stone</li>

                        <li user_id="5">泰国区域经理</li>
                    </ul>
                </div>


            <div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">保存修改</button>
<?php if(isset($customer)):?>
						<a href="/admin/customer/detail?id=<?=$customer['id'];?>" class="btn btn-default">返回客户信息</a>
<?php endif;?>
						<span class="help-block"><span>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>

<script type="text/template" id="taobao_tredes_tpl">
	<div class="modal-header">
		<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
		<h4 class="modal-title" id="myModalLabel">淘宝交易列表</h4>
	</div>
	<div class="modal-body">
		<table class="table">
			<thead>
				<tr>
					<th>支付时间</th>
					<th>支付金额</th>
					<th>旺旺</th>
					<th>状态</th>
					<th>关联</th>
				</tr>
			</thead>
			<tbody>
			{{#service_fee}}
				<tr class="taobao_trade_item" buyer_nick={{buyer_nick}} total_fee={{total_fee}} trade_id={{tid}}>
					<td>{{pay_time}}</td>
					<td>{{total_fee}}</td>
					<td>{{buyer_nick}}</td>
					<td>{{status}}</td>
					<td><a href="javascript:void(0);" class="btn btn-xs btn-primary">选择</a></td>
				</tr>
			{{/service_fee}}
			</tbody>
		</table>
		<p class="alert alert-warning">如果交易列表为空或者不正确，请点击<a href="/admin/oauth/taobao" target="_blank">淘宝API授权</a>(每24小时会失效)</p>
	</div>
	<div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
	</div>
</script>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/customer_edit');
	});
</script>
