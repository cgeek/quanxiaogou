<div class="customer_list clearfix">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/customer/list">所有客户列表</a></li>
		<li class="active">我的客户</li>
	</ol>
	<div class="filter-bar clearfix">
		<form class="form-inline" role="form">
			<div class="clearfix" style="margin-bottom:10px;">
				<div class="pull-left">
					搜索:
					<div class="form-group">
						<input type="text" name="filter_key" class="form-control" id="filter_key" placeholder="联系人姓名、QQ或旺旺" value="<?=isset($filter_key) ? $filter_key :'';?>"/>
						<input type="hidden" name="status" value="<?=isset($status) ? $status : '';?>"/>
					</div>
					<button type="submit" class="btn btn-sm btn-primary">查找</button>
					&nbsp;
				</div>
			
				<a href="/admin/customer/add" class="btn btn-success btn-sm pull-right">添加客户</a>
			</div>
			<div class="clearfix">
				<div class="pull-left">
					<div class="btn-group" >
						<a class="btn btn-sm btn-default <?php if(!isset($_GET['status']) && !isset($_GET['now'])):?>active <?php endif?>" href="/admin/customer">全部</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == 1):?>active <?php endif?>" href="/admin/customer?status=1">等待第一次联系</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == 2):?>active <?php endif?>" href="/admin/customer?status=2">等待确认需求</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == 3):?>active <?php endif?>" href="/admin/customer?status=3">初步行程中</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == 4):?>active <?php endif?>" href="/admin/customer?status=4">细化行程中</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == 5):?>active <?php endif?>" href="/admin/customer?status=5">规划完成，等待反馈</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == 6):?>active <?php endif?>" href="/admin/customer?status=6">旅行结束已反馈</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == -2):?>active <?php endif?>" href="/admin/customer?status=-2">已取消</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == -1):?>active <?php endif?>" href="/admin/customer?status=-1">已删除</a>
					</div>

					<div class="btn-group" >
						<a class="btn btn-sm btn-default <?php if(isset($_GET['now']) && $_GET['now'] == 'going'):?>active <?php endif?>" href="/admin/customer?now=going">
						即将出发的
						</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['now']) && $_GET['now'] == 'in'):?>active <?php endif?>" href="/admin/customer?now=in">
						在路上的
						</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['now']) && $_GET['now'] == 'return'):?>active <?php endif?>" href="/admin/customer?now=return">
						刚回来的
						</a>
					</div>
				</div>
			</div>
		</form>

	</div>
	<div class="table-responsive clearfix" >
		<table class="table table-bordered customer-list-table ">
			<thead>
				<tr style="background:#eee;">
					<th>姓名</th>
					<th width="35%">行程需求</th>
					<th>联系方式</th>
					<th>出发</th>
					<th>目的</th>
					<th>修改时间</th>
					<th>来源</th>
					<th>常用操作</th>
				</tr>
			</thead>
			<tbody>

				
				<?php foreach($new_customer_list as $key=>$customer):?>
				<tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?> customer_id="<?=$customer['id'];?>">
					<td rowspan="2" style="vertical-align:middle;min-width:100px;">
                        <a target="_blank" href="/admin/customer/detail?id=<?=$customer['id'];?>"><?=$customer['contact'];?></a>
                        <p><?=$customer['uid'];?></p>
						<?php if(empty($customer['admin_id'])):?>
						<a href="javascript:void(0);" class="btn btn-xs btn-warning claim-btn">认领</a>
						<?php endif;?>

					</td>
					<td rowspan="2">
						<?php if(mb_strlen($customer['remark'], 'utf-8') > 150):?>
						<div id='c-<?=$customer['id'];?>-partial'>
							<?=nl2br(mb_substr($customer['remark'], 0, 150, 'utf-8'));?>...<br/>
							<a href="#", onClick="javascript:document.getElementById('c-<?=$customer['id'];?>-partial').style.display='none';document.getElementById('c-<?=$customer['id'];?>-all').style.display='';return false;">show all</a>
						</div>
						<div id='c-<?=$customer['id'];?>-all' style="display:none;">
							<?=nl2br($customer['remark']);?><br/>
							<a href="#", onClick="javascript:document.getElementById('c-<?=$customer['id'];?>-partial').style.display='';document.getElementById('c-<?=$customer['id'];?>-all').style.display='none';return false;">hide all</a>
						</div>
						<?php else:?>
						<?=nl2br($customer['remark']);?>
							<?php if(!empty($customer['trip_id'])):?>
							<br/>
								<a class="glyphicon glyphicon-pencil" target="_blank" href="/plan/create/edit?trip_id=<?=$customer['trip_id'];?>"></a>
								<a class="glyphicon glyphicon-file" target="_blank" href="/plan/export/excel?trip_id=<?=$customer['trip_id'];?>"></a>
							<?php endif;?>
						<?php endif;?>
					</td>
					<td rowspan="2" style="min-width:130px;font-family:Courier New;">
						M：<?=$customer['mobile'];?><br/>
						Q：<?=$customer['qq'];?><br/>
						W：<?=$customer['wangwang'];?><br/>
						E：<?=$customer['email'];?><br/>

					</td>
					<td style="min-width:100px;"><?=$customer['origin'];?></td>
					<td><?=$customer['destination'];?></td>
					<td style="min-width:100px;"><?=date('m-d H:i', strtotime($customer['ctime']));?></td>
					<td><?=($customer['service_fee'] == 0) ? '未付款' : '￥' .$customer['service_fee'];?></td>
					<td></td>
				</tr>
				<tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?>>
					<td>
						<?php if($customer['start_date'] !='0000-00-00'):?>
						<?=$customer['start_date'];?><br/>
						<span style="font-size:12px;color:#666;"><?=human_time_admin_hp(strtotime($customer['start_date']))?></span>
						<?php endif;?>
					</td>
					<td>
						<?php if($customer['end_date'] !='0000-00-00'):?>
						<?=$customer['end_date'];?><br/>
						<span style="font-size:12px;color:#666;"><?=human_time_admin_hp(strtotime($customer['end_date']))?>
						<?php if(strtotime($customer['start_date']) > strtotime($customer['end_date'])):?>
						<span class="text-danger">（!）</span></span>
						<?php endif;?>
						<?php endif;?>
					</td>
					<td><?=date('m-d H:i', strtotime($customer['mtime']));?></td>
					<td style="width:100px;word-break:break-all;"><?=$customer['platform'];?><p><?=$customer['source'];?></p></td>
					<td></td>
				</tr>
				<?php endforeach;?>

				<?php foreach($customer_list as $key=>$customer):?>
				<tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?> customer_id="<?=$customer['id'];?>">
					<td rowspan="2" style="vertical-align:middle;min-width:100px;">
						ID：<?=$customer['id'];?><br />
						<a target="_blank" href="/admin/customer/detail?id=<?=$customer['id'];?>"><?=$customer['contact'];?></a>
						<?php if(empty($customer['admin'])):?>
						<a href="javascript:void(0);" class="btn btn-xs btn-warning claim-btn">认领</a>
						<?php else:?>
						<p>[<a style="color:#999; font-size:13px;" href="/admin/customer?admin_id=<?=$customer['admin_id'];?>"><?=$customer['admin']['nick_name'];?></a>]

                            <?php if(!empty($customer['user'])):?>
                               <span style="color:red">==> <?=$customer['user']['nick_name'];?></span>
                            <?php endif;?>

						</p>
						<!--p class="<?=isset($customer['check_item_expires_warning_class']) ? $customer['check_item_expires_warning_class'] : '';?>" style="font-size:13px;">
							<a class="check_list_btn" href="javascript:void(0);">任务<span class="<?=isset($customer['check_item_expires_warning_class']) ? $customer['check_item_expires_warning_class'] : '';?>" style=""><?=$customer['check_item_complete_count'];?>/<?=$customer['check_item_count']?></span></a>
						</p-->
						<?php endif;?>
					</td>
					<td rowspan="2">
						<?php if(mb_strlen($customer['remark'], 'utf-8') > 150):?>
						<div id='c-<?=$customer['id'];?>-partial'>
							<?=nl2br(mb_substr($customer['remark'], 0, 150, 'utf-8'));?>...<br/>
							<a href="#", onClick="javascript:document.getElementById('c-<?=$customer['id'];?>-partial').style.display='none';document.getElementById('c-<?=$customer['id'];?>-all').style.display='';return false;">show all</a>
						</div>
						<div id='c-<?=$customer['id'];?>-all' style="display:none;">
							<?=nl2br($customer['remark']);?><br/>
							<a href="#", onClick="javascript:document.getElementById('c-<?=$customer['id'];?>-partial').style.display='';document.getElementById('c-<?=$customer['id'];?>-all').style.display='none';return false;">hide all</a>
						</div>
						<?php else:?>
						<?=nl2br($customer['remark']);?>
						<?php endif;?>
						<?php if(!empty($customer['trip_id'])):?>
						<br/>
							<a class="glyphicon glyphicon-pencil" target="_blank" href="/plan/create/edit?trip_id=<?=$customer['trip_id'];?>"></a>
							<a class="glyphicon glyphicon-file" target="_blank" href="/plan/export/excel?trip_id=<?=$customer['trip_id'];?>"></a>
						<?php endif;?>
						<p><span class="label label-<?=customerStatusColor($customer['status']);?>"><?=customerStatusCn($customer['status']);?></span></p>
					</td>
					<td rowspan="2" style="min-width:130px;font-family:Courier New;">
						M：<?=$customer['mobile'];?><br/>
						Q：<?=$customer['qq'];?><br/>
						W：<?=$customer['wangwang'];?><br/>
						E：<?=$customer['email'];?><br/>
                        SJY: <?=$customer['uid'];?></br>
					</td>
					<td style="min-width:100px;"><?=$customer['origin'];?></td>
					<td><?=$customer['destination'];?></td>
					<td style="min-width:100px;"><?=date('m-d H:i', strtotime($customer['ctime']));?></td>
					<td><?=($customer['service_fee'] == 0) ? '未付款' : '￥' .$customer['service_fee'];?></td>
					<td rowspan=2>
						<p><a href="/admin/customer/edit?id=<?=$customer['id'];?>" target="_blank" class="btn btn-xs btn-default">修改资料</a></p>
						<p><a href="/admin/order/add?customer_id=<?=$customer['id'];?>" target="_blank" class="btn btn-xs btn-default">添加代订</a></p>
					<?php if(empty($customer['trip_id'])):?>
						<p><a href="/plan/create/step1?customer_id=<?=$customer['id'];?>" target="_blank" class="btn btn-xs btn-default">创建行程</a></p>
					<?php else:?>
						<p><a href="/plan/create/edit?trip_id=<?=$customer['trip_id'];?>" target="_blank" class="btn btn-xs btn-default">修改行程</a></p>
					<?php endif;?>
					</td>
				</tr>
				<tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?>>
					<td>
						<?php if($customer['start_date'] !='0000-00-00'):?>
						<?=$customer['start_date'];?><br/>
						<span style="font-size:12px;color:#666;"><?=human_time_admin_hp(strtotime($customer['start_date']))?></span>
						<?php endif;?>
					</td>
					<td>
						<?php if($customer['end_date'] !='0000-00-00'):?>
						<?=$customer['end_date'];?><br/>
						<span style="font-size:12px;color:#666;"><?=human_time_admin_hp(strtotime($customer['end_date']))?>
						<?php if(strtotime($customer['start_date']) > strtotime($customer['end_date'])):?>
						<span class="text-danger">（!）</span></span>
						<?php endif;?>
						<?php endif;?>
					</td>
					<td><?=date('m-d H:i', strtotime($customer['mtime']));?></td>
					<td style="width:100px;word-break:break-all;"><?=$customer['platform'];?><p><?=$customer['source'];?></p></td>
				</tr>
	<?php endforeach;?>
			</tbody>
		</table>

		<?php 
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
		
	</div>
	<div class=" alert alert-success">符合条件的客户数：<?=$count;?>个</div>
</div>
<?php
$this->render('check_item');
?>
<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/customer_index');
	});
</script>
