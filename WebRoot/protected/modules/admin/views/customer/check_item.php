<script type="text/template" id="customer_check_list_modal_tpl">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
	    	<h4 class="modal-title" id="myModalLabel">{{customer.contact}}的待办事项</h4>
	   	</div>
		<div class="modal-body">
			<h5>
				<span class="glyphicon glyphicon-th-list"></span>
				待完成的工作列表
			</h5>
			<div class="progress">
				<div class="progress-bar" role="progressbar" aria-valuenow="60" aria-valuemin="0" aria-valuemax="100" style="width: {{customer.progress}}%;">
			 	   <span class="sr-only">{{customer.progress}}% Complete</span>
				</div>
			</div>
			<ul class="task-list " customer_id={{customer.id}} >
			{{#check_item_list}}
				<li class="check-item task {{#complete}}complete {{/complete}}clearfix" item-id="{{id}}">
					<div class="checkbox enabled">✓</div>
					<div class="check-item-text editable clearfix">
					<p class="hide-on-edit markeddown {{title_class}}">{{title}}<span style="font-size:10px;margin-left:10px;" class="{{time_left_class}}">{{time_left}}</span></p>
						<div class="edit options-menu" >
							<textarea type="text" class="field full single-line">{{title}}</textarea>
							<div class="edit-controls clearfix">
								<div class="due_date_input" style="text-align:right;">
									<label>提醒时间:</label>
									<div class="input-append date"  data-date="{{due_date_default}}" data-date-format="yyyy-mm-dd" style="width:200px;display:inline-block;">
										<input placeholder="{{due_date}}" name="due_date" class="form-control field " style="width:200px;display:inline-block" size="16" type="text" value="{{due_date}}">
										<span class="add-on"><i class="icon-th"></i></span>					
									</div>
								</div>
								<button class="btn btn-sm btn-success save-btn">保存</button> 
								<a href="javascript:void(0);" class="glyphicon glyphicon-remove cancel"></a>
								<a href="javascript:void(0);" class="option delete">删除</a>
							</div>
						</div>
					</div>
				</li>
			{{/check_item_list}}
			</ul>
			<div class="new-task task" customer_id={{customer.id}}>
				<textarea class="new-task-text full new-checkitem-input" placeholder="添加新任务"></textarea>
				<div class="add-controls clearfix">
					<div class="due_date_input" style="text-align:right;">
						<label>提醒时间:</label>
						<div class="input-append date"  data-date="{{tomorrow}}" data-date-format="yyyy-mm-dd" style="width:200px; display:inline-block;">
							<input placeholder="{{tomorrow}}" name="due_date" class="form-control " style="display:inline-block; width:200px;" size="16" type="text" value="{{tomorrow}}">
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
					<button class="btn btn-sm btn-success add-checklist-item">保存</button>
					<a href="javascript:void(0);" class="glyphicon glyphicon-remove cancel"></a>
				</div>
			</div>
<?php if(empty($hidden_comment)): ?>
			<h5>
				<span class="glyphicon glyphicon-th"></span>
				客户动态
			</h5>
			<div class="new-comment" customer_id={{customer.id}}>
				<div class="member no-menu">
					<img class="member-avatar" height="30" width="30" src="https://trello-avatars.s3.amazonaws.com/20ecd9cd5ae482616a64a979a764a4af/30.png" alt="cgeek (cgeek)" title="cgeek (cgeek)">
					<span class="member-gold-badge"></span>
				</div>
				<textarea class="new-comment-input" placeholder="写点什么关于这个客户的everything"></textarea>
				<div class="add-controls clearfix">
					<button class="btn btn-sm btn-success add-comment-btn">提交</button>
					<a  href="#" class="glyphicon glyphicon-remove cancel"></a> 
				</div>
			</div>
			<ul class="activity-list " customer_id={{customer.id}} >
			{{#comment_list}}
				<li>{{content}}
					<p style="font-size:12px; color:#ccc;">{{ctime}}</p>
				</li>
			{{/comment_list}}
			</ul>
<?php endif;?>
		</div>
</script>

<script type="text/template" id="task-list-item-tpl">
				<li class="check-item task clearfix" item-id="{{id}}">
					<div class="checkbox enabled">✓</div>
					<div class="check-item-text editable clearfix">
						<p class="hide-on-edit markeddown">{{title}}{{time_left}}</p>
						<div class="edit options-menu" >
							<textarea type="text" class="field full single-line">{{title}}</textarea>
							<div class="edit-controls clearfix">
								<div class="due_date_input" style="text-align:right;">
									<label>提醒时间:</label>
									<div class="input-append date"  data-date="{{due_date}}" data-date-format="yyyy-mm-dd"  style="width:200px; display:inline-block">
										<input placeholder="提醒时间" name="due_date" class="form-control field" style="width:200px; display:inline-block" size="16" type="text" value="{{due_date}}">
										<span class="add-on"><i class="icon-th"></i></span>
									</div>
								</div>
								<button class="btn btn-sm btn-success save-btn">保存</button> 
								<a href="javascript:void(0);" class="glyphicon glyphicon-remove cancel"></a>
								<a href="javascript:void(0);" class="option delete">删除</a>
							</div>
						</div>
					</div>
				</li>
</script>
