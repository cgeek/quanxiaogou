<div class="clearfix">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/customer">客户列表</a></li>
		<li class="active"><?=$customer['contact'];?></li>
	</ol>
	<div class="customer-detail table-responsive">
		<!-- Nav tabs -->
		<ul class="nav nav-tabs">
			<li class="active" ><a href="#base_info" data-toggle="tab">基本信息</a></li>
			<li><a href="#traveler" data-toggle="tab">同伴资料(<?=count($traveler_list);?>)</a></li>
			<li><a href="#order" data-toggle="tab">代订信息</a></li>
		</ul>
		<!-- Tab panes -->
		<div class="tab-content">
			<div class="tab-pane active" id="base_info">
				<table class="table table-bordered customer-list-table " style="margin-top:10px;">
					<thead>
						<tr style="background:#eee;">
							<th>姓名</th>
							<th>人数</th>
							<td>出发地</td>
							<td>目的地</td>
							<td>出发日期</td>
							<td>回程日期</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td rowspan="9" style="min-width:50px;vertical-align:middle;">
								<?=$customer['contact'];?>
								<?php if(!empty($customer['admin'])):?>
								<p>【<a href="/admin/customer?admin_id=<?=$customer['admin_id'];?>"><?=$customer['admin']['nick_name'];?></a>】</p>
								<?php endif;?>
								<p>(<?=customerStatusCn($customer['status']);?>)</p>
								<p>
								</p>
							</td>
							<td><?=$customer['num_of_people'];?></td>
							<td><?=$customer['origin'];?></td>
							<td><?=$customer['destination'];?></td>
							<td><?=$customer['start_date'];?></td>
							<td><?=$customer['end_date'];?></td>
						</tr>
						<tr style="background:#eee;">
							<th>手机号码</th>
							<th>QQ号</th>
							<th>邮箱</th>
							<th>旺旺号</th>
							<td>微信uid</td>
						</tr>
						<tr>
							<td><?=$customer['mobile'];?></td>
							<td><?=$customer['qq'];?></td>
							<td><?=$customer['email'];?></td>
							<td><?=$customer['wangwang'];?></td>
							<td><?=$customer['weixin_uuid'];?></td>
						</tr>
						<tr style="background:#eee;">
							<td>服务费</td>
							<td>淘宝支付id</td>
							<td>来源</td>
							<td>来源平台</td>
							<td>创建时间</td>
						</tr>
						<tr>
							<td><?=$customer['service_fee'];?></td>
							<td><?=$customer['taobao_trade_id'];?></td>
							<td><?=$customer['platform'];?></td>
							<td><?=$customer['source'];?></td>
							<td><?=date('Y-m-d',strtotime($customer['ctime']));?></td>
						</tr>
						<tr style="background:#eee;">
							<td colspan="9">相关链接</td>
						</tr>
						<tr>
							<td colspan="9">
								<a class="btn btn-sm btn-default" href="/admin/customer/QQLog?customer_id=<?=$customer['id'];?>">聊天记录</a>
								<a class="btn btn-sm btn-default" href="https://www.dropbox.com/home/QGL/%E5%B0%8F%E7%8B%97%E6%97%85%E8%A1%8C/%E5%AE%A2%E6%88%B7%E8%B4%A6%E5%8D%95%26%E7%A1%AE%E8%AE%A4%E5%8D%95%26%E8%A1%8C%E7%A8%8B%E5%8D%95" target="_blank">确认单(需翻墙)</a>
								<?php if(!empty($customer['journey_url'])):?>
									<a class="btn btn-default" target="_blank" href="/admin/journey/google?customer_id=<?=$customer['id'];?>">查看行程单</a>
								<?php endif;?>
								<?php if(!empty($customer['trip_id'])):?>
									<a class="btn btn-primary btn-sm" target="_blank" href="/plan/create/edit?trip_id=<?=$customer['trip_id'];?>">修改行程</a>
									<a class="btn btn-success btn-sm" target="_blank" href="/plan/export/excel?trip_id=<?=$customer['trip_id'];?>">查看行程单</a>
								<?php else:?>
									<a class="btn btn-primary btn-sm" target="_blank" href="/admin/journey">创建行程</a>
                                <?php endif;?>

								<div class="pull-right">
									<button class="check_list_btn btn btn-sm btn-info" href="javascript:void(0);">待办事项(<?=$customer['progress'];?>%)</button>
									<a class="btn btn-sm btn-warning" href="/admin/order/add?customer_id=<?=$customer['id'];?>" target="_blank">添加代订</a>
									<a class="btn btn-sm btn-primary" href="/admin/customer/edit?id=<?=$customer['id'];?>">修改客户资料</a>
								</div>
							</td>
						</tr>
						<tr style="background:#eee;">
							<td colspan="9">备注信息</td>
						</tr>
						<tr>
							<td colspan="9"><?=nl2br($customer['remark']);?></td>
						</tr>
						<tr customer_id="<?=$customer['id'];?>">
							<td colspan="10">
								<input type="hidden" name="customer_id" value="<?=$customer['id'];?>">
								<div class="btn-group" data-toggle="buttons">
									<button class="btn btn-sm btn-success" <?php if($customer['status']>=1):?> disabled<?php endif;?> status=1 >已经认领</button>
									<button class="btn btn-sm btn-success update-status-btn" <?php if($customer['status']>=2):?> disabled<?php endif;?> status=2 >第一次联系客户</button>
									<button class="btn btn-sm btn-success update-status-btn" <?php if($customer['status']>=3):?> disabled<?php endif;?> status=3 >沟通需求完成</button>
									<button class="btn btn-sm btn-success update-status-btn" <?php if($customer['status']>=4):?> disabled<?php endif;?> status=4 >初步行程规划完成</button>
									<button class="btn btn-sm btn-success update-status-btn" <?php if($customer['status']>=5):?> disabled<?php endif;?> status=5 >详细行程规划完成</button>
									<button class="btn btn-sm btn-primary update-status-btn" <?php if($customer['status']==6):?> disabled<?php endif;?> status=6 >旅行结束已反馈</button>
									<button class="btn btn-sm btn-warning update-status-btn" <?php if($customer['status'] == -2):?> disabled<?php endif;?> status=-2 >客户取消服务</button>
									<button class="btn btn-sm btn-danger update-status-btn" <?php if($customer['status'] == -1):?> disabled<?php endif;?>  status=-1 >删除客户</button>
								</div>
									
							</td>
						</tr>
					</tbody>
				</table>
				<!--
				<div class="alert alert-success col-lg-6" >
				该用户支付服务费: <?=$customer['service_fee'];?> 元, 赚取代付差额总和：<?=$order_margin;?>元， 共计收入：<?=$customer['service_fee'] + $order_margin;?>元
				-->

				<div class="col-lg-6 col-sm-6 col-md-6  col-xs-12 pull-right">
					<div class="panel panel-danger">
						<div class="panel-heading">
							<h3 class="panel-title">交易流水</h3>
						</div>
			
						<!-- Table -->
						<table class="table">
							<thead>
								<tr>
									<th>项目</th>
									<th>流向</th>
									<th>金额</th>
									<th>状态</th>
								</tr>
							</thead>
							<tbody>
				<?php foreach($record_list as $key=>$record):?>
								<tr>
									<td style="width:200px;"><a target="_blank" href="https://lab.alipay.com/consume/queryTradeDetail.htm?tradeNo=<?=$record['alipay_order_no'];?>"><?=$record['order_title'];?></a></td>
									<td >
						<?php if($record['in_out_type'] == '收入'):?>
							<span style="color:#53a000; font-weight:bold;">+<?=$record['total_amount'];?></span>
						<?php else:?>
							<span style="color:#f37800; font-weight:bold;">-<?=$record['total_amount'];?></span>
						<?php endif;?>
									</td>
									<td ><?=$record['in_out_type'];?></td>
									<td ><?=$record['order_status'];?></td>
								</tr>
				<?php endforeach;?>
							</tbody>
				    	</table>
						<div class="panel-footer pull-right">
							流水结余：<?=sprintf('%01.2f', $record_margin);?> 元
						</div>
					</div>
				</div>


				<div class="col-lg-6 col-sm-6 col-md-6  col-xs-12 pull-right">
					<div class="panel panel-success">
						<div class="panel-heading">
							<h3 class="panel-title">收支损益表</h3>
						</div>
						<!-- Table -->
						<table class="table">
							<thead>
								<tr>
									<th>项目</th>
									<th>客户应付</th>
									<th>代理价格</th>
									<th>差额</th>
									<th>状态</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>服务费</td>
									<td><?=$customer['service_fee'];?></td>
								</tr>
				<?php foreach($order_list as $key=>$order):?>
								<tr>
									<td style="width:200px;"><a href="/admin/order/edit/?id=<?=$order['id'];?>"><?=$order['title'];?></a></td>
									<td ><?=$order['total_fee'];?></td>
									<td ><?=$order['payment'];?></td>
									<td <?php if($order['margin'] > 0) :?> style="color:green;"<?php endif;?> ><?=$order['margin'];?></td>
									<td ><?php if($order['status'] == 0) {echo '未付款'; } elseif($order['status'] == 1) {echo '已付款';} else if($order['status'] == 2) {echo '等待需支付尾款';};?></td>
								</tr>
				<?php endforeach;?>
							</tbody>
				    	</table>
						<div class="panel-footer pull-right">
							共计收入：<?=$customer['service_fee'] + $order_margin;?> 元
						</div>
					</div>
				</div>


				<div class="col-lg-6 col-sm-6 col-md-6 pull-left" style="background:#eee; border-radius:5px; margin-top:10px;">
					<h4>客户动态</h4>
					<form class="clearfix" method="POST" id="comment-form">
						<input type="hidden" name="object" value="customer-log">
						<input type="hidden" name="object_id" value="<?=$customer['id'];?>">
						<input type="hidden" name="admin_id" value="<?=Yii::app()->adminUser->id;?>">
						<textarea class="form-control" rows="3" name="content" placeholder="可以记录任何关于这个客户的情况"></textarea>
						<button class="btn btn-sm btn-primary pull-right" id="comment-submit-btn" style="margin-top:10px;">提交</button>
					</form>
					<ul class="comment-list list-unstyled">
			<?php if(!empty($comment_list)):?>
				<?php foreach($comment_list as $comment):?>
						<li><?=nl2br($comment['content']);?> <p style="text-align:right; color:#999;"><?=$comment['ctime'];?></p></li>
				<?php endforeach;?>
			<?php endif;?>
					</ul>
				</div>

			</div>

			<div class="tab-pane" id="traveler">
				<!-- traveler list-->
				<table class="table table-bordered customer-list-table " style="margin-top:10px;">
		<?php if(!empty($traveler_list)):?>
					<thead>
						<tr style="background:#eee;">
							<th>姓名</th>
							<th>英文名</th>
							<th>性别</th>
							<th>出生日期</th>
							<th>护照号</th>
							<th>护照有效期</th>
							<th>年龄</th>
							<th>身份证号</th>
							<th>修改时间</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
			<?php foreach($traveler_list as $traveler):?>
						<tr>
							<td><?=($traveler['is_customer'] == 1) ? '<strong>' . $traveler['name'] . '</strong>' : $traveler['name'] ;?></td>
							<td><?=$traveler['last_name'];?>  <?=$traveler['first_name'];?></td>
							<td><?=($traveler['sex'] == 'male') ? '男' :'女';?></td>
							<td><?=$traveler['birthday'];?></td>
							<td><?=$traveler['passport_no'];?></td>
							<td><?=$traveler['passport_validity'];?></td>
							<td><?=$traveler['age'];?></td>
							<td><?=$traveler['id_no'];?></td>
							<td><?=date('Y-m-d',strtotime($traveler['mtime']));?></td>
							<td>
								<a class="btn btn-default btn-xs" href="/admin/traveler/edit/?id=<?=$traveler['id'];?>">修改资料</a>
							</td>
						</tr>
			<?php endforeach;?>
					</tbody>
		<?php else:?>
					<tbody>
						<tr>
							<td colspan=12>暂无旅客信息</td>
						</tr>
					</tbody>
		<?php endif;?>
				</table>
				<a class="btn btn-sm btn-primary" href="/admin/traveler/add?customer_id=<?=$customer['id'];?>">添加同伴资料</a>
			</div>
			<div class="tab-pane" id="order">
				<!-- order-list -->
				<table class="table table-bordered order-list-table" style="margin-top:10px;">
		<?php if(!empty($order_list)): ?>
					<thead>
						<tr style="background:#eee;">
							<th>代订类型</th>
							<th>代订项目名称</th>
							<th>卖价</th>
							<th>进价(代理价)</th>
							<th>差额(利润)</th>
							<th>修改时间</th>
							<th>操作</th>
						</tr>
					</thead>
					<tbody>
			<?php foreach($order_list as $key=>$order):?>
						<tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?>>
							<td><?=orderTypeCN($order['type']);?></td>
							<td><strong><a href="/admin/order/edit/?id=<?=$order['id'];?>"><?=$order['title'];?></a></strong></td>
							<td><?php if(!empty($order['total_fee'])):?>￥<?=$order['total_fee'];?><?php endif;?></td>
							<td><?php if(empty($order['payment'])) :?>0<?php else:?>￥<?=$order['payment'];?><?php endif;?></td>
							<td>￥<?=round(($order['total_fee'] - $order['payment']), 2);?></td>
							<td rowspan=2><?=date('Y-m-d',strtotime($order['mtime']));?></td>
							<td rowspan=2>
								<a class="btn btn-default btn-xs" href="/admin/order/edit/?id=<?=$order['id'];?>">修改信息</a>
							</td>
						</tr>
						<?php if(!empty($order['remark'])):?>
						<tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?>>
							<td colspan="9"><?=nl2br($order['remark']);?> <br>
							支出：<?=$order['payment_trade_id'];?> | 收入： <?=$order['payout_trade_id'];?></td>
						</tr>
						<?php endif;?>
			<?php endforeach;?>
					</tbody>
		<?php else:?>
					<tbody>
						<tr>
							<td colspan=5>暂无代订项目</td>
						</tr>
					</tbody>
		<?php endif;?>
				</table>
				<a class="btn btn-sm btn-success" href="/admin/order/add?customer_id=<?=$customer['id'];?>">添加代订</a>
				<a class="btn btn-sm btn-primary" href="/admin/order?customer_id=<?=$customer['id'];?>">查看所有代订</a>
			</div>
		</div>
	</div>
</div>
<?php
$this->render('check_item', array(
    'hidden_comment'=>1,
));
?>
<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/customer_detail');
	});
</script>
