<div class="customer_qq_log">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/customer">客户列表</a></li>
		<li><a href="/admin/customer/detail?id=<?=$customer['id'];?>"><?=$customer['contact'];?>的资料</a></li>
		<li class="active">聊天记录</li>
	</ol>
	<div class="customer-detail">
		<!-- actions -->
		<div class="actions" style="margin-bottom:10px;">
			<a class="btn btn-default"  href="/admin/customer/detail?id=<?=$customer['id'];?>">返回用户资料</a>
			<a class="btn btn-primary edit-log-btn"  href="javascript:void(0)">修改聊天记录</a>
		</div>

		<form class="form-horizontal" role="form" id="customer_qq_log_form" style="display:none;">
			<input type="hidden" name="customer_id" value="<?=isset($customer) ? $customer['id']:'';?>">
			<textarea name="qq_log" class="form-control" rows=20><?=$customer['qq_log'];?></textarea>
			<button class="btn btn-primary" id="submit-btn"  type="submit">保存修改</button>
		</form>

<?php if(!empty($customer['qq_log'])):?>
		<pre class="log-box"><?=nl2br($customer['qq_log']);?></pre>
<?php else: ?>
		<pre class="log-box">暂无任何聊天记录</pre>
<?php endif;?>

	</div>
</div>
<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/customer_qq_log');
	});
</script>
