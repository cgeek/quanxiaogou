<div class="trade_list clearfix">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/trade">订单列表</a></li>
		<li class="active">订单</li>
	</ol>
	<div class="filter-bar clearfix">
		<form class="form-inline" role="form">
			<div class="clearfix" style="margin-bottom:10px;">
				<div class="pull-left">
					搜索:
					<div class="form-group">
						<input type="text" name="keyword" class="form-control" id="filter_key" placeholder="订单号，姓名，手机号码" value="<?=$filter_key?>"/>
						<input type="hidden" name="status" value="<?=$status?>"/>
					</div>
					<button type="submit" class="btn btn-sm btn-primary">查找</button>
				</div>
			</div>
			<div class="clearfix">
				<div class="pull-left">
					<div class="btn-group" >
						<a class="btn btn-sm btn-default <?php if(!isset($_GET['status']) && !isset($_GET['now'])):?>active <?php endif?>" href="/admin/trade">全部</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == '未付款'):?>active <?php endif?>" href="/admin/trade?status=未付款">未付款</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == '已付款'):?>active <?php endif?>" href="/admin/trade?status=已付款">已付款</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == '已确认'):?>active <?php endif?>" href="/admin/trade?status=已确认">已确认</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == '已填写信息'):?>active <?php endif?>" href="/admin/trade?status=已填写信息">已填写信息</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == '取消'):?>active <?php endif?>" href="/admin/trade?status=取消">已取消</a>
					</div>

				</div>
				<div class="pull-left" style="margin-left:10px;">
				    <div class="btn-group" >
						<a class="btn btn-sm btn-default <?php if(isset($_GET['status']) && $_GET['status'] == 'remind'):?>active <?php endif;?>" href="/admin/trade?status=remind">3天内出发客户</a>
				    </div>
				</div>
			</div>
		</form>

	</div>
	<div class="trade-list" >
		
<?php if(!empty($trade_list)): ?>
    <?php foreach($trade_list as $trade):?>
            <?php if(!empty($trade['itemList'])):?>
                <?php foreach($trade['itemList'] as $key=>$item):?>
                    <?php if($key == 0):?>
                    <div trade_id=<?=$trade['id'];?> >
                        <?php $lines = count($trade['itemList']); ?>
                        <td rowspan="<?=$lines;?>"><?=$trade['id'];?></td>
                        <td><a href="http://www.shijieyou.com/product/detail/<?=$item['itemId'];?>" target="_blank"><?=$item['title'];?></a></td>
                        <td rowspan="<?=$lines;?>"><?=$trade['peopleNum'];?></td>
                        <td rowspan="<?=$lines;?>"><?=$trade['total'];?></td>
                        <td rowspan="<?=$lines;?>">
                            <?=$trade['phoneNum'];?>
                        </td>
                        <td rowspan="<?=$lines;?>">
                            <?=date('Y-m-d H:i', strtotime($trade['dateCreated']));?>
                        </td>
                        <td rowspan="<?=$lines;?>">
			    <?php if($lines >=1):?>
				<?=date('Y-m-d', strtotime($trade['itemList'][0]['startTime']));?>
			    <?php endif;?>
                        </td>
		        <td><?=$trade['noteCount'];?></td>
                        <td rowspan="<?=$lines;?>">
                            <?=$trade['status'];?>
                        </td>
                        <td rowspan="<?=$lines;?>">
                            <a href="/admin/trade/detail/?id=<?=$trade['id'];?>" class="btn btn-default btn-xs">查看</a>
							<?php if(empty($trade['claimer']) && $trade['status'] != '未付款' && $trade['status'] !='取消'):?>
								<a href="javascript:void(0);" class="btn btn-info btn-xs claim-btn">认领</a>
							<?php elseif(!empty($trade['claimer'])):?>
								<a href="javascript:void(0);" class="btn btn-default btn-xs"><?=$trade['claimer'];?></a>
							<?php endif;?>
                        </td>
                    </div>
                    <?php else:?>
                    <div>
                        <td><?=$item['title'];?></td>
                    </div>

                    <?php endif;?>
                <?php endforeach;?>
            <?php else:?>
				<div>
                    <td><?=$trade['id'];?></td>
                    <td>

                    </td>
                    <td></td>
                    <td></td>
					<td><?=$trade['total'];?></td>
                    <td>
                        <?=date('Y-m-d H:i', strtotime($trade['dateCreated']));?>
                    </td>
		    <td><?=$trade['noteCount'];?></td>
		    <td><?=$trade['status'];?></td>
                    <td>
                        <a href="/trade/detail/?id=<?=$trade['id'];?>" class="btn btn-default btn-xs">查看</a>
                    </td>
                </div>


            <?php endif;?>
	<?php endforeach;?>
<?php else:?>
				<p>暂没有相关订单列表</p>
<?php endif;?>

        <?php if(isset($_GET['offset']) && intval($_GET['offset']) > 0):?>
            <a class="btn btn-default pull-right" href="?status=<?=$status;?>&keyword=<?=$keyword;?>&offset=<?=isset($_GET['offset']) ? intval($_GET['offset']) + 20: '20';?>">下一页</a>
            <a class="btn btn-default pull-right" href="?status=<?=$status;?>&keyword=<?=$keyword;?>&offset=<?=isset($_GET['offset']) ? intval($_GET['offset']) - 20: '20';?>" style="margin-right:10px;">上一页</a>
            <a class="btn btn-default pull-right" style="margin-right:10px;" href="/admin/trade">首页</a>
            
        <?php else:?>
            <a class="btn btn-default pull-right" href="?status=<?=$status;?>&keyword=<?=$keyword;?>&offset=<?=isset($_GET['offset']) ? intval($_GET['offset']) + 20: '20';?>">下一页</a>
        <?php endif;?>
	</div>

</div>
	<?php if(isset($sum)):?>
		<div class=" alert alert-success">符合条件的订单笔数：<?=$sum['total_num'];?>  ,  订单总收入：￥<?=intval($sum['total_fee']);?>,  订单总支出：￥<?=intval($sum['total_payment']);?>, 总差额：￥<?=round(($sum['total_fee'] - $sum['total_payment']), 2);?></div>
	<?php endif;?>
<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/trade_index');
	});
</script>
