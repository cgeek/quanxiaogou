<div class="clearfix">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/trade">订单列表</a></li>
		<li class="active">订单详情</li>
	</ol>
    <div class="trade-detail table-responsive">
        <h4>订单详情</h4>
        <table class="table table-bordered">
            <thead style="background:#eee;">
                <tr>
                    <th>订单号</th>
                    <th>拍下时间</th>
                    <th>付款时间</th>
                    <th>手机号码</th>
                    <th>总人数</th>
                    <th>总付款</th>
                    <th>订单状态</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?=$trade['id'];?></td>
                    <td>
                        <?=date('Y-m-d H:i:s', strtotime($trade['dateCreated']));?>
                    </td>
                    <td>
                    </td>
                    <td>
                        <?=$trade['phoneNum'];?>
                    </td>
                    <td>
                        <?=$trade['peopleNum'];?>
                    </td>
                    <td>
                        <?=$trade['total'];?>
                    </td>
                    <td>
                        <?=$trade['status'];?>
                    </td>
                </tr>
                <tr style="background:#eee;">
                    <th>支付宝交易号</th>
                    <th>商户订单号</th>
                    <th>订单来源</th>
                    <th></th>
                    <th></th>
                    <th></th>
                    <th></th>
                </tr>
                <tr>
                    <td>
                        <?=$trade['alipayTradeNo'];?>
                    </td>
                    <td>
                        <?=$financeId;?>
                    </td>
                    <td><?=urldecode($trade['source']);?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>
            </tbody>
        </table>

                    当前订单状态：
								<div class="btn-group" data-toggle="buttons">
									<button class="btn btn-sm btn-default" <?php if($trade['status']==1):?> btn-success<?php endif;?> status=1 >未付款</button>
									<button class="btn btn-sm btn-default" <?php if($trade['status']==2):?> btn-success<?php endif;?> status=2 >已付款，待填写</button>
									<button class="btn btn-sm btn-default" <?php if($trade['status']==3):?> btn-success<?php endif;?> status=3 >已经填写,待确认</button>
									<button class="btn btn-sm btn-default" <?php if($trade['status']==4):?> btn-success<?php endif;?> status=4 >已经确认，待出行</button>
									<button class="btn btn-sm btn-default" <?php if($trade['status']==5):?> btn-success<?php endif;?> status=5 >完成，未评价</button>
									<button class="btn btn-sm btn-default" <?php if($trade['status']==6):?> btn-success<?php endif;?> status=6 >已评价</button>
								</div>
        <?php if(!empty($trade['itemList'])):?>
<?php foreach($trade['itemList'] as $item):?>
        <h4>商品信息</h4>
        <table class="table table-bordered" style="margin-bottom:0; background:#eee;">
            <thead >
                <tr>
                    <th>商品名称</th>
                    <th>类型</th>
                    <th>人数</th>
                    <th>总价</th>
                    <th>出发时间</th>
                    <th>状态</th>
                    <th>供应商</th>
                    <th>操作</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td><?=$item['title'];?></td>
                    <td><?=!empty($item['parentContentSet']) ? $item['parentContentSet'] . "，" : '';?><?=$item['contentSet'];?></td>
                    <td>
                        <?php if(!empty($item['ageSets'])):?>
                            <?php foreach($item['ageSets'] as $i):?>
                                <?=$i['ageSet'];?>:<?=$i['count'];?>
                            <?php endforeach;?>
                        <?php endif;?>
                    </td>
                    <td><?=$item['price'];?></td>
                    <td><?=date('Y-m-d', strtotime($item['startTime']));?></td>
                    <td><?=$item['status'];?></td>
                    <td><?=$item['localSeller'];?></td>
                    <td>
                         <a class="btn btn-sm btn-success update-status-btn" status="1" href="javascript:void(0);" item_id="<?=$item['id'];?>" trade_id="<?=$trade['id'];?>">确认成功</a>
                         <a class="btn btn-sm btn-danger update-status-btn" status="2" href="javascript:void(0);" item_id="<?=$item['id'];?>" trade_id="<?=$trade['id'];?>">确认失败</a>
                         <a class="btn btn-sm btn-info update-status-btn" status="3" href="javascript:void(0);" item_id="<?=$item['id'];?>" trade_id="<?=$trade['id'];?>">退款中</a>
                         <a class="btn btn-sm btn-info update-status-btn" status="4" href="javascript:void(0);" item_id="<?=$item['id'];?>" trade_id="<?=$trade['id'];?>">已退款</a>
                         <?php if(empty($item['hotelInfo']) && empty($item['touristInfo'])):?><a class="btn btn-sm btn-default" href="javascript:void(0);">用户没填资料<a><?php else:?><a class="btn btn-sm btn-info view-item-info-btn"  href="javascript:void(0);" item_id="<?=$item['id'];?>" trade_id="<?=$trade['id'];?>">查看客户信息</a><?php endif;?>
                         <!--a class="btn btn-sm btn-default"  href="javascript:void(0);" item_id="<?=$item['id'];?>" trade_id="<?=$trade['id'];?>">上传确认单</a-->
                    </td>
                </tr>
		<tr>
            <td colspan=4 >
				备注：<?=$item['note'];?>
			</td>
		    <td colspan=3 style="text-align:right;">
			目前提醒状态：  <span class="label label-info"><?=$item['remind'];?></span>
		    </td>
		    <td>
                         <!--a class="btn btn-sm btn-default update-remind-status-btn" status="0" href="javascript:void(0);" item_id="<?=$item['id'];?>" trade_id="<?=$trade['id'];?>">设为未提醒</a-->
                         <a class="btn btn-sm btn-default update-remind-status-btn" status="1" href="javascript:void(0);" item_id="<?=$item['id'];?>" trade_id="<?=$trade['id'];?>">设为已提醒</a>
                         <a class="btn btn-sm btn-default update-remind-status-btn" status="2" href="javascript:void(0);" item_id="<?=$item['id'];?>" trade_id="<?=$trade['id'];?>">设为不提醒</a>
		    </td>
		</tr>
		<tr>
			<td colspan=7>
				确认单
				<a href="<?=$item['confirmation'];?>" target="_blank"><?=$item['confirmation'];?></a>
			</td>
			<td>
				<!-- The fileinput-button span is used to style the file input field as button -->
				<span class="btn btn-xs btn-success fileinput-button">
					 <i class="glyphicon glyphicon-circle-arrow-up"></i>
					 <span>上传确认单</span>
					 <input type="hidden" name="lineItemId" value="<?=$item['id'];?>">
					 <!-- The file input field used as target for the file upload widget -->
					<input id="fileupload" class="fileupload" type="file" name="file-to-upload" multipart=false/>
				</span>

				<a href="/admin/voucher/edit?tradeId=<?=$trade['id'];?>&itemId=<?=$item['id'];?>" class="btn btn-xs btn-info">创建确认单</a>
			</td>
		</tr>
            </tbody>
        </table>

        <div id="info-<?=$item['id'];?>" style="display:none;">
        <!--table class="table table-bordered table-striped" style="margin-bottom:0;">
            <thead style="background:#d9edf7;">
                <tr>
                    <th colspan="6">联系人信息</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>联系人：</td>
                    <td><a href="javascript:void(0);" class="editable-input info-name" data-name="username" data-type="text" data-pk="1" data-title="输入要修改的姓名" class="editable editable-click" style="display: inline;" trade_id="" item_id="">缪华武</a></td>
                    <td>拼音：</td>
                    <td><a href="javascript:void(0);" class="editable-input info-pinyin" data-type="text" data-pk="1" name="pinyin" data-title="输入拼音" class="editable editable-click" style="display: inline;">Miao Huawu</a></td>
                    <td>邮箱：</td>
                    <td><a href="javascript:void(0);" class="editable-input info-email" data-type="text" data-pk="1" data-title="输入邮箱" class="editable editable-click" style="display: inline;">cgeek@qq.com</a></td>
                </tr>
                <tr>
                    <td>手机号码：</td>
                    <td><a href="javascript:void(0);" class="editable-input info-mobile" data-type="text" data-pk="1" data-title="输入手机号码" class="editable editable-click" style="display: inline;">18626880110</a></td>
                    <td>境外手机号码：</td>
                    <td><a href="javascript:void(0);" class="editable-input info-mobiel2" data-type="text" data-pk="1" data-title="输入境外手机号码" class="editable editable-click" style="display: inline;">+60 1928312313</a></td>
                    <td> </td>
                    <td></td>
                </tr>
            </tbody>
        </table-->
        <?php if(!empty($item['hotelInfo'])):?>
            <?php $hotel = $item['hotelInfo'];?>
        <table class="table table-bordered table-striped" style="margin-bottom:0;">
            <thead style="background:#d9edf7;">
                <tr>
                    <th colspan="6">酒店信息</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>酒店名字：</td>
                    <td><a href="javascript:void(0);" class="editable-input" data-name="hotelName" data-type="text" data-pk="<?=$hotel['id'];?>" info-type="hotelInfo" data-title="修改酒店名称" class="editable editable-click" style="display: inline;"><?=$hotel['hotelName'];?></a></td>
                </tr>
                <tr>
                    <td>酒店地址：</td>
                    <td><a href="javascript:void(0);" class="editable-input" data-type="textarea" data-name="hotelAddress" data-pk="<?=$hotel['id'];?>" info-type="hotelInfo"  data-title="修改酒店地址" class="editable editable-click" style="display: inline;"><?=$hotel['hotelAddress'];?></a></td>
                </tr>
                <tr>
                    <td>酒店电话：</td>
                    <td><a href="javascript:void(0);" class="editable-input" data-type="text" data-name="hotelPhone" data-pk="<?=$hotel['id'];?>" info-type="hotelInfo" data-title="修改酒店电话" class="editable editable-click" style="display: inline;"><?=$hotel['hotelPhone'];?></a></td>
                </tr>
            </tbody>
        </table>
        <?php endif;?>

        <?php if(!empty($item['touristInfo'])):?>
        <table class="table table-bordered table-striped" style="margin-bottom:0;">
            <thead>
                <tr style="background:#d9edf7;">
                    <th colspan="9">旅客信息</th>
                </tr>
                <tr>
                    <th>姓名</th>
                    <th>英文姓</th>
                    <th>英文名</th>
                    <th>性别</th>
                    <th>护照号</th>
                    <th>生日</th>
                    <th>邮箱</th>
                    <th>手机号码</th>
                    <th>境外手机</th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($item['touristInfo'] as $tourist):?>
                <tr>
                    <td><a href="javascript:void(0);" class="editable-input" data-type="text" data-name="touristName" data-pk="<?=$tourist['id'];?>" info-type="touristInfo"  data-title="修改姓名" class="editable editable-click" style="display: inline;"><?=$tourist['touristName'];?></a></td>
                    <td><a href="javascript:void(0);" class="editable-input" data-type="text" data-name="touristLastnameInPassport" data-pk="<?=$tourist['id'];?>" info-type="touristInfo"  data-title="修改拼音" class="editable editable-click" style="display: inline;"><?=$tourist['touristLastnameInPassport'];?></a></td>
                    <td><a href="javascript:void(0);" class="editable-input" data-type="text" data-name="touristFirstnameInPassport" data-pk="<?=$tourist['id'];?>" info-type="touristInfo"  data-title="修改拼音" class="editable editable-click" style="display: inline;"><?=$tourist['touristFirstnameInPassport'];?></a></td>
                    <td><a href="javascript:void(0);" class="editable-input" data-type="text" data-name="touristGender" data-pk="<?=$tourist['id'];?>" info-type="touristInfo"  data-title="修改性别" class="editable editable-click" style="display: inline;"><?=$tourist['touristGender']['name'];?></a></td>
                    <td><a href="javascript:void(0);" class="editable-input" data-type="text" data-name="touristPassportNo" data-pk="<?=$tourist['id'];?>" info-type="touristInfo"  data-title="修改护照号" class="editable editable-click" style="display: inline;"><?=$tourist['touristPassportNo'];?></a></td>
                    <td><a href="javascript:void(0);" class="editable-input" data-type="text" data-name="touristBirthday" data-pk="<?=$tourist['id'];?>" info-type="touristInfo"  data-title="生日" class="editable editable-click" style="display: inline;"><?=date('Y-m-d', strtotime($tourist['touristBirthday']));?></a></td>
                    <td><a href="javascript:void(0);" class="editable-input" data-type="text" data-name="touristEmail" data-pk="<?=$tourist['id'];?>" info-type="touristInfo"  data-title="修改邮箱" class="editable editable-click" style="display: inline;"><?=$tourist['touristEmail'];?></a></td>
                    <td><a href="javascript:void(0);" class="editable-input" data-type="text" data-name="touristMobile" data-pk="<?=$tourist['id'];?>" info-type="touristInfo"  data-title="修改手机" class="editable editable-click" style="display: inline;"><?=$tourist['touristMobile'];?></a></td>
                    <td><a href="javascript:void(0);" class="editable-input" data-type="text" data-name="touristMobileLocal" data-pk="<?=$tourist['id'];?>" info-type="touristInfo"  data-title="修改境外手机号" class="editable editable-click" style="display: inline;"><?=$tourist['touristMobileLocal'];?></a></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php endif;?>

    </div>
<?php endforeach;?>
        <?php endif;?>
    </div>

</div>
    <div class="col-lg-6 col-sm-6 col-md-6 pull-left" style="background:#eee; border-radius:5px; margin-top:10px;">
        <h4>客户动态</h4>
        <form class="clearfix" method="POST" id="comment-form">
            <input type="hidden" name="tradeId" value="<?=$trade['id'];?>">
            <input type="hidden" name="username" value="<?=isset($admin) ? $admin['username'] : '';?>">
            <textarea class="form-control" rows="3" name="content" placeholder="可以记录任何关于这个客户的情况"></textarea>
            <button class="btn btn-sm btn-primary pull-right" id="comment-submit-btn" style="margin-top:10px;">提交</button>
        </form>
        <ul class="comment-list list-unstyled">
<?php if(!empty($trade['noteList'])):?>
     <?php foreach($trade['noteList'] as $note):?>
            <li note_id="<?=$note['id'];?>">
                <span style="color:#333;"><?=$note['content'];?></span>
                <p class="clearfix">
                    <span class="pull-left"><?=$note['username'];?></span>
                    <span class="pull-right"><?=date('Y-m-d H:i:s', strtotime($note['lastUpdated']));?></span> 
                </p>
            </li>
    <?php endforeach;?>
<?php endif;?>
        </ul>
    </div>
<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/trade');
	});
</script>
