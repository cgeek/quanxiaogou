<div class="map-list-page">
	<div class="map-list container">
		<!-- 面包屑 -->
		<ol class="breadcrumb">
			<li><a href="/admin">管理首页</a></li>
			<li><a href="/admin/map">离线地图列表</a></li>
		</ol>
		<form class="form-inline" method="get" action="/admin/map/">
			<div class="form-group">
				<label class="sr-only" for="keyword">关键词搜索</label>
				<input type="text" class="form-control col-xs-6 col-md-6" id="keyword" placeholder="关键词:城市名称，拼音，英文,国家" name="keyword" value="<?=isset($_GET['keyword']) ? $_GET['keyword'] : ''?>">
			</div>
			<button type="submit" class="btn btn-primary">搜索</button>
		</form>
<?php if(!empty($map_list)):?>
		<table class="table">
			<thead>
				<tr>
					<th>#ID</th>
					<th>标题</th>
					<th>下载地址</th>
					<th>大小</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach($map_list as $map):?>
				<tr>
					<td><?=$map['id'];?></td>
					<td><?=$map['title'];?></td>
					<td><?=$map['url'];?></td>
					<td><?=intval($map['size'] / 1024 / 1024);?>M</td>
					<td>
						<a href="/admin/offlineMap/edit?id=<?=$map['id'];?>" class="btn btn-default btn-xs">编辑</a>
					<?php if($map['status'] == 1): ?>
						<a href="/admin/offlineMap/edit?id=<?=$map['id'];?>" class="btn btn-default btn-xs">下线</a>
					<?php elseif($map['status'] == 0):?>
						<a href="/admin/map/edit?id=<?=$map['id'];?>" class="btn btn-default btn-xs">上线</a>
					<?php endif;?>
					</td>
				</tr>
	<?php endforeach;?>
			</tbody>
		</table>
		<?php
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
<?php endif;?>
	</div>
</div>
