<div class="map-edit-page">
	<div class="map-detail container">
		<!-- 面包屑 -->
		<ol class="breadcrumb">
			<li><a href="/admin">管理首页</a></li>
			<li><a href="/admin/offlineMap">离线地图列表</a></li>
			<li>编辑离线信息</li>
		</ol>
		<form class="form-horizontal" role="form" id="map_edit_form" method="post">
			<input type="hidden" name="id" value="<?=isset($map) ? $map['id']:'';?>">
				<div class="form-group">
					<label for="inputTitle" class="col-lg-2 control-label">地图标题</label>
					<div class="col-lg-4">
						<input type="text" name="title" class="form-control" id="inputTitle" placeholder="" value="<?=isset($map)? $map['title'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputCountryID" class="col-lg-2 control-label">选择国家</label>
					<div class="col-lg-2">
						<input type="text" name="country_id" class="form-control" id="inputCountryID" placeholder="" value="<?=isset($map)? $map['country_id'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputCityID" class="col-lg-2 control-label">关联城市</label>
					<div class="col-lg-2">
						<input type="text" data-provide="typeahead" name="city_id" class="form-control typeahead" id="inputCityID" placeholder="" value="<?=isset($map) ? $map['city_id'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputUrl" class="col-lg-2 control-label">下载地址</label>
					<div class="col-lg-6">
						<input type="text" name="url" class="form-control" id="inputUrl" placeholder="" value="<?=isset($map)? $map['url'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputSize" class="col-lg-2 control-label">文件大小</label>
					<div class="col-lg-2">
						<input type="text" name="size" class="form-control" id="inputSize" placeholder="" value="<?=isset($map)? $map['size'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">保存</button>
<?php if(!empty($map)):?>
						<a href="javascript:void(0);" id="delete-customer-btn" class="btn btn-danger">删除地图</a>
<?php endif;?>
						<span class="help-block"></span>
					</div>
				</div>
		</form>
	</div>
</div>

<script type="text/template" id="multiuploadpic-tpl">
	<div class="multiuploadPic-pop">
		<div class="multiuploadpicwrap">
			<a href="javascript:void(0);" class="close">×</a>
			<div class="hd">上传图片</div>
			<div class="bd">
				<div class="multiupload-wrap">
					<div class="init-status">
						<div class="swfupload-btn">
							<div id="uploadBtn"></div>
							<a href="javascript:void(0);" class="uploadbtn" target="_self">添加的照片</a>
						</div>
					</div>
					<div class="upload-status emptypiclist">
						<dl>
							<dt>
								<span class="listtip"></span>
								<a href="#" class="clearlist">清空列表</a>
							</dt>
							<dd class="wait-upload">
								<ul class="piclist" id="fileQueue"></ul>
							</dd>
							<dd class="upload-tool">
								<span class="tntip"></span>
							</dd>
						</dl>
						<div class="complete-list">
							<div class="picedit">
								<ul class="piclist"></ul>
							</div>
							<div class="btmbtn">
								<a href="#" class="btn btn-primary" id="saveUploadBtn">保存图片</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</script>
<script type="text/template" id="cover-image-select-tpl">
<div class="coverpicwrap-pop">
	<div class="coverpicwrap">
		<a href="javascript:void(0);" class="close">×</a>
		<div class="hd">选择封面</div>
		<div class="bd">
			<ul>
			{{#image_list}}
				<li class="note-pic" image_hash={{image_hash}}>
					<div class="image">
						<img class="noteImage" src="{{image_url_small}}" style="width:120px;; height:auto">
					</div>
					<div class="selected "></div>
				</li>
			{{/image_list}}
			</ul>
			<div class="saveBtnWrap"><a id="set-cover-image-btn" class="btn btn-warning saveBtn" href="javascript:void(0);">设为封面</a></div>
		</div>
	</div>
</div>
</script>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/offlnemap_edit');
	});
</script>

