<div class="city-list-page">
	<div class="city-list container">
		<!-- 面包屑 -->
		<ol class="breadcrumb">
			<li><a href="/admin">管理首页</a></li>
			<li><a href="/admin/city">目的地列表</a></li>
		</ol>
		<form class="form-inline" method="get" action="/admin/city/">
			<div class="form-group">
				<label class="sr-only" for="keyword">关键词搜索</label>
				<input type="text" class="form-control col-xs-6 col-md-6" id="keyword" placeholder="关键词:城市名称，拼音，英文,国家" name="keyword" value="<?=isset($_GET['keyword']) ? $_GET['keyword'] : ''?>">
			</div>
			<button type="submit" class="btn btn-primary">搜索</button>
		</form>
<?php if(!empty($city_list)):?>
		<table class="table">
			<thead>
				<tr>
					<th>城市id</th>
					<th>封面图片</th>
					<th>名称</th>
					<th>英文名称</th>
					<th>国家</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach($city_list as $city):?>
				<tr>
					<td><?=$city['id'];?></td>
					<td><img width="80" src="<?=upimage($city['cover_image']);?>"></td>
					<td><?=$city['name'];?></td>
					<td><?=$city['name_english'];?></td>
					<td><?=$city['country_name'];?></td>
					<td><a href="/admin/city/edit?id=<?=$city['id'];?>" class="btn btn-default btn-xs">编辑</a></td>
				</tr>
	<?php endforeach;?>
			</tbody>
		</table>
		<?php
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
<?php endif;?>
	</div>
</div>
