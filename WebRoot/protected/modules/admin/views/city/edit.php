<div class="city-edit-page">
	<div class="city-detail container">
		<!-- 面包屑 -->
		<ol class="breadcrumb">
			<li><a href="/admin">管理首页</a></li>
			<li><a href="/admin/city">目的地列表</a></li>
			<li>编辑城市</li>
		</ol>
		<form class="form-horizontal" role="form" id="city_edit_form" method="post">
			<input type="hidden" name="city_id" value="<?=isset($city) ? $city['id']:'';?>">
				<div class="form-group">
					<label for="inputTitle" class="col-lg-2 control-label">城市名称</label>
					<div class="col-lg-2">
						<input type="text" name="name" class="form-control" id="inputTitle" placeholder="" value="<?=isset($city)? $city['name'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputSubtitle" class="col-lg-2 control-label">英文名</label>
					<div class="col-lg-2">
						<input type="text" name="name_english" class="form-control" id="inputSubtitle" placeholder="" value="<?=isset($city)? $city['name_english'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPinYin" class="col-lg-2 control-label">拼音</label>
					<div class="col-lg-2">
						<input type="text" name="name_pinyin" class="form-control" id="inputPinYin" placeholder="" value="<?=isset($city)? $city['name_pinyin'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputParentId" class="col-lg-2 control-label">父级ID</label>
					<div class="col-lg-2">
						<input type="text" name="parent_id" class="form-control" id="inputParentId" placeholder="" value="<?=isset($city)? $city['parent_id'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputCountryID" class="col-lg-2 control-label">国家ID</label>
					<div class="col-lg-2">
						<input type="text" name="country_id" class="form-control" id="inputCountryID" placeholder="" value="<?=isset($city)? $city['country_id'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputCountryName" class="col-lg-2 control-label">国家</label>
					<div class="col-lg-2">
						<input type="text" name="country_name" class="form-control" id="inputCountryName" placeholder="" value="<?=isset($city)? $city['country_name'] : '';?>">
						<span class="helpblock"></span>
					</div>
				</div>
<?php if(!empty($city)):?>
				<div class="form-group">
					<label for="inputCoverImage" class="col-lg-2 control-label">设置封面图片</label>
					<div class="col-lg-8">
						<input type="hidden" name="cover_image" value="<?=isset($city)? $city['cover_image'] : '';?>">
						<div class="cover-image-box">
						<?php if(isset($city) && !empty($city['cover_image'])) : ?>
							<img src="<?=upimage($city['cover_image']);?>">
						<?php else:?>
							<img src="http://img03.taobaocdn.com/tps/i3/T1MNu0XdXnXXXXXXXX-210-210.png">
						<?php endif;?>
							<div class="loading"></div>
						</div>

						<!-- The fileinput-button span is used to style the file input field as button -->
						<span class="btn btn-success fileinput-button">
							 <i class="glyphicon glyphicon-plus"></i>
							 <span>选择图片</span>
							 <!-- The file input field used as target for the file upload widget -->
							<input id="fileupload" type="file" name="Filedata" multipart=false>
					    </span>
					</div>
				</div>

<?php endif;?>
				<div class="form-group">
					<label for="inputDesc" class="col-lg-2 control-label">简介（亮点）</label>
					<div class="col-lg-6">
						<textarea name="desc" placeholder="简介" rows=5 class="form-control" ><?=isset($city)? $city['desc'] : '';?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inputDesc" class="col-lg-2 control-label"></label>
					<div class="col-lg-4">
						<div class="checkbox">
					    	<label>
							<input type="checkbox" name="status" value="1" <?=(isset($city) && $city['status'] == 1) ? 'checked' : '';?>> 开通目的地
							</label>
						</div>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">保存</button>
<?php if(!empty($city)):?>
						<a href="javascript:void(0);" id="delete-customer-btn" class="btn btn-danger">删除城市</a>
<?php endif;?>
						<span class="help-block"></span>
					</div>
				</div>
		</form>
	</div>
</div>

<script type="text/template" id="multiuploadpic-tpl">
	<div class="multiuploadPic-pop">
		<div class="multiuploadpicwrap">
			<a href="javascript:void(0);" class="close">×</a>
			<div class="hd">上传图片</div>
			<div class="bd">
				<div class="multiupload-wrap">
					<div class="init-status">
						<div class="swfupload-btn">
							<div id="uploadBtn"></div>
							<a href="javascript:void(0);" class="uploadbtn" target="_self">添加的照片</a>
						</div>
					</div>
					<div class="upload-status emptypiclist">
						<dl>
							<dt>
								<span class="listtip"></span>
								<a href="#" class="clearlist">清空列表</a>
							</dt>
							<dd class="wait-upload">
								<ul class="piclist" id="fileQueue"></ul>
							</dd>
							<dd class="upload-tool">
								<span class="tntip"></span>
							</dd>
						</dl>
						<div class="complete-list">
							<div class="picedit">
								<ul class="piclist"></ul>
							</div>
							<div class="btmbtn">
								<a href="#" class="btn btn-primary" id="saveUploadBtn">保存图片</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</script>
<script type="text/template" id="cover-image-select-tpl">
<div class="coverpicwrap-pop">
	<div class="coverpicwrap">
		<a href="javascript:void(0);" class="close">×</a>
		<div class="hd">选择封面</div>
		<div class="bd">
			<ul>
			{{#image_list}}
				<li class="note-pic" image_hash={{image_hash}}>
					<div class="image">
						<img class="noteImage" src="{{image_url_small}}" style="width:120px;; height:auto">
					</div>
					<div class="selected "></div>
				</li>
			{{/image_list}}
			</ul>
			<div class="saveBtnWrap"><a id="set-cover-image-btn" class="btn btn-warning saveBtn" href="javascript:void(0);">设为封面</a></div>
		</div>
	</div>
</div>
</script>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/city_edit');
	});
</script>

