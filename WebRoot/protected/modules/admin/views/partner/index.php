<div class="customer_list clearfix">
    <ol class="breadcrumb">
        <li><a href="/admin">管理首页</a></li>
        <li class="active">商户列表</li>

        <a href="/admin/partner/ctrip" class="btn btn-xs btn-info pull-right">携程定制师</a>
    </ol>
    <div class="filter-bar clearfix">
        <form class="form-inline" role="form">

            <div class="pull-left">
                <div class="form-group">
                    <input type="text" name="filter_key" class="form-control" id="filter_key" placeholder="搜索" value="<?=$filter_key?>"/>
                    <input type="hidden" name="status" value="<?=$status?>"/>
                </div>
                <button type="submit" class="btn btn-sm btn-primary">查找</button>
            </div>

            <a href="/admin/partner/add" class="btn btn-success btn-sm pull-right">添加商户</a>
            <!--div class="btn-group">
                <button type="button" class="btn btn-primary">我的客户</button>
                <button type="button" class="btn btn-primary">所有客户</button>
                <button type="button" class="btn btn-primary">已经完成客户</button>
            </div-->
        </form>
    </div>
    <div class="table-responsive clearfix" >
        <table class="table table-bordered customer-list-table ">
            <thead>
            <tr style="background:#eee;">
                <th>#ID</th>
                <th>用户名</th>
                <th>昵称</th>
                <th>最后登录时间</th>
                <th>创建时间</th>
                <th>状态</th>
                <th>操作</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($partner_list as $key=>$partner):?>
                <tr>
                    <td>
                        <?=$partner['id'];?>
                    </td>
                    <td>
                        <?=$partner['username'];?>
                    </td>
                    <td>
                        <?=$partner['nick_name'];?>
                    </td>
                    <td><?php if(!empty($partner['last_login_time'])):?><?=date('Y-m-d H:i', strtotime($partner['last_login_time']));?><?php endif;?></td>
                    <td><?=date('Y-m-d H:i', strtotime($partner['ctime']));?></td>
                    <td>
                        <?php if($partner['status'] == 0):;?>
                            正常
                        <?php endif;?>
                    </td>
                    <td>
                        <a class="btn btn-xs btn-default" href="/admin/partner/edit?id=<?=$partner['id'];?>">编辑</a>
                        <a class="btn btn-xs btn-default" href="/admin/journey?user_id=<?=$partner['id'];?>">查看行程</a>
                        <!--
                        <?php if($partner['status'] < 0):;?>
                        <a class="btn btn-xs btn-info" href="/admin/partner/edit?id=<?=$partner['id'];?>">启用</a>
                        <?php else:?>
                        <a class="btn btn-xs btn-danger" href="/admin/partner/edit?id=<?=$partner['id'];?>">禁用</a>
                        <?php endif;?>
                        -->
                    </td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php
        $this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false));
        ?>

    </div>
    <div class=" alert alert-success">符合条件的客户数：<?=$count;?>个</div>
</div>
<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('admin/customer_index');
    });
</script>
