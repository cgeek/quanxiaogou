<div class="customer_list clearfix">
    <ol class="breadcrumb">
        <li><a href="/admin">管理首页</a></li>
        <li class="active">携程定制师列表</li>

        <a href="/admin/partner" class="btn btn-xs btn-info pull-right">商户列表</a>
    </ol>
    <div class="filter-bar clearfix">
        <form class="form-inline" role="form">

            <div class="pull-left">
                <div class="form-group">
                    <input type="text" name="filter_key" class="form-control" id="filter_key" placeholder="搜索，昵称，服务区域" value="<?=$filter_key?>"/>
                    <input type="hidden" name="status" value="<?=$status?>"/>
                </div>
                <button type="submit" class="btn btn-sm btn-primary">查找</button>
            </div>

        </form>
    </div>
    <div class="table-responsive clearfix" >
        <table class="table table-bordered customer-list-table ">
            <thead>
            <tr style="background:#eee;">
                <th>#ID</th>
                <th>昵称</th>
                <th>手机</th>
                <th>微信</th>

                <th>服务区域</th>
                <th>一句话</th>

                <th>公司</th>
                <th>注册时间</th>
                <th>国内</th>
            </tr>
            </thead>
            <tbody>
            <?php foreach($advisor_list as $key=>$advisor):?>
                <tr>
                    <td>
                        <a target="_blank" href="http://m.ctrip.com/webapp/tours/csplatform/detail?advisorId=<?=$advisor['ctrip_id'];?>&role=2"><?=$advisor['id'];?></a>
                    </td>
                    <td>
                        <a target="_blank" href="http://m.ctrip.com/webapp/tours/csplatform/detail?advisorId=<?=$advisor['ctrip_id'];?>&role=2"><?=$advisor['nickName'];?></a>
                    </td>
                    <td><?=$advisor['mobilePhone'];?></td>
                    <td><?=$advisor['weixin'];?></td>

                    <td><?=$advisor['districtList'];?></td>
                    <td><?=$advisor['sign'];?></td>

                    <td><?=$advisor['brandName'];?></td>
                    <td><?=$advisor['registeTime'];?></td>
                    <td><?=$advisor['isCitizensInChina'];?></td>
                </tr>
            <?php endforeach;?>
            </tbody>
        </table>
        <?php
        $this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false));
        ?>

    </div>
    <div class=" alert alert-success">符合条件的客户数：<?=$count;?>个</div>
</div>
<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('admin/customer_index');
    });
</script>
