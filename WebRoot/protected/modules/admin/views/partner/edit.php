<div class="partner_list">
    <ol class="breadcrumb">
        <li><a href="/admin">管理首页</a></li>
        <li><a href="/admin/partner">商户列表</a></li>
        <li class="active">编辑</li>
    </ol>
    <div class="partner-detail container">
        <form class="form-horizontal" role="form" id="partner_edit_form">
            <input type="hidden" name="id" value="<?=isset($partner) ? $partner['id']:'';?>">
            <div class="form-group">
                <label for="inputContact" class="col-lg-2 control-label">用户名</label>
                <div class="col-lg-3">
                    <input type="text" name="username" class="form-control" id="inputContact" placeholder="" value="<?=isset($partner)? $partner['username'] : '';?>">
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="form-group">
                <label for="inputContact" class="col-lg-2 control-label">昵称</label>
                <div class="col-lg-3">
                    <input type="text" name="nick_name" class="form-control" id="inputContact" placeholder="" value="<?=isset($partner)? $partner['nick_name'] : '';?>">
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="form-group">
                <label for="inputContact" class="col-lg-2 control-label">团队（公司）名</label>
                <div class="col-lg-3">
                    <input type="text" name="company" class="form-control" id="inputContact" placeholder="" value="<?=isset($partner)? $partner['company'] : '';?>">
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="form-group">
                <label for="inputRemark" class="col-lg-2 control-label">团队（公司）介绍</label>
                <div class="col-lg-5">
                    <textarea class="form-control" placeholder="" name="company_intro" id="inputRemark" rows="5"><?=isset($partner) ? $partner['company_intro'] :'';?></textarea>
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="form-group">
                <label for="inputRemark" class="col-lg-2 control-label">运营备注</label>
                <div class="col-lg-5">
                    <textarea class="form-control" placeholder="" name="remark" id="inputRemark" rows="5"><?=isset($partner) ? $partner['remark'] :'';?></textarea>
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="form-group">
                <label for="inputContact" class="col-lg-2 control-label">密码</label>
                <div class="col-lg-3">
                    <input type="text" name="password" class="form-control" id="inputContact" placeholder="不修改请留空" value="">
                    <span class="help-inline"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-lg-offset-2 col-lg-10">
                    <button type="submit" id="submit-btn" class="btn btn-primary">保存修改</button>

                    <a href="javascript:history.go(-1)" class="btn btn-default">返回客户信息</a>

                    <span class="help-block"><span>
                </div>
            </div>
    </div>
    </form>
</div>
</div>

<script type="text/template" id="taobao_tredes_tpl">
    <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
        <h4 class="modal-title" id="myModalLabel">淘宝交易列表</h4>
    </div>
    <div class="modal-body">
        <table class="table">
            <thead>
            <tr>
                <th>支付时间</th>
                <th>支付金额</th>
                <th>旺旺</th>
                <th>状态</th>
                <th>关联</th>
            </tr>
            </thead>
            <tbody>
            {{#service_fee}}
            <tr class="taobao_trade_item" buyer_nick={{buyer_nick}} total_fee={{total_fee}} trade_id={{tid}}>
                <td>{{pay_time}}</td>
                <td>{{total_fee}}</td>
                <td>{{buyer_nick}}</td>
                <td>{{status}}</td>
                <td><a href="javascript:void(0);" class="btn btn-xs btn-primary">选择</a></td>
            </tr>
            {{/service_fee}}
            </tbody>
        </table>
        <p class="alert alert-warning">如果交易列表为空或者不正确，请点击<a href="/admin/oauth/taobao" target="_blank">淘宝API授权</a>(每24小时会失效)</p>
    </div>
    <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">关闭</button>
    </div>
</script>

<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('admin/partner_edit');
    });
</script>
