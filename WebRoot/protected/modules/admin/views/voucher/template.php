<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<title>确认单</title>
<script src="https://cdn.bootcss.com/jquery/3.0.0-alpha1/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="http://cdn.shijieyou.cn/js/bootstrap-editable.min.js"></script>
<link rel="stylesheet" href="http://cdn.shijieyou.cn/css/bootstrap/bootstrap-editable.css"/>
<link href="https://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container" style="width:800px; margin-top:10px;<?php if(isset($_GET['isEdit'])):?><?php endif;?>">
	<table class="table table-bordered">
		<tbody>
			<tr>
				<td colSpan=3 style="height:40px; line-height:40px; font-size:20px; background:#1EB1ED; color:#fff;text-align:center;">求攻略确认单</td>
			</tr>
			<tr>
				<td colSpan=3 style="text-align:center;">求攻略- 轻松出境自由行</td>
			</tr>
			<tr>
				<td colSpan=2>客户信息</td>
				<td>商家联系信息</td>
			</tr>
			<tr>
				<td style="width:100px;">订单号</td>
				<td><?=isset($order['trade_id']) ? $order['trade_id'] : '';?></td>
				<td rowSpan=4>
					<?=isset($params['contact_info'])? $params['contact_info'] : '';?>
				</td>
			</tr>
			<tr>
				<td>姓名</td>
				<td>
					<span  class="editable-input editable editable-click" data-type="text" data-name="touristName" data-pk="4934931" info-type="touristInfo" data-title="修改姓名" >
						<?=isset($order['contact_name']) ? $order['contact_name'] : '';?>
					</span>
				</td>
			</tr>
			<tr>
				<td>电话</td>
				<td><?=isset($order['mobile']) ? $order['mobile'] : '';?></td>
			</tr>
			<tr>
				<td>人数</td>
				<td><?=isset($order['num']) ? $order['num'] : '';?></td>
			</tr>
			<tr>
				<td colSpan=3 style=" background:#1EB1ED; color:#fff;text-align:center;">
					订购项目
				</td>
			</tr>
			<tr>
				<td>项目名称</td>
				<td colSpan=2><?=isset($params['title']) ? $params['title']: '';?></td>
			</tr>
			<tr>
				<td>服务日期</td>
				<td colSpan=2><?=isset($order['start_date']) ? $order['start_date'] : '';?></td>
			</tr>
			<tr>
				<td>集合地点</td>
				<td colSpan=2><?=isset($params['assembling_place']) ? $params['assembling_place'] : '';?></td>
			</tr>
			<tr>
				<td>酒店信息</td>
				<td colSpan=2>
					<?=isset($order['hotel_name']) ? $order['hotel_name'] : '';?>
				</br>
					<?=isset($order['hotel_address']) ? $order['hotel_address'] : '';?>
				</td>
			</tr>
			<tr>
				<td>费用说明</td>
				<td colSpan=2><?=isset($params['fee_desc']) ? nl2br($params['fee_desc']) : '';?></td>
			</tr>
			<tr>
				<td>备注</td>
				<td colSpan=2>
					<span class="editable-input editable editable-pre-wrapped editable-click editable-empty" data-type="textarea" data-name="hotelAddress" data-pk="4935932" info-type="hotelInfo" data-title="修改酒店地址" style="display: inline;" data-original-title="" title=""><?=isset($params['remark']) ? nl2br($params['remark']) : '';?>
					</span>
				</td>
					
			</tr>
			<tr>
				<td colSpan=3 style="text-align:center;">
					客服热线：4007280727</br>
微信：qiugongluevip</br>
求攻略-轻松出境自由行 求攻略-轻松出境自由行</br>
				</td>
			</tr>
			
		</tbody>
	</table>

</div>

<!-- Modal -->
<div class="modal fade" id="myModal"  role="dialog" aria-labelledby="myModalLabel" >
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
				<h4 class="modal-title" id="myModalLabel">修改模板</h4>
			</div>
			<div class="modal-body">
	<form>
		<div class="form-group">
			<input type="hidden" name="tradeId" class="form-control" placeholder="订单ID" value="<?=isset($_GET['tradeId'])? $_GET['tradeId'] : '';?>">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">标题</label>
			<input name="title" class="form-control" placeholder="title" value="<?=isset($_GET['title'])? $_GET['title'] : '';?>">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">集合地带</label>
			<input name="title" class="form-control" placeholder="title" value="<?=isset($_GET['title'])? $_GET['title'] : '';?>">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">联系信息</label>
			<textarea class="form-control"><?=isset($_GET['title'])? $_GET['title'] : '';?></textarea>
		</div>
		<button type="submit" class="btn btn-default">保存</button>
	</form>	

			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save changes</button>
			</div>
		</div>
	</div>
</div>


<?php if(isset($_GET['isEdit'])):?>
<div class="editBox" style="top:20px; position:fixed; right:20px; width:200px;">
	<form>
		<input type="hidden" name="isEdit" value="1">
		<div class="form-group">
			<label for="exampleInputEmail1">订单ID</label>
			<input name="tradeId" class="form-control" placeholder="订单ID" value="<?=isset($_GET['tradeId'])? $_GET['tradeId'] : '';?>">
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">模板选择
				<a href="/admin/voucher/templateList" style="font-size:12px; margin-left:10px; " target="_blank" >修改</a>
				<a href="/admin/voucher/templateList" style="font-size:12px; margin-left:10px; " target="_blank"> | 新增</a> 
			</label>
<?php if(!empty($template_list)):?>
			<select class="form-control" name="templateId">
				<?php foreach($template_list as $template):?>
				<option <?php if(isset($_GET['templateId']) && $_GET['templateId'] == $template['id']):?>selected<?php endif;?>  value="<?=$template['id'];?>"><?=$template['title'];?></option>
				<?php endforeach;?>
			</select>
<?php endif; ?>
		</div>
		<button id="createBtn" type="submit" class="btn btn-default">生成确认单</button>
		<a class="btn btn-success" target="_blank" href="/plan/export/makepdf?url[]=http://erp.shijieyou.com/admin/voucher/template?id=1">导出PDF</a>
	</form>	

	<div class="" style="margin-top:30px; border-top:1px solid #ccc; padding-top:10px;">

	</div>
</div>


<script>
$(document).ready(function(){
		$('.editable-input').editable({
			url: '/admin/updateInfo',
				params: function(params) {
					params.type = $(this).attr('info-type');
					return params;
				},
				title: '请输入'
		});
});

</script>



<?php endif;?>

<body>
</html>
