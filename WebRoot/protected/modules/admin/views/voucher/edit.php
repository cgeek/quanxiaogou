<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<title>确认单</title>
<script src="https://cdn.bootcss.com/jquery/3.0.0-alpha1/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<script src="http://cdn.shijieyou.cn/js/bootstrap-editable.min.js"></script>
<link rel="stylesheet" href="http://cdn.shijieyou.cn/css/bootstrap/bootstrap-editable.css"/>
<link href="https://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
</head>
<body>

<div class="container" style="width:800px; margin-top:20px;">
	<table class="table table-bordered">
		<tbody>
			<tr>
				<td colSpan=3 style="height:60px; line-height:60px; font-size:25px; background:#1EB1ED; color:#fff;text-align:center; position:relative;"> 求攻略确认单
					<img src="http://www.qiugonglue.com/assets/images/footer-logo.png" style="position:absolute; right:10px; top:5px;">
				</td>
			</tr>
			<!--tr>
				<td colSpan=3 style="text-align:center;">求攻略- 轻松出境自由行</td>
			</tr-->
			<tr>
				<td colSpan=2 style="text-align:center;font-size:15px; font-weight:bold;">客户信息 <?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '사용자 정보' : '';?></td>
				<td style="width:50%;text-align:center;font-size:15px; font-weight:bold;">地接联系信息</td>
			</tr>
			<tr>
				<td style="width:140px;">
					订单号
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '예약번호' : '';?>
				</td>
				<td>
					<span  class="editable-input editable editable-click" data-type="text" data-name="trade_id" data-pk="<?=isset($voucher) ? $voucher['id']:'';?>"  data-title="修改订单号" ><?=isset($order['trade_id']) ? $order['trade_id'] : '';?></span>
				<td rowSpan=4 style="vertical-align:middle;">
					<span  class="editable-input editable editable-click" data-type="textarea" data-name="contact_info" data-pk="<?=isset($voucher) ? $voucher['id']:'';?>"  data-title="修改联系" ><?=isset($params['contact_info']) ? nl2br($params['contact_info']) : '';?></span>
				</td>
			</tr>
			<tr>
				<td>
					姓名
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '성함' : '';?>
				</td>
				<td>
				<span  class="editable-input editable editable-click" data-type="text" data-name="contact_name" data-pk="<?=isset($voucher) ? $voucher['id']:'';?>"  data-title="修改姓名" ><?=isset($order['contact_name']) ? $order['contact_name'] : '';?>
					</span>
				</td>
			</tr>
			<tr>
				<td>
					电话
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '전화' : '';?>
				</td>
				<td>
					<span  class="editable-input editable editable-click" data-type="text" data-name="mobile" data-pk="<?=isset($voucher) ? $voucher['id']:'';?>"  data-title="修改电话" >
					<?=isset($order['mobile']) ? $order['mobile'] : '';?>
					</span>
				</td>
			</tr>
			<tr>
				<td>
					人数
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '수량' : '';?>
				</td>
				<td>
					<span  class="editable-input editable editable-click" data-type="textarea" data-name="num" data-pk="<?=isset($voucher) ? $voucher['id']:'';?>"  data-title="修改人数" ><?=isset($order['num']) ? $order['num'] : '';?></span>
				</td>
			</tr>
			<tr>
				<td colSpan=3 style=" background:#1EB1ED; color:#fff;text-align:center; font-size:16px; font-weight:bold;">
					订购项目
				</td>
			</tr>
			<tr>
				<td>
					项目名称
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '상품명' : '';?>
				</td>
				<td colSpan=2>
					<span  class="editable-input editable editable-click" data-type="textarea" data-name="title" data-pk="<?=isset($voucher) ? $voucher['id']:'';?>"  data-title="修改项目名称" ><?=isset($params['title']) ? $params['title']: '';?></span>
				</td>
			</tr>
			<tr>
				<td>
					服务日期
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '예약날짜' : '';?>
				</td>
				<td colSpan=2>
					<span  class="editable-input editable editable-click" data-type="text" data-name="start_date" data-pk="<?=isset($voucher) ? $voucher['id']:'';?>"  data-title="修改服务日期" >
					<?=isset($order['start_date']) ? $order['start_date'] : '';?>
					</span>
				</td>
			</tr>
			<tr>
				<td>集合地点</td>
				<td colSpan=2>
					<span  class="editable-input editable editable-click" data-type="textarea" data-name="assembling_place" data-pk="<?=isset($voucher) ? $voucher['id']:'';?>"  data-title="修改集合地点" ><?=isset($params['assembling_place']) ? $params['assembling_place'] : '';?>
					</span>
				</td>
			</tr>
			<tr>
				<td>酒店信息</td>
				<td colSpan=2>
					<span  class="editable-input editable editable-click" data-type="textarea" data-name="hotel" data-pk="<?=isset($voucher) ? $voucher['id']:'';?>"  data-title="修改酒店信息" ><?=isset($order['hotel']) ? $order['hotel'] : '';?>
					</span>
				</td>
			</tr>
			<tr>
				<td>费用说明</td>
				<td colSpan=2>
					<span  class="editable-input editable editable-click" data-type="textarea" data-name="fee_desc" data-pk="<?=isset($voucher) ? $voucher['id']:'';?>"  data-title="修改费用说明" ><?=isset($params['fee_desc']) ? nl2br($params['fee_desc']) : '';?>
					</span>
				</td>
			</tr>
			<tr>
				<td>备注</td>
				<td colSpan=2>
					<span  class="editable-input editable editable-click" data-type="textarea" data-name="remark" data-pk="<?=isset($voucher) ? $voucher['id']:'';?>"  data-title="修改备注" ><?=isset($params['remark']) ? nl2br($params['remark']) : '';?>
					</span>
				</td>
					
			</tr>
			<tr>
				<td colSpan=3 style="text-align:center;">
					<p>出境游贴身达人：smartluelue （个人号，不定期发放旅游信息和福利）</p>
					<p>销售客服微信：qiugongluevip（服务时间：09:00-21:00 北京时间）</p>
					
					<p>客服热线：400-728-0727（服务时间：09:00-21:00）</p>
					<p>求攻略-轻松出境自由行 </p>
				</td>
			</tr>
			
		</tbody>
	</table>

</div>

<div class="editBox" style="top:20px; position:fixed; right:20px; width:200px;">
	<form>
		<div class="form-group">
			<label for="exampleInputEmail1">订单ID</label>
			<input name="tradeId" class="form-control" placeholder="订单ID" value="<?=isset($trade_id)? $trade_id : '';?>">
		</div>
		<div class="form-group">
			<label for="exampleInputEmail1">语言</label>
			<select name="lang" class="form-control" >
				<option value="">默认</option>
				<option <?php if(isset($_GET['lang']) && $_GET['lang'] == 'korean'):?>selected<?php endif;?> value="korean">韩语</option>
				<option <?php if(isset($_GET['lang']) && $_GET['lang'] == 'english'):?>selected<?php endif;?>  value="english">英语</option>
			</select>
		</div>
		<div class="form-group">
			<label for="exampleInputPassword1">模板选择
				<a href="/admin/voucher/templateList" style="font-size:12px; margin-left:10px; " target="_blank" >修改</a>
				<a href="/admin/voucher/templateEdit" style="font-size:12px; margin-left:10px; " target="_blank"> | 新增</a> 
			</label>
<?php if(!empty($template_list)):?>
			<select class="form-control" name="templateId">
				<?php foreach($template_list as $template):?>
				<option <?php if(isset($template['selected'])):?>selected<?php endif;?>  value="<?=$template['id'];?>"><?=$template['title'];?></option>
				<?php endforeach;?>
			</select>
<?php endif; ?>
		</div>
		<a href="javascript:void(0);" id="createBtn" type="submit" class="btn btn-default">生成确认单</a>
<?php if(isset($_GET['id'])):?>
<a class="btn btn-success" target="_blank" href="/plan/export/makepdf?url[]=http://erp.shijieyou.com/admin/voucher/view?id=<?=$_GET['id'];?>%26lang=<?=isset($_GET['lang']) ? $_GET['lang'] : '';?>">导出PDF</a>
<?php endif;?>
	</form>	

</div>

<script>
$(document).ready(function(){
		$('.editable-input').editable({
			url: '/admin/voucher/updateInfo',
				params: function(params) {
					params.type = $(this).attr('info-type');
					return params;
				},
				title: '请输入'
		});

		$('#createBtn').click(function(){
			var tradeId = $('input[name=tradeId]').val();
			var templateId = $('select[name=templateId]').val();
			var lang = $('select[name=lang]').val();

			if(tradeId == '') {
				alert('订单ID不能为空');
				return false;
			}
			if(templateId == '') {
				alert('请选择模板');
				return false;
			}

			$.ajax({
				url:'/admin/voucher/create',
				data:{tradeId:tradeId, templateId:templateId},
				dataType:'JSON',
				success: function(r){
					if(r.code == 200) {
						window.location.href = "/admin/voucher/edit?lang=" +lang + "&id=" + r.data.id;
					}
				}
			});
		});
});
</script>

<body>
</html>
