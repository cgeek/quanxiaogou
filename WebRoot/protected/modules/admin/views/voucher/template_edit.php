<div class="template_list clearfix">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/voucher/templateList">模板列表</a></li>
		<li class="active">修改模板</li>
	</ol>
	<div class="table-responsive container" >
		<form action="/admin/voucher/templateSave" method="post">
			<input type="hidden" name="id" value="<?=isset($template)? $template['id'] : '';?>">
			<div class="form-group">
				<label for="exampleInputPassword1">标题</label>
				<input name="title" class="form-control" placeholder="title" value="<?=isset($template)? $template['title'] : '';?>">
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">集合地点</label>
				<input name="assembling_place" class="form-control" placeholder="集合地点" value="<?=isset($params)? $params['assembling_place'] : '';?>">
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">联系信息</label>
				<textarea class="form-control" name="contact_info"><?=isset($params['contact_info'])? $params['contact_info'] : '';?></textarea>
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">费用说明</label>
				<textarea class="form-control" name="fee_desc"><?=isset($params['fee_desc'])? $params['fee_desc'] : '';?></textarea>
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">备注</label>
				<textarea rows=10 class="form-control" name="remark"><?=isset($params['remark'])? $params['remark'] : '';?></textarea>
			</div>
			<div class="form-group">
				<label for="exampleInputPassword1">关联商品ID：</label>
				<input name="item_ids" class="form-control" placeholder="关联商品ID，多个逗号隔开" value="<?=isset($template['item_ids'])? $template['item_ids'] : '';?>">
			</div>
			<button type="submit" class="btn btn-default">保存</button>
		</form>
	</div>
</div>
