<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<title>确认单</title>
<style>
	<?php echo file_get_contents('https://cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css'); ?>
</style>
</head>
<body>

<div class="container" style="width:800px; margin-top:10px;">
	<table class="table table-bordered">
		<tbody>
			<tr>
				<td colSpan=3 style="height:60px; line-height:60px; font-size:25px; background:#1EB1ED; color:#fff;text-align:center; position:relative;"> 求攻略确认单
					<img src="http://www.qiugonglue.com/assets/images/footer-logo.png" style="position:absolute; right:0px; top:20px;">
				</td>
			</tr>
			<!--tr>
				<td colSpan=3 style="text-align:center;">求攻略- 轻松出境自由行</td>
			</tr-->
			<tr>
				<td colSpan=2 style="text-align:center;font-size:15px; font-weight:bold;">客户信息 <?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '사용자 정보' : '';?></td>
				<td style="width:50%;text-align:center; font-size:15px; font-weight:bold;">地接联系信息</td>
			</tr>
			<tr>
				<td style="width:140px;">
					订单号
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '예약번호' : '';?>
				</td>
				<td><?=isset($order['trade_id']) ? $order['trade_id'] : '';?></td>
				<td rowSpan=4  style="vertical-align:middle;">
					<?=isset($params['contact_info'])? nl2br($params['contact_info']) : '';?>
				</td>
			</tr>
			<tr>
				<td >
					姓名
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '성함' : '';?>
				</td>
				<td>
					<?=isset($order['contact_name']) ? $order['contact_name'] : '';?>
				</td>
			</tr>
			<tr>
				<td >
					电话
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '전화' : '';?>
				</td>
				<td><?=isset($order['mobile']) ? $order['mobile'] : '';?></td>
			</tr>
			<tr>
				<td >
					人数
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '수량' : '';?>
				</td>
				<td><?=isset($order['num']) ? $order['num'] : '';?></td>
			</tr>
			<tr>
				<td colSpan=3 style=" background:#1EB1ED; color:#fff;text-align:center; font-size:16px; font-weight:bold;">
					订购项目
				</td>
			</tr>
			<tr>
				<td >
					项目名称
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '상품명' : '';?>
				</td>
				<td colSpan=2><?=isset($params['title']) ? $params['title']: '';?></td>
			</tr>
			<tr>
				<td >
					服务日期
					<?=isset($_GET['lang']) && $_GET['lang'] == 'korean' ? '예약날짜' : '';?>
				</td>
				<td colSpan=2><?=isset($order['start_date']) ? $order['start_date'] : '';?></td>
			</tr>
			<?php if(isset($params['assembling_place']) && !empty($params['assembling_place'])):?>
			<tr>
				<td>集合地点</td>
				<td colSpan=2><?=isset($params['assembling_place']) ? $params['assembling_place'] : '';?></td>
			</tr>
			<?php endif;?>
			<?php if(isset($order['hotel']) && !empty($order['hotel'])):?>
			<tr>
				<td>酒店信息</td>
				<td colSpan=2>
					<?=isset($order['hotel']) ? nl2br($order['hotel']) : '';?>
				</td>
			</tr>
			<?php endif;?>
			<?php if(isset($params['fee_desc']) && !empty($params['fee_desc'])):?>
			<tr>
				<td>费用说明</td>
				<td colSpan=2><?=isset($params['fee_desc']) ? nl2br($params['fee_desc']) : '';?></td>
			</tr>
			<?php endif;?>
			<tr>
				<td>备注</td>
				<td colSpan=2>
					<p></p>
					<?=isset($params['remark']) ? nl2br($params['remark']) : '';?>
					<p></p>
				</td>
					
			</tr>
			<tr>
				<td colSpan=3 style="text-align:center;">
					<p>出境游贴身达人：smartluelue （个人号，不定期发放旅游信息和福利）</p>
					<p>销售客服微信：qiugongluevip（服务时间：09:00-21:00 北京时间）</p>
					<p>客服热线：400-728-0727（服务时间：09:00-21:00）</p>
					<p>求攻略-轻松出境自由行 </p>
				</td>
			</tr>
			
		</tbody>
	</table>

</div>


<body>
</html>
