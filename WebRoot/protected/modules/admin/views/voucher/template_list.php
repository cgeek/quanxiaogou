<div class="template_list clearfix">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li class="active">确认单模板列表</li>
	</ol>
	<div class="filter-bar clearfix">
		<a href="/admin/voucher/templateEdit" class="btn btn-success">新增模板</a>
		<a href="/admin/voucher/edit" target="_blank" class="btn btn-info">新增确认单</a>
	</div>

	<div class="table-responsive" >
		<table class="table table-btriped trip-list-table">
			<thead>
				<tr style="background:#eee;">
					<th>#</th>
					<th>模板标题</th>
					<th>关联商品</th>
					<th>修改时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
<?php if(!empty($template_list)): ?>
	<?php foreach($template_list as $template):?>
				<tr trip_id="<?=$template['id'];?>">
					<td><?=$template['id'];?></td>
					<td><?=$template['title'];?></td>
					<td><?=$template['item_ids'];?></td>
					<td><?=$template['mtime'];?></td>
					<td>
						<a href="/admin/voucher/templateEdit?id=<?=$template['id'];?>" class="btn btn-xs btn-primary" target="_blank">修改</a>
						<!--a href="javascript:void(0);" class="btn btn-xs btn-danger delete_trip_btn" >删除</a-->
					</td>
				</tr>
			<?php endforeach;?>
<?php endif;?>
		</tbody>
	</table>
		<?php 
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
</div>
