<div class="poi-list-page">
	<div class="poi-list container">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/poi">POI列表</a></li>
	</ol>
	<form class="form-inline" role="form" action="/admin/poi/">
	<div class="pull-left">
		城市：
		<div class="form-group">
			<select name="city" class="form-control">
				<option value="0">--筛选城市--</option>
			<?php foreach($city_list as $city):?>
  			  	<option value="<?=$city['id'];?>"<?=$city['selected']?>><?=$city['name'];?></option>
 		 	<?php endforeach;?>
		  	</select>
		</div>
		<div class="form-group">
			<select name="type" class="form-control">
				<option value="">--类型--</option>
				<option value="sight" <?=(isset($_GET['type']) && $_GET['type'] == 'sight') ? 'selected':'';?> >景点</option>
				<option value="shopping" <?=(isset($_GET['type']) && $_GET['type'] == 'shopping') ? 'selected':'';?> >购物</option>
				<option value="restaurant" <?=(isset($_GET['type']) && $_GET['type'] == 'restaurant') ? 'selected':'';?> >餐厅</option>
				<option value="entertainment" <?=(isset($_GET['type']) && $_GET['type'] == 'entertainment') ? 'selected':'';?> >娱乐</option>
				<option value="traffic" <?=(isset($_GET['type']) && $_GET['type'] == 'traffic') ? 'selected':'';?> >交通</option>
		  	</select>
		</div>
		&nbsp;关键词:
		<div class="form-group">
			<input type="text" name="filter_key" class="form-control" id="filter_key" placeholder="名称或描述" value="<?=$filter_key?>"/>
		</div>
		&nbsp;<button type="submit" class="btn btn-primary">过滤</button>
	</div>
	</form>
<?php if(!empty($poi_list)):?>
		<table class="table">
			<thead>
				<tr>
					<th>封面图片</th>
					<th nowrap>所在城市</th>
					<th>类别</th>
					<th>名称</th>
					<th style="width:50%;">描述</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach($poi_list as $poi):?>
				<tr>
					<td><img src="<?=$poi['cover_image_url'];?>" width="160"></td>
					<td><?=$poi['city_name'];?></td>
					<td><?=$poi['type'];?></td>
					<td><?=$poi['poi_name'];?></td>
					<td><?=cut_str($poi['desc'], 180);?></td>
					<td><a href="/admin/poi/edit?poi_id=<?=$poi['poi_id'];?>" class="btn btn-default btn-xs">编辑</a></td>
				</tr>
	<?php endforeach;?>
			</tbody>
		</table>
		<?php 
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
<?php endif;?>
	</div>
</div>
