<div class="poi-edit-page">
	<div class="poi-detail container">
		<ol class="breadcrumb">
			<li><a href="/admin">管理首页</a></li>
			<li><a href="/admin/poi">POI</a></li>
			<li><?=$this->pageTitle?></li>
		</ol>
		<form class="form-horizontal" role="form" id="poi_edit_form" method="post">
			<input type="hidden" name="poi_id" value="<?=isset($poi) ? $poi['poi_id']:'';?>">
				<div class="form-group" style="background:#eee; padding:10px 0; clearfix; margin:10px 0;">
					<label for="source_url" class="col-lg-2 control-label">抓取URL：</label>
					<div class="col-lg-6" style="padding-left:0;">
						<input type="text" name="source_url" class="form-control url_auto_http" id="source_url" placeholder="" value="<?=isset($poi)? $poi['source_url'] : '';?>"/>
					</div>
					<a class="btn btn-info" id="crawling_sync_btn" href="javascript:void(0);">开始抓取</a>
					<span class="helpblock">支持<a href="http://place.qyer.com/" target="_blank">穷游</a>, <a href="http://www.qiugonglue.com/" target="_blank">求攻略</a></span>
				</div>
				<div class="form-group">
					<label for="poi_name" class="col-lg-2 control-label">POI名称(必填)</label>
					<div class="col-lg-5">
						<input type="text" name="poi_name" class="form-control" id="poi_name" placeholder="" value="<?=isset($poi)? $poi['poi_name'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="poi_name_english" class="col-lg-2 control-label">名称(英文)</label>
					<div class="col-lg-5">
						<input type="text" name="poi_name_english" class="form-control" id="poi_name_english" placeholder="" value="<?=isset($poi)? $poi['poi_name_english'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="poi_name_local" class="col-lg-2 control-label">名称(当地语言)</label>
					<div class="col-lg-5">
						<input type="text" name="poi_name_local" class="form-control" id="poi_name_local" placeholder="" value="<?=isset($poi)? $poi['poi_name_local'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="city_id" class="col-lg-2 control-label">所在城市</label>
					<div class="col-lg-2">
						<select name="city_id" id="city_id" class="form-control">
							<option value="">--所在城市--</option>
					<?php foreach($city_list as $city):?>
							<option value="<?=$city['id'];?>"<?=$city['selected']?>><?=$city['name'];?></option>
				  	<?php endforeach;?>
					  	</select>
						<span class="helpblock"></span>
					</div>
					<label for="area" class="pull-left control-label">区域:</label>
					<div class="col-lg-2">
						<input type="text" name="area" class="form-control" id="area" placeholder="" value="<?=isset($poi)? $poi['area'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>

<?php if(!empty($poi) && !empty($poi['poi_id'])):?>
				<div class="form-group">
					<label for="inputSubtitle" class="col-lg-2 control-label">设置封面图片</label>
					<div class="col-lg-8">
						<input type="hidden" name="cover_image" value="<?=isset($poi)? $poi['cover_image'] : '';?>"/>
						<input type="hidden" name="cover_image_before" value="<?=isset($poi)? $poi['cover_image'] : '';?>"/>
						<div class="poi-cover-image-box">
						<?php if(isset($poi) && !empty($poi['cover_image_url'])) : ?>
							<img src="<?=upimage($poi['cover_image_url']);?>"/>
						<?php else:?>
							<img src="http://img03.taobaocdn.com/tps/i3/T1MNu0XdXnXXXXXXXX-210-210.png"/>
						<?php endif;?>
							<div class="loading"></div>
						</div>

						<!-- The fileinput-button span is used to style the file input field as button -->
						<span class="btn btn-xs btn-success fileinput-button">
							 <i class="glyphicon glyphicon-circle-arrow-up"></i>
							 <span>选择图片</span>
							 <!-- The file input field used as target for the file upload widget -->
							<input id="fileupload" type="file" name="Filedata" multipart=false/>
					    </span>
					</div>
				</div>
<?php endif;?>

				<div class="form-group">
					<label for="type" class="col-lg-2 control-label">类型</label>
					<div class="col-lg-2">
						<select name="type" id="type" class="form-control">
							<option value="">--类型--</option>
							<?php foreach($type_list as $type):?>
						  		  <option value="<?=$type['type'];?>"<?=$type['selected']?>><?=$type['name'];?></option>
						  	<?php endforeach;?>
						</select>
						<span class="helpblock"></span>
					</div>
					<div class="col-lg-5">
						<input type="checkbox" name="is_private" value="1" <?=(isset($poi) && $poi['is_private']) ? 'checked' : '';?>> 私藏
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group" id="recommend_div" style="display:none;">
					<label for="recommend" class="col-lg-2 control-label">推荐(食物)</label>
					<div class="col-lg-5">
						<input type="text" name="recommend" class="form-control" id="recommend" placeholder="" value="<?=isset($poi)? $poi['recommend'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="phone" class="col-lg-2 control-label">电话</label>
					<div class="col-lg-3">
						<input type="text" name="phone" class="form-control" id="phone" placeholder="" value="<?=isset($poi)? $poi['phone'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="website" class="col-lg-2 control-label">网址</label>
					<div class="col-lg-5">
						<input type="text" name="website" class="form-control url_auto_http" id="website" placeholder="http://www.xxx.com" value="<?=isset($poi)? $poi['website'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="address" class="col-lg-2 control-label">地址</label>
					<div class="col-lg-5">
						<input type="text" name="address" class="form-control" id="address" placeholder="" value="<?=isset($poi)? $poi['address'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="address_local" class="col-lg-2 control-label">地址(当地语言)</label>
					<div class="col-lg-5">
						<input type="text" name="address_local" class="form-control" id="address_local" placeholder="" value="<?=isset($poi)? $poi['address_local'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="traffic" class="col-lg-2 control-label">交通</label>
					<div class="col-lg-5">
						<textarea name="traffic" class="form-control" id="traffic" rows="3" placeholder=""><?=isset($poi)? $poi['traffic'] : '';?></textarea>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="desc" class="col-lg-2 control-label">文本描述</label>
					<div class="col-lg-6">
						<textarea name="desc" class="form-control" id="desc" rows="7" placeholder=""><?=isset($poi)? $poi['desc'] : '';?></textarea>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="tips" class="col-lg-2 control-label">特别提示</label>
					<div class="col-lg-6">
						<textarea name="tips" class="form-control" id="tips" rows="3" placeholder=""><?=isset($poi)? $poi['tips'] : '';?></textarea>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="lat" class="col-lg-2 control-label">GPS</label>
					<div class="col-lg-8">
						<div class="col-lg-3 show_map_btn">
							<img src="http://maps.googleapis.com/maps/api/staticmap?center=<?=$poi['lat'];?>,<?=$poi['lon'];?>&zoom=12&size=160x160&maptype=roadmap&markers=color:red%7Ccolor:red%7Clabel:C%7C<?=$poi['lat'];?>,<?=$poi['lon'];?>&sensor=false">
						</div>
						<div class="col-lg-6">
							坐标：<input type="text" id="lat_lon_input" class="form-control" placeholder="13.12312300,120.1113300" value="<?=$poi['lat'];?>,<?=$poi['lon'];?>">
							<input type="hidden"  name="lat" class="form-control" id="lat" placeholder="" value="<?=isset($poi)? $poi['lat'] : '';?>"/>
							<input type="hidden"  name="lon" class="form-control" id="lon" placeholder="" value="<?=isset($poi)? $poi['lon'] : '';?>"/>
							<button type="button" id="gps_check" class="btn btn-xs btn-info" style="margin-top:5px;">跳到Google Map获取</button>
						</div>
					</div>
					<span class="helpblock"></span>
				</div>
				<!--
				<div class="form-group">
					<label for="intro" class="col-lg-2 control-label">简介</label>
					<div class="col-lg-5">
						<textarea name="intro" class="form-control" id="intro" rows="6" placeholder=""><?=isset($poi)? $poi['intro'] : '';?></textarea>
						<span class="helpblock"></span>
					</div>
				</div>
				-->
				<div class="col-lg-12 show-more-form-groups clearfix" style="color:blue; cursor:pointer; height:40px;">
					<div class="col-lg-2">
					</div>
					<div class="col-lg-5">显示更多 ...
					</div>
				</div>
				<div class="hidden-form-groups clearfix" style="display:none;">
				<div class="col-lg-12 hide-more-form-groups clearfix" style="color:blue; cursor:pointer; height:40px;">
					<div class="col-lg-2">
					</div>
					<div class="col-lg-5">
					隐藏更多 ...
					</div>
				</div>
				<div class="form-group">
					<label for="html_content" class="col-lg-2 control-label">HTML描述</label>
					<div class="col-lg-5">
						<textarea name="html_content"  style="display:none;"  rows=5 ><?=$poi['html_content'];?></textarea>
						<textarea id="html_content_Text" style="width:700;"  rows=5 ><?=$poi['html_content'];?></textarea>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="poi_name_pinyin" class="col-lg-2 control-label">名称拼音</label>
					<div class="col-lg-5">
						<input type="text" name="poi_name_pinyin" class="form-control" id="poi_name_pinyin" placeholder="" value="<?=isset($poi)? $poi['poi_name_pinyin'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="price" class="col-lg-2 control-label">价格</label>
					<div class="col-lg-5">
						<input type="text" name="price" class="form-control" id="price" placeholder="" value="<?=isset($poi)? $poi['price'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="rank" class="col-lg-2 control-label">排名</label>
					<div class="col-lg-5">
						<input type="text" name="rank" class="form-control" id="rank" placeholder="" value="<?=isset($poi)? $poi['rank'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="rating" class="col-lg-2 control-label">评分</label>
					<div class="col-lg-5">
						<input type="text" name="rating" class="form-control" id="rating" placeholder="" value="<?=isset($poi)? $poi['rating'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="tags" class="col-lg-2 control-label">标签</label>
					<div class="col-lg-5">
						<input type="text" name="tags" class="form-control" id="tags" placeholder="" value="<?=isset($poi)? $poi['tags'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="open_time" class="col-lg-2 control-label">开放时间</label>
					<div class="col-lg-5">
						<input type="text" name="open_time" class="form-control" id="open_time" placeholder="" value="<?=isset($poi)? $poi['open_time'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				</div><!-- end hidden form groups -->

				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">保存</button>
<?php if(!empty($poi)):?>
						<a href="javascript:void(0);" id="delete-poi-btn" class="btn btn-danger">删除POI</a>
						<a href="javascript:void(0);" id="cancel-btn" class="btn btn-default">取 消</a>
<?php endif;?>
						<span class="help-block"></span>
					</div>
				</div>
		</form>
	</div>
</div>
<script src="http://maps.googleapis.com/maps/api/js?sensor=false" type="text/javascript"></script>
<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/poi_edit');
	});
</script>
<script type="text/template" id="map_box_tpl">
		<div class="modal-header">
			<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
			<h4 class="modal-title">修改地图坐标</h4>
		</div>
		<div class="modal-body">
			<div id="map_box"></div>
			<label>当前坐标：<span style="color:green;" class="lat_lon_span">{{lat}}, {{lon}}</span></label>
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
			<button type="button" class="btn btn-primary" id="save_lat_btn">保存修改</button>
		</div>
</script>
