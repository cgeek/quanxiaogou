<div class="">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/customer">客户列表</a></li>
		<?php if(isset($customer)):?>
		<li><a href="/admin/customer/detail?id=<?=$customer['id'];?>"><?=$customer['contact'];?>的信息</a></li>
		<?php endif;?>
		<li class="active">编辑同伴信息</li>
	</ol>
	<div class="traveler-edit">
		<div class="traveler-info-form col-lg-12">
			<form class="form-horizontal" role="form" method="post" id="traveler_edit_form">
				<input type="hidden" name="customer_id" value="<?=isset($customer) ? $customer['id']:'';?>">
				<input type="hidden" name="traveler_id" value="<?=isset($traveler) ? $traveler['id']:'';?>">
				<div class="form-group">
					<label for="inputName" class="col-lg-2 control-label">中文姓名</label>
					<div class="col-lg-3">
						<input type="text" name="name" class="form-control" id="inputName" placeholder="中文姓名" value="<?=isset($traveler)? $traveler['name']:'';?>">
					</div>
				</div>
				<div class="form-group row">
					<label for="inputEnglishName" class="col-lg-2 control-label">英文名</label>
					<div class="col-lg-1">
						<input type="text" name="last_name" class="form-control " id="inputEnglishName" placeholder="姓" value="<?=isset($traveler)? $traveler['last_name']:'';?>">
					</div>
					<div class="col-lg-2">
						<input type="text" name="first_name" class="form-control" id="inputEnglishName" placeholder="名字" value="<?=isset($traveler)? $traveler['first_name']:'';?>">
					</div>
				</div>
				<div class="form-group">
					<label for="inputSex" class="col-lg-2 control-label">性别</label>
					<div class="col-lg-3">
						<select class="form-control" name="sex" >
						  <option <?php if(isset($traveler) && $traveler['sex'] == 'female'):?>selected<?php endif;?> value="female">女</option>
						  <option <?php if(isset($traveler) && $traveler['sex'] == 'male'):?>selected<?php endif;?> value="male">男</option>
						</select>
					</div>
				</div>
				<div class="form-group">
					<label for="inputBirthday" class="col-lg-2 control-label date-picker">出生日期</label>
					<div class="col-lg-2">
						<div class="input-append date date-picker"  data-date="1988-10-01" data-date-format="yyyy-mm-dd">
							<input type="text" name="birthday" class="form-control" id="inputBirthday" style="display:inline-block;" placeholder="格式：1987-10-14" value="<?=isset($traveler)? $traveler['birthday']:'0000-00-00';?>">
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
					<p class="help-block">格式：1987-10-14</p>
				</div>

				<div class="form-group">
					<label for="inputPassportNo" class="col-lg-2 control-label">护照号</label>
					<div class="col-lg-2">
						<input type="text" name="passport_no" class="form-control" id="inputPassportNo" placeholder="" value="<?=isset($traveler)? $traveler['passport_no']:'';?>">
					</div>
				</div>

				<div class="form-group">
					<label for="inputPassportValidity" class="col-lg-2 control-label">护照有效期</label>
					<div class="col-lg-2">
						<div class="input-append date date-picker"  data-date="2023-10-01" data-date-format="yyyy-mm-dd">
							<input type="text" name="passport_validity" class="form-control" id="inputBirthday" style="display:inline-block;" placeholder="格式：1987-10-14" value="<?=isset($traveler)? $traveler['passport_validity']:'0000-00-00';?>">
							<span class="add-on"><i class="icon-th"></i></span>
						</div>
					</div>
					<p class="help-block">格式：2013-10-01</p>
				</div>
				<div class="form-group">
					<label for="inputIdNo" class="col-lg-2 control-label">身份证号码</label>
					<div class="col-lg-3">
						<input type="text" name="id_no" class="form-control" id="inputIdNo" placeholder="如：330812XXXXXXXX" value="<?=isset($traveler)? $traveler['id_no']:'';?>">
					</div>
				</div>
				<!--div class="form-group">
					<label for="inputFlight" class="col-lg-2 control-label">航班信息</label>
					<div class="col-lg-5">
						<textarea name="flight_info" class="form-control" rows="3" placeholder="如果还没有预定机票，则不需填写"><?=isset($traveler)? $traveler['flight_info']:'';?></textarea>
					</div>
				</div-->
				<div class="form-group">
					<label class="col-lg-2 control-label"></label>
					<div class="checkbox col-lg-5">
					<input type="checkbox" <?php if(isset($traveler) && $traveler['is_customer'] == 1):?>checked<?php endif;?> value="1" name="is_customer">
						是主联系人
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">保存信息</button>
						<?php if(isset($customer)):?>
							<a class="btn btn-default" href="/admin/traveler/add?customer_id=<?=$customer['id'];?>">继续添加</a>
							<a class="btn btn-default" href="/admin/customer/detail?id=<?=$customer['id'];?>">返回<?=$customer['contact'];?>的信息</a>
						<?php endif;?>
					</div>
				</div>
			</form>
		</div>	
	</div>
</div>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/traveler_edit');
	});
</script>
