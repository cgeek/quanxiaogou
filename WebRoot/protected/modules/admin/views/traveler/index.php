<div class="traveler_list">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/customer">客户列表</a></li>
		<li class="active"><?=isset($customer) ? $customer['contact'] . '的':'';?>同伴信息</li>
	</ol>
	<div class="filter-bar">
		<form class="form-inline" role="form">
			<div class="form-group">
				<label >筛选条件：</label>
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputCustomerName">联系人姓名</label>
				<input type="text" name="customer_name" class="form-control" id="inputCustomerName" placeholder="联系人姓名">
			</div>
			<div class="form-group">
				<label class="sr-only" for="inputName">旅客姓名</label>
				<input type="text" name="name" class="form-control" id="inputName" placeholder="旅客姓名">
			</div>
			<button type="submit" class="btn btn-primary">查找</button>
			<a href="/admin/traveler/add?customer_id=<?=isset($customer_id)? $customer_id: '';?>" class="btn btn-success pull-right">新增旅客信息</a>
		</form>
	</div>
	<div class="table-responsive" >
		<table class="table table-bordered traveler-list-table">
			<thead>
				<tr style="background:#eee;">
					<th>#</th>
					<th>姓名</th>
					<th>英文名</th>
					<th>性别</th>
					<th>出生日期</th>
					<th>护照号</th>
					<th>护照有效期</th>
					<th>年龄</th>
					<th>身份证号</th>
					<th>是否是联系人</th>
					<th>修改时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
<?php if(!empty($traveler_list)):?>
	<?php foreach($traveler_list as $traveler):?>
				<tr>
					<td><?=$traveler['id'];?></td>
					<td><?=$traveler['name'];?></td>
					<td><?=$traveler['english_name'];?></td>
					<td><?=$traveler['sex'];?></td>
					<td><?=$traveler['birthday'];?></td>
					<td><?=$traveler['passport_no'];?></td>
					<td><?=$traveler['passport_validity'];?></td>
					<td><?=$traveler['age'];?></td>
					<td><?=$traveler['id_no'];?></td>
					<td><?=$traveler['is_customer'];?></td>
					<td><?=date('Y-m-d',strtotime($traveler['mtime']));?></td>
					<td>
						<a class="btn btn-primary btn-xs" href="/admin/traveler/edit/?id=<?=$traveler['id'];?>">修改信息</a>
						<a class="btn btn-primary btn-xs" href="/admin/customer/detail?id=<?=$traveler['customer_id'];?>">客户信息</a>
					</td>
				</tr>
	<?php endforeach;?>
<?php else:?>
				<tr>
					<td colspan=12>暂无旅客信息</td>
				</tr>
<?php endif;?>
			</tbody>
		</table>
		<?php 
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
	</div>
</div>
<script>
	seajs.use('/assets/js/router.js', function(router){
	//	router.load('admin/traveler_index');
	});
</script>
