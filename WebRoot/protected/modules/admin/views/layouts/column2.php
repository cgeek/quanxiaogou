<?php $this->beginContent('/layouts/main'); ?>
		<div class="main"><?php echo $content; ?></div>
		<div class="sidebar-nav">
		<?php if($this->id == 'default' || $this->id == 'user'):?>
			<ul class="nav nav-list">
				<li class="nav-header">流量统计</li>
				<li class="nav-sub"><a href="/admin/default/stats">客户端Ad浏览量</a></li>
				<li class="nav-sub"><a href="/admin/default/stats?type=weixin">微信页面浏览量</a></li>
				<li class="nav-sub"><a href="/admin/default/stats?type=order">客户日增</a></li>
			</ul>
			<ul class="nav nav-list">
				<li class="nav-header">账户管理</li>
				<li class="nav-sub"><a href="/admin/user/changeInfo">修改资料</a></li>
				<li class="nav-sub"><a href="/admin/user/changePassword">修改密码</a></li>
			</ul>
			<!--ul class="nav nav-list">
				<li class="nav-header">系统相关</li>
				<li class="nav-sub"><a target="_blank" href="/admin/oauth/taobao">淘宝后台授权</a></li>
			</ul-->

		<?php elseif($this->id == 'package'):?>
			<ul class="nav nav-list">
				<li class="nav-header">酒店产品</li>
				<li class="nav-sub"><a href="/admin/package?type=hotel&status=1">出售中的酒店</a></li>
				<li class="nav-sub"><a href="/admin/package?type=hotel&status=0">未上架的酒店</a></li>
				<li class="nav-sub"><a href="/admin/package/addHotel">添加酒店 </a></li>
			</ul>
			<ul class="nav nav-list">
				<li class="nav-header">活动产品</li>
				<li class="nav-sub"><a href="/admin/package?type=activity&status=1">出售中的活动</a></li>
				<li class="nav-sub"><a href="/admin/package?type=activity&status=0">未上架的活动</a></li>
				<li class="nav-sub"><a href="">添加新活动</a></li>
			</ul>

		<?php elseif($this->id == 'weixin'):?>
			<ul class="nav nav-list">
				<li class="nav-header">消息管理</li>
				<li class="nav-sub"><a href="/admin/weixin/messageList">消息列表</a></li>
			</ul>
			<ul class="nav nav-list">
				<li class="nav-header">基本设置</li>
				<li class="nav-sub"><a href="/admin/weixin/editContent?id=1">服务介绍</a></li>
				<li class="nav-sub"><a href="/admin/weixin/editContent?id=2">团队介绍</a></li>
			</ul>

		<?php elseif($this->id == 'todo'):?>
			<ul class="nav nav-list">
				<li class="nav-header">任务管理</li>
				<li class="nav-sub"><a href="/admin/todo">所有任务</a></li>
				<li class="nav-sub"><a href="/admin/todo?admin_id=<?=Yii::app()->adminUser->id;?>">我的任务</a></li>
			</ul>

		<?php elseif($this->id == 'customer' || $this->id == 'traveler'):?>
			<ul class="nav nav-list">
				<li class="nav-header">客户管理</li>
				<li class="nav-sub"><a href="/admin/customer?admin_id=<?=Yii::app()->adminUser->id;?>">我负责的客户</a></li>
				<li class="nav-sub"><a href="/admin/customer?all=true">所有客户</a></li>
				<li class="nav-sub"><a href="/admin/customer?status=0">未认领的客户</a></li>
				<li class="nav-sub"><a href="/admin/customer?status=1">进行中的客户</a></li>
				<li class="nav-sub"><a href="/admin/customer?status=5">已经结束的客户</a></li>
				<li class="nav-sub"><a href="/admin/customer?status=-2">已取消的客户</a></li>
				<li class="nav-sub"><a href="/admin/customer/add">添加客户</a></li>
			</ul>
			<ul class="nav nav-list">
				<li class="nav-header">旅客信息管理</li>
				<li class="nav-sub"><a href="/admin/traveler">所有旅客</a></li>
			</ul>

			<ul class="nav nav-list">
				<li class="nav-header">交易管理</li>
				<li class="nav-sub"><a href="/admin/order/alipayRecord">支付宝交易列表</a></li>
				<li class="nav-sub"><a href="/admin/order/recordCustomers">最近交易过的客户</a></li>
			</ul>
		<?php elseif($this->id == 'order'):?>
			<ul class="nav nav-list">
				<li class="nav-header">代订管理</li>
				<li class="nav-sub"><a href="/admin/order">所有代订</a></li>
				<li class="nav-sub"><a href="/admin/order?status=0">等待付款</a></li>
				<li class="nav-sub"><a href="/admin/order?status=1">已付款</a></li>
				<li class="nav-sub"><a href="/admin/order?status=2">等待支付尾款</a></li>
				<li class="nav-sub"><a href="/admin/order?status=-2">已取消代订</a></li>
			</ul>
			<ul class="nav nav-list">
				<li class="nav-header">交易管理</li>
				<li class="nav-sub"><a href="/admin/order/alipayRecord">支付宝交易列表</a></li>
				<li class="nav-sub"><a href="/admin/order/recordCustomers">最近交易过的客户</a></li>
			</ul>

		<?php elseif($this->id == 'journey' || $this->id == 'demoTripDay'):?>
			<ul class="nav nav-list">
				<li class="nav-header">行程管理</li>
				<li class="nav-sub"><a href="/admin/journey">所有行程</a></li>
				<!--li class="nav-sub"><a href="/plan/create/step1" target="_blank">创建新行程</a></li-->
			</ul>

		<?php elseif($this->id == 'poi' || $this->id == 'hotel' || $this->id == 'note' || $this->id == 'city' || $this->id == 'demoTripDay' || $this->id == 'offlineMap'):?>
			<ul class="nav nav-list">
				<li class="nav-header">POI管理</li>
				<li class="nav-sub"><a href="/admin/poi">所有POI</a></li>
				<li class="nav-sub"><a href="/admin/poi/new">添加POI</a></li>
			</ul>
			<ul class="nav nav-list">
				<li class="nav-header">酒店管理</li>
				<li class="nav-sub"><a href="/admin/hotel">所有酒店</a></li>
				<li class="nav-sub"><a href="/admin/hotel/new">添加酒店</a></li>
			</ul>
			<ul class="nav nav-list">
				<li class="nav-header">贴士管理</li>
				<li class="nav-sub"><a href="/admin/note">所有贴士</a></li>
				<li class="nav-sub"><a href="/admin/note/new">添加贴士</a></li>
			</ul>
			<ul class="nav nav-list">
				<li class="nav-header">目的地管理</li>
				<li class="nav-sub"><a href="/admin/city">所有目的地</a></li>
				<li class="nav-sub"><a href="/admin/city/new">添加目的地</a></li>
			</ul>


		<?php elseif($this->id == 'voucher'):?>
			<ul class="nav nav-list">
				<li class="nav-header">确认单管理</li>
				<li class="nav-sub"><a href="/admin/voucher/templateList">模板列表</a></li>
				<li class="nav-sub"><a href="/admin/voucher/list">确认单</a></li>
			</ul>
		<?php endif;?>

		</div>

<?php $this->endContent(); ?>
