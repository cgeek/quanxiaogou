<div class="todo">
	<div class="container">
		<table class="table">
			<thead>
				<tr>
					<th>客户</th>
					<th>标题</th>
					<th>关联客服</th>
					<th>创建时间</th>
					<th>状态</th>
				</tr>
			</thead>
			<tbody>
			<?php foreach($check_item_list as $check_item):?>
				<tr>
					<th><?=$check_item['customer']['contact'];?></th>
					<th><?=$check_item['title'];?></th>
					<th><?=$check_item['admin']['nick_name'];?></th>
					<th><?=date('Y-m-d', strtotime($check_item['ctime']));?></th>
					<th><?=$check_item['status'];?></th>
				</tr>
			<?php endforeach;?>
			</tbody>
		</table>
		<?php 
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
	</div>
</div>
