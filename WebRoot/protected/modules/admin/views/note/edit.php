<div class="note-edit-page">
	<div class="note-detail container">
		<ol class="breadcrumb">
			<li><a href="/admin">管理首页</a></li>
			<li><a href="/admin/note">攻略</a></li>
			<li><?=$this->pageTitle?></li>
		</ol>
		<form class="form-horizontal" role="form" id="note_edit_form" method="post">
			<input type="hidden" name="id" value="<?=isset($note) ? $note['id']:'';?>">
			<input type="hidden" name="type" value="tips"><!-- 暂时只有一个类型，贴士-->
<?php if(!empty($note)):?>
				<div class="form-group">
					<label for="city_id" class="col-lg-2 control-label">所在城市</label>
					<div class="col-lg-2">
						<select name="city_id" id="city_id" class="form-control">
						  <option value="">--所在城市--</option>
					<?php foreach($city_list as $city):?>
				  		  <option value="<?=$city['id'];?>"<?=$city['selected']?>><?=$city['name'];?></option>
				  	<?php endforeach;?>
					  	</select>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="title" class="col-lg-2 control-label">标题</label>
					<div class="col-lg-5">
						<input type="text" name="title" class="form-control" id="title" placeholder="" value="<?=isset($note)? $note['title'] : '';?>"/>
						<span class="helpblock"></span>
					</div>
				</div>
				<div class="form-group">
					<label for="content" class="col-lg-2 control-label">内容</label>
					<div class="col-lg-5">
						<textarea name="content" class="form-control" id="content" rows="12" placeholder=""><?=isset($note)? $note['content'] : '';?></textarea>
						<span class="helpblock"></span>
					</div>
				</div>
<?php endif;?>
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">保存</button>
<?php if(!empty($note)):?>
						<a href="javascript:void(0);" id="delete-note-btn" class="btn btn-danger">删除攻略</a>
<?php endif;?>
						<span class="help-block"></span>
					</div>
				</div>
		</form>
	</div>
</div>
<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/poi_note_edit');
	});
</script>
