<div class="note-list-page">
	<div class="note-list container">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/note">旅游贴士列表</a></li>
	</ol>
	<form class="form-inline" method="get" action="/admin/note/">
		<div class="form-group">
			<label class="sr-only" for="keyword">关键词搜索</label>
			<select class="form-control" name="city_id">
				<option value="">所有城市</option>
				<?php if(!empty($city_list)):?>
					<?php foreach($city_list as $city):?>
						<option value="<?=$city['id'];?>" <?=$city['selected'] ? 'selected' : '';?>><?=$city['name'];?></option>
					<?php endforeach;?>
				<?php endif;?>
			</select>
		</div>
		<div class="form-group">
			<label class="sr-only" for="keyword">关键词搜索</label>
			<input type="text" class="form-control" name="keyword" placeholder="关键词" value="<?=isset($keyword)? $keyword : '';?>">
		</div>
		<button type="submit" class="btn btn-primary">筛选</button>
		<a href="/admin/note/new" class="pull-right btn btn-success">添加</a>
	</form>

<?php if(!empty($note_list)):?>
	<ul class="demo_trip_day_list clearfix ">
	<?php foreach($note_list as $note):?>
		<li class="col-md-4" note_id="<?=$note['id'];?>">
			<div class="" style="border:1px solid #ccc; padding:10px;">
				<div class="title"><a href="/admin/note/edit?id=<?=$note['id'];?>"><?=$note['title'];?></a></div>
				<div class="desc"><?=nl2br($note['content']);?></div>
				<div class="action clearfix">
					<div class="pull-right">
						<a href="/admin/note/edit?id=<?=$note['id'];?>" class="btn btn-xs btn-primary">修改</a>
						<a href="javascript:void(0);" class="btn btn-xs btn-default delete_note_btn">删除</a>
					</div>
				</div>
			</div>
		</li>
	<?php endforeach;?>
	</ul>
<?php endif;?>


		<?php
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
</div>


<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/note_list');
	});

</script>
