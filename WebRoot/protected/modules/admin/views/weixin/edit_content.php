<div class="weixin">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/weixin">微信管理</a></li>
		<li class="active"><?=isset($content) ? $content['title'] :'';?></li>
	</ol>
	<div class="container">
		<div class="weixin-content-edit-form left">
			<form class="form-horizontal" role="form" id="weixin_content" method="post" style="width:500px;">
				<input type="hidden" name="id" value="<?=isset($content) ? $content['id']:'';?>">
				<div class="form-group">
					<label for="inputTitle" class="col-lg-2 control-label">标题</label>
					<div class="col-lg-8">
						<input id="title" class="form-control" value="<?=$content['title'];?>" >
					</div>
				</div>
				<div class="form-group">
					<label for="inputSubtitle" class="col-lg-2 control-label">封面</label>
					<div class="col-lg-8">
						<input type="hidden" name="cover_image" value="<?=isset($content)? $content['cover_image'] : '';?>">
						<div class="weixin-cover-image-box">
						<?php if(isset($content) && !empty($content['cover_image'])) : ?>
							<img src="<?=upimage($content['cover_image'], 'org');?>">
						<?php else:?>
							<img src="" >
						<?php endif;?>
							<div class="loading"></div>
						</div>
						<span class="btn btn-default fileinput-button">
							 <i class="glyphicon glyphicon-plus"></i>
							 <span>选择图片</span>
							<input id="fileupload" type="file" name="Filedata" multipart=false>
					    </span>
					</div>
				</div>
				<div class="form-group">
					<label for="inputDesc" class="col-lg-2 control-label">摘要</label>
					<div class="col-lg-8">
						<textarea id="inputDesc" name="desc" class="form-control"><?=$content['desc'];?></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inputContent" class="col-lg-2 control-label">详细信息</label>
					<div class="col-lg-8">
						<textarea name="content"  style="display:none;"  rows=5 ><?=$content['content'];?></textarea>
						<textarea id="detailText" style="width:360px;"  rows=5 ><?=$content['content'];?></textarea>
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">保存</button>
						<span class="help-block"></span>
					</div>
				</div>
			</form>
		</div>
		<div class="weixin-content-preview left">
		</div>
	</div>
</div>

<script type="text/template" id="multiuploadpic-tpl">
	<div class="multiuploadPic-pop">
		<div class="multiuploadpicwrap">
			<a href="javascript:void(0);" class="close">×</a>
			<div class="hd">上传图片</div>
			<div class="bd">
				<div class="multiupload-wrap">
					<div class="init-status">
						<div class="swfupload-btn">
							<div id="uploadBtn"></div>
							<a href="javascript:void(0);" class="uploadbtn" target="_self">添加的照片</a>
						</div>
					</div>
					<div class="upload-status emptypiclist">
						<dl>
							<dt>
								<span class="listtip"></span>
								<a href="#" class="clearlist">清空列表</a>
							</dt>
							<dd class="wait-upload">
								<ul class="piclist" id="fileQueue"></ul>
							</dd>
							<dd class="upload-tool">
								<span class="tntip"></span>
							</dd>
						</dl>
						<div class="complete-list">
							<div class="picedit">
								<ul class="piclist"></ul>
							</div>
							<div class="btmbtn">
								<a href="#" class="btn btn-primary" id="saveUploadBtn">保存图片</a>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</script>
<script type="text/template" id="cover-image-select-tpl">
<div class="coverpicwrap-pop">
	<div class="coverpicwrap">
		<a href="javascript:void(0);" class="close">×</a>
		<div class="hd">选择封面</div>
		<div class="bd">
			<ul>
			{{#image_list}}
				<li class="note-pic" image_hash={{image_hash}}>
					<div class="image">
						<img class="noteImage" src="{{image_url_small}}" style="width:120px;; height:auto">
					</div>
					<div class="selected "></div>
				</li>
			{{/image_list}}
			</ul>
			<div class="saveBtnWrap"><a id="set-cover-image-btn" class="btn btn-warning saveBtn" href="javascript:void(0);">设为封面</a></div>
		</div>
	</div>
</div>
</script>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/weixin_content_edit');
	});
</script>
