<div class="weixin_message_list clearfix">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li><a href="/admin/weixin">微信管理</a></li>
		<li class="active">消息列表</li>
	</ol>
	<div class="table-responsive" >
		<table class="table">
			<thead>
				<tr style="background:#eee;">
					<th>CreateTime</th>
					<th>Content</th>
					<th>FromUserName</th>
					<th>ToUserName</th>
					<th>MsgType</th>
					<th>MsgId</th>
				</tr>
			</thead>
			<tbody>
<?php if(!empty($message_list)): ?>
	<?php foreach($message_list as $message):?>
				<tr>
					<td><?=date('Y-m-d H:i:s',$message['CreateTime']);?></td>
					<td><?=$message['Content'];?></td>
					<td><?=$message['FromUserName'];?></td>
					<td><?=$message['ToUserName'];?></td>
					<td><?=$message['MsgType'];?></td>
					<td><?=$message['MsgId'];?></td>
				</tr>
	<?php endforeach;?>
<?php else:?>
				<tr>
					<td colspan=11>暂没有微信信息</td>
				</tr>
<?php endif;?>
			</tbody>
		</table>
		<?php 
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
	</div>
		<a target="_blank" href="https://mp.weixin.qq.com/" class="btn btn-primary">立即回复</a>

</div>
