<?php
Yii::import('ext.SimpleHTMLDOM.SimpleHTMLDOM');

class HotelController extends Controller{
	private $_data;
	
	public $layout = '/layouts/column2';
	
	public function actionIndex(){
		$this->pageTitle = '酒店列表';
		
		//base params 
		$page = Yii::app()->request->getParam('page');
		$city_id = isset($_GET['city']) ? intval($_GET['city']) : 0;
		
		$hotel_list = array();
		
		$pageSize = 20;
		$page = !empty($page) ? intval($page) : 1;
		$status = Yii::app()->request->getParam('status');
		$status = empty($status) ? 0 : $status;
		$offset = ($page - 1) * $pageSize;
		
		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		$criteria->addCondition("status=$status");
		if($city_id){
			$criteria->addCondition("city_id=$city_id");
		}
		
		if(isset($_GET['filter_key']) && $_GET['filter_key']!=''){
			$criteria->addCondition("(`title` like '%{$_GET['filter_key']}%'"
				. " OR `hotel_name_english` like '%{$_GET['filter_key']}%'"
				. " OR `desc` like '%{$_GET['filter_key']}%')");
			$this->_data['filter_key'] = $_GET['filter_key'];
		}else{
			$this->_data['filter_key'] = '';
		}
		
		$criteria->order = ' `ctime` DESC';
		
		$count = Hotel::model()->count($criteria);
		$hotels = Hotel::model()->findAll($criteria);
		if(!empty($hotels)) {
			foreach($hotels as $hotel) {
				$hotel = $hotel->attributes;
				$hotel['cover_image'] = $hotel['cover_image'];
				$hotel['cover_image_url'] = upimage($hotel['cover_image']);
				//$hotel['cover_image'] = $this->_getImageIdFromURL($hotel['cover_image']);
				$hotel_list[] = $hotel;
			}
		}
		$this->_data['hotel_list'] = $hotel_list;
		
		$city_list = $this->_getCityList($city_id);
		$this->_data['city_list'] = $city_list;
		
		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;

		$this->render('list', $this->_data);
	}
	
	public function actionNew(){
		$this->pageTitle = '新增酒店';
		
		$hotel = array();
		$hotel['hotel_id'] = 0;
		$this->_data['hotel'] = $hotel;

		$city_list = $this->_getCityList();
		$this->_data['city_list'] = $city_list;
		
		$type_list = $this->_getTypeList('hotel');
		$this->_data['type_list'] = $type_list;

		$this->render('edit', $this->_data);
	}
	
	public function actionEdit(){
		$this->pageTitle = '编辑酒店';
		$hotel_id = Yii::app()->request->getParam('hotel_id');
		
		$hotel = Hotel::model()->findByPk($hotel_id);
		if(empty($hotel)) {
			throw new CHttpException('404', '找不到POI');
		}
		
		$hotel = $hotel->attributes;
		
		$hotel['cover_image_url'] = upimage($hotel['cover_image']);
		//$hotel['cover_image'] = $this->_getImageIdFromURL($hotel['cover_image']);
		//get images
		
		$this->_data['hotel'] = $hotel;
		
		$city_list = $this->_getCityList($hotel['city_id']);
		$this->_data['city_list'] = $city_list;
		
		$type_list = $this->_getTypeList($hotel['type']);
		$this->_data['type_list'] = $type_list;
		
		$this->render('edit', $this->_data);
	}
	
	public function actionSaveHotel(){
		$accept_fields = array('hotel_id', 'title', 'cover_image', 'cover_image_before'
				, 'city_id', 'hotel_name_pinyin', 'hotel_name_english', 'type', 'recommend'
				, 'desc', 'html_content', 'phone', 'website', 'email', 'address', 'address_local', 'price'
				, 'rank', 'rating', 'intro', 'tags', 'open_time', 'traffic', 'lat', 'lon', 'area'
				, 'agoda_url', 'agoda_price', 'booking_url', 'booking_price');
		$data = array();
		$reset_cover_image = 0;
		if(isset($_POST['cover_image']) && isset($_POST['cover_image_before']) && $_POST['cover_image'] != $_POST['cover_image_before']){
			$reset_cover_image = 1;
		}
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			}
		}
		if(!empty($data['city_id'])) {
			$city = City::model()->findByPk($data['city_id']);
			if(!empty($city)) {
				$data['city_id'] = $data['city_id'];
				$data['city_name'] = $city['name'];
				$data['country_id'] = $city['country_id'];
			}
		}
		if(!empty($data['hotel_id'])) {
			$hotel = Hotel::model()->findByPk($data['hotel_id']);
			if(empty($hotel)) {
				ajax_response(404, '找不到酒店信息');
			}
			Hotel::model()->updateByPk($data['hotel_id'], $data);
			ajax_response(200, '');
		} else {
			$new_hotel = new Hotel;
			$new_hotel->city_id = $data['city_id'];
			$new_hotel->country_id = $data['country_id'];
			$new_hotel->city_name = $data['city_name'];
			$new_hotel->title = $data['title'];
			$new_hotel->type = $data['type'];
			$new_hotel->hotel_name_english = $data['hotel_name_english'];
			$new_hotel->hotel_name_pinyin = $data['hotel_name_pinyin'];
			$new_hotel->cover_image = $data['cover_image'];
			$new_hotel->desc = $data['desc'];
			$new_hotel->html_content = $data['html_content'];
			$new_hotel->phone = $data['phone'];
			$new_hotel->website = $data['website'];
			$new_hotel->address = $data['address'];
			$new_hotel->address_local = $data['address_local'];
			$new_hotel->price = $data['price'];
			$new_hotel->rank = $data['rank'];
			$new_hotel->rating = $data['rating'];
			$new_hotel->intro = $data['intro'];
			$new_hotel->tags = $data['tags'];
			$new_hotel->open_time = $data['open_time'];
			$new_hotel->traffic = $data['traffic'];
			$new_hotel->lat = $data['lat'];
			$new_hotel->lon = $data['lon'];
			$new_hotel->area = $data['area'];
			$new_hotel->recommend = $data['recommend'];
			$new_hotel->agoda_url = $data['agoda_url'];
			$new_hotel->agoda_price = $data['agoda_price'];
			$new_hotel->booking_url = $data['booking_url'];
			$new_hotel->booking_price = $data['booking_price'];
			$new_hotel->status = 0;
			$new_hotel->mtime = date('Y-m-d H:i:s', time());
			$new_hotel->ctime = date('Y-m-d H:i:s', time());

			if($new_hotel->save()) {
				$new_hotel_id = Yii::app()->db->getLastInsertId();
				ajax_response(200,'',array('new_hotel' => 'ok', 'hotel_id' => $new_hotel_id));
			} else {
				var_dump($new_hotel->getErrors());
				ajax_response(500,'添加失败');
			}
		}
	}
	
	public function actionDeleteHotel(){
		$hotel_id = Yii::app()->request->getParam('hotel_id');
		$hotel_db = Hotel::model()->findByPk($hotel_id);
		if(empty($hotel_db)) {
			ajax_response('404', '找不到酒店');
		}
		$hotel_db->status = -1;
		if($hotel_db->save()) {
			ajax_response('200', '');
		} else {
			ajax_response('500', '删除失败');
		}
	}
	
	private function _getTypeList($currentType = ''){
		$type_list_temp = array();
		$type_list_temp['hotel'] = '酒店';
		$type_list_temp['sight'] = '景点';
		$type_list_temp['restaurant'] = '美食';
		$type_list_temp['shopping'] = '购物';
		$type_list_temp['entertainment'] = '娱乐';
		$type_list_temp['poiline'] = '行程';
		$type_list = array();
		reset($type_list_temp);
		while (list($key, $val) = each($type_list_temp)) {
			$type = array();
			$type['type'] = $key;
			$type['name'] = $val;
			if($currentType == $key){
				$type['selected'] = ' selected';
			}else{
				$type['selected'] = '';
			}
			$type_list[] = $type;
		}
		return $type_list;
	}
	
	private function _getCityList($currentCity = 0){
		$city_list = array();
		$criteriaCity = new CDbCriteria;
		$criteriaCity->addCondition('status=1');
		$cities = City::model()->findAll($criteriaCity);
		if(!empty($cities)) {
			foreach($cities as $city){
				$city = $city->attributes;
				if($currentCity == $city['id']){
					$city['selected'] = ' selected';
				}else{
					$city['selected'] = '';
				}
				$city_list[] = $city;
			}
		}
		return $city_list;
	}

	private function _getImageIdFromURL($url){
		$return_value = "";
		if($url){
			if(preg_match("/^http:\/\/?[^\/]+\/([^\_]*)/i",$url, $matches)){
				$return_value = $matches[1];
			}
		}
		return $return_value;
	}


	public function actionCrawlingAgoda()
	{
		$url = Yii::app()->request->getParam('url');

		$refer = 'http://www.agoda.com/';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36');
		curl_setopt($ch, CURLOPT_REFERER,$refer);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		$r = curl_exec($ch);
		curl_close($ch);

		$hotel = array();
		//正则提前坐标，因为地图链接进了SimpleHtmlDOM就没了
		$parent = '/latitude=([0-9\.]+)&amp;longitude=([0-9\.]+)/i';
		if(preg_match($parent, $r, $matches)) {
			if(count($matches) == 3) {
				//aogda 这个鸟东西，好像把lat和lon弄反了？
				$hotel['lon'] = $matches[1];
				$hotel['lat'] = $matches[2];
			}
		}

		$simpleHTML = new SimpleHTMLDOM;
		$html = $simpleHTML->str_get_html($r);

		foreach($html->find('h1[itemprop=name] span') as $key=>$data) {
			if($key == 0) {
				$hotel['title'] = trim($data->innertext);
			} else if($key == 1){
				$hotel['hotel_name_english'] = substr($data->innertext, 2, -1);
			}

		}
		foreach($html->find('p[class=fontsmalli sblueboldunder]') as $data) {
			$address = $data->plaintext;
			if(!empty($address)) {
				$hotel['address'] =substr(trim($address), 0, -14);
			}
		}
		/*
		foreach($html->find('span[id=ctl00_ctl00_MainContent_ContentMain_HotelHeaderHD_lblHotelName]') as $data) {
			$hotel['title'] = $data->plaintext;
		}

		foreach($html->find('span[id=ctl00_ctl00_MainContent_ContentMain_HotelHeaderHD_lblEHotelName]') as $data) {
			$hotel['hotel_name_english'] = substr($data->plaintext, 2, -2);
		}
		 */

		foreach($html->find('li[id=ctl00_ctl00_MainContent_ContentMain_mainHotelPhotoHD_rLocation] span') as $data) {
			$hotel['area'] = $data->plaintext;
		}

		foreach($html->find('div[class=hotel_photos] img') as $data) {
			$hotel['cover_image'] = $data->src;
		}
		foreach($html->find('input[id=ctl00_ctl00_MainContent_AlternateHotelLis_hotelleftsearchbox_TextSearch1_txtSearch]') as $data) {
			$hotel['city_name'] = $data->value;
		}
		foreach($html->find('div[id=ctl00_ctl00_MainContent_ContentMain_HotelInformation1_pnlDescription] div') as $data) {
			$desc = preg_replace('/<br\\s*?\/??>/i', '', $data->innertext);
			$desc = strip_tags($desc);
			$hotel['desc'] = trim($desc);
		}
		ajax_response(200, '', $hotel);
	}
}
