<?php

Yii::import('ext.Notify',true);

class OrderController extends Controller
{
	private $_data;
	public $layout = '/layouts/column2';

	public function actionIndex()
	{
		$this->pageTitle = '代订列表';

		$order_list = array();
		$customer_id = Yii::app()->request->getParam('customer_id');
		$admin_id = Yii::app()->request->getParam('admin_id');

		if(!empty($customer_id)) {
			$customer = Customer::model()->findByPk($customer_id);
			if(!empty($customer)) {
				$customer = $customer->attributes;
				$this->_data['customer'] = $customer;
			}
		}
		
		$pageSize = 20;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;

		if(!empty($customer_id)) {
			$criteria->addCondition("customer_id=$customer_id");
		}
		if(!empty($admin_id)) {
			$criteria->addCondition("admin_id=$admin_id");
			$pageSize = 10000;
			$criteria->limit = 10000;
		}

		if(!isset($_GET['status'])) {
			$criteria->addCondition("status>=0");
		} else if($_GET['status'] == 0) {
			$criteria->addCondition("status=0");
		} else {
			$criteria->addCondition("status='{$_GET['status']}'");
		}

		$criteria->order = ' `ctime` DESC';
		$count = Order::model()->count($criteria);
		$orders = Order::model()->findAll($criteria);
		if(!empty($orders)) {
			foreach($orders as $order) {
				$order = $order->attributes;
				$customer = array();
				$customer = Customer::model()->findByPk($order['customer_id']);
				if(!empty($customer)) {
					$customer = $customer->attributes;
				}
				if(!empty($order['admin_id'])) {
					$admin_db = Admin::model()->findByPk($customer['admin_id']);
					if(!empty($admin_db)) {
						$customer['admin'] = $admin_db->attributes;
					}
				}
				$order['customer'] = $customer;
				if($order['status'] == 0) {
					$order['status'] = '等待付款';
				} else if($order['status'] == 1) {
					$order['status'] = '已付款';
				} else if($order['status'] == 2) {
					$order['status'] = '等待支付尾款';
				} else if($order['status'] == 5) {
					$order['status'] = '已完成';
				}
				$order_list[] = $order;
			}
		}
		$this->_data['order_list'] = $order_list;

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;


		//统计订单
		$sum = array('total_fee' => 0 , 'total_payment' => 0);
		if(!empty($order_list)) {
			foreach($order_list as $order) {
				$sum['total_fee'] += $order['total_fee'];
				$sum['total_payment'] += $order['payment'];
			}
			$sum['total_num'] = count($order_list);
			$this->_data['sum'] = $sum;
		}

		if(empty($admin_id) && empty($customer_id)) {
			$connection = Yii::app()->db;
			$sql = 'SELECT count(*) as total_num, SUM( total_fee ) as total_fee, SUM(payment) as total_payment FROM `order` WHERE status >= 0';
			$command = $connection->createCommand($sql);
			$sum = $command->queryRow();
			$this->_data['sum'] = $sum;
		}

		$this->render('index', $this->_data);
	}

	public function actionEdit()
	{
		$this->pageTitle = '编辑代订信息'; 

		$id = Yii::app()->request->getParam('id');
		$criteria = new CDbCriteria;
		$order = Order::model()->findByPk($id);
		if(empty($order)) {
			die('not exists');
		}
		$order = $order->attributes;
		$this->_data['order'] = $order;

		$customer_id = $order['customer_id'];
		if(!empty($customer_id)) {
			$customer = Customer::model()->findByPk($customer_id);
			if(!empty($customer)) {
				$customer = $customer->attributes;
				$this->_data['customer'] = $customer;
			}
		}

		$this->render('edit', $this->_data);
	}

	public function actionAdd()
	{
		$this->pageTitle = '添加代订信息'; 

		$customer_id = Yii::app()->request->getParam('customer_id');
		if(!empty($customer_id)) {
			$customer = Customer::model()->findByPk($customer_id);
			if(!empty($customer)) {
				$customer = $customer->attributes;
				$this->_data['customer'] = $customer;
			}
		}
		$this->render('edit', $this->_data);
	}

	public function actionSaveOrder()
	{
		$accept_fields = array('customer_id', 'type', 'title', 'channel', 'booking_type', 'desc', 'booking_url', 'balance_payment','remark', 'order_id','start_date', 'end_date','agent', 'source_url', 'payment_type', 'payment_path', 'total_fee', 'down_payment', 'payment_deadline','payment_trade_id', 'payout_trade_id','income', 'payment','payment_nick_name' ,'source', 'commission', 'status');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}
		if(!empty($data['order_id'])) {
			$order_id = $data['order_id'];
			unset($data['order_id']);
			$this->_save_order($order_id, $data);
		} else {
			$this->_new_order($data);
		}
	}

	private function _save_order($order_id, $data)
	{

		/************** for notify ******************************************/
		$customer  = Customer::model()->findByPk($data['customer_id']);
		$uid       = $customer->uid;
		/********************************************************************/


		$order_db = Order::model()->findByPk($order_id);
		if(empty($order_db)) {
			ajax_response('404', '找不到该代订信息，无法保存');
		}
		if(empty($order_db['admin_id']) && !empty(Yii::app()->adminUser->id)) {
			$data['admin_id'] = Yii::app()->adminUser->id;
		}
		
		Order::model()->updateByPk($order_id, $data);
		$this->_data['order_id'] = $order_id;

        //修改过了，重新取一次订单信息，用户同步
		$order_db = Order::model()->findByPk($order_id)->attributes;
		
		/************** for notify ******************************************/
	        $remote_server = "http://www.shijieyou.com";
            $channel       = "channel=" . $order_db['channel'];

            $booking_type  = "booking_type=" . $order_db['booking_type'];
            $booking_url   = "booking_ur=" . $order_db['booking_url'];
            $product_type  = "product_type=" . $order_db['type'];
            $product_title = "product_title=" . $order_db['title'];
            $product_desc  = "product_desc=" . $order_db['desc'];
            $start_date    = "start_date=" . $order_db['start_date'];
            $end_date      = "end_date=" . $order_db['end_date'];
            $pay_type      = "pay_type=" . $order_db['payment_type'];
            $total_fee     = "total_fee=" . $order_db['total_fee'];
            $trade_status  = "trade_status=" . $order_db['status'];
            $trade_id      = "trade_id=" . $order_id;
            $customer_id   = "customer_id=" . $order_db['customer_id'];
		    $deposit       = "deposit=" .  $order_db['down_payment'];
            $final         = "final=" . $order_db['balance_payment'];

		$real_data = "$channel&$booking_type&$booking_url&$product_type&$product_title&$product_desc&$start_date&$end_date&$pay_type&$total_fee&$trade_status&$trade_id&$deposit&$final&$customer_id";
		
		$post_data = "uid=$uid&act=update&obj=trade&$real_data";
				
		$notify = new Notify();
			
		$notify->call($remote_server, $post_data);
		/*************** end  notify *********************************************/



		ajax_response(200,'',$this->_data);
	}

	private function _new_order($data)
	{

		/************** for notify ******************************************/
		$customer  = Customer::model()->findByPk($data['customer_id']);
		$uid       = $customer->uid;
		/********************************************************************/
	

		$new_order = new Order;
		$new_order->customer_id = $data['customer_id'];
		$new_order->title = $data['title'];
		$new_order->channel = isset($data['channel']) ? $data['channel'] : '';
		$new_order->booking_type = isset($data['booking_type']) ? $data['booking_type'] : '';
		$new_order->booking_url= isset($data['booking_url']) ? $data['booking_url'] : '';
		$new_order->desc = isset($data['desc']) ? $data['desc'] : '';
		$new_order->remark = $data['remark'];
		$new_order->type = $data['type'];
		$new_order->start_date = isset($data['start_date']) ? $data['start_date'] : '';
		$new_order->end_date = isset($data['end_date']) ? $data['end_date'] : '';
		$new_order->agent = isset($data['agent']) ? $data['agent'] : '';
		$new_order->agent_url = isset($data['agent_url']) ? $data['agent_url'] : '';
		$new_order->total_fee = isset($data['total_fee']) ? $data['total_fee'] : '';
		$new_order->payment_path = isset($data['payment_path']) ? $data['payment_path'] : '';
		$new_order->payment_type = isset($data['payment_type']) ? $data['payment_type'] : '';
		$new_order->income = isset($data['income']) ? $data['income'] : 0;
		$new_order->payment = isset($data['payment']) ? $data['payment'] : 0;
		$new_order->commission = isset($data['commission']) ? $data['commission'] : 0;
		$new_order->down_payment = isset($data['down_payment']) ? $data['down_payment'] : '';
		$new_order->balance_payment = isset($data['balance_payment']) ? $data['balance_payment'] : '';
		$new_order->payment_deadline = isset($data['payment_deadline']) ? $data['payment_deadline'] : '';
		$new_order->payment_nick_name = isset($data['payment_nick_name']) ? $data['payment_nick_name'] : '';
		$new_order->payment_trade_id = isset($data['payment_trade_id']) ? $data['payment_trade_id'] : '';
		$new_order->payout_trade_id = isset($data['payout_trade_id']) ? $data['payout_trade_id'] : '';
		$new_order->status = isset($data['status']) ? $data['status'] : 0;

		if(!empty(Yii::app()->adminUser->id)) {
			$new_order->admin_id = Yii::app()->adminUser->id;
		}

		$new_order->mtime = date('Y-m-d H:i:s', time());
		$new_order->ctime = date('Y-m-d H:i:s', time());
		if($new_order->save()) {
			$this->_data['order_id'] = Yii::app()->db->getLastInsertId();

			/************** for notify ******************************************/
		        $remote_server = "http://www.shijieyou.com";

			$post_data = "uid=$uid&act=insert&obj=trade&";
			
			$channel       = "channel=$new_order->channel";
			
			$booking_type  = "booking_type=$new_order->booking_type";
			$booking_url   = "booking_url=$new_order->booking_url";
			$product_type  = "product_type=$new_order->type";
			$product_title = "product_title=$new_order->title";
			$product_desc  = "product_desc=$new_order->desc";
			$start_date    = "start_date=$new_order->start_date";
			$end_date      = "end_date=$new_order->end_date";
			$pay_type      = "pay_type=$new_order->payment_type";
			$total_fee     = "total_fee=$new_order->total_fee";
			$deposit       = "deposit=$new_order->down_payment";
			$final         = "final=$new_order->balance_payment";
			

			$trade_status  = "trade_status=$new_order->status";
			$trade_id      = "trade_id=$new_order->id";
            $customer_id   = "customer_id=" . $new_order->customer_id;

			$real_data = "$channel&$booking_type&$booking_url&$product_type&$product_title&$product_desc&$start_date&$end_date&$pay_type&$total_fee&$trade_status&$trade_id&$deposit&$final&$customer_id";
			$notify = new Notify();
			
			$notify->call($remote_server, "$post_data$real_data");
			/********************************************************************/
						

			ajax_response('200', '', $this->_data);
		} else {
			var_dump($new_order->getErrors());
			ajax_response('500', '保存失败');
		}
	}

	public function actionTaobao()
	{
		$service_fee_item_id = '35291955590';
		$agent_fee_item_id = '35456840040';

		//$access_token = Yii::app()->session['access_token'];

		$options_token = SystemOptions::model()->findByAttributes(array('key' => 'taobao_access_token'));
		if(empty($options_token)) {
			ajax_response('404', '找不到access token');
		}
		$options_token = $options_token->attributes;
		$access_token = $options_token['value'];

		$trades = Yii::app()->taobao->getTradesSold($access_token);
		if(empty($trades) || empty($trades['trades']) || empty($trades['trades']['trade'])) {
			ajax_response(404, '暂时没有新订单');
		}

		$trade_list = array();
		$trades = $trades['trades']['trade'];
		foreach($trades as $trade) {
			if($trade['status'] == 'TRADE_CLOSED' || $trade['status'] == 'TRADE_CLOSED_BY_TAOBAO') {
				continue;
			}
			$t = array(
				'buyer_nick' => $trade['buyer_nick'],
				'created' => $trade['created'],
				'modified' => $trade['modified'],
				'num_iid' => $trade['num_iid'],
				'payment' => $trade['payment'],
				'pay_time' => $trade['pay_time'],
				'total_fee' => $trade['payment'],
				'tid' => $trade['tid'],
				'status' => getTradeStatusName($trade['status']),
			);
			//如果是服务费
			if($trade['num_iid'] == $service_fee_item_id) {
				$trade_id = $trade['tid'];
				$customer = Customer::model()->findByAttributes(array('taobao_trade_id' => $trade_id));
				if(!empty($customer)) {
					$customer = $customer->attributes;
					$t['customer_id'] = $customer['id'];
				} else {
					$trade_list['service_fee'][] = $t;
				}
			} else {
				$trade_list['trades'][] = $t;
			}
		}
		//	echo json_encode($trade_list);
		ajax_response(200, '', $trade_list);
	}

	public function actionUpdateAlipayRecord()
	{
		$customer_id = Yii::app()->request->getParam('customer_id');
		$status = Yii::app()->request->getParam('status');
		$record_id = Yii::app()->request->getParam('record_id');

		$alipay_record = AlipayRecord::model()->findByPk($record_id);
		if(empty($alipay_record)) {
			ajax_response('404', '无法找到该交易记录');
		}

		if(!empty($customer_id)) {
			$alipay_record->customer_id = $customer_id;
		}
		if(!empty($status)) {
			$alipay_record->status = $status;
		}
		if($alipay_record->save()) {
			ajax_response('200', '');
		} else {
			ajax_response('500', '保存失败');
		}
	}

	public function actionAlipayRecord()
	{
		
		$this->layout = '/layouts/column1';
		$this->pageTitle = '支付宝交易管理';

		$record_list = array();
		$customer_id = Yii::app()->request->getParam('customer_id');
		if(!empty($customer_id)) {
			$customer = Customer::model()->findByPk($customer_id);
			if(!empty($customer)) {
				$customer = $customer->attributes;
				$this->_data['customer'] = $customer;
			}
		}

		//筛选参数
		$alipay_order_no = Yii::app()->request->getParam('alipay_order_no');
		$order_status = Yii::app()->request->getParam('order_status');
		$owner_user_id = Yii::app()->request->getParam('owner_user_id');
		$total_amount = Yii::app()->request->getParam('total_amount');

		$pageSize = 40;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		if(isset($_GET['customer_id']) && $_GET['customer_id'] != '') {
			$criteria->addCondition("customer_id='{$_GET['customer_id']}'");
		}
		if(!empty($alipay_order_no)) {
			$criteria->addCondition("alipay_order_no='$alipay_order_no'");
		}
		if(!empty($order_status)) {
			$criteria->addCondition("order_status='$order_status'");
		}
		if(!empty($owner_user_id)) {
			$criteria->addCondition("owner_user_id='$owner_user_id'");
		}
		if(!empty($total_amount)) {
			$criteria->addCondition("total_amount Like '$total_amount'");
		}
		$criteria->addCondition("status>=0");
		$criteria->order = ' `create_time` DESC';
		$count = AlipayRecord::model()->count($criteria);
		$records = AlipayRecord::model()->findAll($criteria);
		if(!empty($records)) {
			foreach($records as $record) {
				$record = $record->attributes;
				$record_list[] = $record;
			}
		}

		$this->_data['record_list'] = $record_list;

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;

		$this->render('alipay_record', $this->_data);
	}

	public function actionImportAlipayRecord()
	{
		$tmpFile = CUploadedFile::getInstanceByName('Filedata');//读取文件上传域,并使用系统上传组件上传 
		if(empty($tmpFile) || empty($tmpFile->tempName)) {
			ajax_response(500, '上传失败,文件为空');
		}


		$recordArray = array();
		$file = fopen($tmpFile->tempName, "r");
		while(!feof($file))
		{
			//$line = fgets($file);
			$line = stream_get_line($file, 65535, "\n");
			$line = iconv("gb2312","utf-8//IGNORE",$line);
			$recordArray[] = $line;
		}
		fclose($file);

		if(empty($recordArray)) {
			ajax_response('404', '没有记录需要导入');
		}
		$line_count = $insert_num = $update_num = 0;
		$update_array = $insert_array = array();
		$line_count = count($recordArray);
		for($i = 5; $i < $line_count - 5; $i++) {
			if(!empty($recordArray[$i])) {
				$record = explode(',', $recordArray[$i]);
				if(count($record) != 16) {
					echo count($record) . "\n";
					continue;
				}

				//查询是否已经存在
				$alipay_order_no = trim($record[0]);
				$alipayRecord = AlipayRecord::model()->findByAttributes(array('alipay_order_no' => $alipay_order_no));
				if(!empty($alipayRecord)) {
					if($alipayRecord->order_status != trim($record[11])) {
						$alipayRecord->order_status = trim($record[11]);
						$alipayRecord->payment_time = trim($record[3]);
						$alipayRecord->modified_time = trim($record[4]);

						$alipayRecord->save();
						$update_num++;
						$update_array[] = $alipay_order_no;
					}
				} else {
					$new_record = new AlipayRecord;
					$new_record->alipay_order_no = trim($record[0]);
					$new_record->merchant_order_no = trim($record[1]);
					$new_record->create_time = trim($record[2]);
					$new_record->payment_time = trim($record[3]);
					$new_record->modified_time = trim($record[4]);
					$new_record->order_from = trim($record[5]);
					$new_record->order_type = trim($record[6]);
					$new_record->owner_user_id = trim($record[7]);
					$new_record->order_title = trim($record[8]);
					$total_amount = trim($record[9]);
					$new_record->total_amount = !empty($total_amount) ? $total_amount : '0';
					$new_record->in_out_type = trim($record[10]);
					$new_record->order_status = trim($record[11]);
					$new_record->ctime = date('Y-m-d H:i:s', time());
					$new_record->save();

					$insert_num++;
					$insert_array[] = $alipay_order_no;
				}
			}
		}

		$update_ids_string = implode(';', $update_array);
		$insert_ids_string = implode(';', $insert_array);
		ajax_response('200', "导入成功, 新增加了$insert_num 条记录, $update_num 条记录更新状态. 更新ids：$update_ids_string . 插入ids：$insert_ids_string");
	}

	public function actionRecordCustomers()
	{
		$start_date = Yii::app()->request->getParam('start_date');
		$end_date = Yii::app()->request->getParam('end_date');

		if(empty($start_date)) {
			$start_date = date('Y-m-d', strtotime("-1 month"));
		}
		if(empty($end_date)) {
			$end_date = date('Y-m-d', time());
		}

		$criteria = new CDbCriteria;
		$criteria->offset = 0;
		$criteria->limit = 1000;
		$criteria->addCondition("modified_time <= '{$end_date}'");
		$criteria->addCondition("modified_time > '{$start_date}'");

		$criteria->order = 'modified_time desc';
		$criteria->distinct = true;
		$criteria->select = "customer_id";

		$customer_list = array();
		$r = AlipayRecord::model()->findAll($criteria);
		$order_balance_total = $alipay_balance_total = 0;
		if(!empty($r)) {
			foreach($r as $customer) {
				$customer = $customer->attributes;
				if(!empty($customer) && !empty($customer['customer_id'])) {
					$customer_db = Customer::model()->findByPk($customer['customer_id'])->attributes;
					if(!empty($customer_db)) {

						$customer_db['order_balance'] = $this->_orderBalance($customer_db);
						$customer_db['alipay_balance'] = $this->_alipayBalance($customer_db);

						$customer_db['admin'] = Admin::model()->findByPk($customer_db['admin_id'])->attributes;
						$customer_list[] = $customer_db;

						$order_balance_total += $customer_db['order_balance'];
						$alipay_balance_total += $customer_db['alipay_balance'];
					}
				}
			}
		}

		//echo json_encode($customer_list);
		$this->_data['customer_list'] = $customer_list;
		$this->_data['start_date'] = $start_date;
		$this->_data['end_date'] = $end_date;
		$this->_data['order_balance_total'] = $order_balance_total;
		$this->_data['alipay_balance_total'] = $alipay_balance_total;

		$this->render('record_customers', $this->_data);
	}


	private function _orderBalance($customer)
	{
		$order_margin = 0;
		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id=". $customer['id']);
		$criteria->addCondition("status>=0");
		$criteria->order = ' `ctime` DESC';
		$orders = Order::model()->findAll($criteria);
		if(!empty($orders)) {
			foreach($orders as $order) {
				$order = $order->attributes;

				$margin = 0;
				if($order['payment_path'] == 'alipay_peerpay')  {
					$margin = 0;
				} else {
					$margin = round(($order['total_fee'] - $order['payment']), 2);
				}
				$order['margin'] = $margin;
				$order_margin += $margin;
			}
		}

		//加上服务费
		$order_margin += $customer['service_fee'];
		return $order_margin;
	}

	private function _alipayBalance($customer)
	{
		//交易流水
		$record_margin = 0;
		$record_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id='{$customer['id']}'");
		$criteria->addCondition("status>=0");
		$criteria->order = ' `create_time` DESC';
		$count = AlipayRecord::model()->count($criteria);
		$records = AlipayRecord::model()->findAll($criteria);
		if(!empty($records)) {
			foreach($records as $record) {
				$record = $record->attributes;
				if($record['in_out_type'] == '收入') {
					$record_margin += $record['total_amount'];
				} else {
					$record_margin -= $record['total_amount'];
				}
				$record_list[] = $record;
			}
		}

		return  $record_margin;
	}

	public function actionBookingForm()
	{
		//$customer_id = Yii::app()->request->getParam('customer_id');
		$order_id = Yii::app()->request->getParam('order_id');

		$order = Order::model()->findByPk($order_id);
		if(empty($order)) {
			die('order not exists');
		}
		$order = $order->attributes;

		$customer_id = $order['customer_id'];
		$customer = Customer::model()->findByPk($customer_id);
		if(empty($customer)) {
			die('customer not exists');
		}

		$customer = $customer->attributes;

		$traveler_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id=". $customer['id']);
		$criteria->addCondition("status>=0");
		$criteria->order = ' `ctime` ASC';
		$travelers = Traveler::model()->findAll($criteria);
		if(!empty($travelers)) {
			foreach($travelers as $traveler) {
				$traveler = $traveler->attributes;
				$traveler_list[] = $traveler;
			}
		}

		$this->_data['customer'] = $customer;
		$this->_data['order'] = $order;
		$this->_data['traveler_list'] = $traveler_list;
		//ajax_response('200','', $this->_data);
		header('Content-type: text/html; charset=UTF-8');
		$this->renderPartial('booking_form', $this->_data);


	}

	public function actionWord()
	{

		Yii::import('ext.SimpleHTMLDOM.SimpleHTMLDOM');


		$html = file_get_contents('http://dev.quanxiaogou.com/admin/order/bookingForm?order_id=1000200');
		//echo $html;die();
		$PHPWord = new PHPWord();
		$section = $PHPWord->createSection();

		$simpleHTML = new SimpleHTMLDOM;
		$html_dom = $simpleHTML->str_get_html( $html);
		$html_dom_array = $html_dom->find('html',0)->children();

		$initial_state = array(
			// Required parameters:
			'phpword_object' => &$PHPWord, // Must be passed by reference.
			'base_root' => 'http://dev.quanxiaogou.com/', // Required for link elements - change it to your domain.
			'base_path' => '/htmltodocx/', // Path from base_root to whatever url your links are relative to.

			// Optional parameters - showing the defaults if you don't set anything:
			'current_style' => array('size' => '11'), // The PHPWord style on the top element - may be inherited by descendent elements.
			'parents' => array(0 => 'body'), // Our parent is body.
			'list_depth' => 0, // This is the current depth of any current list.
			'context' => 'section', // Possible values - section, footer or header.
			'pseudo_list' => TRUE, // NOTE: Word lists not yet supported (TRUE is the only option at present).
			'pseudo_list_indicator_font_name' => 'Wingdings', // Bullet indicator font.
			'pseudo_list_indicator_font_size' => '7', // Bullet indicator size.
			'pseudo_list_indicator_character' => 'l ', // Gives a circle bullet point with wingdings.
			'table_allowed' => TRUE, // Note, if you are adding this html into a PHPWord table you should set this to FALSE: tables cannot be nested in PHPWord.
			'treat_div_as_paragraph' => TRUE, // If set to TRUE, each new div will trigger a new line in the Word document.

			// Optional - no default:    
			'style_sheet' => htmltodocx_styles_example(), // This is an array (the "style sheet") - returned by htmltodocx_styles_example() here (in styles.inc) - see this function for an example of how to construct this array.
		);

		htmltodocx_insert_html($section, $html_dom_array[0]->nodes, $initial_state);
		$html_dom->clear(); 
		unset($html_dom);

		$h2d_file_uri = tempnam('', 'htd');
		$objWriter = PHPWord_IOFactory::createWriter($PHPWord, 'Word2007');

		$objWriter->save($h2d_file_uri);

		header('Content-Description: File Transfer');
		header('Content-Type: application/octet-stream');
		header('Content-Disposition: attachment; filename=example.docx');
		header('Content-Transfer-Encoding: binary');
		header('Expires: 0');
		header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
		header('Pragma: public');
		header('Content-Length: ' . filesize($h2d_file_uri));
		ob_clean();
		flush();
		$status = readfile($h2d_file_uri);
		unlink($h2d_file_uri);
		exit;



	
	}

	public function actionBookingVoucher()
	{


	}

}
