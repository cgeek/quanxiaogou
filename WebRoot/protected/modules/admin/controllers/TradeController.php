<?php

class TradeController extends Controller
{
	private $_data;
	public $layout = '/layouts/column1';

	public function actionIndex()
    {

        $api = 'http://www.shijieyou.com/api/tradelist';

        $offset = Yii::app()->request->getParam('offset');
        $status = Yii::app()->request->getParam('status');
        $keyword = Yii::app()->request->getParam('keyword');


	if($status == 'remind') {
            $api .= "?offset=$offset&type=$status&keyword=$keyword";
	} else {
            $api .= "?offset=$offset&status=$status&keyword=$keyword";
	}

        $this->_data['status'] = $status;
        $this->_data['keyword'] = $keyword;
        //echo $api;die();
        $this->pageTitle = '世界游订单列表';
        $r = file_get_contents($api);
        $r = json_decode($r, true);

        $trade_list = $r['tradeList'];

        $this->_data['trade_list'] = $trade_list;
        if(isset($_GET['json'])) {
			ajax_response(200, '', $this->_data);
		}
		
		if(isMobile()) {
			 $this->render('index_m', $this->_data);
		} else {
			$this->render('index', $this->_data);	
		}
        

    }

    public function actionDetail()
    {
		
        $id = Yii::app()->request->getParam('id');
        $api = 'http://www.shijieyou.com/api/tradedetail/'.$id;

        $this->pageTitle = '世界游订单详情';
        $r = file_get_contents($api);
        $r = json_decode($r, true);
        if(empty($r)) {
            die();
        }

        $this->_data['trade'] = $r;
		/*
        $id = Yii::app()->request->getParam('id');
        //$api = 'http://www.shijieyou.com/api/tradedetail/'.$id;
		$api = 'http://beta.json-generator.com/api/json/get/Hesh4hl';

        $this->pageTitle = '世界游订单详情';
        $r = file_get_contents($api);
        $r = json_decode($r, true);
        if(empty($r)) {
            die();
        }
		//echo json_encode($r['data']);die();
        $this->_data['trade'] = $r['data']['trade'];
		*/
			
        $admin_id = Yii::app()->adminUser->id;
        $admin = Admin::model()->findByPk($admin_id);
        if(!empty($admin)) {
            $admin = $admin->attributes;
            $this->_data['admin'] = $admin;
        }
        if(isset($_GET['json'])) {
			ajax_response(200, '', $this->_data);
		}
        $this->render('detail', $this->_data);
    }

    public function actionUpdateStatus()
    {
        $item_id = Yii::app()->request->getParam('item_id');
        $trade_id = Yii::app()->request->getParam('trade_id');
        $status = Yii::app()->request->getParam('status');
        if(empty($item_id) || empty($trade_id) || empty($status)) {
            ajax_response('200', '参数不正确');
        }

        $api = "http://www.shijieyou.com/api/changeitemstatus?tradeId=$trade_id&itemId=$item_id&status=$status";
        $r = file_get_contents($api);
        $r = json_decode($r, true);
        if($r['success'] == true) {
            ajax_response('200', '修改成功');
        }
        
        ajax_response('500', '状态修改失败' . $r['msg']);
    }
	
	public function actionClaimTrade()
	{
        $trade_id = Yii::app()->request->getParam('trade_id');
        //$claimer = Yii::app()->request->getParam('claimer');
		
		$admin_id = Yii::app()->adminUser->id;
        $admin = Admin::model()->findByPk($admin_id);
		
		$claimer = '';
        if(!empty($admin)) {
            $admin = $admin->attributes;
            $claimer = $admin['username'];
        }
		
        if(empty($trade_id) || empty($claimer)) {
            ajax_response('400', '参数不正确');
        }

        $api = "http://www.shijieyou.com/api/claimTrade?tradeId=$trade_id&claimer=$claimer";
        $r = file_get_contents($api);
        $r = json_decode($r, true);
        if($r['success'] == true) {
            ajax_response('200', '修改成功');
        }
        
        ajax_response('500', '状态修改失败' . $r['msg']);
		
	}

    public function actionUpdateTravelInfo()
    {
        $type = Yii::app()->request->getParam('infoType');
        $id = Yii::app()->request->getParam('pk');
        $value = Yii::app()->request->getParam('value');
        $name = Yii::app()->request->getParam('name');

        if(empty($type) || empty($id) || empty($name)|| empty($value)) {
            ajax_response('200', '参数不正确');
        }
        $value=urlencode($value);
        $api = "http://www.shijieyou.com/api/updateInfo?type=$type&id=$id&name=$name&value=$value";
        $r = file_get_contents($api);
        $r = json_decode($r, true);
        if($r['success'] == true) {
            ajax_response('200', '修改成功');
        } else {
            echo "update error";
            die();
        }

    }

    public function actionSaveNote()
    {
        $username = Yii::app()->request->getParam('username');
        $tradeId = Yii::app()->request->getParam('tradeId');
        $content = Yii::app()->request->getParam('content');

        if(empty($content)) {
            ajax_response('200', '备注内容不能为空');
        }
        $content = urlencode($content);
        $api = "http://www.shijieyou.com/api/saveNote?username=$username&tradeId=$tradeId&content=$content";
        $r = file_get_contents($api);
        $r = json_decode($r, true);
        if($r['success'] == true) {
            ajax_response('200', '保存成功');
        } else {
            echo "update error";
            die();
        }

    }

    public function actionUpdateRemindStatus()
    {
        $item_id = Yii::app()->request->getParam('item_id');
        $trade_id = Yii::app()->request->getParam('trade_id');
        $status = Yii::app()->request->getParam('status');
        if(empty($item_id) || empty($trade_id) ) {
            ajax_response('200', '参数不正确');
        }
        $api = "http://www.shijieyou.com/api/changeItemsRemindStatus?tradeId=$trade_id&itemId=$item_id&status=$status";
        $r = file_get_contents($api);
        $r = json_decode($r, true);
        if($r['success'] == true) {
            ajax_response('200', '修改成功');
        }
        
        ajax_response('500', '状态修改失败' . $r['msg']);
    }
	
    public function actionUploadFile()
    {
        $item_id = Yii::app()->request->getParam('itemId');
        if(empty($item_id)) {
            ajax_response(404, '目标商品为空');
        }
        $tmpFile = CUploadedFile::getInstanceByName('file-to-upload');
        if(empty($tmpFile)) {
            ajax_response(400, '上传文件为空');
        }
        $fileName = $tmpFile->name;
        $file = '/tmp/' . $fileName;
        $tmpFile->saveAs($file);

        $ch = curl_init();
        $data = array('file'=>'@' . $file, 'lineItemId'=>$item_id);
        curl_setopt($ch, CURLOPT_URL,'http://www.shijieyou.com/api/uploadFile');
        curl_setopt($ch, CURLOPT_POST,true);
        curl_setopt($ch, CURLOPT_POSTFIELDS,$data);
        curl_setopt($ch, CURLOPT_INFILESIZE,filesize($file));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $content = curl_exec($ch);
        curl_close($ch);
        unlink($file);
        $content = json_decode($content, true);
        ajax_response(200, '', $content['data']);
    }

}
