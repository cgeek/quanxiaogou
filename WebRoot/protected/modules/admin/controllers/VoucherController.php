<?php

class VoucherController extends Controller
{
	private $_data;
	public $layout = '/layouts/column1';

	public function actionIndex()
	{
		
	}

	public function actionEdit()
	{
		$id = Yii::app()->request->getParam('id');
		$tradeId = Yii::app()->request->getParam('tradeId');
		$itemId = Yii::app()->request->getParam('itemId');

		$templates = VoucherTemplate::model()->findAll();
		$template_list = array();
		if(!empty($templates)) {
			foreach($templates as $template) {
				$template = $template->attributes;
				if(!empty($template['item_ids'])) {
					$id_array = explode(',', $template['item_ids']);
					if(in_array($itemId, $id_array)) {
						$template['selected'] = true;
					}
				}
				$template_list[]  = $template;
			}
		}
		$this->_data['template_list'] = $template_list;

		if(!empty($tradeId)) {
			$this->_data['trade_id'] = $tradeId;
		}
		$voucher = Voucher::model()->findByPk($id);
		if(!empty($voucher)) {
			$params = json_decode($voucher->service_info, true);
			$this->_data['params'] = $params;
			$trade_info = json_decode($voucher->trade_info, true);
			$this->_data['order'] = $trade_info;
			$this->_data['trade_id'] = $voucher->trade_id;
			$this->_data['voucher'] = $voucher->attributes;
		}
		$this->renderPartial('edit', $this->_data);
	}

	public function actionView()
	{
		$id = Yii::app()->request->getParam('id');
		$voucher = Voucher::model()->findByPk($id);
		if(!empty($voucher)) {
			$params = json_decode($voucher->service_info, true);
			$this->_data['params'] = $params;
			$trade_info = json_decode($voucher->trade_info, true);
			$this->_data['order'] = $trade_info;
		}
		$this->renderPartial('view', $this->_data);
	}

	public function actionTemplate()
	{
		$trade_id = Yii::app()->request->getParam('tradeId');
		if(!empty($trade_id)) {
			$voucher = Voucher::model()->findByAttributes(array('trade_id'=>$trade_id));
		}
		if(!empty($voucher)) {
			$params = json_decode($voucher->service_info, true);
			$this->_data['params'] = $params;
			$trade_info = json_decode($voucher->trade_info, true);
			$this->_data['order'] = $trade_info;
		} else {
			$templateId = Yii::app()->request->getParam('templateId');
			$template = VoucherTemplate::model()->findByPk($templateId);
			$params = json_decode($template->params, true);
			$params['title'] = $template->title;
			$this->_data['id'] = Yii::app()->request->getParam('id');
			$this->_data['params'] = $params;
			$this->_data['order'] = array('trade_id'=>123456, 'contact_name'=>'喵喵', 'mobile' => '18626880110', 'num' => '成人2人', 'hotel_name' => 'xxx酒店', 'hotel_address' => 'xxx酒店地址', 'start_date' => '2015-09-22');
		}
		$this->renderPartial('template', $this->_data);
	}

	public function actionCreate()
	{
		$trade_id = Yii::app()->request->getParam('tradeId');
		$templateId = Yii::app()->request->getParam('templateId');
		$template = VoucherTemplate::model()->findByPK($templateId);
		$new_voucher = new Voucher();
		$new_voucher->trade_id = $trade_id;

		$trade_info = array('trade_id'=>'', 'contact_name'=> '', 'mobile'=>'', 'num'=>'', 'hotel'=> '', 'start_date' => '');
		if(!empty($trade_id)) {
			$trade = file_get_contents('http://www.shijieyou.com/api/tradedetail/' . $trade_id);
			$trade = json_decode($trade, true);
			if(!empty($trade) && !empty($trade['itemList'][0])) {
				$trade_info['trade_id'] = $trade['id'];
				$trade_info['mobile'] = $trade['itemList'][0]['touristInfo'][0]['touristMobile'];
				$trade_info['contact_name'] = $trade['itemList'][0]['touristInfo'][0]['touristName'];
				$trade_info['num'] = $trade['itemList'][0]['ageSet'];
				$trade_info['start_date'] = date('Y-m-d', strtotime($trade['itemList'][0]['startTime']));
				$hotel = '';
				if(!empty($trade['itemList'][0]['hotelInfo']['hotelName'])) {
					$hotel .= "酒店名称：" . $trade['itemList'][0]['hotelInfo']['hotelName'] . "\r" . "酒店地址：" . $trade['itemList'][0]['hotelInfo']['hotelAddress'];
				}
				$trade_info['hotel'] = $hotel;
			}
		}

		$params = array('title'=>'', 'contact_info'=>'', 'title'=>'', 'fee_desc'=>'', 'remark'=>'', 'assembling_place'=>'');
		if(!empty($template)) {
			$params = json_decode($template->params, true);
			$params['title'] = $template->title;
		}
		$new_voucher->trade_info = json_encode($trade_info);
		$new_voucher->service_info = json_encode($params);
		$new_voucher->mtime = date('Y-m-d H:i:s', true);
		$new_voucher->ctime = date('Y-m-d H:i:s', true);
		if($new_voucher->save()) {
			$new_id = Yii::app()->db->getLastInsertId();
			ajax_response(200,'',array('id' => $new_id));
		} else {
			var_dump($new_voucher->getErrors());
		}
	}

	public function actionUpdateInfo()
	{
		$name = Yii::app()->request->getParam('name', true);
		$value = Yii::app()->request->getParam('value', true);
		$type = Yii::app()->request->getParam('type', true);
		$pk = Yii::app()->request->getParam('pk', true);

		$voucher = Voucher::model()->findByPk($pk);
		if(empty($voucher)) {
			ajax_response(404, '找不到确认单');
		}
		$update_trade_info = array();
		$ar = array('trade_id', 'contact_name', 'mobile', 'num', 'hotel', 'start_date');
		foreach($ar as $v) {
			if($name == $v) {
				$update_trade_info[$v] = $value;
			}
		}
		$update_params= array();
		$ar = array('contact_info', 'title', 'fee_desc', 'remark', 'assembling_place');
		foreach($ar as $v) {
			if($name == $v) {
				$update_params[$v] = $value;
			}
		}
		$params = json_decode($voucher->service_info, true);
		if(!empty($params)) {
			foreach($params as $key => $val) {
				if(isset($update_params[$key])) {
					$params[$key] = $update_params[$key];
				}
			}
		}
		$trade_info = json_decode($voucher->trade_info, true);
		if(!empty($update_trade_info)) {
			foreach($trade_info as $key => $val) {
				if(isset($update_trade_info[$key])) {
					$trade_info[$key] = $update_trade_info[$key];
				}
			}
		}

		$voucher->service_info = json_encode($params);
		$voucher->trade_info = json_encode($trade_info);

		if($voucher->save()) {
			ajax_response(200, '');
		} else {
			ajax_response(500, '');
		}

	}

	public function actionTemplateSave()
	{
		$id = Yii::app()->request->getParam('id');

		$title = Yii::app()->request->getParam('title', true);
		$item_ids = Yii::app()->request->getParam('item_ids', true);
		$params['contact_info']= Yii::app()->request->getParam('contact_info');
		$params['assembling_place']= Yii::app()->request->getParam('assembling_place');
		$params['fee_desc']= Yii::app()->request->getParam('fee_desc');
		$params['remark']= Yii::app()->request->getParam('remark');

		if(!empty($id)) {

			$template = VoucherTemplate::model()->findByPk($id);

			if(empty($template)) {
				ajax_response(500, '找不到酒店');
			}
			$template->title = $title;
			$template->item_ids = $item_ids;
			$template->params = json_encode($params);
			if(!$template->save()) {
				var_dump($template->getErrors());
			}
			$this->redirect('/admin/voucher/templateList');
			//ajax_response(200,'',array('id' => $id));
		} else {
			$template_new = new VoucherTemplate();
			$template_new->title = $title;
			$template->item_ids = $item_ids;
			$template_new->params = json_encode($params);
			$template_new->mtime = date('Y-m-d H:i:s');
			if(!$template_new->save()) {
				var_dump($template_new->getErrors());
			}
			$new_id = Yii::app()->db->getLastInsertId();
			//ajax_response(200,'',array('id' => $new_id));
			$this->redirect('/admin/voucher/templateList');
		}

	}

	public function actionTemplateList()
	{
		$this->layout = '/layouts/column2';
		$this->pageTitle = '模板列表';
		
		$template_list = array();
		$pageSize = 20;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$offset = ($page - 1) * $pageSize;
		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;

		$criteria->order = ' `mtime` DESC';
		$count = VoucherTemplate::model()->count($criteria);
		$templates = VoucherTemplate::model()->findAll($criteria);
		if(!empty($templates)) {
			foreach($templates as $template) {
				$template = $template->attributes;
				$template_list[] = $template;
			}
		}
		$this->_data['template_list'] = $template_list;
		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;


		$this->render('template_list', $this->_data);
	}

	public function actionTemplateEdit()
	{
		$this->layout = '/layouts/column2';
		$this->pageTitle = '模板管理';

		$id = Yii::app()->request->getParam('id');
		if(!empty($id)) {
			$template = VoucherTemplate::model()->findByPk($id);
			if(empty($template)) {
				ajax_response(500, '找不到确认单');
			}
			$template = $template->attributes;
			$this->_data['params'] = json_decode($template['params'], true);
			$this->_data['template'] = $template;
		}
		//ajax_response($this->_data);
		$this->render('template_edit', $this->_data);
	}
}
