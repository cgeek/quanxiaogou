<?php
Yii::import('ext.SimpleHTMLDOM.SimpleHTMLDOM');

class PoiController extends Controller{
	private $_data;

	public $layout = '/layouts/column2';

	public function actionIndex(){
		$this->pageTitle = 'POI';
		
		//base params 
		$type = Yii::app()->request->getParam('type');
		$page = Yii::app()->request->getParam('page');
		$city_id = isset($_GET['city']) ? intval($_GET['city']) : 0;
		
		$poi_list = array();
		
		$pageSize = 20;
		$page = !empty($page) ? intval($page) : 1;
		$status = Yii::app()->request->getParam('status');
		$status = empty($status) ? 0 : $status;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		$criteria->addCondition("status=$status");
		if(!empty($type)) {
			$criteria->addCondition("type='{$type}'");
		}
		if($city_id){
			$criteria->addCondition("city_id=$city_id");
		}
		
		if(isset($_GET['filter_key']) && $_GET['filter_key']!=''){
			$criteria->addCondition("(`poi_name` like '%{$_GET['filter_key']}%'"
				. " OR `desc` like '%{$_GET['filter_key']}%')");
			$this->_data['filter_key'] = $_GET['filter_key'];
		}else{
			$this->_data['filter_key'] = '';
		}

		$criteria->order = ' `ctime` DESC';
		
		$count = Poi::model()->count($criteria);
		$pois = Poi::model()->findAll($criteria);
		if(!empty($pois)) {
			foreach($pois as $poi) {
				$poi = $poi->attributes;
				$poi['cover_image_url'] = upimage($poi['cover_image']);
				$poi['cover_image'] = $this->_getImageIdFromURL($poi['cover_image']);
				$poi_list[] = $poi;
			}
		}
		$this->_data['poi_list'] = $poi_list;
		
		$city_list = $this->_getCityList($city_id);
		$this->_data['city_list'] = $city_list;
		
		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;

		$this->render('list', $this->_data);
	}
	
	public function actionNew(){
		$this->pageTitle = '新增POI';
		
		$poi = array();
		$poi['poi_id'] = 0;
		$this->_data['poi'] = $poi;

		$city_list = $this->_getCityList();
		$this->_data['city_list'] = $city_list;
		
		$type_list = $this->_getTypeList();
		$this->_data['type_list'] = $type_list;
		
		$this->render('edit', $this->_data);
	}
	
	public function actionEdit(){
		$this->pageTitle = '编辑POI';
		$poi_id = Yii::app()->request->getParam('poi_id');
		
		$poi = Poi::model()->findByPk($poi_id);
		if(empty($poi)) {
			throw new CHttpException('404', '找不到POI');
		}
		
		$poi = $poi->attributes;
		
		$poi['cover_image_url'] = $poi['cover_image'];
		$poi['cover_image'] = $this->_getImageIdFromURL($poi['cover_image']);
		//get images
		
		$this->_data['poi'] = $poi;
		
		$city_list = $this->_getCityList($poi['city_id']);
		$this->_data['city_list'] = $city_list;
		
		$type_list = $this->_getTypeList($poi['type']);
		$this->_data['type_list'] = $type_list;
		
		$this->render('edit', $this->_data);
	}
	
	public function actionSavePoi(){
		$accept_fields = array('poi_id', 'poi_name', 'cover_image', 'cover_image_before'
				, 'city_id','poi_name_english', 'poi_name_pinyin', 'poi_name_local', 'type', 'recommend'
				, 'desc', 'html_content', 'phone', 'website', 'address', 'address_local', 'price', 'tips'
				, 'rank', 'rating', 'intro', 'tags', 'open_time', 'traffic', 'is_private', 'source_url', 'lat', 'lon', 'area');
		$data = array();
		$reset_cover_image = 0;
		if(isset($_POST['cover_image']) && isset($_POST['cover_image_before']) && $_POST['cover_image'] != $_POST['cover_image_before']){
			$reset_cover_image = 1;
		}
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			}
		}
		if(!isset($data['is_private']) || empty($data['is_private']))
		{
			$data['is_private'] = 0;
		}
		if(!empty($data['city_id'])) {
			$city = City::model()->findByPk($data['city_id']);
			if(!empty($city)) {
				$data['city_id'] = $data['city_id'];
				$data['city_name'] = $city['name'];
				$data['country_id'] = $city['country_id'];
			}
		}
		if(!empty($data['poi_id'])) {
			$poi = Poi::model()->findByPk($data['poi_id']);
			if(empty($poi)) {
				ajax_response(404, '找不到POI信息');
			}
			Poi::model()->updateByPk($data['poi_id'], $data);
			ajax_response(200, '');
		} else {
			$new_poi = new Poi;
			$new_poi->city_id = $data['city_id'];
			$new_poi->country_id = $data['country_id'];
			$new_poi->city_name = $data['city_name'];
			$new_poi->poi_name = $data['poi_name'];
			$new_poi->type = $data['type'];
			$new_poi->poi_name_local = $data['poi_name_local'];
			$new_poi->poi_name_pinyin = $data['poi_name_pinyin'];
			$new_poi->poi_name_english = $data['poi_name_english'];
			$new_poi->cover_image = $data['cover_image'];
			$new_poi->desc = $data['desc'];
			$new_poi->html_content = $data['html_content'];
			$new_poi->phone = $data['phone'];
			$new_poi->website = $data['website'];
			$new_poi->address = $data['address'];
			$new_poi->address_local = $data['address_local'];
			$new_poi->price = $data['price'];
			$new_poi->rank = $data['rank'];
			$new_poi->rating = $data['rating'];
			$new_poi->intro = $data['intro'];
			$new_poi->tips = $data['tips'];
			$new_poi->tags = $data['tags'];
			$new_poi->open_time = $data['open_time'];
			$new_poi->traffic = $data['traffic'];
			$new_poi->lat = $data['lat'];
			$new_poi->lon = $data['lon'];
			$new_poi->area = $data['area'];
			$new_poi->recommend = $data['recommend'];
			$new_poi->source_url = $data['source_url'];
			$new_poi->status = 0;
			$new_poi->mtime = date('Y-m-d H:i:s', time());
			$new_poi->ctime = date('Y-m-d H:i:s', time());

			if($new_poi->save()) {
				$new_poi_id = Yii::app()->db->getLastInsertId();
				ajax_response(200,'',array('new_poi' => 'ok', 'poi_id' => $new_poi_id));
			} else {
				var_dump($new_poi->getErrors());
				ajax_response(500,'添加失败');
			}
		}
	}
	
	public function actionDeletePoi(){
		$poi_id = Yii::app()->request->getParam('poi_id');
		$poi_db = Poi::model()->findByPk($poi_id);
		if(empty($poi_db)) {
			ajax_response('404', '找不到POI');
		}
		$poi_db->status = -1;
		if($poi_db->save()) {
			ajax_response('200', '');
		} else {
			ajax_response('500', '删除失败');
		}
	}
	
	private function _getCityList($currentCity = 0){
		$city_list = array();
		$criteriaCity = new CDbCriteria;
		$criteriaCity->addCondition("status=1");
		$cities = City::model()->findAll($criteriaCity);
		if(!empty($cities)) {
			foreach($cities as $city){
				$city = $city->attributes;
				if($currentCity == $city['id']){
					$city['selected'] = ' selected';
				}else{
					$city['selected'] = '';
				}
				$city_list[] = $city;
			}
		}
		return $city_list;
	}
	
	private function _getTypeList($currentType = ''){
		if(empty($currentType))
		{
			$currentType = 'sight';
		}
		$type_list_temp = array();
		//$type_list_temp['hotel'] = '酒店';
		$type_list_temp['sight'] = '景点';
		$type_list_temp['restaurant'] = '美食';
		$type_list_temp['shopping'] = '购物';
		$type_list_temp['entertainment'] = '娱乐';
		$type_list_temp['traffic'] = '交通';
		$type_list = array();
		reset($type_list_temp);
		while (list($key, $val) = each($type_list_temp)) {
			$type = array();
			$type['type'] = $key;
			$type['name'] = $val;
			if($currentType == $key){
				$type['selected'] = ' selected';
			}else{
				$type['selected'] = '';
			}
			$type_list[] = $type;
		}
		return $type_list;
	}
	
	private function _getImageIdFromURL($url){
		$return_value = $url;
		//如果是妙妙行程管家下面的截取image_id
		if(strpos($url, 'http://xiaogoulvxing.b0.upaiyun.com/')){
			if(preg_match("/^http:\/\/?[^\/]+\/([^\_]*)/i",$url, $matches)){
				$return_value = $matches[1];
			}
		}
		return $return_value;
	}

	public function actionCrawlingPoi()
	{
		$url = Yii::app()->request->getParam('url');

		preg_match('/[\w][\w-]*\.(?:com\.cn|com|cn|co|net|org|gov|cc|biz|info)(\/|$)/isU', $url, $domain);
		$domain = rtrim($domain[0], '/');

		if($domain == 'qyer.com') {
			$refer = 'http://place.qyer.com/';
			$this->_crawlingQyer($url, $refer);
		} elseif($domain == 'qiugonglue.com') {
			$pin_id = 0;
			$reg = "/pin[\/\-](\d*)/i";
			if(preg_match($reg, $url, $matches)) {
				$pin_id = $matches[1];
			}
			if($pin_id <= 0) {
				ajax_response('400', '页面不支持');
			}
			$this->_crawlingQGL($pin_id);
		} else {
			ajax_response('400', '目前还不支持该网站，如果需要，请联系喵喵添加');
		}
	}

	private function _curl($url, $refer) 
	{
		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL, $url);
		curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 5.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/30.0.1599.101 Safari/537.36');
		curl_setopt($ch, CURLOPT_REFERER,$refer);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1); 
		$r = curl_exec($ch);
		curl_close($ch);

		return $r;
	}

	//妙妙行程管家和求攻略之间导入接口
	public function _crawlingQGL($pin_id)
	{
		$url = 'http://www.qiugonglue.com/api/v2/xiaogou/pin?pin_id=' . $pin_id;
		$refer = 'http://www.qiugonglue.com';

		$r = $this->_curl($url, $refer);

		$r = json_decode($r, true);
		//var_dump($r);die();
		//echo $r;
		ajax_response(200, '', $r['data']);
	}

	private function _crawlingQyer($url, $refer)
	{
		$r = $this->_curl($url, $refer);

		$poi = array();
		$simpleHTML = new SimpleHTMLDOM;
		$html = $simpleHTML->str_get_html($r);

		foreach($html->find('p[class="pl_topbox_en"] a') as $data) {
			$poi['poi_name_english'] = $data->plaintext;
		}
		foreach($html->find('p[class="pl_topbox_cn"] a') as $data) {
			$poi['poi_name'] = $data->plaintext;
		}
		foreach($html->find('div[id="summary_box"]') as $data) {
			$poi['desc'] = $data->plaintext;
		}
		//poi属性字段转换
		$key_filter = array('地址：' => 'address', '到达方式：' => 'traffic', '开放时间：'=>'open_time', 
			'门票：'=>'price','电话：'=>'phone', '网址：'=>'website', '所属分类：'=>'tags','营业时间：' => 'open_time');
		foreach($html->find('ul[class="pla_textdetail_list"] li') as $data) {
			$key = trim($data->children(0)->plaintext);
			if(isset($key_filter[$key])) {
				$key = $key_filter[$key];
				$val = $data->children(1)->plaintext;
				$poi[$key] = trim($val);
			}
		}
		foreach($html->find('div[class="pla_textpage_tips"] div[class="texts fontSong"]') as $data) {
			$poi['tips'] = $data->plaintext;
		}
		//获取坐标
		foreach($html->find('div[class="pla_sidemap"] img') as $data) {
			$lat_lon = $data->src;
			$rule = '/[0-9]+\.[0-9]+/';
			preg_match_all($rule, $lat_lon, $matches);
			if(count($matches[0]) == 2) {
				$poi['lat'] = $matches[0][0];
				$poi['lon'] = $matches[0][1];
			}
		}
		//cover image
		foreach($html->find('div[class="pla_sidephoto"] img') as $data) {
			$src = $data->src;
			if(!empty($src)) {
				$poi['cover_image'] = $src;
			}
		}
		ajax_response(200, '', $poi);
	}

	public function actionGetCrawlingList()
	{
		$url = Yii::app()->request->getParam('url');
		
		$r = $this->_curl($url, $refer);

		$poi = array();
		$simpleHTML = new SimpleHTMLDOM;
		$html = $simpleHTML->str_get_html($r);

		foreach($html->find('ul[class="pla_listpage_poilist"] li') as $data) {
			$title = $data->find('h3')->plaintext;

			var_dump($title);
		}
		
		

	}

}
