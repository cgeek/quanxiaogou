<?php

Yii::import('ext.Notify',true);

class CustomerController extends Controller
{
	private $_data;
	public $layout = '/layouts/column2';

	public function actionIndex()
	{
		$this->layout = '/layouts/column1';
		$this->pageTitle = '我的客户';
		$customer_list = array();

		$pageSize = 20;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		$criteria->order = 'mtime desc';

		$current_admin_id = Yii::app()->adminUser->id;
		$criteria->addCondition("admin_id={$current_admin_id}");

		if(isset($_GET['status']) && $_GET['status']!='') {
			$criteria->addCondition("status = {$_GET['status']}");
        } elseif(!isset($_GET['filter_key']) || empty($_GET['filter_key'])) {
            $criteria->addCondition("status!= -1");
            $criteria->addCondition("status!= 6");
		}

		if(isset($_GET['now']) && $_GET['now']!='') {
			if($_GET['now'] == 'going') {
				$criteria->order = 'start_date asc';

				$next_7_days = date("Y-m-d",strtotime("+1 month"));
				$day  = date("Y-m-d",time());
				$criteria->addBetweenCondition("start_date", $day, $next_7_days, 'AND');
				
			} else if($_GET['now'] == 'in') {
				$criteria->order = 'start_date desc';

				$day  = date("Y-m-d",time());
				$criteria->addCondition("start_date < '{$day}'");
				$criteria->addCondition("end_date > '{$day}'");

			} else if($_GET['now'] == 'return') {
				$criteria->order = 'end_date desc';

				$pre_7_days = date("Y-m-d",strtotime("-1 month"));
				$day  = date("Y-m-d",time());
				$criteria->addBetweenCondition("end_date", $pre_7_days, $day, 'AND');
			}		

		}

		if(isset($_GET['filter_key']) && $_GET['filter_key']!=''){
			$criteria->addCondition("(contact like '%{$_GET['filter_key']}%'"
				. " OR qq like '%{$_GET['filter_key']}%'"
				. " OR uid like '%{$_GET['filter_key']}%'"				
				. " OR wangwang like '%{$_GET['filter_key']}%')");
			$this->_data['filter_key'] = $_GET['filter_key'];
		}
		if(isset($_GET['payment']) && $_GET['payment'] == TRUE){
			$criteria->addCondition("service_fee != '' ");
		}

		$count = Customer::model()->count($criteria);
		$customers = Customer::model()->findAll($criteria);
		if(!empty($customers)) {
			foreach($customers as $customer) 
			{
				$customer = $customer->attributes;
				if(!empty($customer['admin_id'])) {
					$admin_db = Admin::model()->findByPk($customer['admin_id']);
					if(!empty($admin_db)) {
						$customer['admin'] = $admin_db->attributes;
					}
				}
                if(!empty($customer['user_id'])) {
                    $user_db = User::model()->findByPk($customer['user_id']);
                    if(!empty($user_db)) {
                        $customer['user'] = $user_db->attributes;
                    }
                }
				$customer_list[] = $customer;
			}
		}

		$this->_data['customer_list'] = $customer_list;
		//ajax_response(200, '', $this->_data);

		//未认领的客户
		$new_customer_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("status=0");
		$new_customers = Customer::model()->findAll($criteria);
		if(!empty($new_customers)) {
			foreach($new_customers as $new_customer) {
				$new_customer_list[] = $new_customer->attributes;
			}
		}
		$this->_data['new_customer_list'] = $new_customer_list;

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;


		$this->render('my', $this->_data);
	}

	//public function actionIndex()
	public function actionList()
	{
		$this->pageTitle = '客户列表';

		$customer_list = array();

		$pageSize = 20;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;

		/*
		if(!isset($_GET['all']) && !isset($_GET['admin_id']) 
			&& (isset($_GET['status']) && $_GET['status'] !=0)) {
			$current_admin_id = Yii::app()->adminUser->id;
			$kefu_id_array = array(2,4,5);
			if(in_array($current_admin_id, $kefu_id_array)) {
				$criteria->addCondition("admin_id={$current_admin_id}");
			}
			}
		 */
		if(isset($_GET['admin_id']) && $_GET['admin_id']!='') {
			$criteria->addCondition("admin_id={$_GET['admin_id']}");
			$this->_data['admin_id'] = $_GET['admin_id'];
		}
		
		if(isset($_GET['status']) && $_GET['status']!='') {
			$criteria->addCondition("status = {$_GET['status']}");
			$this->_data['status'] = $_GET['status'];
			$this->_data['status0_btn_class'] = 'btn-default';
			$this->_data['status1_btn_class'] = $_GET['status'] == '1' ? 'btn-success' : 'btn-default';
			$this->_data['status5_btn_class'] = $_GET['status'] == '5' ? 'btn-success' : 'btn-default';
			$this->_data['status_2_btn_class'] = $_GET['status'] == '-2' ? 'btn-success' : 'btn-default';
		} else {
			$criteria->addCondition("status >= 0");
			$this->_data['status0_btn_class'] = 'btn-success';
			$this->_data['status1_btn_class'] = 'btn-default';
			$this->_data['status5_btn_class'] = 'btn-default';
			$this->_data['status_2_btn_class'] = 'btn-default';
		}

		if(isset($_GET['filter_key']) && $_GET['filter_key']!=''){
			$criteria->addCondition("(contact like '%{$_GET['filter_key']}%'"
				. " OR qq like '%{$_GET['filter_key']}%'"
				. " OR wangwang like '%{$_GET['filter_key']}%')");
			$this->_data['filter_key'] = $_GET['filter_key'];
		}
		if(isset($_GET['payment']) && $_GET['payment'] == TRUE){
			$criteria->addCondition("service_fee != '' ");
		}
		
		$order_direct = '';	// default order direct 'ASC'
		if(isset($_GET['orderby']) && $_GET['orderby']!=''){
			$orderby = $_GET['orderby'];
			if(substr($orderby, 0, 1) == '_'){
				$orderby = substr($orderby, 1);
				$order_direct = 'DESC';
			}
			$criteria->order = ' `'. $orderby .'` ' . $order_direct;
			$this->_data['orderby_btn_class'] = 'btn-primary';
			if($order_direct == 'DESC'){
				$this->_data['orderby_mark'] = '<span class="glyphicon glyphicon-arrow-down"></span>';
			}else{
				$this->_data['orderby_mark'] = '<span class="glyphicon glyphicon-arrow-up"></span>';
			}
		}else{
			$criteria->order = ' `ctime` DESC';
		}
		if($order_direct == 'DESC'){
			// next order ASC
			$this->_data['next_orderby'] = 'start_date';
		}else{
			// next order DESC
			$this->_data['next_orderby'] = '_start_date';
		}

		$count = Customer::model()->count($criteria);
		$customers = Customer::model()->findAll($criteria);
		if(!empty($customers)) {
			foreach($customers as $customer) {
				$customer = $customer->attributes;
				if(!empty($customer['admin_id'])) {
					$admin_db = Admin::model()->findByPk($customer['admin_id']);
					if(!empty($admin_db)) {
						$customer['admin'] = $admin_db->attributes;
					}
				}

				//进度
				if($customer['check_item_count'] > 0) {
					$customer['progress'] = intval($customer['check_item_complete_count'] / $customer['check_item_count'] * 100);
				} else {
					$customer['progress'] = 0;
				}
				if($customer['check_item_count'] > 0 && $customer['check_item_count'] == $customer['check_item_complete_count']) {
					$customer['complete'] = true;
				}
				
				//是否有Check item过期
				$customer['check_item_expires_warning_class'] = '';

				$criteria = new CDbCriteria;
				$criteria->addCondition("customer_id=". $customer['id']);
				$criteria->addCondition("status =0");
				$criteria->order = ' `due_date` ASC';

				$check_items = CheckItem::model()->find($criteria);

					$check_item=$check_items->attributes;
					if(!empty($check_item)) {
						if(strtotime($check_item['due_date']) < time())
						{
							$customer['check_item_expires_warning_class'] = 'text-danger';
						}
					}

				$customer_list[] = $customer;
			}
		}
		$this->_data['customer_list'] = $customer_list;

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;

		$this->render('index', $this->_data);
	}

	public function actionAdd()
	{
		$this->pageTitle = '添加客户';
		$this->render('edit');
	}

	public function actionEdit()
	{
		$this->pageTitle = '编辑客户信息';

		$id = Yii::app()->request->getParam('id');
		$customer = Customer::model()->findByPk($id);
		if(empty($customer)) {
			die('not exists');
		}
		$customer = $customer->attributes;
		$this->_data['customer'] = $customer;
		$this->render('edit', $this->_data);
	}

	public function actionClaim()
	{

		if(Yii::app()->request->isAjaxRequest) {
			$customer_id = Yii::app()->request->getParam('customer_id');
			$customer = Customer::model()->findByPk($customer_id);
			if(empty($customer)) {
				ajax_response('404', '找不到客户');
			}

			$admin_db = Admin::model()->findByPk(Yii::app()->adminUser->id);
			if(empty($admin_db)) {
				ajax_response('500', '未登录或你不存在在地球上');
			}
			$admin_db = $admin_db->attributes;
			$customer->admin_id = Yii::app()->adminUser->id;
			$customer->status = 1;
			if($customer->save()) {
                //认领完成后, 添加一个代办事项
                /*
				$new_check_item = new CheckItem;
				$new_check_item->title = '第一次成功联系上该客户';
				$new_check_item->customer_id = $customer_id;
				$new_check_item->admin_id = Yii::app()->adminUser->id;
				$new_check_item->due_date = date('Y-m-d', strtotime("+1 day "));
				$new_check_item->status = 0;
				$new_check_item->mtime = date('Y-m-d H:i:s');
				$new_check_item->ctime = date('Y-m-d H:i:s');

				$new_check_item->save();
				//update count
                $this->_update_customer_check_count($customer_id);
                 */

				ajax_response('200', '', $admin_db['nick_name']);
			} else {
				ajax_response('500', '删除失败');
			}
		}

	}

	public function actionDelete()
	{
		if(Yii::app()->request->isAjaxRequest) {
			$customer_id = Yii::app()->request->getParam('customer_id');
			$customer = Customer::model()->findByPk($customer_id);
			if(empty($customer)) {
				ajax_response('404', '找不到客户');
			}

			$customer->status = '-1';
			if($customer->save()) {
				ajax_response('200', '');
			} else {
				ajax_response('500', '删除失败');
			}
		}
	}

	public function actionUpdateStatus()
	{
		if(Yii::app()->request->isAjaxRequest) {
			$customer_id = Yii::app()->request->getParam('customer_id');
			$status = Yii::app()->request->getParam('status');
			if(empty($status)) {
				ajax_response('404', '参数不正确');
			}

			$customer = Customer::model()->findByPk($customer_id);
			if(empty($customer)) {
				ajax_response('404', '找不到客户');
            }
            //判断操作不能跨步骤
            if($status > 1 && $status <=6 
                && ( $status - $customer->status > 1) ) {
				ajax_response('403', '不能跨步骤操作');
            }

			$customer->status = $status;

			if($customer->save()) {
                if(!empty($customer->uid)) {
			        // notify shijieyou 
    		    	$notify = new Notify();
	    		    $post_data = "customer_id=$customer_id&uid=$customer->uid&obj=custom&act=update&status=$customer->status";
		    	    $notify->call("http://www.shijieyou.com", $post_data);
                }

				ajax_response('200', '');
			} else {
				ajax_response('500', '删除失败');
			}

		}
	}

	public function actionCancel()
	{
		if(Yii::app()->request->isAjaxRequest) {
			$customer_id = Yii::app()->request->getParam('customer_id');
			$customer = Customer::model()->findByPk($customer_id);
			if(empty($customer)) {
				ajax_response('404', '找不到客户');
			}

			$customer->status = '-2';
			if($customer->save()) {
				ajax_response('200', '');
			} else {
				ajax_response('500', '删除失败');
			}
		}
	}

	public function actionDetail()
	{
		$id = Yii::app()->request->getParam('id');
		$customer = Customer::model()->findByPk($id);
		if(empty($customer)) {
			die('not exists');
		}
		$customer = $customer->attributes;
		if(!empty($customer['admin_id'])) {
			$admin_db = Admin::model()->findByPk($customer['admin_id']);
			if(!empty($admin_db)) {
				$customer['admin'] = $admin_db->attributes;
			}
		}
		
		//进度
		if($customer['check_item_count'] > 0) {
			$customer['progress'] = intval($customer['check_item_complete_count'] / $customer['check_item_count'] * 100);
		} else {
			$customer['progress'] = 0;
		}
		if($customer['check_item_count'] > 0 && $customer['check_item_count'] == $customer['check_item_complete_count']) {
			$customer['complete'] = true;
		}
		
		$this->_data['customer'] = $customer;

		$this->pageTitle =  $customer['contact'] ;

		$order_margin = 0;
		$order_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id=". $customer['id']);
		$criteria->addCondition("status>=0");
		$criteria->order = ' `ctime` DESC';
		$orders = Order::model()->findAll($criteria);
		if(!empty($orders)) {
			foreach($orders as $order) {
				$order = $order->attributes;

				$margin = 0;
				if($order['payment_path'] == 'alipay_peerpay')  {
					$margin = 0;
				} else {
					$margin = round(($order['total_fee'] - $order['payment']), 2);
				}
				$order['margin'] = $margin;
				$order_list[] = $order;
				$order_margin += $margin;
			}
		}

		$this->_data['order_list'] = $order_list;

		$this->_data['order_margin'] = $order_margin;

		$traveler_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id=". $customer['id']);
		$criteria->addCondition("status>=0");
		$criteria->order = ' `ctime` ASC';
		$travelers = Traveler::model()->findAll($criteria);
		if(!empty($travelers)) {
			foreach($travelers as $traveler) {
				$traveler = $traveler->attributes;
				$traveler_list[] = $traveler;
			}
		}
		$this->_data['traveler_list'] = $traveler_list;

		$comment_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("object='customer-log'");
		$criteria->addCondition("object_id='{$customer['id']}'");
		$criteria->addCondition("status=1");
		$criteria->order = ' `ctime` DESC';
		$count = Comment::model()->count($criteria);
		$comments = Comment::model()->findAll($criteria);
		if(!empty($comments)) {
			foreach($comments as $comment) {
				$comment = $comment->attributes;
				$comment_list[] = $comment;
			}
		}
		$this->_data['comment_list'] = $comment_list;


		//交易流水
		$record_margin = 0;
		$record_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id='{$customer['id']}'");
		$criteria->addCondition("status>=0");
		$criteria->order = ' `create_time` DESC';
		$count = AlipayRecord::model()->count($criteria);
		$records = AlipayRecord::model()->findAll($criteria);
		if(!empty($records)) {
			foreach($records as $record) {
				$record = $record->attributes;
				if($record['in_out_type'] == '收入') {
					$record_margin += $record['total_amount'];
				} else {
					$record_margin -= $record['total_amount'];
				}
				$record_list[] = $record;
			}
		}
		$this->_data['record_list'] = $record_list;
		$this->_data['record_margin'] = $record_margin;
		$this->render('detail', $this->_data);
	}


	public function actionQQLog()
	{
		$this->pageTitle = '编辑客户信息';

		$customer_id = Yii::app()->request->getParam('customer_id');
		$customer = Customer::model()->findByPk($customer_id);
		if(empty($customer)) {
			die('not exists');
		}
		$customer = $customer->attributes;
		$this->_data['customer'] = $customer;
		$this->render('qq_log', $this->_data);
	}

	public function actionSaveCustomer()
	{
		$accept_fields = array('customer_id', 'contact', 'origin', 'destination',  'sex', 'num_of_people', 'start_date', 'end_date', 'has_booked_airline_ticket', 'has_booked_hotel','no_booking','remark', 'mobile', 'qq', 'weixin', 'email', 'wangwang', 'journey_url', 'service_fee', 'taobao_trade_id', 'qq_log', 'platform', 'source', 'trip_id', 'admin_id', 'user_id');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}
		if(empty($data['has_booked_airline_ticket'])) {
			$data['has_booked_airline_ticket'] = 0;
		}
		if(empty($data['has_booked_hotel'])) {
			$data['has_booked_hotel'] = 0;
		}
		if(empty($data['no_booking'])) {
			$data['no_booking'] = 0;
		}

		if(!empty($data['customer_id'])) {
			$customer_id = $data['customer_id'];
			unset($data['customer_id']);
			$this->_save_customer($customer_id, $data);
		} else {
			$this->_new_customer($data);
		}
	}

	private function _save_customer($customer_id, $data)
	{
		$customer_db = Customer::model()->findByPk($customer_id);
		if(empty($customer_db)) {
			ajax_response('404', '找不到该客户信息，无法保存');
		}

		//判断自动任务是否为空，如果是，自动添加默认任务
		if(!empty($data['start_date'])) {
			$criteria = new CDbCriteria;
			$criteria->addCondition("customer_id='{$customer_db['id']}'");
			$criteria->addCondition("status >= 0");
			$count = CheckItem::model()->count($criteria);
			if($count <= 0) {
				$this->_add_check_list($customer_db['id']);
			}
		}
		
		Customer::model()->updateByPk($customer_id, $data);
		$this->_data['customer_id'] = $customer_id;
		ajax_response(200,'',$this->_data);
	}

	private function _new_customer($data)
	{
		$new_customer = new Customer;
		$new_customer->contact = $data['contact'];
		$new_customer->sex = $data['sex'];
		$new_customer->start_date = $data['start_date'];
		$new_customer->origin = $data['origin'];
		$new_customer->destination = $data['destination'];
		$new_customer->end_date = $data['end_date'];
		$new_customer->num_of_people = $data['num_of_people'];
		$new_customer->has_booked_airline_ticket = $data['has_booked_airline_ticket'];
		$new_customer->has_booked_hotel = $data['has_booked_hotel'];
		$new_customer->no_booking = $data['no_booking'];
		$new_customer->remark = $data['remark'];
		$new_customer->mobile = $data['mobile'];
		$new_customer->qq = $data['qq'];
		$new_customer->weixin = $data['weixin'];
		$new_customer->wangwang = $data['wangwang'];
		$new_customer->email = $data['email'];
		$new_customer->service_fee = empty($data['service_fee']) ? 0 : intval($data['service_fee']);
		$new_customer->taobao_trade_id = $data['taobao_trade_id'];
		$new_customer->journey_url = empty($data['journey_url']) ? '' : $data['journey_url'];

		if(!empty(Yii::app()->adminUser->id)) {
			$new_customer->admin_id = Yii::app()->adminUser->id;
		}

		$new_customer->ctime = date('Y-m-d H:i:s', time());
		$new_customer->status = 1;
		if($new_customer->save()) {
			$this->_data['customer_id'] = Yii::app()->db->getLastInsertId();
			if(!empty($data['start_date'])) {
				$this->_add_check_list($this->_data['customer_id']);
			}
			ajax_response('200', '', $this->_data);
		} else {
			var_dump($new_customer->getErrors());
			ajax_response('500', '保存失败');
		}
	}

	public function actionCus() {
		//$this->_add_check_list(10);
	}

	private function _add_check_list($customer_id) 
	{
		if(empty($customer_id) || empty(Yii::app()->adminUser->id)) {
			return false;
		}
		$customer_db = Customer::model()->findByPk($customer_id);
		if(empty($customer_db)) {
			//TODO Log
			return false;
		}
		$customer_db = $customer_db->attributes;
		//查找存在check_list
		if(empty($customer_db['start_date'])) {
			return false;
		}
		$start_time = strtotime($customer_db['start_date']);
		if($start_time <= 0) {
			return false;
		}
		$check_list_templates = array(
			//array('title' =>'提醒用户办签证', 'due_time'=> date('Y-m-d H:i:s', $start_time - 3600 *24 * 30 + 3600 * 11 )),
			array('title' =>'确保此用户账已经全部算清楚', 'due_time' =>date('Y-m-d H:i:s', $start_time - 3600 *24 * 3 + 3600 * 11) ),
			//array('title' =>'与客户沟通祝客户一路顺风', 'due_time'=>date('Y-m-d H:i:s', $start_time - 3600 * 24 * 1 + 3600 * 11) ),
		);

		foreach($check_list_templates as $item) {
			$new_check_item = new CheckItem;
			$new_check_item->title = $item['title'];
			$new_check_item->customer_id = $customer_id;
			$new_check_item->admin_id = Yii::app()->adminUser->id;
			$new_check_item->due_date = $item['due_time'];
			$new_check_item->status = 0;
			$new_check_item->mtime = date('Y-m-d H:i:s');
			$new_check_item->ctime = date('Y-m-d H:i:s');

			$new_check_item->save();
		}

		//update count
		$this->_update_customer_check_count($customer_id);
	}

	private function _update_customer_check_count($customer_id) 
	{
		if(empty($customer_id)) {
			return false;
		}

		$check_item_total = $check_item_complete_count = 0;
		$criteria = new CDbCriteria;
		$criteria->addCondition("status >= 0");
		$criteria->addCondition("customer_id = $customer_id");
		$check_item_total = CheckItem::model()->count($criteria);

		$criteria = new CDbCriteria;
		$criteria->addCondition("status = 1");
		$criteria->addCondition("customer_id = $customer_id");
		$check_item_complete_count = CheckItem::model()->count($criteria);

		$updateData = array('check_item_count' => $check_item_total, 'check_item_complete_count' => $check_item_complete_count);
		Customer::model()->updateByPk($customer_id, $updateData);

		return true;
	}

}
