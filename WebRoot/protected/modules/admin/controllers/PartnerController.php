<?php

Yii::import('ext.Notify',true);

class PartnerController extends Controller
{
    private $_data;
    public $layout = '/layouts/column1';

    public function actionIndex()
    {
        $this->layout = '/layouts/column1';
        $this->pageTitle = '我的客户';
        $partner_list = array();

        $pageSize = 20;
        $page = isset($_GET['page']) ? intval($_GET['page']) : 1;
        $offset = ($page - 1) * $pageSize;

        $criteria = new CDbCriteria;
        $criteria->offset = $offset;
        $criteria->limit = $pageSize;
        $criteria->order = 'id desc';

        if (isset($_GET['status']) && $_GET['status'] != '') {
            $criteria->addCondition("status = {$_GET['status']}");
        } elseif (!isset($_GET['filter_key']) || empty($_GET['filter_key'])) {
            $criteria->addCondition("status!= -1");
            $criteria->addCondition("status!= 6");
        }

        if (isset($_GET['filter_key']) && $_GET['filter_key'] != '') {
            $criteria->addCondition("(username like '%{$_GET['filter_key']}%'"
                . " OR nick_name like '%{$_GET['filter_key']}%' )");
            $this->_data['filter_key'] = $_GET['filter_key'];
        }

        $count = Partner::model()->count($criteria);
        $partners = Partner::model()->findAll($criteria);
        if (!empty($partners)) {
            foreach ($partners as $partner) {
                $partner = $partner->attributes;

                $partner_list[] = $partner;
            }
        }

        $this->_data['partner_list'] = $partner_list;

        //分页
        $pages = new CPagination($count);
        $pages->pageSize = $pageSize;
        $pages->pageVar = 'page';
        $pages->applyLimit($criteria);
        $this->_data['pages'] = $pages;
        $this->_data['count'] = $count;


        $this->render('index', $this->_data);
    }

    public function actionAdd()
    {
        $this->pageTitle = '添加客户';
        $this->render('edit');
    }

    public function actionEdit()
    {
        $this->pageTitle = '编辑客户信息';

        $id = Yii::app()->request->getParam('id');
        $partner = Partner::model()->findByPk($id);
        if (empty($partner)) {
            die('not exists');
        }
        $partner = $partner->attributes;
        $this->_data['partner'] = $partner;
        $this->render('edit', $this->_data);
    }

    public function actionSavePartner()
    {
        $accept_fields = array('id', 'username', 'password', 'nick_name', 'company',  'company_intro', 'company_logo', 'remark');
        $data = array();
        foreach($accept_fields as $field) {
            if(isset($_POST[$field])) {
                $data[$field] = Yii::app()->request->getParam("{$field}");
            }

        }
        if(empty($data['password'])) {
            unset($data['password']);
        } else {
            $data['password'] = md5($data['password']);
        }

        if(!empty($data['id'])) {
            $partner_id = $data['id'];
            unset($data['id']);
            $this->_save_partner($partner_id, $data);
        } else {
            $this->_new_partner($data);
        }
    }

    private function _save_partner($partner_id, $data)
    {
        $partner_db = Partner::model()->findByPk($partner_id);
        if(empty($partner_db)) {
            ajax_response('404', '找不到该商户信息，无法保存');
        }

        partner::model()->updateByPk($partner_id, $data);
        $this->_data['partner_id'] = $partner_id;
        ajax_response(200,'',$this->_data);
    }

    private function _new_partner($data)
    {
        //检查是否是重名
        $exist = Partner::model()->findByAttributes(array("username" => "{$data['username']}"));
        if(!empty($exist)) {
            ajax_response('501', '用户名重复');
        }

        $new_partner = new Partner;
        $new_partner->username = $data['username'];
        $new_partner->nick_name = $data['nick_name'];
        $new_partner->password = $data['password'];
        $new_partner->company = $data['company'];
        $new_partner->company_intro = $data['company_intro'];
        $new_partner->company_logo = $data['company_logo'];
        $new_partner->remark = $data['remark'];

        $new_partner->ctime = date('Y-m-d H:i:s', time());
        $new_partner->status = 1;
        if($new_partner->save()) {
            $this->_data['partner_id'] = Yii::app()->db->getLastInsertId();
            if(!empty($data['start_date'])) {
                //$this->_add_check_list($this->_data['partner_id']);
            }

            //复制一个案例行程，337 江江的泰国行程
            $this->_copy($this->_data['partner_id'], 337);

            ajax_response('200', '', $this->_data);
        } else {
            var_dump($new_partner->getErrors());
            ajax_response('500', '保存失败');
        }
    }


    public function actionCtrip()
    {
        $this->layout = '/layouts/column1';
        $this->pageTitle = '携程行程规划师';
        $advisor_list = array();

        $pageSize = 100;
        $page = isset($_GET['page']) ? intval($_GET['page']) : 1;
        $offset = ($page - 1) * $pageSize;

        $criteria = new CDbCriteria;
        $criteria->offset = $offset;
        $criteria->limit = $pageSize;
        $criteria->order = 'id desc';

        if (isset($_GET['filter_key']) && $_GET['filter_key'] != '') {
            $criteria->addCondition("(name like '%{$_GET['filter_key']}%'"
                . " OR nickName like '%{$_GET['filter_key']}%' "
                . " OR districtList like '%{$_GET['filter_key']}%' "
                . ")");
            $this->_data['filter_key'] = $_GET['filter_key'];
        }

        $count = Advisor::model()->count($criteria);
        $advisors = Advisor::model()->findAll($criteria);
        if (!empty($advisors)) {
            foreach ($advisors as $advisor) {
                $advisor = $advisor->attributes;

                $advisor_list[] = $advisor;
            }
        }

        $this->_data['advisor_list'] = $advisor_list;

        //分页
        $pages = new CPagination($count);
        $pages->pageSize = $pageSize;
        $pages->pageVar = 'page';
        $pages->applyLimit($criteria);
        $this->_data['pages'] = $pages;
        $this->_data['count'] = $count;


        $this->render('advisors', $this->_data);
    }

    public function _copy($partner_id = 0, $sourceId = 0, $targetTitle= '案例行程', $targetCustomerId = 0)
    {

        $source_trip = Trip::model()->findByPk($sourceId);
        if(empty($source_trip)) {
            ajax_response('404', '复制源不存在', '');
        }
        $source_trip = $source_trip->attributes;
        if($source_trip['status'] <= 0 ) {
            ajax_response('404', '复制源已经删除，不能复制', '');
        }

        //copy trip
        $new_trip = new Trip;
        $new_trip->title = empty($targetTitle) ? $source_trip['title'] . "-副本" : $targetTitle;
        $new_trip->customer_id = $targetCustomerId;
        //$new_trip->admin_id = Yii::app()->adminUser->id;
        $new_trip->user_id = $partner_id;
        $new_trip->start_time = empty($targetStartTime) ? $source_trip['start_time']: $targetStartTime;
        $new_trip->days = $source_trip['days'];
        $new_trip->start_city_id = $source_trip['start_city_id'];
        $new_trip->end_city_id = $source_trip['end_city_id'];
        $new_trip->ctime = date('Y-m-d H:i:s');
        $new_trip->mtime = date('Y-m-d H:i:s');
        $new_trip->status = 1;
        $hash_string = $new_trip->title . time();
        $new_trip->hash_id = md5($hash_string);
        $new_trip->from_trip_id = $sourceId;
        if(!$new_trip->save()) {
            ajax_response('500', '复制失败001，请联系喵喵');
        }
        $new_trip_id = Yii::app()->db->getLastInsertId();
        if(empty($new_trip_id)) {
            ajax_response('500', '复制失败001，请联系喵喵');
        }
        //更新customer的trip_id
        Customer::model()->updateByPk($targetCustomerId, array('trip_id' => $new_trip_id));

        //copy trip_day
        $trip_list = array();
        $criteria = new CDbCriteria;
        $criteria->addCondition("trip_id='{$sourceId}'");
        $criteria->addCondition("status >='0'");
        $criteria->order = 'sort asc';
        $tripDays = TripDay::model()->findAll($criteria);
        if(!empty($tripDays)) {
            foreach($tripDays as $day) {
                $new_trip_day = new TripDay;
                $new_trip_day->trip_id = $new_trip_id;
                $new_trip_day->date = $day->date;
                $new_trip_day->title = $day->title;
                $new_trip_day->desc = $day->desc;
                $new_trip_day->traffic_note = $day->traffic_note;
                $new_trip_day->image = $day->image;
                $new_trip_day->city_id = $day->city_id;
                $new_trip_day->city_ids = $day->city_ids;
                $new_trip_day->user_id = $day->user_id;
                $new_trip_day->note = $day->note;
                $new_trip_day->sort = $day->sort;
                $new_trip_day->status = $day->status;

                if($new_trip_day->save()) {
                    $trip_day_id = $day['id'];
                    $new_trip_day_id = Yii::app()->db->getLastInsertId();
                    if($new_trip_day_id <=0) {
                        ajax_response('500', '插入TripDay失败, 请联系喵喵');
                    }

                    //copy trip_day_hotel
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $hotels = TripDayHotel::model()->findAll($criteria);
                    if(!empty($hotels)) {
                        foreach($hotels as $hotel) {
                            $hotelEvent = new TripDayHotel;
                            $hotelEvent->trip_id = $new_trip_id;
                            $hotelEvent->trip_day_id = $new_trip_day_id;
                            $hotelEvent->hotel_id = $hotel->hotel_id;
                            $hotelEvent->title = $hotel->title;
                            $hotelEvent->note = $hotel->note;
                            $hotelEvent->cover_image = $hotel->cover_image;
                            $hotelEvent->currency = $hotel->currency;
                            $hotelEvent->spend = $hotel->spend;
                            $hotelEvent->ticketcount = $hotel->ticketcount;
                            $hotelEvent->type = $hotel->type;
                            $hotelEvent->desc = $hotel->desc;
                            $hotelEvent->address = $hotel->address;
                            $hotelEvent->lat = $hotel->lat;
                            $hotelEvent->lon = $hotel->lon;
                            $hotelEvent->mtime = date('Y-m-d H:i:s' , time());
                            $hotelEvent->ctime = date('Y-m-d H:i:s' , time());
                            $hotelEvent->status = $hotel->status;
                            $hotelEvent->save();
                        }
                    }

                    //copy trip_day_poi
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $pois = TripDayPoi::model()->findAll($criteria);
                    if(!empty($pois)) {
                        foreach($pois as $poi) {
                            $poiEvent = new TripDayPoi;
                            $poiEvent->trip_id = $new_trip_id;
                            $poiEvent->trip_day_id = $new_trip_day_id;
                            $poiEvent->poi_id = $poi->poi_id;
                            $poiEvent->type = $poi->type;
                            $poiEvent->poi_name= $poi->poi_name;
                            $poiEvent->desc = $poi->desc;
                            $poiEvent->cover_image = $poi->cover_image;
                            $poiEvent->price = $poi->price;
                            $poiEvent->note = $poi->note;
                            $poiEvent->lat = $poi->lat;
                            $poiEvent->lon = $poi->lon;
                            $poiEvent->sort = $poi->sort;
                            $poiEvent->mtime = date('Y-m-d H:i:s' , time());
                            $poiEvent->ctime = date('Y-m-d H:i:s' , time());
                            $poiEvent->status = $poi->status;
                            $poiEvent->save();
                        }
                    }

                    //copy trip_day_traffic
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $traffics = TripDayTraffic::model()->findAll($criteria);
                    if(!empty($traffics)) {
                        foreach($traffics as $traffic) {
                            $traffic = $traffic->attributes;
                            $new_traffic = new TripDayTraffic;
                            $new_traffic->trip_id = $new_trip_id;
                            $new_traffic->trip_day_id = $new_trip_day_id;
                            $new_traffic->note = $traffic['note'];
                            $new_traffic->from_place = $traffic['from_place'];
                            $new_traffic->from_placeid = $traffic['from_placeid'];
                            $new_traffic->to_place = $traffic['to_place'];
                            $new_traffic->to_placeid = $traffic['to_placeid'];
                            $new_traffic->days = $traffic['days'];
                            $new_traffic->traffic_number = $traffic['traffic_number'];
                            $new_traffic->spend = !empty($traffic['spend']) ? $traffic['spend'] : 0;
                            $new_traffic->currency = $traffic['currency'];
                            $new_traffic->start_hours = $traffic['start_hours'];
                            $new_traffic->start_minutes = $traffic['start_minutes'];
                            $new_traffic->end_hours = $traffic['end_hours'];
                            $new_traffic->end_minutes = $traffic['end_minutes'];
                            $new_traffic->ticketcount = $traffic['ticketcount'];
                            $new_traffic->mode = $traffic['mode'];
                            $new_traffic->status = 0;
                            $new_traffic->mtime = date('Y-m-d H:i:s', time());
                            $new_traffic->ctime = date('Y-m-d H:i:s', time());
                            $new_traffic->save();
                        }
                    }

                }
            }
        }

        //copy trip_note
        $trip_list = array();
        $criteria = new CDbCriteria;
        $criteria->addCondition("trip_id='{$sourceId}'");
        $criteria->addCondition("status >='0'");
        $tripNotes = TripNote::model()->findAll($criteria);
        if(!empty($tripNotes)) {
            foreach($tripNotes as $note) {
                $new_trip_note= new TripNote;
                $new_trip_note->trip_id = $new_trip_id;
                $new_trip_note->note = $note->note;
                $new_trip_note->status = $note->status;

                $new_trip_note->save();
            }
        }
        ajax_response(200, '', $trip_list);
    }

}