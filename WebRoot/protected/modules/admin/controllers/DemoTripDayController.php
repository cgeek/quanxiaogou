<?php

class DemoTripDayController extends Controller
{
	private $_data;

	public $layout = '/layouts/column2';

	public function actionIndex() 
	{
		$this->pageTitle = '经典行程管理';
		
		$page = Yii::app()->request->getParam('page');
		$city_id = isset($_GET['city_id']) ? intval($_GET['city_id']) : 0;
		$keyword = Yii::app()->request->getParam('keyword');

		$poi_list = array();
		
		$pageSize = 10;
		$page = !empty($page) ? intval($page) : 1;
		$status = Yii::app()->request->getParam('status');
		$status = empty($status) ? 0 : $status;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		$criteria->addCondition("status=$status");

		if($city_id > 0) {
			$criteria->addCondition("city_id=$city_id");
		}

		if(!empty($keyword)) {
			$criteria->addCondition("`title` like '%{$keyword}%'"
				. " OR `desc` like '%{$keyword}%'"
				. " OR `traffic_note` like '%{$keyword}%'");
			$this->_data['keyword'] = $keyword;
		}

		$count = DemoTripDay::model()->count($criteria);
		$demo_days = DemoTripDay::model()->findAll($criteria);
		if(!empty($demo_days)) {
			foreach($demo_days as $demo) {
				$demo = $demo->attributes;
				$demo_trip_day_list[] = $demo;
			}
		}
		$this->_data['demo_list'] = $demo_trip_day_list;

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;

		//ajax_response('200', '', $this->_data);

		$this->_data['city_list'] = $this->_getCityList($city_id);

		$this->render('index', $this->_data);
	}

	public function actionEdit()
	{
		$id = Yii::app()->request->getParam('id');

		$city_id = 0;
		$demoTripDay = DemoTripDay::model()->findByPk($id);
		if(!empty($demoTripDay)) {
			$this->_data['demoTripDay'] = $demoTripDay->attributes;
			$city_id = $demoTripDay->city_id;
		}
		
		$this->_data['city_list'] = $this->_getCityList($city_id);
		$this->render('edit', $this->_data);

	}

	public function actionSave()
	{
		$accept_fields = array('id', 'title', 'desc', 'traffic_note', 'city_id', 'admin_id',  'note', 'status');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}

		if(!empty($data['id'])) {
			$id = $data['id'];
			unset($data['id']);
			$demo = DemoTripDay::model()->findByPk($id);
			if(empty($demo)) {
				ajax_response('404', '找不到该行程');
			}
			DemoTripDay::model()->updateByPk($id, $data);
			ajax_response('200', '保存成功');
		} else {
			ajax_response('404', '缺少参数');
		}
	}

	private function _getCityList($currentCity = 0){
		$city_list = array();
		$criteriaCity = new CDbCriteria;
		$criteriaCity->addCondition("status=1");
		$cities = City::model()->findAll($criteriaCity);
		if(!empty($cities)) {
			foreach($cities as $city){
				$city = $city->attributes;
				if($currentCity == $city['id']){
					$city['selected'] = ' selected';
				}else{
					$city['selected'] = '';
				}
				$city_list[] = $city;
			}
		}
		return $city_list;
	}
	
}
