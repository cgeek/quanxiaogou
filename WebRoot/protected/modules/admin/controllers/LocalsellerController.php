<?php

class LocalsellerController extends Controller
{
    private $_data;
    public $layout = '/layouts/column1';

    public function actionIndex()
    {
        $this->pageTitle = '商户列表';

        $country = Yii::app()->request->getParam('country');
        $keyword = Yii::app()->request->getParam('keyword');
        $typeEnum = Yii::app()->request->getParam('typeEnum');
        $statusEnum = Yii::app()->request->getParam('statusEnum');

        $localseller_list = array();
        $pageSize = 15;
        $page = isset($_GET['page']) ? intval($_GET['page']) : 1;
        $offset = ($page - 1) * $pageSize;
        $criteria = new CDbCriteria;
        $criteria->offset = $offset;
        $criteria->limit = $pageSize;

        /*
        if(isset($_GET['status']) && $_GET['status'] != '') {
            $criteria->addCondition("status={$_GET['status']}");
            $this->_data['status'] = $_GET['status'];
        } else {
            $criteria->addCondition("status>=0");
        }

        if($admin_id > 0) {
            $criteria->addCondition("admin_id='{$admin_id}'");
        }
        */

        if(!empty($typeEnum) && $typeEnum != 'all') {
            $criteria->addCondition("type_enum='{$typeEnum}'");
        }
        if(!empty($statusEnum) && $statusEnum != 'all') {
            $criteria->addCondition("status_enum='{$statusEnum}'");
        }
        if(!empty($country)) {
            $criteria->addCondition("country='{$country}'");
        }

        if(!empty($keyword)){
            $criteria->addCondition("(name like '%{$keyword}%' or introduce like '%{$keyword}%')");
            $this->_data['keyword'] = $keyword;
        }
        $criteria->order = ' `date_created` DESC';
        $count = LocalSeller::model()->count($criteria);
        $localsellers = LocalSeller::model()->findAll($criteria);
        if(!empty($localsellers)) {
            foreach($localsellers as $localseller) {
                $localseller = $localseller->attributes;
                /*
                $trip['admin_name'] = '未知';
                if($trip['admin_id'] > 0) {
                    $admin = Admin::model()->findByPk($trip['admin_id']);
                    if(!empty($admin)) {
                        $trip['admin_name'] = $admin->nick_name;
                    }
                }
                */
                $localseller_list[] = $localseller;


            }
        }
        $this->_data['localseller_list'] = $localseller_list;

        //分页
        $pages=new CPagination($count);
        $pages->pageSize= $pageSize;
        $pages->pageVar = 'page';
        $pages->applyLimit($criteria);
        $this->_data['pages'] = $pages;
        $this->_data['count'] = $count;

        $this->render('index', $this->_data);
    }
}
