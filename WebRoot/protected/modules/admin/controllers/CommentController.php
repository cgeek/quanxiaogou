<?php

class CommentController extends Controller
{
	private $_data;
	public $layout = '/layouts/column2';

	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionSave()
	{
		$accept_fields = array('content', 'user_id', 'admin_id', 'object', 'object_id');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}

		if(empty($data['content'])) {
			ajax_response('404', '');
		}

		$new_comment = new Comment;
		$new_comment->content = trim($data['content']);
		$new_comment->user_id = isset($data['user_id']) ? $data['user_id'] : 0;
		$new_comment->admin_id = isset($data['admin_id']) ? $data['admin_id'] : 0;
		$new_comment->object = isset($data['object']) ? $data['object'] : '';
		$new_comment->object_id = isset($data['object_id']) ? $data['object_id'] : 0;
		$new_comment->status = 1;
		$new_comment->ctime = date('Y-m-d H:i:s', time());

		if($new_comment->save()) {
			ajax_response(200, '');
		} else {
			var_dump($new_comment->getErrors());
			ajax_response(500, '保存失败');
		}
	}

}
