<?php

class JourneyController extends Controller
{
	private $_data;
	public $layout = '/layouts/column2';

	public function actionIndex()
	{
		$this->pageTitle = '行程列表';
		$customer_id = Yii::app()->request->getParam('customer_id');
		$admin_id = Yii::app()->request->getParam('admin_id');
        $user_id = Yii::app()->request->getParam('user_id');
		$keyword = Yii::app()->request->getParam('keyword');

		$trip_list = array();
		$pageSize = 15;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$offset = ($page - 1) * $pageSize;
		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;

		if(isset($_GET['status']) && $_GET['status'] != '') {
			$criteria->addCondition("status={$_GET['status']}");
			$this->_data['status'] = $_GET['status'];
		} else {
			$criteria->addCondition("status>=0");
		}

		if($user_id > 0) {
			$criteria->addCondition("user_id='{$user_id}'");
		}
		if($customer_id > 0) {
			$criteria->addCondition("customer_id='{$customer_id}'");
		}

		if(!empty($keyword)){
			$criteria->addCondition("(title like '%{$keyword}%')");
			$this->_data['keyword'] = $keyword; 
		}
		$criteria->order = ' `ctime` DESC';
		$count = Trip::model()->count($criteria);
		$trips = Trip::model()->findAll($criteria);
		if(!empty($trips)) {
			foreach($trips as $trip) {
				$trip = $trip->attributes;
				$trip['admin_name'] = '未知';
				if($trip['user_id'] > 0) {
					$partner = Partner::model()->findByPk($trip['user_id']);
					if(!empty($partner)) {
						$trip['partner_nickname'] = $partner->nick_name;
					}
				}
				$trip_list[] = $trip;
			}
		}
		$this->_data['trip_list'] = $trip_list;

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;


        $partner_list = Partner::model()->findAll();
        $this->_data['partner_list'] = $partner_list;

		$this->render('trip_list', $this->_data);
	}

	public function actionUpdateTripCustomer()
	{

		$trip_id = Yii::app()->request->getParam('trip_id');
		$customer_id = Yii::app()->request->getParam('customer_id');
		if(empty($trip_id) || empty($customer_id)) {
			ajax_response('404', '参数不正确');
		}
		$trip_db = Trip::model()->findByPk($trip_id);
		if(empty($trip_db)) {
			ajax_response('404', '找不到行程');
		}
		$customer_db = Customer::model()->findByPk($customer_id);
		if(empty($customer_db) || $customer_db->status <= 0) {
			ajax_response('404', '找不到客户, 或者客户已经删除或者未认领');
		}
		if($customer_db['trip_id'] > 0 && $customer_db['trip_id'] != $trip_id) {
			ajax_response('404', '客户已经有绑定行程,目前不支持绑定多个, 已经绑定id是：' . $customer_db['trip_id']);
		}

		$trip_db->customer_id = $customer_id;
		$trip_db->admin_id = $customer_db['admin_id'];
		$trip_db->save();

		$customer_db->trip_id = $trip_id;
		$customer_db->save();
		ajax_response(200, '');
	}


	public function actionGoogle()
	{
		$this->pageTitle = '行程管理';

		$customer_id = Yii::app()->request->getParam('customer_id');
		$customer = Customer::model()->findByPk($customer_id);
		if(!empty($customer)) {
			$customer = $customer->attributes;
			$this->_data['customer'] = $customer;

			$this->pageTitle = $customer['contact'] . '的行程';
		}
		$this->render('index', $this->_data);
	}

	public function actionDeleteTrip()
	{
		$trip_id = Yii::app()->request->getParam('trip_id');
		$trip_db = Trip::model()->findByPk($trip_id);
		if(empty($trip_db)) {
			ajax_response('404', '找不到行程');
		}
		$trip_db->status = -1;
		$trip_db->save();

		if($trip_db->customer_id > 0) {
			$customer_db = Customer::model()->findByPk($trip_db->customer_id);
			if(!empty($customer_db) && $customer_db->trip_id == $trip_id) {
				$customer_db->trip_id = 0;
				$customer_db->save();
			}
		}
		ajax_response(200,'删除成功');
	}

    public function actionCopy()
    {
        $sourceId = Yii::app()->request->getParam('sourceId');
        $targetTitle = Yii::app()->request->getParam('targetTitle');
        $targetPartnerId = Yii::app()->request->getParam('targetPartnerId');

        //checkCustomer
        $partner = Partner::model()->findByPk($targetPartnerId);
        if(empty($partner)) {
            ajax_response('404', '商户不存在', '');
        }

        $source_trip = Trip::model()->findByPk($sourceId);
        if(empty($source_trip)) {
            ajax_response('404', '复制源不存在', '');
        }
        $source_trip = $source_trip->attributes;
        if($source_trip['status'] <= 0 ) {
            ajax_response('404', '复制源已经删除，不能复制', '');
        }

        //copy trip
        $new_trip = new Trip;
        $new_trip->title = empty($targetTitle) ? $source_trip['title'] : $targetTitle;
        $new_trip->user_id = $targetPartnerId;
        $new_trip->start_time = empty($targetStartTime) ? $source_trip['start_time']: $targetStartTime;
        $new_trip->days = $source_trip['days'];
        $new_trip->start_city_id = $source_trip['start_city_id'];
        $new_trip->end_city_id = $source_trip['end_city_id'];
        $new_trip->ctime = date('Y-m-d H:i:s');
        $new_trip->mtime = date('Y-m-d H:i:s');
        $new_trip->status = 1;
		$hash_string = $new_trip->title . time();
		$new_trip->hash_id = md5($hash_string);
        $new_trip->from_trip_id = $sourceId;
        if(!$new_trip->save()) {
            ajax_response('500', '复制失败001，请联系喵喵');
        }
        $new_trip_id = Yii::app()->db->getLastInsertId();
        if(empty($new_trip_id)) {
            ajax_response('500', '复制失败001，请联系喵喵');
        }
        
        //copy trip_day
		$trip_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("trip_id='{$sourceId}'");
		$criteria->addCondition("status >='0'");
		$criteria->order = 'sort asc';
		$tripDays = TripDay::model()->findAll($criteria);
		if(!empty($tripDays)) {
            foreach($tripDays as $day) {
                $new_trip_day = new TripDay;
                $new_trip_day->trip_id = $new_trip_id;
                $new_trip_day->date = $day->date;
                $new_trip_day->title = $day->title;
                $new_trip_day->desc = $day->desc;
                $new_trip_day->traffic_note = $day->traffic_note;
                $new_trip_day->image = $day->image;
                $new_trip_day->city_id = $day->city_id;
                $new_trip_day->city_ids = $day->city_ids;
                $new_trip_day->user_id = $day->user_id;
                $new_trip_day->note = $day->note;
                $new_trip_day->sort = $day->sort;
                $new_trip_day->status = $day->status;

                if($new_trip_day->save()) {
                    $trip_day_id = $day['id'];
                    $new_trip_day_id = Yii::app()->db->getLastInsertId();
                    if($new_trip_day_id <=0) {
                        ajax_response('500', '插入TripDay失败, 请联系喵喵');
                    }
                    
                    //copy trip_day_hotel
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $hotels = TripDayHotel::model()->findAll($criteria);
                    if(!empty($hotels)) {
                        foreach($hotels as $hotel) {
                            $hotelEvent = new TripDayHotel;
                            $hotelEvent->trip_id = $new_trip_id;
                            $hotelEvent->trip_day_id = $new_trip_day_id;
                            $hotelEvent->hotel_id = $hotel->hotel_id;
                            $hotelEvent->title = $hotel->title;
                            $hotelEvent->note = $hotel->note;
                            $hotelEvent->cover_image = $hotel->cover_image;
                            $hotelEvent->currency = $hotel->currency;
                            $hotelEvent->spend = $hotel->spend;
                            $hotelEvent->ticketcount = $hotel->ticketcount;
                            $hotelEvent->type = $hotel->type;
                            $hotelEvent->desc = $hotel->desc;
                            $hotelEvent->address = $hotel->address;
                            $hotelEvent->lat = $hotel->lat;
                            $hotelEvent->lon = $hotel->lon;
                            $hotelEvent->mtime = date('Y-m-d H:i:s' , time());
                            $hotelEvent->ctime = date('Y-m-d H:i:s' , time());
                            $hotelEvent->status = $hotel->status;
                            $hotelEvent->save();
                        }
                    }

                    //copy trip_day_poi
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $pois = TripDayPoi::model()->findAll($criteria);
                    if(!empty($pois)) {
                        foreach($pois as $poi) {
                            $poiEvent = new TripDayPoi;
                            $poiEvent->trip_id = $new_trip_id;
                            $poiEvent->trip_day_id = $new_trip_day_id;
                            $poiEvent->poi_id = $poi->poi_id;
                            $poiEvent->type = $poi->type;
                            $poiEvent->poi_name= $poi->poi_name;
                            $poiEvent->desc = $poi->desc;
                            $poiEvent->cover_image = $poi->cover_image;
                            $poiEvent->price = $poi->price;
                            $poiEvent->note = $poi->note;
                            $poiEvent->lat = $poi->lat;
                            $poiEvent->lon = $poi->lon;
                            $poiEvent->sort = $poi->sort;
                            $poiEvent->mtime = date('Y-m-d H:i:s' , time());
                            $poiEvent->ctime = date('Y-m-d H:i:s' , time());
                            $poiEvent->status = $poi->status;
                            $poiEvent->save();
                        }
                    }

                    //copy trip_day_traffic
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $traffics = TripDayTraffic::model()->findAll($criteria);
                    if(!empty($traffics)) {
                        foreach($traffics as $traffic) {
                            $traffic = $traffic->attributes;
                            $new_traffic = new TripDayTraffic;
                            $new_traffic->trip_id = $new_trip_id;
                            $new_traffic->trip_day_id = $new_trip_day_id;
                            $new_traffic->note = $traffic['note'];
                            $new_traffic->from_place = $traffic['from_place'];
                            $new_traffic->from_placeid = $traffic['from_placeid'];
                            $new_traffic->to_place = $traffic['to_place'];
                            $new_traffic->to_placeid = $traffic['to_placeid'];
                            $new_traffic->days = $traffic['days'];
                            $new_traffic->traffic_number = $traffic['traffic_number'];
                            $new_traffic->spend = !empty($traffic['spend']) ? $traffic['spend'] : 0;
                            $new_traffic->currency = $traffic['currency'];
                            $new_traffic->start_hours = $traffic['start_hours'];
                            $new_traffic->start_minutes = $traffic['start_minutes'];
                            $new_traffic->end_hours = $traffic['end_hours'];
                            $new_traffic->end_minutes = $traffic['end_minutes'];
                            $new_traffic->ticketcount = $traffic['ticketcount'];
                            $new_traffic->mode = $traffic['mode'];
                            $new_traffic->status = 0;
                            $new_traffic->mtime = date('Y-m-d H:i:s', time());
                            $new_traffic->ctime = date('Y-m-d H:i:s', time());
                            $new_traffic->save();
                        }
                    }

                }
			}
        }

        //copy trip_note
		$note_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("trip_id='{$sourceId}'");
		$criteria->addCondition("status >='0'");
		$tripNotes = TripNote::model()->findAll($criteria);
		if(!empty($tripNotes)) {
            foreach($tripNotes as $note) {
                $new_trip_note= new TripNote;
                $new_trip_note->trip_id = $new_trip_id;
                $new_trip_note->note = $note->note;
                $new_trip_note->status = $note->status;

                $new_trip_note->save();
            }
        }
		ajax_response(200, '', $trip_list);
    }
}
