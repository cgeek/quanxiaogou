<?php
Yii::import('ext.upyun.QUpyun',true);

class ImageController extends Controller
{
	private $_data;
	
	private $upyun;

	public function actionIndex()
	{
		echo 'image';
	}

	public function actionFilter()
	{
		$format = Yii::app()->request->getParam('format');
		$item_id = Yii::app()->request->getParam('item_id');
		$shop_id = Yii::app()->request->getParam('shop_id');
		$user_id = Yii::app()->request->getParam('user_id');
		$status = Yii::app()->request->getParam('status');
		$page = Yii::app()->request->getParam('page');
		$pageSize = Yii::app()->request->getParam('pageSize');

		$page = empty($page) ? 1 : $page;
		$pageSize = empty($pageSize) ? 10 : $pageSize;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		if(!empty($item_id) ) {
			$criteria->addCondition("item_id={$item_id}");
		}
		if(!empty($shop_id) ) {
			$criteria->addCondition("shop_id={$shop_id}");
		}
		if(!empty($user_id) ) {
			$criteria->addCondition("user_id={$user_id}");
		}
		if(!empty($status) ) {
			$criteria->addCondition("status={$status}");
		}
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		$count = Image::model()->count($criteria);
		$this->_data['total'] = $count;
		$image_list = array();
		if($count > 0) {
			$image_list_db = Image::model()->findAll($criteria);
			foreach($image_list_db as $image_db) {
				$image_db = $image_db->attributes;
				$image_db['image_url_small'] = upimage($image_db['image_hash'], '160x160');
				$image_list[] = $image_db;
			}
		}
		$this->_data['image_list'] = $image_list;

		if( $format == 'web') {
			$this->renderPartial('filter', $this->_data);
		} else {
			ajax_response(200, '', $this->_data);
		}
	}

	public function actionUploadify()
	{
		$tmpFile = CUploadedFile::getInstanceByName('Filedata');//读取图像上传域,并使用系统上传组件上传 
		if(empty($tmpFile) || empty($tmpFile->tempName)) {
			ajax_response(500, '上传失败,文件为空');
		}
		/*
		$verifyToken = md5('unique_salt' . $_POST['timestamp']);
		if (empty($tmpFile) || $_POST['token'] != $verifyToken) {
			ajax_response(500, '参数不正确');
		}
		 */
		$object = Yii::app()->request->getParam('object');
		$object_id = Yii::app()->request->getParam('object_id');

		$image_str = file_get_contents($tmpFile->tempName); 
		$image_info= get_image_info($image_str, $tmpFile->tempName);

		if(empty($image_str) || empty($image_info) || empty($image_info['image_hash']))
		{
			//TODO error_log
			ajax_response(500, '图片错误');
		}
		$image_hash = $image_info['image_hash'];
		$r = Yii::app()->upyun->upload($image_hash, $image_str);
		if(empty($r)) {
			//TODO error_log 
			ajax_response(500, '上传云空间失败');
		}
		$new_image = new Image;
		$new_image->image_hash = $image_hash;
		$new_image->width = isset($image_info['width']) ? $image_info['width'] : 0;
		$new_image->height = isset($image_info['height']) ? $image_info['height'] : 0;
		$new_image->lat = isset($image_info['lat']) ? $image_info['lat'] : 0;
		$new_image->lon = isset($image_info['lon']) ? $image_info['lon'] : 0;
		$new_image->exif = isset($image_info['exif']) ? $image_info['exif'] : 0;
		$new_image->desc = '';
		$new_image->object = isset($object) ? $object: '';
		$new_image->object_id = isset($object_id) ? $object_id: 0;
		$new_image->ctime = date('y-m-d H:i:s', time());
		$new_image->status = 0;

		if($new_image->save()) {
			$new_id = Yii::app()->db->getLastInsertId();
			$image_db = Image::model()->findByPk($new_id);
			if(empty($image_db)) {
				ajax_response(500, '保存图片信息到数据库失败');
			}
			$image_db = $image_db->attributes;
			if(isset($image_db['exif'])) {
				unset($image_db['exif']);
			}
			$image_db['image_url_small'] = upimage($image_db['image_hash'], '160x160');
			$image_db['image_url_medium'] = upimage($image_db['image_hash'], '240x240');
			$image_db['image_url'] = upimage($image_db['image_hash'], 'org');
			ajax_response(200, '', $image_db);
		} else {
			ajax_response(500, '保存图片信息到数据库失败');
		}
	}

	private function _save2upyun($image_hash,$image_string)
	{
		$this->upyun = new Upyun();
		$status = $this->upyun->writeFile('/'. $image_hash, $image_string);
		if(FALSE === $status)
		{
			Yii::log("image upload to upyun error, image_hash: $image_hash", 'error');
		}
	}

	public function actionDelete()
	{
		$id = Yii::app()->request->getParam('image_id');

		$image_db = Image::model()->findByPk($id);
		if(empty($image_db)) {
			ajax_response(404, '找不到图片，无法删除');
		}
		Image::model()->updateByPk($id, array('status' => -1));
		ajax_response(200, '');
	}
}
