<?php

class NoteController extends Controller{
	private $_data;
	
	public $layout = '/layouts/column2';
	
	public function actionIndex(){
		$this->pageTitle = '攻略列表';
		
		//base params
		$page = Yii::app()->request->getParam('page');
		$city_id = isset($_GET['city']) ? intval($_GET['city']) : 0;
		
		$note_list = array();
		
		$pageSize = 20;
		$page = !empty($page) ? intval($page) : 1;
		$status = Yii::app()->request->getParam('status');
		$status = empty($status) ? 0 : $status;
		$offset = ($page - 1) * $pageSize;
		
		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		$criteria->addCondition("status=$status");
		if($city_id){
			$criteria->addCondition("city_id=$city_id");
		}
		
		if(isset($_GET['filter_key']) && $_GET['filter_key']!=''){
			$criteria->addCondition("(`title` like '%{$_GET['filter_key']}%'"
				. " OR `content` like '%{$_GET['filter_key']}%')");
			$this->_data['filter_key'] = $_GET['filter_key'];
		}else{
			$this->_data['filter_key'] = '';
		}
		
		$criteria->order = ' `ctime` DESC';
		
		$count = Note::model()->count($criteria);
		$notes = Note::model()->findAll($criteria);
		if(!empty($notes)) {
			foreach($notes as $note) {
				$note = $note->attributes;
				$note['cover_image_url'] = $note['cover_image'];
				$note['cover_image'] = $this->_getImageIdFromURL($note['cover_image']);
				$note_list[] = $note;
			}
		}
		$this->_data['note_list'] = $note_list;
		
		$city_list = $this->_getCityList($city_id);
		$this->_data['city_list'] = $city_list;
		
		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;

		$this->render('list', $this->_data);
	}
	
	public function actionNew(){
		$this->pageTitle = '新增攻略';
		
		$note = array();
		$note['id'] = 0;
		$this->_data['note'] = $note;

		$city_list = $this->_getCityList();
		$this->_data['city_list'] = $city_list;
		
		$type_list = $this->_getTypeList();
		$this->_data['type_list'] = $type_list;

		$this->render('edit', $this->_data);
	}
	
	public function actionEdit(){
		$this->pageTitle = '编辑攻略';
		$id = Yii::app()->request->getParam('id');
		
		$note = Note::model()->findByPk($id);
		if(empty($note)) {
			throw new CHttpException('404', '找不到POI');
		}
		
		$note = $note->attributes;
		
		$note['cover_image_url'] = $note['cover_image'];
		$note['cover_image'] = $this->_getImageIdFromURL($note['cover_image']);
		
		$this->_data['note'] = $note;
		
		$city_list = $this->_getCityList($note['city_id']);
		$this->_data['city_list'] = $city_list;
		
		$type_list = $this->_getTypeList($note['type']);
		$this->_data['type_list'] = $type_list;
		
		$this->render('edit', $this->_data);
	}
	
	public function actionSaveNote(){
		$accept_fields = array('id', 'title', 'cover_image', 'cover_image_before'
				, 'city_id', 'type', 'content', 'website'
				, 'tags', 'lat', 'lon');
		$data = array();
		$reset_cover_image = 0;
		if(isset($_POST['cover_image']) && isset($_POST['cover_image_before']) && $_POST['cover_image'] != $_POST['cover_image_before']){
			$reset_cover_image = 1;
		}
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				if($field == 'cover_image' || $field == 'cover_image_before'){
					if($field == 'cover_image' && $reset_cover_image){
						$data[$field] = upimage($_POST[$field], '240x240');
					}
				}else{
					$data[$field] = Yii::app()->request->getParam("{$field}");
				}
			}
		}
		if(!empty($data['city_id'])) {
			$city = City::model()->findByPk($data['city_id']);
			if(!empty($city)) {
				$data['city_id'] = $data['city_id'];
				$data['city_name'] = $city['name'];
				$data['country_id'] = $city['country_id'];
			}
		}
		if(!empty($data['id'])) {
			$note = Note::model()->findByPk($data['id']);
			if(empty($note)) {
				ajax_response(404, '找不到攻略信息');
			}
			Note::model()->updateByPk($data['id'], $data);
			ajax_response(200, '');
		} else {
			$new_note = new Note;
			$new_note->city_id = $data['city_id'];
			$new_note->country_id = $data['country_id'];
			$new_note->city_name = $data['city_name'];
			$new_note->title = $data['title'];
			$new_note->type = $data['type'];
			$new_note->cover_image = $data['cover_image'];
			$new_note->content = $data['content'];
			$new_note->website = $data['website'];
			$new_note->tags = $data['tags'];
			$new_note->lat = $data['lat'];
			$new_note->lon = $data['lon'];
			$new_note->status = 0;
			$new_note->mtime = date('Y-m-d H:i:s', time());
			$new_note->ctime = date('Y-m-d H:i:s', time());

			if($new_note->save()) {
				$new_id = Yii::app()->db->getLastInsertId();
				ajax_response(200,'',array('new_poi' => 'ok', 'id' => $new_id));
			} else {
				var_dump($new_note->getErrors());
				ajax_response(500,'添加失败');
			}
		}
	}
	
	public function actionDeleteNote()
	{
		$id = Yii::app()->request->getParam('id');
		$note_db = Note::model()->findByPk($id);
		if(empty($note_db)) {
			ajax_response('404', '找不到攻略');
		}
		$note_db->status = -1;
		if($note_db->save()) {
			ajax_response('200', '');
		} else {
			ajax_response('500', '删除失败');
		}
	}
	
	private function _getTypeList($currentType = ''){
		$type_list_temp = array();
		$type_list_temp['tips'] = '贴士';
		//$type_list_temp['traffic'] = '交通建议';
		$type_list = array();
		reset($type_list_temp);
		while (list($key, $val) = each($type_list_temp)) {
			$type = array();
			$type['type'] = $key;
			$type['name'] = $val;
			if($currentType == $key){
				$type['selected'] = ' selected';
			}else{
				$type['selected'] = '';
			}
			$type_list[] = $type;
		}
		return $type_list;
	}
	
	private function _getCityList($currentCity = 0){
		$city_list = array();
		$criteriaCity = new CDbCriteria;
		$criteriaCity->addCondition("status=1");
		$cities = City::model()->findAll($criteriaCity);
		if(!empty($cities)) {
			foreach($cities as $city){
				$city = $city->attributes;
				if($currentCity == $city['id']){
					$city['selected'] = ' selected';
				}else{
					$city['selected'] = '';
				}
				$city_list[] = $city;
			}
		}
		return $city_list;
	}

	private function _getImageIdFromURL($url){
		$return_value = "";
		if($url){
			if(preg_match("/^http:\/\/?[^\/]+\/([^\_]*)/i",$url, $matches)){
				$return_value = $matches[1];
			}
		}
		return $return_value;
	}
}
