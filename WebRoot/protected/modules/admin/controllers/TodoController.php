<?php

class TodoController extends Controller
{
	private $_data;

	public $layout = '/layouts/column2';

	public function actionIndex()
	{
		$this->pageTitle = '任务管理';

		$check_item_list = array();
		
		$pageSize = 20;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;

		$admin_id = Yii::app()->request->getParam('admin_id');
		if(!empty($admin_id)) {
			$criteria->addCondition("admin_id=$admin_id");
		}
		$criteria->addCondition("status=0");
		$criteria->order = ' `ctime` DESC';
		$count = CheckItem::model()->count($criteria);
		$check_items = CheckItem::model()->findAll($criteria);
		if(!empty($check_items)) {
			foreach($check_items as $check_item) {
				$check_item = $check_item->attributes;

				//admin
				$admin = array();
				if(!empty($check_item['admin_id'])) {
					$admin_db = Admin::model()->findByPk($check_item['admin_id']);
					if(!empty($admin_db)) {
						$admin = $admin_db->attributes;
					}
				}
				$check_item['admin'] = $admin;

				//customer
				$customer = array();
				if(!empty($check_item['customer_id'])) {
					$customer_db = Customer::model()->findByPk($check_item['customer_id']);
					if(!empty($customer_db)) {
						$customer = $customer_db->attributes;
					}
				} 
				$check_item['customer'] = $customer;

				$check_item_list[] = $check_item;
			}
		}
		$this->_data['check_item_list'] = $check_item_list;

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;

		$this->render('index', $this->_data);
	}

	public function actionCheckList()
	{
		$this->pageTitle = 'CheckList';

		$customer_id = Yii::app()->request->getParam('customer_id');
		$customer = Customer::model()->findByPk($customer_id);
		if(empty($customer)) {
			die('not exists');
		}
		$customer = $customer->attributes;
		if($customer['check_item_count'] > 0) {
			$customer['progress'] = intval($customer['check_item_complete_count'] / $customer['check_item_count'] * 100);
		} else {
			$customer['progress'] = 0;
		}
		if($customer['check_item_count'] > 0 && $customer['check_item_count'] == $customer['check_item_complete_count']) {
			$customer['complete'] = true;
		}
		$this->_data['customer'] = $customer;

		$tomorrow  = mktime(11, 0, 0, date("m")  , date("d")+1, date("Y"));
		$tomorrow_str = date('Y-m-d H:i', $tomorrow);

		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id=". $customer_id);
		$criteria->addCondition("status >=0");
		$criteria->order = ' `ctime` ASC';
		$check_items = CheckItem::model()->findAll($criteria);
		if(!empty($check_items)) {
			foreach($check_items as $check_item) {
				$check_item = $check_item->attributes;
				if($check_item['status'] == 1) {
					$check_item['complete'] = TRUE;
				}
				if($check_item['due_date'] != NULL && substr($check_item['due_date'], 0, 4) != '0000'){
					$check_item['due_date_default'] = $check_item['due_date'];
				}else{
					$check_item['due_date_default'] = $tomorrow_str;
				}
				if(strtotime($check_item['due_date']) > time())
				{
					$check_item['time_left'] = human_time_admin_hp(strtotime($check_item['due_date']));
					$check_item['time_left_class'] = 'text-muted';
				}else{
					$check_item['time_left'] = human_time(strtotime($check_item['due_date']));

					$check_item['time_left_class'] = 'text-danger';
					$check_item['title_class'] = 'text-danger';
				}
				$check_item_list[] = $check_item;
			}
		}
		$this->_data['check_item_list'] = $check_item_list;
		$this->_data['tomorrow'] = $tomorrow_str;

		$comment_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("object='customer-log'");
		$criteria->addCondition("object_id='{$customer['id']}'");
		$criteria->addCondition("status=1");
		$criteria->order = ' `ctime` DESC';
		$count = Comment::model()->count($criteria);
		$comments = Comment::model()->findAll($criteria);
		if(!empty($comments)) {
			foreach($comments as $comment) {
				$comment = $comment->attributes;
				$comment_list[] = $comment;
			}
		}
		$this->_data['comment_list'] = $comment_list;

		ajax_response(200, '', $this->_data);
	}


	//check list api
	public function actionCloseCheckItem()
	{
		$item_id = Yii::app()->request->getParam('check_item_id');
		if(empty($item_id)) {
			ajax_response('404', '参数不正确');
		}
		$check_item = CheckItem::model()->findByPk($item_id);
		if(empty($check_item)) {
			ajax_response(404, '找不到信息');
		}
		$check_item->status = -1;
		$check_item->mtime = date('Y-m-d H:i:s', time());
		if($check_item->save()) {
			//TODO 更新
			$this->_data['progress'] = $this->_updateCustomerCount($check_item->customer_id);
			$this->_add_comment($check_item->customer_id, '取消了任务：' . $check_item->title);
			ajax_response(200,'', $this->_data);
		} else {
			ajax_response(500, '操作失败');
		}
	}

	public function actionUpdateCheckItem()
	{
		$item_id = Yii::app()->request->getParam('check_item_id');
		$title = Yii::app()->request->getParam('title');
		$due_date = Yii::app()->request->getParam('due_date');
		if(empty($item_id) || empty($title)) {
			ajax_response('404', '参数不正确');
		}
		$check_item = CheckItem::model()->findByPk($item_id);
		if(empty($check_item)) {
			ajax_response(404, '找不到信息');
		}
		$check_item->title = $title;
		$check_item->due_date = $due_date;
		$check_item->mtime = date('Y-m-d H:i:s', time());
		if($check_item->save()) {
			//TODO 更新
			$this->_data['progress'] = $this->_updateCustomerCount($check_item->customer_id);
			$this->_add_comment($check_item->customer_id, '修改了任务：' . $check_item->title);
			ajax_response(200,'', $this->_data);
		} else {
			ajax_response(500, '操作失败');
		}
	}


	public function actionUpdateCheckItemStatus()
	{
		$item_id = Yii::app()->request->getParam('check_item_id');
		$status = Yii::app()->request->getParam('status');
		if(empty($item_id)) {
			ajax_response('404', '参数不正确');
		}
		$check_item = CheckItem::model()->findByPk($item_id);
		if(empty($check_item)) {
			ajax_response(404, '找不到信息');
		}
		$check_item->status = intval($status);
		$check_item->mtime = date('Y-m-d H:i:s', time());
		if($check_item->save()) {
			//TODO 更新状态
			$this->_data['progress'] = $this->_updateCustomerCount($check_item->customer_id);
			if($status == 1) {
				$this->_add_comment($check_item->customer_id, '完成了任务：' . $check_item->title);
			} else if($status == 0) {
				$this->_add_comment($check_item->customer_id, '取消完成了任务：' . $check_item->title);
			}
			ajax_response(200,'', $this->_data);
		} else {
			ajax_response(500, '操作失败');
		}
	}

	public function actionNewCheckItem()
	{
		$title = Yii::app()->request->getParam('title');
		$due_date = Yii::app()->request->getParam('due_date');
		$customer_id = Yii::app()->request->getParam('customer_id');
		if(empty($title) || empty($due_date) || empty($customer_id)) {
			ajax_response('404', '参数不正确');
		}
		$new_check_item = new CheckItem;
		$new_check_item->title = $title;
		$new_check_item->due_date = $due_date;
		$new_check_item->admin_id = Yii::app()->adminUser->id;
		$new_check_item->customer_id = $customer_id;
		$new_check_item->status = 0;
		$new_check_item->mtime = date('Y-m-d H:i:s', time());
		$new_check_item->ctime = date('Y-m-d H:i:s', time());

		if($new_check_item->save()) {
			$new_check_id = Yii::app()->db->getLastInsertId();
			$check_db = CheckItem::model()->findByPk($new_check_id);
			if(empty($check_db)) {
				ajax_response(500, '操作失败');
			}
			$this->_data['progress'] = $this->_updateCustomerCount($customer_id);
			$this->_data['check_item'] = $check_db->attributes;

			$this->_add_comment($check_item->customer_id, '创建了任务：' . $check_item->title);
			ajax_response(200,'', $this->_data);
		} else {
			ajax_response(500, '操作失败');
		}
	}

	private function _updateCustomerCount($customer_id)
	{
		if(empty($customer_id)) {
			return 0;
		}

		$customer = Customer::model()->findByPk($customer_id);
		if(empty($customer)) {
			return 0;
		}

		$check_item_total = $check_item_complete_count = 0;

		$criteria = new CDbCriteria;
		$criteria->addCondition("status >= 0");
		$criteria->addCondition("customer_id = $customer_id");
		$check_item_total = CheckItem::model()->count($criteria);

		$criteria = new CDbCriteria;
		$criteria->addCondition("status = 1");
		$criteria->addCondition("customer_id = $customer_id");
		$check_item_complete_count = CheckItem::model()->count($criteria);

		$customer->check_item_count = $check_item_total;
		$customer->check_item_complete_count = $check_item_complete_count;

		$customer->save();
		if($check_item_total > 0) {
			return intval($check_item_complete_count / $check_item_total * 100);
		} else {
			return 0;
		}
	}

	private function _add_comment($customer_id, $content) 
	{

		if(empty($customer_id) || empty($content)) {
			return false;
		}
		$new_comment = new Comment;
		$new_comment->content = trim($content);
		$new_comment->user_id = 0;
		$new_comment->admin_id = Yii::app()->adminUser->id;
		$new_comment->object = 'customer-log';
		$new_comment->object_id = $customer_id;
		$new_comment->status = 1;
		$new_comment->ctime = date('Y-m-d H:i:s', time());

		if($new_comment->save()) {
			return true;
		} else {
			return false;
		}
	}

}
