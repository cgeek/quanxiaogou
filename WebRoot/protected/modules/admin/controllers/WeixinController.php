<?php

class WeixinController extends Controller
{
	private $_data;
	public $layout = '/layouts/column2';

	public function actionIndex()
	{
		$this->pageTitle = '微信管理';
		$this->actionMessageList();
	}

	public function actionMessageList()
	{
		$this->pageTitle = '微信消息列表';

		$page = Yii::app()->request->getParam('p');
		$type = Yii::app()->request->getParam('type');

		$message_list = array();

		$pageSize = 20;
		$page = !empty($page) ? intval($page) : 1;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		$criteria->order = ' `CreateTime` DESC';
		$count = WeixinMessage::model()->count($criteria);
		$messages = WeixinMessage::model()->findAll($criteria);
		if(!empty($messages)) {
			foreach($messages as $message) {
				$message = $message->attributes;
				$message_list[] = $message;
			}
		}
		$this->_data['message_list'] = $message_list;

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;
		$this->render('message_list', $this->_data);
	}

	public function actionEditContent()
	{
		$this->pageTitle = '微信内容管理';
		$id = Yii::app()->request->getParam('id');

		$content = WeixinContent::model()->findByPk($id);
		if(!empty($content)) {
			$content = $content->attributes;
		}
		$this->_data['content'] = $content;
		$this->render('edit_content', $this->_data);
	}

	public function actionSaveContent()
	{
		$accept_fields = array('id', 'title', 'cover_image' ,'desc', 'content');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}
		if(!empty($data['id'])) {
			$content = WeixinContent::model()->findByPk($data['id']);
			if(empty($content)) {
				ajax_response(404, '找不到修改的信息');
			}
			WeixinContent::model()->updateByPk($data['id'], $data);
			ajax_response(200, '');
		} else {
			$new_content = new WeixinContent;
			$new_content->title = $data['title'];
			$new_content->desc = $data['desc'];
			$new_content->content = $data['content'];
			$new_content->mtime = date('Y-m-d H:i:s', time());
			$new_content->ctime = date('Y-m-d H:i:s', time());
			$new_content->status = 0;

			if($new_content->save()) {
				$new_content_id = Yii::app()->db->getLastInsertId();
				ajax_response(200,'',array('new_content' => 'ok', 'content_id' => $new_content_id));
			} else {
				var_dump($new_content->getErrors());
				ajax_response(500,'添加失败');
			}
		}

	}

}
