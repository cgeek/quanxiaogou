<?php

class PackageController extends Controller
{
	private $_data;

	public $layout = '/layouts/column2';

	public function actionIndex()
	{
		$this->pageTitle = '产品';

		//base params 
		$type = Yii::app()->request->getParam('type');
		$page = Yii::app()->request->getParam('p');
		$status = Yii::app()->request->getParam('status');

		$type = empty($type) ? 'hotel' : $type;
		$status = empty($status) ? 0 : $status;

		$package_list = array();

		$pageSize = 20;
		$page = !empty($page) ? intval($page) : 1;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		$criteria->addCondition("status=$status");
		$criteria->addCondition("type='{$type}'");
		$criteria->order = ' `ctime` DESC';
		$count = Package::model()->count($criteria);
		$packages = Package::model()->findAll($criteria);
		if(!empty($packages)) {
			foreach($packages as $package) {
				$package = $package->attributes;
				$package_list[] = $package;
			}
		}
		$this->_data['package_list'] = $package_list;

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;

		$this->render('list_' . $type, $this->_data);
	}

	public function actionEdit()
	{
		$this->pageTitle = '编辑产品';
		$package_id = Yii::app()->request->getParam('package_id');

		$package = Package::model()->findByPk($package_id);
		if(empty($package)) {
			throw new CHttpException('404', '找不到宝贝');
		}
		$package = $package->attributes;
		$package['cover_image_url'] = upimage($package['cover_image']);
		//get images
		$this->_data['images'] = $this->_getImagesByObject($package['type'], $package['id']);
		
		$this->_data[$package['type']] = $package;
		$this->render('edit_'. $package['type'], $this->_data);
	}

	public function actionEditDetail()
	{
		$this->pageTitle = '编辑产品';
		$package_id = Yii::app()->request->getParam('package_id');

		$package = Package::model()->findByPk($package_id);
		if(empty($package)) {
			throw new CHttpException('404', '找不到宝贝');
		}
		$package = $package->attributes;
		$package['cover_image_url'] = upimage($package['cover_image']);
		//get images
		$this->_data['images'] = $this->_getImagesByObject($package['type'], $package['id']);
		
		$this->_data[$package['type']] = $package;
		$this->render('edit_detail', $this->_data);
	}

	public function actionAddHotel()
	{
		$this->render('edit_hotel', $this->_data);
	}

	public function actionDeletePackage()
	{
		$package_id = Yii::app()->request->getParam('package_id');
		$package_db = Package::model()->findByPk($package_id);
		if(empty($package_db)) {
			ajax_response('404', '找不到酒店');
		}
		$package_db->status = -1;
		if($package_db->save()) {
			ajax_response('200', '');
		} else {
			ajax_response('500', '删除失败');
		}
	}

	public function actionSaveHotel()
	{
		$accept_fields = array('hotel_id', 'title', 'cover_image' ,'subtitle', 'selling_points');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}
		if(!empty($data['hotel_id'])) {
			$hotel = Package::model()->findByPk($data['hotel_id']);
			if(empty($hotel)) {
				ajax_response(404, '找不到酒店信息');
			}
			Package::model()->updateByPk($data['hotel_id'], $data);
			ajax_response(200, '');
		} else {
			$new_hotel = new Package;
			$new_hotel->title = $data['title'];
			$new_hotel->subtitle = $data['subtitle'];
			$new_hotel->selling_points = $data['selling_points'];
			$new_hotel->type = 'hotel';
			$new_hotel->mtime = date('Y-m-d H:i:s', time());
			$new_hotel->ctime = date('Y-m-d H:i:s', time());

			if($new_hotel->save()) {
				$new_hotel_id = Yii::app()->db->getLastInsertId();
				ajax_response(200,'',array('new_hotel' => 'ok', 'hotel_id' => $new_hotel_id));
			} else {
				var_dump($new_hotel->getErrors());
				ajax_response(500,'添加失败');
			}
		}
	}

	public function actionRooms()
	{
		$this->pageTitle = '编辑产品';
		$package_id = Yii::app()->request->getParam('package_id');
		$package = Package::model()->findByPk($package_id);
		if(empty($package)) {
			throw new CHttpException('404', '找不到宝贝');
		}
		$package = $package->attributes;
		$this->_data[$package['type']] = $package;

		$room_list = array();
		$criteria = new CDbCriteria;
		$criteria->offset = 0;
		$criteria->limit = 100;
		$criteria->addCondition("status >= 0");
		$criteria->addCondition("package_id={$package_id}");
		$criteria->order = ' `ctime` DESC';
		$count = Room::model()->count($criteria);
		$rooms = Room::model()->findAll($criteria);
		if(!empty($rooms)) {
			foreach($rooms as $room) {
				$room = $room->attributes;
				$room['cover_image_url'] = upimage($room['cover_image']);
				$room_list[] = $room;
			}
		}
		$this->_data['room_list'] = $room_list;

		$room_id = Yii::app()->request->getParam('room_id');
		if(!empty($room_id)) {
			$edit_room = Room::model()->findByPk($room_id);
			if(empty($edit_room)) {
				throw new CHttpException('404', '找不到该房型');
			}
			$edit_room = $edit_room->attributes;
			$edit_room['cover_image_url'] = upimage($edit_room['cover_image']);
			$this->_data['images'] = $this->_getImagesByObject('room', $room_id);
			$this->_data['edit_room'] = $edit_room;
		}
		$this->render('edit_rooms', $this->_data);
	}

	public function actionSaveRoom()
	{
		$accept_fields = array('hotel_id','room_id', 'name', 'english_name', 'cover_image', 'price', 'agent_price', 'desc', 'offerings','facilities','size', 'view', 'bed');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}
		if(!empty($data['room_id'])) {
			$room = Room::model()->findByPk($data['room_id']);
			if(empty($room)) {
				ajax_response(404, '找不到房型信息');
			}
			Room::model()->updateByPk($data['room_id'], $data);
			ajax_response(200, '', array('room_id' => $room['id'], 'package_id'=>$room['package_id']));
		} else {
			if(empty($data['hotel_id'])) {
				ajax_response(404, '参数不正确，确实hotel id');
			}
			$new_room = new Room;
			$new_room->package_id = $data['hotel_id'];
			$new_room->name = $data['name'];
			$new_room->desc = $data['desc'];
			$new_room->mtime = date('Y-m-d H:i:s', time());
			$new_room->ctime = date('Y-m-d H:i:s', time());

			if($new_room->save()) {
				$new_room_id = Yii::app()->db->getLastInsertId();
				ajax_response(200,'',array('new_room'=>'ok', 'room_id' => $new_room_id));
			} else {
				var_dump($new_room->getErrors());
				ajax_response(500,'添加失败');
			}
		}
	}

	public function actionDeleteRoom()
	{
		$room_id = Yii::app()->request->getParam('room_id');
		$room_db = Room::model()->findByPk($room_id);
		if(empty($room_db)) {
			ajax_response('404', '找不到房型');
		}
		$room_db->status = -1;
		if($room_db->save()) {
			ajax_response('200', '', array('room_id' => $room_id, 'package_id' => $room_db['package_id']));
		} else {
			ajax_response('500', '删除失败');
		}
	}

	public function actionSaveActivity()
	{
		$accept_fields = array('activity_id', 'title', 'cover_image' ,'subtitle', 'selling_points', 'detail');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}
		if(!empty($data['activity_id'])) {
			$activity = Package::model()->findByPk($data['activity_id']);
			if(empty($activity)) {
				ajax_response(404, '找不到酒店信息');
			}
			Package::model()->updateByPk($data['activity_id'], $data);
			ajax_response(200, '');
		} else {
			$new_activity = new Package;
			$new_activity->title = $data['title'];
			$new_activity->subtitle = $data['subtitle'];
			$new_activity->selling_points = $data['selling_points'];
			$new_activity->type = 'activity';
			$new_activity->mtime = date('Y-m-d H:i:s', time());
			$new_activity->ctime = date('Y-m-d H:i:s', time());

			if($new_activity->save()) {
				$new_activity_id = Yii::app()->db->getLastInsertId();
				ajax_response(200,'',array('new_activity' => 'ok', 'activity_id' => $new_activity_id));
			} else {
				var_dump($new_hotel->getErrors());
				ajax_response(500,'添加失败');
			}
		}
	}


	public function actionImageList()
	{

		$object = Yii::app()->request->getParam('object');
		$object_id = Yii::app()->request->getParam('object_id');

		if(empty($object) || empty($object_id)) {
			ajax_response(404, '参数不正确');
		}

		$p = Yii::app()->request->getParam('p');
		$p = empty($p) ? 1 : $p;

		$this->_data['image_list'] = $this->_getImagesByObject($object, $object_id, $p);
		
		ajax_response(200, '', $this->_data);
	}

	private function _getImagesByObject($object, $object_id , $page = 1, $limit = 50)
	{
		if(empty($object) || empty($object_id)) {
			return array();
		}
		$page = ($page >= 1) ? $page : 1;
		$offset = ($page - 1) * $limit;

		$image_list = array();
		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $limit;
		$criteria->addCondition("status >= 0");
		$criteria->addCondition("object = '{$object}'");
		$criteria->addCondition("object_id = {$object_id}");
		$criteria->order = ' `ctime` ASC';
		$count = Image::model()->count($criteria);
		$images = Image::model()->findAll($criteria);
		if(!empty($images)) {
			foreach($images as $image) {
				$image = $image->attributes;
				$image_list[] = array(
					'id' => $image['id'],
					'image_hash' => $image['image_hash'],
					'image_url' => upimage($image['image_hash'], 'original'),
					'image_url_small' => upimage($image['image_hash'], '160x160'),
					'desc' => empty($image['desc']) ? '' : $image['desc'],
				);
			}
		}
		return $image_list;
	}


	public function actionTaobao()
	{
		$options_token = SystemOptions::model()->findByAttributes(array('key' => 'taobao_access_token'));
		if(empty($options_token)) {
			ajax_response('404', '找不到access token');
		}
		$options_token = $options_token->attributes;
		$access_token = $options_token['value'];
		$items = Yii::app()->taobao->getItemsOnsale($access_token);

		$item_list = array();
		foreach($items['items']['item'] as $item) {

			$item_list[] = $item;
		}
		$this->_data['items'] = $items['items']['item'];
		ajax_response(200,'',$this->_data);
		//$this->render('taobao', $this->_data);
	}

	public function actionTaobaoSkus()
	{
		$options_token = SystemOptions::model()->findByAttributes(array('key' => 'taobao_access_token'));
		if(empty($options_token)) {
			ajax_response('404', '找不到access token');
		}
		$options_token = $options_token->attributes;
		$access_token = $options_token['value'];
		
		$items = Yii::app()->taobao->getSkusByItem('36330434529', $access_token);

		ajax_response($items);
	}
}
