<?php

Yii::import('ext.Notify',true);

class TravelerController extends Controller
{
	private $_data;
	public $layout = '/layouts/column2';

	public function actionIndex()
	{
		$this->pageTitle = '旅客信息列表';

		$traveler_list = array();
		$customer_id = Yii::app()->request->getParam('customer_id');
		if(!empty($customer_id)) {
			$customer = Customer::model()->findByPk($customer_id);
			$customer = $customer->attributes;
			$this->_data['customer'] = $customer;
			$this->_data['customer_id'] = $customer_id;
		}

		$pageSize = 10;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;

		if(!empty($customer_id)) {
			$criteria->addCondition("customer_id=$customer_id");
		}
		$criteria->addCondition("status=0");
		$criteria->order = ' `ctime` DESC';
		$count = Traveler::model()->count($criteria);
		$travelers = Traveler::model()->findAll($criteria);
		if(!empty($travelers)) {
			foreach($travelers as $traveler) {
				$traveler = $traveler->attributes;
				$traveler_list[] = $traveler;
			}
		}
		$this->_data['traveler_list'] = $traveler_list;

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;

		$this->render('index', $this->_data);
	}

	public function actionAdd()
	{
		$this->pageTitle = '添加旅客信息';

		$customer_id = Yii::app()->request->getParam('customer_id');
		if(!empty($customer_id)) {
			$customer = Customer::model()->findByPk($customer_id);
			if(empty($customer)) {
				die('customer not exists');
			}
			$customer = $customer->attributes;
			$this->_data['customer'] = $customer;
			$this->_data['customer_id'] = $customer_id;
		}
		$this->render('edit', $this->_data);
	}

	public function actionEdit()
	{
		$this->pageTitle = '编辑旅客信息';

		$id = Yii::app()->request->getParam('id');
		$criteria = new CDbCriteria;
		$traveler = Traveler::model()->findByPk($id);
		if(empty($traveler)) {
			die('not exists');
		}
		$traveler = $traveler->attributes;
		$customer_id = $traveler['customer_id'];
		if(!empty($customer_id)) {
			$customer = Customer::model()->findByPk($customer_id);
			if(empty($customer)) {
				die('customer not exists');
			}
			$customer = $customer->attributes;
			$this->_data['customer'] = $customer;
			$this->_data['customer_id'] = $customer_id;
		}

		$this->_data['traveler'] = $traveler;
		$this->render('edit', $this->_data);
	}

	public function actionSaveTraveler()
	{
		$accept_fields = array('traveler_id', 'name', 'english_name', 'first_name', 'last_name', 'passport_no',  'passport_validity', 'birthday', 'sex', 'age', 'id_no', 'customer_id','is_customer','flight_info');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}

		if(!empty($data['birthday']) &&  birthday($data['birthday'])) {
			$data['age'] = birthday($data['birthday']);
		}
		if(!empty($data['traveler_id'])) {
			$traveler_id = $data['traveler_id'];
			unset($data['traveler_id']);
			$this->_save_traveler($traveler_id, $data);
		} else {
			$this->_new_traveler($data);
		}
	}

	private function _save_traveler($traveler_id, $data)
	{
		/************** for notify ******************************************/
		$customer  = Customer::model()->findByPk($data['customer_id']);
		$uid       = $customer->uid;
		/********************************************************************/
	
		$traveler_db = Traveler::model()->findByPk($traveler_id);
		if(empty($traveler_db)) {
			ajax_response('404', '找不到该客户信息，无法保存');
		}
		
		Traveler::model()->updateByPk($traveler_id, $data);
		$this->_data['traveler_id'] = $traveler_id;


			
		/************** for notify ******************************************/
	        $remote_server = "http://www.shijieyou.com";


		$name_cn         = "name_cn=" . $data['name'];
		$gender          = "gender=" . $data['sex'];
		$birth_date      = "birth_date=" .  $data['birthday'];
		$first_name_en   = "first_name_en=" . $data['first_name'];
		$last_name_en    = "last_name_en=" . $data['last_name'];
		$passport_no     = "passport_no=" . $data['passport_no'];
		$passport_expire_date = "passport_expire_date=" . $data['passport_validity'];
		$identity_card   = "identity_card=" . $data['id_no'];
		$is_leader       = "leader=" . $data['is_customer'];

		$real_data = "$name_cn&$gender&$birth_date&$first_name_en&$last_name_en&$passport_no&$passport_expire_date&$identity_card&$is_leader";
		$post_data = "uid=$uid&obj=companion&act=update&$real_data";
		$notify = new Notify();
			
		$notify->call($remote_server, $post_data);
		/********************************************************************/



		ajax_response(200,'',$this->_data);
	}

	private function _new_traveler($data)
	{
	
		/************** for notify ******************************************/
		$customer  = Customer::model()->findByPk($data['customer_id']);
		$uid       = $customer->uid;
		/********************************************************************/

		$new_traveler = new Traveler;
		$new_traveler->name = $data['name'];
		$new_traveler->sex = $data['sex'];
		$new_traveler->birthday = $data['birthday'];
		$new_traveler->age = $data['age'];
		$new_traveler->english_name = $data['english_name'];
		$new_traveler->first_name = isset($data['first_name']) ? $data['first_name'] : '';
		$new_traveler->last_name = isset($data['last_name']) ? $data['last_name'] : '';
		$new_traveler->passport_no = isset($data['passport_no']) ? $data['passport_no'] : '';
		$new_traveler->passport_validity = isset($data['passport_validity']) ? $data['passport_validity'] : '';
		$new_traveler->id_no = isset($data['id_no']) ? $data['id_no'] : '';
		$new_traveler->customer_id = $data['customer_id'];
		$new_traveler->is_customer = isset($data['is_customer']) ? $data['is_customer'] : 0;
		$new_traveler->flight_info = isset($data['flight_info']) ? $data['flight_info'] : '';
		$new_traveler->status = 0;

		$new_traveler->mtime = date('Y-m-d H:i:s', time());
		$new_traveler->ctime = date('Y-m-d H:i:s', time());
		if($new_traveler->save()) {
			$this->_data['traveler_id'] = Yii::app()->db->getLastInsertId();
			


						
			/************** for notify ******************************************/
		        //$remote_server = "http://www.shijieyou.com";

			$name_cn         = "name_cn=$new_traveler->name";
			$gender          = "gender=$new_traveler->sex";
			$birth_date      = "birth_date=$new_traveler->birthday";

			$first_name_en   = "first_name_en=$new_traveler->first_name";
			$last_name_en    = "last_name_en=$new_traveler->last_name";
			$passport_no     = "passport_no=$new_traveler->passport_no";
			$passport_expire_date = "passport_expire_date=$new_traveler->passport_validity";
			$identity_card   = "identity_card=$new_traveler->id_no";
			$is_leader       = "leader=$new_traveler->is_customer";
			
			$real_data = "$name_cn&$gender&$birth_date&$first_name_en&$last_name_en&$passport_no&$passport_expire_date&$identity_card&$is_leader";

		        $remote_server = "http://www.shijieyou.com";
			$post_data = "uid=$uid&obj=companion&act=insert&$real_data";			
			$notify = new Notify();
			$notify->call($remote_server, $post_data);

			/********************************************************************/


			ajax_response('200', '', $this->_data);
		} else {
			//var_dump($new_traveler->getErrors());
			ajax_response('500', '保存失败');
		}
	}
}
