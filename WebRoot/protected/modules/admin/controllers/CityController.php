<?php

class CityController extends Controller
{
	public $layout = 'column2';

	public $_data;

	public function actionIndex()
	{
		$this->pageTitle = '目的地管理';

		//base params 
		$page = Yii::app()->request->getParam('page');
		$status = Yii::app()->request->getParam('status');

		$keyword = Yii::app()->request->getParam('keyword');

		$city_list = array();

		$pageSize = 20;
		$page = !empty($page) ? intval($page) : 1;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		//$status = empty($status) ? 1 : $status;
		//$criteria->addCondition("status=$status");
		if(!empty($keyword)) {
			$keyword = mysql_escape_string(trim($keyword));
			$criteria->addCondition("name Like '%$keyword%'");
			$criteria->addCondition("name_english Like '%$keyword%'", "OR");
			$criteria->addCondition("name_pinyin Like '%$keyword%'", "OR");
			$criteria->addCondition("country_name Like '%$keyword%'", "OR");
		} else {
			$status = empty($status) ? 1 : $status;
			$criteria->addCondition("status=$status");
		}
		$criteria->order = ' `id` ASC';
		$count = City::model()->count($criteria);
		$citys = City::model()->findAll($criteria);
		if(!empty($citys)) {
			foreach($citys as $city) {
				$city = $city->attributes;
				$city_list[] = $city;
			}
		}
		$this->_data['city_list'] = $city_list;

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;

		$this->render('index', $this->_data);
	}

	public function actionEdit()
	{
		$this->pageTitle = '目的地编辑';
		//base params 
		$id = Yii::app()->request->getParam('id');
		if(empty($id)) {
			die('no id');
		}

		$city = City::model()->findByPk($id);
		if(empty($city)) {
			die('no city');
		}

		$this->_data['city'] = $city->attributes;

		$this->render('edit', $this->_data);

	}

	public function actionNew()
	{
		$this->pageTitle = '目的地编辑';
		
		$this->render('edit',$this->_data);
	}

	public function actionSave()
	{
		$accept_fields = array('city_id', 'name', 'name_english', 'name_pinyin','cover_image', 'parent_id', 'desc', 'country_id', 'country_name', 'lat', 'lon', 'status');
		$data = array();
		foreach($accept_fields as $field) {
			$data[$field] = Yii::app()->request->getParam("{$field}");
		}

		if(!empty($data['city_id'])) {
			$city = City::model()->findByPk($data['city_id']);
			if(!empty($city)) {
				$city->name = $data['name'];
				$city->name_english = $data['name_english'];
				$city->country_id = $data['country_id'];
				$city->country_name = $data['country_name'];
				$city->name_pinyin = $data['name_pinyin'];
				$city->parent_id = $data['parent_id'];
				$city->cover_image = $data['cover_image'];
				$city->desc = $data['desc'];

				$city->status = empty($data['status']) ?  0 : $data['status'];
				if($city->save()) {
					ajax_response(200,'保存成功', $data);
				} else {
					ajax_response(500,'保存失败');
				}
			} else {
				ajax_response(500,'保存失败');
			}
		} else {
			$new_city = new City;
			$new_city->country_id = $data['country_id'];
			$new_city->name = $data['name'];
			$new_city->name_english = empty($data['name_english']) ? '': $data['name_english'];
			$new_city->name_pinyin = empty($data['name_pinyin']) ? '': $data['name_pinyin'];
			$new_city->cover_image = empty($data['cover_image']) ? '': $data['cover_image'];
			$new_city->parent_id = empty($data['parent_id']) ? 0: $data['parent_id'];
			$new_city->lat = empty($data['lat']) ? 0: $data['lat'];
			$new_city->lon = empty($data['lon']) ? 0: $data['lon'];
			$new_city->desc= empty($data['desc']) ? '': $data['desc'];
			$new_city->country_name = empty($data['country_name']) ? '': $data['country_name'];
			$new_city->status = empty($data['status']) ?  0 : $data['status'];
			$new_city->mtime = date('Y-m-d H:i:s', time());
			$new_city->ctime = date('Y-m-d H:i:s', time());

			if($new_city->save()) {
				$new_id = Yii::app()->db->getLastInsertId();
				ajax_response(200,'',array('new_city' => 'ok', 'id' => $new_id));
			} else {
				var_dump($new_note->getErrors());
				ajax_response(500,'添加失败');
			}
		}


	}
}
