<?php

class DefaultController extends Controller
{
	private $_data;

	public $layout = '/layouts/column1';

	public function filters()
	{
		return array(
			'adminAccess', // perform access control for CRUD operations
		);
	}

	public function filterAdminAccess($filterChain)
	{
		if ($filterChain->action->id==='login' || !Yii::app()->adminUser->isGuest)
			$filterChain->run();
		else
			Yii::app()->adminUser->loginRequired();
	}


	public function actionIndex()
	{
		$this->pageTitle = '管理首页';
		$customer_list = array();

		$criteria = new CDbCriteria;
		$next_30_days = date("Y-m-d",strtotime("+2 year"));
		$day  = date("Y-m-d",time());
		$criteria->addBetweenCondition("start_date", $day, $next_30_days, 'AND');
		$criteria->addCondition("status=1");
		$criteria->order = ' `start_date` ASC';
		$count = Customer::model()->count($criteria);
		$customers = Customer::model()->findAll($criteria);
		if(!empty($customers)) {
			foreach($customers as $customer) {
				$customer = $customer->attributes;
				if(!empty($customer['admin_id'])) {
					$admin = Admin::model()->findByPk($customer['admin_id']);
					if(!empty($admin)) {
						$customer['admin'] = $admin->attributes;
					}
				}
				$customer_list[] = $customer;
			}
		}
		$this->_data['nearest_customer_list'] = $customer_list;
		
		$checkitem_list = array();
		$criteria = new CDbCriteria;
		$next_30_days = date("Y-m-d",strtotime("+2 year"));
		//$criteria->addBetweenCondition("due_date", $day, $next_30_days, 'AND');
		$criteria->addCondition("due_date<='" . $next_30_days . "'");
		$criteria->addCondition("admin_id=" . Yii::app()->adminUser->id);
		$criteria->addCondition("status=0");
		$criteria->order = ' `due_date` ASC';
		$count = CheckItem::model()->count($criteria);
		$checkitems = CheckItem::model()->findAll($criteria);
		if(!empty($checkitems)){
			foreach($checkitems as $checkitem){
				$checkitem = $checkitem->attributes;
                if(empty($checkitem)) {
                    continue;
                }
                $customer = Customer::model()->findByPk($checkitem['customer_id']);
                if(empty($customer)) {
                    continue;
                }
				$checkitem['customer'] = $customer->attributes;
				if(!empty($checkitem['customer']) && $checkitem['customer']['status'] < 0) {
					continue;
				}
				if(strtotime($checkitem['due_date']) < time())
				{
					$checkitem['class_expired'] = 'text-danger';
				}else{
					$checkitem['class_expired'] = '';
				}

				$checkitem_list[] = $checkitem;
			}
		}
		$this->_data['nearest_checkitem_list'] = $checkitem_list;
		
		$this->render('index', $this->_data);
	}
	private function _getAdChart()
	{
		$chart = array(
			'chart' => array('type'=>'line'),
			'title' => array('text'=>'最近7天/customer/ad页面浏览量', 'x'=>-20),
			'yAxis' => array('title'=>array('text'=>'浏览量')),
		);
		$stats = array();
		$days = array();
		$client_names = array();

		for($i=7; $i>=1; $i--)
		{
			$criteria = new CDbCriteria;
			$day  = date("Y-m-d",time() - $i*86400);
			$days[] = date('m-d', time() - $i*86400);

			$criteria->addCondition("date='$day'");
			$criteria->addCondition("object!='page-ad-client-all'");

			$db = Stats::model()->findAll($criteria);
			$stats[$day] = array();

			if(!empty($db)) {
				foreach($db as $row) {
					$stat = $row->attributes;
					$object = $stat['object'];
					$count = $stat['count'];
					preg_match('/^page-ad-client-([^$]*)$/m', $object, $matches);
					if(!empty($matches))
					{
						$client_name = $matches[1];
						if(!in_array($client_name, $client_names))
						{
							$client_names[] = $client_name;
						}
						$stats[$day][$client_name] = $count;
					}
				}
			}
		}
		$series = array();

		foreach($client_names as $client_name)
		{
			$data = array();
			foreach($stats as $date=>$stat)
			{
				if(!isset($stat[$client_name]))
				{
					$count = 0;
				}else{
					$count = $stat[$client_name];
				}
				$data[] = intval($count);
			}

			$series[] = array(
				'name'=>$client_name,
				'data'=>$data,
			);
		}
		$chart['xAxis'] = array('categories' => $days);
		$chart['series'] = $series;
		$this->_data['chart'] = $chart;
	}
	private function _getOrderChart()
	{
		$chart = array(
			'chart' => array('type'=>'line'),
			'title' => array('text'=>'最近14天Order数', 'x'=>-20),
			'yAxis' => array('title'=>array('text'=>'Order')),
		);
		$stats = array();
		$days = array();
		$counts = array();

		for($i=14; $i>=1; $i--)
		{
			$criteria = new CDbCriteria;
			$day  = date("Y-m-d",time() - $i*86400);
			$day_end = date("Y-m-d 23:59:59",time() - $i*86400);
			$days[] = date('m-d', strtotime($day));

			$criteria->addBetweenCondition("ctime", $day, $day_end, 'AND');

			$criteria->addCondition("status>0");
			$count = Customer::model()->count($criteria);
			$counts[] = intval($count);
		}

		$series = array();


			$series[] = array(
				'name'=>'Order Count',
				'data'=>$counts,
			);
		$chart['xAxis'] = array('categories' => $days);
		$chart['series'] = $series;
		$this->_data['chart'] = $chart;
	}
	private function _getWeixinAboutChart()
	{
		$chart = array(
			'chart' => array('type'=>'line'),
			'title' => array('text'=>'最近14天weixin页面浏览数', 'x'=>-20),
			'yAxis' => array('title'=>array('text'=>'Page View')),
		);
		$stats = array();
		$days = array();
		$counts = array();

		for($i=14; $i>=1; $i--)
		{
			$criteria = new CDbCriteria;
			$day  = date("Y-m-d",time() - $i*86400);
			$days[] = date('m-d', strtotime($day));


			$db = Stats::model()->findByAttributes(array('date'=>$day, 'object'=>'page-weixin-about'));
			$count = $db['count'];
			$counts[] = intval($count);
		}

		$series = array();


			$series[] = array(
				'name'=>'weixin/about',
				'data'=>$counts,
			);
		$chart['xAxis'] = array('categories' => $days);
		$chart['series'] = $series;
		$this->_data['chart'] = $chart;
	}

	public function actionStats()
	{
		$type = request()->getParam('type');
		switch($type)
		{
		case 'order':
			$this->_getOrderChart();
			break;
		case 'weixin':
			$this->_getWeixinAboutChart();
			break;
		case 'ad':
		default:
			$this->_getAdChart();
			break;
		}

		$this->render('stats', $this->_data);
	}
	
}
