<?php

class OfflineMapController extends Controller
{
	private $_data = array();

	public $layout = 'column2';

	public function actionIndex()
	{
		$this->pageTitle = '离线地图列表';

		//base params 
		$page = Yii::app()->request->getParam('page');
		$status = Yii::app()->request->getParam('status');

		$keyword = Yii::app()->request->getParam('keyword');

		$map_list = array();

		$pageSize = 20;
		$page = !empty($page) ? intval($page) : 1;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		//$status = empty($status) ? 1 : $status;
		//$criteria->addCondition("status=$status");
		if(!empty($keyword)) {
			$keyword = mysql_escape_string(trim($keyword));
			$criteria->addCondition("name Like '%$keyword%'");
			$criteria->addCondition("title Like '%$keyword%'", "OR");
		} else {
			$status = empty($status) ? 1 : $status;
			$criteria->addCondition("status=$status");
		}
		$criteria->order = ' `id` ASC';
		$count = OfflineMap::model()->count($criteria);
		$maps = OfflineMap::model()->findAll($criteria);
		if(!empty($maps)) {
			foreach($maps as $map) {
				$map = $map->attributes;
				$map_list[] = $map;
			}
		}

		$this->_data['map_list'] = $map_list;

		$this->render('index', $this->_data);
	}

	public function actionNew()
	{
		$this->render('edit');
	}

	public function actionEdit()
	{
		$id = Yii::app()->request->getParam('id', true);

		$offlineMap = OfflineMap::model()->findByPk($id);
		if(!empty($offlineMap)) {
			$offlineMap = $offlineMap->attributes;
			$this->_data['map'] = $offlineMap;
		}

		

		$this->render('edit', $this->_data);
	}

	public function actionSave()
	{
		$accept_fields = array('id', 'title', 'url', 'size', 'country_id','city_id', 'status');
		$data = array();
		foreach($accept_fields as $field) {
			$data[$field] = Yii::app()->request->getParam("{$field}");
		}

		if(!empty($data['id'])) {
			$offlineMap = OfflineMap::model()->findByPk($data['id']);
			OfflineMap::model()->updateByPk($data['id'], $data);
			ajax_response(200,'保存成功', $data);
		} else {
			$new_map = new OfflineMap;
			$new_map->country_id = $data['country_id'];
			$new_map->city_id = $data['city_id'];
			$new_map->title = $data['title'];
			$new_map->url = empty($data['url']) ? '': $data['url'];
			$new_map->status = empty($data['status']) ?  0 : $data['status'];
			$new_map->mtime = date('Y-m-d H:i:s', time());
			$new_map->ctime = date('Y-m-d H:i:s', time());
			$new_map->status = 1;

			if($new_map->save()) {
				$new_id = Yii::app()->db->getLastInsertId();
				ajax_response(200,'',array('new_map' => 'ok', 'id' => $new_id));
			} else {
				var_dump($new_map->getErrors());
				ajax_response(500,'添加失败');
			}
		}


	}

}
