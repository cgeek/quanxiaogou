<!doctype html>
<html>
<head>
	<title><?=isset($trip)? $trip['title'] : '妙妙行程管家';?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />

	<script src="http://cdn.bootcss.com/jquery/1.10.2/jquery.min.js"></script>

	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->
	<?php if(isset($_GET['print']) && $_GET['print'] == 1):?>
	<style>
		@font-face {
			font-family: 'XiHei';
			src: url('/fonts/xihei.ttf') format('truetype');
		}
		@font-face {
			font-family: 'MyOptima';
			src: url('/fonts/optima.ttf') format('truetype');
		}
		body, div, p, td, tr, th {
			font-family: 'XiHei', 'MyOptima';
		}
		<?php if(!isset($_GET['allow_page_break'])):?>
		span {
			display: inline-block;
		}
		<?php endif;?>
	</style>
	<?php endif;?>

</head>
<body>

<div <?php if(!isset($_GET['iframe'])):?>class="container" style="width:845px;"<?php endif;?>>
	<?php if(!isset($_GET['print']) && !isset($_GET['hash_id'])):?>
	<div >
		<a class="pull-right" target="_blank" href="/plan/export/makepdf?url[]=<?=urlencode('http://'.$_SERVER['HTTP_HOST'].'/plan/export/excel?trip_id=' . $trip['id'] . '&print=1');?>">保存PDF</a>
		<a class="pull-left" href="/plan/create/edit?trip_id=<?=$trip['id'];?>">返回修改</a>
		<a class="pull-left" href="/plan/export/excel?hash_id=<?=$trip['hash_id'];?>"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;发给客户的Excel链接</a>
		<a class="pull-left" href="/plan/export/pdf?hash_id=<?=$trip['hash_id'];?>"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;发给客户的PDF版本链接</a>
	</div>
	<?php endif;?>
	<table class="table table-bordered table-striped" style="margin-top:20px; ">
		<thead>
			<tr>
                <th colspan=5 style="color:#fff; text-align:center; font-size:20px;background:#579FC9;"><?=$trip['title'];?>
                    <?php if(isset($_GET['hash_id'])):?>
                        <a href="http://erp.shijieyou.com/plan/export/pdf?from=excel&hash_id=cf9fd273619e79fe8f7079b317356c88" style="font-size:13px; color:#fff; float:right;line-height:30px;" target="_blank">查看100元私人定制案例>>></a>
                    <?php endif;?>
                </th>
			</tr>
			<tr style="background:#ccc;">
				<th style="width:100px;font-size:15px; text-align:center;">日期</th>
				<th style="width:500px;font-size:15px; text-align:center;">行程简介</th>
				<th style="text-align:center;font-size:15px; ">交通</th>
			</tr>
		</thead>
		<tbody>
<?php if(!empty($trip_day_list)):?>
	<?php foreach($trip_day_list as $key => $trip_day):?>
			<tr>
				<td >
					<span>
					D<?=$key+1;?>：<?=date('m-d', strtotime($trip_day['date']));?>
					<p><?=getWeek($trip_day['date']);?>
				<?php if(!empty($trip_day['city_list'])):?>
					</br>
					<?php foreach($trip_day['city_list'] as $key=> $city):?>
						<?=($key > 0) ? ' - ': '';?><?=$city['city_name'];?>
					<?php endforeach; ?>
				<?php endif;?>
					</p>
					</span>
				</td>
				<td>
					<span>
				<?php if(!empty($trip_day['title'])):?>
					<p><strong><?=$trip_day['title'];?></strong></p>
				<?php endif;?>

				<?=nl2br($trip_day['desc']);?>

				<?php if(!empty($trip_day['hotel_list'])):?>
					<?php foreach($trip_day['hotel_list'] as $hotel):?>
						<p style="border-top:1px dashed #ccc; margin-bottom:0; margin-top:8px; font-weight:bold;padding-top:8px;">
							酒店：<?=$hotel['title'];?>
							<?php if(!empty($hotel['spend'])): ?> <span style="font-weight:normal;"> &nbsp;&nbsp;参考价格：<?=$hotel['spend'];?>元/晚</span><?php endif;?>
						</p>
					<?php endforeach; ?>
				<?php endif;?>
					</span>
				</td>
				<td>
				<?php if(!empty($trip_day['traffic_list'])):?>
					<?php foreach($trip_day['traffic_list'] as $traffic):?>
					<p style="border-bottom:1px dashed #ccc; background-image:url('/images/bg_flight.png'); background-repeat:no-repeat;background-position:top right;margin-bottom:5px; margin-top:0px; padding-bottom:5px;">
						航班号：<?=$traffic['traffic_number'];?> <br>
						<?=$traffic['from_place'];?> - <?=$traffic['to_place'];?>  <?=sprintf('%02d', $traffic['start_hours']);?>:<?=sprintf('%02d', $traffic['start_minutes']);?> - <?=sprintf('%02d', $traffic['end_hours']);?>:<?=sprintf('%02d', $traffic['end_minutes']);?>
					</p>
					<?php endforeach; ?>
				<?php endif;?>
					<?=nl2br($trip_day['traffic_note']);?>
				</td>
			</tr>
	<?php endforeach;?>
<?php endif;?>
		</tbody>
    </table>



<?php if(!empty($trip_note)):?>
	<h1 style="height:40px; background:#589FC8; font-size:18px; line-height:40px; text-align:center; color:#fff; width:100%;">出行贴士</h1>
<span style="display:inline-block">
	<?=$trip_note;?>
</span>
<?php endif;?>
</div>

<script>
$(function(){window.name　=　document.body.scrollHeight;});
</script>


<?php if(isset($_GET['hash_id'])):?>
<div style="display:none;">
<script type="text/javascript">
    var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F661ba1e3fc414ca4e7919246483e4d91' type='text/javascript'%3E%3C/script%3E"));
</script>
</div>
<?php endif;?>

</body>
</html>
