<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
	<title><?=$trip['title'];?></title>
	<link href="/css/app_new.css" rel="stylesheet" type="text/css" />
	<script src="/assets/libs/jquery.js" type="text/javascript"></script>
</head>

<body>
<div class="container">
	<div class="hd">
		<h1 class="trip_title"><?=$trip['title'];?></h1>
		<!--p class="trip_info">
			<span class="trip_day_days">旅行用时4天3晚</span>
			<span class="trip_day_money">参考预算：4500/人</span>
		</p>
		<p>
			让孩子与小老虎亲密接触是一次刺激新鲜的体验。让孩子看到各种大小、姿势的大象并且手工DIY是个不错的认知及动手的项目。在夜间与动物约会，相信大人小孩都会喜欢。
		</p-->
	</div>
	<div class="trip_day_list">
<?php if(!empty($trip_day_list)):?>
	<?php foreach($trip_day_list as $key=>$trip_day):?>
		<a name="a_day_<?=$trip_day['id'];?>"></a>
		<div class="trip_day">
			<div class="trip_day_hd clearfix">
				<div class="trip_day_key">D<?=$key+1;?></div>
				<div class="places clearfix">
				<?php foreach($trip_day['city_list'] as $key=>$city):?>
				<span class="tag"><?=$city['city_name'];?> <?php if($key < count($trip_day['city_list']) - 1):;?> - <?php endif;?> </span>
				<?php endforeach;?>
				</div>
			</div>
			<div class="trip_day_detail">
				<div class="trip_day_date"><?=date('m-d', strtotime($trip_day['date']));?>  <?=getWeek($trip_day['date']);?></div>
				<div class="trip_day_title"><?=$trip_day['title'];?></div>
			<?php foreach($trip_day['traffic_list'] as $traffic):?>
				<div class="routebox">
					<div class="citys clearfix">
						<div class="route">
						<div class="city">
							<p class="name"><?=$traffic['from_place'];?></p>
							<p class="time"><span><?=sprintf('%02d', $traffic['start_hours']);?>:<?=sprintf('%02d',$traffic['start_minutes']);?></span></p>
						</div>
						<div class="icon"><img src="/images/app/route_icon_plane.png" ></div>
						<div class="city">
							<p class="name"><?=$traffic['to_place'];?></p>
							<p class="time"><span><?=sprintf('%02d', $traffic['end_hours']);?>:<?=sprintf('%02d', $traffic['end_minutes']);?></span></p>
						</div>
						</div>
					</div>
					<div class="infos">
						<p>班次：<?=$traffic['traffic_number'];?></p>
					</div>
					<?php if(false && !empty($traffic['note'])):?>
						<p class="traffic-remark">备注：<?=nl2br($traffic['note']);?></p>
					<?php endif;?>
				</div>
			<?php endforeach;?>
				<?php if(!empty($trip_day['desc'])):?>
				<p class="trip_day_desc"><?=nl2br($trip_day['desc']);?></p>
             	<?php endif;?>
				<?php if(!empty($trip_day['traffic_note'])):?>
				<p class="trip_day_traffic_note"><strong>交通建议：</strong><br/>
					<?=nl2br($trip_day['traffic_note']);?>
				</p>
             	<?php endif;?>
                <?php if(!empty($trip_day['images'])):?>
                    <?php foreach($trip_day['images'] as $image):?>
				<div class="trip_day_image">
                    <a href="<?=$image['image_url'];?>" target="_blank" onClick="ok.performClick(this.src);">
                        <img src="<?=$image['image_url'];?>">
					    <span class="zoom_btn">点击放大</span>
					</a>
                </div>
                    <?php endforeach;?>
             	<?php endif;?>
			</div>
		</div>

		<?php if(count($trip_day['poi_list_by_type']) > 0):?>
		<div class="trip_day_pois clearfix">
			<?php foreach($trip_day['poi_list_by_type'] as $poi_type=>$pois):?>
				<?php foreach($pois as $poi):?>
			<a class="" href="javascript:void(0);" onClick="popupPoiShow(<?=$poi['id'];?>);">
				<div class="item">
					<div class="poi_cover_image_li">
						<img src="<?=upimage($poi['cover_image']);?>" >
					</div>
					<div class="poi_info">
						<img src="/images/app/poi-icon-<?=$poi_type;?>.png" width="21" height="21">
						<?=$poi['poi_info']['poi_name'];?> &nbsp;&nbsp;
						<?=$poi['poi_info']['poi_name_english'];?> 
						<i class="arrow"></i>
					</div>
				</div>
			</a>
				<div class="item_pop" id="popup_poi_content_<?=$poi['id'];?>" style="display:none;">
					<div class="poi_pop_view">
						<div class="poi_cover_image">
							<img src="<?=upimage($poi['cover_image'], 'org');?>" width=100%  onClick="ok.performClick(this.src);" />
							<i class="popup_poi_close"><img src="/images/app/close.png"></i>
							<span class="poi_name"><?=$poi['poi_info']['poi_name'];?></span>
						</div>
						<div class="poi_detail"> 
								<div class="desc">
									<?=nl2br($poi['poi_info']['desc']);?>
								</div>
								<div class="extend_info" style="border-top:1px dashed #ccc; margin-top:10px;">
									<ul >
										<?php if(!empty($poi['poi_info']['address'])):?>
										<li class="clearfix">
											<span class="tit">地址：</span>
											<div class="txt"><?=$poi['poi_info']['address']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['phone'])):?>
										<li class="clearfix">
											<span class="tit">电话：</span>
											<div class="txt"><?=$poi['poi_info']['phone']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['website'])):?>
										<li class="clearfix">
											<span class="tit">网址：</span>
											<div class="txt" style="word-break:break-all;"><a href="<?=preg_match('/^http/i', $poi['poi_info']['website']) ? $poi['poi_info']['website'] : 'http://' . $poi['poi_info']['website'];?>" target="_blank"><?=$poi['poi_info']['website']?></a></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['price'])):?>
										<li class="clearfix">
											<span class="tit">价格：</span>
											<div class="txt"><?=$poi['poi_info']['price']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['recommend'])):?>
										<li class="clearfix">
											<span class="tit">推荐：</span>
											<div class="txt"><?=$poi['poi_info']['recommend']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['open_time'])):?>
										<li class="clearfix">
											<span class="tit">开放时间：</span>
											<div class="txt">
												<?=nl2br($poi['poi_info']['open_time']);?>
											</div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['traffic'])):?>
										<li class="clearfix">
											<span class="tit">交通建议：</span>
											<div class="txt">
												<?=nl2br($poi['traffic']);?>
											</div>
										</li>
										<?php endif;?>
									</ul>
								</div>
							</div>
						<?php if(!empty($poi['poi_info']['tips'])):?>
						<div class="poi_tips">
							特别提示：<br/>
							<?=nl2br($poi['poi_info']['tips']);?>
						</div>
						<?php endif;?>
						</div>
				</div>
				<?php endforeach;?>
			<?php endforeach;?>
		</div>	
		<?php endif;?>
	<?php endforeach;?>
<?php endif;?>
	</div>
</div>


	<p class="pagemulu" id="pagemulu"><span class="icon" title="目录"></span></p>

	<div class="poupu_mask" id="poupu_mask" style="display:none;"></div>
	<div class="popup_mulu" id="popup_mulu" style="display:none;">
	<div class="toptit">
		<h2 class="title">&nbsp;&nbsp;</h2>
		<p class="close" id="popup_mulu_close"></p>
	</div>
	<div class="content" id="popup_mulu_content">
		<ul class="lists">
		<?php if(!empty($trip_day_list)):?>
			<?php foreach($trip_day_list as $key=>$trip_day):?>
			<li class="<?php if($key == 0):?>firstitem<?php elseif($key == count($trip_day_list) - 1):?>lastitem<?php endif;?>" onclick="popupHide()">
				<a href="#a_day_<?=$trip_day['id'];?>">
					<p class="maskbg"></p>
					<p class="day"><span>D<?=$key+1;?></span></p>
					<h3 class="tit">
					</h3>
					<p class="txt">
						<?=date('m月d日', strtotime($trip_day['date']));?>  <?=getWeek($trip_day['date']);?>
						&nbsp;&nbsp;
						<?php foreach($trip_day['city_list'] as $key=>$city):?>
							<?=$key > 0 ? '-' :'';?><span class="tag"><?=$city['city_name'];?></span>
						<?php endforeach;?>
					</p>
					<p class="title"><?=$trip_day['title'];?></p>
				</a>
			</li>
			<?php endforeach;?>
		<?php endif;?>
		</ul>
	</div>
</div>


<div class="popup_poi" id="popup_poi" style="display:none;">
	<!--p class="btmshade"></p-->
	<div class="content" id="popup_poi_content">
			
	</div>
</div>

<script>
var win_w = $(window).width();
var win_h = $(window).height();
var poupu_mask = $("#poupu_mask");
var popup_poi = $("#popup_poi");
var popup_mulu = $("#popup_mulu");
var scrolltop;

//公用隐藏函数
function popupHide(){
	$("html,body").css({"height":"","overflow":""});
	poupu_mask.hide();
	popup_poi.removeClass("popup_layshow").css({"display":"none"});
	popup_mulu.removeClass("popup_layshow").css({"display":"none"});
	$(window).scrollTop(scrolltop);
}

//公用显示函数
function popupShow(){
	scrolltop = $(window).scrollTop();
	$("html,body").css({"height":win_h,"overflow":"hidden"});
	poupu_mask.show();
}

//POI显示函数
function popupPoiShow(poi_id){
	$("#js_popup_poi_title").html($("#js_popup_poi_title_" + poi_id).html());
	$("#popup_poi_content").html($("#popup_poi_content_" + poi_id).html());
	popupShow();
	popup_poi.show().addClass("popup_layshow");;
	var left = popup_poi.offset().left;
	var height = win_h - left * 2 - 45;

	$("#popup_poi_content").height(height).scrollTop(0);
}

//Hotel显示函数
function popupHotelShow(hotel_id){
	$("#js_popup_poi_title").html($("#js_popup_hotel_title_" + hotel_id).html());
	$("#popup_poi_content").html($("#popup_hotel_content_" + hotel_id).html());
	popupShow();
	popup_poi.show().addClass("popup_layshow");;
	var left = popup_poi.offset().left;
	var height = win_h - left * 2 - 45;
	$("#popup_poi_content").height(height).scrollTop(0);
}

$("body").delegate('.popup_poi_close', 'click', function() {
	popupHide();
});

//mulu显示函数
function popupMuluShow(){
	popupShow();
	popup_mulu.show().addClass("popup_layshow");
	var left = popup_mulu.offset().left;
	var height = win_h - left - 45 - 56;
	$("#popup_mulu_content").height(height).scrollTop(0);
}
$("#pagemulu").click(function(){
	popupMuluShow();
});


$("#popup_mulu_close").click(function(){
	popupHide();
});

$("#poupu_mask").click(function(){
	popupHide();
});



//加载更多...
$(function(){
	/*
	$(".planday .intro").each(function(){
		var height = $(this).height();
		if(height > 50){
			$(this).parents(".planday").find(".btn_updown").show();
		}
		//console.log(height);
	});
	$(".planday .btn_updown").toggle(function(){
		$(this).parents(".planday").find(".intros").css("max-height","none");
		$(this).find("span").addClass("fold").text("点击收起");
	},function(){
		$(this).parents(".planday").find(".intros").css("max-height","54px");
		$(this).find("span").removeClass("fold").text("点击查看更多");
	});
	*/
});
</script>
</body>
</html>

