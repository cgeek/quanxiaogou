<!doctype html>
<html>
<head>
	<title><?=isset($trip)? $trip['title'] : '妙妙行程管家';?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->
	<?php if(isset($_GET['print']) && $_GET['print'] == 1):?>
	<style>
		@font-face {
			font-family: 'XiHei';
			src: url('/fonts/xihei.ttf') format('truetype');
		}
		@font-face {
			font-family: 'MyOptima';
			src: url('/fonts/optima.ttf') format('truetype');
		}
		body, div, p, td, tr, th {
			font-family: 'XiHei', 'MyOptima';
		}
		<?php if(!isset($_GET['allow_page_break'])):?>
		span {
			display: inline-block;
		}
		<?php endif;?>
	</style>
	<?php endif;?>

</head>
<body>

<div class="container" style="width:1150px;">
	<h1 style="height:50px; background:#589FC8; font-size:25px; line-height:50px; text-align:center; color:#fff;">出行贴士</h1>
	<?=$trip_note['note'];?>
</div>
