<!doctype html>
<html>
<head>
	<title><?=isset($trip)? $trip['title'] : '妙妙行程管家';?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/pdf.css" />
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->
	<?php if(false && isset($_GET['print']) && $_GET['print'] == 1):?>
	<style>
		@font-face {
			font-family: 'XiHei';
			src: url('/fonts/xihei.ttf') format('truetype');
		}
		@font-face {
			font-family: 'Microsoft_Sans_Serif';
			src: url('/fonts/Microsoft_Sans_Serif.ttf') format('truetype');
		}
		body, div, p, td, tr, th {
			font-family: 'Microsoft_Sans_Serif', 'XiHei';
		}
		table, th, tr { border:1px solid #B6B5B5;}
		<?php if(!isset($_GET['allow_page_break'])):?>
		span {
			display: inline-block;
		}
		<?php endif;?>
	</style>
	<?php endif;?>

</head>
<body>

<div <?php if(!isset($_GET['iframe'])):?>class="container" style="width:980px;"<?php endif;?> >
	<?php if(!empty(Yii::app()->adminUser->id) && !isset($_GET['iframe']) ):?>
    <div <?php if(isset($_GET['hash_id'])):?>style="display:none;"<?php endif;?>>
		<a class="pull-right" target="_blank" href="/plan/export/makepdf?url[]=<?=urlencode('http://'.$_SERVER['HTTP_HOST'].'/plan/export/pdf?trip_id=' . $trip['id'] . '&print=1&admin_id=' . $trip['admin_id']);?>">保存PDF</a>
		<a class="pull-left" href="/plan/create/edit?trip_id=<?=$trip['id'];?>">返回修改</a>
	</div>
	<?php endif;?>
	<div class="pdf-title clearfix">
		<h1><?=$trip['title'];?></h1>
		<img src="/images/pdf/biaoyu.gif">
	</div>
	<div class="contact_card clearfix">
		<div class="info">
            <ul>
<?php if(!empty($trip_admin)):?>
                <li>专属管家：<?=$trip_admin['nick_name'];?></li>
				<li>联系电话：<?=!empty($trip_admin['mobile']) ? $trip_admin['mobile'] : '400-728-0727';?></li>
<?php else:?>
                <li>专属管家：CherryCha</li>
				<li>联系电话：400-728-0727</li>

<?php endif;?>
			</ul>
			<!--div class="m20" style="line-height:60px; height:60px;">扫描二维码在手机或者平板电脑上看行程<img src="/images/pdf/go.png"></div-->
		</div>
		<div class="qr_box">
			<!--img src="http://chart.apis.google.com/chart?cht=qr&chld=H&chs=150x150&chl=http://erp.shijieyou.com/plan/export/app?hash_id=<?=$trip['hash_id'];?>" width="150px">
			<img src="/images/pdf/happy_trip.png"-->
		</div>
	</div>
	<table class="table table-bordered table-striped" style="margin-top:20px;">
		<thead>
			<tr>
				<th colspan=5 style="text-align:center; font-size:20px;background:#589FC8; color:#fff;"><?=$trip['title'];?></th>
			</tr>
			<tr style="background:#CCCCCC; color:#666;">
				<th style="width:100px;font-size:15px; text-align:center;">日期</th>
				<th style="width:570px;font-size:15px; text-align:center;">行程简介</th>
				<th style="width:310px; text-align:center;font-size:15px; ">交通</th>
				<!--th style="width:210px; text-align:center;font-size:15px; ">住宿</th-->
			</tr>
		</thead>
		<tbody>
<?php if(!empty($trip_day_list)):?>
	<?php foreach($trip_day_list as $key => $trip_day):?>
			<tr>
				<td class="text-center" style="vertical-align:middle;">
					DAY
					<div class="one_day_key_s"><?=$key+1;?></div>
					<div class="one_day_time_table">
						<?=date('m月d', strtotime($trip_day['date']));?><br>
						<?=getWeek($trip_day['date']);?>
					</div>
					<p>
					<span class="pdf_table_citys">
				<?php if(!empty($trip_day['city_list'])):?>
					<?php foreach($trip_day['city_list'] as $key=> $city):?>
						<?=($key > 0) ? ' - ': '';?><?=$city['city_name'];?>
					<?php endforeach; ?>
				<?php endif;?>
					</span>
					</p>
				</td>
				<td>
					<span>
				<?php if(!empty($trip_day['title'])):?>
					<p style="border-bottom:1px dashed #ccc;padding-bottom:10px;"><strong><?=$trip_day['title'];?></strong></p>
				<?php endif;?>

				<?=nl2br($trip_day['desc']);?>
				<?php if(!empty($trip_day['hotel_list'])):?>
					<?php foreach($trip_day['hotel_list'] as $hotel):?>
						<p style="border-top:1px dashed #ccc; margin-bottom:0; margin-top:8px; font-weight:bold;padding-top:8px;">
							酒店：<?=$hotel['title'];?>
							<?php if(!empty($hotel['spend'])): ?> <span style="font-weight:normal;"> &nbsp;&nbsp;参考价格：<?=$hotel['spend'];?>元/晚</span><?php endif;?>
						</p>
					<?php endforeach; ?>
				<?php endif;?>

					</span>
				</td>
				<td>
				<?php if(!empty($trip_day['traffic_list'])):?>
					<ul class="excel_traffic_list">
					<?php foreach($trip_day['traffic_list'] as $traffic):?>
					<li style="">
						航班号：<?=$traffic['traffic_number'];?> <br>
						<?=$traffic['from_place'];?> - <?=$traffic['to_place'];?>  <?=sprintf('%02d', $traffic['start_hours']);?>:<?=sprintf('%02d', $traffic['start_minutes']);?> - <?=sprintf('%02d', $traffic['end_hours']);?>:<?=sprintf('%02d', $traffic['end_minutes']);?>
					</li>
					<?php endforeach; ?>
					</ul>
				<?php endif;?>
					<?=nl2br($trip_day['traffic_note']);?>
				</td>
				<!--td>
				<?php if(!empty($trip_day['hotel_list'])):?>
					<?php foreach($trip_day['hotel_list'] as $hotel):?>
						<p>
							<?=$hotel['title'];?>
								<br>
							<?php if(!empty($hotel['spend'])): ?> <span style="font-weight:normal;">参考价格：<?=$hotel['spend'];?>元/晚</span><?php endif;?>
						</p>
					<?php endforeach; ?>
				<?php endif;?>
					
				</td-->
			</tr>
	<?php endforeach;?>
<?php endif;?>
		</tbody>
	</table>




<div class="trip_detail" style="margin-top:50px;">
<?php if(!empty($trip_day_list)):?>
	<?php foreach($trip_day_list as $key => $trip_day):?>
	<div class="one_day" style="margin-bottom:50px;">
		<div class="one_day_key" style="float:left;width:100%;display:inline-block;">
			<div class="the_key" style="float:left; display:inline-block;">
				<span style="display:inline-block; width:100%; ">D<?=$key+1;?></span>
			</div>
			<div style=" margin-left:70px;">
				<p style="font-size:16px; "><?=date('m-d', strtotime($trip_day['date']));?> <?=getWeek($trip_day['date']);?></p>
				<p style="font-size:20px; font-weight:bold; margin-top:5px;">
			<?php if(!empty($trip_day['city_list'])):?>
					<?php foreach($trip_day['city_list'] as $key=> $city):?>
						<?=($key > 0) ? ' - ': '';?><?=$city['city_name'];?>
					<?php endforeach; ?>
			<?php endif;?>
				<?php if(!empty($trip_day['title'])):?>
					&nbsp;&nbsp;&nbsp;&nbsp;<?=$trip_day['title'];?>
				<?php endif;?>
				</p>
			</div>

		</div>
		<div class="one_day_detail" style="margin-left:70px;">
			<div class="one_day_traffic clearfix">
				<?php if(!empty($trip_day['traffic_list'])):?>
					<?php foreach($trip_day['traffic_list'] as $traffic):?>
				<div style="display:inline-block;">
					<div class="routebox clearfix">
						<div class="citys">
							<div class="city">
								<p class="name"><?=$traffic['from_place'];?> </p>
								<p class="time">
									<span><?=sprintf('%02d', $traffic['start_hours']);?>:<?=sprintf('%02d', $traffic['start_minutes']);?> </span>
								</p>
							</div>
							<div class="icon">
								<img src="/images/pdf/flight.png"  alt="">
							</div>
							<div class="city">
								<p class="name"><?=$traffic['to_place'];?></p>
								<p class="time">
								<span>
									<?=sprintf('%02d', $traffic['end_hours']);?>:<?=sprintf('%02d', $traffic['end_minutes']);?>
								</span></p>
							</div>
						</div>
						<div class="infos" style="margin-top:5px;">
							<p style="margin-bottom:0;">班次：<?=$traffic['traffic_number'];?></p>
							<?php if(!empty($traffic['spend'])):?>
							<p style="margin-bottom:0;">费用：<?=$traffic['spend'];?> x <?=$traffic['ticketcount'];?> CNY</p>
							<?php endif;?>
						</div>
					</div>
				</div>
					<?php endforeach; ?>
				<?php endif;?>
			</div>
			<div class="one_day_desc">
				<?=nl2br($trip_day['desc']);?>
			</div>
			<?php if(!empty($trip_day['traffic_note'])):?>
			<div class="one_day_desc" style="border-top:1px dashed #ccc;padding-top:10px; margin-top:10px;">
				<strong>交通建议：</strong><br><?=nl2br($trip_day['traffic_note']);?>
			</div>
			<?php endif;?>
            <?php if(!empty($trip_day['images'])):?>
                <?php foreach($trip_day['images'] as $image):?>
			<div class="one_day_image" style=" margin-top:10px;">
                <img src="<?=$image['image_url'];?>" style="max-height:600px; max-width:800px;">
            </div>
                <?php endforeach;?>
			<?php endif;?>

		<?php if(count($trip_day['poi_list_by_type']) > 0):?>
			<div class="one_day_poi_list">
			<?php foreach($trip_day['poi_list_by_type'] as $poi_type=>$pois):?>
			<?php if(!empty($pois)):?>
				<div class="poi_list">
					<div class="pin_type"><span><?=$poi_type_cn[$poi_type];?></span></div>
					<ul class="poilists">
					<?php foreach($pois as $poi):?>
					<li class="clearfix">
						<div class="poi_detail">
							<div class="title">
							<h4><?=$poi['poi_info']['poi_name'];?>
								<?=!empty($poi['poi_info']['poi_name_english']) ? ' / ' .$poi['poi_info']['poi_name_english'] :'' ;?>
								<?=!empty($poi['poi_info']['poi_name_local']) ? ' / ' .$poi['poi_info']['poi_name_local'] :'' ;?></h4>
							</div>
							<div class="content"> 
								<div class="intros">
									<p class="text"><?=nl2br($poi['desc']);?></p>
								</div>
								<div class="extend_info" style="border-top:1px solid #ccc;">
									<ul >
										<?php if(!empty($poi['poi_info']['address'])):?>
										<li class="clearfix">
											<span class="tit">地址：</span>
											<div class="txt"><?=$poi['poi_info']['address']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['phone'])):?>
										<li class="clearfix">
											<span class="tit">电话：</span>
											<div class="txt"><?=$poi['poi_info']['phone']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['website'])):?>
										<li class="clearfix">
											<span class="tit">网址：</span>
											<div class="txt" style="word-break:break-all;"><a href="<?=preg_match('/^http/i', $poi['poi_info']['website']) ? $poi['poi_info']['website'] : 'http://' . $poi['poi_info']['website'];?>" target="_blank"><?=$poi['poi_info']['website']?></a></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['price'])):?>
										<li class="clearfix">
											<span class="tit">价格：</span>
											<div class="txt"><?=$poi['poi_info']['price']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['recommend'])):?>
										<li class="clearfix">
											<span class="tit">推荐：</span>
											<div class="txt"><?=$poi['poi_info']['recommend']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['open_time'])):?>
										<li class="clearfix">
											<span class="tit">开放时间：</span>
											<div class="txt">
												<?=nl2br($poi['poi_info']['open_time']);?>
											</div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['traffic'])):?>
										<li class="clearfix">
											<span class="tit">交通建议：</span>
											<div class="txt">
												<?=nl2br($poi['traffic']);?>
											</div>
										</li>
										<?php endif;?>
									</ul>
								</div>
							</div>
						</div>
						<div class="cover_image" style="float:right;">
							<img src="<?=upimage($poi['cover_image']);?>" alt="" width="180" height="130" onerror="this.src='/images/app/poi_48_48.png';"/>
						</div>
						</li>
					<?php endforeach;?>
					</ul>
				</div>
			<?php endif;?>
			<?php endforeach;?>
			</div>
		<?php endif;?>
		</div>
	</div>
	<?php endforeach;?>
<?php endif;?>
</div>

<?php if(!empty($trip_note)):?>
	<h1 style="height:50px; background:#589FC8; font-size:25px; line-height:50px; text-align:center; color:#fff; width:100%;">出行贴士</h1>
<span style="display:inline-block">
	<?=$trip_note;?>
</span>
<?php endif;?>

</div>

<?php if(isset($_GET['hash_id'])):?>
<div style="display:none;">
<script type="text/javascript">
    var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
    document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F661ba1e3fc414ca4e7919246483e4d91' type='text/javascript'%3E%3C/script%3E"));
</script>
</div>
<?php endif;?>

</body>
</html>

