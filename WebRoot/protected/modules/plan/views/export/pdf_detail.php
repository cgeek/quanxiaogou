<!doctype html>
<html>
<head>
	<title><?=isset($trip)? $trip['title'] : '妙妙行程管家';?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/pdf.css" />
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->
	<?php if(isset($_GET['print']) && $_GET['print'] == 1):?>
	<style>
		@font-face {
			font-family: 'XiHei';
			src: url('/fonts/xihei.ttf') format('truetype');
		}
		@font-face {
			font-family: 'MyOptima';
			src: url('/fonts/optima.ttf') format('truetype');
		}
		body, div, p, td, tr, th {
			font-family: 'XiHei', 'MyOptima';
		}
		table, th, tr { border:1px solid #B6B5B5;}
		<?php if(!isset($_GET['allow_page_break'])):?>
		span {
			display: inline-block;
		}
		<?php endif;?>
	</style>
	<?php endif;?>

</head>
<body>

<div class="container" style="width:950px;">

<div class="trip_detail" style="margin-top:50px;">
<?php if(!empty($trip_day_list)):?>
	<?php foreach($trip_day_list as $key => $trip_day):?>
	<div class="one_day" style="margin-bottom:50px;">
		<div class="one_day_key" style="float:left;width:100%;display:inline-block;">
			<div class="the_key" style="float:left; display:inline-block;">
				<span style="display:inline-block; width:100%; ">D<?=$key+1;?></span>
			</div>
			<div style=" margin-left:70px;">
				<p style="font-size:16px; "><?=date('m-d', strtotime($trip_day['date']));?> <?=getWeek($trip_day['date']);?></p>
				<p style="font-size:20px; font-weight:bold; margin-top:5px;">
			<?php if(!empty($trip_day['city_list'])):?>
					<?php foreach($trip_day['city_list'] as $key=> $city):?>
						<?=($key > 0) ? ' - ': '';?><?=$city['city_name'];?>
					<?php endforeach; ?>
			<?php endif;?>
				<?php if(!empty($trip_day['title'])):?>
					&nbsp;&nbsp;&nbsp;&nbsp;<?=$trip_day['title'];?>
				<?php endif;?>
				</p>
			</div>

		</div>
		<div class="one_day_detail" style="margin-left:70px;">
			<div class="one_day_traffic clearfix">
				<?php if(!empty($trip_day['traffic_list'])):?>
					<?php foreach($trip_day['traffic_list'] as $traffic):?>
				<div style="display:inline-block;">
					<div class="routebox clearfix">
						<div class="citys">
							<div class="city">
								<p class="name"><?=$traffic['from_place'];?> </p>
								<p class="time">
									<span><?=sprintf('%02d', $traffic['start_hours']);?>:<?=sprintf('%02d', $traffic['start_minutes']);?> </span>
								</p>
							</div>
							<div class="icon">
								<img src="/images/pdf/flight.png"  alt="">
							</div>
							<div class="city">
								<p class="name"><?=$traffic['to_place'];?></p>
								<p class="time">
								<span>
									<?=sprintf('%02d', $traffic['end_hours']);?>:<?=sprintf('%02d', $traffic['end_minutes']);?>
								</span></p>
							</div>
						</div>
						<div class="infos" style="margin-top:5px;">
							<p style="margin-bottom:0;">班次：<?=$traffic['traffic_number'];?></p>
							<p style="margin-bottom:0;">费用：<?=$traffic['spend'];?> x <?=$traffic['ticketcount'];?> CNY</p>
						</div>
					</div>
				</div>
					<?php endforeach; ?>
				<?php endif;?>
			</div>
			<div class="one_day_desc">
				<?=nl2br($trip_day['desc']);?>
			</div>

		<?php if(count($trip_day['poi_list_by_type']) > 0):?>
			<div class="one_day_poi_list">
			<?php foreach($trip_day['poi_list_by_type'] as $poi_type=>$pois):?>
			<?php if(!empty($pois)):?>
				<div class="poi_list">
					<div class="pin_type"><span><?=$poi_type_cn[$poi_type];?></span></div>
					<ul class="poilists">
					<?php foreach($pois as $poi):?>
					<li class="clearfix">
						<div class="poi_detail">
							<div class="title">
								<h4><?=$poi['poi_name'];?></h4>
							</div>
							<div class="content"> 
								<div class="intros">
									<p class="text"><?=nl2br($poi['desc']);?></p>
								</div>
								<div class="extend_info" style="border-top:1px solid #ccc;">
									<ul >
										<li class="clearfix">
											<span class="tit">地址：</span>
											<div class="txt"><?=$poi['poi_info']['address']?></div>
										</li>
										<?php if(!empty($poi['poi_info']['phone'])):?>
										<li class="clearfix">
											<span class="tit">电话：</span>
											<div class="txt"><?=$poi['poi_info']['phone']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['website'])):?>
										<li class="clearfix">
											<span class="tit">网址：</span>
											<div class="txt" style="word-break:break-all;"><a href="<?=preg_match('/^http/i', $poi['poi_info']['website']) ? $poi['poi_info']['website'] : 'http://' . $poi['poi_info']['website'];?>" target="_blank"><?=$poi['poi_info']['website']?></a></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['price'])):?>
										<li class="clearfix">
											<span class="tit">价格：</span>
											<div class="txt"><?=$poi['poi_info']['price']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['recommend'])):?>
										<li class="clearfix">
											<span class="tit">推荐：</span>
											<div class="txt"><?=$poi['poi_info']['recommend']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['open_time'])):?>
										<li class="clearfix">
											<span class="tit">开放时间：</span>
											<div class="txt">
												<?=nl2br($poi['poi_info']['open_time']);?>
											</div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['traffic'])):?>
										<li class="clearfix">
											<span class="tit">交通建议：</span>
											<div class="txt">
												<?=nl2br($poi['traffic']);?>
											</div>
										</li>
										<?php endif;?>
									</ul>
								</div>
							</div>
						</div>
						<div class="cover_image" style="float:right;">
							<img src="<?=$poi['cover_image'];?>" alt="" width="210" height="155" onerror="this.src='/images/app/poi_48_48.png';"/>
						</div>
						</li>
					<?php endforeach;?>
					</ul>
				</div>
			<?php endif;?>
			<?php endforeach;?>
			</div>
		<?php endif;?>
		</div>
	</div>
	<?php endforeach;?>
<?php endif;?>

</div>

</body>
</html>
