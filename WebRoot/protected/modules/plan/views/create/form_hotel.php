<?php if(!empty($hotel)):?>
<form action="#" method="POST" id="jsplans_pophotel_form">
	<div class="pl_poi_box">
		<div class="title clearfix fontYaHei">
			<img src="<?=upimage($hotel['cover_image']);?>" onerror="this.src='/images/plan/pl_hotel_default.png';" width="80" height="80" alt="" id="js_plans_pophotel_pic"/>
			<p class="btit" id="js_plans_addhotelevent_show"><strong><?=$hotel['title'];?></strong></p>
			<input type="text" name="title" style="display:none;" class="ui2_input" value="<?=$hotel['title'];?>" placeholder="请输入酒店的名字" id="js_plans_addhotelevent" />
			<a href="/">添加?</a>
			<div class="clearfix title_tpap" style="display:none;" id="js_plans_searchhotel_result">
				<div class="title_list">
					<ul>
					</ul>
				</div>
			</div>
		</div>
		<div class="list hotel_list_cnt clearfix">
			<div class="item">
				<div class="text fontSong">参考价格:</div>
				<div class="cnt">
					<div class="money_select">
						<input type="text" name="spend" value="<?=$hotel['spend'];?>" id="jsSignlePrice" class="ui2_input _jsspendinput" placeholder=""/>
						<input type="hidden" name="currency" value="<?=$hotel['currency'];?>" />
						<div class="tra_select">
							<strong class="titles"><span>人民币</span></strong>
							<div class="contents">
								<ul>
									<li><a href="javascript:;" data-value="CNY">人民币</a></li>
                                    <li><a href="javascript:;" data-value="USD">美元</a></li>
                                    <li><a href="javascript:;" data-value="MYR">马来西亚令吉</a></li>
                                    <li><a href="javascript:;" data-value="THB">泰铢</a></li>
                                    <li><a href="javascript:;" data-value="TWD">新台币</a></li>
                                    <li><a href="javascript:;" data-value="SGD">新加坡元</a></li>
                                    <li><a href="javascript:;" data-value="MOP">澳门币</a></li>
                                    <li><a href="javascript:;" data-value="HKD">港元</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="item number">
				<div class="text fontSong">房间数量:</div>
				<div class="cnt">
					<input type="hidden" name="ticketcount" value="<?=$hotel['ticketcount'];?>">
					<div class="tra_select" id="_jspoiticketcount">
						<strong class="titles cny_bg"><span><?=$hotel['ticketcount'];?></span></strong>
						<div class="contents">
							<ul>
                            	<li><a data-value="1" href="javascript:;">1</a></li>
                                <li><a data-value="2" href="javascript:;">2</a></li>
                                <li><a data-value="3" href="javascript:;">3</a></li>
                                <li><a data-value="4" href="javascript:;">4</a></li>
                                <li><a data-value="5" href="javascript:;">5</a></li>
                                <li><a data-value="6" href="javascript:;">6</a></li>
                                <li><a data-value="7" href="javascript:;">7</a></li>
                                <li><a data-value="8" href="javascript:;">8</a></li>
                                <li><a data-value="9" href="javascript:;">9</a></li>
                                <li><a data-value="10" href="javascript:;">10</a></li>
                                <li><a data-value="11" href="javascript:;">11</a></li>
                                <li><a data-value="12" href="javascript:;">12</a></li>
                                <li><a data-value="13" href="javascript:;">13</a></li>
                                <li><a data-value="14" href="javascript:;">14</a></li>
                                <li><a data-value="15" href="javascript:;">15</a></li>
                                <li><a data-value="16" href="javascript:;">16</a></li>
                                <li><a data-value="17" href="javascript:;">17</a></li>
                                <li><a data-value="18" href="javascript:;">18</a></li>
                                <li><a data-value="19" href="javascript:;">19</a></li>
                                <li><a data-value="20" href="javascript:;">20</a></li>
                            </ul>
                        </div>
                    </div>
                    <p class="money" id="_jspoicountspend"></p>
                </div>
            </div>
        </div>
		<div class="textarea">
			<textarea name="note" id="js_plans_pophotel_note" placeholder="在这里添加备注" class="ui2_textarea" rel-tip="#js_plans_pophotel_note_tip"><?=$hotel['note'];?></textarea>
			<div class="clearfix pr">
				<p class="words cLightgray mt5" id="js_plans_pophotel_note_tip"><span class="sum">0</span> / 1000 字</p>
			</div>
		</div>
		<div class="button clearfix">
			<input type="button" class="ui_buttonB fr ml10" value="保 存" id="js_plans_pophotel_save"/>
			<input type="button" class="ui_button_cancel fr" value="取 消" id="js_plans_pophotel_cancel"/>
		</div>
	</div>
</form>

<?php else:?>


<form action="#" method="POST" id="jsplans_pophotel_form">
	<div class="pl_poi_box">
		<div class="title clearfix fontYaHei">
			<img src="http://static.qyer.com/images/plan2/pl_hotel_default.png" onerror="this.src='http://static.qyer.com/images/plan2/pl_hotel_default.png';" width="80" height="80" alt="" id="js_plans_pophotel_pic"/>
			<p class="btit" id="js_plans_addhotelevent_show" style="display:none;"><strong></strong></p>
			<input type="text" class="ui2_input" value="" placeholder="请输入酒店的名字" id="js_plans_addhotelevent" />
			<a href="/admin/hotel/new" target="_blank">添加</a>
			<div class="clearfix title_tpap" style="display:none;" id="js_plans_searchhotel_result">
				<div class="title_list">
					<ul>
                    </ul>
                </div>
            </div>
		</div>
        <div class="list hotel_list_cnt clearfix">
			<div class="item">
				<div class="text fontSong">入住日期:</div>
				<div class="cnt">
					<input type="hidden" name="inDay" value="">
					<div class="tra_select">
						<strong class="titles"><span>&nbsp;</span></strong>
						<div class="contents">
							<ul>
					<?php if(!empty($trip_day_list)):?>
						<?php foreach($trip_day_list as $trip_day):?>
								<li><a href="javascript:;" data-value="<?=$trip_day['trip_day_id'];?>"><?=$trip_day['title'];?></a></li>
						<?php endforeach;?>
					<?php endif;?>
							</ul>
						</div>
					</div>
				</div>
			</div>
            <div class="item">
                <div class="text fontSong">退房日期:</div>
                <div class="cnt"><input type="hidden" name="outDay" value="">
                    <div class="tra_select">
                        <strong class="titles"><span>&nbsp;</span></strong>
                        <div class="contents">
                            <ul>
					<?php if(!empty($trip_day_list)):?>
						<?php foreach($trip_day_list as $key=>$trip_day):?>
							<?php if($key == 0) continue;?>
								<li><a href="javascript:;" data-value="<?=$trip_day['trip_day_id'];?>"><?=$trip_day['title'];?></a></li>
						<?php endforeach;?>
					<?php endif;?>
							</ul>
						</div>
					</div>
				</div>
			</div>
            <div class="item">
				<div class="text fontSong">参考价格:</div>
				<div class="cnt">
					<div class="money_select">
						<input type="text" name="spend" value="" id="jsSignlePrice" class="ui2_input _jsspendinput" placeholder=""/>
						<input type="hidden" name="currency" value="CNY" />
						<div class="tra_select">
							<strong class="titles"><span>人民币</span></strong>
							<div class="contents">
								<ul>
                                    <li><a href="javascript:;" data-value="CNY">人民币</a></li>
                                    <li><a href="javascript:;" data-value="EUR">欧元</a></li>
                                    <li><a href="javascript:;" data-value="USD">美元</a></li>
                                    <li><a href="javascript:;" data-value="THB">泰铢</a></li>
                                    <li><a href="javascript:;" data-value="TWD">新台币</a></li>
                                    <li><a href="javascript:;" data-value="MYR">马来西亚令吉</a></li>
                                    <li><a href="javascript:;" data-value="SGD">新加坡元</a></li>
                                    <li><a href="javascript:;" data-value="KRW">韩元</a></li>
								</ul>
							</div>
						</div>
					</div>
                </div>
            </div>
			<div class="item number">
            	<div class="text fontSong">房间数量:</div>
                <div class="cnt"><input type="hidden" name="ticketcount" value="1">
                	<div class="tra_select" id="_jspoiticketcount">
                    	<strong class="titles cny_bg"><span>1</span></strong>
                        <div class="contents">
                        	<ul>
                            	<li><a data-value="1" href="javascript:;">1</a></li>
                                <li><a data-value="2" href="javascript:;">2</a></li>
                                <li><a data-value="3" href="javascript:;">3</a></li>
                                <li><a data-value="4" href="javascript:;">4</a></li>
                                <li><a data-value="5" href="javascript:;">5</a></li>
                                <li><a data-value="6" href="javascript:;">6</a></li>
                                <li><a data-value="7" href="javascript:;">7</a></li>
                                <li><a data-value="8" href="javascript:;">8</a></li>
                                <li><a data-value="9" href="javascript:;">9</a></li>
                                <li><a data-value="10" href="javascript:;">10</a></li>
                                <li><a data-value="11" href="javascript:;">11</a></li>
                                <li><a data-value="12" href="javascript:;">12</a></li>
                                <li><a data-value="13" href="javascript:;">13</a></li>
                                <li><a data-value="14" href="javascript:;">14</a></li>
                                <li><a data-value="15" href="javascript:;">15</a></li>
                                <li><a data-value="16" href="javascript:;">16</a></li>
                                <li><a data-value="17" href="javascript:;">17</a></li>
                                <li><a data-value="18" href="javascript:;">18</a></li>
                                <li><a data-value="19" href="javascript:;">19</a></li>
                                <li><a data-value="20" href="javascript:;">20</a></li>
                            </ul>
                        </div>
                    </div>
                    <p class="money" id="_jspoicountspend"></p>
                </div>
            </div>
        </div>
        <div class="textarea">
			<textarea name="note" id="js_plans_pophotel_note" placeholder="在这里添加备注" class="ui2_textarea" rel-tip="#js_plans_pophotel_note_tip"></textarea>
			<div class="clearfix pr">
				<p class="words cLightgray mt5" id="js_plans_pophotel_note_tip"><span class="sum">0</span> / 1000 字</p>
			</div>
		</div>
		<div class="button clearfix">
            <input type="button" class="ui_buttonB fr ml10" value="保 存" id="js_plans_pophotel_save"/>
            <input type="button" class="ui_button_cancel fr" value="取 消" id="js_plans_pophotel_cancel"/>
        </div>
    </div>
</form>

<?php endif;?>
