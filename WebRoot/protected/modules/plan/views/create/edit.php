<div class="pl_hd clearfix">
    <div class="fl">
        <div class="pl_tit clearfix">
            <h2 class="cGray fontYaHei fl" id="trip-title">
				<span id="subtriptitle"><?=$trip['title'];?></span>
			</h2>
			<div class="pl_tit_edit fl pr10 none" id="trip-title-ipt" style="display: none;">
				<input id="js_trip_title_input" type="text" class="ui_input fontYaHei fl" value="<?=$trip['title'];?>">
			</div>
        </div>
	</div>
	<div class="fr">
		<ul class="export_buttons">
			<li><a target="_blank" href="/plan/export/app?trip_id=<?=$trip['id'];?>"><span class="glyphicon glyphicon-phone"></span>手机版本</a></li>
			<li><a target="_blank" href="/plan/export/excel?trip_id=<?=$trip['id'];?>"><span class="glyphicon glyphicon-file"></span>Excel讨论版本</a></li>
            <li><a target="_blank" href="/plan/export/pdf?hash_id=<?=$trip['hash_id'];?>&admin_id=<?=$trip['admin_id'];?>"><span class="glyphicon glyphicon-list-alt"></span>PDF版本</a></li>
		</ul>
	</div>
</div>
<div class="pl_bd">
    <div class="pl_left">
    	<div class="pl_menu">
        	<!--行程单标题 -->
			<div class="menu_tit">
            	<strong class="fontYaHei f16 cGray">行程单 (<span id="js_plan_totalday">--</span>天)</strong>
			</div>
			<!--行程单列表 -->
			<div class="menu_cnt" id="js_plan_allday_list">
				<div class="xcd_date clearfix">
					<span>出发时间：</span>
					<input id="plans_datepicker" type="text" class="ui_input" value="<?=$trip['start_time'];?>" />
				</div>
				<div class="xqzb_cnt" id="js_plan_note_btn">
                	<div class="clearfix">
                    	<span class="xqzb_image"></span>
						<dl>
                        	<dt>旅游小贴士</dt>
                        	<dd>各种注意事项 ...</dd>
                    	</dl>
                	</div>
            	</div>
				<div class="xqzb_cnt" id="js_recommend_package_btn">
                	<div class="clearfix">
                    	<span class="xqzb_image"></span>
						<dl>
                        	<dt>推荐旅游产品</dt>
                        	<dd>推荐酒店，本地活动等</dd>
                    	</dl>
                	</div>
            	</div>
            	<ul class="xcd_item"></ul>
				<div class="xcd_add" id="xcd_addoneday_last">
                	<a href="javascript:void(0);" data-bn-ipg="left-addday">添加一天</a>
				</div>
			</div>
		</div>
    </div>
	<div class="pl_right">
		<div class="pl_rright">
			<div class="pl_rtag">
		    	<ul class="fontYaHei f14 cGray">
		        	<li class="pr pl_rtag_li current _jstabdefaultcity" data-bn-ipg="right-tab-city">
		            	<strong class="rtag_jt" >
							<span class="text">
								<span id="_jsdefaultcityname" data-bn-ipg="right-selectcity" class="title">请选择城市</span>
								<span class="cBlue">切换城市</span>
		            		</span>
						</strong>
						<!--下拉城市 -->
						<div class="csearch_list">
							<div class="city_list fontYahei" id="_jsdefaultcitylist">
								<a href="javascript:;" data-cityid="3" data-cityname="吉隆坡" data-bn-ipg="right-select-citylist"><span>吉隆坡</span></a>
							</div>
						</div>
					</li>
					<li class="pl_rtag_li _jstabnoteclip" data-bn-ipg="right-tab-note"><span class="rtag_line">小贴士</span></li>
					<li class="pl_rtag_li _jstabtripview" data-bn-ipg="right-tab-tripview"><span class="rtag_line">行程总览</span></li>
				</ul>
			</div>
		
			<div class="pl_rnote" style="display:none;" id="_jsnoteclip">
			    <div class="pl_rncnt">
			        <!--剪辑和笔记 -->
			        <div class="pl_rnote_cnt" style="display:;" id="leftrrr">
			            <!--类型 -->
			               <div class="head clearfix">
			               </div>
			               <div id="_jsnotecliplist">
			               </div>
						<div class="more tc" id="_jsnoteclippager" style="display:none;">
			                <a href="javascript:;" class="_jsmorenoteclip" data-bn-ipg="right-note-more"><span>更多</span></a>
			                <a href="javascript:;" class="more_load _jsmorenoteclipload" style="display:none"><span>正在载入</span></a>
			            </div>                       
			        </div>
        
			        <!--剪辑和笔记 end edit -->
			        <div class="pl_rnote_cnt" id="rightrrr" style="display:none;">
        	
			        </div>
			    </div>
			</div>
			
			<div class="pl_rcity " style="text-align:left">
				<div class="pl_rccnt" >
					<div class="head clearfix pt10 f14" >
						<div class="btn-group" data-toggle="buttons" id="_jsPoiTypeSelect">
							<label class="btn btn-sm btn-default">
								<input type="radio" name="type" checked=true value="hotel" id="hotel">酒店
							</label>
							<label class="btn btn-sm btn-default active">
								<input type="radio" name="type" checked=true value="sight" id="sight">景点
							</label>
							<label class="btn btn-sm btn-default">
								<input type="radio" name="type" value="restaurant" id="restaurant">美食
							</label>
							<label class="btn btn-sm btn-default">
								<input type="radio" name="type" value="shopping" id="shopping">购物
							</label>
							<label class="btn btn-sm btn-default">
								<input type="radio" name="type" value="entertainment" id="entertainment">娱乐
							</label>
							<label class="btn btn-sm btn-default">
								<input type="radio" name="type" value="island" id="island">海岛
							</label>
							<label class="btn btn-sm btn-default">
								<input type="radio" name="type" value="demo_trip_day" id="island">经典行程
							</label>
						</div>
						<p class="fl pl10 cLightgray" id="_jshotroutecommentshow"></p>
						<!--div class="fr list_link">
							<div class="link_tab current" id="_jstabtolist"><a href="javascript:;" class="link_list"  onfocus="this.blur();">列表</a></div>
							<div class="link_tab " id="_jstabtomap"><a href="javascript:;"  class="link_map" onfocus="this.blur();">地图</a></div>
						</div--> 
						<!-- todo skying -->        	
						<div class="search_input_box">
							<div class="pl_poi_search" id="_jssearchbox">
								<input type="text" class="ui2_input" placeholder="搜索" value="" id="_jssearchkey" autocomplete="off">
								<span class="close" id="_jssearchclose"></span>
							</div>
						</div>            	
					</div>
					<div class="clear"></div>       
					<div>
						<!--景点类型 -->
						<div class="pl_poi_cnt" style="display:none">
			            	<div id="_jspoilist">
								
			            	</div>
							<div class="pl_poiline_more _jsloadmorepoihotel" style="display:none">
								<a href="javascript:;" class="loading">正在载入...</a>
							</div>
						</div>
						<!--住宿类型 -->
						<div class="pl_stay_cnt" style="display:none;">
							<div id="_jshotellist">
								<div class='_jshotrouterecomment' style='display:none; width:0px; height:0; overflow:hidden' ></div>
							</div>
							<div class="pl_poiline_more _jsloadmorepoihotel" style="display:none;">
								<a href="javascript:;" class="loading">正在载入...</a>
							</div>
				            <div style="width:20px;height:25px"></div>            	  
						</div>
            			<div class="pl_rnote_map"   style="width:100%; display:none">
							<div id="jsplan_map_list" style="height:100%; width:100%"><img src="/images/plan/map_list_pic.jpg" /></div>
							<div class="map_loading" style="display:none">
								<div class="loading">
									<span class="fontYaHei">加载中...</span>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div> 
		</div>
		
		
		
		<div class="pl_rleft">
			<div class="pl_nday" id="js_plan_nday">
				<div class="pl_nday_main">
					<div class="xcd_cnt_tit">
						<span class="n_title_al">
							<span class="n_prev n_prev_disabled" title="上一天" id="jsplan_go_prev"></span>
							<span class="n_title" id="xcd-oneday-title">
								<span class="all">
									<strong class="day">第<em>1</em>天</strong>
									<span class="date">（1月28日 星期四）</span>
								</span>
							</span>
							<span class="n_next" title="下一天" id="jsplan_go_next"></span>
						</span>
						<!--div class="n_map"><a href="javascript:void(0);" id="jsplan_goto_curoneday_map"><span>当日地图</span></a></div-->
					</div>
					<div class="xcd_cnt_day xcd_cnt_day_auto">
						<div class="day_city">
							<div class="list clearfix fontYaHei" id="js_plan_taglist">
								<div class="add_city" id="js_plan_addtag">
									<span class="add_city_btn" id="js_plan_addcity_btn" data-bn-ipg="mid-addcity">添加城市</span>
									<div class="csearch_list" id="js_plan_addcity_list">
										<span class="arrowbig"></span>
										<span class="arrow"></span>
										<div class="pr">
											<input type="text" class="ui_input" placeholder="热门城市" id="xcd-city-input"/>
											<div class="jd_item fontYaHei cGray" id="xcd-city-list">
												<ul></ul>
											</div>
										</div>
										<div class="hot clearfix" id="js_plan_add_hotplace_city">
											<span class="fl">热门：</span>
											<a href="javascript:void(0);" data-cityname="吉隆坡" data-cityid="3" title="吉隆坡">吉隆坡</a>
											<a href="javascript:void(0);" data-cityname="亚庇" data-cityid="1" title="亚庇">亚庇</a>
											<a href="javascript:void(0);" data-cityname="斗湖" data-cityid="102250" title="斗湖">斗湖</a>
											<a href="javascript:void(0);" data-cityname="兰卡威" data-cityid="9" title="兰卡威">兰卡威</a>
											<a href="javascript:void(0);" data-cityname="马六甲" data-cityid="10" title="马六甲">马六甲</a>
											<a href="javascript:void(0);" data-cityname="槟城" data-cityid="102253" title="槟城">槟城</a>
											<br/>
											<a href="javascript:void(0);" data-cityname="曼谷" data-cityid="4" title="曼谷">曼谷</a>
											<a href="javascript:void(0);" data-cityname="普吉" data-cityid="5" title="普吉">普吉</a>
											<a href="javascript:void(0);" data-cityname="清迈" data-cityid="6" title="清迈">清迈</a>
											<a href="javascript:void(0);" data-cityname="拜县" data-cityid="17" title="拜县">拜县</a>
											<a href="javascript:void(0);" data-cityname="苏梅" data-cityid="8" title="苏梅">苏梅</a>
											<a href="javascript:void(0);" data-cityname="象岛" data-cityid="12" title="象岛">象岛</a>
											<br/>
											<a href="javascript:void(0);" data-cityname="台北" data-cityid="102207" title="台北">台北</a>
											<a href="javascript:void(0);" data-cityname="高雄" data-cityid="102210" title="高雄">高雄</a>
											<a href="javascript:void(0);" data-cityname="花莲" data-cityid="102209" title="花莲">花莲</a>
										</div>
									</div>
								</div>
								<div class="clear"></div>
							</div>
							<div class="clear"></div>
						</div>
						
						<div class="borderdotted1"></div>
						<!--交通-->
						<div class="traffic_item" id="_jstrafficcontainer">
							<!--交通弹层 -->
							<div class="add_traffic pr">
								<div class="ui_prompt" style="display:none; left:20px; top:-30px;" id="jsplan_traffic_prompt">
									<div class="ui_prompt_cnt">在这里添加你的交通信息吧！<img src="/images/plan/tips_close.png" width="11" height="11" alt="关闭" class="vm hand"></div>
									<div class="ui_prompt_awbtm"></div>
								</div>
								<div class="traffic_btn">城市间交通：<a href="javascript:void(0);" class="add_Abtn" id="_jsshowtrafficform"><span class="">添加自定义城市交通</span></a>
								</div>
							</div>
							<div id="_jstrafficlist"></div>
						</div>
					
							
						<div class="borderdotted1"></div>
						<!--酒店住宿 -->
						<div class="hotel_item" id="_jshotelcontainer">
							<!--添加住宿 -->
							<div class="add_activity">
			                   	<div class="add_hotel_btn">
									酒店：请从右侧添加酒店 或者<a href="javascript:void(0);" class="add_Abtn" data-bn-ipg="mid-addhotel">添加自定义住宿</a>

								</div>
							</div>
							<div class="hotel_item_ul" id="js_plan_hotels">
							</div>
						</div>
							
						<div class="borderdotted1"></div>
						<!--内容 -->
						<div class="day_item" id="_jspoicontainer">
							<!--添加活动 -->
							<div class="add_activity">
			               		<div class="activity_btn">
									<strong><span>请从右侧添加线路或景点</span></strong>
								</div>
							</div>
			                <div class="day_item_ul" id="js_plan_activites"></div>                
						</div>

						<div class="borderdotted1"></div>
						<!--行程简介 -->
						<div class="trip_info" style="margin-top:10px;">
							<div class="xcd_note">
								<div class="xcd_note_text">
									<input type="text" name="title" value="" placeholder="行程标题" class="js_trip_info_update" style="width:100%;height:30px; border:0;">
								</div>
							</div>
							<div class="xcd_note">
								<div class="xcd_note_text">
									<textarea name="desc" placeholder="行程简介" class="js_autoheight ui2_textarea js_trip_info_update" id="js_plan_onedayDescTextarea"></textarea>
								</div>
							</div>
							<div class="xcd_note">
								<div class="xcd_note_text">
									<textarea name="traffic_note" placeholder="交通建议" class="js_autoheight ui2_textarea js_trip_info_update" id="js_plan_onedayTrifficDescTextarea"></textarea>
								</div>
							</div>
							<div class="xcd_note clearfix" >
								<span class="pull-left btn btn-sm btn-primary fileinput-button" id="_js_upload_trip_day_image" style="position:relative">
									 <i class="glyphicon glyphicon-circle-arrow-up"></i>
									 <span>上传当日行程图片</span>
									<input id="tripDayImageFile" style="font-size:20px;" type="file" name="Filedata" multipart=false />
								</span>
								<button class="pull-left btn btn-default btn-sm" style="margin-left:10px; margin-bottom:20px;" id="_js_set_demo_trip">添加到经典行程</button><span class="set_demo_trip_help"></span>
								<a href="javascript:void(0);" class="pull-right btn btn-success btn-sm" style="margin-bottom:20px;">保存</a>
							</div>
                            <div class="xcd_note trip_day_image_box">
                                <ul>
                                </ul>
							</div>
						</div>
						<div id="js_xcd_layer_loading" style="display:none;"></div>
					</div>
				</div>
			</div>
			<div class="pl_recommend_package" js="">
				
			</div>
			<div class="pl_prepare" id="js_plan_prepare">
				<div class="pl_trip_note_box" style="padding:10px; height:100%;">
					<div class="actions" style="text-align:center; color:#666; line-height:30px;font-size:16px;">
						<div class="pull-left" style="position:relative;">
							
							<span class="btn btn-xs btn-success fileinput-button">
								 <i class="glyphicon glyphicon-circle-arrow-up"></i>
								 <span>插入图片</span>
								 <!-- The file input field used as target for the file upload widget -->
								<input id="fileupload" type="file" name="Filedata" multipart=false/>
							</span>
							<span id="note-pic-uploading" style="font-size:14px;display:none;">正在上传...</span>
						</div>
						<span>小贴士</span>
						<div class="pull-right" style="margin-bottom:10px;">
							<button class="btn btn-success btn-sm" href="javascript:void(0);" id="js_plan_note_save_btn">保存</button>
							<a class="btn btn-default btn-sm" href="/plan/export/tripNote?trip_id=<?=$trip['id'];?>" target="_blank">预览</a>
						</div>
					</div>

					<textarea id="tripNoteText" style="width:100%;min-height:400px; height:100%;background:#fff;"></textarea>
				</div>
			</div>
		</div>
		<div class="clear"></div>
	</div>
	<div class="clear"></div>
</div>


<script type="text/template" id="poiItemView-tpl">
{{#poi_list}}
<div class="pl_poi_list">
	<div class="header clearfix">
		<a href="javascript:;" class="fl _jsplanpoidetail" data-poi_id="{{poi_id}}">
			<img src="{{cover_image}}" onerror="this.src='/images/plan/poi_200_133.png'" alt="" title="" width="80" height="60">
		</a>

		{{#is_hotel}}
		<div class="content">
			<a href="/admin/hotel/edit?hotel_id={{id}}" target="_blank" class="title_link _jsplanpoidetail" data-poi_id="{{id}}" data-btn="right-poi-title-1" title="{{poi_name}}">
				<span class="cBlack">{{title}}</span>
			</a>
			<div class="clearfix">
				{{#area}}区域：{{area}}</br>{{/area}}
				<div class="content">
					{{#agoda_url}}<a target="_blank" href="{{agoda_url}}">agoda预定</a>{{/agoda_url}}
				</div>
			</div>
		</div>
		<div class="actions">
			<a href="javascript:void(0);" class="fr add_btn _jsrightaddpoi" data-poi_id="{{poi_id}}" data-address="{{address}}" data-href="/admin/poi/edit?poi_id={{poi_id}}" data-poi_name="{{poi_name}}" data-type="{{type}}" data-desc="{{desc}}" data-cover_image="{{cover_image}}" data-city_id="{{city_id}}" data-city_name="{{city_name}}" lat="{{lat}}" lon="{{lon}}">
				<span class="tips">添加到计划</span>
			</a>
			<a href="javascript:void(0);" class="fr add_recommend_hotel_btn _jsrightaddrecommendhotel" data-hotel_id="{{id}}">
			</a>
		</div>
		{{/is_hotel}}
		{{^is_hotel}}
		<a href="/admin/poi/edit?poi_id={{poi_id}}" target="_blank" class="title_link _jsplanpoidetail" data-poi_id="{{poi_id}}" data-btn="right-poi-title-1" title="{{poi_name}}">
			<span class="cBlack">{{poi_name}}</span>
		</a>
		<div class="clearfix">
			{{#area}}区域：{{area}}</br>{{/area}}
			<div class="content">
				<p class="jj_h">{{desc}}</p>
			</div>
		</div>
		<a href="javascript:void(0);" class="fr add_btn _jsrightaddpoi" data-poi_id="{{poi_id}}" data-address="{{address}}" data-href="/admin/poi/edit?poi_id={{poi_id}}" data-poi_name="{{poi_name}}" data-type="{{type}}" data-desc="{{desc}}" data-cover_image="{{cover_image}}" data-city_id="{{city_id}}" data-city_name="{{city_name}}" lat="{{lat}}" lon="{{lon}}">
			<span class="tips">添加到计划</span>
		</a>
		{{/is_hotel}}
	</div>
</div>
{{/poi_list}}
</script>

<script type="text/template" id="noteItemView-tpl">
{{#note_list}}
<div class="pl_poi_list note">
	<div class="header clearfix">
		<a href="javascript:;" class="title_link _jsplanpoidetail" data-poi_id="{{poi_id}}" data-btn="right-poi-title-1" title="{{poi_name}}">
			<span class="cBlack">{{title}}</span>
		</a>
		<a href="javascript:void(0);" class="fr add_btn _jsrightaddnote" data-content="{{content}}" data-title="{{title}}">
			<span class="tips">添加到行程</span>
		</a>
	</div>
	<div class="content clearfix">
		<p class="jj_h">{{{content}}}</p>
	</div>
</div>
{{/note_list}}
</script>

<script type="text/template" id="demoTripDayView-tpl">
{{#demo_trip_day_list}}
<div class="pl_poi_list note">
	<div class="header clearfix">
		<a href="javascript:;" class="title_link _jsplanpoidetail" data-poi_id="{{poi_id}}" data-btn="right-poi-title-1" title="{{poi_name}}">
			<span class="cBlack">{{title}}</span>
		</a>
		<a href="javascript:void(0);" class="fr add_btn _jsrightadd_demotripday"  data-desc="{{desc}}" data-traffic_note="{{traffic_note}}" data-title="{{title}}">
			<span class="tips">添加到计划</span>
		</a>
	</div>
	<div class="content clearfix">
		<p class="jj_h">{{{desc}}}</p>
	</div>
	<div class="content clearfix" style="border-top:1px dashed #ccc;padding-top:10px;">
		<p class="jj_h">{{{traffic_note}}}</p>
	</div>
</div>
{{/demo_trip_day_list}}
</script>



<script type="text/template" id="onedayView-tpl">
    <a href="javascript:void(0);" class="insert" title="插入一天" data-bn-ipg="left-insertday" onfocus="this.blur();">插入一天</a>
    <a href="javascript:void(0);" class="delete" onfocus="this.blur();">{{datedesc}}</a>
	<a href="javascript:void(0);" class="remove" onfocus="this.blur();" title="删除一天">删除</a>
	{{^has_city}}
	<p class="nonep">暂无安排{{has_city}}</p>
	{{/has_city}}
	{{#has_city}}
    <dl>
		<dt>
		{{#city_list}}
			{{^first}}<img src="/images/plan/xcd_city_icon.png" class="vm ml5 mr5" alt="">{{/first}}
			<em>{{city_name}}</em>
		{{/city_list}}
			<dd>{{timedesc}}</dd>
		</dt>
    </dl>
	{{/has_city}}
    <div class="clearfix"></div>
</script>


<!-- poi_event_template -->
<script type="text/template" id="onedayevent-t1">

        <div class="pr">
			{{#href}}
				<a href="javascript:;"  class="_jsplanpoidetail"><img src="{{cover_image}}" alt="" width="48" height="48" class="fl" onerror="this.src='http://static.qyer.com/images/place/no/poi_80_80.png'" /> </a>
			{{/href}}
			{{^href}}
				<img src="{{cover_image}}" alt="" width="48" height="48" class="fl" onerror="this.src='http://static.qyer.com/images/place/no/poi_80_80.png'"/>
			{{/href}}
		    <div class="content">
		        <div><strong class="fb">
					{{#href}}
						<a href="/admin/poi/edit?poi_id={{poi_id}}" target="_blank"  class="_jsplanpoidetail" data-poi_id="{{poi_id}}">{{poi_name}}</a>
					{{/href}}
					{{^href}}
						{{poi_name}}
					{{/href}}
				</strong></div>
				{{#desc}}
					<p class="tcnt"><strong class="fb">简介：</strong>{{desc}}</p>
				{{/desc}}
				{{#note}}
		        	<p class="overheight"><strong class="fb">备注：</strong>{{note}}</p>
				{{/note}}
		    </div>
		    <div class="clear"></div>
		    <div class="p_list_seting">
		        <span>设置</span>
		        <ul>
		            <li><a href="javascript:;" class="edit jsplan_edit" data-bn-ipg="planedit-col2-event-edit"><strong>编辑</strong></a></li>
		            <li><a href="javascript:;" class="del delete jsplan_del" data-bn-ipg="planedit-col2-event-delete"><strong>删除</strong></a></li>
		            <!--li class="hvlist">
		                <a href="javascript:;" class="next"><strong class="move">移动到</strong></a>
		                <ul class="jsplan_move">
		                </ul>
		            </li-->
		        </ul>
		    </div>
		</div>

</script>

<!-- hotel event template -->
<script type="text/template" id="onedayevent-t2">
	<div class="pr">
		<a href="{{href}}" target="_blank">
			<img src="{{cover_image}}" class="fl" alt="" width="48" height="48" onerror="this.src='/images/plan/pl_hotel_default.png';"/>
		</a>
        <div class="content">
        	<div><strong class="fb"><a href="{{href}}" target="_blank">{{title}}</a></strong></div>
			{{#price}}
				<p>{{price}}</p>
			{{/price}}
			{{#spend}}
        		<p class="tcnt"><strong class="fb">参考价格：</strong><span class="number">{{currency}} {{spend}} * {{ticketcount}}</span></p>
			{{/spend}}
			{{#note}}
        		<p class="overheight"><strong class="fb">备注：</strong>{{note}}</p>
			{{/note}}
		</div>

    <div class="clearfix"></div>
    <div class="p_list_seting">
        <span>设置</span>
        <ul>
            <li><a href="javascript:void(0);" class="edit jsplan_edit" data-bn-ipg="planedit-col2-event-edit"><strong>编辑</strong></a></li>
            <li><a href="javascript:void(0);" class="del jsplan_del" data-bn-ipg="planedit-col2-event-delete"><strong>删除</strong></a></li>
            <!--li class="hvlist">
                <a href="javascript:void(0);" class="next"><strong class="move">移动到</strong></a>
                <ul class="jsplan_move">
                    <li><a href="#">第一天</a></li>
                </ul>
            </li-->
        </ul>
    </div>
</div>
</script>

<script type="text/template" id="tpl_searchlivecity">
	<div class="citylist_box" >{{#list}}<a {{#current}}class="current"{{/current}} href="javascript:void(0)" data-city_id="{{city_id}}" data-city_name="{{city_name}}" data-title="{{name}}" data-type="{{type}}" data-dataid="{{id}}"><span>{{name}}</span></a>{{/list}}</div>
</script>


<script type="text/template" id="tpl_onedaytraffic">
	<div class="list clearfix">
		<div class="city">
			<div class="name">
				<strong class="fontYaHei" title="{{from_place}}">{{from_place}}</strong>
				<div><span class="fontArial">{{start_hours}} : {{start_minutes}}</span></div>
			</div>
			<div class="icon"></div>
			<div class="name">
				<strong class="fontYaHei" title="{{to_place}}">{{to_place}}</strong>
				<div>
					<span class="fontArial">{{end_hours}} : {{end_minutes}}</span>
				</div>
			</div>
		</div>
		<div class="money fontYaHei">
			<p>班次：<span class="fontArial">{{traffic_number}}</span></p>
			<p class="mt20">费用：<span class="fontArial">{{ticketcount}} x {{spend}}</span> {{currency}}</p>
		</div>
		{{#note}}<div class="text">备注：{{note}}</div>{{/note}}
		<div class="edit">
			<a href="javascript:;" class="del _jsdelonetraffic" onfocus="this.blur();" title="删除">删除</a>
			<a href="javascript:;" class="ebj _jseditonetraffic" onfocus="this.blur();" title="编辑">编辑</a>
		</div>
	</div>
</script>

<script type="text/template" id="citylist_select">
{{#city_list}}
	<a href="javascript:;" data-cityid="{{city_id}}" data-cityname="{{city_name}}"><span>{{city_name}}</span></a>
{{/city_list}}
</script>

<script>
seajs.use('/assets/plan/router.js', function(router) {
	window.tripInfo = {"tripId" : "<?=$trip['id'];?>", "tripTitle": "<?=$trip['title'];?>", "starttime":"<?=$trip['start_time'];?>"};
	router.load('plan');
});
</script>
