<link href="/css/traffic.css" rel="stylesheet" type="text/css" />
<form name="trafficform" id="trafficform" onsubmit="return false;">
	<input type="hidden" name="id" value="<?=isset($traffic) ? $traffic['id'] : '';?>" />
	<input type="hidden" name="doaction"  value="<?=isset($traffic) ? 'edit' : 'add';?>"/>
    <div class="pl_traffic_box">
        <h3 class="fontYaHei">自定义交通方案</h3>
        <div class="list clearfix">
        	<div class="item">
            	<div class="text fontSong"><span>·</span>出发城市:</div>
                <div class="cnt">
                	<input type="text"  name="from_place" id="trafficfromplace" class="ui2_input" placeholder="" value="<?=isset($traffic) ? $traffic['from_place'] : '';?>" />
                    <div class="citylist" id="trafficfromplacelist" style="z-index:999">
                    </div>
                	<input type="hidden" name="from_placeid" value="<?=isset($traffic) ? $traffic['from_placeid'] : '';?>"/>
                </div>
            </div>
        	<div class="item">
            	<div class="text fontSong"><span>·</span>到达城市:</div>
                <div class="cnt">
                	<input type="text" name="to_place" id="traffictoplace" class="ui2_input" placeholder="" value="<?=isset($traffic) ? $traffic['to_place'] : '';?>" />
                    <div class="citylist" id="traffictoplacelist" style="z-index:999">
                    </div>               	
                	<input type="hidden" name="to_placeid" value="<?=isset($traffic) ? $traffic['to_placeid'] : '';?>" />
                </div>
            </div>

        	<div class="item">
            	<div class="text fontSong"><span>·</span>交通方式:</div>
                <div class="cnt">
                	<input type="hidden" name="mode" id="t_tripmode" value="<?=isset($traffic) ? $traffic['mode'] : '';?>"/>
                	<div class="tra_style_select">
                    	<strong class="titles"><span class="plane">飞机</span></strong><!-- 错误添加 ui2_error -->
                        <div class="contents">
                        	<ul>
								<li><a href="javascript:;" class="plane" data-value="1">飞机</a></li>
                            	<li><a href="javascript:;" class="train" data-value="2">火车</a></li>
                            	<li><a href="javascript:;" class="bus" data-value="3">巴士</a></li>
                            	<li><a href="javascript:;" class="car" data-value="4">租车</a></li>
                            	<li><a href="javascript:;" class="steamer" data-value="5">轮船</a></li>
                            	<li><a href="javascript:;" class="other" data-value="6">其他</a></li>
                           </ul>
                        </div>
                    </div>
                </div>
			</div>

        	<div class="item">
            	<div class="text fontSong">班　　次:</div>
                <div class="cnt">
                	<input type="text" name="traffic_number"  id="traffic_number" placeholder="AK207" class="ui2_input" value="<?=isset($traffic) ? $traffic['traffic_number'] : '';?>" />
                </div>
			</div>

        	<div class="item">
            	<div class="text fontSong">出发时间:</div>
                <div class="cnt">
                	<input type="text" name="start_hours"  id="start_hours" placeholder="时" style="width:30px;" class="ui2_input" value="<?=isset($traffic) ? $traffic['start_hours'] : '';?>" />
					<input type="text" name="start_minutes"  id="start_minutes" placeholder="分" style="width:30px;" class="ui2_input" value="<?=isset($traffic) ? $traffic['start_minutes'] : '';?>" />
					如：08:25
                </div>
            </div>
        	<div class="item">
            	<div class="text fontSong">到达时间:</div>
                <div class="cnt">
                	<input type="text" name="end_hours"  id="end_hours" placeholder="时" style="width:30px;" class="ui2_input" value="<?=isset($traffic) ? $traffic['end_hours'] : '';?>" />
					<input type="text" name="end_minutes"  id="end_minutes" placeholder="分" style="width:30px;" class="ui2_input" value="<?=isset($traffic) ? $traffic['end_minutes'] : '';?>" />
                    <input type="hidden" name="days" value="0" />
                	<div class="tra_select" id="_jstrafficdays">
                    	<strong class="titles"><span>当日</span></strong>
                        <div class="contents">
                        	<ul>
                            	<li><a href="javascript:;" data-value='0'>当日</a></li>
                            	<li><a href="javascript:;" data-value='1'>次日</a></li>
                            	<li><a href="javascript:;" data-value='2'>+2</a></li>
                            	<li><a href="javascript:;" data-value='3'>+3</a></li>
                            	<li><a href="javascript:;" data-value='4'>+4</a></li>
                            	<li><a href="javascript:;" data-value='5'>+5</a></li>
                            	<li><a href="javascript:;" data-value='6'>+6</a></li>
                            	<li><a href="javascript:;" data-value='7'>+7</a></li>
                            	<li><a href="javascript:;" data-value='8'>+8</a></li>
                            	<li><a href="javascript:;" data-value='9'>+9</a></li>
                            	<li><a href="javascript:;" data-value='10'>+10</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        	<div class="item">
            	<div class="text fontSong">花　　费:</div>
                <div class="cnt">
                	<div class="money_select">
						<input type="text" name="spend" value="<?=isset($traffic) ? $traffic['spend'] : '';?>" class="ui2_input _jsspendinput" placeholder=""/>
						<input type="hidden" name="currency" value="CNY" />
						<div class="tra_select">
							<strong class="titles"><span>人民币</span></strong>
							<div class="contents">
								<ul>
            	                	<li><a href="javascript:;" data-value="CNY">人民币</a></li>
                                	<li><a href="javascript:;" data-value="EUR">欧元</a></li>
                                	<li><a href="javascript:;" data-value="USD">美元</a></li>
                                	<li><a href="javascript:;" data-value="THB">泰铢</a></li>
                                	<li><a href="javascript:;" data-value="TWD">新台币</a></li>
                                	<li><a href="javascript:;" data-value="MYR">马来西亚令吉</a></li>
                                	<li><a href="javascript:;" data-value="SGD">新加坡元</a></li>
                                	<li><a href="javascript:;" data-value="KRW">韩元</a></li>
                                	<li><a href="javascript:;" data-value="KHR">瑞尔</a></li>
                                	<li><a href="javascript:;" data-value="GBP">英镑</a></li>
                                	<li><a href="javascript:;" data-value="CHF">瑞士法郎</a></li>
                                	<li><a href="javascript:;" data-value="AUD">澳洲元</a></li>
                                	<li><a href="javascript:;" data-value="CZK">捷克克朗</a></li>
                                	<li><a href="javascript:;" data-value="IDR">印尼盾</a></li>
                                	<li><a href="javascript:;" data-value="PHP">菲律宾比索</a></li>
                                	<li><a href="javascript:;" data-value="NPR">尼泊尔卢比</a></li>
                                	<li><a href="javascript:;" data-value="SEK">瑞典克朗</a></li>
                                	<li><a href="javascript:;" data-value="DKK">丹麦克朗</a></li>
                                	<li><a href="javascript:;" data-value="HUF">匈牙利福林</a></li>
                                	<li><a href="javascript:;" data-value="NZD">新西兰元</a></li>
                                	<li><a href="javascript:;" data-value="CAD">加拿大元</a></li>
                                	<li><a href="javascript:;" data-value="TRY">新土耳其里拉</a></li>
                                	<li><a href="javascript:;" data-value="INR">印度卢比</a></li>
                                	<li><a href="javascript:;" data-value="RUB">俄罗斯卢布</a></li>
                                	<li><a href="javascript:;" data-value="EGY">埃及镑</a></li>
                                	<li><a href="javascript:;" data-value="NOK">挪威克朗</a></li>
                                	<li><a href="javascript:;" data-value="LAK">老挝基普</a></li>
                                	<li><a href="javascript:;" data-value="LKR">斯里兰卡卢比</a></li>
                                	<li><a href="javascript:;" data-value="BUK">缅甸元</a></li>
                                	<li><a href="javascript:;" data-value="PLN">波兰兹罗提</a></li>
                                	<li><a href="javascript:;" data-value="MXN">墨西哥比索</a></li>
                                	<li><a href="javascript:;" data-value="ZAR">南非兰特</a></li>
                                	<li><a href="javascript:;" data-value="BRL">雷亚尔</a></li>
                                	<li><a href="javascript:;" data-value="ISK">冰岛克朗</a></li>
                                	<li><a href="javascript:;" data-value="ARS">阿根廷比索</a></li>
                                	<li><a href="javascript:;" data-value="RON">罗马尼亚列伊</a></li>
                                	<li><a href="javascript:;" data-value="BNG">保加利亚列弗</a></li>
                                	<li><a href="javascript:;" data-value="UAH">格里夫纳</a></li>
                                	<li><a href="javascript:;" data-value="ZWR">津巴布韦元</a></li>
                                	<li><a href="javascript:;" data-value="MNT">蒙古图格里克</a></li>
                                	<li><a href="javascript:;" data-value="MOP">澳门币</a></li>
                                	<li><a href="javascript:;" data-value="HKD">港元</a></li>
                                	<li><a href="javascript:;" data-value="ILS">新谢克尔</a></li>
                                	<li><a href="javascript:;" data-value="JPY">日元</a></li>
                                	<li><a href="javascript:;" data-value="AED">阿联酋迪尔汗</a></li>
                                	<li><a href="javascript:;" data-value="PKR">巴基斯坦卢比</a></li>
                                	<li><a href="javascript:;" data-value="MVR">马尔代夫拉菲亚</a></li>
                                	<li><a href="javascript:;" data-value="BTN">不丹努尔特鲁姆</a></li>
                                	<li><a href="javascript:;" data-value="VND">越南盾</a></li>
								</ul>
							</div>
						</div>
					</div>
				</div>
            </div>
        	<div class="item">
            	<div class="text fontSong">数　　量:</div>
                <div class="cnt">
					<input type="hidden" name="ticketcount" value="<?=isset($traffic) ? $traffic['ticketcount'] : '1';?>" />
                	<div class="tra_select _jsticketcountlist">
                    	<strong class="titles"><span><?=isset($traffic) ? $traffic['ticketcount'] : '1';?></span></strong>
                        <div class="contents">
							<ul>
                        		<li><a href="javascript:;" data-value="1">1</a></li>
	                            <li><a href="javascript:;" data-value="2">2</a></li>
    	                        <li><a href="javascript:;" data-value="3">3</a></li>
        	                    <li><a href="javascript:;" data-value="4">4</a></li>
            	                <li><a href="javascript:;" data-value="5">5</a></li>
                	            <li><a href="javascript:;" data-value="6">6</a></li>
                    	        <li><a href="javascript:;" data-value="7">7</a></li>
                        	    <li><a href="javascript:;" data-value="8">8</a></li>
	                            <li><a href="javascript:;" data-value="9">9</a></li>
    	                        <li><a href="javascript:;" data-value="10">10</a></li>
        	                    <li><a href="javascript:;" data-value="11">11</a></li>
            	                <li><a href="javascript:;" data-value="12">12</a></li>
                	            <li><a href="javascript:;" data-value="13">13</a></li>
                    	        <li><a href="javascript:;" data-value="14">14</a></li>
                        	    <li><a href="javascript:;" data-value="15">15</a></li>
                            	<li><a href="javascript:;" data-value="16">16</a></li>
      	            	        <li><a href="javascript:;" data-value="17">17</a></li>
        	                    <li><a href="javascript:;" data-value="18">18</a></li>
            	                <li><a href="javascript:;" data-value="19">19</a></li>
                	            <li><a href="javascript:;" data-value="20">20</a></li>
                        	</ul>
						</div>
					</div>
                	<p class="money" id="_jstrafficcountspend"></p>
				</div>
			</div>
        </div>
        
        <div class="textarea">
			<textarea name="note" id="trafficnote" placeholder="在这里添加备注" class="ui2_textarea" rel-tip="#_jstrafficnotetip"><?=isset($traffic) ? $traffic['note'] : '';?></textarea>
			<div class="clearfix pr">
				<p class="words cLightgray mt5" id="_jstrafficnotetip"><span class="sum">0</span> / 1000 字</p>
			</div>
		</div>
        <div class="button clearfix">
            <input type="button" class="ui_buttonB fr ml10" id="_jssubmittraffic" value="保 存" />
            <input type="button" class="ui_button_cancel fr _jscloseform" value="取 消" />
        </div>
    </div>
</form>
    

