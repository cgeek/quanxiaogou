 <style>
.sortable { list-style-type: none; margin: 0; padding: 0; width: 60%; }
.sortable li { margin: 0 3px 3px 3px; padding: 0.4em;  padding-left: 1.5em; font-size: 1.4em; background:#ccc;cursor: move ; }
.sortable li span { position: absolute; margin-left: -1.3em; }
.ui-state-highlight { height: 1.5em; line-height: 1.2em; }
</style>

<ul class="sortable">
	<li class="ui-state-default clearfix">Item 1--ifxoxo.com</li>
	<li class="ui-state-default clearfix">Item 2--ifxoxo.com</li>
	<li class="ui-state-default clearfix">Item 3--ifxoxo.com</li>
</ul>
<script>
seajs.use('/assets/plan/router.js', function(router) {
	router.load('test');
});
</script>
