<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<title></title>
<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css">
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="/assets/libs/jquery.js" type="text/javascript"></script>
<script src="/assets/libs/jquery.json-2.4.min.js" type="text/javascript"></script>
<script src="/assets/libs/gmap3.js" type="text/javascript"></script>
<script>
</script>
<style>
	div.map-overlay {
		text-align:center;
		color:#FFFFFF; 
		padding:2px 5px; 
		font-family:Optima; 
		background-color:rgba(0, 0, 0, .5);
		font-size:11px;
	}
	div#main-map {
		width:780px;
		height:600px;
		border:1px solid #ccc;

	}
	div.operate-panel {
		float:right;
		width:460px;
		height:600px;
		border:1px solid #ccc;

	}
	div.generate-panel {
		padding:5px;
	}
	div.days-panel {
		float:left;
		height:600px;
	}
	div.day-button, div.kml-button{
		float:left;
		clear:left;
		padding:5px;
		cursor:pointer;
		color:blue;
	}
	div.cur-day-panel {
		float:left;
		height:580px;
		width:180px;
		border:1px solid #ccc;
		margin-left:10px;
		padding:5px;
	}
	div.selected-panel{
		float:left;
		height:580px;
		width:180px;
		border:1px solid #ccc;
		margin-left:10px;
		padding:5px;
		overflow:auto;
	}
	div.one-poi{
		margin:10px 5px;
		padding:5px;
		border:1px solid #ccc;
		cursor:pointer;
	}
	div.one-kml{
		margin:10px 5px;
		padding:5px;
		border:1px solid #ccc;
		cursor:pointer;
	}
	div.dark {
		background-color:#ccc;
		cursor:not-allowed;
	}
</style>
<script>
	var trip_days = <?=json_encode($trip_day_list);?>;
	var kml_list = <?=$kml_list;?>;
	$(document).ready(function(){
		$('.day-button').click(function(){
			var day_id = $(this).attr('day-id');
			var day_content = trip_days[day_id];
			var hotels = day_content['hotel_list'];
			var poies = day_content['poi_list'];

			var poies_copy = $.merge([], poies);
			$.merge(poies_copy, hotels);


			$('.cur-day-panel').empty();
			$.each(poies_copy,  function(key, poi) {
				var poi_div = $('<div></div>').html(poi.name)
					.addClass('one-poi')
					.attr('lat', poi.lat)
					.attr('lon', poi.lon)
					.attr('poi_id', poi.id)
					.attr('poi_name', poi.name)
					.attr('type', poi.type);

				if(poi.lat.length == 0 || poi.lon.length ==0)
				{
					poi_div.addClass('dark');
				}

				poi_div.click(function(){
					$('.selected-panel').append(this);
					reset_select_panel_click_function();
				});

				if(selected_panel_exists(poi.id) == false){
					poi_div.appendTo('.cur-day-panel');
				}
			});
		});
		$('.kml-button').click(function(){
			$('.cur-day-panel').empty();
			$.each(kml_list,  function(key, kml) {
				var kml_div = $('<div></div>').html(kml.title)
					.addClass('one-kml')
					.attr('kml-id', kml.id);


				kml_div.click(function(){
					$('.selected-panel').append(this);
					reset_select_panel_click_function();
				});

				if(selected_panel_exists(kml.id) == false){
					kml_div.appendTo('.cur-day-panel');
				}
			});
		});
		function reset_select_panel_click_function(){
			$('.selected-panel').children('div').each(function(index){
				$(this).click(function(){
					$(this).remove();
				})
				
			});
		}
		$('#re-generate-map').click(function(){
			$('#main-map').gmap3({clear:{name:['marker', 'overlay']}});
			var map_conf = generate_map_conf();
			//console.log($.toJSON(night_bazaar));

			//console.log(map_conf);
			//map_conf = merge_map_conf(map_conf, night_bazaar);
			console.log(map_conf);
			$('#main-map').gmap3(map_conf);
			//$('#main-map').gmap3(kmllayer);
		});
		function merge_map_conf(oc, nc){
			if(undefined != nc.overlay)
			{
				if(undefined != nc.overlay.values)
				{
					if(!oc.hasOwnProperty('overlay'))
					{
						oc.overlay = {values:[]};
					}
					$.merge(oc.overlay.values, nc.overlay.values);

				}
			}
			if(undefined != nc.polygon)
			{
				if(undefined != nc.polygon.values)
				{
					if(!oc.hasOwnProperty('polygon'))
					{
						oc.polygon = {values:[]};
					}
					$.merge(oc.polygon.values, nc.polygon.values);
				}
			}
			return oc;
		}

		function generate_map_conf(){
			var map_conf = [];
			var markers = [];
			var overlays = [];
			var polygon = [];
			$('.selected-panel').children('div.one-poi').each(function(index){
					console.log('none');
					console.log($(this).attr('kml-id'));
					var marker = {latLng:[$(this).attr('lat'), $(this).attr('lon')]};
					if($(this).attr('type') == 'hotel')
					{
						marker.options = { icon:"/images/marker_hotel.png" };
					}
					var overlay  = {
						latLng:[$(this).attr('lat'), $(this).attr('lon')], 
							options:{
								content: '<div class="map-overlay">' +
									$(this).attr('poi_name') +
									'</div>',
									offset:{ y:-32, x:14 }
							},
					};

				$.merge(markers, [marker]);
				$.merge(overlays, [overlay]);
			});
			map_conf= {marker:{values:markers}};
			map_conf.overlay={values:overlays};
			map_conf.autofit={};
			map_conf.map={};
			map_conf.map.options={
				scrollwheel:false,
				streetViewControl:false,
				zoomControl:false,
				mapTypeControl: false,
				navigationControl: false,
				panControl:false,
			};
			$('.selected-panel').children('div.one-kml').each(function(index){
				if($(this).is("[kml-id]")){
					var kml = kml_list['id-' + $(this).attr('kml-id')];
					console.log('ddd');
					console.log(map_conf);
					console.log(kml.kml);
					console.log('888');
					console.log(map_conf);
					map_conf = merge_map_conf(map_conf, kml.kml);
				}
				});
			return map_conf;
		}

		function selected_panel_exists(poi_id){
			var ret = false;
			$('.selected-panel').children('div').each(function(index){
				if($(this).attr('poi_id') == poi_id) {
					ret = true;
				}
			});
			return ret;

		}

		google.maps.Map.prototype.clearMarkers = function() {
			for(var i=0; i < this.markers.length; i++){
				this.markers[i].setMap(null);
			}
			this.markers = new Array();
		};

	});
</script>
</head>

<body>
	<h1 style="width:100%; text-align:center; font-size:18px; padding-top:20px; padding-bottom:10px;"><?=$trip['title'];?></h1>
	<div class="operate-panel">
		<div class="generate-panel">
			<a class="btn btn-sm btn-primary" id="re-generate-map">重新生成地图</a>
		</div>
		<div class="days-panel">
			<div class="kml-button">KML</div>
			<?php foreach($trip_day_list as $day => $trip):?>
			<?php $day = $day + 1;?>
			<div class="day-button" day-id="<?=($day-1);?>">D<?=$day;?></div>
			<?php endforeach;?>
		</div>
		<div class="cur-day-panel">
		</div>
		<div class="selected-panel">
		</div>
	</div>
	<div id="main-map">
	</div>
</body>
</html>

