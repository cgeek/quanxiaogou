<!doctype html>
<html>
<head>
	<title><?php echo !empty($this->pageTitle) ? $this->pageTitle . ' - ' : '';?>行程管家ERP</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
	<!--link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css" /-->
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/plan.css" />
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/seajs/dist/sea.js"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->

</head>
<body <?php if(!empty(Yii::app()->adminUser)):?> admin_id=<?=Yii::app()->adminUser->id;?> <?php endif;?>>
	<div id="topbar" class="navbar navbar-inverse navbar-fixed-top">
		<div class="">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/admin">行程管家ERP</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li class="<?=($this->id == 'default') ? 'active' : '';?>"><a href="/admin">首页</a></li>
					<li><a href="/user/customers">客户</a></li>
					<li class="active"><a href="/user/trips">行程</a></li>
                    <li ><a href="/user/settings">设置</a></li>
				</ul>
				<ul class="nav navbar-nav navbar-right">
					<li><a href="http://shop102147639.taobao.com/" target="_blank">淘宝</a></li>
<?php if(Yii::app()->user->isGuest):?>
					<li><a href="/user/login">登录</a></li>
<?php else:?>
					<li class="dropdown">
					<a href="#" class="dropdown-toggle" data-toggle="dropdown">账号设置 <b class="caret"></b></a>
						<ul class="dropdown-menu">
							<li><a href="/user/changePassword">修改密码</a></li>
							<li><a href="/user/logout">退出登录</a></li>
						</ul>
					</li>
<?php endif;?>
				</ul>
			</div><!--/.nav-collapse -->
		</div>
	</div>
	<div id="content" class="clearfix" style="margin-top:50px;">
			<?php echo $content; ?>
	</div>

	<!-- Modal -->
	<div class="modal fade" id="modalView" tabindex="-1" role="dialog" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
			</div><!-- /.modal-content -->
		</div><!-- /.modal-dialog -->
	</div><!-- /.modal -->

<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('head');
    });
</script>

</body>
</html>

