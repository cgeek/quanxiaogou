<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class Controller extends CController
{
	/**
	 * @var string the default layout for the controller view. Defaults to '//layouts/column1',
	 * meaning using a single column layout. See 'protected/views/layouts/column1.php'.
	 */
	public $layout='/layouts/column1';
	/**
	 * @var array context menu items. This property will be assigned to {@link CMenu::items}.
	 */
	public $menu=array();
	/**
	 * @var array the breadcrumbs of the current page. The value of this property will
	 * be assigned to {@link CBreadcrumbs::links}. Please refer to {@link CBreadcrumbs::links}
	 * for more details on how to specify this property.
	 */

	public $breadcrumbs=array();

	public function filters()
	{
		return array(
			'adminAccess', // perform access control for CRUD operations
		);
	}

	public function filterAdminAccess($filterChain)
	{
		if ($filterChain->action->id==='login' || $filterChain->action->id==='makepdf' 
			|| $filterChain->action->id==='pdf' 
			|| $filterChain->action->id==='excel' 
			|| $filterChain->action->id==='app' 
			|| !Yii::app()->adminUser->isGuest)
			$filterChain->run();
		else
			Yii::app()->adminUser->loginRequired();
	}

	public function ajax_response($code = 200,$message="",$data = array())
	{
		$result['code'] = $code;
		$result['message'] = $message;
		$result['data'] = $data;
		echo json_encode($result);
		exit();
	}

}
