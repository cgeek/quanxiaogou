<?php

class CreateController extends Controller
{
	private $_data;

	public function actionCookie()
	{
		
		$cookie = Yii::app()->request->getCookies();

		unset($cookie['planinfo']);
		Yii::app()->request->cookies['planinfo'] = NULL;
		//var_dump($cookie['planinfo']->value);

	}

	public function actionStep1()
	{
		$this->render('step1', $this->_data);
	}

	public function actionStep2()
	{
		$country_id = Yii::app()->request->getParam('country_id');
		if(empty($country_id)) {
			$country_id = 2;
		}
		$country_db = Country::model()->findByPk($country_id);
		if(empty($country_db)) {
			echo 'country not find';
			die();
		}
		$this->_data['country'] = $country_db->attributes;
		$this->render('step2', $this->_data);
	}

	public function actionStep3()
	{

		$this->render('step3', $this->_data);
	}

	public function actionStep4()
	{
		$country_id = Yii::app()->request->getParam('country_id');
		$customer_id = Yii::app()->request->getParam('customer_id');

		$cookie = Yii::app()->request->getCookies();
		$planinfo = $cookie['planinfo']->value;
		if(empty($planinfo)) {
			$this->redirect('/plan/create/step1');
		}
		$planinfo = json_decode($planinfo, true);

		$title = "我的行程";
		$country_db = Country::model()->findByPk($country_id);
		if(!empty($country_db)) {
			$country_db = $country_db->attributes;
			$title = $country_db['name'] . "的行程";
		}

		$total_days = $the_day = 0;
		$trip_day_list = array();
		foreach($planinfo['citylist'] as $city) {
			//echo $city['name'];die();
			$total_days += $city['days'];
			for($i=0; $i< intval($city['days']); $i++) {
				$day = array(
					'city_name' => $city['city_name'],
					'city_id' => $city['city_id']
				);
				if($the_day > 0) {
					$day['date'] = date('Y-m-d', strtotime($planinfo['starttime'] . "+$the_day day" ));
				} else {
					$day['date'] = $planinfo['starttime'];
				}
				$trip_day_list[$the_day]  = $day;
				$the_day++;
			}
		}

		$admin_id = 0;
		if(!empty($customer_id)) {
			$customer_db = Customer::model()->findByPk($customer_id);
			if(!empty($customer_db)) {
				$title =  $customer_db['contact'] . '的' . $country_db['name'] . "行程";
				$customer_db = $customer_db->attributes;
				$admin_id = $customer_db['admin_id'];
				$customer_id = $customer_db['id'];
			} else {
				$customer_id = 0;
			}
		}

		$new_trip = new Trip();
		$new_trip->title = $title;
		$new_trip->admin_id = $admin_id;
		$new_trip->customer_id = $customer_id;
		$new_trip->start_time = $planinfo['starttime'];
		$new_trip->days = $total_days;
		$new_trip->start_city_id = $planinfo['scity']['i'];
		$new_trip->end_city_id = $planinfo['ecity']['i'];
		$new_trip->mtime = date('Y-m-d H:i:s', time());
		$new_trip->ctime = date('Y-m-d H:i:s', time());
		$new_trip->status = 1;
		$hash_string = $title . time() . $total_days;
		$new_trip->hash_id = md5($hash_string);

		if(!$new_trip->save()) {
			die('创建行程失败');
		}
		$trip_id = Yii::app()->db->getLastInsertId();

		//更新trip_id到customer表
		if($customer_id > 0 && $trip_id > 0) {
			Customer::model()->updateByPk($customer_id, array('trip_id' => $trip_id));
		}
		
		foreach($trip_day_list as $key=>$city) {
			$city_ids = array($city);
			//如果是第一天
			if($key == 0 && !empty($planinfo['scity'])) {
				$city_ids = array( array('city_id'=> $planinfo['scity']['i'], 'city_name' => $planinfo['scity']['n'], 'date' => $planinfo['starttime']),
				   					$city);
			}
			if(isset($trip_day_list[$key+1]) && $city['city_id'] != $trip_day_list[$key+1]['city_id'] ) {
				$city_ids[] =$trip_day_list[$key+1];
			}
			//如果是最后一天
			if($key == count($trip_day_list) - 1 && !empty($planinfo['ecity'])) {
				$edate = date('Y-m-d', strtotime($planinfo['starttime'] . "+$key day" ));
				$ecity = array(
								'city_id'=> $planinfo['ecity']['i'], 
								'city_name' => $planinfo['scity']['n'],
								'date'=> $edate
							);
				$city_ids[] = $ecity;
			}
			$city_ids = json_encode($city_ids);
			$new_trip_day = new TripDay;
			$new_trip_day->trip_id = $trip_id;
			$new_trip_day->date = $city['date'];
			$new_trip_day->title = '';
			$new_trip_day->city_id = $city['city_id'];
			$new_trip_day->status = 0;
			$new_trip_day->sort = $key + 1;
			$new_trip_day->city_ids = $city_ids;
			if(!$new_trip_day->save()) {
				var_dump($new_trip_day->getErrors());
			}
		}
		unset($cookie['planinfo']);

		$this->redirect('/plan/create/edit/?trip_id=' . $trip_id);
	}

	public function actionEdit($trip_id = NULL)
	{
		$this->pageTitle = '行程计划工具';
		//echo $trip_id;
		if(empty($trip_id)) {
			die('error');
		}
		$trip = Trip::model()->findByPk($trip_id);
		if(empty($trip)) {
			echo '404';die();
		}
		$this->_data['trip'] = $trip->attributes; 
		$this->render('edit', $this->_data);
	}

	public function actionTest()
	{
		$this->render('test', $this->_data);
	}
}
