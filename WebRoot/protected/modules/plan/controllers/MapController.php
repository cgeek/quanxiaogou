<?php
Yii::import('ext.ETcPdf',true);
Yii::import('ext.WkHtmlToPdf',true);

class MapController extends Controller
{
	private $_data;


	public function actionEdit()
	{
		$this->_trip_info();
		//var_dump($this->_data); exit;
		$this->_data['kml_list'] = $this->_get_kml_list_json();
		$this->_data['poi_type_cn'] = $this->_get_poi_type_cn();
		$this->renderPartial('map_edit', $this->_data);
	}

	private function _get_kml_list_json()
	{
		$ret = '';

		$criteria = new CDbCriteria;
		$criteria->addCondition("status >='0'");
		$kmls = Kml::model()->findAll($criteria);
		if(!empty($kmls)) {
			foreach($kmls as $key=>$kml) {
				$kml = $kml->attributes; 
				$ret["id-{$kml['id']}"] = array(
					'id'=>$kml['id'],
					'title'=>$kml['title'],
					'kml'=>json_decode($kml['kml']),
				);
			}
		}
		return json_encode($ret);
	}

	private function _trip_info()
	{

		$trip_id = Yii::app()->request->getParam('trip_id');
		$hash_id = Yii::app()->request->getParam('hash_id');

		if(empty($trip_id) && empty($hash_id)) {
			die('404');
		}

		$trip_db = null;
		if(!empty($trip_id)) {
			$trip_db = Trip::model()->findByPk($trip_id);
		} else if(!empty($hash_id)) {
			$trip_db = Trip::model()->findByAttributes(array('hash_id'=>$hash_id));
		}

		if(empty($trip_db)) {
			die('404');
		}
		$trip_db = $trip_db->attributes;
		$trip_id = $trip_db['id'];
		$this->_data['trip'] = $trip_db;

		$trip_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("trip_id='{$trip_id}'");
		$criteria->addCondition("status >='0'");
		$criteria->order = 'sort asc';
		$tripDays = TripDay::model()->findAll($criteria);
		if(!empty($tripDays)) {
			foreach($tripDays as $key=>$day) {
				//$result = $day->attributes;
				//date 
				//$result['date'] = date("Y-m-d", strtotime("+$key Day", strtotime($trip_db['start_time'])));
				$city_list = array();
				$city_list = json_decode($day['city_ids'], true);
				//$result['city_list'] = $city_list;
				if(count($city_list)) {
					//$result['has_city'] = TRUE;
				}

				$traffics = array();
				$criteria = new CDbCriteria;
				$criteria->addCondition("trip_day_id='{$day['id']}'");
				$criteria->addCondition("status >='0'");
				$tripDayTraffics = TripDayTraffic::model()->findAll($criteria);
				if(!empty($tripDayTraffics)) {
					foreach($tripDayTraffics as $traffic) {
						$traffics[] = $traffic->attributes;
					}
				}
				//$result['traffic_list'] = $traffics;

				$hotels = array();
				$criteria = new CDbCriteria;
				$criteria->addCondition("trip_day_id='{$day['id']}'");
				$criteria->addCondition("status >='0'");
				$tripDayHotels = TripDayHotel::model()->findAll($criteria);
				if(!empty($tripDayHotels)) {
					foreach($tripDayHotels as $hotel) {
						//$r = $hotel->attributes;
						$r = array();
						$hotel_info = Hotel::model()->findByPk($hotel->hotel_id);
						if(!empty($hotel_info)) {
							$hotel_info = $hotel_info->attributes;
							$r['id']= $hotel_info['id'];
							$r['name']= $hotel_info['title'];
							$r['lat']= $hotel_info['lat'];
							$r['lon']= $hotel_info['lon'];
							$r['type']= 'hotel';
						}
						$hotels[] = $r;
					}
				}
				$result['hotel_list'] = $hotels;

				$pois = array();
				$criteria = new CDbCriteria;
				$criteria->addCondition("trip_day_id='{$day['id']}'");
				$criteria->addCondition("status >='0'");
				$tripDayPois = TripDayPoi::model()->findAll($criteria);
				if(!empty($tripDayPois)) {
					foreach($tripDayPois as $poi) {
						$r = array();
						$poi_info = Poi::model()->findByPk($poi->poi_id);
						if(!empty($poi_info)) {
							$poi_info = $poi_info->attributes;
							$r['id']= $poi_info['poi_id'];
							$r['name']= $poi_info['poi_name'];
							$r['lat']= $poi_info['lat'];
							$r['lon']= $poi_info['lon'];
							$r['type']= $poi_info['type'];
						}
						//$r['poi_info'] = $poi_info;
						$pois[] = $r;

					}
				}
				$result['poi_list'] = $pois;
				$pois_by_type = array(
					'restaurant' => array(),
					'sight' => array(),
					'entertainment' => array(),
					'shopping' => array(),
				);
				foreach($pois as $poi)
				{
					$poi_type = $poi['type'];
					if($poi['type'] == 'island')
					{
						$poi_type = 'sight';
					}
					$pois_by_type[$poi_type][] = $poi;
				}
				//$result['poi_list_by_type'] = $pois_by_type;
				//var_dump($pois_by_type);;

				$trip_list[] = $result;
			}
		}
		//ajax_response(200, '', $trip_list);
		$this->_data['trip_day_list'] = $trip_list;

	}


	private function _get_poi_type_cn()
	{
		return array(
			'sight' => '地点',
			'entertainment' => '活动',
			'restaurant' => '餐厅',
			'shopping' => '购物',
		);
	
	}

}
	
