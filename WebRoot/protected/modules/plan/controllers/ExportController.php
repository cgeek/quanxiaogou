<?php
Yii::import('ext.ETcPdf',true);
Yii::import('ext.WkHtmlToPdf',true);

class ExportController extends Controller
{
	private $_data;

	public function actionApp()
	{
		$this->_trip_info();
		$this->_data['poi_type_cn'] = $this->_get_poi_type_cn();
		$this->renderPartial('app_new', $this->_data);
	}

	public function actionAppNew()
	{
		$this->_trip_info();
		$this->_data['poi_type_cn'] = $this->_get_poi_type_cn();
		$this->renderPartial('app_new', $this->_data);
	}

	public function actionFull()
	{
		$this->_trip_info();
		//var_dump($this->_data); exit;
		$this->_data['poi_type_cn'] = $this->_get_poi_type_cn();
		$this->renderPartial('full', $this->_data);
	}
	
	public function actionMakeimage(){
		$urls = Yii::app()->request->getParam('url');
		if($urls != null && sizeof($urls) > 0){
			$hash_str = "";
			if($this->checkRequestUrls($urls)){
				$options = array(
				    'no-outline',           // option without argument
				    'encoding' => 'UTF-8',  // option with argument
					'margin-top' => '0',
					'margin-bottom' => '0',
					'margin-left' => '0',
					'margin-right' => '0',
					'page-width' => '180',
					'page-height' => '240',
				);
				$pdf = new WkHtmlToPdf($options);
				foreach($urls as $url){
					$hash_str .= $url;
					$pdf->addPage("'{$url}'");
				}
				$this->_data['pdfname'] = md5($hash_str) . ".pdf";
				$this->_data['pdf'] = $pdf;
				$tmp_filename = tempnam(sys_get_temp_dir(), 'xtcn');
//				echo $tmp_filename . "<br/>\n";
				$tmp_output_pdf_filename = $tmp_filename . '.pdf';
				$tmp_output_jpg_filename = $tmp_filename . '.jpg';
				$pdf->saveAs($tmp_output_pdf_filename);
//				echo $tmp_output_pdf_filename . "<br/>\n";
				if(file_exists($tmp_output_pdf_filename)){
					// read page 1 
					$im = new Imagick();
					$im->setResolution(200,200);
					$im->readImage($tmp_output_pdf_filename);

					// convert to jpg 
					$im->setImageColorspace(255); 
					$im->setCompression(Imagick::COMPRESSION_JPEG); 
					$im->setCompressionQuality(60);
					$im->setImageFormat('jpeg'); 

					//resize 
//					$im->resizeImage(290, 375, imagick::FILTER_LANCZOS, 1);

					//write image on server 
					$im->writeImages($tmp_output_jpg_filename, true);
					$im->clear(); 
					$im->destroy(); 
					
					if(!file_exists($tmp_output_jpg_filename)){
						$im = new Imagick();
						
						$i = 0;
						$done_load = false;
						while(!$done_load){
							$load_file_name = $tmp_filename . '-' . $i . '.jpg';
							if(file_exists($load_file_name)){
								$im->readImage($load_file_name);
								unlink($load_file_name);
								$i++;
							}else{
								$done_load = true;
							}
						}

						/* Append the images into one */
						$im->resetIterator();
						$combined = $im->appendImages(true);
						
//						$combined-> writeImage($tmp_output_jpg_filename);

						/* Output the image */
						$combined->setImageFormat("png");
						header("Content-Type: image/png");
						echo $combined;
					}else{
						$im = new Imagick();
						$im->readImage($tmp_output_jpg_filename);
						unlink($tmp_output_jpg_filename);
						/* Output the image */
						$im->setImageFormat("png");
						header("Content-Type: image/png");
						echo $im;
					}
					unlink($tmp_output_pdf_filename);
//					echo “done.”;
				}
				if(file_exists($tmp_filename)){
					unlink($tmp_filename);
				}
			}else{
				ajax_response('404', '暂时不能访问');
			}
		}else{
			ajax_response('500', '参数错误');
		}
	}
	
	public function actionMakepdf()
	{
		$urls = Yii::app()->request->getParam('url');
		if($urls != null && sizeof($urls) > 0){
			$hash_str = "";
			if($this->checkRequestUrls($urls)){
				$options = array(
				    'no-outline',           // option without argument
				    'encoding' => 'UTF-8',  // option with argument
				);
				$pdf = new WkHtmlToPdf($options);
				foreach($urls as $url){
					$hash_str .= $url;
					$pdf->addPage("'{$url}' --footer-right 'P[page]'");
					//$pdf->addPage("'{$url}' --footer-html http://dev.quanxiaogou.com/plan/export/pdfFooter --header-right 'P[page]'");
				}
				$this->_data['pdfname'] = md5($hash_str) . ".pdf";
                $this->_data['pdf'] = $pdf;
                $pdf->send($this->_data['pdfname']);
				//$this->renderPartial('makepdf', $this->_data);
			} else {
				ajax_response('404', '暂时不能访问');
			}
		}else{
			ajax_response('500', '参数错误');
		}
	}

	public function actionTripNote()
	{
		$trip_id = Yii::app()->request->getParam('trip_id');
		if(empty($trip_id)) {
			ajax_response(404, '缺少参数');
		}

		$trip_note = TripNote::model()->findByAttributes(array('trip_id'=>$trip_id));
		if(!empty($trip_note)) {
			$this->_data['trip_note'] = $trip_note->attributes;
		}
		$this->renderPartial('trip_note', $this->_data);

	}

	public function actionPdfFooter()
	{
		header('Content-type: text/html; charset=utf-8');
		$this->renderPartial('pdf_footer');
	}

	private function checkRequestUrls($urls){
		$return_value = false;
		if($urls != null && sizeof($urls) > 0){
			$return_value = true;
			foreach($urls as $url){
				$urlArr = parse_url($url);
				/*
				if(substr_count($urlArr['host'], 'quanxiaogou.com') < 1){
					$return_value = false;
					break;
				}
				 */
			}
		}
		return $return_value;
	}

	public function actionExcel()
	{
		$this->_trip_info();
		//ajax_response(200, '', $this->_data);
		$this->renderPartial('excel', $this->_data);
	}

	public function actionPDF()
	{
		$this->_trip_info();
        $this->_data['poi_type_cn'] = $this->_get_poi_type_cn();
		//ajax_response(200, '', $this->_data);
		$this->renderPartial('pdf', $this->_data);
	}

	public function actionPDFDetail()
	{
		$this->_trip_info();
		$this->_data['poi_type_cn'] = $this->_get_poi_type_cn();
		//ajax_response(200, '', $this->_data);
		$this->renderPartial('pdf_detail', $this->_data);
	}


	public function actionPDF_old(){
		$this->_trip_info();
		
		$trip = $this->_data['trip'];
		
		$pdf = new MYPDF(PDF_PAGE_ORIENTATION, PDF_UNIT, PDF_PAGE_FORMAT, true, "UTF-8", false);
		$pdf->SetBarcodeURL('http://www.quanxiaogou.com/plan/export/app?hash_id=' . $trip['hash_id']);
		
		// set document information
		$pdf->SetCreator(PDF_CREATOR);
		$pdf->SetAuthor('xtcn');
		$pdf->SetTitle('行程 - 妙妙行程管家');
		$pdf->SetSubject($trip['title']);
		$pdf->SetKeywords('PDF, 行程列表, 旅行定制, 妙妙行程管家');

		// set default header data
		$pdf->SetHeaderData(PDF_HEADER_LOGO, PDF_HEADER_LOGO_WIDTH, PDF_HEADER_TITLE . ' : ' . $trip['title'], PDF_HEADER_STRING, array(0,64,255), array(0,64,128));
		$pdf->setFooterData(array(0,64,0), array(0,64,128));

		// set header and footer fonts
		$pdf->setHeaderFont(Array(PDF_FONT_NAME_MAIN, '', PDF_FONT_SIZE_MAIN));
		$pdf->setFooterFont(Array(PDF_FONT_NAME_DATA, '', PDF_FONT_SIZE_DATA));

		// set default monospaced font
		$pdf->SetDefaultMonospacedFont(PDF_FONT_MONOSPACED);

		// set margins
		$pdf->SetMargins(PDF_MARGIN_LEFT, PDF_MARGIN_TOP, PDF_MARGIN_RIGHT);
		$pdf->SetHeaderMargin(PDF_MARGIN_HEADER);
		$pdf->SetFooterMargin(PDF_MARGIN_FOOTER);

		// set auto page breaks
		$pdf->SetAutoPageBreak(TRUE, PDF_MARGIN_BOTTOM);

		// set image scale factor
		$pdf->setImageScale(PDF_IMAGE_SCALE_RATIO);
		
		// set some language-dependent strings (optional)
		$l = Array();

		// PAGE META DESCRIPTORS --------------------------------------
		$l['a_meta_charset'] = 'UTF-8';
		$l['a_meta_dir'] = 'ltr';
		$l['a_meta_language'] = 'zh';
		// TRANSLATIONS --------------------------------------
		$l['w_page'] = '页码';
		$pdf->setLanguageArray($l);
		
		$pdf->setFontSubsetting(true);

		// Content
		$pdf->AddPage();
//		$title_font = 'applelihei';
		$title_font = 'droidsansfallback';
//		$content_font = 'huawenxihei';
		$content_font = 'fzltxh';
//		$content_font = 'st_heiti_light';

		$pdf->SetFont($content_font, '', 11);
//		$pdf->SetFont('fzltxh', '', 11);
//		$pdf->SetFont('sofiasans', '', 11);
//		$pdf->SetFont('hmbc', '', 11);
		
		$html = '';
		$html .='<table style="margin-top:20px;" border="0.2px" id="printTable">'
			  . '		<tr style="font-family:' . $title_font . '; background-color:#D0E2F5;">'
			  . '		<td colspan=5 style="text-align:center; font-size:20px; background:#D0E2F5;">' . $trip['title'] . '</td>'
			  . '		</tr>'
			  . '		<tr style="font-family:' . $title_font . '; background-color:#6EA93E;">'
			  . '			<td style="width:87px;">日期</td>'
			  . '			<td style="width:360px;">行程简介</td>'
			  . '			<td>交通建议</td>'
			  . '		</tr>';
		$trip_day_list = $this->_data['trip_day_list'];
		$i = 0;
		foreach($trip_day_list as $trip_day){
			$i++;
			$tr_style = '';
			if($i % 2){
				$tr_style = ' style="background-color:#F9F9F9;"';
			}
			$html .= '<tr' . $tr_style . '>';
			$html .= '  <td style="width:87px;">';
			$html .= $trip_day['date'];
			if(!empty($trip_day['city_list'])){
				$html .= '<p>';
				foreach($trip_day['city_list'] as $key => $city){
					$html .= ($key > 0) ? ' - ' : '';
					$html .= $city['city_name'];
				}
				$html .= '</p>';
			}
			$html .= '  </td>';
			$html .= '  <td style="width:360px;">';
			if(!empty($trip_day['title'])){
				$html .= '<p style="font-family:' . $title_font . ';">' .  $trip_day['title'] . '</p>';
			}
			$html .= nl2br($trip_day['desc']);
			if(!empty($trip_day['hotel_list'])){
				foreach($trip_day['hotel_list'] as $hotel){
					$html .= <<<HTML
						<br/>
						<div style="font-family:{$title_font}; border-top:1px dashed #ccc; margin-bottom:0; margin-top:8px; font-weight:bold;padding-top:8px;"><br/>酒店：{$hotel['title']}</div>
HTML;
				}
			}
			$html .= '  </td>';
			$html .= '  <td>';
			if(!empty($trip_day['traffic_list'])){
				foreach($trip_day['traffic_list'] as $traffic){
					$format_str_start_hours = sprintf('%02d', $traffic['start_hours']);
					$format_str_start_minutes = sprintf('%02d', $traffic['start_minutes']);
					$format_str_end_hours = sprintf('%02d', $traffic['end_hours']);
					$format_str_end_minutes = sprintf('%02d', $traffic['end_minutes']);
					$html .= <<<HTML
						<div style="border-bottom:1px dashed #ccc;background-image:url('/images/bg_flight.png'); background-repeat:no-repeat;background-position:top right; margin-bottom:5px; margin-top:0px; padding-bottom:5px;">
							航班号：{$traffic['traffic_number']} <br>
							{$traffic['from_place']} - {$traffic['to_place']}  {$format_str_start_hours}:{$format_str_start_minutes} - {$format_str_end_hours}:{$format_str_end_minutes}<br/>
						</div>
HTML;
				}
			}
			$html .= '<br/>';
			$html .= nl2br($trip_day['traffic_note']);
			$html .= '  </td>';
			$html .= '</tr>';
		}
		$html .='</table>';

		$pdf->setCellPaddings(1, 1, 1, 2);

		$pdf->writeHTML($html, true, false, true, false, '');
				
		$this->_data['pdf'] = $pdf;
		$this->renderPartial('pdf', $this->_data);
	}

	public function actionUserCheckList(){
		$this->_trip_info();
		$trip_info = $this->_data['trip_day_list'];
		$cities = array();
		foreach($trip_info as $trip_day)
		{
			if(!empty($trip_day['city_list']))
			{
				foreach($trip_day['city_list'] as $city)
				{
					if(empty($cities[$city['city_name']]))
					{
						$cities[$city['city_name']] = 0; 
					}
					$cities[$city['city_name']]++; 
				}
			}

		}
		$city_name_sql_in_part = "('all'";
		foreach ($cities as $city_name=>$count)
		{
			$city_name_sql_in_part.=",'$city_name'";
		}
		$city_name_sql_in_part .= ")";

		$ck_list_categories = array(
			'行前准备'=> array(
				'empty_line' => 4,
			),
			'电子文档备份到邮箱'=>array(),
			'需要打印的文件'=>array(),
			'离家之前'=>array(),
			'要带的东西'=>array(
				'empty_line' => 4,
			),
			'要带回来的东西'=> array(
				'empty_line' => 6,
				'color_theme' => 'sea-blue',
			),

		);
		$ck_items = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("city_name in $city_name_sql_in_part");
		$criteria->order = 'display_order asc';
		$db_ck_list = UserCheckList::model()->findAll($criteria);
		if(!empty($db_ck_list)) 
		{
			foreach($db_ck_list as $ob_cklist) 
			{
				$cklist = $ob_cklist->attributes;
				$ck_items[] = $cklist;
			}
		}
		$f_ck_items = $ck_list_categories;
		foreach($ck_list_categories as $cat=>$val)
		{
			foreach($ck_items as $ck_item)
			{
				if($cat == $ck_item['category'])
				{
					$f_ck_items[$cat][] = $ck_item;
				}
			}
		}
		$this->_data['ck_items'] = $f_ck_items;
		$this->renderPartial('user_check_list', $this->_data);
	}

	private function _trip_info()
	{

		$trip_id = Yii::app()->request->getParam('trip_id');
		$hash_id = Yii::app()->request->getParam('hash_id');
		$admin_id = Yii::app()->request->getParam('admin_id');

		if(empty($trip_id) && empty($hash_id)) {
			die('404');
		}

        if(!empty($admin_id)) {
            $admin_db = Admin::model()->findByPk($admin_id);
            if(!empty($admin_db)) {
                $this->_data['trip_admin'] = $admin_db->attributes;
            }
        }
		$trip_db = null;
		if(!empty($trip_id)) {
			$trip_db = Trip::model()->findByPk($trip_id);
		} else if(!empty($hash_id)) {
			$trip_db = Trip::model()->findByAttributes(array('hash_id'=>$hash_id));
		}

		if(empty($trip_db)) {
			die('404');
		}
		$trip_db = $trip_db->attributes;
		$trip_id = $trip_db['id'];
		$this->_data['trip'] = $trip_db;

        if(!empty($trip_db['admin_id'])) {
            $admin = Admin::model()->findByPk($trip_db['admin_id']);
            if(!empty($admin)) {
                $this->_data['admin'] = $admin->attributes;
            }
        }
		$this->_data['trip_note'] = '';
		//trip_note
		$trip_note_db = TripNote::model()->findByAttributes(array('trip_id'=>$trip_id));
		if(!empty($trip_note_db)) {
			$this->_data['trip_note'] = $trip_note_db->note;
		}


		$trip_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("trip_id='{$trip_id}'");
		$criteria->addCondition("status >='0'");
		$criteria->order = 'sort asc';
		$tripDays = TripDay::model()->findAll($criteria);
		if(!empty($tripDays)) {
			foreach($tripDays as $key=>$day) {
				$result = $day->attributes;
				//date 
				$result['date'] = date("Y-m-d", strtotime("+$key Day", strtotime($trip_db['start_time'])));
				$city_list = array();
				$city_list = json_decode($day['city_ids'], true);
				$result['city_list'] = $city_list;
				if(count($city_list)) {
					$result['has_city'] = TRUE;
                }

                //当日图片，支持多图
                $images = array();
                if(!empty($result['image'])) {
                    $image_list = explode(',', $result['image']);
                    if(!empty($image_list)) {
                        foreach($image_list as $image) {
                            if(empty($image)) {
                                continue;
                            }
                            $images[] = array(
                                'image_hash' => $image,
                                'image_url' => upimage($image, 'org')
                            );
                        }
                    }
                }
                $result['images'] = $images;

				$traffics = array();
				$criteria = new CDbCriteria;
				$criteria->addCondition("trip_day_id='{$day['id']}'");
				$criteria->addCondition("status >='0'");
				$tripDayTraffics = TripDayTraffic::model()->findAll($criteria);
				if(!empty($tripDayTraffics)) {
					foreach($tripDayTraffics as $traffic) {
						$traffics[] = $traffic->attributes;
					}
				}
				$result['traffic_list'] = $traffics;

				$hotels = array();
				$criteria = new CDbCriteria;
				$criteria->addCondition("trip_day_id='{$day['id']}'");
				$criteria->addCondition("status >='0'");
				$tripDayHotels = TripDayHotel::model()->findAll($criteria);
				if(!empty($tripDayHotels)) {
					foreach($tripDayHotels as $hotel) {
						$r = $hotel->attributes;
						$hotel_info = Hotel::model()->findByPk($hotel->hotel_id);
						if(!empty($hotel_info)) {
							$hotel_info = $hotel_info->attributes;
							$r['hotel_name_english']= $hotel_info['hotel_name_english'];
							$r['address']= $hotel_info['address'];
							$r['desc']= $hotel_info['desc'];
							$r['phone'] = $hotel_info['phone'];
							$r['traffic']= $hotel_info['traffic'];
							$r['website'] = $hotel_info['website'];
							$r['price'] = $hotel_info['price'];
						}
						$hotels[] = $r;
					}
				}
				$result['hotel_list'] = $hotels;

				$pois = array();
				$criteria = new CDbCriteria;
				$criteria->addCondition("trip_day_id='{$day['id']}'");
				$criteria->addCondition("status >='0'");
				$tripDayPois = TripDayPoi::model()->findAll($criteria);
				if(!empty($tripDayPois)) {
					foreach($tripDayPois as $poi) {
						$r = $poi->attributes;
						$poi_info = Poi::model()->findByPk($poi->poi_id);
						if(!empty($poi_info)) {
							$poi_info = $poi_info->attributes;
							$r['cover_image']= $poi_info['cover_image'];
							$r['address']= $poi_info['address'];
							$r['desc']= $poi_info['desc'];
							$r['phone'] = $poi_info['phone'];
							$r['traffic']= $poi_info['traffic'];
							$r['website'] = $poi_info['website'];
							$r['price'] = $poi_info['price'];
							$r['is_private'] = $poi_info['is_private'];
						}
						$r['poi_info'] = $poi_info;
						$pois[] = $r;

					}
				}
				$result['poi_list'] = $pois;
				$pois_by_type = array(
					/*
					'restaurant' => array(),
					'sight' => array(),
					'entertainment' => array(),
					'shopping' => array(),
					 */
				);
				foreach($pois as $poi)
				{
					$poi_type = $poi['type'];
					if($poi['type'] == 'island')
					{
						$poi_type = 'sight';
					}
					$pois_by_type[$poi_type][] = $poi;
				}
				$result['poi_list_by_type'] = $pois_by_type;
				//var_dump($pois_by_type);;

				$trip_list[] = $result;
			}
		}
		//ajax_response(200, '', $trip_list);
		$this->_data['trip_day_list'] = $trip_list;

	}

	private function _get_poi_type_cn()
	{
		return array(
			'sight' => '景点',
			'entertainment' => '活动',
			'restaurant' => '餐厅',
			'shopping' => '购物',
		);
	
	}

}

// Extend the TCPDF class to create custom Header and Footer
class MYPDF extends EtcPdf {
	protected $barcode_url = '';
	
	public function SetBarcodeURL($url){
		$this->barcode_url = $url;
	}
	
	// Page footer
	public function Footer() {
		// Position at 15 mm from bottom
		$this->SetY(-15);
		// Set font
		$this->SetFont('stsongstdlight', 'I', 8);
		// Page number
		$this->Cell(0, 10, '第 ' . $this->getAliasNumPage() . ' 页 / 共 ' . $this->getAliasNbPages() . ' 页', 0, false, 'C', 0, '', 0, false, 'T', 'M');
		$barcode_style = array(
			'border' => false,
			'padding' => 0,
			'fgcolor' => array(0,0,0),
			'bgcolor' => false
		);
		$this->write2DBarcode($this->barcode_url, 'QRCODE,H', 179, 1, 16, 16, $barcode_style, 'N');
	}
}
