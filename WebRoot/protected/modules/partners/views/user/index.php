<link rel="stylesheet" type="text/css" href="/css/trip.css">
<link rel="stylesheet" type="text/css" href="/css/partner.css">

<div class="portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa icon-docs"></i>我的行程案例
        </div>
    </div>
    <div class="portlet-body">

        <div class="tripList row">
            <?php foreach($trip_list as $trip) :?>
                <div class="tripCard col-lg-6 col-sm-12">
                    <div class="innerCard">
                        <div class="coverImage col-sm-2">

                        </div>
                        <div class="tripInfo">
                            <h4><?=$trip['title'];?></h4>
                            <p class="tripTime">出发日期：<?=$trip['start_time'];?>，<?=$trip['days'];?>天</p>
                            <?php if($trip['customer_id'] > 0):?>
                                <p><a href="/customer/edit/?id=<?=$trip['customer_id'];?>"><i class="fa fa-user"></i> <?=$trip['customer_name'];?></a></p>
                            <?php endif;?>
                        </div>
                        <div class="tripActions" trip_id="<?=$trip['id'];?>">
                            <a href="/trip/preview?type=app&id=<?=$trip['id'];?>" class="btn btn-xs btn-success">
                                <i class="fa fa-share"></i>发送
                            </a>

                            <a href="/trip/edit?id=<?=$trip['id'];?>" class="btn btn-xs btn-success" target="_blank">
                                <i class="fa fa-edit"></i>修改
                            </a>

                            <a class="btn btn-xs btn-default copy-btn" data-toggle="modal" data-target=".modal-copy-trip">
                                <i class="fa fa-copy"></i>复制
                            </a>
                            <a href="javascript:void(0);" class="btn btn-xs btn-danger delete_trip_btn" >
                                <i class="fa fa-trash"></i>删除
                            </a>
                        </div>
                    </div>
                </div>
            <?php endforeach;?>
                <div class="tripCard col-lg-6 col-sm-12">
                    <div class="innerCard newTrip">
                        <a class="create-trip-btn" href="javascript:void(0);">+ 添加行程</a>
                    </div>
                </div>
        </div>

    </div>
</div>


<div class="portlet">
    <div class="portlet-title">
        <div class="caption">
            <i class="fa fa-bell-o"></i>最新客户需求
        </div>
        <div class="">

        </div>
    </div>
    <div class="portlet-body">
        <div class="note note-info">
            <h4 class="block">如何收集客户需求？</h4>
            <p style="word-break: break-all;"> 你可以通过微信发送下面链接给客户收集客户需求：<a target="_blank" href="<?=Yii::app()->request->hostInfo;?>/customer/collect?pid=<?=Yii::app()->partner->id;?>">http://partner.xiaogoulvxing.com/customer/collect?pid=<?=Yii::app()->partner->id;?></a></p>
        </div>
        <div class="customerList row">

            <?php foreach($customer_list as $customer) :?>
                <div class="customerCard col-lg-3 col-sm-3">
                    <div class="innerCard">
                        <div class="cardHead">
                            <span class="contact"><?=$customer['contact'];?></span>
                            <div class="connect">
                                <a href="/customer/edit?id=<?=$customer['id'];?>">ID：<?=$customer['id'];?></a> <br />
                                Mobile：<?=$customer['mobile'];?><br />
                                wechat：<?=$customer['wechat'];?>
                            </div>
                        </div>
                        <div class="cardBody">
                            <div class="row">
                                <div class="col-sm-6 origin">
                                    <h4>出发地</h4>
                                    <?=empty($customer['origin'])? '未知' : $customer['origin'];?>
                                </div>
                                <div class="col-sm-6 destination">
                                    <h4>目的地</h4>
                                    <?=$customer['destination'];?>
                                </div>
                                <div class="col-sm-12 travelDate">
                                    <?php if(!empty($customer['start_date'])):?>
                                        <?=$customer['start_date'];?> &nbsp;&nbsp; --- &nbsp;&nbsp; <?=$customer['end_date'];?>
                                    <?php endif;?>
                                </div>
                            </div>
                            <div class="remark">
                                <h4>行程需求：</h4>
                                <p><?=$customer['remark'];?></p>
                            </div>
                            <div class="actions">
                                <a class="btn green-haze btn-xs" href="/customer/edit/?id=<?=$customer['id'];?>">编辑</a>
                                <a  class="btn green-haze btn-xs" href="/trip/quickCreate/?customer_id=<?=$customer['id'];?>">创建行程</a>
                                <a class="btn btn-danger btn-xs delete_customer_btn" href="javascript:void(0);" customer_id="<?=$customer['id'];?>">删除</a>
                            </div>

                        </div>

                    </div>
                </div>
            <?php endforeach;?>

            <div class="customerCard col-lg-3 col-sm-3">
                <div class="innerCard addCustomerBox">

                    <a href="/customer/add" class="addCustomer">
                        <span style="font-size:20px;display: block;">手动添加</span>
                        +
                    </a>

                </div>
            </div>
        </div>
    </div>
</div>





<div class="modal fade modal-copy-trip" tabindex="-1" >
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">复制行程</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="copy-form">
                    <div class="form-group" style="display: none;">
                        <label for="sourceId">当前行程ID</label>
                        <input name="sourceId" type="text" class="form-control" id="sourceId" value="" readonly>
                    </div>
                    <div class="form-group">
                        <label for="inputTitle">复制后的行程标题</label>
                        <input name="targetTitle" type="text" class="form-control" id="inputTitle" placeholder="行程标题">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success copy-submit-btn">确定复制</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade modal-create-trip" tabindex="-1" >
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">创建行程</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="create-trip-form" action="/trip/quickCreate" method="get">
                    <div class="form-group">
                        <label for="inputTitle">行程标题：</label>
                        <input name="title" type="text" class="tripTitle form-control" id="inputTitle" placeholder="行程标题">
                    </div>
                    <div class="form-group">
                        <label for="inputTitle">类型：</label>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="type" id="optionsRadios1" value="customer" checked>
                                客户的行程
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="type" id="optionsRadios2" value="demo">
                                行程模板
                            </label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-success create-trip-submit-btn">确定创建</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>
<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('partner/trip_list');
        router.load('partner/customer_list');
    });
</script>
