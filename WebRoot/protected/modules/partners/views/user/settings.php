<!-- END THEME PANEL -->
<h3 class="page-title"> 修改资料
    <small>user account </small>
</h3>
<div class="row">
    <div class="col-md-12">
        <div class="profile-sidebar">
            <div class="portlet light profile-sidebar-portlet " style="padding-bottom:20px !important;">
                <div class="profile-userpic">
                    <img id="companyLogo" src="<?=($user['company_logo'] == '') ? '/images/no-avatar.jpg': upimage($user['company_logo'], 'w240h240');?>" class="img-responsive" alt="">
                </div>
                <div class="profile-usertitle">

                    <span class="btn btn-xs btn-success fileinput-button">
							 <i class="glyphicon glyphicon-circle-arrow-up"></i>
							 <span>修改头像</span>
							 <!-- The file input field used as target for the file upload widget -->
							<input id="fileupload" type="file" name="Filedata" multipart=false/>
					    </span>


                    <!--div class="profile-usertitle-name"> <?=(Yii::app()->partner->company == '') ? '': Yii::app()->partner->company;?> </div>
                    <div class="profile-usertitle-job"> &nbsp; </div-->
                </div>
            </div>
        </div>

        <div class="profile-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="portlet light ">
                        <div class="portlet-title tabbable-line">
                            <div class="caption caption-md">
                                <i class="icon-globe theme-font hide"></i>
                                <span class="caption-subject font-blue-madison bold uppercase">账号信息</span>
                            </div>
                            <ul class="nav nav-tabs">
                                <li class="active">
                                    <a href="#tab_1_1" data-toggle="tab">修改资料</a>
                                </li>
                                <!--li>
                                    <a href="#tab_1_2" data-toggle="tab">修改头像</a>
                                </li-->
                                <li>
                                    <a href="#tab_1_3" data-toggle="tab">修改密码</a>
                                </li>
                            </ul>
                        </div>
                        <div class="portlet-body">
                            <div class="tab-content">
                                <!-- PERSONAL INFO TAB -->
                                <div class="tab-pane active" id="tab_1_1">
                                    <form id="userSettingsForm" role="form" action="/user/settings" method="POST">
                                        <input type="hidden" name="id" value="<?=$user['id'];?>">
                                        <div class="form-group">
                                            <label class="control-label">用户名</label>
                                            <input type="text" name="user_name" placeholder="John" class="form-control" disabled="disabled" value="<?=$user['username'];?>" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">昵称</label>
                                            <input type="text" name="nick_name" placeholder="nick name" class="form-control" value="<?=$user['nick_name'];?>" />
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">邮箱地址</label>
                                            <input type="text" name="email" placeholder="Email" class="form-control" value="<?=$user['email'];?>" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">手机号</label>
                                            <input type="text" name="mobile" placeholder="+1 646 580 DEMO" value="<?=$user['mobile'];?>" class="form-control" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">公司名称</label>
                                            <input type="text" name="company" placeholder="" value="<?=$user['company'];?>" class="form-control" /> </div>
                                        <div class="form-group">
                                            <label class="control-label">公司简介</label>
                                            <textarea class="form-control" name="company_intro" rows="5" placeholder=""><?=$user['company_intro'];?></textarea>
                                        </div>
                                        <div class="margiv-top-10">
                                            <button type="submit" id="submit-btn" class="btn green">确认修改</button>
                                        </div>
                                    </form>
                                </div>
                                <!-- END PERSONAL INFO TAB -->
                                <!-- CHANGE AVATAR TAB -->
                                <div class="tab-pane" id="tab_1_2">

                                </div>
                                <!-- END CHANGE AVATAR TAB -->
                                <!-- CHANGE PASSWORD TAB -->
                                <div class="tab-pane" id="tab_1_3">

                                    <form action="#" id="changePasswordForm" method="post">
                                        <div class="form-group">
                                            <label class="control-label">旧密码</label>
                                            <input type="password" name="old_password" class="form-control" id="inputOldPassword" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">新密码</label>
                                            <input type="password" name="new_password" class="form-control" id="inputNewPassword" placeholder="">
                                        </div>
                                        <div class="form-group">
                                            <label class="control-label">确认新密码</label>
                                            <input type="password" name="check_new_password" class="form-control" id="inputCheckNewPassword" placeholder="">
                                        </div>
                                        <div class="margin-top-10">
                                            <button id="submit-password-btn" type="submit" class="btn green"> 确认修改 </button>
                                        </div>
                                    </form>
                                </div>
                                <!-- END CHANGE PASSWORD TAB -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PROFILE CONTENT -->
    </div>
</div>
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->



<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('partner/changeUserInfo');
        router.load('partner/changePassword');
    });
</script>
