

<div class="tabbable-line">
    <ul class="nav nav-tabs ">
        <li class="active">
            <a href="javascript:void(0);" > 我的攻略 </a>
        </li>

        <a class="pull-right btn btn-sm btn-success hidden-xs" type="demo" href="/note/new">添加攻略</a>
    </ul>

    <div class="tab-content">

        <div class="portlet-body" >
            <div class="mt-element-card mt-element-overlay">
                <div class="row">
                    <?php if(!empty($note_list)):?>
                    <?php foreach($note_list as $note):?>
                        <div class="col-lg-3 col-md-4 col-sm-6 col-xs-12">
                            <div class="mt-card-item" style="background: #fff;">
                                <div class="mt-card-avatar mt-overlay-1">
                                    <img src="<?=empty($note['cover_image']) ? '/images/trip-cover-image.png' : $note['cover_image'];?>">
                                    <div class="mt-overlay">
                                        <ul class="mt-info">
                                            <li>
                                                <a class="btn default btn-outline" href="/note/edit?id=<?=$note['id'];?>">
                                                    <i class="icon-pencil"></i>
                                                </a>
                                            </li>
                                            <li>
                                                <a target="_blank" class="btn default btn-outline" href="/note/view/<?=$note['id'];?>">
                                                    <i class="icon-link"></i>
                                                </a>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="mt-card-content">
                                    <h3 class="mt-card-name"><?=$note['title'];?></h3>
                                    <p class="mt-card-desc font-grey-mint"><?=$note['desc'];?></p>

                                </div>
                            </div>
                        </div>
                    <?php endforeach;?>
                    <?php endif;?>
                </div>
            </div>

        </div>
    </div>
</div>