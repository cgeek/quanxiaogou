<link href="/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
<script src="/assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
<script src="/assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>


<script src="//cdn.bootcss.com/underscore.js/1.8.3/underscore-min.js"></script>

<div class="row">
    <div class="col-md-12">
        <div class="portlet light portlet-fit portlet-form ">
            <div class="portlet-title">
                <div class="caption">
                    <i class="icon-settings font-dark"></i>
                    <span class="caption-subject font-dark sbold uppercase">编辑攻略</span>
                </div>
            </div>
            <div class="portlet-body">
                <!-- BEGIN FORM-->
                <form action="#" id="note_edit_form" class="form-horizontal">

                    <div class="form-body">
                        <div class="alert alert-danger display-hide">
                            <button class="close" data-close="alert"></button> You have some form errors. Please check below. </div>
                        <div class="alert alert-success display-hide">
                            <button class="close" data-close="alert"></button> Your form validation is successful! </div>
                        <div class="form-group">
                            <label for="city_id" class="col-lg-2 control-label">所在城市<span class="required"> * </span></label>
                            <div class="col-lg-2">
                                <select name="city_id" id="city_id" class="form-control">
                                    <option value="">所在城市</option>
                                    <?php foreach($city_list as $city):?>
                                        <option value="<?=$city['id'];?>"<?=$city['selected']?>><?=$city['name'];?></option>
                                    <?php endforeach;?>
                                </select>
                                <span class="helpblock"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-2">标题
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <input type="text" name="title" data-required="1" class="form-control" value="<?=isset($note)? $note['title'] : '';?>" /> </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-2">内容
                                <span class="required"> * </span>
                            </label>
                            <div class="col-md-9">
                                <textarea class="wysihtml5 form-control" rows="15" name="content" data-error-container="#editor1_error"><?=isset($note)? $note['content'] : '';?></textarea>
                                <div id="editor1_error"> </div>
                            </div>
                        </div>
                    </div>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-2 col-md-9">
                                <button type="submit" class="btn green">保存</button>
                                <button type="button" class="btn default">取消</button>
                            </div>
                        </div>
                    </div>
                </form>
                <!-- END FORM-->
            </div>
            <!-- END VALIDATION STATES-->
        </div>
    </div>
</div>
</div>
<!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->


<script>




  //  var xhrFetchingImages =false;


//    $.extend($.fn.wysihtml5.Constructor.prototype, bootWysiOverrides);

    $(function() {

        // override options
        var wysiwygOptions = {
            customTags: {
                "em": {},
                "strong": {},
                "hr": {}
            },
            customStyles: {
                // keys with null are used to preserve items with these classes, but not show them in the styles dropdown
                'shrink_wrap': null,
                'credit': null,
                'tombstone': null,
                'chat': null,
                'caption': null
            },
            customTemplates: {
                /* this is the template for the image button in the toolbar */
                image: function(locale) {
                    return "<li>" +
                        "<div class='bootstrap-wysihtml5-insert-image-modal modal hide fade'>" +
                        "<div class='modal-header'>" +
                        "<a class='close' data-dismiss='modal'>&times;</a>" +
                        "<h3>" + locale.image.insert + "</h3>" +
                        "</div>" +
                        "<div class='modal-body'>" +
                        "<div class='chooser_wrapper'>" +
                        "<table class='image_chooser images'></table>" +
                        "<h4>Or Upload one to insert</h4>" +
                        "<input name=\"file\" id=\"file1\" type=\"file\">" +
                        "<div id=\"uploadresult\"></div>" +
                        "</div>" +
                        "</div>" +
                        "<div class='modal-footer'>" +
                        "<a href='#' class='btn' data-dismiss='modal'>" + locale.image.cancel + "</a>" +
                        "</div>" +
                        "</div>" +
                        "<a class='btn default' data-wysihtml5-command='insertImage' title='" + locale.image.insert + "'><i class='fa fa-picture-o'></i></a>" +
                        "</li>";
                }
            }
        };

        $('.tip').tooltip();
        $('.wysihtml5').wysihtml5($.extend(wysiwygOptions,{}));
    });

    //$('.wysihtml5').wysihtml5();


</script>



<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('partner/poi_note_edit');
    });
</script>