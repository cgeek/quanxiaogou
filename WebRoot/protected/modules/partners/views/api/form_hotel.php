<?php if(!empty($hotel)):?>
<form action="#" method="POST" id="jsplans_pophotel_form">
	<div class="pl_poi_box">
		<div class="title clearfix fontYaHei">
			<img src="<?=upimage($hotel['cover_image']);?>" onerror="this.src='/images/plan/pl_hotel_default.png';" width="80" height="80" alt="" id="js_plans_pophotel_pic"/>
			<p class="btit" id="js_plans_addhotelevent_show"><strong><?=$hotel['title'];?></strong></p>
			<input type="text" name="title" style="display:none;" class="ui2_input" value="<?=$hotel['title'];?>" placeholder="请输入酒店的名字" id="js_plans_addhotelevent" />
			<div class="clearfix title_tpap" style="display:none;" id="js_plans_searchhotel_result">
				<div class="title_list">
					<ul>
					</ul>
				</div>
			</div>
		</div>

		<div class="textarea">
			<textarea name="note" id="js_plans_pophotel_note" placeholder="在这里添加备注" class="ui2_textarea" rel-tip="#js_plans_pophotel_note_tip"><?=$hotel['note'];?></textarea>
			<div class="clearfix pr">
				<p class="words cLightgray mt5" id="js_plans_pophotel_note_tip"><span class="sum">0</span> / 1000 字</p>
			</div>
		</div>
		<div class="button clearfix">
			<input type="button" class="ui_buttonB fr ml10" value="保 存" id="js_plans_pophotel_save"/>
			<input type="button" class="ui_button_cancel fr" value="取 消" id="js_plans_pophotel_cancel"/>
		</div>
	</div>
</form>

<?php else:?>


<form action="#" method="POST" id="jsplans_pophotel_form">
	<div class="pl_poi_box">
		<div class="title clearfix fontYaHei">
			<img src="http://static.qyer.com/images/plan2/pl_hotel_default.png" onerror="this.src='http://static.qyer.com/images/plan2/pl_hotel_default.png';" width="80" height="80" alt="" id="js_plans_pophotel_pic"/>
			<p class="btit" id="js_plans_addhotelevent_show" style="display:none;"><strong></strong></p>
			<input type="text" class="ui2_input" value="" placeholder="请输入酒店的名字" id="js_plans_addhotelevent" />

			<div class="clearfix title_tpap" style="display:none;" id="js_plans_searchhotel_result">
				<div class="title_list">
					<ul>
                    </ul>
                </div>
            </div>
		</div>
        <div class="list hotel_list_cnt clearfix">
			<div class="item">
				<div class="text fontSong">入住日期:</div>
				<div class="cnt">
					<input type="hidden" name="inDay" value="">
					<div class="tra_select">
						<strong class="titles"><span>&nbsp;</span></strong>
						<div class="contents">
							<ul>
					<?php if(!empty($trip_day_list)):?>
						<?php foreach($trip_day_list as $trip_day):?>
								<li><a href="javascript:;" data-value="<?=$trip_day['trip_day_id'];?>"><?=$trip_day['title'];?></a></li>
						<?php endforeach;?>
					<?php endif;?>
							</ul>
						</div>
					</div>
				</div>
			</div>
            <div class="item">
                <div class="text fontSong">退房日期:</div>
                <div class="cnt"><input type="hidden" name="outDay" value="">
                    <div class="tra_select">
                        <strong class="titles"><span>&nbsp;</span></strong>
                        <div class="contents">
                            <ul>
					<?php if(!empty($trip_day_list)):?>
						<?php foreach($trip_day_list as $key=>$trip_day):?>
							<?php if($key == 0) continue;?>
								<li><a href="javascript:;" data-value="<?=$trip_day['trip_day_id'];?>"><?=$trip_day['title'];?></a></li>
						<?php endforeach;?>
					<?php endif;?>
							</ul>
						</div>
					</div>
				</div>
			</div>

        </div>
        <div class="textarea">
			<textarea name="note" id="js_plans_pophotel_note" placeholder="在这里添加备注" class="ui2_textarea" rel-tip="#js_plans_pophotel_note_tip"></textarea>
			<div class="clearfix pr">
				<p class="words cLightgray mt5" id="js_plans_pophotel_note_tip"><span class="sum">0</span> / 1000 字</p>
			</div>
		</div>
		<div class="button clearfix">
            <input type="button" class="ui_buttonB fr ml10" value="保 存" id="js_plans_pophotel_save"/>
            <input type="button" class="ui_button_cancel fr" value="取 消" id="js_plans_pophotel_cancel"/>
        </div>
    </div>
</form>

<?php endif;?>
