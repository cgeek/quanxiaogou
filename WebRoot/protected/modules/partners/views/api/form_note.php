
<form action="#" method="POST" id="jsplans_popdesc_form">
    <div class="pl_poi_box">
        <div class="textarea">
            <h5>行程亮点</h5>
            <input type="text" class="ui2_input" value="<?=isset($trip_day) ? $trip_day['title'] : '';?>" placeholder="当日行程亮点" name="trip_title" style="color:#999; width: 510px;">
        </div>
        <div class="textarea">
            <h5>行程安排</h5>
            <textarea  style="min-height:150px; border:1px solid #ccc; padding:5px; font-size:13px; line-height: 20px; color:#999; outline: none;" name="desc" placeholder="当天行程安排" class="ui2_textarea" rel-tip="#js_plans_pophotel_note_tip"><?=isset($trip_day) ? $trip_day['desc'] : '';?></textarea>
        </div>
        <div class="textarea">
            <h5>当日交通说明</h5>
            <textarea  style="min-height:60px; border:1px solid #ccc; padding:5px; font-size:13px; line-height: 20px; color:#999; outline: none;" name="traffic_note" placeholder="当天交通说明、建议、注意事项。如：地铁换乘，接送地址" class="ui2_textarea" rel-tip="#js_plans_pophotel_note_tip"><?=isset($trip_day) ? $trip_day['traffic_note'] : '';?></textarea>
        </div>
        <div class="button clearfix">
            <input type="button" class="ui_buttonB fr ml10" value="保 存" id="js_plans_popdesc_save"/>
            <input type="button" class="ui_button_cancel fr" value="取 消" id="js_plans_popdesc_cancel"/>
        </div>
    </div>
</form>