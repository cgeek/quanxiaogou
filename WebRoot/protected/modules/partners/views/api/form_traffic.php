<link href="/css/traffic.css" rel="stylesheet" type="text/css" />
<form name="trafficform" id="trafficform" onsubmit="return false;">
	<input type="hidden" name="id" value="<?=isset($traffic) ? $traffic['id'] : '';?>" />
	<input type="hidden" name="doaction"  value="<?=isset($traffic) ? 'edit' : 'add';?>"/>
    <div class="pl_traffic_box">
        <h3 class="fontYaHei">自定义交通方案</h3>
        <div class="list clearfix">
        	<div class="item">
            	<div class="text fontSong"><span>·</span>出发城市:</div>
                <div class="cnt">
                	<input type="text"  name="from_place" id="trafficfromplace" class="ui2_input" placeholder="" value="<?=isset($traffic) ? $traffic['from_place'] : '';?>" />
                    <div class="citylist" id="trafficfromplacelist" style="z-index:999">
                    </div>
                	<input type="hidden" name="from_placeid" value="<?=isset($traffic) ? $traffic['from_placeid'] : '';?>"/>
                </div>
            </div>
        	<div class="item">
            	<div class="text fontSong"><span>·</span>到达城市:</div>
                <div class="cnt">
                	<input type="text" name="to_place" id="traffictoplace" class="ui2_input" placeholder="" value="<?=isset($traffic) ? $traffic['to_place'] : '';?>" />
                    <div class="citylist" id="traffictoplacelist" style="z-index:999">
                    </div>               	
                	<input type="hidden" name="to_placeid" value="<?=isset($traffic) ? $traffic['to_placeid'] : '';?>" />
                </div>
            </div>

        	<div class="item">
            	<div class="text fontSong"><span>·</span>交通方式:</div>
                <div class="cnt">
                	<input type="hidden" name="mode" id="t_tripmode" value="<?=isset($traffic) ? $traffic['mode'] : '';?>"/>
                	<div class="tra_style_select">
                    	<strong class="titles"><span class="plane">飞机</span></strong><!-- 错误添加 ui2_error -->
                        <div class="contents">
                        	<ul>
								<li><a href="javascript:;" class="plane" data-value="1">飞机</a></li>
                            	<li><a href="javascript:;" class="train" data-value="2">火车</a></li>
                            	<li><a href="javascript:;" class="bus" data-value="3">巴士</a></li>
                            	<li><a href="javascript:;" class="car" data-value="4">租车</a></li>
                            	<li><a href="javascript:;" class="steamer" data-value="5">轮船</a></li>
                            	<li><a href="javascript:;" class="other" data-value="6">其他</a></li>
                           </ul>
                        </div>
                    </div>
                </div>
			</div>

        	<div class="item">
            	<div class="text fontSong">班　　次:</div>
                <div class="cnt">
                	<input type="text" name="traffic_number"  id="traffic_number" placeholder="AK207" class="ui2_input" value="<?=isset($traffic) ? $traffic['traffic_number'] : '';?>" />
                </div>
			</div>

        	<div class="item">
            	<div class="text fontSong">出发时间:</div>
                <div class="cnt">
                	<input type="text" name="start_hours"  id="start_hours" placeholder="时" style="width:30px;" class="ui2_input" value="<?=isset($traffic) ? $traffic['start_hours'] : '';?>" />
					<input type="text" name="start_minutes"  id="start_minutes" placeholder="分" style="width:30px;" class="ui2_input" value="<?=isset($traffic) ? $traffic['start_minutes'] : '';?>" />
					如：08:25
                </div>
            </div>
        	<div class="item">
            	<div class="text fontSong">到达时间:</div>
                <div class="cnt">
                	<input type="text" name="end_hours"  id="end_hours" placeholder="时" style="width:30px;" class="ui2_input" value="<?=isset($traffic) ? $traffic['end_hours'] : '';?>" />
					<input type="text" name="end_minutes"  id="end_minutes" placeholder="分" style="width:30px;" class="ui2_input" value="<?=isset($traffic) ? $traffic['end_minutes'] : '';?>" />
                    <input type="hidden" name="days" value="0" />
                	<div class="tra_select" id="_jstrafficdays">
                    	<strong class="titles"><span>当日</span></strong>
                        <div class="contents">
                        	<ul>
                            	<li><a href="javascript:;" data-value='0'>当日</a></li>
                            	<li><a href="javascript:;" data-value='1'>次日</a></li>
                            	<li><a href="javascript:;" data-value='2'>+2</a></li>
                            	<li><a href="javascript:;" data-value='3'>+3</a></li>
                            	<li><a href="javascript:;" data-value='4'>+4</a></li>
                            	<li><a href="javascript:;" data-value='5'>+5</a></li>
                            	<li><a href="javascript:;" data-value='6'>+6</a></li>
                            	<li><a href="javascript:;" data-value='7'>+7</a></li>
                            	<li><a href="javascript:;" data-value='8'>+8</a></li>
                            	<li><a href="javascript:;" data-value='9'>+9</a></li>
                            	<li><a href="javascript:;" data-value='10'>+10</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        
        <div class="textarea">
			<textarea name="note" id="trafficnote" placeholder="在这里添加备注" class="ui2_textarea" rel-tip="#_jstrafficnotetip"><?=isset($traffic) ? $traffic['note'] : '';?></textarea>

		</div>
        <div class="button clearfix">
            <input type="button" class="ui_buttonB fr ml10" id="_jssubmittraffic" value="保 存" />
            <input type="button" class="ui_button_cancel fr _jscloseform" value="取 消" />
        </div>
    </div>
</form>
    

