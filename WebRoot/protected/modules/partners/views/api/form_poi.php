

    <form onsubmit="return false" id="poiform" autocomplete="off">
        <input type="hidden" name="doaction" value="<?php if(empty($poi)):?>add<?php else:?>edit<?php endif;?>" />
        <input type="hidden" name="poi_id" value="<?=isset($poi) ? $poi['poi_id']:'';?>">
        <input type="hidden" name="type" value="<?=isset($poi) ? $poi['type']:'';?>">
        <input type="hidden" name="cover_image" value="<?=isset($poi) ? $poi['cover_image']:'';?>">
        <input type="hidden" name="desc" value="<?=isset($poi) ? $poi['desc']:'';?>">
        <input type="hidden" name="poi_name" value="<?=isset($poi) ? $poi['poi_name']:'';?>">
        <div class="pl_poi_box">
            <div class="title clearfix fontYaHei">
                <img id="_jspoicoverimg" src="<?=isset($poi) ? $poi['cover_image']:'';?>" width="80" height="80" alt="" onerror="this.src='/images/plan/poi_80_80.png'"/>
                <p id="_jstabpoititle" class="btit" data-title="<?=isset($poi) ? $poi['poi_name']:'';?>" >
                    <strong><?=isset($poi) ? $poi['poi_name']:'';?></strong><span><?=isset($poi) ? $poi['poi_name_english']:'';?></span>
                </p>
                <input type="text" name="title" autocomplete="off" id="poititleinput" class="ui2_input" value="<?=isset($poi) ? $poi['poi_name']:'';?>" placeholder="" <?php if(!empty($poi['poi_name'])):?> style="display:none;" <?php endif;?> />
                <div class="clearfix title_tpap" id="_jstitlesearchlist" style="display:none;"></div>
            </div>

            <div class="list clearfix">
                <div class="item" id="_jscatetypecontainer" style="display:none">
                    <div class="text fontSong">类　　型:</div>
                    <div class="cnt">
                        <input type="hidden" name="catetypeid" value="32" />
                        <div class="tra_select">
                            <strong class="titles"><span>景点</span></strong>
                            <div class="contents">
                                <ul>
                                    <li><a href="javascript:;" data-value="32">景点</a></li>
                                    <li><a href="javascript:;" data-value="148">活动</a></li>
                                    <li><a href="javascript:;" data-value="147">购物</a></li>
                                    <li><a href="javascript:;" data-value="78">餐饮</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="clearfix"></div>
            </div>
            <div class="textarea">
                <textarea name="note" id="poinote" placeholder="在这里添加备注" class="ui2_textarea" rel-tip="#_jspoinotetips"><?=isset($poi) ? $poi['note']:'';?></textarea>

            </div>
            <div class="button clearfix">
                <input type="button" id="_jssubmitonedaypoi" class=" fr ml10  ui_buttonB" value="保 存" />
                <input type="button" id="_jscancelonedaypoi" class="ui_button_cancel fr" value="取 消" />
            </div>
        </div>
    </form>
