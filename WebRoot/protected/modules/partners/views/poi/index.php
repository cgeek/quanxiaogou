

<div class="tabbable-line">
    <ul class="nav nav-tabs ">
        <li class="active">
            <a href="javascript:void(0);" > 我的地点 </a>
        </li>

        <a class="pull-right btn btn-sm btn-success hidden-xs" type="demo" href="/poi/new">添加POI</a>
    </ul>

    <div class="tab-content">
        <div class="portlet-body" >
            <div class="mt-element-card mt-element-overlay">
                <div class="row">
                    <?php foreach($poi_list as $poi):?>
                    <div class="item col-lg-3 col-md-4 col-sm-6 col-xs-12">
                        <div class="mt-card-item" style="background: #fff;">
                            <div class="mt-card-avatar mt-overlay-1" style="height:175px;background:#ccc;">
                                <img src="<?=$poi['cover_image_url'];?>">
                                <div class="mt-overlay">
                                    <ul class="mt-info">
                                        <li>
                                            <a class="btn default btn-outline" href="/poi/edit?id=<?=$poi['poi_id'];?>">
                                                <i class="icon-pencil"></i>
                                            </a>
                                        </li>
                                        <li>
                                            <a class="btn default btn-outline" href="javascript:;">
                                                <i class="icon-close" poi_id="<?=$poi['poi_id'];?>"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="mt-card-content">
                                <h3 class="mt-card-name"><?=$poi['poi_name'];?></h3>
                                <p class="mt-card-desc font-grey-mint" style="text-align:left; padding:0 10px; height:40px; overflow: hidden; text-overflow: ellipsis;"><?=$poi['desc'];?></p>

                            </div>
                        </div>
                    </div>
                    <?php endforeach;?>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('body').delegate('.icon-close', 'click', function(e){
            if(confirm('确认要删除？')) {
                var poi_id = $(this).attr('poi_id');

                var line = $(this).parents('.item');
                $.ajax({
                    url : '/poi/deletePoi',
                    type: 'POST',
                    data: {'poi_id': poi_id, 'status' : '-1'},
                    dataType:'JSON',
                    success:function(result) {
                        if(result.code == 200) {
                            line.remove();
                        } else {
                            alert(result.message);
                        }
                    },
                    error: function(){
                        alert('服务器错误');
                    }
                });
            }
        });
    })
</script>