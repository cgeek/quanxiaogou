<!doctype html>
<html>
<head>
	<meta charset="utf-8">
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
	<title><?=$trip['title'];?></title>
    <style>
        @charset "utf-8";

        /* CSS reset */
        html {-webkit-text-size-adjust:none;}
        body,div,dl,dt,dd,ul,ol,li,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,textarea,p,blockquote,th,td,hr,button,article,aside,details,figcaption,figure,footer,header,menu,nav,section {margin:0;padding:0;outline:none;}
        article,aside,details,figcaption,figure,footer,header,menu,nav,section {display:block;}
        audio,canvas,video {display:inline-block;*display:inline;*zoom:1;}
        body,button,input,select,textarea {font:13px/18px optima,helvetica, arial,sans-serif;}
        input,button,select,textarea {font-size:100%;color:#777;outline:none;-webkit-appearance:none;}
        textarea {resize:none;}
        table {border-collapse:collapse;border-spacing:0;}
        fieldset,img {border:0;}
        iframe {display:block;}
        abbr,acronym {border:0;font-variant:normal;}
        del {text-decoration:line-through;}
        address,caption,cite,code,dfn,em,strong,th,var {font-style:normal;font-weight:500;}
        ol,ul {list-style:none;}
        caption,th {text-align:left;}
        h1,h2,h3,h4,h5,h6 {font-size:100%;font-weight:500;}
        q:before,q:after {content:"";}
        sub,sup {font-size:75%;line-height:0;position:relative;vertical-align:baseline;}
        sup {top:-0.5em;}
        sub {bottom:-0.25em;}
        strong {font-weight:bold;}
        a {text-decoration:none;color:#006aad;}
        a:hover {text-decoration:none;}
        a:focus {outline:none;}

        input::-webkit-input-placeholder,textarea::-webkit-input-placeholder{color:#959595 !important;word-wrap:break-word;word-break:break-all;}
        input:-moz-placeholder,textarea:-moz-placeholder{color:#959595 !important;word-wrap:break-word;word-break:break-all;}
        input:-ms-input-placeholder,textarea:-ms-input-placeholder{color:#959595 !important;word-wrap:break-word;word-break:break-all;}

        .clearfix:after {clear: both; content: "."; display: block; height: 0; visibility: hidden;}
        .clearfix {}
        body {background:#fcfcfc;}

        .container { background:#F6F5F3; padding:10px 0; max-width:640px; margin:0 auto;}
        .hd {border-bottom:1px solid #ccc; padding-bottom:10px; }
        .hd .trip_info { margin-bottom:5px; font-size:14px;margin-top:5px; }
        .hd .trip_info span.trip_day_days { padding-left:20px; background:url('../images/app/trip-day-days.png') no-repeat; background-size:15px 15px; }
        .hd .trip_info span.trip_day_money { margin-left:10px; padding-left:20px; background:url('../images/app/trip-day-money.png') no-repeat; background-size:15px 15px; }
        .hd p { line-height:20px; font-size:14px;}
        /*position*/


        .trip_title { color:#17C4BB; font-size:15px;}
        .trip_day { position:relative;padding:10px;}
        .trip_day_date_box { position: relative;  width: 75px;  height: 40px;  float: left;  display: inline-block;}
        .trip_day_hd { }
        .trip_day_hd .trip_day_key {font-family:FZLTTHB--B51-0;  color:#4a4a4a; font-size:14px; display:block; float:left;}
        .trip_day_date_box  .trip_day_date { position: absolute;  bottom: 0;  font-size: 12px;}
        .trip_day_hd .places { color:#4a4a4a; font-size:15px; float:left;}
        .trip_day_detail {}
        .trip_day_detail .trip_day_date {color:#ccc;}
        .trip_day_detail .trip_day_title { font-size:16px; line-height:30px; padding:5px 0 ; font-family:FZLTXHB--B51-0; color:#4a4a4a;  letter-spacing:0.94px;}

        .routebox { padding-top:12px; }
        .routebox .citys {  background: #ffffff;  box-shadow: 0px 2px 6px 0px rgba(0,0,0,0.20); height:50px; padding:10px 30px; text-align:center;}
        .routebox .citys .route { display:inline-block; margin:0 auto;}
        .routebox .citys .city { display:block; float:left; text-align:center; line-height: 25px; }
        .routebox .citys .city .name { color:#4a4a4a; font-size:16px;}
        .routebox .citys .city .time { font-size:16px; color:#4a4a4a; }
        .routebox .citys .icon { display:block; width:40px; padding:0 10px; float:left;}
        .routebox .citys .icon img { margin-top:8px; width:24px;}
        .routebox .infos { padding:0 10px; background:#58E2C2;  color:#fff; line-height:28px; text-align:left;}

        .trip_day_image {width:100%; height:101px; overflow:hidden; border-radius:5px; border:1px solid #96BFED; background:#F0EDE4; position:relative; cursor:pointer; margin-bottom:10px;}
        .trip_day_image .zoom_btn {position:absolute; top:35%; left:45%; border:1px solid #496F95;  background:#577EA5; color:#fff; padding:5px 10px; border-radius:5px; opacity:0.8; cursor:pointer;font-size:12px;}
        .trip_day_image img { width:100%; }
        .trip_day_desc, .trip_day_traffic_note { padding-left:30px; }
        .trip_day_desc {font-family:FZLTXHB--B51-0;  font-size:12px;  color:#4a4a4a;  letter-spacing:0.73px;  line-height:20px;  background: url(http://dev.miaomiao.cc/images/app/trip_day_note_icon.png) no-repeat;  line-height: 20px;  background-size: 15px 18px;}
        .trip_day_traffic_note  {color:#333; background: url(http://dev.miaomiao.cc/images/app/trip_day_traffic_icon.png) no-repeat;  line-height: 20px;  background-size: 21px 14px;  margin-top:10px; }

        .trip_day_pois { padding:0 15px;}
        .trip_day_pois .item { width:130px; display: inline-block; vertical-align: middle; margin-bottom:20px; margin-right:10px; }
        .trip_day_pois .poi_cover_image_li { width:100%; height:160px; border-radius:5px; overflow:hidden; }
        .trip_day_pois .poi_cover_image_li img { width:100%; border-radius:5px 5px 0 0; }
        .item {position: relative;}
        .item img { vertical-align:middle; display:inline-block;}
        .item .poi_info { position:absolute; bottom: 0px; height:30px; line-height:30px; color:#fff; font-size:14px; white-space:nowrap; text-overflow:ellipsis;overflow:hidden; text-align: center; width:100%;}
        .item .poi_info img { margin-left:10px;}
        .item .poi_info i.arrow { background:url('../images/app/arrow.png'); background-size:8px 13px; width:8px; height:13px; position:absolute; right:10px; top:15px;}

        .pagemulu { position:fixed; z-index:99; left:10px; bottom:20px;}
        .pagemulu .icon { display:block; width:44px; height:50px; background:url(../images/app/mulu.png) no-repeat; background-size:44px 50px; cursor:pointer;}
        .poupu_mask { position:absolute; z-index:999; left:0; top:0; width:80%; height:100%; background:rgba(0,0,0,0.8);}

        /*popup mulu*/
        .popup_mulu { position:absolute; opacity:0; z-index:999;  bottom:0px; width:80%; transition: opacity 0.5s;}
        .popup_mulu .toptit { position:relative; font-size:15px; line-height:20px; }
        .popup_mulu .toptit .title { padding-left:15px; margin-right:44px; color:#000; border-top-left-radius:4px;}
        .popup_mulu .close { position:absolute; bottom:10px; right:0; height:44px; width:44px; background:url(../images/app/close2.png) no-repeat center center; background-size:18px 18px; cursor:pointer; border-top-right-radius:4px;}
        .popup_mulu .content { width:100%; height:200px; max-height:600px; overflow:auto; border-bottom-left-radius:4px; border-bottom-right-radius:4px;}
        .popup_mulu .lists { padding:10px; background:url(../images/app/mulu_bg_gray.png) no-repeat 14px 42px; background-size:1px 100%;}
        .popup_mulu .lists li { position:relative; padding:15px 0 0px 20px;}
        .popup_mulu .lists a { display:block; color:#777;}
        .popup_mulu .lists .day {   height:42px; }
        .popup_mulu .lists .day span { font-size:15px; line-height:42px; font-weight: bold;  text-align:left; color:#17C4BB; margin-right:10px;}
        .popup_mulu .lists .day em {color:#17C4BB; font-size:15px;}
        .popup_mulu .lists .tit { font-size:15px; line-height:25px; color:#17C4BB;}
        .popup_mulu .lists .txt { line-height:16px; font-size:13px; color:#999; width:100%;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;}
        .popup_mulu .lists .title { line-height:25px; font-size:14px; color:#fff; width:100%;overflow:hidden;text-overflow:ellipsis;white-space:nowrap;opacity: 0.8;}
        .popup_mulu .lists li  .maskbg { position:absolute;  left: 1px;  width: 7px;  height: 7px;  background: #17C4BB;  top: 30px;  z-index: 9999;  border-radius: 100%;}

        .popup_mulu_poi {padding-top:10px; color:#fff; opacity: 0.8;}
        .popup_mulu_poi i{
        width: 5px;
        height: 5px;
        display: inline-block;
        position: absolute;
        left: 2px;
        margin-top: 5px;
        background: #ccc;
        border-radius: 5px;
        }
        .popup_mulu .bottom { position:relative; font-size:15px; line-height:44px; border-bottom:1px solid #ebebeb; background:#75A1CD; height:44px;}
        .popup_mulu .bottom .title { padding-left:15px; margin-right:44px; color:#000; background:#fff; border-top-left-radius:4px;}
        .popup_mulu .bottom .close { position:absolute; top:0; right:0; height:44px; width:44px; background:#ebebeb url(../images/app/popup_close.png) no-repeat center center; background-size:18px 18px; cursor:pointer; border-top-right-radius:4px;}

        /*popup poi*/
        .popup_poi { position:absolute; opacity:0; z-index:999; left:0; top:0; margin:50px 20px 20px 20px; transition: opacity 0.5s;}
        .popup_poi .btmshade { position:absolute; left:0; bottom:0; width:100%; height:11px; background:url(../images/app/halftransp.png) repeat-x; background-size:100% 11px;}
        .popup_poi .toptit { position:relative; font-size:15px; line-height:44px; border-bottom:1px solid #ebebeb;}
        .popup_poi .toptit .title { padding-left:15px; margin-right:44px; color:#444; background:#fff; border-top-left-radius:3px;}
        .popup_poi .toptit .close { position:absolute; top:0; right:0; height:44px; width:44px; background:#ebebeb url(../images/app/popup_close.png) no-repeat center center; background-size:18px 18px; cursor:pointer; border-top-right-radius:3px;}
        .popup_poi .content { width:100%; height:200px; overflow:hidden; overflow-y:auto; background:#fff; border-radius:4px;}
        .popup_poi .intros{ margin-left:17px;margin-right:17px;margin-top:0px; margin-bottom:0px;padding-top:8px; padding-bottom:8px; border-top:1px dashed #ccc; }
        .popup_poi .intros-first { border-top:none;}
        .popup_poi .intros .title { font-size:15px; margin-bottom:10px; color:#444; font-weight:700;}
        .popup_poi .lists { margin-left:17px;margin-right:17px;margin-top:0px; margin-bottom:0px;padding-top:8px; padding-bottom:8px; border-top:1px dashed #ccc; }
        .popup_poi .lists .title { font-size:15px; margin-bottom:10px; color:#444; font-weight:700;}
        .popup_poi .lists .item { padding-left:48px; margin-bottom:10px;}
        .popup_poi .lists .tit { float:left; margin-left:-48px; color:#444; font-weight:700;}

        /*popup show*/
        .popup_layshow { opacity:1;}
        .poi_pop_view .poi_cover_image { position:relative; min-height:150px; }
        .poi_pop_view .poi_cover_image img { border-radius:4px 4px 0 0; }
        .poi_pop_view .poi_cover_image .popup_poi_close {position:fixed; top:60px; right:30px; cursor:pointer; }
        .poi_pop_view .poi_cover_image .poi_name {position:absolute; top:10px; left:15px;  color:#fff; font-size:20px; text-shadow: 0 0 18px #000;}
        .poi_pop_view .poi_detail {padding:10px;}
        .poi_pop_view .poi_detail .extend_info li {border-bottom:1px solid #ccc; padding:5px 0;}
        .poi_pop_view .poi_detail .extend_info li:last-child {border-bottom:0px; }
        .poi_pop_view .poi_detail .extend_info li span {float:left; }
        .poi_pop_view .poi_tips {background:#FFF8E4; color:#17C4BB; padding:10px;}

    </style>
	<script src="/assets/libs/jquery.js" type="text/javascript"></script>
    <!--script src="http://res.wx.qq.com/open/js/jweixin-1.0.0.js"></script-->
</head>

<body>
<div class="container">
	<!--div class="hd">
		<h1 class="trip_title"><?=$trip['title'];?></h1>
		<p class="trip_info">
			<span class="trip_day_days">旅行用时4天3晚</span>
			<span class="trip_day_money">参考预算：4500/人</span>
		</p>
		<p>
			让孩子与小老虎亲密接触是一次刺激新鲜的体验。让孩子看到各种大小、姿势的大象并且手工DIY是个不错的认知及动手的项目。在夜间与动物约会，相信大人小孩都会喜欢。
		</p>
	</div-->

	<div class="trip_day_list">
<?php if(!empty($trip_day_list)):?>
	<?php foreach($trip_day_list as $key=>$trip_day):?>
		<a name="a_day_<?=$trip_day['id'];?>"></a>
		<div class="trip_day">
			<div class="trip_day_hd clearfix">
                <div class="trip_day_date_box">
                    <div class="trip_day_key">DAY <?=$key+1;?></div>
                    <div class="trip_day_date"><?=date('m.d', strtotime($trip_day['date']));?>  <?=getWeek($trip_day['date'], true);?></div>
                </div>
				<div class="places clearfix">
				<?php foreach($trip_day['city_list'] as $key=>$city):?>
				<span class="tag"><?=$city['city_name'];?> <?php if($key < count($trip_day['city_list']) - 1):;?> - <?php endif;?> </span>
				<?php endforeach;?>
				</div>
			</div>
			<div class="trip_day_detail">

				<div class="trip_day_title"><?=$trip_day['title'];?></div>
			<?php foreach($trip_day['traffic_list'] as $traffic):?>
				<div class="routebox">

                    <div class="infos">
                        <p>班次：<?=$traffic['traffic_number'];?></p>
                    </div>
					<div class="citys clearfix">
						<div class="route">
						<div class="city">
							<p class="name"><?=$traffic['from_place'];?></p>
							<p class="time"><span><?=sprintf('%02d', $traffic['start_hours']);?>:<?=sprintf('%02d',$traffic['start_minutes']);?></span></p>
						</div>
						<div class="icon"><img src="/images/app/router-go.png" ></div>
						<div class="city">
							<p class="name"><?=$traffic['to_place'];?></p>
							<p class="time"><span><?=sprintf('%02d', $traffic['end_hours']);?>:<?=sprintf('%02d', $traffic['end_minutes']);?></span></p>
						</div>
						</div>
					</div>
					<?php if(false && !empty($traffic['note'])):?>
						<p class="traffic-remark">备注：<?=nl2br($traffic['note']);?></p>
					<?php endif;?>
				</div>
			<?php endforeach;?>
				<?php if(!empty($trip_day['desc'])):?>
				<p class="trip_day_desc"><?=nl2br($trip_day['desc']);?></p>
             	<?php endif;?>
				<?php if(!empty($trip_day['traffic_note'])):?>
				<p class="trip_day_traffic_note">
					<?=nl2br($trip_day['traffic_note']);?>
				</p>
             	<?php endif;?>
                <?php if(!empty($trip_day['images'])):?>
                    <?php foreach($trip_day['images'] as $image):?>
				<div class="trip_day_image">
                    <a href="<?=$image['image_url'];?>" target="_blank" onClick="ok.performClick(this.src);">
                        <img src="<?=$image['image_url'];?>">
					    <span class="zoom_btn">点击放大</span>
					</a>
                </div>
                    <?php endforeach;?>
             	<?php endif;?>
			</div>
		</div>

		<?php if(count($trip_day['poi_list_by_type']) > 0):?>
		<div class="trip_day_pois clearfix">
			<?php foreach($trip_day['poi_list_by_type'] as $poi_type=>$pois):?>
				<?php foreach($pois as $poi):?>
			<a class="" href="javascript:void(0);" onClick="popupPoiShow(<?=$poi['id'];?>);">
				<div class="item">
					<div class="poi_cover_image_li" style="background:url('<?=upimage($poi['cover_image'], 'w320h240');?>') no-repeat center;background-size:cover;">
                        <div class="poi_type_icon" style="position: absolute;right: 10px;top: 10px;width: 20px;height: 20px;">
                            <img src="/images/app/poi-icon-<?=$poi_type;?>.png" width="21" height="21">
                        </div>
                        <div class="poi_info">

                            <?=$poi['poi_info']['poi_name'];?> &nbsp;&nbsp;

                        </div>
					</div>

				</div>
			</a>
				<div class="item_pop" id="popup_poi_content_<?=$poi['id'];?>" style="display:none;">
					<div class="poi_pop_view">
						<div class="poi_cover_image">
							<img src="<?=upimage($poi['cover_image'], 'org');?>" width=100%  onClick="ok.performClick(this.src);" />
							<i class="popup_poi_close"><img src="/images/app/close.png"></i>
							<span class="poi_name"><?=$poi['poi_info']['poi_name'];?></span>
						</div>
						<div class="poi_detail"> 
								<div class="desc">
									<?=nl2br($poi['poi_info']['desc']);?>
								</div>
								<div class="extend_info" style="border-top:1px dashed #ccc; margin-top:10px;">
									<ul >
										<?php if(!empty($poi['poi_info']['address'])):?>
										<li class="clearfix">
											<span class="tit">地址：</span>
											<div class="txt"><?=$poi['poi_info']['address']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['phone'])):?>
										<li class="clearfix">
											<span class="tit">电话：</span>
											<div class="txt"><?=$poi['poi_info']['phone']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['website'])):?>
										<li class="clearfix">
											<span class="tit">网址：</span>
											<div class="txt" style="word-break:break-all;"><a href="<?=preg_match('/^http/i', $poi['poi_info']['website']) ? $poi['poi_info']['website'] : 'http://' . $poi['poi_info']['website'];?>" target="_blank"><?=$poi['poi_info']['website']?></a></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['price'])):?>
										<li class="clearfix">
											<span class="tit">价格：</span>
											<div class="txt"><?=$poi['poi_info']['price']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['recommend'])):?>
										<li class="clearfix">
											<span class="tit">推荐：</span>
											<div class="txt"><?=$poi['poi_info']['recommend']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['open_time'])):?>
										<li class="clearfix">
											<span class="tit">开放时间：</span>
											<div class="txt">
												<?=nl2br($poi['poi_info']['open_time']);?>
											</div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['traffic'])):?>
										<li class="clearfix">
											<span class="tit">交通建议：</span>
											<div class="txt">
												<?=nl2br($poi['traffic']);?>
											</div>
										</li>
										<?php endif;?>
									</ul>
								</div>
							</div>
						<?php if(!empty($poi['poi_info']['tips'])):?>
						<div class="poi_tips">
							特别提示：<br/>
							<?=nl2br($poi['poi_info']['tips']);?>
						</div>
						<?php endif;?>
						</div>
				</div>
				<?php endforeach;?>
			<?php endforeach;?>
		</div>	
		<?php endif;?>
	<?php endforeach;?>
<?php endif;?>
	</div>
</div>


	<p class="pagemulu" id="pagemulu"><span class="icon" title="目录"></span></p>

	<div class="poupu_mask" id="poupu_mask" style="display:none;"></div>
	<div class="popup_mulu" id="popup_mulu" style="display:none;">
	<div class="toptit">
		<h2 class="title">&nbsp;&nbsp;</h2>

	</div>
	<div class="content" id="popup_mulu_content">
        <p class="close" id="popup_mulu_close"></p>
		<ul class="lists">
		<?php if(!empty($trip_day_list)):?>
			<?php foreach($trip_day_list as $key=>$trip_day):?>
			<li class="<?php if($key == 0):?>firstitem<?php elseif($key == count($trip_day_list) - 1):?>lastitem<?php endif;?>" onclick="popupHide()">
				<a href="#a_day_<?=$trip_day['id'];?>">
					<p class="maskbg"></p>
					<p class="day"><span>DAY <?=$key+1;?></span>
                        <em><?php foreach($trip_day['city_list'] as $key=>$city):?>
                            <?=$key > 0 ? '-' :'';?> <?=$city['city_name'];?>
                        <?php endforeach;?></em>
                    </p>
					<p class="title"><?=$trip_day['title'];?></p>


                    <?php foreach($trip_day['poi_list_by_type'] as $poi_type=>$pois):?>
                    <?php foreach($pois as $poi):?>
                            <p class="popup_mulu_poi"><i></i><?=$poi['poi_name'];?></p>
                    <?php endforeach;?>
                    <?php endforeach;?>
				</a>
			</li>
			<?php endforeach;?>
		<?php endif;?>
		</ul>
	</div>
</div>


<div class="popup_poi" id="popup_poi" style="display:none;">
	<!--p class="btmshade"></p-->
	<div class="content" id="popup_poi_content">
			
	</div>
</div>

<script>
var win_w = $(window).width();
var win_h = $(window).height();
var poupu_mask = $("#poupu_mask");
var popup_poi = $("#popup_poi");
var popup_mulu = $("#popup_mulu");
var scrolltop;

//公用隐藏函数
function popupHide(){
	$("html,body").css({"height":"","overflow":""});
	poupu_mask.hide();
	popup_poi.removeClass("popup_layshow").css({"display":"none"});
	popup_mulu.removeClass("popup_layshow").css({"display":"none"});
	$(window).scrollTop(scrolltop);
}

//公用显示函数
function popupShow(){
	scrolltop = $(window).scrollTop();
	$("html,body").css({"height":win_h,"overflow":"hidden"});
	poupu_mask.show();
}

//POI显示函数
function popupPoiShow(poi_id){
	$("#js_popup_poi_title").html($("#js_popup_poi_title_" + poi_id).html());
	$("#popup_poi_content").html($("#popup_poi_content_" + poi_id).html());
	popupShow();
	popup_poi.show().addClass("popup_layshow");;
	var left = popup_poi.offset().left;
	var height = win_h - left * 2 - 45;

	$("#popup_poi_content").height(height).scrollTop(0);
}

//Hotel显示函数
function popupHotelShow(hotel_id){
	$("#js_popup_poi_title").html($("#js_popup_hotel_title_" + hotel_id).html());
	$("#popup_poi_content").html($("#popup_hotel_content_" + hotel_id).html());
	popupShow();
	popup_poi.show().addClass("popup_layshow");;
	var left = popup_poi.offset().left;
	var height = win_h - left * 2 - 45;
	$("#popup_poi_content").height(height).scrollTop(0);
}

$("body").delegate('.popup_poi_close', 'click', function() {
	popupHide();
});

//mulu显示函数
function popupMuluShow(){
	popupShow();
	popup_mulu.show().addClass("popup_layshow");
	var left = popup_mulu.offset().left;
	var height = win_h;
	$("#popup_mulu_content").height(height).scrollTop(0);
}
$("#pagemulu").click(function(){
	popupMuluShow();
});


$("#popup_mulu_close").click(function(){
	popupHide();
});

$("#poupu_mask").click(function(){
	popupHide();
});



//加载更多...
$(function(){
	/*
	$(".planday .intro").each(function(){
		var height = $(this).height();
		if(height > 50){
			$(this).parents(".planday").find(".btn_updown").show();
		}
		//console.log(height);
	});
	$(".planday .btn_updown").toggle(function(){
		$(this).parents(".planday").find(".intros").css("max-height","none");
		$(this).find("span").addClass("fold").text("点击收起");
	},function(){
		$(this).parents(".planday").find(".intros").css("max-height","54px");
		$(this).find("span").removeClass("fold").text("点击查看更多");
	});
	*/

    <?php if(isset($sign)):?>
    wx.config({
        debug: false,
        appId: '<?=$sign['appId'];?>',
        timestamp: '<?=$sign["timestamp"];?>',
        nonceStr: '<?=$sign["nonceStr"];?>',
        signature: '<?=$sign["signature"];?>',
        jsApiList: ['checkJsApi', 'onMenuShareTimeline',
            'onMenuShareAppMessage', 'onMenuShareQQ',
            'onMenuShareWeibo', 'hideMenuItems', 'showMenuItems',
            'hideAllNonBaseMenuItem', 'showAllNonBaseMenuItem',
            'translateVoice', 'startRecord', 'stopRecord',
            'onRecordEnd', 'playVoice', 'pauseVoice', 'stopVoice',
            'uploadVoice', 'downloadVoice', 'chooseImage',
            'previewImage', 'uploadImage', 'downloadImage',
            'getNetworkType', 'openLocation', 'getLocation',
            'hideOptionMenu', 'showOptionMenu', 'closeWindow',
            'scanQRCode', 'chooseWXPay', 'openProductSpecificView',
            'addCard', 'chooseCard', 'openCard']
    });

    var data = { trip: {share_title: '<?=$trip['title'];?>', share_url:'http://www.miaomiao.cc/export/app?hash_id=<?=$trip['hash_id'];?>', share_icon:'', share_content:'开始你的奇妙之旅' }}
    wx.ready(function () {
        wx.onMenuShareTimeline({
            title: data['trip']['share_title'],
            link: data['trip']['share_url'],
            imgUrl: data['trip']['share_icon'],
            success: function () {

            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
            }
        });
        wx.onMenuShareAppMessage({
            title: data['trip']['share_title'],
            link: data['trip']['share_url'],
            imgUrl: data['trip']['share_icon'],
            desc: data['trip']['share_content'],
            success: function () {

            },
            cancel: function () {
                // 用户取消分享后执行的回调函数
            }
        });

    });

    <?php endif;?>
});
</script>
</body>
</html>

