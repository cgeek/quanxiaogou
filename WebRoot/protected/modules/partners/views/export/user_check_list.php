<!doctype html>
<html>
<head>
	<title><?=isset($trip)? $trip['title'] : '妙妙行程管家';?></title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
	
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->
	<?php if(isset($_GET['print']) && $_GET['print'] == 1):?>
	<style>
		@font-face {
			font-family: 'XiHei';
			src: url('/fonts/xihei.ttf') format('truetype');
		}
		@font-face {
			font-family: 'MyOptima';
			src: url('/fonts/optima.ttf') format('truetype');
		}
		body, div, p, td, tr, th {
			font-family: 'XiHei', 'MyOptima';
		}
	</style>
	<?php else:?>
	<style>
		body, div, p, td, tr, th {
			font-family: 'Optima';
		}
	</style>
	<?php endif;?>
	<style>
		div.container-col {
			float:left;
		}
		table.check-list{
	 		border-color:chocolate;
			margin-top:40px;
			width:250px;
			margin-left:10px;
			margin-right:10px;
		}
		table.check-list>thead>tr>th, table.check-list>thead>tr>td {
			border-bottom-width:1px;
			border-color:darkgoldenrod;
		}
		table.check-list>thead>tr>th.cat{
			background-color:coral;
			border-color:chocolate;
		}

		table.check-list>thead>tr.check-head{
			background-color:bisque;
		}
		table.check-list>tbody>tr>th, table.check-list>tbody>tr>td {
			border-color:darkgrey;
		}

		table.sea-blue{
			border-color:royalblue;
		}
		table.sea-blue>thead>tr>th.cat{
			background-color:cornflowerblue;
			border-color:royalblue;
		}
		table.sea-blue>thead>tr.check-head{
			background-color:lightblue;
		}
		table.sea-blue>thead>tr>th, table.check-list>thead>tr>td {
			border-color:steelblue;
		}
	</style>

</head>
<body>

<div class="container" style="width:845px;">
	<?php if(!isset($_GET['print'])):?>
	<div style="text-align:right; width:100%;">
		<a target="_blank" href="/plan/export/makepdf?url[]=<?=urlencode('http://'.$_SERVER['HTTP_HOST'].'/plan/export/userchecklist/?print=1&trip_id='.$trip['id']);?>">Save as PDF</a>
	</div>
	<?php endif;?>
	<div style="text-align:center; padding:15px;">
		<span style="font-size:32px;">Check Check &trade;</span>
		<span style="font-size:12px;padding-left:20px;">by 妙妙行程管家</span>
	</div>
	<div class="container-col">
	<?php $total_height=0;?>
	<?php foreach($ck_items as $cat=>$ck_item):?>
	<?php 
	$el = 0;
	if(isset($ck_item['empty_line']))
	{
		$el = $ck_item['empty_line'];
		unset($ck_item['empty_line']);
	}
	$ct = '';
	if(isset($ck_item['color_theme']))
	{
		$cl = $ck_item['color_theme'];
		unset($ck_item['color_theme']);
	}
	$this_height=196;

	foreach($ck_item as $item)
	{
		$this_height+=38;
		if(!empty($item['desc']))
		{
			$this_height+=20;
		}
	}
	?>
	<?php if($total_height+$this_height > 1100):?>
	</div>
	<div class="container-col">
	<?php endif;?>
	<?php if($total_height+$this_height > 1100){$total_height=$this_height;}else{$total_height+=$this_height;}?>
	<?php //echo $this_height; echo "&nbsp;"; echo $total_height;?>
	<table class="table table-bordered table-striped check-list <?=$cl;?>">
		<thead>
			<tr>
				<th colspan=2 class="cat" style="text-align:center; font-size:16px;"><?=$cat;?></th>
			</tr>
			<tr class="check-head">
				<th style="width:500px;font-size:15px; text-align:center;">准备事项</th>
				<th style="text-align:center;font-size:15px; ">Check</th>
			</tr>
		</thead>
		<tbody>
			<?php foreach($ck_item as $item):?>
			<tr>
				<td><?php if($item['city_name'] != 'all'){echo '[' . $item['city_name']. ']';}?><?=$item['item'];?>

					<?php if(!empty($item['desc'])):?>
					<br/><span style="font-size:11px;font-color:#999;"><?=$item['desc'];?></span>
					<?php endif;?>
				</td>
				<td>&nbsp;</td>
			</tr>
			<?php endforeach;?>
			<?php for($i=1; $i<=$el; $i++):?>
			<tr>
				<td>&nbsp;</td>
				<td>&nbsp;</td>
			</tr>
			<?php endfor;?>

		</tbody>
	</table>
	<?php endforeach;?>
	</div>
</div>


</body>
</html>
