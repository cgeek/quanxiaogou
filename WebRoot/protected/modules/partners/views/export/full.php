<!doctype html>
<html>
<head>
<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
<title></title>
<link href="/css/app.css" rel="stylesheet" type="text/css" />
<script src="http://maps.googleapis.com/maps/api/js?v=3.exp&sensor=false"></script>
<script src="/assets/libs/jquery.js" type="text/javascript"></script>
<script src="/assets/libs/gmap3.js" type="text/javascript"></script>
<script>
</script>
<style>
	div.map-overlay {
		text-align:center;
		color:#FFFFFF; 
		padding:2px 5px; 
		font-family:Optima; 
		background-color:rgba(0, 0, 0, .5);
		font-size:11px;
	}
	</style>
</head>

<body>
	<!--planday start-->
	<h1 style="width:100%; text-align:center; font-size:18px; padding-top:20px; padding-bottom:10px;"><?=$trip['title'];?></h1>
<?php if(!empty($trip_day_list)):?>
	<?php foreach($trip_day_list as $key=>$trip_day):?>
	<a name="a_day_<?=$trip_day['id'];?>"></a>
	<div class="planday">
		<div class="titles">
			<h2 class="title" id="a_first"><em>第<?=$key+1;?>天</em>
				（<?=$trip_day['date'];?>  <?=getWeek($trip_day['date']);?>）
			</h2>
		</div>
		<div class="content">
			<div class="places clearfix">
				<!--<p class="icon"><img src="/images/app/icon_place.png" width="21" height="21" alt=""></p>-->
				<?php foreach($trip_day['city_list'] as $city):?>
					<span class="tag"><?=$city['city_name'];?></span>
				<?php endforeach;?>
			</div>
			<?php foreach($trip_day['traffic_list'] as $traffic):?>
			<div class="routebox clearfix">
				<div class="citys">
					<div class="city">
						<p class="name"><?=$traffic['from_place'];?></p>
						<p class="time"><span><?=sprintf('%02d', $traffic['start_hours']);?>:<?=sprintf('%02d',$traffic['start_minutes']);?></span></p>

					</div>
					<div class="icon"><img src="/images/app/route_icon_plane.png" width="35" height="35" alt=""></div>
					<div class="city">
						<p class="name"><?=$traffic['to_place'];?></p>
						<p class="time"><span><?=sprintf('%02d', $traffic['end_hours']);?>:<?=sprintf('%02d', $traffic['end_minutes']);?></span></p>
					</div>
				</div>
				<div class="infos">
					<p>班次：<?=$traffic['traffic_number'];?></p>
					<!--<p>费用：CNY 0</p>-->
				</div>
				<?php if(!empty($traffic['note'])):?>
				<p class="traffic-remark">备注：<?=nl2br($traffic['note']);?></p>
				<?php endif;?>
			</div>
			<?php endforeach;?>
			
			
			
			<div class="intros intros-first">
			<?php if(!empty($trip_day['title'])):?>
			<h3 class="day-title"><?=$trip_day['title'];?></h3>
			<?php endif;?>
				<p class="intro">
					<?=nl2br($trip_day['desc']);?>
				</p>
			</div>
			<?php if(!empty($trip_day['traffic_note'])):?>
			<div class="intros">
				<h4><strong>交通建议：</strong></h4>
				<p class="intro">
					<?=nl2br($trip_day['traffic_note']);?>
				</p>
			</div>
			<?php endif;?>
			
		
			<?php if(!empty($trip_day['hotel_list'])):?>
			<div class="intros">
				<h4><strong>入住酒店：</strong></h4>
				<ul class="poilists">
					<?php foreach($trip_day['hotel_list'] as $hotel):?>
					<li>
					<!--
						<p class="firstmask"></p>
						<p class="icon"><img src="/images/app/icon_poi_hotel.png" width="21" height="21" alt="交通" onerror="this.src='/images/app/poi_48_48.png';"></p>
						-->
						<div id="js_popup_hotel_<?=$hotel['id'];?>" style="display:none;">
							<p class="btmshade"></p>
							<div class="toptit">
								<h2 class="title" id="js_popup_hotel_title_<?=$hotel['id'];?>"><?=$hotel['title'];?></h2>
								<p class="close"></p>
							</div>
							<div class="content" id="popup_hotel_content_<?=$hotel['id'];?>">
								<div class="intros intros-first">
									<h3 class="title">简介</h3>
									<p class="text"><?=nl2br($hotel['desc']);?></p>
								</div>
								<div class="lists">
									<!--h3 class="title">基本信息</h3-->
									<ul class="list">
										<?php if(!empty($hotel['hotel_name_english'])):?>
										<li class="item clearfix">
											<p class="tit">英文：</p>
											<div class="txt"><?=$hotel['hotel_name_english']?></div>
										</li>
										<?php endif;?>
										<li class="item clearfix">
											<p class="tit">地址：</p>
											<div class="txt"><?=$hotel['address']?></div>
										</li>
										<?php if(!empty($hotel['phone'])):?>
										<li class="item clearfix">
											<p class="tit">电话：</p>
											<div class="txt"><?=$hotel['phone']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($hotel['website'])):?>
										<li class="item clearfix">
											<p class="tit">网址：</p>
											<div class="txt" style="word-break:break-all;"><a href="<?=preg_match('/^http/i', $hotel['website']) ? $hotel['website'] : 'http://' . $hotel['website'];?>" target="_blank"><?=$hotel['website']?></a></div>
										</li>
										<?php endif;?>
									</ul>
								</div>
								<?php if(!empty($hotel['traffic'])):?>
								<div class="intros">
									<h3 class="title">交通建议</h3>
									<p class="text"><?=nl2br($hotel['traffic']);?></p>
								</div>
								<?php endif;?>
							</div>
						</div>
						<a href="javascript:void(0);" onClick="popupHotelShow(<?=$hotel['id'];?>);">
							<p class="imgs">
								<img src="<?=$hotel['cover_image'];?>" alt="" onerror="this.src='/images/app/poi_48_48.png';"/>
							</p>
							<div class="cnt">
								<h3 class="title"><?=$hotel['title'];?></h3>
								<div class="txts clearfix">
									<p class="txt"><?=cut_str($hotel['desc'], 18);?></p>
								</div>
							</div>
							<?php if(!empty($hotel['note'])):?>
							<div class='poi-remark'>
							备注：<?=nl2br($hotel['note']);?>
							</div>
							<?php endif;?>
						</a>
					</li>
					<?php endforeach;?>
				</ul>
			</div>
			<?php endif;?>


			<?php if(!empty($trip_day['poi_list_by_type'])):?>
			<?php foreach($trip_day['poi_list_by_type'] as $poi_type=>$pois):?>
			<?php if(!empty($pois)):?>
			<div class="intros">
			<h4><strong><?=$poi_type_cn[$poi_type];?>：</strong></h4>
				<ul class="poilists">
					<?php foreach($pois as $poi):?>
					<li>
					<!--
						<p class="firstmask"></p>
						<p class="icon"><img src="/images/app/icon_poi_<?=$poi['type'];?>.png" width="21" height="21" alt="交通" onerror="this.src='/images/app/poi_48_48.png';"></p>
						-->
						<div id="js_popup_poi_<?=$poi['id'];?>" style="display:none;">
							<p class="btmshade"></p>
							<div class="toptit">
								<h2 class="title" id="js_popup_poi_title_<?=$poi['id'];?>"><?=$poi['poi_name'];?></h2>
								<p class="close"></p>
							</div>
							<div class="content" id="popup_poi_content_<?=$poi['id'];?>">
								<div class="intros intros-first">
									<h3 class="title">简介</h3>
									<p class="text"><?=nl2br($poi['desc']);?></p>
									<?php if(!empty($poi['is_private'])):?>
									<div class='' style="border:1px dashed coral; margin-top:5px; margin-bottom:5px; height:44px;">
										<div style="float:left; color:coral; padding:4px; background-color:bisque; text-align:center; border-right:1px dashed coral; margin-right:4px; height:36px;">
										私藏<br/>
										sī cáng<br/>
										</div>
										<div style="color:#999; padding:4px; ">
										私人珍藏地点，一般人我不告诉他，知道的人多了就不好了。
										</div>
									</div>
									<?php endif;?>
								</div>
								<div class="lists">
									<!--h3 class="title">基本信息</h3-->
									<ul class="list">
										<li class="item clearfix">
											<p class="tit">地址：</p>
											<div class="txt"><?=$poi['poi_info']['address']?></div>
										</li>
										<?php if(!empty($poi['poi_info']['phone'])):?>
										<li class="item clearfix">
											<p class="tit">电话：</p>
											<div class="txt"><?=$poi['poi_info']['phone']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['website'])):?>
										<li class="item clearfix">
											<p class="tit">网址：</p>
											<div class="txt" style="word-break:break-all;"><a href="<?=preg_match('/^http/i', $poi['poi_info']['website']) ? $poi['poi_info']['website'] : 'http://' . $poi['poi_info']['website'];?>" target="_blank"><?=$poi['poi_info']['website']?></a></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['price'])):?>
										<li class="item clearfix">
											<p class="tit">价格：</p>
											<div class="txt"><?=$poi['poi_info']['price']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['recommend'])):?>
										<li class="item clearfix">
											<p class="tit">推荐：</p>
											<div class="txt"><?=$poi['poi_info']['recommend']?></div>
										</li>
										<?php endif;?>
										<?php if(!empty($poi['poi_info']['open_time'])):?>
										<li class="item clearfix">
											<p class="tit">开放时间：</p>
											<div class="txt">
												<?=nl2br($poi['poi_info']['open_time']);?>
											</div>
										</li>
										<?php endif;?>
									</ul>
								</div>
								<?php if(!empty($poi['traffic'])):?>
								<div class="intros">
									<h3 class="title">交通建议</h3>
									<p class="text"><?=nl2br($poi['traffic']);?></p>
								</div>
								<?php endif;?>
								<?php if(!empty($poi['type']) && 'restaurant' == $poi['type'] &&(empty($poi['is_private']))):?>
								 <div class='poi-remark-bottom'>
									 这里介绍的餐厅大部分可以在我们做的攻略App系列中找到详细的介绍、图片、GPS经纬度等，出发前一定要记得下载哦 :)
								</div>
								<?php endif;?>
							</div>
						</div>
						<a href="javascript:void(0);" onClick="popupPoiShow(<?=$poi['id'];?>);">
							<p class="imgs">
								<img src="<?=$poi['cover_image'];?>" alt="" onerror="this.src='/images/app/poi_48_48.png';"/>
							</p>
							<div class="cnt">
								<h3 class="title"><?=$poi['poi_name'];?></h3>
							<?php if(!empty($poi['is_private'])):?>
							<div class='' style="color:coral; float:right; margin-top:-18px;">
								私藏
							</div>
							<?php endif;?>
								<div class="txts clearfix">
									<p class="txt"><?=cut_str($poi['desc'], 18);?></p>
								</div>
							</div>
							<?php if(!empty($poi['note'])):?>
							<div class='poi-remark'>
							备注：<?=nl2br($poi['note']);?>
							</div>
							<?php endif;?>
						</a>
					</li>
					<?php endforeach;?>
				</ul>
			</div>
			<?php endif;?>
			<?php endforeach;?>
			<?php endif;?>
			
		<div class="btn_updown" style="display:none;"><span>点击查看更多</span></div>
	</div>
	
	<?php endforeach;?>
<?php endif;?>


	<!--planday end-->
	<!--a name="a_note"></a>
	<div class="planbox">
		<h2 class="title"><em>Tips：</em></h2>
		<div class="texts">各种tips放这里</div>
	</div-->
	
	<p class="pagemulu" id="pagemulu"><span class="icon" title="目录"></span></p>

	<div class="poupu_mask" id="poupu_mask" style="display:none;"></div>
	<div class="popup_mulu" id="popup_mulu" style="display:none;">
	<div class="toptit">
		<h2 class="title">行程目录</h2>
		<p class="close" id="popup_mulu_close"></p>
	</div>
	<div class="content" id="popup_mulu_content">
		<ul class="lists">
		<?php if(!empty($trip_day_list)):?>
			<?php foreach($trip_day_list as $key=>$trip_day):?>
			<li class="firstitem" onclick="popupHide()">
				<a href="#a_day_<?=$trip_day['id'];?>">
					<p class="maskbg"></p>
					<p class="day"><span>D<?=$key+1;?></span></p>
					<h3 class="tit">
						
						<?php foreach($trip_day['city_list'] as $key=>$city):?>
							<?=$key > 0 ? '-' :'';?><span class="tag"><?=$city['city_name'];?></span>
						<?php endforeach;?>
					</h3>
					<p class="txt">（11月19日  星期二）</p>
				</a>
			</li>
			<?php endforeach;?>
		<?php endif;?>
			<li class="lastitem" onclick="popupHide()">
				<a href="#a_note">
					<p class="maskbg"></p>
					<p class="day"></p>
					<h3 class="tit">Tips</h3>
					<p class="txt"></p>
				</a>
			</li>
		</ul>
	</div>
</div>


<div class="popup_poi" id="popup_poi" style="display:none;">
	<p class="btmshade"></p>
	<div class="toptit">
		<h2 class="title" id="js_popup_poi_title">卢浮宫</h2>
		<p class="close" id="popup_poi_close"></p>
	</div>
	<div class="content" id="popup_poi_content">
		
	</div>
</div>
<p style="height:50px;"></p>

<script>
var win_w = $(window).width();
var win_h = $(window).height();
var poupu_mask = $("#poupu_mask");
var popup_poi = $("#popup_poi");
var popup_mulu = $("#popup_mulu");
var scrolltop;

//公用隐藏函数
function popupHide(){
	$("html,body").css({"height":"","overflow":""});
	poupu_mask.hide();
	popup_poi.removeClass("popup_layshow").css({"display":"none"});
	popup_mulu.removeClass("popup_layshow").css({"display":"none"});
	$(window).scrollTop(scrolltop);
}

//公用显示函数
function popupShow(){
	scrolltop = $(window).scrollTop();
	$("html,body").css({"height":win_h,"overflow":"hidden"});
	poupu_mask.show();
}

//POI显示函数
function popupPoiShow(poi_id){
	$("#js_popup_poi_title").html($("#js_popup_poi_title_" + poi_id).html());
	$("#popup_poi_content").html($("#popup_poi_content_" + poi_id).html());
	popupShow();
	popup_poi.show().addClass("popup_layshow");;
	var left = popup_poi.offset().left;
	var height = win_h - left * 2 - 45;
	$("#popup_poi_content").height(height).scrollTop(0);
}
//Hotel显示函数
function popupHotelShow(hotel_id){
	$("#js_popup_poi_title").html($("#js_popup_hotel_title_" + hotel_id).html());
	$("#popup_poi_content").html($("#popup_hotel_content_" + hotel_id).html());
	popupShow();
	popup_poi.show().addClass("popup_layshow");;
	var left = popup_poi.offset().left;
	var height = win_h - left * 2 - 45;
	$("#popup_poi_content").height(height).scrollTop(0);
}

$("#popup_poi_close").click(function(){
	popupHide();
});

//mulu显示函数
function popupMuluShow(){
	popupShow();
	popup_mulu.show().addClass("popup_layshow");
	var left = popup_mulu.offset().left;
	var height = win_h - left - 45 - 56;
	$("#popup_mulu_content").height(height).scrollTop(0);
}
$("#pagemulu").click(function(){
	popupMuluShow();
});


$("#popup_mulu_close").click(function(){
	popupHide();
});

$("#poupu_mask").click(function(){
	popupHide();
});



//加载更多...
$(function(){
	/*
	$(".planday .intro").each(function(){
		var height = $(this).height();
		if(height > 50){
			$(this).parents(".planday").find(".btn_updown").show();
		}
		//console.log(height);
	});
	$(".planday .btn_updown").toggle(function(){
		$(this).parents(".planday").find(".intros").css("max-height","none");
		$(this).find("span").addClass("fold").text("点击收起");
	},function(){
		$(this).parents(".planday").find(".intros").css("max-height","54px");
		$(this).find("span").removeClass("fold").text("点击查看更多");
	});
	*/
});
</script>
</body>
</html>

