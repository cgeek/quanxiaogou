
<div class="note note-info">
    <h4 class="block">如何收集客户需求？</h4>
    <p style="word-break: break-all;"> 你可以通过微信发送下面链接给客户收集客户需求：<a target="_blank" href="<?=Yii::app()->request->hostInfo;?>/customer/collect?pid=<?=Yii::app()->partner->id;?>"><?=Yii::app()->request->hostInfo;?>/customer/collect?pid=<?=Yii::app()->partner->id;?></a></p>
</div>
<div class="customerList row">

    <?php foreach($customer_list as $customer) :?>
    <div class="customerCard col-lg-3 col-sm-3">
        <div class="innerCard">
            <div class="cardHead">
                <span class="contact"><?=$customer['contact'];?></span>
                <div class="connect">
                    <a href="/customer/edit?id=<?=$customer['id'];?>">ID：<?=$customer['id'];?></a> <br />
                    <i class="fa fa-phone"></i> <?=$customer['mobile'];?><br />
                    <i class="fa fa-wechat"></i> <?=$customer['weixin'];?>
                </div>
            </div>
            <div class="cardBody">
                <div class="row">
                    <div class="col-sm-6 origin">
                        <h4>出发地</h4>
                        <?=empty($customer['origin'])? '未知' : $customer['origin'];?>
                    </div>
                    <div class="col-sm-6 destination">
                        <h4>目的地</h4>
                        <?=$customer['destination'];?>
                    </div>
                    <div class="col-sm-12 travelDate">
                        <?php if(!empty($customer['start_date'])):?>
                        <?=$customer['start_date'];?> &nbsp;&nbsp; --- &nbsp;&nbsp; <?=$customer['end_date'];?>
                        <?php endif;?>
                    </div>
                </div>
                <div class="remark">
                    <h4>行程需求：</h4>
                    <p><?=$customer['remark'];?></p>
                </div>
                <div class="actions">
                    <a class="btn green-haze btn-xs" href="/customer/edit/?id=<?=$customer['id'];?>">编辑</a>

                    <a  class="btn green-haze btn-xs" href="/trip/quickCreate/?customer_id=<?=$customer['id'];?>">创建行程</a>
                    <?php if($customer['trip_id'] > 0):?>
                        <a  class="btn green-haze btn-xs" href="/trip/?customer_id=<?=$customer['id'];?>">查看行程</a>
                    <?php endif;?>
                    <a class="btn btn-danger btn-xs delete_customer_btn" href="javascript:void(0);" customer_id="<?=$customer['id'];?>">删除</a>
                </div>

            </div>

        </div>
    </div>
    <?php endforeach;?>

    <div class="customerCard col-lg-3 col-sm-3">
        <div class="innerCard addCustomerBox">
            <a href="/customer/add" class="addCustomer">
                <span style="font-size:20px;display: block;">手动添加</span>
                +
            </a>

        </div>
    </div>
</div>




<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('partner/customer_list');
    });
</script>
