<!DOCTYPE html>
<html>
<head>
    <title>妙妙行程管家 - 你的私人旅行顾问！</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="zh" />
    <meta name="description" content="妙妙行程管家" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/base.css" />
    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/seajs/dist/sea.js"></script>

    <script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
    <style>
        .error {color:red;}
    </style>
</head>
<body>
<div class="content">
    <div class="container">
        <h3 style="text-align:center;color:#999;padding-bottom:10px;">定制服务申请</h3>
        <div class="customer-info-form col-lg-12">
            <form class="form-horizontal" role="form" id="collect-form" action="/customer/collect" method="post">
                <input name="user_id" type="hidden" class="form-control" value="<?=isset($partner) ? $partner['id'] : '';?>">
                <div class="form-group">
                    <label for="inputContact" class="col-lg-2 control-label">联系人姓名</label>
                    <div class="col-lg-2">
                        <input type="text" name="contact" class="form-control" id="inputContact" placeholder="" value="<?=isset($customer)? $customer['contact'] : '';?>">
                        <span class="help-inline"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputOrigin" class="col-lg-2 control-label">出发城市</label>
                    <div class="col-lg-2">
                        <input type="text" name="origin" class="form-control" id="inputOrigin" placeholder="" value="<?=isset($customer)? $customer['origin'] : '';?>">
                    </div>
                </div>
                <div class="form-group">
                    <label for="inputDestination" class="col-lg-2 control-label">目的地</label>
                    <div class="col-lg-3">
                        <input type="text" name="destination" class="form-control" id="inputDestination" placeholder="多个用逗号隔开，如：曼谷、普吉岛" value="<?=isset($customer) ? $customer['destination'] :'';?>">
                    </div>

                </div>

                <div class="form-group">
                    <label for="peopleNums" class="col-lg-2 control-label">出行人数</label>
                    <div class="col-lg-3">
                        <input type="text" name="num_of_people" class="form-control" id="peopleNums" placeholder="如：2大1小"  value="<?=isset($customer) ? $customer['num_of_people'] :'';?>">
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputRemark" class="col-lg-2 control-label">需求说明</label>
                    <div class="col-lg-5">
                        <textarea class="form-control" placeholder="大概预算，蜜月游、家庭游，带老人小孩？" name="remark" id="inputRemark" rows="5"><?=isset($customer) ? $customer['remark'] :'';?></textarea>
                        <span class="help-inline"></span>
                    </div>
                </div>

                <div class="form-group">
                    <label for="inputMobile" class="col-lg-2 control-label">手机号码</label>
                    <div class="col-lg-2">
                        <input type="text" name="mobile" class="form-control" id="inputMobile" placeholder="" value="<?=isset($customer) ? $customer['mobile'] :'';?>">
                    </div>
                    <span class="help-block"></span>
                </div>
                <div class="form-group">
                    <label for="inputWeixin" class="col-lg-2 control-label">微信号</label>
                    <div class="col-lg-2">
                        <input type="text" name="weixin" class="form-control" id="inputWeixin" placeholder="" value="<?=isset($customer) ? $customer['weixin'] :'';?>">
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-lg-offset-2 col-lg-10">
                        <button type="submit" id="submit-btn" class="btn btn-success">提交申请</button>
                        <span class="help-block"><span>
                    </div>
                </div>
            </form>
            <div class="post-success-box alert alert-success " style="text-align:center; display:none;">
                <h3>恭喜你，你的申请已经提交成功！</h3>
                <p>我尽快联系你，为你定制详细的行程计划！</p>
            </div>
        </div>
    </div>
</div>



<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('customer');
    });
</script>

</body>
</html>