<!DOCTYPE html>
<html>
<head>
    <title>妙妙行程管家</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="zh" />
    <meta name="description" content="妙妙行程管家" />
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

    <link href="/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME GLOBAL STYLES -->
    <!-- BEGIN THEME LAYOUT STYLES -->
    <link href="/assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />
    <!-- END THEME LAYOUT STYLES -->

    <link rel="stylesheet" type="text/css" href="/css/partner.css?v=1.2">
    <link rel="stylesheet" type="text/css" href="/css/plan_new.css?v=1.22">

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/seajs/dist/sea.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/libs/html5shiv.js"></script>
    <script src="../../assets/libs/respond.min.js"></script>
    <![endif]-->


    <script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>


    <script src="/assets/global/scripts/app.min.js" type="text/javascript"></script>

    <script src="/assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
    <script src="/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

    <style>
        @media (max-width: 767px)
            .page-header.navbar {
                height:auto;
            }
    </style>
</head>
<body class="page-header-fixed <?php  if(($this->id == 'trip' && $this->action->id == 'edit')): ?>page-sidebar-closed<?php endif;?> page-sidebar-closed-hide-logo page-container-bg-solid">
    <div class="page-header navbar navbar-fixed-top" style="height:auto;">
        <div class="page-header-inner ">
            <div class="page-logo">
                <a href="/">
                    <img src="/images/plan/trip-logo.png" alt="logo" width="120px" class="logo-default" />
                </a>
                <a href="/trip/" style="font-size:25px; color:#fff; padding:15px;">
                    <i class="fa fa-reply white" ></i>
                </a>
            </div>
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> 返回 </a>

            <div class="page-top">

                <div class="pl_tit clearfix">
                    <h2 class="cGray fontYaHei fl" id="trip-title">
                        <span id="subtriptitle"><?=$trip['title'];?></span>
                        <i class="fa fa-edit" style="font-size:13px;"></i>
                    </h2>
                    <div class="pl_tit_edit fl pr10 none" id="trip-title-ipt" style="display: none;">
                        <input id="js_trip_title_input" type="text" class="ui_input fontYaHei fl" value="<?=$trip['title'];?>">
                    </div>
                </div>

                <div class="export-trip pull-right">
                    <div class="btn-group">
                        <a class="btn green" href="javascript:;" data-toggle="dropdown" aria-expanded="false">
                            <i class="fa fa-file"></i> 导出行程
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu">
                            <li class="nav-item start ">
                                <a href="/trip/preview?type=app&id=<?=$trip['id'];?>" target="_blank"  class="nav-link ">
                                    <i class="fa fa-share"></i>
                                    <span class="title">发送给客人</span>
                                </a>
                            </li>
                            <li class="nav-item start ">
                                <a href="/trip/preview?type=app&id=<?=$trip['id'];?>" target="_blank"  class="nav-link ">
                                    <i class="fa fa-mobile"></i>
                                    <span class="title">预览手机版本</span>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"> </div>

    <div class="page-container">
        <div class="page-sidebar-wrapper">
            <div class="page-sidebar navbar-collapse collapse" style="position: fixed; z-index:9999;">
                <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu page-sidebar-menu-closed" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

                    <li class="nav-item <?php if($this->id == 'trip') :?>active open<?php endif;?> ">
                        <a href="javascript:;" class="nav-link nav-toggle">
                            <i class="fa fa-edit"></i>
                            <span class="title">行程</span>
                            <span class="arrow"></span>
                        </a>
                    </li>
                    <!--li class="nav-item <?php if($this->id == 'customer') :?>active open<?php endif;?>">
                        <a href="/trip/cost" class="nav-link nav-toggle">
                            <i class="fa fa-dollar"></i>
                            <span class="title">费用</span>
                            <span class="arrow"></span>
                        </a>
                    </li-->
                    <!--li class="nav-item <?php if($this->id == 'customer') :?>active open<?php endif;?>">
                        <a href="javascript:void(0);" class="nav-link nav-toggle">
                            <i class="fa fa-file"></i>
                            <span class="title">预览</span>
                            <span class="arrow"></span>
                        </a>
                        <ul class="sub-menu">

                            <li class="nav-item start ">
                                <a href="/trip/preview?type=app&id=<?=$trip['id'];?>" target="_blank"  class="nav-link ">
                                    <i class="fa fa-mobile"></i>
                                    <span class="title">手机版</span>
                                </a>
                            </li>
                            <li class="nav-item start ">
                                <a href="/trip/preview?type=excel&id=<?=$trip['id'];?>" target="_blank"  class="nav-link ">
                                    <i class="icon-bar-chart"></i>
                                    <span class="title">Excel</span>
                                </a>
                            </li>
                            <li class="nav-item start ">
                                <a href="/trip/preview?type=pdf&id=<?=$trip['id'];?>" target="_blank" class="nav-link ">
                                    <i class="fa fa-file"></i>
                                    <span class="title">PDF</span>
                                </a>
                            </li>
                        </ul>
                    </li-->

                </ul>
            </div>

        </div>



        <div class="page-content-wrapper">
            <div class="page-content">

                <div class="pl_bd">
                    <div class="pl_left">
                        <div class="pl_menu">
                            <!--行程单标题 -->
                            <div class="menu_tit">
                                <strong class="fontYaHei f16 cGray">行程单 (<span id="js_plan_totalday">--</span>天)</strong>
                            </div>
                            <!--行程单列表 -->
                            <div class="menu_cnt" id="js_plan_allday_list">
                                <div class="xcd_date clearfix">
                                    <span>出发时间：</span>
                                    <input id="plans_datepicker" type="text" class="ui_input date-picker" value="<?=$trip['start_time'];?>" />
                                </div>
                                <ul class="xcd_item"></ul>
                                <div class="xcd_add" id="xcd_addoneday_last">
                                    <a href="javascript:void(0);" data-bn-ipg="left-addday">添加一天</a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="pl_right">
                        <div class="pl_rright hidden-xs">
                            <div class="pl_lib_tit">
                                <li class="pr pl_rtag_li current _jstabdefaultcity" data-bn-ipg="right-tab-city">
                                    <strong class="rtag_jt" >
							<span class="text">
								<span id="_jsdefaultcityname" data-bn-ipg="right-selectcity" class="title">城市</span>
								<span class="cBlue"><i class="fa fa-sort-desc"></i></span>
		            		</span>
                                    </strong>
                                    <!--下拉城市 -->
                                    <div class="csearch_list">
                                        <div class="city_list fontYahei" id="_jsdefaultcitylist">
                                            <a href="javascript:;" data-cityid="3" data-cityname="吉隆坡" data-bn-ipg="right-select-citylist"><span>吉隆坡</span></a>
                                        </div>
                                    </div>
                                </li>
                            </div>

                            <div class="tabbable-line" style="position: relative;">
                                <ul class="nav nav-tabs " style="display: inline-block;">
                                    <li class="active _jstabdefaultcity">
                                        <a href="javascript:void(0);"><i class="fa fa-map-marker"></i>&nbsp;地点</a>
                                    </li>
                                    <!--li class="_jsTabDemoList">
                                        <a href="javascript:void(0);" ><i class="fa fa-stumbleupon"></i>&nbsp;我的线路</a>
                                    </li-->
                                    <!--li >
                                        <a href="javascript:void(0);" ><i class="fa fa-hotel"></i>&nbsp;酒店</a>
                                    </li>
                                    <li >
                                        <a href="javascript:void(0);" ><i class="fa fa-file"></i>&nbsp;攻略</a>
                                    </li-->
                                </ul>

                                <div class="searchPOIBox" style="display: inline-block; position: absolute; bottom:5px; right:5px; width:205px;">
                                    <form id="_jsSearchPoiFrom" class="form-inline" role="form">
                                        <div class="form-group">
                                            <input id="_jsSearchKeyword" type="text" style=" width:130px;height: 24px; font-size: 12px; padding:2px 10px; line-height: 20px;" class="form-control" id="exampleInputEmail2" placeholder="输入关键词">
                                        </div>
                                        <a id="_jsSearchPoi" href="javascript:void(0);" class="btn btn-sm btn-default">搜索</a>
                                    </form>
                                </div>
                            </div>

                            <div class="pl_rnote" style="display:none;" id="_jsnoteclip">
                                <div class="pl_rncnt">
                                    <!--剪辑和笔记 -->
                                    <div class="pl_rnote_cnt" style="display:;" id="leftrrr">
                                        <!--类型 -->
                                        <div class="head clearfix">
                                        </div>
                                        <div id="_jsnotecliplist">
                                        </div>
                                        <div class="more tc" id="_jsnoteclippager" style="display:none;">
                                            <a href="javascript:;" class="_jsmorenoteclip" data-bn-ipg="right-note-more"><span>更多</span></a>
                                            <a href="javascript:;" class="more_load _jsmorenoteclipload" style="display:none"><span>正在载入</span></a>
                                        </div>
                                    </div>

                                    <!--剪辑和笔记 end edit -->
                                    <div class="pl_rnote_cnt" id="rightrrr" style="display:none;">

                                    </div>
                                </div>
                            </div>

                            <div class="pl_rdemo" style="display: none;" id="_jsDemoBox">
                                <div class="pl_rccnt">
                                    <div class="pl_demo_cnt" style="display:block">
                                        <div id="_jsDemoList">

                                        </div>
                                    </div>
                                    <div class="pl_poiline_more _jsLoadMoreDemo" style="display:block; margin-top:20px;">
                                        <a href="javascript:;" class="loading">正在载入...</a>
                                    </div>
                                </div>
                            </div>
                            <div class="pl_rcity">
                                <div class="pl_rccnt" >
                                    <div class="poi_type_nav head clearfix pt10 f14" >
                                        <div class="btn-group" data-toggle="buttons" id="_jsPoiTypeSelect">
                                            <!--label class="btn btn-sm btn-default">
                                                <input type="radio" name="type" checked=true value="hotel" id="hotel">酒店
                                            </label-->
                                            <label class="btn green btn-outline btn-xs active">
                                                <input type="radio" name="type" checked=true value="sight" id="sight">景点
                                            </label>
                                            <label class="btn green btn-outline btn-xs ">
                                                <input type="radio" name="type" value="restaurant" id="restaurant">美食
                                            </label>
                                            <label class="btn green btn-outline btn-xs">
                                                <input type="radio" name="type" value="shopping" id="shopping">购物
                                            </label>
                                            <label class="btn green btn-outline btn-xs">
                                                <input type="radio" name="type" value="entertainment" id="entertainment">娱乐
                                            </label>
                                            <label class="btn green btn-outline btn-xs">
                                                <input type="radio" name="type" value="mine" id="entertainment">我的地点
                                            </label>
                                            <!--label class="btn btn-sm btn-default">
                                                <input type="radio" name="type" value="island" id="island">海岛
                                            </label-->
                                            <!--label class="btn btn-sm btn-default">
                                                <input type="radio" name="type" value="demo_trip_day" id="island">行程模板
                                            </label-->
                                        </div>
                                    </div>
                                    <div class="clear"></div>
                                    <div>
                                        <!--景点类型 -->
                                        <div class="pl_poi_cnt" style="display:none">
                                            <div id="_jspoilist">

                                            </div>
                                            <div class="pl_poiline_more _jsloadmorepoihotel" style="display:none">
                                                <a href="javascript:;" class="loading">正在载入...</a>
                                            </div>
                                        </div>
                                        <!--住宿类型 -->
                                        <div class="pl_stay_cnt" style="display:none;">
                                            <div id="_jshotellist">
                                                <div class='_jshotrouterecomment' style='display:none; width:0px; height:0; overflow:hidden' ></div>
                                            </div>
                                            <div class="pl_poiline_more _jsloadmorepoihotel" style="display:none;">
                                                <a href="javascript:;" class="loading">正在载入...</a>
                                            </div>
                                            <div style="width:20px;height:25px"></div>
                                        </div>
                                        <div class="pl_rnote_map"   style="width:100%; display:none">
                                            <div id="jsplan_map_list" style="height:100%; width:100%"><img src="/images/plan/map_list_pic.jpg" /></div>
                                            <div class="map_loading" style="display:none">
                                                <div class="loading">
                                                    <span class="fontYaHei">加载中...</span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>



                        <div class="pl_rleft">
                            <div class="pl_nday" id="js_plan_nday">
                                <div class="pl_nday_main">
                                    <div class="xcd_cnt_tit">
                                        <div class="xcd_title">
                                            <input type="text" name="title" value="" placeholder="行程亮点" class="js_trip_info_update">
                                        </div>
                                        <!--div class="setDemoDay toggler tooltips"
                                             data-container="body" data-placement="left" data-html="true" data-original-title="将当天安排设置为我的线路"
                                             id="_js_set_demo_trip" style="">
                                            <i class="fa fa-star-o"></i>
                                        </div-->
                                    </div>
                                    <div class="xcd_cnt_day_actions">

                                        <div class="day_city ">
                                            <div class="row">
                                                <div class="list col-sm-9 clearfix fontYaHei" id="js_plan_taglist">
                                                    <div class="add_city" id="js_plan_addtag">
                                                        <span class="add_city_btn green-haze" id="js_plan_addcity_btn" data-bn-ipg="mid-addcity"> + 添加城市</span>
                                                        <div class="csearch_list" id="js_plan_addcity_list">
                                                            <span class="arrowbig"></span>
                                                            <span class="arrow"></span>
                                                            <div class="pr">
                                                                <input type="text" class="ui_input" placeholder="请输入城市名称" id="xcd-city-input"/>
                                                                <div class="jd_item fontYaHei cGray" id="xcd-city-list">
                                                                    <ul></ul>
                                                                </div>
                                                            </div>
                                                            <div class="hot clearfix" id="js_plan_add_hotplace_city">
                                                                <span class="fl">热门：</span>
                                                                <a href="javascript:void(0);" data-cityname="台北" data-cityid="102207" title="台北">台北</a>
                                                                <a href="javascript:void(0);" data-cityname="高雄" data-cityid="104764" title="高雄">高雄</a>
                                                                <a href="javascript:void(0);" data-cityname="花莲" data-cityid="102209" title="花莲">花莲</a>
                                                                <a href="javascript:void(0);" data-cityname="垦丁" data-cityid="104765" title="垦丁">垦丁</a>
                                                                <a href="javascript:void(0);" data-cityname="屏东" data-cityid="104877" title="屏东">屏东</a>
                                                                <a href="javascript:void(0);" data-cityname="新北" data-cityid="104760" title="新北">新北</a>

                                                                <br/>
                                                                <a href="javascript:void(0);" data-cityname="曼谷" data-cityid="4" title="曼谷">曼谷</a>
                                                                <a href="javascript:void(0);" data-cityname="普吉岛" data-cityid="104057" title="普吉岛">普吉岛</a>
                                                                <a href="javascript:void(0);" data-cityname="清迈" data-cityid="6" title="清迈">清迈</a>
                                                                <a href="javascript:void(0);" data-cityname="拜县" data-cityid="17" title="拜县">拜县</a>
                                                                <a href="javascript:void(0);" data-cityname="苏梅" data-cityid="104067" title="苏梅">苏梅</a>
                                                                <a href="javascript:void(0);" data-cityname="象岛" data-cityid="12" title="象岛">象岛</a>
                                                                <br/>
                                                                <a href="javascript:void(0);" data-cityname="东京" data-cityid="102267" title="东京">东京</a>
                                                                <a href="javascript:void(0);" data-cityname="大阪" data-cityid="102261" title="大阪">大阪</a>
                                                                <a href="javascript:void(0);" data-cityname="京都" data-cityid="102298" title="京都">京都</a>
                                                                <a href="javascript:void(0);" data-cityname="奈良" data-cityid="103838" title="奈良">奈良</a>
                                                                <a href="javascript:void(0);" data-cityname="北海道" data-cityid="106025" title="北海道">北海道</a>
                                                                <a href="javascript:void(0);" data-cityname="高山" data-cityid="103819" title="高山市">高山市</a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="clear"></div>
                                                </div>
                                                <div class="col-sm-3">
                                                    <div class="actions pull-right">
                                                        <div class="btn-group">
                                                            <a class="btn green btn-sm" href="javascript:;" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"> + 添加安排
                                                                <i class="fa fa-angle-down"></i>
                                                            </a>
                                                            <ul class="dropdown-menu pull-right">

                                                                <li class="add_desc_btn">
                                                                    <a href="javascript:;"><i class="fa fa-edit"></i> 行程简介</a>
                                                                </li>
                                                                <li class="divider"> </li>

                                                                <li class="add_poi_btn">
                                                                    <a href="javascript:;"><i class="fa fa-map-marker"></i> 地点（POI）</a>
                                                                </li>
                                                                <li id="_jstrafficcontainer">
                                                                    <a href="javascript:;" id="_jsshowtrafficform"><i class="fa fa-plane"></i> 城际交通</a>
                                                                </li>
                                                                <li class="add_hotel_btn">
                                                                    <a href="javascript:;"><i class="fa fa-hotel"></i> 当日酒店</a>
                                                                </li>

                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="xcd_cnt_day xcd_cnt_day_auto">
                                        <div class="traffic_item">
                                            <div id="_jstrafficlist"></div>
                                        </div>
                                        <div class="xcd_note">
                                            <div class="trip_day_desc"></div>

                                            <div class="trip_day_traffic_note"></div>

                                        </div>


                                        <div class="day_poi_list">
                                            <!--酒店住宿 -->
                                            <div class="hotel_item" id="_jshotelcontainer">
                                                <div class="hotel_item_ul" id="js_plan_hotels">
                                                </div>
                                            </div>
                                            <div class="day_item" id="_jspoicontainer">
                                                <div class="day_item_ul" id="js_plan_activites"></div>
                                            </div>
                                        </div>
                                        <!--行程简介 -->
                                        <!--div class="trip_info" style="margin-top:10px;">

                                            <div class="xcd_note clearfix" >
                                                <span class="pull-left btn btn-sm btn-primary fileinput-button" id="_js_upload_trip_day_image" style="position:relative">
                                                     <i class="glyphicon glyphicon-circle-arrow-up"></i>
                                                     <span>上传当日行程图片</span>
                                                    <input id="tripDayImageFile" style="font-size:20px;" type="file" name="Filedata" multipart=false />
                                                </span>
                                                <button class="pull-left btn btn-default btn-sm" style="margin-left:10px; margin-bottom:20px;" id="_js_set_demo_trip">保存为行程模板</button><span class="set_demo_trip_help"></span>
                                                <a href="javascript:void(0);" class="pull-right btn btn-success btn-sm" style="margin-bottom:20px;">保存</a>
                                            </div>
                                            <div class="xcd_note trip_day_image_box">
                                                <ul>
                                                </ul>
                                            </div>
                                        </div-->

                                        <div id="js_xcd_layer_loading">
                                            <div class="day_trip_empty">本日还未安排行程</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="pl_recommend_package" js="">

                            </div>
                            <div class="pl_prepare" id="js_plan_prepare">
                                <div class="pl_trip_note_box" style="padding:10px; height:100%; display: none;">
                                    <div class="actions" style="text-align:center; color:#666; line-height:30px;font-size:16px;">
                                        <div class="pull-left" style="position:relative;">

							<span class="btn btn-xs btn-success fileinput-button">
								 <i class="glyphicon glyphicon-circle-arrow-up"></i>
								 <span>插入图片</span>
								 <!-- The file input field used as target for the file upload widget -->
								<input id="fileupload" type="file" name="Filedata" multipart=false/>
							</span>
                                            <span id="note-pic-uploading" style="font-size:14px;display:none;">正在上传...</span>
                                        </div>
                                        <span>小贴士</span>
                                        <div class="pull-right" style="margin-bottom:10px;">
                                            <button class="btn btn-success btn-sm" href="javascript:void(0);" id="js_plan_note_save_btn">保存</button>
                                        </div>
                                    </div>

                                    <textarea id="tripNoteText" style="width:100%;min-height:400px; height:100%;background:#fff;"></textarea>
                                </div>
                            </div>
                        </div>
                        <div class="clear"></div>
                    </div>
                    <div class="clear"></div>
                </div>


                <script type="text/template" id="poiItemView-tpl">

                    {{#poi_list}}
                    <div class="pl_poi_list">
                        <div class="header clearfix">
                            <a href="javascript:;" class="fl _jsplanpoidetail" data-poi_id="{{poi_id}}">
                                <img src="{{cover_image}}" onerror="this.src='/images/plan/poi_200_133.png'" alt="" title="" width="80" height="60">
                            </a>

                            {{#is_hotel}}
                            <div class="content">
                                <a href="/hotel/edit?hotel_id={{id}}" target="_blank" class="title_link _jsplanpoidetail" data-poi_id="{{id}}" data-btn="right-poi-title-1" title="{{poi_name}}">
                                    <span class="cBlack">{{title}}</span>
                                </a>
                                <div class="clearfix">
                                    {{#area}}<i class="fa fa-map-marker"></i>：{{area}}</br>{{/area}}
                                    <div class="content">
                                        {{#agoda_url}}<a target="_blank" href="{{agoda_url}}">agoda预定</a>{{/agoda_url}}
                                    </div>
                                </div>
                            </div>
                            <div class="actions">
                                <a href="javascript:void(0);" class="fr add_btn _jsrightaddpoi" data-poi_id="{{poi_id}}" data-address="{{address}}" data-href="/poi/edit?id={{poi_id}}" data-poi_name="{{poi_name}}" data-type="{{type}}" data-desc="{{desc}}" data-cover_image="{{cover_image}}" data-city_id="{{city_id}}" data-city_name="{{city_name}}" lat="{{lat}}" lon="{{lon}}">
                                    <span class="tips">添加到安排</span>
                                </a>
                                <a href="javascript:void(0);" class="fr add_recommend_hotel_btn _jsrightaddrecommendhotel" data-hotel_id="{{id}}">
                                </a>
                            </div>
                            {{/is_hotel}}
                            {{^is_hotel}}
                            <a href="javascript:void(0);" target="_blank" class="title_link _jsplanpoidetail" data-poi_id="{{poi_id}}" data-btn="right-poi-title-1" title="{{poi_name}}">
                                <span class="cBlack">{{poi_name}}</span>
                            </a>
                            <div class="clearfix">
                                {{#area}}<i class="fa fa-map-marker"></i> {{area}}</br>{{/area}}
                                <div class="content">
                                    <p class="jj_h">{{desc}}</p>
                                </div>
                            </div>
                            <a href="javascript:void(0);" class="fr add_btn _jsrightaddpoi" data-poi_id="{{poi_id}}" data-address="{{address}}" data-href="/poi/edit?poi_id={{poi_id}}" data-poi_name="{{poi_name}}" data-type="{{type}}" data-desc="{{desc}}" data-cover_image="{{cover_image}}" data-city_id="{{city_id}}" data-city_name="{{city_name}}" lat="{{lat}}" lon="{{lon}}">
                                <span class="tips">添加</span>
                            </a>
                            {{/is_hotel}}
                        </div>
                    </div>
                    {{/poi_list}}
                </script>

                <script type="text/template" id="noteItemView-tpl">
                    {{#note_list}}
                    <div class="pl_poi_list note">
                        <div class="header clearfix">
                            <a href="javascript:;" class="title_link _jsplanpoidetail" data-poi_id="{{poi_id}}" data-btn="right-poi-title-1" title="{{poi_name}}">
                                <span class="cBlack">{{title}}</span>
                            </a>
                            <a href="javascript:void(0);" class="fr add_btn _jsrightaddnote" data-content="{{content}}" data-title="{{title}}">
                                <span class="tips">添加到行程</span>
                            </a>
                        </div>
                        <div class="content clearfix">
                            <p class="jj_h">{{{content}}}</p>
                        </div>
                    </div>
                    {{/note_list}}
                </script>

                <script type="text/template" id="demoTripDayView-tpl">
                    {{#demo_trip_day_list}}
                    <div class="pl_demo_list">
                        <div class="header clearfix">
                            <a href="javascript:;" class="title_link _jsplanpoidetail" data-poi_id="{{poi_id}}" data-btn="right-poi-title-1" title="{{poi_name}}">
                                <span class="cBlack">{{title}}</span>
                            </a>
                            <a href="javascript:void(0);" class="fr add_btn _jsrightadd_demotripday" data-demo_id="{{id}}"  data-desc="{{desc}}" data-traffic_note="{{traffic_note}}" data-title="{{title}}">
                                <span class="tips">添加到计划</span>
                            </a>
                        </div>
                        <div class="content clearfix">
                            <p class="">{{{desc}}}</p>
                        </div>
                    </div>
                    {{/demo_trip_day_list}}
                </script>



                <script type="text/template" id="onedayView-tpl">
                    <a href="javascript:void(0);" class="insert" title="插入一天" data-bn-ipg="left-insertday" onfocus="this.blur();">插入一天</a>
                    <a href="javascript:void(0);" class="delete" onfocus="this.blur();">{{datedesc}}</a>
                    <a href="javascript:void(0);" class="remove" onfocus="this.blur();" title="删除一天">删除</a>
                    {{^has_city}}
                    <p class="nonep">暂无安排{{has_city}}</p>
                    {{/has_city}}
                    {{#has_city}}
                    <dl>
                        <dt>
                            {{#city_list}}
                            {{^first}}<img src="/images/plan/xcd_city_icon.png" class="vm ml5 mr5" alt="">{{/first}}
                            <em>{{city_name}}</em>
                            {{/city_list}}
                        <dd>{{timedesc}}</dd>
                        </dt>
                    </dl>
                    {{/has_city}}
                    <div class="clearfix"></div>
                </script>


                <!-- poi_event_template -->
                <script type="text/template" id="onedayevent-t1">
                    <div class="pr">
                        {{#href}}
                        <a href="javascript:;"  class="_jsplanpoidetail"><img src="{{cover_image}}" alt="" width="48" height="48" class="fl" onerror="this.src='/images/plan/poi_80_80.png'" /> </a>
                        {{/href}}
                        {{^href}}
                        <img src="{{cover_image}}" alt="" width="48" height="48" class="fl" onerror="this.src='/images/plan/poi_80_80.png"/>
                        {{/href}}
                        <div class="content">
                            <div><strong class="fb">
                                    {{#href}}
                                    <a href="javascript:void();" target="_blank"  class="_jsplanpoidetail" data-poi_id="{{poi_id}}">{{poi_name}}</a>
                                    {{/href}}
                                    {{^href}}
                                    {{poi_name}}
                                    {{/href}}
                                </strong></div>
                            {{#desc}}
                            <p class="tcnt"><strong class="fb">简介：</strong>{{desc}}</p>
                            {{/desc}}
                            {{#note}}
                            <p class="overheight"><strong class="fb">备注：</strong>{{note}}</p>
                            {{/note}}
                        </div>
                        <div class="clear"></div>
                        <div class="p_list_seting">
                            <span>设置</span>
                            <ul>
                                <li><a href="javascript:;" class="edit jsplan_edit" data-bn-ipg="planedit-col2-event-edit"><strong>编辑</strong></a></li>
                                <li><a href="javascript:;" class="del delete jsplan_del" data-bn-ipg="planedit-col2-event-delete"><strong>删除</strong></a></li>
                                <!--li class="hvlist">
                                    <a href="javascript:;" class="next"><strong class="move">移动到</strong></a>
                                    <ul class="jsplan_move">
                                    </ul>
                                </li-->
                            </ul>
                        </div>
                    </div>

                </script>

                <!-- hotel event template -->
                <script type="text/template" id="onedayevent-t2">

                    <div class="pr">
                        <a href="{{href}}" target="_blank">
                            <img src="{{cover_image}}" class="fl" alt="" width="48" height="48" onerror="this.src='/images/plan/pl_hotel_default.png';"/>
                        </a>
                        <div class="content">
                            <div><strong class="fb"><a href="{{href}}" target="_blank">{{title}}</a></strong></div>
                            {{#price}}
                            <p>{{price}}</p>
                            {{/price}}

                            {{#note}}
                            <p class="overheight"><strong class="fb">备注：</strong>{{note}}</p>
                            {{/note}}
                        </div>

                        <div class="clearfix"></div>
                        <div class="p_list_seting">
                            <span>设置</span>
                            <ul>
                                <li><a href="javascript:void(0);" class="edit jsplan_edit" data-bn-ipg="planedit-col2-event-edit"><strong>编辑</strong></a></li>
                                <li><a href="javascript:void(0);" class="del jsplan_del" data-bn-ipg="planedit-col2-event-delete"><strong>删除</strong></a></li>
                                <!--li class="hvlist">
                                    <a href="javascript:void(0);" class="next"><strong class="move">移动到</strong></a>
                                    <ul class="jsplan_move">
                                        <li><a href="#">第一天</a></li>
                                    </ul>
                                </li-->
                            </ul>
                        </div>
                    </div>
                </script>

                <script type="text/template" id="tpl_searchlivecity">
                    <div class="citylist_box" >{{#list}}<a {{#current}}class="current"{{/current}} href="javascript:void(0)" data-city_id="{{city_id}}" data-city_name="{{city_name}}" data-title="{{name}}" data-type="{{type}}" data-dataid="{{id}}"><span>{{name}}</span></a>{{/list}}</div>
                </script>

                <script type="text/template" id="tpl_searchlivepoi">
                    <div class="poilist_box" >{{#poi_list}}<a {{#current}}class="current"{{/current}} href="javascript:void(0)" data-city_id="{{city_id}}" data-city_name="{{city_name}}" data-desc="{{desc}}" data-cover_image="{{cover_image}}" data-title="{{poi_name}}" data-type="{{type}}" data-dataid="{{poi_id}}"><span>{{poi_name}}({{city_name}})</span></a>{{/poi_list}}</div>
                </script>


                <script type="text/template" id="tpl_onedaytraffic">
                    <div class="list clearfix">
                        <div class="city col-sm-6">
                            <div class="name">
                                <strong class="fontYaHei" title="{{from_place}}">{{from_place}}</strong>
                                <div><span class="fontArial">{{start_hours}} : {{start_minutes}}</span></div>
                            </div>
                            <div class="icon {{mode_icon}}"></div>
                            <div class="name">
                                <strong class="fontYaHei" title="{{to_place}}">{{to_place}}</strong>
                                <div>
                                    <span class="fontArial">{{end_hours}} : {{end_minutes}}</span>
                                </div>
                            </div>
                        </div>
                        <div class="money fontYaHei col-sm-6">
                            <p>班次：<span class="fontArial">{{traffic_number}}</span></p>

                        </div>
                        {{#note}}<div class="text">备注：{{note}}</div>{{/note}}
                        <div class="edit">
                            <a href="javascript:;" class="del _jsdelonetraffic" onfocus="this.blur();" title="删除">删除</a>
                            <a href="javascript:;" class="ebj _jseditonetraffic" onfocus="this.blur();" title="编辑">编辑</a>
                        </div>
                    </div>
                </script>

                <script type="text/template" id="citylist_select">
                    {{#city_list}}
                    <a href="javascript:;" data-cityid="{{city_id}}" data-cityname="{{city_name}}"><span>{{city_name}}</span></a>
                    {{/city_list}}
                </script>

                <script>
                    seajs.use('/assets/trip/router.js', function(router) {
                        window.tripInfo = {"tripId" : "<?=$trip['id'];?>", "tripTitle": "<?=$trip['title'];?>", "starttime":"<?=$trip['start_time'];?>"};
                        router.load('plan');
                    });
                </script>


            </div>
        </div>


    </div>


    <script type='text/javascript'>
        (function(m, ei, q, i, a, j, s) {
            m[i] = m[i] || function() {
                (m[i].a = m[i].a || []).push(arguments)
            };
            j = ei.createElement(q),
                s = ei.getElementsByTagName(q)[0];
            j.async = true;
            j.charset = 'UTF-8';
            j.src = '//static.meiqia.com/dist/meiqia.js';
            s.parentNode.insertBefore(j, s);
        })(window, document, 'script', '_MEIQIA');
        _MEIQIA('entId', 22383);
    </script>

<div style="display:none;">
    <script type="text/javascript">
        var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
        document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F53f78afdb90bf194696a6823b9c5aac8' type='text/javascript'%3E%3C/script%3E"));
    </script>
</div>
</body>
</html>