<link rel="stylesheet" type="text/css" href="/css/plan.css">
<div class="plan-create-header">
    <div class="">
        <div class="plan-create-hd clearfix">
            <h2 class="title">创建行程</h2>
            <div class="list">
                <span class="list_number current"><span>1</span>选择国家</span>
                <span class="list_gt"></span>
                <span class="list_number"><span>2</span>选择城市</span>
                <span class="list_gt"></span>
                <span class="list_number"><span>3</span>城市排序及天数调整</span>
            </div>
        </div>
    </div>
</div>
<div class="plan-found-content">
    <div class="">
        <!--div class="found_search">
            <input type="text" class="ui_input" id="addcity" value="" placeholder="请在这里搜索一个想去的国家或城市" data-bn-ipg="plancreate-country-search">
            <!--搜索联想框 -->
        <div class="found_layer" id="plan_search_drop" style="display: none;"></div>
        </div-->
        <div class="found_tag">
            <a href="javascript:;" class="current" data-bn-ipg="plancreate-continent-0">热门国家地区</a><span>|</span>
            <a href="javascript:;" class="_jschgcontinent" data-id="10" data-bn-ipg="plancreate-continent-1">亚洲</a><span>|</span>
        </div>
        <div class="found_city clearfix">
            <div class="item row" id="_jscountrylist">
            </div>
        </div>
    </div>
</div>


<div class="pl_found_mask" style="">
    <div class="pl_found_mask_cnt">
        <div class="fontYaHei clearfix list_two_hang _jschoosetags" style="display: none;"></div>
        <div class="fontYaHei content_text _jsnochoosetags">
            请从上边选择你客户想去的国家和地区
        </div>
        <a href="javascript:void(0);" class="button fr button_disabled" id="_jstostep3" onfocus="this.blur();" data-bn-ipg="plancreate-bottom-next">下一步</a>
    </div>
</div>

<script type="text/template" id="country-list-tpl">
    {{#country_list}}
    <div class="list col-sm-3">
        <div class="img">
            <img src="{{cover_image}}" width="200" height="143" alt="">
        </div>
        <div class="title">
            <a href="/place/{{id}}" class="name fl" target="_blank" data-bn-ipg="plancreate-countryname-1">{{name}}</a>
            <a href="javascript:;" data-type="1" data-id="{{id}}" class="ui_btn_smallB fr _jsgotostep2" data-bn-ipg="plancreate-countryselect-2">选择</a>
        </div>
    </div>
    {{/country_list}}
</script>

<script>
    seajs.use('/assets/trip/router.js', function(router){
        router.load('/step/step1');
    });
</script>
