
<div class="tabbable-line">
    <ul class="nav nav-tabs ">
        <li>
            <a href="/trip/?type=customer" > 客户的行程 </a>
        </li>
        <li>
            <a href="/trip/?type=demo"> 行程模板 </a>
        </li>
        <li class="active">
            <a href="/trip/demoDayTrip"> 日游线路 </a>
        </li>
    </ul>

    <div class="tab-content">

        <?php if(!empty($trip_list)):?>
        <div class="tripList row">
            <?php foreach($trip_list as $trip) :?>
                <div class="col-lg-4 card">
                    <div class="portlet light portlet-fit ">
                        <div class="portlet-title">
                            <div class="caption">
                                <span class="caption-subject font-green bold uppercase" ><?=$trip['title'];?></span>
                                <a class="delete-demo-day" href="javascript:void(0);" demo_day_id="<?=$trip['id'];?>">
                                    <i class="fa fa-close" style="color:#999; font-size:14px; position: absolute; right:25px; top:30px;"></i>

                                </a>
                            </div>
                        </div>
                        <div class="portlet-body">
                            <div class="caption-desc font-grey-cascade"
                                 style="width:100%;display: -webkit-box;-webkit-line-clamp: 2;-webkit-box-orient: vertical;height:120px; overflow: hidden; line-height: 20px; text-overflow: ellipsis; word-break: break-all;">
                                <?=nl2br($trip['desc']);?>
                            </div>

                        </div>
                    </div>
                </div>
            <?php endforeach;?>
        </div>

        <?php else:?>
            <div class="note note-info">
                <h4 class="block">什么是线路？</h4>
                <p> 线路是指一天行程的安排（即一天的行程模板）</p>
                <p style="padding-top:10px;">
                    线路会出现在制作行程界面的右侧，可通过线路的组合，创建出各种灵活的客户行程。
                </p>
                <p style="padding-top:10px; padding-bottom:10px;">
                    通过选择线路加入行程，可以像搭积木一样，几分钟就可以创建个性化的行程。
                </p>
                <h4 class="block">如何创建线路？</h4>
                <p> 可以在行程编辑页面，点击<i class="fa fa-star-o"></i> 按钮设置当前编辑的安排成我的线路</p>
                <p>
                    <a href="/images/setDemo.png" target="_blank"><img src="/images/setDemo.png" width="200px" /></a>
                </p>
            </div>
        <?php endif;?>

    </div>

</div>



<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('partner/demo_day_list');
    });
</script>
