
<div class="tabbable-line">
    <ul class="nav nav-tabs ">
        <li <?php if($type == 'app'):?>class="active"<?php endif;?>>
            <a href="/trip/preview?type=app&id=<?=$trip['id'];?>" > 手机版 </a>
        </li>
        <li <?php if($type == 'excel'):?>class="active"<?php endif;?> >
            <a href="/trip/preview?type=excel&id=<?=$trip['id'];?>"> Excel简洁版 </a>
        </li>
        <!--li <?php if($type == 'pdf'):?>class="active"<?php endif;?> >
            <a href="/trip/preview?type=pdf&id=<?=$trip['id'];?>"> PDF打印版 </a>
        </li-->

    </ul>

    <div class="tab-content">

        <?php if($type == 'excel'):?>
            <style>
                .page-content {background:#fff !important;}
                .address-box {  text-align: center; position: relative;  }
                .address-box label {line-height: 23px;font-size: 14px;color: #000;}
                .address-box .g-address {display: inline-block; width:600px;}
                .address-box .g-address input {outline:none; width:600px;}
                .address-box .copy-btn {width: 100%;height:35px;line-height:35px;position: absolute;right: 0px; top:-3px; color:#17C4BB;border: 1px solid #17C4BB;color: #17C4BB;}
                .address-box #clip_container {position: relative;width:100px; display:inline-block; text-align: center;}
                .address-box .copy-btn-flash { position: absolute; right: 0px; top: 0px; width: 204px; height: 40px; z-index: 99;}

            </style>

            <div class="address-box">
                <label>发送链接:</label>
                <div class="g-address" style="">
                    <input type="text" class="form-control" value="<?=Yii::app()->request->hostInfo;?>/export/excel?hash_id=<?=$trip['hash_id'];?>">
                </div>
                <p id="clip_container">
                    <b id="a_cptxt" class="copy-btn">点击复制</b>
                    <div class="copy-btn-flash">
                        <embed id="ZeroClipboardMovie_1" src="/assets/js/ZeroClipboard.swf" loop="false" menu="false"
                               quality="best" bgcolor="#ffffff" width="204" height="40" name="ZeroClipboardMovie_1"
                               align="middle" allowscriptaccess="always" allowfullscreen="false"
                               type="application/x-shockwave-flash"
                               pluginspage="http://www.macromedia.com/go/getflashplayer"
                               flashvars="id=1&amp;width=204&amp;height=40" wmode="transparent">
                    </div>
                </p>


            </div>

            <iframe src="<?=Yii::app()->request->hostInfo;?>/export/excel?hash_id=<?=$trip['hash_id'];?>&print=1" frameborder="0" scrolling="no" id="test" onload="this.height=100" width="100%"></iframe>

            <script type="text/javascript">
                function reinitIframe(){
                    var iframe = document.getElementById("test");
                    try{ var bHeight = iframe.contentWindow.document.body.scrollHeight;
                    var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
                        var height = Math.max(bHeight, dHeight);
                        iframe.height = height;
                        console.log(height); }catch (ex){} }
                window.setInterval("reinitIframe()", 200);
            </script>

            <script src="/assets/js/qrcode.min.js"></script>
            <script src="/assets/js/ZeroClipboard.js"></script>
            <script type="text/javascript">

                var codeurl="<?=Yii::app()->request->hostInfo;?>/export/excel?hash_id=<?=$trip['hash_id'];?>";

                function init_cpswf() {
                    ZeroClipboard.moviePath = '/assets/js/ZeroClipboard.swf'
                    var clip = new ZeroClipboard.Client();
                    clip.setHandCursor( true );
                    clip.setCSSEffects( true );
                    clip.addEventListener('load', function (client) {
                        debugstr("Flash movie loaded and ready.");
                    });

                    clip.addEventListener('mouseOver', function (client) {
                        clip.setText(codeurl);
                    });

                    clip.addEventListener('complete', function (client, text) {
                        //document.getElementById('a_cptxt').className = '';
                        alert('行程URL已复制，您可以在微信上直接粘贴，发送给客人！');
                    });
                    clip.glue( 'a_cptxt', 'clip_container' );
                }

                init_cpswf();

            </script>

        <?php elseif($type == 'pdf'): ?>
            <style>
                .page-content {background:#fff !important;}
                .mobile-box{
                    width:282px;
                    height:577px;
                    margin:0 auto;
                }
            </style>


            <iframe src="<?=Yii::app()->request->hostInfo;?>/export/pdf?hash_id=<?=$trip['hash_id'];?>&print=1" frameborder="0" scrolling="no" id="test" onload="this.height=100" width="100%"></iframe>

            <script type="text/javascript">
                function reinitIframe(){
                    var iframe = document.getElementById("test");
                    try{ var bHeight = iframe.contentWindow.document.body.scrollHeight;
                        var dHeight = iframe.contentWindow.document.documentElement.scrollHeight;
                        var height = Math.max(bHeight, dHeight);
                        iframe.height = height;
                        console.log(height); }catch (ex){} }
                window.setInterval("reinitIframe()", 200);
            </script>

        <?php else:?>

            <style>
                .page-content {background:#fff !important;}
                .mobile-box{
                    width:282px;
                    height:550px;
                    margin:0 auto;
                }
                .mobile-frist{
                    width: 282px;
                    height: 46px;
                    background: #ececec;
                    border-radius: 40px 40px 0 0 !important;
                    position: relative;
                }
                .mobile-center{
                    width: 282px;
                    height: 488px;
                    border-left: 1px solid #ececec;
                    border-right: 1px solid #ececec;
                    background: #000;
                    position: relative;
                }
                .mobile-last{
                    width: 282px;
                    height: 43px;
                    background: #ececec;
                    border-radius: 0 0 30px 30px !important;
                    text-align: center;
                }
                .voice{
                    width: 60px;
                    height: 6px;
                    background: #aea6ae;
                    margin: 0 auto;
                    border-radius: 6px;
                    position: absolute;
                    top: 21px;
                    left: 111px;
                }
                .mobile-ifream{
                    width: 100%;
                    height: 100%;
                    border:none;
                    position: absolute;
                    left: 0;
                    top: 0;
                    background:#F6F5F3;
                }
                .shareBoxB {
                    width: 314px;
                    height: 530px;
                    opacity: 0.85;
                    margin:0 auto;
                    position: relative;
                    margin-top:100px;
                }
                .create-qrcodeBox {
                    width: 200px;
                    margin: 0 auto;
                    margin-top: 20px;
                }
                .g-qrcode {
                    width: 145px;
                    margin: auto;
                    padding: 5px;
                    background: #fff;
                }
                .g-qrcode-desc {
                    width: 100%;
                    height: 60px;
                    line-height: 30px;
                    color:#23b168;
                    text-align: center;
                    font-size: 15px;
                }
                .address-box {
                    width: 320px;
                    margin: auto;
                    margin-top: 10px;
                }
                .address-box label {line-height: 23px;font-size: 14px;color: #000;}
                .g-address {display: inline-block;}
                .g-address input {
                    width: 240px;
                    height: 28px;
                    border-radius: 3px;
                    outline: none;
                    border: 1px solid #63c7bb;
                    background: #fff;
                    text-indent: .5em;
                    font-size: 14px;
                    padding:0 5px;
                }
                #clip_container {position: relative;width:100%;text-align: center; margin-bottom:40px;}
                .industry-zone {
                    width: 280px;
                    margin: auto;
                    margin-top: 12px;
                    height: 65px;
                }
                .copy-btn {width: 100%;height:40px;line-height:40px;position: absolute;left: 0px;border: 1px solid #17C4BB;color: #17C4BB;}
                .copy-btn-flash {position: absolute; right: 0px; top: 0px; width: 204px; height: 40px; z-index: 99;}
            </style>
            <div class="main-box clearfix" style="color:#666;margin: 0 auto;">
                <!--mobile box-->
                <div class="row">
                    <div class="col-sm-6 mobile-preview hidden-xs">
                    <div class="mobile-box">
                        <div class="mobile-frist">
                            <p class="voice"></p>
                        </div>
                        <div class="mobile-center">
                            <iframe name="game" src="<?=Yii::app()->request->hostInfo;?>/export/app?hash_id=<?=$trip['hash_id'];?>" width="290" height="488" class="mobile-ifream" id="ifream-game-box"></iframe>
                        </div>
                        <div class="mobile-last">
                            <div class="game-refresh"></div>
                        </div>
                    </div>
                    </div>
                    <div class="col-sm-6 shareBoxB" style="height:558px;">
                        <!-- qrcode -->
                        <div class="create-qrcodeBox">
                            <div class="g-qrcode" id="create-qrcode">
                                <div class="userdemo-ico" style="top: 18.5%;"></div>
                            </div>
                            <div class="g-qrcode-desc">微信扫描二维码在手机上预览<br />并发送给客人</div>
                        </div>
                        <!-- game url -->
                        <div class="address-box">
                            <label>行程链接：</label>
                            <div class="g-address">
                                <input type="text" value="<?=Yii::app()->request->hostInfo;?>/export/app?hash_id=<?=$trip['hash_id'];?>">
                            </div>
                            <p id="clip_container">
                                <b id="a_cptxt" class="copy-btn">点击复制</b>
                            <div class="copy-btn-flash"><embed id="ZeroClipboardMovie_1" src="/assets/js/ZeroClipboard.swf" loop="false" menu="false" quality="best" bgcolor="#ffffff" width="204" height="40" name="ZeroClipboardMovie_1" align="middle" allowscriptaccess="always" allowfullscreen="false" type="application/x-shockwave-flash" pluginspage="http://www.macromedia.com/go/getflashplayer" flashvars="id=1&amp;width=204&amp;height=40" wmode="transparent"></div></p>
                        </div>
                    </div>
                </div>
            </div>

            <script src="/assets/js/qrcode.min.js"></script>
            <script src="/assets/js/ZeroClipboard.js"></script>
            <script type="text/javascript">
                var codeshow = $("#create-qrcode");
                var codeurl="<?=Yii::app()->request->hostInfo;?>/export/app?hash_id=<?=$trip['hash_id'];?>";
                var qrcode = new QRCode(codeshow[0], {
                    width : 134,
                    height : 134
                });
                qrcode.makeCode(codeurl);

                function init_cpswf() {
                    ZeroClipboard.moviePath = '/assets/js/ZeroClipboard.swf'
                    var clip = new ZeroClipboard.Client();
                    clip.setHandCursor( true );
                    clip.setCSSEffects( true );
                    clip.addEventListener('load', function (client) {
                        debugstr("Flash movie loaded and ready.");
                    });

                    clip.addEventListener('mouseOver', function (client) {
                        clip.setText(codeurl);
                    });

                    clip.addEventListener('complete', function (client, text) {
                        //document.getElementById('a_cptxt').className = '';
                        alert('行程URL已复制，您可以在微信上直接粘贴，发送给客人！');
                    });
                    clip.glue( 'a_cptxt', 'clip_container' );
                }

                init_cpswf();

            </script>
        <?php endif;?>



    </div>

</div>
