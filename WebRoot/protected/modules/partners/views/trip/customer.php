<div class="page-bar">
    <ul class="page-breadcrumb">
        <li>
            <i class="icon-home"></i>
            <a href="/">首页</a>
            <i class="fa fa-angle-right"></i>
        </li>
        <li>
            <span>客户的行程</span>
        </li>
    </ul>
</div>

<div class="tripList row">
    <?php foreach($trip_list as $trip) :?>
        <div class="tripCard col-lg-12 col-sm-12">
            <div class="innerCard">
                <div class="coverImage col-sm-2">
                </div>
                <div class="tripInfo">
                    <h4><?=$trip['title'];?></h4>
                    <p class="tripTime">出发日期：<?=$trip['start_time'];?>，<?=$trip['days'];?>天</p>

                    <p></p>
                </div>
                <div class="tripActions" trip_id="<?=$trip['id'];?>">
                    <a href="/trip/edit?id=<?=$trip['id'];?>" class="btn btn-xs btn-success" target="_blank">
                        <i class="icon icon-edit"></i>修改
                    </a>

                    <a class="btn btn-xs btn-default copy-btn" data-toggle="modal" data-target=".modal-copy-trip">
                        <i class="icon-copy"></i>复制
                    </a>
                    <a href="javascript:void(0);" class="btn btn-xs btn-danger delete_trip_btn" >
                        <i class="icon tos-icon icon-trash"></i>删除
                    </a>
                </div>

                <div class="tripViews">
                    <a class="btn btn-xs btn-default hidden-xs" target="_blank" href="/export/pdf?trip_id=<?=$trip['id'];?>&admin_id=<?=Yii::app()->partner->id;?>">PDF版本</a>
                    <a class="btn btn-xs btn-default" target="_blank" href="/export/app?trip_id=<?=$trip['id'];?>&admin_id=<?=Yii::app()->partner->id;?>">手机版本</a>
                    <a class="btn btn-xs btn-default hidden-xs" target="_blank" href="/export/excel?trip_id=<?=$trip['id'];?>&admin_id=<?=Yii::app()->partner->id;?>">讨论版本</a>

                </div>
            </div>
        </div>
    <?php endforeach;?>
</div>


<div class="modal fade modal-copy-trip" tabindex="-1" >
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">复制行程</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="copy-form">
                    <div class="form-group" style="display: none;">
                        <label for="sourceId">当前行程ID</label>
                        <input name="sourceId" type="text" class="form-control" id="sourceId" value="" readonly>
                    </div>
                    <div class="form-group">
                        <label for="inputTitle">复制后的行程标题</label>
                        <input name="targetTitle" type="text" class="form-control" id="inputTitle" placeholder="行程标题">
                    </div>
                    <!--div class="form-group">
                        <label for="inputCustomerId">复制给的目标用户ID</label>
                        <input name="targetCustomerId" type="text" class="form-control" id="inputCustomerId" placeholder="用户ID">
                    </div-->
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary copy-submit-btn">确定复制</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('partner/trip_list');
    });
</script>
