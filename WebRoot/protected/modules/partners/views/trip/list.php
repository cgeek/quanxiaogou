
<div class="tabbable-line">
    <ul class="nav nav-tabs ">
        <li <?php if($type == 'customer'):?>class="active"<?php endif;?> >
            <a href="/trip/?type=customer" > 客户的行程 </a>
        </li>
        <!--li <?php if($type == 'demo'):?>class="active"<?php endif;?> >
            <a href="/trip/?type=demo"> 行程模板 </a>
        </li>
        <li <?php if($type == 'dayDemo'):?>class="active"<?php endif;?> >
            <a href="/demoDay/list"> 日游线路 </a>
        </li-->

        <?php if($type == 'demo'):?>
            <a class="pull-right btn btn-sm btn-success hidden-xs create-trip-btn" type="demo" href="javascript:void(0);">添加行程模板</a>
        <?php else:?>
            <a class="pull-right btn btn-sm btn-success hidden-xs create-trip-btn" type="customer" href="javascript:void(0);">添加客户行程</a>
        <?php endif;?>
    </ul>

    <div class="tab-content">
        <?php if(!empty($trip_list)):?>
        <div class="tripList row">
            <?php foreach($trip_list as $trip) :?>
                <div class="tripCard col-lg-12 col-sm-12" trip_id="<?=$trip['id'];?>">
                    <div class="innerCard">
                        <div class="coverImage col-sm-2">
                        </div>
                        <div class="tripInfo">
                            <h4><a href="/trip/edit?id=<?=$trip['id'];?>"><?=$trip['title'];?></a></h4>
                            <p class="tripTime">出发日期：<?=$trip['start_time'];?>，<?=$trip['days'];?>天</p>
                            <?php if($trip['customer_id'] > 0):?>
                                <p class="customer"><a href="/customer/edit/?id=<?=$trip['customer_id'];?>"><i class="fa fa-user"></i> <?=$trip['customer_name'];?></a></p>
                            <?php endif;?>
                        </div>

                        <div class="tripActions" trip_id="<?=$trip['id'];?>">
                            <a href="/trip/preview?type=app&id=<?=$trip['id'];?>" class="btn btn-xs btn-success">
                                <i class="fa fa-share"></i>发送
                            </a>

                            <a href="/trip/edit?id=<?=$trip['id'];?>" class="btn btn-xs btn-success">
                                <i class="fa fa-edit"></i>修改
                            </a>

                            <a class="btn btn-xs btn-default copy-btn" data-toggle="modal" data-target=".modal-copy-trip">
                                <i class="fa fa-copy"></i>复制
                            </a>
                            <a href="javascript:void(0);" class="btn btn-xs btn-danger delete_trip_btn" >
                                <i class="fa fa-trash"></i>删除
                            </a>
                        </div>

                    </div>
                </div>
            <?php endforeach;?>
        </div>

        <?php else:?>
            <?php if($type == 'demo'):?>

                <?php if(isset($_GET['query'])):?>
                    <p>没有找到符合条件的行程模板</p>
                <?php endif?>

                <div class="note note-info">
                    <h4 class="block">什么是行程模板？</h4>
                    <p> 行程模板是专属自己的经典行程库。把自己的经典行程添加为行程模板以后，就可以很方便的在此基础上进行复制、修改啦，可以大大节省时间哦 </p>
                    <p style="padding-top:10px;">
                        比如：<span style="text-decoration: underline;">台湾环岛14日游，普吉岛蜜月7日游，日本家庭休闲5日游</span>
                    </p>
                    <p style="margin-top: 20px;">赶紧创建自己的行程模板吧
                        <a class=" btn btn-xs btn-success create-trip-btn" type="demo" href="javascript:void(0);">+ 添加行程模板</a>
                    </p>
                </div>

            <?php else: ?>
                <?php if(isset($_GET['query'])):?>
                    <p>没有找到符合条件的行程，赶紧创建一个吧</p>
                <?php else:?>
                    <p>你还没有客户行程，赶紧<a class="  create-trip-btn" type="customer" href="javascript:void(0);">创建</a>一个吧, </p>
                <?php endif;?>
            <?php endif;?>
        <?php endif;?>

    </div>

</div>


<div class="modal fade modal-copy-trip" tabindex="-1" >
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">复制行程</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="copy-form">
                    <div class="form-group" style="display: none;">
                        <label for="sourceId">当前行程ID</label>
                        <input name="sourceId" type="text" class="form-control" id="sourceId" value="" readonly>
                    </div>
                    <div class="form-group">
                        <label for="inputTitle">复制后的行程标题</label>
                        <input name="targetTitle" type="text" class="form-control" id="inputTitle" placeholder="行程标题">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary copy-submit-btn">确定复制</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>


<div class="modal fade modal-create-trip" tabindex="-1" >
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">创建行程</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="create-trip-form" action="/trip/quickCreate" method="get">
                    <div class="form-group">
                        <label for="inputTitle">行程标题</label>
                        <input name="title" type="text" class="tripTitle form-control" id="inputTitle" placeholder="行程标题，如：普吉岛7天蜜月游">
                    </div>
                    <div class="form-group" style="display: none;">
                        <label for="inputTitle">类型</label>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="type" id="typeCustomer" value="customer" checked>
                                客户的行程
                            </label>
                        </div>
                        <div class="radio-inline">
                            <label>
                                <input type="radio" name="type" id="typeDemo" value="demo">
                                行程模板
                            </label>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary create-trip-submit-btn">确定创建</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('partner/trip_list');
    });
</script>
