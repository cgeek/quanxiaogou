<link rel="stylesheet" type="text/css" href="/css/plan.css">
<div class="plan-create-header">
	<div class="container" style="width: 100%;">
		<div class="plan-create-hd clearfix">
			<h2 class="title">创建行程</h2>
			<div class="list">
				<span class="list_number"><span>1</span>选择国家</span>
				<span class="list_gt"></span>
				<span class="list_number current"><span>2</span>选择城市</span>
				<span class="list_gt"></span>
				<span class="list_number"><span>3</span>城市排序及天数调整</span>
			</div>
		</div>
	</div>
</div>
<div class="plan-found-content">
	<div class="container" style="width: 100%;">
		<!--div class="found_search">
			<input type="text" class="ui_input" id="addcity" value="" placeholder="请在这里搜索一个想去的国家或城市" data-bn-ipg="plancreate-country-search">
			<!--搜索联想框 -->
    		<div class="found_layer" id="plan_search_drop" style="display: none;"></div>
		</div-->
		<div class="found_text_title fontYaHei" id="_jswordtitle"></div>
		<div class="found_city clearfix">
			<div class="item" id="_jscitylist"></div>
		</div>
		<div class="found_more" id="_jscityloading" style="display:none">
			<span class="text"><span>正在加载中</span></span>
		</div>

	</div>
</div>


<div class="pl_found_mask" style="display:none;">
    <div class="pl_found_mask_cnt">
		<div class="fontYaHei clearfix list_two_hang _jschoosetags" style="display: none;"></div>
		<!--没有城市显示层 -->
    	<div class="content_text content_text _jsnochoosetags">
		请从上边添加几个你想去的城市
		</div>
		<!--不可点状态 添加 class button_disabled -->
		<a href="javascript:void(0);" class="button_prev fl" id="_jstostep1" onfocus="this.blur();">选择其它国家</a>
	    <a href="javascript:void(0);" class="button fr button_disabled" id="_jstostep3" onfocus="this.blur();" data-bn-ipg="plancreate-bottom-next">下一步</a>
	</div>
</div>

<div class="pl_found_move" id="_jsmiddleware" style="display:none">
</div>

<script type="text/template" id="city-list-tpl">
		{{#city_list}}
				<div class="list">
					<div class="img">
						<img src="{{cover_image}}" width="200" height="143" alt="">
					</div>
					<div class="title">
						<a href="/place/{{id}}" class="name fl" target="_blank" data-bn-ipg="plancreate-countryname-1">{{name}}</a>
						<a href="javascript:;" class="ui_btn_smallB fr add addnewcity" data-cityname="{{name}}" data-cityid="{{id}}" data-bn-ipg="plancreate-cityselect-2">选择</a>
					</div>
				</div>
		{{/city_list}}
</script>


<script>
seajs.use('/assets/trip/router.js', function(router){
	window.countryInfo = {type: 1, id: <?=$country['id'];?>, name: '<?=$country['name'];?>'};
	router.load('/step/step2');
});
</script>
