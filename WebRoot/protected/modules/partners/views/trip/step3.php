<link rel="stylesheet" type="text/css" href="/css/plan.css">

<div class="plan-create-header">
	<div class="">
		<div class="plan-create-hd clearfix">
			<h2 class="title">创建行程</h2>
			<div class="list">
				<span class="list_number"><span>1</span>选择国家</span>
				<span class="list_gt"></span>
				<span class="list_number"><span>2</span>选择城市</span>
				<span class="list_gt"></span>
				<span class="list_number current"><span>3</span>城市排序及天数调整</span>
			</div>
		</div>
	</div>
</div>
<div class="plan-found-content">
	<div class="container" style="width:100%;">
		<div class="found_sort_cnt">
			<div class="clearfix date">
				<span class="fl fontYaHei">预计出行日期</span>
				<input name="datepicker" type="text" readonly="readonly" id="datepicker" class="ui_input" placeholder="选择日期" data-bn-ipg="plancreate-selectdate"/>
			</div>
			<div class="city_input clearfix">
				<span class="titles fontYaHei">出发城市</span>
				<div class="input">
					<input type="text" class="ui2_input" placeholder="请输入出发城市" value="" id="plans_step3_startcity">
					<!--联想框 -->
            	    <div class="mask" id="plans_step3_startcity_list"></div>
				</div>
			</div>
			<div class="item clearfix ui-sortable" id="jsplan_citylist_step3">
			</div>
			<!--div class="add_city">
				<a href="javascript:void(0);" class="link" id="jsplans_addnewcity">+ 添加一个新城市</a>
				<input type="text" class="ui2_input" placeholder="" value="" id="jsplans_searchaddcity" style="display:none;">
				<!--搜索联想框 -->
                <div class="found_layer" id="plan_search_drop" style="display:none;"></div>
			</div-->
			<div class="city_input clearfix z32">
				<span class="titles fontYaHei">返回城市</span>
				<div class="input">
					<input type="text" class="ui2_input" placeholder="请输入返回城市" value="" id="plans_step3_endcity">
					<div class="mask" id="plans_step3_endcity_list"></div>
				</div>
			</div>
		</div>
		<div class="found_sort_map">
			<script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=false"></script>  

			<div id="gmap" style="width:100%; height:100%;"></div>
		</div>
	</div>
</div>


<div class="pl_found_mask">
	<div class="pl_found_mask_cnt">
		<a href="javascript:window.history.go(-1);" class="button_up fl" onfocus="this.blur();" data-bn-ipg="plancreate-bottom-last">上一步</a>
		<a href="javascript:void(0);" onfocus="this.blur();" id="jsplan_step3_gonext" class="action_button fr" data-bn-ipg="plancreate-bottom-startedit">开始创建行程</a>
		<p class="fr f16 all_p" id="jsplan_daystips"></p>
	</div>
</div>

<script type="text/template" id="step3-city-list-tpl">
				<div class="list" data-cid="c1" title="可以拖拽排序">
					<strong>吉隆坡</strong>
					<div class="select">
						<span class="reduce" title="减" data-bn-ipg="plancreate-bottom-delete"></span>
						<span class="number">2天</span>
						<span class="add" title="加"></span>
					</div>
					<span class="close" title="删除城市"></span>
				</div>
</script>


	
<script type="text/template" id="tpl_searchlivecity">
	<ul>{{#list}}<li><a class="{{current}}" href="javascript:void(0)" data-title="{{name}}" data-type="{{type}}" data-dataid="{{id}}"><span>{{name}}（{{country_name}}）</span></a></li>{{/list}}</ul>
</script>

<script>
seajs.use('/assets/trip/router.js', function(router){
	router.load('/step/step3');
});
</script>
