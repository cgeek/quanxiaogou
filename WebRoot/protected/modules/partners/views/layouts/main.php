<!DOCTYPE html>
<html>
<head>
    <title>妙妙行程管家</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="language" content="zh" />
    <meta name="description" content="妙妙行程管家" />
    <!--link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" /-->
    <link href="/assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />

    <link href="/assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
    <link href="/assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />

    <link href="/assets/global/css/profile.min.css" rel="stylesheet" type="text/css" />

    <link href="/assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
    <link href="/assets/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
    <link href="/assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" type="text/css" href="/css/partner.css?v=1.2">

    <script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/seajs/dist/sea.js"></script>
    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="../../assets/libs/html5shiv.js"></script>
    <script src="../../assets/libs/respond.min.js"></script>
    <![endif]-->


    <script src="/assets/global/plugins/jquery.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
    <script src="/assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>


    <script src="/assets/global/scripts/app.min.js" type="text/javascript"></script>

    <script src="/assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
    <script src="/assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>

    <style>
        @media (max-width: 767px)
            .page-header.navbar {
                height:auto;
            }
    </style>
</head>
<body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">

    <div class="page-header navbar navbar-fixed-top" style="height:auto;">
        <div class="page-header-inner ">
            <div class="page-logo">
                <a href="/">
                    <img src="/images/plan/trip-logo-green.png" alt="logo" width="120px" class="logo-default" /> </a>
                <!--div class="menu-toggler sidebar-toggler"></div-->
            </div>
            <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>

            <div class="page-top hidden-xs">
                <?php if( ($this->id == 'trip') || ($this->id == 'customer')  || ($this->id == 'note') || ($this->id == 'poi')):?>
                <form class="search-form search-form-expanded" action="<?php if($this->id == 'customer'):?>/customer/<?php elseif($this->id == 'trip'):?>/trip/<?php endif;?>" method="GET">
                    <div class="input-group">
                        <?php if($this->id == 'trip' && isset($_GET['type']) ):?>
                            <input type="hidden" class="form-control"  name="type" value="<?=$_GET['type'];?>">
                        <?php endif;?>
                        <input type="text" class="form-control"
                               placeholder="<?php if($this->id == 'customer'):?>搜索客户...
                               <?php elseif($this->id == 'trip'):?>搜索行程...
                               <?php elseif($this->id == 'note'):?>搜索攻略...
                               <?php elseif($this->id == 'poi'):?>搜索地点...
                               <?php endif;?>" name="query" value="<?=isset($_GET['query']) ? $_GET['query'] :'';?>">
                        <span class="input-group-btn">
                            <a href="javascript:;" class="btn submit">
                                <i class="icon-magnifier"></i>
                            </a>
                        </span>
                    </div>
                </form>
                <?php endif;?>
                <div class="top-menu">
                    <ul class="nav navbar-nav pull-right">


                        <li class="dropdown dropdown-user">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">

                                <span class="username username-hide-on-mobile"> <?=Yii::app()->partner->nick_name;?> </span>
                                <i class="fa fa-angle-down"></i>
                            </a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li>
                                    <a href="/user/settings">
                                        <i class="icon-user"></i> 个人设置 </a>
                                </li>
                                <li class="divider"> </li>
                                <li>
                                    <a href="/user/logout">
                                        <i class="icon-key"></i> 退出
                                    </a>
                                </li>
                            </ul>
                        </li>

                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="clearfix"> </div>

    <div class="page-container">
        <?php echo $content; ?>
    </div>


    <script type='text/javascript'>
        (function(m, ei, q, i, a, j, s) {
            m[i] = m[i] || function() {
                (m[i].a = m[i].a || []).push(arguments)
            };
            j = ei.createElement(q),
                s = ei.getElementsByTagName(q)[0];
            j.async = true;
            j.charset = 'UTF-8';
            j.src = '//static.meiqia.com/dist/meiqia.js';
            s.parentNode.insertBefore(j, s);
        })(window, document, 'script', '_MEIQIA');
        _MEIQIA('entId', 22383);
    </script>

    <div style="display:none;">
        <script type="text/javascript">
            var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
            document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F53f78afdb90bf194696a6823b9c5aac8' type='text/javascript'%3E%3C/script%3E"));
        </script>
    </div>
</body>
</html>
