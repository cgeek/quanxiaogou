<?php $this->beginContent('/layouts/main'); ?>


<div class="page-sidebar-wrapper">
    <div class="page-sidebar navbar-collapse collapse" style="position: fixed; z-index:9999;">
        <div class="portlet profile-sidebar-portlet hidden-xs">
            <a href="/user/settings">
            <div class="profile-userpic">

                <img src="<?=(Yii::app()->partner->company_logo == '') ? '/images/no-avatar.jpg': Yii::app()->partner->company_logo;?>" style="max-width:60px;" class="img-responsive" alt="">
            </div>
            <div class="profile-usertitle">

                    <div class="profile-usertitle-name" style="font-size:15px;"><?=(Yii::app()->partner->company == '') ? '公司名字': Yii::app()->partner->company;?></div>

            </div>
            </a>
        </div>

        <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-compact" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">

            <li class="nav-item <?php if($this->id == 'trip') :?>active open<?php endif;?> ">
                <a href="/trip/" class="nav-link nav-toggle">
                    <i class="icon-layers"></i>
                    <span class="title">行程</span>
                    <span class="arrow"></span>
                </a>

            </li>
            <li class="nav-item <?php if($this->id == 'customer') :?>active open<?php endif;?>">
                <a href="/customer/" class="nav-link nav-toggle">
                    <i class="icon-users"></i>
                    <span class="title">客户</span>
                    <span class="arrow"></span>
                </a>

            </li>
            <!--li class="nav-item <?php if($this->id == 'note') :?>active open<?php endif;?>">
                <a href="/note/" class="nav-link nav-toggle">
                    <i class="icon-docs"></i>
                    <span class="title">攻略</span>
                    <span class="arrow"></span>
                </a>
            </li-->
            <li class="nav-item <?php if($this->id == 'poi') :?>active open<?php endif;?> ">
                <a href="/poi/" class="nav-link nav-toggle">
                    <i class="icon-pointer"></i>
                    <span class="title">地点</span>
                    <span class="arrow"></span>
                </a>
            </li>
        </ul>
    </div>
</div>

<div class="page-content-wrapper">
    <div class="page-content">
        <?php echo $content; ?>
    </div>
</div>


<?php $this->endContent(); ?>


