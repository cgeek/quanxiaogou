<?php

class AccountController extends Controller
{
	public $_identity;

	private $_data;

	public function actionSign()
	{
		$post = file_get_contents("php://input");
		if(empty($post)) {
			ajax_response('404', '');
		}
		$data = json_decode($post, TRUE);

		$username = $data['username'];
		$password = $data['password'];
		if(empty($username) || empty($password)) {
			$this->ajax_response(false, "用户名或者密码不能为空");
		}
		if($this->_identity===null)
		{
			$this->_identity=new PartnerIdentity($username, $password);
			$this->_identity->authenticate();
		}

		if($this->_identity->errorCode === UserIdentity::ERROR_NONE)
		{
			$rememberMe = Yii::app()->request->getParam('remember');
			$duration= ($rememberMe == 1) ? 3600*24*30 : 0; // 30 days
			Yii::app()->partner->login($this->_identity,$duration);
			//update last login time
			Partner::model()->updateByPk($this->_identity->id, array('last_login_time' => new CDbExpression('NOW()'))); 
			$this->ajax_response(200, "恭喜你，登录成功！");
		} else {
			if($this->_identity->errorCode === UserIdentity::ERROR_PASSWORD_INVALID) {
				$this->ajax_response(501, "密码不正确，请重新输入");
			} else if($this->_identity->errorCode === UserIdentity::ERROR_USERNAME_INVALID) { 
				$this->ajax_response(502, "用户名不存在，请重新输入");
			}
		}

		ajax_response('500', '登录失败', '');
	}

	public function actionSignout()
	{
		Yii::app()->partner->logout(false);
		$referrer = Yii::app()->request->getUrlReferrer();
		$referrer = !empty($referrer) ? $referrer : '/';
		$this->redirect($referrer);

	}


    public function actionLogin()
    {
        if(!Yii::app()->user->isGuest) {
            $this->redirect('/user/dashboard');
        }

        if(Yii::app()->request->isAjaxRequest) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            if(empty($username) || empty($password)) {
                ajax_response(false, "用户名或者密码不能为空");
            }
            if($this->_identity===null)
            {
                $this->_identity=new UserIdentity($username, $password);
                $this->_identity->authenticate();
            }
            if($this->_identity->errorCode === UserIdentity::ERROR_NONE)
            {
                $rememberMe = Yii::app()->request->getParam('remember');
                $duration= ($rememberMe == 1) ? 3600*24*30 : 0; // 30 days
                Yii::app()->user->login($this->_identity,$duration);
                //update last login time
                User::model()->updateByPk($this->_identity->id, array('last_login_time' => new CDbExpression('NOW()')));

                ajax_response(200, "恭喜你，登录成功！");
            } else {
                if($this->_identity->errorCode === UserIdentity::ERROR_PASSWORD_INVALID) {
                    ajax_response(500, "密码不正确，请重新输入");
                } else if($this->_identity->errorCode === UserIdentity::ERROR_USERNAME_INVALID) {
                    ajax_response(501, "用户名不存在，请重新输入");
                }
            }
        } else {
            $this->renderPartial('login');
        }

    }


    public function actionLogout()
    {
        Yii::app()->user->logout(false);
        $referrer = Yii::app()->request->getUrlReferrer();
        $referrer = !empty($referrer) ? $referrer : '/';
        $this->redirect($referrer);
    }

}
