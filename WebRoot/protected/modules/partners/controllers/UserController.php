<?php
/**
 * Created by PhpStorm.
 * User: cgeek
 * Date: 8/16/16
 * Time: 11:00 AM
 */

class UserController extends Controller
{
    public $_identity;
    public $_data;
    public $layout = '/layouts/column2';



    public function beforeAction($action)
    {
        $login_user_id = Yii::app()->partner->id;
        if(intval($login_user_id) <= 0 &&  $this->action->id != 'login' &&  $this->action->id != 'logout') {
            $this->redirect('/');
        }
        return parent::beforeAction($action);
    }


    public function actionIndex()
    {
        $this->pageTitle = '行程列表';

        $login_user_id = Yii::app()->partner->id;

        $trip_list = array();
        $pageSize = 3;
        $page = isset($_GET['page']) ? intval($_GET['page']) : 1;
        $offset = ($page - 1) * $pageSize;
        $criteria = new CDbCriteria;
        $criteria->offset = $offset;
        $criteria->limit = $pageSize;
        $criteria->addCondition("status>=0");
        $criteria->addCondition("user_id=$login_user_id");
        $criteria->order = ' `ctime` DESC';
        $trips = Trip::model()->findAll($criteria);
        if(!empty($trips)) {
            foreach($trips as $trip) {
                $trip = $trip->attributes;
                if($trip['customer_id'] > 0) {
                    $customer_db = Customer::model()->findByPk($trip['customer_id']);
                    $trip['customer_name'] = $customer_db['contact'];
                }
                $trip_list[] = $trip;
            }
        }
        $this->_data['trip_list'] = $trip_list;



        $customer_list = array();
        $pageSize = 3;
        $page = isset($_GET['page']) ? intval($_GET['page']) : 1;
        $offset = ($page - 1) * $pageSize;

        $criteria = new CDbCriteria;
        $criteria->offset = $offset;
        $criteria->limit = $pageSize;
        $criteria->order = 'mtime desc';
        $criteria->addCondition("user_id={$login_user_id}");
        $criteria->addCondition("status >= 0");
        $customers = Customer::model()->findAll($criteria);
        foreach($customers as $customer)
        {
            $customer = $customer->attributes;
            $customer_list[] = $customer;
        }

        $this->_data['customer_list'] = $customer_list;

        $this->render('index', $this->_data);
    }

    public function actionChangePassword()
    {
        $this->pageTitle = "修改密码";

        if(Yii::app()->partner->isGuest) {
            $this->redirect('/');
        }

        if(Yii::app()->request->isAjaxRequest) {
            $old_password = Yii::app()->request->getParam('old_password');
            $new_password = Yii::app()->request->getParam('new_password');

            if(empty($old_password) || empty($new_password)) {
                ajax_response('404', '参数不正确');
            }
            $user_id = Yii::app()->partner->id;
            $user = Partner::model()->findByPk($user_id);
            if(empty($user)) {
                ajax_response('500', '用户不存在或者未登录');
            }
            $user = $user->attributes;
            if(md5($old_password) != $user['password']) {
                ajax_response('500', '原密码错误');
            }

            $update['password'] = md5($new_password);
            Partner::model()->updateByPk($user_id, $update);

            ajax_response('200', '修改成功');

        } else {
            $user_id = Yii::app()->partner->id;
            $user = Partner::model()->findByPk($user_id);
            if(empty($user)) {
                echo 'no admin'; die();
            }
            $this->_data['admin'] = $user->attributes;
            $this->render('changePassword', $this->_data);
        }
    }

    public function actionSettings()
    {
        $this->pageTitle = "修改信息";

        if(Yii::app()->partner->isGuest) {
            $this->redirect('/');
        }



        if(Yii::app()->request->isAjaxRequest) {
            $nick_name = Yii::app()->request->getParam('nick_name');
            $email = Yii::app()->request->getParam('email');
            $company = Yii::app()->request->getParam('company');
            $company_intro = Yii::app()->request->getParam('company_intro');
            $mobile = Yii::app()->request->getParam('mobile');
            $weixin = Yii::app()->request->getParam('weixin');
            $qq = Yii::app()->request->getParam('qq');

            if(empty($nick_name)) {
                ajax_response('404', '参数不正确');
            }

            $login_user_id  = Yii::app()->partner->id;
            $user = Partner::model()->findByPk($login_user_id);
            if(empty($user)) {
                ajax_response('500', '用户不存在或者未登录');
            }
            $user = $user->attributes;

            $update['nick_name'] = $nick_name;
            $update['mobile'] = $mobile;
            $update['email'] = $email;
            $update['qq'] = $qq;
            $update['weixin'] = $weixin;
            $update['company'] = $company;
            $update['company_intro'] = $company_intro;

            Partner::model()->updateByPk($login_user_id, $update);


            $login_user = Partner::model()->findByPk($login_user_id)->attributes;
            unset($login_user['password']);
            Yii::app()->partner->setState('__userInfo',$login_user);

            ajax_response('200', '修改成功');

        } else {
            $login_user_id = Yii::app()->partner->id;
            $user = Partner::model()->findByPk($login_user_id);
            if(empty($user)) {
                echo 'no user'; die();
            }
            $this->_data['user'] = $user->attributes;

            $this->render('settings', $this->_data);
        }


    }
    
    public function actionLogin()
    {

        if(Yii::app()->request->isAjaxRequest) {
            $username = $_POST['username'];
            $password = $_POST['password'];
            if(empty($username) || empty($password)) {
                ajax_response(false, "用户名或者密码不能为空");
            }
            if($this->_identity===null)
            {
                $this->_identity=new PartnerIdentity($username, $password);
                $this->_identity->authenticate();
            }
            if($this->_identity->errorCode === PartnerIdentity::ERROR_NONE)
            {
                $rememberMe = Yii::app()->request->getParam('remember');
                $duration= ($rememberMe == 1) ? 3600*24*30 : 0; // 30 days
                Yii::app()->partner->login($this->_identity,$duration);
                //update last login time
                User::model()->updateByPk($this->_identity->id, array('last_login_time' => new CDbExpression('NOW()')));

                ajax_response(200, "恭喜你，登录成功！");
            } else {
                if($this->_identity->errorCode === PartnerIdentity::ERROR_PASSWORD_INVALID) {
                    ajax_response(500, "密码不正确，请重新输入");
                } else if($this->_identity->errorCode === PartnerIdentity::ERROR_USERNAME_INVALID) {
                    ajax_response(501, "用户名不存在，请重新输入");
                }
            }
        } else {
            if(!Yii::app()->partner->isGuest) {
                $this->redirect('/trip/');
            }
            $this->renderPartial('login');
        }
    }


    public function actionLogout()
    {
        Yii::app()->partner->logout(false);
        $referrer = Yii::app()->request->getUrlReferrer();
        $referrer = !empty($referrer) ? $referrer : '/';
        $this->redirect('/');
    }

}
