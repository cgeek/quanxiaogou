<?php
/**
 * Created by PhpStorm.
 * User: cgeek
 * Date: 8/16/16
 * Time: 11:00 AM
 */

class PoiController extends Controller
{
    public $_identity;
    public $_data;
    public $layout = '/layouts/column2';

    public function beforeAction($action)
    {
        $login_user_id = Yii::app()->partner->id;
        if(intval($login_user_id) <= 0) {
            $this->redirect('/');
        }
        return parent::beforeAction($action);
    }

    public function actionIndex()
    {
        $this->pageTitle = 'POI列表';

        $login_user_id = Yii::app()->partner->id;

        $type = Yii::app()->request->getParam('type');
        $page = Yii::app()->request->getParam('page');
        $city_id = isset($_GET['city']) ? intval($_GET['city']) : 0;

        $poi_list = array();

        $pageSize = 20;
        $page = !empty($page) ? intval($page) : 1;
        $offset = ($page - 1) * $pageSize;

        $criteria = new CDbCriteria;
        $criteria->offset = $offset;
        $criteria->limit = $pageSize;
        $criteria->addCondition("user_id=$login_user_id");

        if($city_id){
            $criteria->addCondition("city_id=$city_id");
        }


        $criteria->addCondition("status=0");

        $criteria->order = ' `ctime` DESC';

        $count = Poi::model()->count($criteria);
        $pois = Poi::model()->findAll($criteria);
        if(!empty($pois)) {
            foreach($pois as $poi) {
                $poi = $poi->attributes;
                $poi['cover_image_url'] = upimage($poi['cover_image'], 'w240h240');
                $poi['cover_image'] = $this->_getImageIdFromURL($poi['cover_image']);
                $poi_list[] = $poi;
            }
        }
        $this->_data['poi_list'] = $poi_list;



        //分页
        $pages=new CPagination($count);
        $pages->pageSize= $pageSize;
        $pages->pageVar = 'page';
        $pages->applyLimit($criteria);
        $this->_data['pages'] = $pages;
        $this->_data['count'] = $count;

        $this->render('index', $this->_data);
    }


    public function actionNew(){
        $this->pageTitle = '新增POI';

        $poi = array();
        $poi['poi_id'] = 0;

        if(isset($_GET['city_id'])) {
            $city = City::model()->findByPk(intval($_GET['city_id']));
            if(!empty($city)) {
                $poi['city_id'] = $city->id;
                $poi['city_name'] = $city->name;
            }
        }

        $this->_data['poi'] = $poi;

        $type_list = $this->_getTypeList();
        $this->_data['type_list'] = $type_list;

        //var_dump($poi);die();
        $this->render('edit', $this->_data);
    }

    public function actionEdit(){
        $this->pageTitle = '编辑POI';
        $poi_id = Yii::app()->request->getParam('id');

        $poi = Poi::model()->findByPk($poi_id);
        if(empty($poi)) {
            throw new CHttpException('404', '找不到POI');
        }

        $poi = $poi->attributes;

        $poi['cover_image_url'] = $poi['cover_image'];
        //$poi['cover_image'] = $this->_getImageIdFromURL($poi['cover_image']);
        //get images

        $this->_data['poi'] = $poi;



        $city_list = $this->_getCityList($poi['city_id']);
        $this->_data['city_list'] = $city_list;

        $type_list = $this->_getTypeList($poi['type']);
        $this->_data['type_list'] = $type_list;

        $this->render('edit', $this->_data);
    }

    public function actionSavePoi(){
        $login_user_id = Yii::app()->partner->id;

        $accept_fields = array('poi_id', 'poi_name', 'cover_image', 'cover_image_before'
        , 'city_id', 'city_name','poi_name_english', 'poi_name_pinyin', 'poi_name_local', 'type', 'recommend'
        , 'desc', 'html_content', 'phone', 'website', 'address', 'address_local', 'price', 'tips'
        , 'rank', 'rating', 'intro', 'tags', 'open_time', 'traffic', 'is_private', 'source_url', 'lat', 'lon', 'area');
        $data = array();
        $reset_cover_image = 0;
        if(isset($_POST['cover_image']) && isset($_POST['cover_image_before']) && $_POST['cover_image'] != $_POST['cover_image_before']){
            $reset_cover_image = 1;
        }
        foreach($accept_fields as $field) {
            if(isset($_POST[$field])) {
                $data[$field] = Yii::app()->request->getParam("{$field}");
            }
        }
        if(!isset($data['is_private']) || empty($data['is_private']))
        {
            $data['is_private'] = 0;
        }
        if(!empty($data['city_id'])) {
            $city = City::model()->findByPk($data['city_id']);
            if(!empty($city)) {
                $data['city_id'] = $data['city_id'];
                $data['city_name'] = $city['name'];
                $data['country_id'] = $city['country_id'];
            }
        } elseif(!empty($data['city_name'])) {
            $city = City::model()->findByAttributes(array('name' =>$data['city_name']));
            if(!empty($city)) {
                $data['city_id'] = $city['id'];
                $data['city_name'] = $city['name'];
                $data['country_id'] = $city['country_id'];
            }
        }

        if(!empty($data['poi_id'])) {
            $poi = Poi::model()->findByPk($data['poi_id']);
            if(empty($poi)) {
                ajax_response(404, '找不到POI信息');
            }
            Poi::model()->updateByPk($data['poi_id'], $data);
            ajax_response(200, '');
        } else {
            $new_poi = new Poi;
            $new_poi->user_id = $login_user_id;
            $new_poi->city_id = $data['city_id'];
            $new_poi->country_id = $data['country_id'];
            $new_poi->city_name = $data['city_name'];
            $new_poi->poi_name = $data['poi_name'];
            $new_poi->type = $data['type'];
            $new_poi->poi_name_local = $data['poi_name_local'];
            $new_poi->poi_name_pinyin = $data['poi_name_pinyin'];
            $new_poi->poi_name_english = $data['poi_name_english'];
            $new_poi->cover_image = $data['cover_image'];
            $new_poi->desc = $data['desc'];
            $new_poi->html_content = $data['html_content'];
            $new_poi->phone = $data['phone'];
            $new_poi->website = $data['website'];
            $new_poi->address = $data['address'];
            $new_poi->address_local = $data['address_local'];
            $new_poi->price = $data['price'];
            $new_poi->rank = $data['rank'];
            $new_poi->rating = $data['rating'];
            $new_poi->intro = $data['intro'];
            $new_poi->tips = $data['tips'];
            $new_poi->tags = $data['tags'];
            $new_poi->open_time = $data['open_time'];
            $new_poi->traffic = $data['traffic'];
            $new_poi->lat = $data['lat'];
            $new_poi->lon = $data['lon'];
            $new_poi->area = $data['area'];
            $new_poi->recommend = $data['recommend'];
            $new_poi->source_url = $data['source_url'];
            $new_poi->status = 0;
            $new_poi->mtime = date('Y-m-d H:i:s', time());
            $new_poi->ctime = date('Y-m-d H:i:s', time());

            if($new_poi->save()) {
                $new_poi_id = Yii::app()->db->getLastInsertId();
                ajax_response(200,'',array('new_poi' => 'ok', 'poi_id' => $new_poi_id));
            } else {
                var_dump($new_poi->getErrors());
                ajax_response(500,'添加失败');
            }
        }
    }


    public function actionDeletePoi(){
        $poi_id = Yii::app()->request->getParam('poi_id');
        $poi_db = Poi::model()->findByPk($poi_id);
        if(empty($poi_db)) {
            ajax_response('404', '找不到POI');
        }
        //check user primsion;
        if($poi_db->user_id != Yii::app()->partner->id) {
            ajax_response('500', '权限不正确');
        }
        $poi_db->status = '-1';
        if($poi_db->save()) {
            ajax_response('200', '');
        } else {
            ajax_response('500', '删除失败');
        }
    }

    private function _getCityList($currentCity = 0){
        $city_list = array();
        $criteriaCity = new CDbCriteria;
        $criteriaCity->addCondition("status=1");
        $cities = City::model()->findAll($criteriaCity);
        if(!empty($cities)) {
            foreach($cities as $city){
                $city = $city->attributes;
                if($currentCity == $city['id']){
                    $city['selected'] = ' selected';
                }else{
                    $city['selected'] = '';
                }
                $city_list[] = $city;
            }
        }
        return $city_list;
    }

    private function _getTypeList($currentType = ''){
        if(empty($currentType))
        {
            $currentType = 'sight';
        }
        $type_list_temp = array();
        //$type_list_temp['hotel'] = '酒店';
        $type_list_temp['sight'] = '景点';
        $type_list_temp['restaurant'] = '美食';
        $type_list_temp['shopping'] = '购物';
        $type_list_temp['entertainment'] = '娱乐';
        //$type_list_temp['traffic'] = '交通';
        $type_list = array();
        reset($type_list_temp);
        while (list($key, $val) = each($type_list_temp)) {
            $type = array();
            $type['type'] = $key;
            $type['name'] = $val;
            if($currentType == $key){
                $type['selected'] = ' selected';
            }else{
                $type['selected'] = '';
            }
            $type_list[] = $type;
        }
        return $type_list;
    }

    private function _getImageIdFromURL($url){
        $return_value = $url;
        //如果是妙妙行程管家下面的截取image_id
        if(strpos($url, 'http://xiaogoulvxing.b0.upaiyun.com/')){
            if(preg_match("/^http:\/\/?[^\/]+\/([^\_]*)/i",$url, $matches)){
                $return_value = $matches[1];
            }
        }
        return $return_value;
    }

}
