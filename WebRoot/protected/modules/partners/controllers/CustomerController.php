<?php

Yii::import('ext.Notify',true);

class CustomerController extends Controller
{
	private $_data;
	public $layout = '/layouts/column2';

	public function actionIndex()
	{
		$this->pageTitle = '我的客户';
		$customer_list = array();

        $login_user_id = Yii::app()->partner->id;
        if(intval($login_user_id) <= 0) {
            $this->redirect('/');
        }

		$pageSize = 20;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$offset = ($page - 1) * $pageSize;

        $keyword = Yii::app()->request->getParam('query');

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
		$criteria->order = 'mtime desc';
		$criteria->addCondition("user_id={$login_user_id}");

        $criteria->addCondition("status >= 0");

        if(!empty($keyword)){
            $criteria->addCondition("(contact like '%{$keyword}%')");
            $this->_data['keyword'] = $keyword;
        }

		$count = Customer::model()->count($criteria);
		$customers = Customer::model()->findAll($criteria);
		if(!empty($customers)) {
			foreach($customers as $customer) 
			{
				$customer = $customer->attributes;
                if(!empty($customer['user_id'])) {
                    $user_db = Partner::model()->findByPk($customer['user_id']);
                    if(!empty($user_db)) {
                        $customer['user'] = $user_db->attributes;
                    }
                }
				$customer_list[] = $customer;
			}
		}

		$this->_data['customer_list'] = $customer_list;
		//ajax_response(200, '', $this->_data);

		//分页
		$pages=new CPagination($count);
		$pages->pageSize= $pageSize;
		$pages->pageVar = 'page';
		$pages->applyLimit($criteria);
		$this->_data['pages'] = $pages;
		$this->_data['count'] = $count;


		$this->render('/customer/index', $this->_data);
	}

	public function actionAdd()
	{
		$this->pageTitle = '添加客户';
		$this->render('edit');
	}

	public function actionEdit()
	{
		$this->pageTitle = '编辑客户信息';

		$id = Yii::app()->request->getParam('id');
		$customer = Customer::model()->findByPk($id);
		if(empty($customer)) {
			die('not exists');
		}
		$customer = $customer->attributes;
		$this->_data['customer'] = $customer;
		$this->render('edit', $this->_data);
	}


	public function actionDelete()
	{
		if(Yii::app()->request->isAjaxRequest) {
			$customer_id = Yii::app()->request->getParam('customer_id');
			$customer = Customer::model()->findByPk($customer_id);
			if(empty($customer)) {
				ajax_response('404', '找不到客户');
			}

			$customer->status = '-1';
			if($customer->save()) {
				ajax_response('200', '');
			} else {
				ajax_response('500', '删除失败');
			}
		}
	}

	public function actionUpdateStatus()
	{
		if(Yii::app()->request->isAjaxRequest) {
			$customer_id = Yii::app()->request->getParam('customer_id');
			$status = Yii::app()->request->getParam('status');
			if(empty($status)) {
				ajax_response('404', '参数不正确');
			}

			$customer = Customer::model()->findByPk($customer_id);
			if(empty($customer)) {
				ajax_response('404', '找不到客户');
            }

			$customer->status = $status;

			if($customer->save()) {

				ajax_response('200', '');
			} else {
				ajax_response('500', '删除失败');
			}

		}
	}


	public function actionSaveCustomer()
	{
		$accept_fields = array('customer_id', 'contact', 'origin', 'destination',  'sex', 'num_of_people', 'start_date', 'end_date', 'has_booked_airline_ticket', 'has_booked_hotel','no_booking','remark', 'mobile', 'qq', 'weixin', 'email', 'wangwang', 'journey_url', 'service_fee', 'taobao_trade_id', 'qq_log', 'platform', 'source', 'trip_id', 'admin_id', 'user_id');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}
		if(empty($data['has_booked_airline_ticket'])) {
			$data['has_booked_airline_ticket'] = 0;
		}
		if(empty($data['has_booked_hotel'])) {
			$data['has_booked_hotel'] = 0;
		}
		if(empty($data['no_booking'])) {
			$data['no_booking'] = 0;
		}

        if(empty($data['contact'])) {
            ajax_response(404,'姓名不能为空');
        }
		if(!empty($data['customer_id'])) {
			$customer_id = $data['customer_id'];
			unset($data['customer_id']);
			$this->_save_customer($customer_id, $data);
		} else {
			$this->_new_customer($data);
		}
	}

	private function _save_customer($customer_id, $data)
	{
		$customer_db = Customer::model()->findByPk($customer_id);
		if(empty($customer_db)) {
			ajax_response('404', '找不到该客户信息，无法保存');
		}
		
		Customer::model()->updateByPk($customer_id, $data);
		$this->_data['customer_id'] = $customer_id;
		ajax_response(200,'',$this->_data);
	}

	private function _new_customer($data)
	{
		$new_customer = new Customer;
		$new_customer->contact = $data['contact'];
		$new_customer->sex = $data['sex'];
		$new_customer->start_date = $data['start_date'];
		$new_customer->origin = $data['origin'];
		$new_customer->destination = $data['destination'];
		$new_customer->end_date = $data['end_date'];
		$new_customer->num_of_people = $data['num_of_people'];
		$new_customer->has_booked_airline_ticket = $data['has_booked_airline_ticket'];
		$new_customer->has_booked_hotel = $data['has_booked_hotel'];
		$new_customer->no_booking = $data['no_booking'];
		$new_customer->remark = $data['remark'];
		$new_customer->mobile = $data['mobile'];
		$new_customer->qq = $data['qq'];
		$new_customer->weixin = $data['weixin'];
		$new_customer->wangwang = $data['wangwang'];
		$new_customer->email = $data['email'];
		$new_customer->service_fee = empty($data['service_fee']) ? 0 : intval($data['service_fee']);
		$new_customer->taobao_trade_id = $data['taobao_trade_id'];
		$new_customer->journey_url = empty($data['journey_url']) ? '' : $data['journey_url'];

        if($data['user_id'] > 0) {
            $new_customer->user_id = $data['user_id'];
        } elseif (!empty(Yii::app()->partner->id)) {
			$new_customer->user_id = Yii::app()->partner->id;
		}

		$new_customer->ctime = date('Y-m-d H:i:s', time());
		$new_customer->status = 1;
		if($new_customer->save()) {
            $customer_id = Yii::app()->db->getLastInsertId();
            $this->_data['customer_id'] = $customer_id;

			ajax_response('200', '', $this->_data);
		} else {
			var_dump($new_customer->getErrors());
			ajax_response('500', '保存失败');
		}
	}

    public function actionCollect()
    {

        if(Yii::app()->request->isAjaxRequest) {
            $accept_fields = array('user_id', 'contact',  'sex', 'num_of_people','origin','destination',  'start_date', 'end_date', 'has_booked_airline_ticket', 'has_booked_hotel','no_booking','remark', 'mobile', 'qq', 'weixin', 'email', 'platform', 'source', 'weixin_uuid', 'uuid');
                $data = array();
                foreach($accept_fields as $field) {
                    if(isset($_POST[$field])) {
                        $data[$field] = Yii::app()->request->getParam("{$field}");
                    }
                }

                $this->_new_customer($data);

        } else {
            $partner_id = Yii::app()->request->getParam('pid');
            $partner = Partner::model()->findByPk($partner_id);
            if(empty($partner)) {
                die('not exists');
            }
            $partner = $partner->attributes;
            $this->_data['partner'] = $partner;
            $this->renderPartial('collect', $this->_data);
        }

    }


}
