<?php

class DefaultController extends Controller
{
	public function actionIndex()
	{
        if(Yii::app()->partner->id > 0) {
            $this->redirect('/trip/');
        }
		$this->renderPartial('index');
	}
}
