<?php

class InquiryController extends Controller
{
	public function actionPartnerUnread()
	{
		$post = file_get_contents("php://input");
		if(empty($post)) {
			ajax_response('404', '');
		}
		$data = json_decode($post, TRUE);

		$last_id = $data['last_id'];

		$inquiry_list = array();
		$criteria = new CDbCriteria;
		$criteria->offset = 0;
		$criteria->limit = 10;
		$criteria->addCondition("id > $last_id");
		$criteria->order = 'ctime desc';
		$inquirys = Inquiry::model()->findAll($criteria);
		if(!empty($inquirys)) {
			foreach($inquirys as $inquiry) {
				$inquiry = $inquiry->attributes;
				$inquiry['ctime'] = date('m-d H:i', strtotime($inquiry['ctime']));
				$inquiry['unread'] = true;
				$inquiry_list[] = $inquiry;
			}
			$r = array('new_count' => 1, 'inquiry_list' => $inquiry_list);
			ajax_response('200', '', $r);
		} else {
			ajax_response('200', '');
		}
	}

	public function actionCreate()
	{
		$post = file_get_contents("php://input");
		if(empty($post)) {
			ajax_response('404', '');
		}
		$data = json_decode($post, TRUE);
		if(empty($data) || empty($data['content'])) {
			ajax_response('404', '');
		}

		$content = $data['content'];
		$inquiry = new Inquiry;
		$inquiry->content = $content;
		$inquiry->ctime = date('Y-m-d H:i:s', time());
		$inquiry->mtime = date('Y-m-d H:i:s', time());

		if($inquiry->save()) {
			$id = Yii::app()->db->getLastInsertId();
			$r = Inquiry::model()->findByPk($id)->attributes;
			$r['ctime'] = date('m-d H:i', time()); 
			ajax_response('200', '', $r);
		} else {
			ajax_response('500', '保存失败');
		}
	}

	public function actionUpdate()
	{
		$post = file_get_contents("php://input");
		$data = json_decode($post, TRUE);
		if(!empty($data['id'])) {
			$id = $data['id'];
			$inquiry_db = Inquiry::model()->findByPk($id);
			if(!empty($inquiry_db)) {
				Inquiry::model()->updateByPk($id, $data);
				ajax_response(200, '保存成功');
			} else {
				ajax_response(500, '保存失败');
			}
		} else {
			ajax_response(500, '保存失败');
		}
	}

	public function actionList()
	{

		$inquiry_list = array();
		$criteria = new CDbCriteria;
		$criteria->offset = 0;
		$criteria->limit = 15;
		$criteria->order = ' ctime desc';
		$inquirys = Inquiry::model()->findAll($criteria);
		foreach($inquirys as $inquiry) {
			$inquiry = $inquiry->attributes;
			$inquiry['ctime'] = date('m-d H:i', strtotime($inquiry['ctime'])); 
			$inquiry_list[] = $inquiry;
		}
		ajax_response('200', '', $inquiry_list);
	}

}
