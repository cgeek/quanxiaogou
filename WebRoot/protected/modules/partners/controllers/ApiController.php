<?php

class ApiController extends Controller
{
	private $_data;

	public function actionIndex()
	{
		echo "api";
	}

	public function actionSearchHotel()
	{
		$keyword = Yii::app()->request->getParam('kw');

		if(empty($keyword)) {
			ajax_response(200,'');
		}
		$keyword = mysql_escape_string(trim($keyword));

		$hotel_list = array();

		$criteria = new CDbCriteria;
		$criteria->offset = 0;
		$criteria->limit = 15;
		$criteria->addCondition("title Like '$keyword%'", "OR");
		$criteria->addCondition("hotel_name_english Like '$keyword%'", "OR");
		$criteria->addCondition("hotel_name_pinyin Like '$keyword%'", "OR");
		$hotels = Hotel::model()->findAll($criteria);

		if(!empty($hotels)) {
			foreach($hotels as $key=>$hotel) {
				$hotel = $hotel->attributes;
				$hotel['hotel_id'] = $hotel['id'];
				$hotel['cover_image'] = upimage($hotel['cover_image'], 'org');
				$hotel_list[] = $hotel;
			}
		}
		$this->_data['hotel_list'] = $hotel_list;
		ajax_response(200, '', $this->_data);
	}


	public function actionSearchPoi()
	{
		$keyword = Yii::app()->request->getParam('kw');

		if(empty($keyword)) {
			ajax_response(200,'');
		}
		$keyword = mysql_escape_string(trim($keyword));

		$hotel_list = array();

		$criteria = new CDbCriteria;
		$criteria->offset = 0;
		$criteria->limit = 15;
        $criteria->order = 'qyer_commentcounts desc';
		$criteria->addCondition("poi_name Like '$keyword%'", "OR");
		$criteria->addCondition("poi_name_pinyin Like '$keyword%'", "OR");
		$pois = Poi::model()->findAll($criteria);

		if(!empty($pois)) {
			foreach($pois as $key=>$poi) {
				$poi = $poi->attributes;
				$poi['id'] = $poi['id'];
				$poi['cover_image'] = upimage($poi['cover_image'], 'org');
				$poi_list[] = $poi;
			}
		}
		$this->_data['poi_list'] = $poi_list;
		ajax_response(200, '', $this->_data);
	}


	public function actionSearchCity()
	{
		$keyword = Yii::app()->request->getParam('kw');
		$status = Yii::app()->request->getParam('status');

		if(empty($keyword)) {
			ajax_response(200,'');
		}
		$keyword = mysql_escape_string(trim($keyword));

		$city_list = array();
		$criteria = new CDbCriteria;
		$criteria->offset = 0;
		$criteria->limit = 15;
		if(!empty($status)) {
			$criteria->addCondition("status='{$status}'");
		}
		$criteria->addCondition("name Like '$keyword%'", "OR");
		$criteria->addCondition("name_english Like '$keyword%'", "OR");
		$criteria->addCondition("name_pinyin Like '$keyword%'", "OR");
		$citys = City::model()->findAll($criteria);
		if(!empty($citys)) {
			foreach($citys as $key=>$city) {
				$city = $city->attributes;
				$city['cover_image'] = upimage($city['cover_image'], 'org');
				$city = array(
					'id' => $city['id'],
					'name' => $city['name'],
					'name_english' => $city['name_english'],
					'type' => $city['type'],
					'cover_image' => $city['cover_image'],
					'country_name' => $city['country_name'],
				);
				if($key == 0) {
					$city['current'] = 'current';
				}
				$city_list[] = $city;
			}
		}
		$this->_data['list'] = $city_list;
		ajax_response(200, '', $this->_data);
	}

	public function actionGetCountryList()
	{
		$country_list = array();
		$continent_id = Yii::app()->request->getParam('continent_id');

		$page = Yii::app()->request->getParam('page');
		$page = intval($page) > 1 ? $page : 1;
		$limit = 10;
		$offset = ($page - 1) * $limit;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $limit;
		if(!empty($continent_id)) {
			$criteria->addCondition("continent_id='{$continent_id}'");
		}
		$countrys = Country::model()->findAll($criteria);
		if(!empty($countrys)) {
			foreach($countrys as $country) {
				$country = $country->attributes;
				$country['cover_image'] = upimage($country['cover_image'], 'org');
				$country_list[] = $country;
			}
		}
		$this->_data['country_list'] = $country_list;
		
		ajax_response(200, '', $this->_data);
	}

	public function actionGetCityList()
	{
		$country_id = Yii::app()->request->getParam('country_id');
		$status = Yii::app()->request->getParam('status');
		$city_list = array();
		if(empty($country_id)) {

		}
		$page = Yii::app()->request->getParam('page');
		$page = intval($page) > 1 ? $page : 1;
		$limit = 10;
		$offset = ($page - 1) * $limit;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $limit;

		if(!empty($status)) {
			$criteria->addCondition("status='{$status}'");
		}
		$criteria->addCondition("country_id='{$country_id}'");
		$count = City::model()->count($criteria);
		$citys = City::model()->findAll($criteria);
		if(!empty($citys)) {
			foreach($citys as $city) {
				$city = $city->attributes;
				$city['cover_image'] = upimage($city['cover_image'], 'org');
				$city_list[] = $city;
			}
		}
		$this->_data['city_list'] = $city_list;
		ajax_response(200, '', $this->_data);
	}

	public function actionGetCityInfos()
	{

		$this->_data['citys'] = array(
			'cid2' => array('cityname'=> '沙巴', 'cityname_en' => "Sabah", 'lat' => '18.77.5663', 'lng'=> '98.923729', 'fundays'=>5)
		);
		$this->_data['countrys'] = array('2');
		ajax_response(200, '', $this->_data);
	}

	public function actionFetchAllDays()
	{
		$trip_id = Yii::app()->request->getParam('trip_id');
		
		$trip_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("trip_id='{$trip_id}'");
		$criteria->addCondition("status >='0'");
		$criteria->order = 'sort asc';
		$tripDays = TripDay::model()->findAll($criteria);
		if(!empty($tripDays)) {
			foreach($tripDays as $day) {
				$result = array('xid' => $day['id']);
				$city_list = array();
				$city_list = json_decode($day['city_ids'], true);
				$result['city_list'] = $city_list;
				if(count($city_list)) {
					$result['has_city'] = TRUE;
				}
				$result['date'] = $day['date'];
				$trip_list[] = $result;
			}
		}
		ajax_response(200, '', $trip_list);
	}

	public function actionPoiList()
	{
		$city_id = Yii::app()->request->getParam('city_id');
		$type = Yii::app()->request->getParam('type');
		$type = empty($type) ? 'sight' : $type;

		$this->_data['type'] = $type;

		if($type == 'demo_trip_day') {
			$this->_getDemoTripDayList($city_id);
		} else if($type == 'hotel') {
			$this->_getHotelList($city_id);
		} else {
			$this->_getPoiList($city_id, $type);
		}
	}

    public function actionDemoList()
    {

        $city_id = Yii::app()->request->getParam('city_id');
        $this->_getDemoTripDayList($city_id);

    }

	public function actionNoteList()
	{

		$city_id = Yii::app()->request->getParam('city_id');
		$type = Yii::app()->request->getParam('type');
		//$type = empty($type) ? 'sight' : $type;

		$this->_getNoteList($city_id, $type);
	}

	private function _getNoteList($city_id = 0, $type = 'sight')
	{
		$note_list = array();
		$page = Yii::app()->request->getParam('page');
		$page = intval($page) > 1 ? $page : 1;
		$limit = 10;
		$offset = ($page - 1) * $limit;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $limit;
		if(!empty($city_id)) {
			$criteria->addCondition("city_id='{$city_id}'");
		}
		$count = Note::model()->count($criteria);
		$notes = Note::model()->findAll($criteria);
		if(!empty($notes)) {
			foreach($notes as $note) {
				$note = $note->attributes;
				$note['content'] = nl2br($note['content']);
				$note_list[] = $note;
			}
		}
		$this->_data['note_list'] = $note_list;
		ajax_response(200, '', $this->_data);
	}
	
	private function _getHotelList($city_id = 0)
	{
		if(empty($city_id)) {
			ajax_response('200', '');
		}
		$page = Yii::app()->request->getParam('page');
		$page = intval($page) > 1 ? $page : 1;
		$limit = 10;
		$offset = ($page - 1) * $limit;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $limit;
		$criteria->addCondition("city_id='{$city_id}'");
		$criteria->addCondition("status >= 0");
		$count = Hotel::model()->count($criteria);
		$pois = Hotel::model()->findAll($criteria);
		if(!empty($pois)) {
			foreach($pois as $poi) {
				$poi = $poi->attributes;
				$poi['is_hotel'] = true;
				$poi_list[] = $poi;
			}
		}
		$this->_data['poi_list'] = $poi_list;
		ajax_response(200, '', $this->_data);
	}

	private function _getPoiList($city_id = 0, $type = 'sight')
	{
		if(empty($city_id)) {
			ajax_response('200', '');
		}
		$page = Yii::app()->request->getParam('page');
        $searchkey = Yii::app()->request->getParam('searchkey');
		$page = intval($page) > 1 ? $page : 1;
		$limit = 10;
		$offset = ($page - 1) * $limit;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $limit;
        $criteria->order = 'qyer_commentcounts desc';
        if($type == 'mine') {
            $login_user_id = Yii::app()->partner->id;
            $criteria->addCondition("user_id='{$login_user_id}'");
        } else {
            $criteria->addCondition("type='{$type}'");
            $criteria->addCondition("city_id='{$city_id}'");
        }

        if(!empty($searchkey)) {
            //echo $searchkey;die();
            $criteria->addSearchCondition("poi_name", "{$searchkey}");
        }

		$criteria->addCondition("status >= 0");
		$count = Poi::model()->count($criteria);
		$pois = Poi::model()->findAll($criteria);
		if(!empty($pois)) {
			foreach($pois as $poi) {
				$poi = $poi->attributes;
				$poi['cover_image'] = upimage($poi['cover_image']);
				$poi_list[] = $poi;
			}
		}

		$this->_data['poi_list'] = $poi_list;
		ajax_response(200, '', $this->_data);
	}

	public function actionAddEvent()
	{
		$trip_id = Yii::app()->request->getParam('trip_id');
		$trip_day_id = Yii::app()->request->getParam('trip_day_id');
		
		$type = Yii::app()->request->getParam('type');

		$note = Yii::app()->request->getParam('note');
		$currency = Yii::app()->request->getParam('currency');
		$spend = Yii::app()->request->getParam('spend');
		$ticketcount = Yii::app()->request->getParam('ticketcount');

		if($type == 'hotel') {
			$id = Yii::app()->request->getParam('id');
			$title  = Yii::app()->request->getParam('title');
			if(empty($title)) {
				ajax_response('404', '缺少参数');
			}
			$hotel_db = array();
			if(!empty($id)) {
				$hotel_db = Hotel::model()->findByPk($id);
			} else {
				//TODO 没有id的创建一个酒店, 需要city id
                $new_hotel = new Hotel;
                $new_hotel->city_id = 0;
                $new_hotel->country_id = 0;
                $new_hotel->city_name = '';
                $new_hotel->title = $title;
                $new_hotel->type = 'hotel';
                $new_hotel->ctime = date('Y-m-d H:i:s', time());
                $new_hotel->mtime = date('Y-m-d H:i:s', time());
                $new_hotel->status = 0;
                $new_hotel->save();

                $hotel_db = Hotel::model()->findByPk(Yii::app()->db->getLastInsertId());

			}
			if(empty($hotel_db)) {
				ajax_response('404', '找不到poi');
			}
			$hotel_db = $hotel_db->attributes;

			$inday = Yii::app()->request->getParam('in_day');
			$outday = Yii::app()->request->getParam('out_day');

			if(!empty($inday) && !empty($outday)) {
				$criteria = new CDbCriteria;
				$criteria->addCondition("trip_id='{$trip_id}'");
				$criteria->order = 'sort asc';
				$criteria->addCondition("status=0");
				$trip_day_list = TripDay::model()->findAll($criteria);

				$list = array();
				$inday_xid = $outday_xid = $xid = 0;
				if(!empty($trip_day_list)) {
					foreach($trip_day_list as $key => $trip_day) {
						$trip_day = $trip_day->attributes;
						if($trip_day['id'] == $inday) {
							$inday_xid = $key;
						}
						if($trip_day['id'] == $outday) {
							$outday_xid = $key;
						}
						$list[] = $trip_day;
					}
					if(empty($outday_xid)) {
						ajax_response('500', '添加失败');
					}
					foreach($list as $key=>$trip_day) {
						if($key < $inday_xid || $key >= $outday_xid) {
							continue;
						}
						$hotelEvent = new TripDayHotel;
						$hotelEvent->trip_id = $trip_id;
						$hotelEvent->trip_day_id = $trip_day['id'];
						$hotelEvent->hotel_id = $id;
						$hotelEvent->title = $hotel_db['title'];
						$hotelEvent->note = $note;
						$hotelEvent->spend = $spend;
						$hotelEvent->currency = $currency;
						$hotelEvent->ticketcount = $ticketcount;
						$hotelEvent->cover_image = $hotel_db['cover_image'];
						$hotelEvent->desc = $hotel_db['desc'];
						$hotelEvent->address = $hotel_db['address'];
						$hotelEvent->lat = $hotel_db['lat'];
						$hotelEvent->lon = $hotel_db['lon'];
						$hotelEvent->mtime = date('Y-m-d H:i:s' , time());
						$hotelEvent->ctime = date('Y-m-d H:i:s' , time());
						$hotelEvent->status = 0;
						$hotelEvent->save();
						if($trip_day['id'] == $trip_day_id) {
							$xid = Yii::app()->db->getLastInsertId();
						}
					}
					ajax_response('200', '', array('xid'=>$xid));
				}
				ajax_response('500', '添加失败');

			} else {
				$hotelEvent = new TripDayHotel;
				$hotelEvent->trip_id = $trip_id;
				$hotelEvent->trip_day_id = $trip_day_id;
				$hotelEvent->hotel_id = $id;
				$hotelEvent->title = $hotel_db['title'];
				$hotelEvent->note = $note;
				$hotelEvent->spend = $spend;
				$hotelEvent->currency = $currency;
				$hotelEvent->ticketcount = $ticketcount;
				$hotelEvent->cover_image = $hotel_db['cover_image'];
				$hotelEvent->desc = $hotel_db['desc'];
				$hotelEvent->address = $hotel_db['address'];
				$hotelEvent->lat = $hotel_db['lat'];
				$hotelEvent->lon = $hotel_db['lon'];
				$hotelEvent->mtime = date('Y-m-d H:i:s' , time());
				$hotelEvent->ctime = date('Y-m-d H:i:s' , time());
				$hotelEvent->status = 0;
				if($hotelEvent->save()) {
					$id = Yii::app()->db->getLastInsertId();
					ajax_response('200', '', array('xid'=>$id));
				} else {
					ajax_response('500', '无法保存');
				}
			}
			
		} else {

			$poi_id = Yii::app()->request->getParam('id');
			$poi_db = Poi::model()->findByPk($poi_id);
			if(empty($poi_db)) {
				ajax_response('404', '找不到poi');
			}

			$poi_db = $poi_db->attributes;

			$poiEvent = new TripDayPoi;
			$poiEvent->trip_id = $trip_id;
			$poiEvent->trip_day_id = $trip_day_id;
			$poiEvent->poi_id = $poi_id;
			$poiEvent->poi_name = $poi_db['poi_name'];
			$poiEvent->desc = $poi_db['desc'];
			$poiEvent->type = $poi_db['type'];
			$poiEvent->cover_image = $poi_db['cover_image'];
			$poiEvent->price = $poi_db['price'];
			$poiEvent->note = empty($note) ? '': $note;
			$poiEvent->lat = $poi_db['lat'];
			$poiEvent->lon = $poi_db['lon'];
			$poiEvent->mtime = date('Y-m-d H:i:s', time());
			$poiEvent->ctime = date('Y-m-d H:i:s', time());
			$poiEvent->status = 0;
		
			if($poiEvent->save()) {
				$id = Yii::app()->db->getLastInsertId();
				ajax_response('200', '', array('xid'=>$id));
				ajax_response('200', '');
			} else {
				ajax_response('500', '保存失败');
			}
		}
	}
	public function actionRemoveEvent()
	{
		$trip_id = Yii::app()->request->getParam('trip_id');
		$trip_day_id = Yii::app()->request->getParam('trip_day_id');
		$event_id = Yii::app()->request->getParam('event_id');
		$type = Yii::app()->request->getParam('type');

		if(empty($type) || empty($event_id)) {
			ajax_response('404', '参数不正确');
		}

		$event = null;
		if($type == 'hotel') {
			$event = TripDayHotel::model()->findByPk($event_id);
		} else {
			$event = TripDayPoi::model()->findByPk($event_id);
		}

		if(empty($event)){
			ajax_response(500, '移除失败');
		}
		$event->status = -1;
		if($event->save()) {
			ajax_response(200, '');
		} else {
			ajax_response(500, '移除失败');
		}

	}

	public function actionFetchAllEvents()
	{
		$trip_id = Yii::app()->request->getParam('trip_id');
		$trip_day_id = Yii::app()->request->getParam('trip_day_id');

		$trip_day = TripDay::model()->findByPk($trip_day_id);
		if(empty($trip_day)) {
			ajax_response(400, '找不到改日行程');
		}
		$trip_day = $trip_day->attributes;
		$this->_data['title'] = empty($trip_day['title']) ? '' : $trip_day['title'];
		$this->_data['traffic_note'] = empty($trip_day['traffic_note']) ? '' : $trip_day['traffic_note'];
        $this->_data['desc'] = empty($trip_day['desc']) ? '' : nl2br($trip_day['desc']);

        $trip_day_images = explode(',', $trip_day['image']);
        $images = array();
        if(!empty($trip_day_images)) {
            foreach($trip_day_images as $image) {
                if(empty($image)) {
                    continue;
                }
                $images[] = array('image_hash' => $image, 'image_url' => upimage($image, 'org'));
            } 
        }
		//$this->_data['image'] = $trip_day_images;
		$this->_data['image_url'] = upimage($trip_day['image'], 'org');
        $this->_data['images'] = $images;

		$hotel_list = $poi_list = $note = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("trip_day_id='{$trip_day_id}'");
		$criteria->addCondition("status=0");
		$pois = TripDayPoi::model()->findAll($criteria);
		if(!empty($pois)) {
			foreach($pois as $poi) {
				$poi = $poi->attributes;
				$poi['xid'] = $poi['id'];
				$poi['event_id'] = $poi['id'];
				$poi['poi_id'] = $poi['poi_id'];
				$poi['poi_name'] = $poi['poi_name'];
				$poi['type'] = $poi['type'];
				$poi['href'] = '/admin/poi/edit?poi_id=' . $poi['id'];
				$poi['cover_image'] = upimage($poi['cover_image']);
				$poi['start_hours'] = 0;
				$poi['start_minutes'] = 0;
				$poi['end_hours'] = 0;
				$poi['end_minutes'] = 0;
				$poi_list[] = $poi;
			}
		}
		$this->_data['poi_list'] = $poi_list;

		$criteria = new CDbCriteria;
		$criteria->addCondition("trip_day_id='{$trip_day_id}'");
		$criteria->addCondition("status>=0");
		$hotels = TripDayHotel::model()->findAll($criteria);
		if(!empty($hotels)) {
			foreach($hotels as $hotel) {
				$hotel = $hotel->attributes;
				$hotel['xid'] = $hotel['id'];
				$hotel['title'] = $hotel['title'];
				$hotel['event_id'] = $hotel['id'];
				$hotel['type'] = 'hotel';
				$hotel['href'] = '/admin/hotel/edit?hotel_id=' . $hotel['hotel_id'];
				$hotel['hotel_id'] = $hotel['hotel_id'];
				$hotel['cover_image'] = $hotel['cover_image'];
				$hotel_list[] = $hotel;
			}
		}
		$this->_data['hotel_list'] = $hotel_list;

		ajax_response(200, '', $this->_data);
	}

	public function actionSaveEvent()
	{
		$event_id = Yii::app()->request->getParam('event_id');
		$type = Yii::app()->request->getParam('type');

		if(empty($event_id) || empty($type)) {
			ajax_response('404', '参数不正确');
		}
		$note = Yii::app()->request->getParam('note');
		$currency = Yii::app()->request->getParam('currency');
		$spend = Yii::app()->request->getParam('spend');
		$ticketcount = Yii::app()->request->getParam('ticketcount');

		if($type == 'hotel') {
			$hotel_id = Yii::app()->request->getParam('hotel_id');
			$event = TripDayHotel::model()->findByPk($event_id);
			if(empty($event)) {
				ajax_response('404', '找不到该事件');
			}

			$event->note = $note;
			$event->currency = $currency;
			$event->spend = $spend;
			$event->ticketcount = $ticketcount;
			if($hotel_id != $event->hotel_id) {
				
			}
			if($event->save()) {
				ajax_response('200', '保存成功');
			}

			ajax_response('500', '保存失败!');

		} else if(in_array($type, array('sight', 'restaurant', 'shopping', 'entertainment', 'island', 'traffic_tips'))) {
			$event = TripDayPoi::model()->findByPk($event_id);
			if(empty($event)) {
				ajax_response('404', '找不到该事件');
			}

			//var_dump($note);exit;
			$event->note = $note;
			/*
			$event->currency = $currency;
			$event->spend = $spend;
			$event->ticketcount = $ticketcount;
			 */
				
			if($event->save()) {
				ajax_response('200', '保存成功');
			}

			ajax_response('500', '保存失败!');

		} else if($type =='note') {

		}
	}

	public function actionRemoveTraffic()
	{
		$traffic_id = Yii::app()->request->getParam('traffic_id');
		$trip_day_id = Yii::app()->request->getParam('trip_day_id');
		if(empty($trip_day_id)) {
			ajax_response('404', '参数不正确');
		}

		$tripDayTraffic = TripDayTraffic::model()->findByPk($traffic_id);
		if(empty($tripDayTraffic)) {
			ajax_response('404', '参数不正确');
		}
		$tripDayTraffic->status = -1;
		if($tripDayTraffic->save()) {
			ajax_response(200, '');
		} else {
			ajax_response(500, '保存失败');
		}
	}

	public function actionSaveTraffic()
	{
		$accept_fields = array('xid', 'trip_id', 'trip_day_id', 'note', 'from_place','from_placeid', 'to_place','to_placeid', 'days', 'traffic_number', 'spend', 'currency', 'ticketcount', 'start_hours', 'start_minutes',  'end_hours', 'end_minutes','mode');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}
		if(!empty($data['xid'])) {
			$id = $data['xid'];
			unset($data['xid']);
			$tripDayTraffic = TripDayTraffic::model()->findByPk($id);
			if(!empty($tripDayTraffic)) {
				TripDayTraffic::model()->updateByPk($id, $data);
				ajax_response(200, '保存成功');
			} else {
				ajax_response(500, '保存失败');
			}
		} else {
			$new_traffic = new TripDayTraffic;
			$new_traffic->trip_id = $data['trip_id'];
			$new_traffic->trip_day_id = $data['trip_day_id'];
			$new_traffic->note = $data['note'];
			$new_traffic->from_place = $data['from_place'];
			$new_traffic->from_placeid = $data['from_placeid'];
			$new_traffic->to_place = $data['to_place'];
			$new_traffic->to_placeid = $data['to_placeid'];
			$new_traffic->days = $data['days'];
			$new_traffic->traffic_number = $data['traffic_number'];
			$new_traffic->spend = !empty($data['spend']) ? $data['spend'] : 0;
			$new_traffic->currency = $data['currency'];
			$new_traffic->start_hours = $data['start_hours'];
			$new_traffic->start_minutes = $data['start_minutes'];
			$new_traffic->end_hours = $data['end_hours'];
			$new_traffic->end_minutes = $data['end_minutes'];
			$new_traffic->ticketcount = $data['ticketcount'];
			$new_traffic->mode = $data['mode'];

			$new_traffic->status = 0;
			$new_traffic->mtime = date('Y-m-d H:i:s', time());
			$new_traffic->ctime = date('Y-m-d H:i:s', time());
			
			if($new_traffic->save()) {
				$xid = Yii::app()->db->getLastInsertId();
				ajax_response(200, '', array('xid'=>$xid));
			} else {
				var_dump($new_traffic->getErrors());
				ajax_response(500, '保存失败');
			}
		}
		 
	}

	public function actionTrafficList()
	{
		$trip_day_id = Yii::app()->request->getParam('trip_day_id');
		if(empty($trip_day_id)) {
			ajax_response('404', '参数不正确');
		}

		$traffic_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("trip_day_id='{$trip_day_id}'");
		$criteria->addCondition("status=0");
		$traffics = TripDayTraffic::model()->findAll($criteria);

        $icon_array = array('icon_plane', 'icon_train', 'icon_bus', 'icon_car', 'icon_steamer', 'icon_other');

		if(!empty($traffics)) {
			foreach($traffics as $traffic) {
				$traffic = $traffic->attributes;
				$traffic['xid'] = $traffic['id'];
				$traffic['start_hours'] = sprintf('%02d', $traffic['start_hours']);
				$traffic['end_hours'] = sprintf('%02d', $traffic['end_hours']);
				$traffic['start_minutes'] = sprintf('%02d', $traffic['start_minutes']);
				$traffic['end_minutes'] = sprintf('%02d', $traffic['end_minutes']);

                //echo $icon_array[$traffic['mode']];die();
                $traffic['mode_icon'] = $icon_array[$traffic['mode'] - 1];
				$traffic_list[] = $traffic;
			}
		}
		$this->_data['traffic_list'] = $traffic_list;
		ajax_response(200, '', $this->_data);
	}

	public function actionTrafficForm()
	{
		$id = Yii::app()->request->getParam('id');
		if(!empty($id)) {
			$traffic = TripDayTraffic::model()->findByPk($id);
			if(!empty($traffic)) {
				$this->_data['traffic'] = $traffic->attributes;
			}
		}
		$this->renderPartial('/api/form_traffic', $this->_data);
	}

	public function actionHotelForm()
	{
		$id = Yii::app()->request->getParam('eventid');
		if(!empty($id)) {
			$hotel = TripDayHotel::model()->findByPk($id);
			if(!empty($hotel)) {
				$this->_data['hotel'] = $hotel->attributes;
			}
		} else {
			$trip_id = Yii::app()->request->getParam('trip_id');
			$trip_day_id = Yii::app()->request->getParam('trip_day_id');

			$trip_db = Trip::model()->findByPk($trip_id);
			if(empty($trip_db)) {
				die('No trip find !');
			}
			$list = array();
			$criteria = new CDbCriteria;
			$criteria->addCondition("trip_id='{$trip_id}'");
			$criteria->order = 'sort asc';
			$criteria->addCondition("status=0");
			$trip_day_list = TripDay::model()->findAll($criteria);
			if(!empty($trip_day_list)) {
				foreach($trip_day_list as $key => $trip_day) {
					$trip_day = $trip_day->attributes;
					$city_ids = json_decode($trip_day['city_ids'], true);
					$city_string = '';
					if(!empty($city_ids)) {
						foreach($city_ids as $i => $city) {
							if($i > 0) {
								$city_string .= ' - ' . $city['city_name'];
							} else {
								$city_string .= $city['city_name'];
							}
						}
					}
					$day = $key + 1;
					$day_time = date('m/d', strtotime("+$key days", strtotime($trip_db->start_time)));
					$list[] = array(
						//'title' => '第' . $day . '天 ' . $city_string,
						'title' => $day_time . $city_string,
						'trip_day_id' => $trip_day['id'],
						'the_day' => $day
					);
				}
				$this->_data['trip_day_list'] = $list;
			}
		}

		$this->renderPartial('form_hotel', $this->_data);
	}

    public function actionPoiForm()
    {
        $id = Yii::app()->request->getParam('id');
        if(empty($id)) {
            //ajax_response('404', '参数不正确');
        }

        $poiEvent = TripDayPoi::model()->findByPk($id);
        if(empty($poiEvent)) {
            //ajax_response('404', '找不到信息');
        }else {
            $poi  = $poiEvent->attributes;
            $poi['cover_image'] =  upimage($poi['cover_image']);
            //var_dump($poi);die();
            $this->_data['poi']= $poi;
        }

        $this->renderPartial('/api/form_poi', $this->_data);
    }

	public function actionDayDescForm()
	{
		$id = Yii::app()->request->getParam('trip_day_id');
		if(!empty($id)) {
            $tripDay = TripDay::model()->findByPk($id);
            if(!empty($tripDay)) {
                $this->_data['trip_day']= $tripDay->attributes;
            }


        }

		$this->renderPartial('/api/form_note', $this->_data);
	}

	public function actionDeleteOneday()
	{
		$trip_day_id = Yii::app()->request->getParam('trip_day_id');
		$trip_id = Yii::app()->request->getParam('trip_id');

		if(empty($trip_id) || empty($trip_day_id)) {
			ajax_response(404, '缺少参数');
		}

		$trip_day = TripDay::model()->findByPk($trip_day_id);
		if(empty($trip_day)) {
			ajax_response(404, '找不到行程');
		}
		$trip_day->status = -1;
		if($trip_day->save()) {
            $trip_days = TripDay::model()->countByAttributes(array('trip_id' => $trip_id, 'status' => 0));
            Trip::model()->updateByPk($trip_id, array('days'=>$trip_days));
            ajax_response(200,'');
		} else {
			ajax_response(500,'');
		}
	}

	public function actionAddBlankOneday()
	{
		$index = Yii::app()->request->getParam('index');
		$trip_id = Yii::app()->request->getParam('trip_id');

		if(empty($trip_id) || empty($index)) {
			ajax_response(404, '缺少参数');
		}
		$trip_db = Trip::model()->findByPk($trip_id);
		if(empty($trip_db)) {
			ajax_response(404, '行程不存在');
		}

		$criteria = new CDbCriteria;
		$criteria->addCondition("trip_id='{$trip_id}'");
		$criteria->order = 'sort asc';
		$criteria->addCondition("status=0");
		$trip_day_list = TripDay::model()->findAll($criteria);
		if(!empty($trip_day_list)) {
			foreach($trip_day_list as $key => $trip_day) {
				$old_sort = $trip_day->sort;
				if($old_sort < $index) {
					$trip_day->sort = $old_sort;
					$trip_day->save();
				} else {
					$trip_day->sort = $old_sort + 1;
					$trip_day->save();
				}
			}
		}
		$start_time = empty($trip_db->start_time) ? date('Y-m-d', time()) : $trip_db->start_time;
		$new_trip_day = new TripDay;
		$new_trip_day->trip_id = $trip_id;
		$new_trip_day->title = '';
		$new_trip_day->date = $start_time;
		$new_trip_day->city_id = 0;
		$new_trip_day->user_id = 0;
		$new_trip_day->sort = $index;
		$new_trip_day->status = 0;
		if($new_trip_day->save()) {
			$id = Yii::app()->db->getLastInsertId();
			$new_trip_day_db = TripDay::model()->findByPk($id);
			if(!empty($new_trip_day_db)) {
                //更新行程天数
                $trip_days = TripDay::model()->countByAttributes(array('trip_id' => $trip_id, 'status' => 0));
                Trip::model()->updateByPk($trip_id, array('days'=>$trip_days));
				$new_trip_day = $new_trip_day_db->attributes;
				ajax_response(200, '', $new_trip_day);
			} else {
				ajax_response(500, '插入失败');
			}
		} else {
            //var_dump($new_trip_day->getErrors());
			ajax_response(500, '插入失败2');
		}
		
	}

	public function actionUpdateOnedayCitys()
	{
		$trip_id = Yii::app()->request->getParam('trip_id');
		$trip_day_id = Yii::app()->request->getParam('trip_day_id');
		
		$tripDay = TripDay::model()->findByPk($trip_day_id);
		if(empty($tripDay)) {
			ajax_response(400, '找不到日程');
		}
		$city_list = Yii::app()->request->getParam('city_list');

		/*
		$old_city_list = TripDayCity::model()->findAllByAttributes(array('trip_day_id'=>$trip_day_id));
		if(!empty($old_city_list)) {
			foreach('');
		}
		 */
		//if(empty($city_list)) {
			$tripDay->city_ids = json_encode($city_list);
			$tripDay->save();
		//}
		/*
		foreach($city_list as $city) {
			if(!empty($city)) {
				$tripDay->city_id = $city['city_id'];
				$tripDay->save();
				break;
			}
		}
		 */
		//var_dump($city_list);
		ajax_response(200, '', $city_list);
	}

	public function actionUpdateTrip()
	{
		$trip_id = Yii::app()->request->getParam('trip_id');

		$title = Yii::app()->request->getParam('title');
		$start_time = Yii::app()->request->getParam('start_time');

		$trip_db = Trip::model()->findByPk($trip_id);
		if(empty($trip_db)) {
			ajax_response('404', '未找到行程');
		}

		if(!empty($start_time)) {
			$trip_db->start_time = $start_time;
		}
		if(!empty($title)) {
			$trip_db->title = $title;
		}
		if(!empty($title) || !empty($start_time)) {
			$trip_db->save();
		}
		ajax_response('200', '');

	}

	public function actionUpdateOneday()
	{
		$trip_day_id = Yii::app()->request->getParam('trip_day_id');

		$desc = Yii::app()->request->getParam('desc');
		$title = Yii::app()->request->getParam('title');
		$traffic_note = Yii::app()->request->getParam('traffic_note');
		$image = Yii::app()->request->getParam('image');

		$tripDay = TripDay::model()->findByPk($trip_day_id);
		if(!empty($tripDay)) {
			if(isset($_POST['desc'])) {
				$tripDay->desc = $desc;
			}
			if(isset($_POST['title'])) {
				$tripDay->title = $title;
			}
			if(isset($_POST['traffic_note'])) {
				$tripDay->traffic_note = $traffic_note;
			}
			if(isset($_POST['image'])) {
				$tripDay->image = $image;
			}

			if($tripDay->save()) {
				ajax_response(200, '');
			} else {
				ajax_response(500, '保存失败');
			}
		} else {
			ajax_response(500, '找不到信息');
		}
	}

	public function actionResetOnedayOrder()
	{
		$trip_id = Yii::app()->request->getParam('trip_id');
		$new_order = Yii::app()->request->getParam('new_order');

		if(empty($trip_id) || empty($new_order)) {
			ajax_response(404, '缺少参数');
		}

		$new_order_array = explode(',', $new_order);
		foreach($new_order_array as $key=>$trip_day_id) {
			$trip_day = TripDay::model()->findByPk($trip_day_id);
			$trip_day->sort = $key + 1;
			$trip_day->save();
		}
		ajax_response(200, '');
	}

	public function actionSaveTripNote()
	{
		$trip_id = Yii::app()->request->getParam('trip_id');
		$note = Yii::app()->request->getParam('note');

		$trip_note = TripNote::model()->findByAttributes(array('trip_id'=>$trip_id));
		if(empty($trip_note)) {
			$trip_note = new TripNote;
			$trip_note->trip_id = $trip_id;
			$trip_note->note = $note;
			$trip_note->status = 0;

			$trip_note->save();
		} else {
			$trip_note->note = $note;
			$trip_note->save();
		}

		ajax_response(200, '');
		
	}

	public function actionGetTripNote()
	{
		$trip_id = Yii::app()->request->getParam('trip_id');
		if(empty($trip_id)) {
			ajax_response(404, '缺少参数');
		}

		$trip_note = TripNote::model()->findByAttributes(array('trip_id'=>$trip_id));
		if(empty($trip_note)) {
			$result = '';
			//ajax_response(404, '找不到贴士');
		}
		$trip_note = $trip_note->attributes;

		$result = $trip_note['note'];
		ajax_response(200, '', $result);

	}

    public function actionSetDemoTripDay()
    {

        $trip_day_id = Yii::app()->request->getParam('trip_day_id');

        if(empty($trip_day_id)) {
            ajax_response(404, '参数错误');
        }

        $trip_day = TripDay::model()->findByPk($trip_day_id);
        if(empty($trip_day)) {
            ajax_response(404, '找不到当天行程');
        }

        $city_id = 0;
        if(!empty($trip_day->city_ids)) {
            $city_list = json_decode($trip_day->city_ids);
            if(!empty($city_list)) {
                $city_id = $city_list[0]->city_id;
            }
        }

        $demo = new DemoTripDay();
        $demo->source_trip_day_id = $trip_day_id;
        $demo->title = $trip_day->title;
        $demo->desc = $trip_day->desc;
        $demo->traffic_note = $trip_day->traffic_note;
        $demo->city_id = $city_id;
        $demo->user_id = Yii::app()->partner->id;
        $demo->ctime = time();
        $demo->mtime = time();
        $demo->status = 1;

        if($demo->save()) {
            ajax_response(200, '');
        } else {
            ajax_response(500, '保存失败');
        }
        /*
        $demo = new DemoTripDay;
        $demo->title = $title;
        $demo->desc = $desc;
        //$demo->traffic_note = $traffic_note;
        $demo->city_id = $city_id;
        $demo->admin_id = Yii::app()->adminUser->id;
        $demo->ctime = date('Y-m-d H:i:s', time());
        $demo->status = 0;

        if($demo->save()) {
            ajax_response(200, '');
        } else {
            ajax_response(500, '保存失败');
        }
        */
    }

	public function actionSaveDemoTripDay()
	{

		$title = Yii::app()->request->getParam('title');
		$desc = Yii::app()->request->getParam('desc');
		$traffic_note = Yii::app()->request->getParam('traffic_note');

		$city_id = Yii::app()->request->getParam('city_id');
		if(empty($title) || empty($desc) || empty($city_id)) {
			ajax_response(404, '标题和内容不能为空');
		}

		$demo = new DemoTripDay;
		$demo->title = $title;
		$demo->desc = $desc;
		$demo->traffic_note = $traffic_note;
		$demo->city_id = $city_id;
		$demo->admin_id = Yii::app()->adminUser->id;
		$demo->ctime = date('Y-m-d H:i:s', time());
		$demo->status = 0;

		if($demo->save()) {
			ajax_response(200, '');
		} else {
			ajax_response(500, '保存失败');
		}
	}

	public function _getDemoTripDayList($city_id) 
	{

        $login_user_id = Yii::app()->partner->id;

		$demo_list = array();

		$page = Yii::app()->request->getParam('page');
		$page = intval($page) > 1 ? $page : 1;
		$limit = 10;
		$offset = ($page - 1) * $limit;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $limit;
        $criteria->addCondition("user_id='{$login_user_id}'");
		$criteria->addCondition("city_id='{$city_id}'");
		$criteria->order = 'sort asc';

		$criteria->addCondition("status >=0");

		$trip_day_list = DemoTripDay::model()->findAll($criteria);

		if(!empty($trip_day_list)) {
			foreach($trip_day_list as $trip_day) {
				$trip_day = $trip_day->attributes;
                $trip_day['desc'] = $trip_day['desc'];
				$demo_list[] = $trip_day;
			}
		}
		$this->_data['demo_trip_day_list'] = $demo_list;
		ajax_response(200, '', $this->_data);
	}

	public function actionSaveDemoNote()
	{
		$note = Yii::app()->request->getParam('note');

		if(empty($note)) {
			ajax_response(404, '内容不能为空');
		}
		$trip_note = new Note;
		$trip_note->city_id = 0;
		$trip_note->country_id = 0;
		$trip_note->city_name = '';
		$trip_note->title = '';
		$trip_note->content = note;

		$trip_note->save();

		ajax_response(200, '');
		
    }

    public function actionCopy()
    {
        $sourceId = Yii::app()->request->getParam('sourceId');
        $targetTitle = Yii::app()->request->getParam('targetTitle');
        $targetCustomerId = Yii::app()->request->getParam('targetCustomerId');

        //checkCustomer
        $customer_db = Customer::model()->findByPk($targetCustomerId);
        if(empty($customer_db)) {
            ajax_response('404', '用户不存在', '');
        }
        $customer_db = $customer_db->attributes;

        $source_trip = Trip::model()->findByPk($sourceId);
        if(empty($source_trip)) {
            ajax_response('404', '复制源不存在', '');
        }
        $source_trip = $source_trip->attributes;
        if($source_trip['status'] <= 0 ) {
            ajax_response('404', '复制源已经删除，不能复制', '');
        }

        //copy trip
        $new_trip = new Trip;
        $new_trip->title = empty($targetTitle) ? $customer_db['contact'] . '的行程' : $targetTitle;
        $new_trip->customer_id = $targetCustomerId;
		$new_trip->admin_id = Yii::app()->adminUser->id;
        $new_trip->start_time = empty($targetStartTime) ? $source_trip['start_time']: $targetStartTime;
        $new_trip->days = $source_trip['days'];
        $new_trip->start_city_id = $source_trip['start_city_id'];
        $new_trip->end_city_id = $source_trip['end_city_id'];
        $new_trip->ctime = date('Y-m-d H:i:s');
        $new_trip->mtime = date('Y-m-d H:i:s');
        $new_trip->status = 1;
        
        $new_trip->save();
        $new_trip_id = Yii::app()->db->getLastInsertId();
        if(empty($new_trip_id)) {
            ajax_response('500', '复制失败001，请联系喵喵');
        }
        
        //copy trip_day
		$trip_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("trip_id='{$sourceId}'");
		$criteria->addCondition("status >='0'");
		$criteria->order = 'sort asc';
		$tripDays = TripDay::model()->findAll($criteria);
		if(!empty($tripDays)) {
            foreach($tripDays as $day) {
                $new_trip_day = new TripDay;
                $new_trip_day->trip_id = $new_trip_id;
                $new_trip_day->date = $day->date;
                $new_trip_day->title = $day->title;
                $new_trip_day->desc = $day->desc;
                $new_trip_day->traffic_note = $day->traffic_note;
                $new_trip_day->image = $day->image;
                $new_trip_day->city_id = $day->city_id;
                $new_trip_day->city_ids = $day->city_ids;
                $new_trip_day->user_id = $day->user_id;
                $new_trip_day->note = $day->note;
                $new_trip_day->sort = $day->sort;
                $new_trip_day->status = $day->status;

                if($new_trip_day->save()) {
                    $trip_day_id = $day['id'];
                    $new_trip_day_id = Yii::app()->db->getLastInsertId();
                    if($new_trip_day_id <=0) {
                        ajax_response('500', '插入TripDay失败, 请联系喵喵');
                    }
                    
                    //copy trip_day_hotel
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $hotels = TripDayHotel::model()->findAll($criteria);
                    if(!empty($hotels)) {
                        foreach($hotels as $hotel) {
                            $hotelEvent = new TripDayHotel;
                            $hotelEvent->trip_id = $new_trip_id;
                            $hotelEvent->trip_day_id = $new_trip_day_id;
                            $hotelEvent->hotel_id = $hotel->hotel_id;
                            $hotelEvent->title = $hotel->title;
                            $hotelEvent->note = $hotel->note;
                            $hotelEvent->cover_image = $hotel->cover_image;
                            $hotelEvent->currency = $hotel->currency;
                            $hotelEvent->spend = $hotel->spend;
                            $hotelEvent->ticketcount = $hotel->ticketcount;
                            $hotelEvent->type = $hotel->type;
                            $hotelEvent->desc = $hotel->desc;
                            $hotelEvent->address = $hotel->address;
                            $hotelEvent->lat = $hotel->lat;
                            $hotelEvent->lon = $hotel->lon;
                            $hotelEvent->mtime = date('Y-m-d H:i:s' , time());
                            $hotelEvent->ctime = date('Y-m-d H:i:s' , time());
                            $hotelEvent->status = $hotel->status;
                            $hotelEvent->save();
                        }
                    }

                    //copy trip_day_poi
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $pois = TripDayPoi::model()->findAll($criteria);
                    if(!empty($pois)) {
                        foreach($pois as $poi) {
                            $poiEvent = new TripDayPoi;
                            $poiEvent->trip_id = $new_trip_id;
                            $poiEvent->trip_day_id = $new_trip_day_id;
                            $poiEvent->poi_id = $poi->poi_id;
                            $poiEvent->type = $poi->type;
                            $poiEvent->poi_name= $poi->poi_name;
                            $poiEvent->desc = $poi->desc;
                            $poiEvent->cover_image = $poi->cover_image;
                            $poiEvent->price = $poi->price;
                            $poiEvent->note = $poi->note;
                            $poiEvent->lat = $poi->lat;
                            $poiEvent->lon = $poi->lon;
                            $poiEvent->sort = $poi->sort;
                            $poiEvent->mtime = date('Y-m-d H:i:s' , time());
                            $poiEvent->ctime = date('Y-m-d H:i:s' , time());
                            $poiEvent->status = $poi->status;
                            $poiEvent->save();
                        }
                    }

                    //copy trip_day_traffic
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $traffics = TripDayTraffic::model()->findAll($criteria);
                    if(!empty($traffics)) {
                        foreach($traffics as $traffic) {
                            $traffic = $traffic->attributes;
                            $new_traffic = new TripDayTraffic;
                            $new_traffic->trip_id = $new_trip_id;
                            $new_traffic->trip_day_id = $new_trip_day_id;
                            $new_traffic->note = $traffic['note'];
                            $new_traffic->from_place = $traffic['from_place'];
                            $new_traffic->from_placeid = $traffic['from_placeid'];
                            $new_traffic->to_place = $traffic['to_place'];
                            $new_traffic->to_placeid = $traffic['to_placeid'];
                            $new_traffic->days = $traffic['days'];
                            $new_traffic->traffic_number = $traffic['traffic_number'];
                            $new_traffic->spend = !empty($traffic['spend']) ? $traffic['spend'] : 0;
                            $new_traffic->currency = $traffic['currency'];
                            $new_traffic->start_hours = $traffic['start_hours'];
                            $new_traffic->start_minutes = $traffic['start_minutes'];
                            $new_traffic->end_hours = $traffic['end_hours'];
                            $new_traffic->end_minutes = $traffic['end_minutes'];
                            $new_traffic->ticketcount = $traffic['ticketcount'];
                            $new_traffic->mode = $traffic['mode'];
                            $new_traffic->status = 0;
                            $new_traffic->mtime = date('Y-m-d H:i:s', time());
                            $new_traffic->ctime = date('Y-m-d H:i:s', time());
                            $new_traffic->save();
                        }
                    }

                }
			}
        }

        //copy trip_note
		$note_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("trip_id='{$sourceId}'");
		$criteria->addCondition("status >='0'");
		$tripNotes = TripNote::model()->findAll($criteria);
		if(!empty($tripNotes)) {
            foreach($tripNotes as $note) {
                $new_trip_note= new TripNote;
                $new_trip_note->trip_id = $new_trip_id;
                $new_trip_note->note = $note->note;
                $new_trip_note->status = $note->status;

                $new_trip_note->save();
            }
        }
		ajax_response(200, '', $trip_list);
    }


    public function actionUpdateTripDayFromDemo()
    {
        $trip_day_id = Yii::app()->request->getParam('trip_day_id');
        $demo_id = Yii::app()->request->getParam('demo_id');


        $demo_day_trip = DemoTripDay::model()->findByPk($demo_id);
        $trip_day = TripDay::model()->findByPk($trip_day_id);


        $trip_day->title = $demo_day_trip->title;
        $trip_day->desc = $demo_day_trip->desc;
        $trip_day->traffic_note = $demo_day_trip->traffic_note;


        if($trip_day->save()) {
            echo 'ok';
        }

        var_dump($trip_day->attributes);
    }
}
