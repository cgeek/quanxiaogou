<?php
/**
 * Created by PhpStorm.
 * User: cgeek
 * Date: 8/16/16
 * Time: 11:00 AM
 */

class TripController extends Controller
{
    public $_identity;
    public $_data;
    public $layout = '/layouts/column2';


    public function beforeAction($action)
    {
        $login_user_id = Yii::app()->partner->id;
        if(intval($login_user_id) <= 0) {
            $this->redirect('/');
        }
        return parent::beforeAction($action);
    }

    public function actionPreview()
    {
        $id = Yii::app()->request->getParam('id');
        $type = Yii::app()->request->getParam('type');

        $trip = Trip::model()->findByPk($id);
        if(!empty($trip)) {
            $this->_data['trip'] = $trip;
        }
        $this->_data['id'] = $id;
        $this->_data['type'] = $type;

        $this->render('preview', $this->_data);
    }

    public function actionDemoDayTrip()
    {
        $login_user_id = Yii::app()->partner->id;
        if(intval($login_user_id) <= 0) {
            $this->redirect('/');
        }

        $page = Yii::app()->request->getParam('page');

        $demo_trip_day_list = array();

        $pageSize = 10;
        $page = !empty($page) ? intval($page) : 1;
        $status = Yii::app()->request->getParam('status');
        $status = empty($status) ? 1 : $status;
        $offset = ($page - 1) * $pageSize;

        $criteria = new CDbCriteria;
        $criteria->offset = $offset;
        $criteria->limit = $pageSize;
        $criteria->addCondition("status=$status");
        $criteria->addCondition("user_id=$login_user_id");

        $count = DemoTripDay::model()->count($criteria);
        $demo_days = DemoTripDay::model()->findAll($criteria);
        if(!empty($demo_days)) {
            foreach($demo_days as $demo) {
                $demo = $demo->attributes;
                $demo_trip_day_list[] = $demo;
            }
        }
        $this->_data['trip_list'] = $demo_trip_day_list;

        //分页
        $pages=new CPagination($count);
        $pages->pageSize= $pageSize;
        $pages->pageVar = 'page';
        $pages->applyLimit($criteria);
        $this->_data['pages'] = $pages;
        $this->_data['count'] = $count;

        $this->render('demo_day_list', $this->_data);

    }

    public function actionDeleteDemoTripDay()
    {
        $demo_day_id = Yii::app()->request->getParam('demo_day_id');

        if(empty($demo_day_id)) {
            ajax_response(404, 'params error');
        }
        $demo_day  = DemoTripDay::model()->findByPk($demo_day_id);
        if(empty($demo_day)) {

            ajax_response(404, '找不到线路');
        }

        $r = DemoTripDay::model()->updateByPk($demo_day_id, array('status' => '-1'));
        if($r) {
            ajax_response(200, '');
        } else {
            ajax_response(200, '服务器正忙，请稍后再试');
        }

    }

    public function actionEditDemoDay()
    {
        $demo_day_id = Yii::app()->request->getParam('id');

        $demo_day  = DemoTripDay::model()->findByPk($demo_day_id);

        $this->render('edit_demo_day', array('demo_day' =>$demo_day));
    }

    public function actionIndex()
    {
        $this->pageTitle = '行程列表';

        $keyword = Yii::app()->request->getParam('query');
        $customer_id = Yii::app()->request->getParam('customer_id');
        $type = Yii::app()->request->getParam('type');
        $type = empty($type) ? 'customer' : $type;
        $this->_data['type'] = $type;

        $trip_list = array();
        $pageSize = 15;
        $page = isset($_GET['page']) ? intval($_GET['page']) : 1;
        $offset = ($page - 1) * $pageSize;
        $criteria = new CDbCriteria;
        $criteria->offset = $offset;
        $criteria->limit = $pageSize;

        $login_user_id = Yii::app()->partner->id;
        if(intval($login_user_id) <= 0) {
            $this->redirect('/');
        }

        if($type == 'demo') {
            $criteria->addCondition("user_id=$login_user_id");
            //$criteria->addCondition('type=2');
        } elseif($type == 'customer') {
            $criteria->addCondition("user_id=$login_user_id");
            //$criteria->addCondition('type=1');
        } else {
            //$criteria->addCondition('type=3');
        }

        if($customer_id > 0) {
            $criteria->addCondition("customer_id=$customer_id");
        }

        if(isset($_GET['status']) && $_GET['status'] != '') {
            $criteria->addCondition("status={$_GET['status']}");
            $this->_data['status'] = $_GET['status'];
        } else {
            $criteria->addCondition("status >= 0");
        }

        if(!empty($keyword)){
            $criteria->addCondition("(title like '%{$keyword}%')");
            $this->_data['keyword'] = $keyword;
        }
        $criteria->order = ' `ctime` DESC';
        $count = Trip::model()->count($criteria);
        $trips = Trip::model()->findAll($criteria);
        if(!empty($trips)) {
            foreach($trips as $trip) {
                $trip = $trip->attributes;
                if($trip['customer_id'] > 0) {
                    $customer_db = Customer::model()->findByPk($trip['customer_id']);
                    $trip['customer_name'] = $customer_db['contact'];
                }
                $trip_list[] = $trip;
            }
        }
        $this->_data['trip_list'] = $trip_list;

        //分页
        $pages=new CPagination($count);
        $pages->pageSize= $pageSize;
        $pages->pageVar = 'page';
        $pages->applyLimit($criteria);
        $this->_data['pages'] = $pages;
        $this->_data['count'] = $count;

        $this->render('list', $this->_data);

    }

    public function actionEdit($trip_id = NULL)
    {
        $trip_id = Yii::app()->request->getParam('id');
        $this->pageTitle = '行程计划工具';
        //echo $trip_id;
        if(empty($trip_id)) {
            die('error');
        }
        $trip = Trip::model()->findByPk($trip_id);
        if(empty($trip)) {
            echo '404';die();
        }
        $this->_data['trip'] = $trip->attributes;

        $this->renderPartial('edit', $this->_data);
    }

    public function actionEditDemo($trip_id = NULL)
    {
        $trip_id = Yii::app()->request->getParam('id');
        $this->pageTitle = '行程计划工具';
        //echo $trip_id;
        if(empty($trip_id)) {
            die('error');
        }
        $trip = Trip::model()->findByPk($trip_id);
        if(empty($trip)) {
            echo '404';die();
        }
        $this->_data['trip'] = $trip->attributes;
        $this->render('edit', $this->_data);
    }


    public function actionCustomer()
    {
        $this->render('customer');
    }

    public function actionDemo()
    {
        $this->render('demo');
    }

    /**
     * 快速创建行程，不用选择国家城市，这么麻烦了
     *
     */
    public function actionQuickCreate()
    {
        //如果有客户id，取客户的时间，目的地，姓名等
        $customer_id = Yii::app()->request->getParam('customer_id');

        $type = Yii::app()->request->getParam('type');
        $type = ($type == 'demo') ? '2' : '1';

        $title = Yii::app()->request->getParam('title');
        $title = empty($title) ? '行程安排' : $title;

        $login_user_id = Yii::app()->partner->id;
        if(intval($login_user_id) <= 0) {
            $this->redirect('/');
        }

        $start_time = date('Y-m-d', time());
        $total_days = 1;
        $start_city_id = $end_city_id = '';
        if(!empty($customer_id)) {
            $customer_db = Customer::model()->findByPk($customer_id);
            if(!empty($customer_db)) {
                $customer_db = $customer_db->attributes;
                $title =  ($title != '行程安排') ? $title :$customer_db['contact'] . '的行程';
                if(!empty($customer_db['start_date'])) {
                    $start_time = $customer_db['start_date'];
                }


            } else {
                $customer_id = 0;
            }
        }

        $new_trip = new Trip();
        $new_trip->user_id = $login_user_id;
        $new_trip->title = $title;
        $new_trip->customer_id = $customer_id;
        $new_trip->start_time = $start_time;
        $new_trip->days = $total_days;
        $new_trip->start_city_id = $start_city_id;
        $new_trip->end_city_id = $end_city_id;
        $new_trip->mtime = date('Y-m-d H:i:s', time());
        $new_trip->ctime = date('Y-m-d H:i:s', time());
        $new_trip->status = 1;
        $hash_string = $title . time() . $total_days;
        $new_trip->hash_id = md5($hash_string);
        $new_trip->type = $type;

        if(!$new_trip->save()) {
            die('创建行程失败,请联系管理员');
        }
        $trip_id = Yii::app()->db->getLastInsertId();

        //更新trip_id到customer表
        if($customer_id > 0 && $trip_id > 0) {
            Customer::model()->updateByPk($customer_id, array('trip_id' => $trip_id));
        }

        $new_trip_day = new TripDay;
        $new_trip_day->trip_id = $trip_id;
        $new_trip_day->date = date('Y-m-d', time());
        $new_trip_day->title = '';
        $new_trip_day->city_id = '';
        $new_trip_day->status = 0;
        $new_trip_day->sort = 1;
        $new_trip_day->city_ids = '';
        if(!$new_trip_day->save()) {
            var_dump($new_trip_day->getErrors());
        }

        $this->redirect('/trip/edit/?id=' . $trip_id);


    }

    public function actionCreate()
    {
        $step = Yii::app()->request->getParam('step');
        $step = empty($step) ? '1' : intval($step);
        if($step == 1) {
            //Yii::app()->getRequest()->getCookies()->clear('citylist');
            Yii::app()->getRequest()->cookies['citylist'] = $cookie = new CHttpCookie('citylist', '');
            $this->render('step1', $this->_data);
        } elseif($step == 2) {
            $country_id = Yii::app()->request->getParam('country_id');
            if(empty($country_id)) {
                $country_id = 2;
            }
            $country_db = Country::model()->findByPk($country_id);
            if(empty($country_db)) {
                echo 'country not find';
                die();
            }
            $this->_data['country'] = $country_db->attributes;
            $this->render('step2', $this->_data);
        } elseif($step == 3) {
            $this->render('step3', $this->_data);
        } elseif($step == 4) {
            $country_id = Yii::app()->request->getParam('country_id');
            $customer_id = Yii::app()->request->getParam('customer_id');

            $cookie = Yii::app()->request->getCookies();
            $planinfo = $cookie['planinfo']->value;
            if(empty($planinfo)) {
                $this->redirect('/plan/create/step1');
            }
            $planinfo = json_decode($planinfo, true);

            $title = "我的行程";
            $country_db = Country::model()->findByPk($country_id);
            if(!empty($country_db)) {
                $country_db = $country_db->attributes;
                $title = $country_db['name'] . "的行程";
            }

            $total_days = $the_day = 0;
            $trip_day_list = array();
            foreach($planinfo['citylist'] as $city) {
                //echo $city['name'];die();
                $total_days += $city['days'];
                for($i=0; $i< intval($city['days']); $i++) {
                    $day = array(
                        'city_name' => $city['city_name'],
                        'city_id' => $city['city_id']
                    );
                    if($the_day > 0) {
                        $day['date'] = date('Y-m-d', strtotime($planinfo['starttime'] . "+$the_day day" ));
                    } else {
                        $day['date'] = $planinfo['starttime'];
                    }
                    $trip_day_list[$the_day]  = $day;
                    $the_day++;
                }
            }

            $admin_id = 0;
            if(!empty($customer_id)) {
                $customer_db = Customer::model()->findByPk($customer_id);
                if(!empty($customer_db)) {
                    $title =  $customer_db['contact'] . '的' . $country_db['name'] . "行程";
                    $customer_db = $customer_db->attributes;
                    $admin_id = $customer_db['admin_id'];
                    $customer_id = $customer_db['id'];
                } else {
                    $customer_id = 0;
                }
            }

            $new_trip = new Trip();
            $new_trip->title = $title;
            $new_trip->admin_id = $admin_id;
            $new_trip->customer_id = $customer_id;
            $new_trip->user_id = Yii::app()->partner->id;
            $new_trip->start_time = $planinfo['starttime'];
            $new_trip->days = $total_days;
            $new_trip->start_city_id = $planinfo['scity']['i'];
            $new_trip->end_city_id = $planinfo['ecity']['i'];
            $new_trip->mtime = date('Y-m-d H:i:s', time());
            $new_trip->ctime = date('Y-m-d H:i:s', time());
            $new_trip->status = 1;
            $hash_string = $title . time() . $total_days;
            $new_trip->hash_id = md5($hash_string);

            if(!$new_trip->save()) {
                die('创建行程失败');
            }
            $trip_id = Yii::app()->db->getLastInsertId();

            //更新trip_id到customer表
            if($customer_id > 0 && $trip_id > 0) {
                Customer::model()->updateByPk($customer_id, array('trip_id' => $trip_id));
            }

            foreach($trip_day_list as $key=>$city) {
                $city_ids = array($city);
                //如果是第一天
                if($key == 0 && !empty($planinfo['scity'])) {
                    $city_ids = array( array('city_id'=> $planinfo['scity']['i'], 'city_name' => $planinfo['scity']['n'], 'date' => $planinfo['starttime']),
                        $city);
                }
                if(isset($trip_day_list[$key+1]) && $city['city_id'] != $trip_day_list[$key+1]['city_id'] ) {
                    $city_ids[] =$trip_day_list[$key+1];
                }
                //如果是最后一天
                if($key == count($trip_day_list) - 1 && !empty($planinfo['ecity'])) {
                    $edate = date('Y-m-d', strtotime($planinfo['starttime'] . "+$key day" ));
                    $ecity = array(
                        'city_id'=> $planinfo['ecity']['i'],
                        'city_name' => $planinfo['scity']['n'],
                        'date'=> $edate
                    );
                    $city_ids[] = $ecity;
                }
                $city_ids = json_encode($city_ids);
                $new_trip_day = new TripDay;
                $new_trip_day->trip_id = $trip_id;
                $new_trip_day->date = $city['date'];
                $new_trip_day->title = '';
                $new_trip_day->city_id = $city['city_id'];
                $new_trip_day->status = 0;
                $new_trip_day->sort = $key + 1;
                $new_trip_day->city_ids = $city_ids;
                if(!$new_trip_day->save()) {
                    var_dump($new_trip_day->getErrors());
                }
            }
            unset($cookie['planinfo']);

            $this->redirect('/trip/edit/?id=' . $trip_id);
        }


    }

    public function actionDeleteTrip()
    {
        $trip_id = Yii::app()->request->getParam('trip_id');
        $trip_db = Trip::model()->findByPk($trip_id);
        //检查是否是自己的行程，不能删除别人的
        if(empty($trip_db)) {
            ajax_response('404', '找不到行程');
        }
        $trip_db->status = -1;
        $trip_db->save();

        if($trip_db->customer_id > 0) {
            $customer_db = Customer::model()->findByPk($trip_db->customer_id);
            if(!empty($customer_db) && $customer_db->trip_id == $trip_id) {
                $customer_db->trip_id = 0;
                $customer_db->save();
            }
        }
        ajax_response(200,'删除成功');
    }

    public function actionCopy()
    {
        $sourceId = Yii::app()->request->getParam('sourceId');
        $targetTitle = Yii::app()->request->getParam('targetTitle');
        $targetCustomerId = Yii::app()->request->getParam('targetCustomerId');

        //checkCustomer

        $source_trip = Trip::model()->findByPk($sourceId);
        if(empty($source_trip)) {
            ajax_response('404', '复制源不存在', '');
        }
        $source_trip = $source_trip->attributes;
        if($source_trip['status'] <= 0 ) {
            ajax_response('404', '复制源已经删除，不能复制', '');
        }

        //copy trip
        $new_trip = new Trip;
        $new_trip->title = empty($targetTitle) ? $source_trip['title'] . "-副本" : $targetTitle;
        $new_trip->customer_id = $targetCustomerId;
        //$new_trip->admin_id = Yii::app()->adminUser->id;
        $new_trip->user_id = Yii::app()->partner->id;
        $new_trip->start_time = empty($targetStartTime) ? $source_trip['start_time']: $targetStartTime;
        $new_trip->days = $source_trip['days'];
        $new_trip->start_city_id = $source_trip['start_city_id'];
        $new_trip->end_city_id = $source_trip['end_city_id'];
        $new_trip->ctime = date('Y-m-d H:i:s');
        $new_trip->mtime = date('Y-m-d H:i:s');
        $new_trip->status = 1;
        $hash_string = $new_trip->title . time();
        $new_trip->hash_id = md5($hash_string);
        $new_trip->from_trip_id = $sourceId;
        if(!$new_trip->save()) {
            ajax_response('500', '复制失败001，请联系喵喵');
        }
        $new_trip_id = Yii::app()->db->getLastInsertId();
        if(empty($new_trip_id)) {
            ajax_response('500', '复制失败001，请联系喵喵');
        }
        //更新customer的trip_id
        Customer::model()->updateByPk($targetCustomerId, array('trip_id' => $new_trip_id));

        //copy trip_day
        $trip_list = array();
        $criteria = new CDbCriteria;
        $criteria->addCondition("trip_id='{$sourceId}'");
        $criteria->addCondition("status >='0'");
        $criteria->order = 'sort asc';
        $tripDays = TripDay::model()->findAll($criteria);
        if(!empty($tripDays)) {
            foreach($tripDays as $day) {
                $new_trip_day = new TripDay;
                $new_trip_day->trip_id = $new_trip_id;
                $new_trip_day->date = $day->date;
                $new_trip_day->title = $day->title;
                $new_trip_day->desc = $day->desc;
                $new_trip_day->traffic_note = $day->traffic_note;
                $new_trip_day->image = $day->image;
                $new_trip_day->city_id = $day->city_id;
                $new_trip_day->city_ids = $day->city_ids;
                $new_trip_day->user_id = $day->user_id;
                $new_trip_day->note = $day->note;
                $new_trip_day->sort = $day->sort;
                $new_trip_day->status = $day->status;

                if($new_trip_day->save()) {
                    $trip_day_id = $day['id'];
                    $new_trip_day_id = Yii::app()->db->getLastInsertId();
                    if($new_trip_day_id <=0) {
                        ajax_response('500', '插入TripDay失败, 请联系喵喵');
                    }

                    //copy trip_day_hotel
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $hotels = TripDayHotel::model()->findAll($criteria);
                    if(!empty($hotels)) {
                        foreach($hotels as $hotel) {
                            $hotelEvent = new TripDayHotel;
                            $hotelEvent->trip_id = $new_trip_id;
                            $hotelEvent->trip_day_id = $new_trip_day_id;
                            $hotelEvent->hotel_id = $hotel->hotel_id;
                            $hotelEvent->title = $hotel->title;
                            $hotelEvent->note = $hotel->note;
                            $hotelEvent->cover_image = $hotel->cover_image;
                            $hotelEvent->currency = $hotel->currency;
                            $hotelEvent->spend = $hotel->spend;
                            $hotelEvent->ticketcount = $hotel->ticketcount;
                            $hotelEvent->type = $hotel->type;
                            $hotelEvent->desc = $hotel->desc;
                            $hotelEvent->address = $hotel->address;
                            $hotelEvent->lat = $hotel->lat;
                            $hotelEvent->lon = $hotel->lon;
                            $hotelEvent->mtime = date('Y-m-d H:i:s' , time());
                            $hotelEvent->ctime = date('Y-m-d H:i:s' , time());
                            $hotelEvent->status = $hotel->status;
                            $hotelEvent->save();
                        }
                    }

                    //copy trip_day_poi
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $pois = TripDayPoi::model()->findAll($criteria);
                    if(!empty($pois)) {
                        foreach($pois as $poi) {
                            $poiEvent = new TripDayPoi;
                            $poiEvent->trip_id = $new_trip_id;
                            $poiEvent->trip_day_id = $new_trip_day_id;
                            $poiEvent->poi_id = $poi->poi_id;
                            $poiEvent->type = $poi->type;
                            $poiEvent->poi_name= $poi->poi_name;
                            $poiEvent->desc = $poi->desc;
                            $poiEvent->cover_image = $poi->cover_image;
                            $poiEvent->price = $poi->price;
                            $poiEvent->note = $poi->note;
                            $poiEvent->lat = $poi->lat;
                            $poiEvent->lon = $poi->lon;
                            $poiEvent->sort = $poi->sort;
                            $poiEvent->mtime = date('Y-m-d H:i:s' , time());
                            $poiEvent->ctime = date('Y-m-d H:i:s' , time());
                            $poiEvent->status = $poi->status;
                            $poiEvent->save();
                        }
                    }

                    //copy trip_day_traffic
                    $criteria = new CDbCriteria;
                    $criteria->addCondition("trip_day_id='{$trip_day_id}'");
                    $criteria->addCondition("status>=0");
                    $traffics = TripDayTraffic::model()->findAll($criteria);
                    if(!empty($traffics)) {
                        foreach($traffics as $traffic) {
                            $traffic = $traffic->attributes;
                            $new_traffic = new TripDayTraffic;
                            $new_traffic->trip_id = $new_trip_id;
                            $new_traffic->trip_day_id = $new_trip_day_id;
                            $new_traffic->note = $traffic['note'];
                            $new_traffic->from_place = $traffic['from_place'];
                            $new_traffic->from_placeid = $traffic['from_placeid'];
                            $new_traffic->to_place = $traffic['to_place'];
                            $new_traffic->to_placeid = $traffic['to_placeid'];
                            $new_traffic->days = $traffic['days'];
                            $new_traffic->traffic_number = $traffic['traffic_number'];
                            $new_traffic->spend = !empty($traffic['spend']) ? $traffic['spend'] : 0;
                            $new_traffic->currency = $traffic['currency'];
                            $new_traffic->start_hours = $traffic['start_hours'];
                            $new_traffic->start_minutes = $traffic['start_minutes'];
                            $new_traffic->end_hours = $traffic['end_hours'];
                            $new_traffic->end_minutes = $traffic['end_minutes'];
                            $new_traffic->ticketcount = $traffic['ticketcount'];
                            $new_traffic->mode = $traffic['mode'];
                            $new_traffic->status = 0;
                            $new_traffic->mtime = date('Y-m-d H:i:s', time());
                            $new_traffic->ctime = date('Y-m-d H:i:s', time());
                            $new_traffic->save();
                        }
                    }

                }
            }
        }

        //copy trip_note
        $trip_list = array();
        $criteria = new CDbCriteria;
        $criteria->addCondition("trip_id='{$sourceId}'");
        $criteria->addCondition("status >='0'");
        $tripNotes = TripNote::model()->findAll($criteria);
        if(!empty($tripNotes)) {
            foreach($tripNotes as $note) {
                $new_trip_note= new TripNote;
                $new_trip_note->trip_id = $new_trip_id;
                $new_trip_note->note = $note->note;
                $new_trip_note->status = $note->status;

                $new_trip_note->save();
            }
        }
        ajax_response(200, '', $trip_list);
    }
}
