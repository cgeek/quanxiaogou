<?php
/**
 * Created by PhpStorm.
 * User: cgeek
 * Date: 8/16/16
 * Time: 11:00 AM
 */

class DemoDayController extends Controller
{
    public $_identity;
    public $_data;
    public $layout = '/layouts/column2';


    public function beforeAction($action)
    {
        $login_user_id = Yii::app()->partner->id;
        if (intval($login_user_id) <= 0) {
            $this->redirect('/');
        }
        return parent::beforeAction($action);
    }


    public function actionPreview()
    {
        $id = Yii::app()->request->getParam('id');
        $type = Yii::app()->request->getParam('type');

        $trip = Trip::model()->findByPk($id);
        if (!empty($trip)) {
            $this->_data['trip'] = $trip;
        }
        $this->_data['id'] = $id;
        $this->_data['type'] = $type;

        $this->render('preview', $this->_data);
    }

    public function actionList()
    {
        $login_user_id = Yii::app()->partner->id;
        if (intval($login_user_id) <= 0) {
            $this->redirect('/');
        }

        $page = Yii::app()->request->getParam('page');

        $demo_trip_day_list = array();

        $pageSize = 10;
        $page = !empty($page) ? intval($page) : 1;
        $status = Yii::app()->request->getParam('status');
        $status = empty($status) ? 1 : $status;
        $offset = ($page - 1) * $pageSize;

        $criteria = new CDbCriteria;
        $criteria->offset = $offset;
        $criteria->limit = $pageSize;
        $criteria->addCondition("status=$status");
        $criteria->addCondition("user_id=$login_user_id");


        $count = DemoTripDay::model()->count($criteria);
        $demo_days = DemoTripDay::model()->findAll($criteria);
        if (!empty($demo_days)) {
            foreach ($demo_days as $demo) {
                $demo = $demo->attributes;
                $demo_trip_day_list[] = $demo;
            }
        }
        $this->_data['trip_list'] = $demo_trip_day_list;

        //分页
        $pages = new CPagination($count);
        $pages->pageSize = $pageSize;
        $pages->pageVar = 'page';
        $pages->applyLimit($criteria);
        $this->_data['pages'] = $pages;
        $this->_data['count'] = $count;

        $this->render('list', $this->_data);

    }

    public function actionDeleteDemoTripDay()
    {
        $demo_day_id = Yii::app()->request->getParam('demo_day_id');

        if (empty($demo_day_id)) {
            ajax_response(404, 'params error');
        }
        $demo_day = DemoTripDay::model()->findByPk($demo_day_id);
        if (empty($demo_day)) {

            ajax_response(404, '找不到线路');
        }

        $r = DemoTripDay::model()->updateByPk($demo_day_id, array('status' => '-1'));
        if ($r) {
            ajax_response(200, '');
        } else {
            ajax_response(200, '服务器正忙，请稍后再试');
        }

    }

    public function actionEdit()
    {
        $demo_day_id = Yii::app()->request->getParam('id');

        $demo_day = DemoTripDay::model()->findByPk($demo_day_id);

        $this->render('edit', array('demo_day' => $demo_day));
    }
}