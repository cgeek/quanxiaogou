<?php
class PoiActionCommand extends CConsoleCommand
{
	public function __construct()
	{
		date_default_timezone_set('Asia/Shanghai');
	}

	public function actionChinaCity()
	{
		
		$db2 =Yii::app()->db2;
		$sql = "SELECT * FROM places WHERE parent_id=2004;";
		$sql_command = $db2->createCommand($sql);
		$citys = $sql_command->queryAll();
		echo "共有找到：" . count($citys) . "条poi";

		$sheng_ids = array();
		if(!empty($citys)) {
			foreach($citys as $city) {
				if(
					//$city['place_name'] == '台湾' || 
					$city['place_name'] == '香港' || 
					$city['place_name'] == '澳门') {
					continue;
				}
				$new_city = new City;
				$new_city->name = $city['place_name'];
				$new_city->name_english = $city['place_name_en'];
				$new_city->name_pinyin = $city['place_name_pinyin'];
				$new_city->country_id = 1;
				$new_city->country_name = "中国";
				$new_city->type = 3;
				$new_city->desc = $city['desc'];
				$new_city->mtime = date('Y-m-d H:i:s', time());
				$new_city->ctime = date('Y-m-d H:i:s', time());
				$new_city->status = 0;

				$new_city->save();
				echo $city['place_name'];
				echo "\n";

				$new_city_id = Yii::app()->db->getLastInsertId();
				$sheng_ids[] = array('parent_id'=>$city['place_id'], 'city_id'=>$new_city_id);
			}
		}

		if(!empty($sheng_ids)) {
			foreach($sheng_ids as $place) {
				$sql = "SELECT * FROM places WHERE parent_id='{$place['parent_id']}';";
				$sql_command = $db2->createCommand($sql);
				$city_list = $sql_command->queryAll();

				if(!empty($city_list)) {
					foreach($city_list as $city) {
						$new_city = new City;
						$new_city->name = $city['place_name'];
						$new_city->name_english = $city['place_name_en'];
						$new_city->name_pinyin = $city['place_name_pinyin'];
						$new_city->parent_id = $place['city_id'];
						$new_city->country_id = 1;
						$new_city->country_name = "中国";
						$new_city->type = 1;
						$new_city->desc = $city['desc'];
						$new_city->mtime = date('Y-m-d H:i:s', time());
						$new_city->ctime = date('Y-m-d H:i:s', time());
						$new_city->status = 0;
		
						$new_city->save();
						echo $city['place_name'];
						echo "\n";
					}
				}
			}
		}

	}


	public function actionHelp()
	{
		$city_id = array(
			'172' => array('city_id'=> 2, 'city_name'=>'沙巴', 'country_id'=>2), //沙巴
			'81' => array('city_id'=> 3, 'city_name'=>'吉隆坡', 'country_id'=>2), //吉隆坡
			'77' => array('city_id'=> 9, 'city_name'=>'兰卡威', 'country_id'=>2), //兰卡威
			'439' => array('city_id'=> 10, 'city_name'=>'马六甲', 'country_id'=>2), //马六甲
			'179' => array('city_id'=> 11, 'city_name'=>'热浪岛', 'country_id'=>3), //热浪岛
			'13' => array('city_id'=> 4, 'city_name'=>'曼谷', 'country_id'=>3), //曼谷
			'171' => array('city_id'=> 5, 'city_name'=>'普吉岛', 'country_id'=>3), //普吉岛
			'4' => array('city_id'=> 6, 'city_name'=>'清迈', 'country_id'=>3), //清迈
			'180' => array('city_id'=> 7, 'city_name'=>'芭堤雅', 'country_id'=>3), //芭堤雅
			'375' => array('city_id'=> 8, 'city_name'=>'苏梅岛', 'country_id'=>3), //苏梅岛
			'435' => array('city_id'=> 12, 'city_name'=>'象岛', 'country_id'=>3), //象岛
			'413' => array('city_id'=> 13, 'city_name'=>'首尔', 'country_id'=>4), //首尔
			'351' => array('city_id'=> 14, 'city_name'=>'济州岛', 'country_id'=>4), //济州岛
			'343' => array('city_id'=> 15, 'city_name'=>'香港', 'country_id'=>1), //香港
			'426' => array('city_id'=> 16, 'city_name'=>'澳门', 'country_id'=>1), //澳门
		);

		foreach($city_id as $key=>$city) {
			$this->_insert_pois($key, $city['city_id'], $city['city_name'], $city['country_id']);
		}
	}

	public function actionSingaporePoi() 
	{
		$this->_insert_pois(96, 102252, '新加坡', 5);
	}

	public function actionJejuPoi() 
	{
		$this->_insert_pois(351, 14, '济州岛', 4);
	}

	public function actionTaiwanPoi() 
	{
		$this->_insert_pois(9, 102207, '台湾', 1);
	}

	private function _insert_pois($board_id , $city_id, $city_name, $country_id) {
		echo "导入 {$city_name} 的POI\n";
		$db2 =Yii::app()->db2;
		$sql = "SELECT pin_id FROM board_pin WHERE status >= 0 AND board_id='{$board_id}';";
		$sql_command = $db2->createCommand($sql);
		$pin_ids = $sql_command->queryAll();
		echo "共有找到：" . count($pin_ids) . "条pin\n";
		if(!empty($pin_ids)) {
			$insert_count = 0;
			foreach($pin_ids as $pin_id) {
				$pin_id = $pin_id['pin_id'];
				//$sql = "SELECT * FROM pin LEFT JOIN poi ON pin.poi_id=poi.poi_id WHERE pin_id='{$pin_id}'";
				$sql = "SELECT * FROM pin WHERE pin_id='{$pin_id}'";
				$sql_command = $db2->createCommand($sql);
				$pin_db = $sql_command->queryRow();
				if($pin_db['poi_id'] > 0) {
					$sql = "SELECT * FROM poi WHERE poi_id='{$pin_db['poi_id']}'";
					$sql_command = $db2->createCommand($sql);
					$poi_db = $sql_command->queryRow();

					if(empty($poi_db)) {
						echo "id 为{$pin_db['poi_id']} 的POI在源数据库表poi中没找到记录! \n";
						continue;
					}

					if($pin_db['pin_type'] == 'hotel') {
						$p = Hotel::model()->findByAttributes(array('qgl_pin_id' => $pin_db['pin_id']));
						if(!empty($p)) {
							continue;
						}

						$new_hotel = new hotel;
						$new_hotel->city_id = $city_id;
						$new_hotel->country_id = $country_id;
						$new_hotel->city_name = $city_name;
						$new_hotel->title = !empty($pin_db['title']) ? $pin_db['title'] : $poi_db['poi_name'];
						$new_hotel->type = hotel;
						$new_hotel->hotel_name_english = $poi_db['poi_name_local'];
						$new_hotel->hotel_name_pinyin = $poi_db['poi_name_pinyin'];
						$new_hotel->cover_image = empty($pin_db['cover_image']) ? '' : 'http://qimages.b0.upaiyun.com/' .$pin_db['cover_image'] . '_medium.jpg' ;
						$new_hotel->phone = $poi_db['phone'];
						$new_hotel->website = $poi_db['website'];
						$new_hotel->address = $poi_db['address'];
						$new_hotel->address_local = $poi_db['address_local'];
						$new_hotel->price = $poi_db['price'];
						$new_hotel->rank = $poi_db['rank'];
						$new_hotel->rating = $poi_db['rating'];
						$new_hotel->intro = $poi_db['intro'];
						$new_hotel->html_content = $pin_db['html_content'];
						$new_hotel->desc = $pin_db['content'];
						
						$new_hotel->tags = $poi_db['tags'];
						$new_hotel->open_time = $poi_db['open_time'];
						$new_hotel->traffic = $poi_db['traffic'];

						$new_hotel->lat = $poi_db['lat'];
						$new_hotel->lon = $poi_db['lon'];
						$new_hotel->area = $poi_db['area'];
						$new_hotel->recommend = $poi_db['recommend_food'];
						$new_hotel->qgl_pin_id = $pin_db['pin_id'];
						$new_hotel->ctime = date('Y-m-d H:i:s', time());
						$new_hotel->mtime = date('Y-m-d H:i:s', time());
						$new_hotel->status = 0;

						if($new_hotel->save()) {
							echo "插入Hotel {$city_name} :  {$pin_db['pin_id']} {$new_hotel->title} 成功! \n";
							$insert_count++;
						} else {
							echo "插入{$city_name} : {$pin_db['pin_id']} 失败! \n";
							var_dump($new_hotel->getErrors());
						}
						
					} else {

						$p2 = Poi::model()->findByAttributes(array('qgl_pin_id' => $pin_db['pin_id']));
						if(!empty($p2)) {
							continue;
						}

						$new_poi = new Poi;
						$new_poi->city_id = $city_id;
						$new_poi->country_id = $country_id;
						$new_poi->city_name = $city_name;
						$new_poi->poi_name = !empty($pin_db['title']) ? $pin_db['title'] : $poi_db['poi_name'];
						$new_poi->type = $pin_db['pin_type'];
						$new_poi->poi_name_local = $poi_db['poi_name_local'];
						$new_poi->poi_name_pinyin = $poi_db['poi_name_pinyin'];
						$new_poi->cover_image = empty($pin_db['cover_image']) ? '' : 'http://qimages.b0.upaiyun.com/' .$pin_db['cover_image'] . '_medium.jpg' ;
						$new_poi->phone = $poi_db['phone'];
						$new_poi->website = $poi_db['website'];
						$new_poi->address = $poi_db['address'];
						$new_poi->address_local = $poi_db['address_local'];
						$new_poi->price = $poi_db['price'];
						$new_poi->rank = $poi_db['rank'];
						$new_poi->rating = $poi_db['rating'];
						$new_poi->intro = $poi_db['intro'];
						$new_poi->html_content = $pin_db['html_content'];
						$new_poi->desc = $pin_db['content'];
						
						$new_poi->tags = $poi_db['tags'];
						$new_poi->open_time = $poi_db['open_time'];
						$new_poi->traffic = $poi_db['traffic'];
						$new_poi->lat = $poi_db['lat'];
						$new_poi->lon = $poi_db['lon'];
						$new_poi->area = $poi_db['area'];
						$new_poi->recommend = $poi_db['recommend_food'];
						$new_poi->qgl_pin_id = $pin_db['pin_id'];
						$new_poi->ctime = date('Y-m-d H:i:s', time());
						$new_poi->mtime = date('Y-m-d H:i:s', time());
						$new_poi->status = 0;
						if($new_poi->save()) {
							echo "插入poi {$city_name} :  {$pin_db['pin_id']} {$new_poi->poi_name} 成功! \n";
							$insert_count++;
						} else {
							echo "插入{$city_name} : {$pin_db['pin_id']} 失败! \n";
							var_dump($new_poi->getErrors());
						}


					}


				}
			}
			echo "共导入{$insert_count}条POI\n";
		}

	}

    /**
     * 从口碑旅行采集
     */
    public function actionCrawlKoubeiLvxing()
    {

        $cityId = '24352';
        $modules = array('hotel', 'restaurant', 'attraction', 'activity', 'shopping');

        foreach($modules as $type) {

            for($i = 1; $i < 1000; $i++) {
                $url = "http://www.koubeilvxing.com/search?lat=0.000000&lng=0.000000&rows=10&module=$type&placeId=$cityId&page=$i";

                $content = file_get_contents($url);
                $content = json_decode($content, true);

                if($content['ret'] != 1 || empty($content['list'])) {
                    echo "no result: $i";

                    break;
                }

                foreach($content['list'] as $item) {
                    echo $item['name_cn'] . "\n";
                }
                sleep(1);
                echo $url . "\n";
            }

            break;
        }
        //http://www.koubeilvxing.com/search?lat=0.000000&lng=0.000000&module=restaurant&page=1&placeId=restaurant&rows=10

    }



}
