<?php

Yii::import('ext.upyun.QUpyun');

class UpyunCommand extends CConsoleCommand
{

	public function actionPurge()
	{
		$sql = "SELECT distinct image_id FROM `image` WHERE 1";
		$sql_command = Yii::app()->db->createCommand($sql);
		$images = $sql_command->query();
		if(empty($images)) {
			//Yii::log("sync_weibo error, user not exist,{$pin['user_id']}",'error');
			return false;
		}
		foreach($images as $image)
		{
			$image_id = $image['image_id'];
			$purge = 'http://qgl-merchant.b0.upaiyun.com/'. $image_id;
			echo $purge . "\n";
			$this->_purge($purge);
		}

	}

	private function _purge($purge = '')
	{
		if(empty($purge))
		{
			return false;
		}
		$user = "xinquan";
		$password = 'miaomiao';
		$bucket = "qgl-merchant";    
		$date = gmdate('D, d M Y H:i:s \G\M\T'); // 取得世界时间   
		$sign = md5($purge."&".$bucket."&".$date."&".md5($password)); // 计算签名
		$process = curl_init('http://purge.upyun.com/purge/');

		// 设置表头参数
		curl_setopt($process, CURLOPT_POST, 1);
		curl_setopt($process, CURLOPT_HTTPHEADER, array("Expect:", 
			"Authorization: UpYun {$bucket}:{$user}:{$sign}", "DATE:{$date}")); 

		curl_setopt($process, CURLOPT_POSTFIELDS, "purge=".urlencode($purge));
		curl_setopt($process, CURLOPT_HEADER, 0);
		curl_setopt($process, CURLOPT_TIMEOUT, 30);
		curl_setopt($process, CURLOPT_RETURNTRANSFER, 1);

		$content = curl_exec($process);
		$ret_code = curl_getinfo($process, CURLINFO_HTTP_CODE);

		if($ret_code != 200)
			echo var_export($content, 1);
		else
			echo var_export(json_decode($content));

		curl_close($process); 

	}


	public function actionUpdateGallery() 
	{
		$sql = "SELECT item_id, shop_id, cover_image FROM `item` WHERE `cover_image` !=''";
		$sql_command = Yii::app()->db->createCommand($sql);
		$images = $sql_command->query();
		if(empty($images)) {
			//Yii::log("sync_weibo error, user not exist,{$pin['user_id']}",'error');
			return false;
		}
		foreach($images as $image)
		{
			$image_id = $image['cover_image'];
			$item_id = $image['item_id'];
			$shop_id = $image['shop_id'];
			if(isValidURL($image_id)) {
				echo $image_id . "\n";
				$this->_downloadCoverImage($image_id, $item_id, $shop_id);
			} else {
				if(strlen($image_id) == 32) {
					$criteria = new CDbCriteria;
					$criteria->addCondition("image_id='{$image_id}'");
					$criteria->addCondition("item_id='{$item_id}'");
					$image_db = Image::model()->find($criteria);
					if(!empty($image_db)) {
						echo $image_id;
						$image_db = $image_db->attributes;
						Image::model()->updateByPk($image_db['item_id'], array('in_gallery'=>1));

					}
				}
			}
		}


	}

	public function _downloadCoverImage($url, $item_id, $shop_id) 
	{
		$image_str = file_get_contents($url); 
		$image_info = get_image_info_from_string($image_str);
		if(empty($image_str) ||empty($image_info) || empty($image_info['image_id']))
		{
			echo "download image null: $url\n";
			return false;
		}
		$image_id = $image_info['image_id'];
		echo "new image" . $image_id . "\n";
		$r = Yii::app()->upyun->upload($image_id, $image_str);
		if(!empty($r)) {
			$new_image = new Image;
			$new_image->image_id = $image_id;
			$new_image->image_url = $url;
			$new_image->width = $image_info['width'];
			$new_image->height = $image_info['height'];
			$new_image->desc = '';
			$new_image->item_id = (isset($item_id) && $item_id > 0 ) ? $item_id : 0;
			$new_image->shop_id = (isset($shop_id) && $shop_id> 0 ) ? $shop_id: 0;
			$new_image->in_gallery = 1;
			$new_image->ctime = time();
			$new_image->status = 0;
			if($new_image->save()) {
				Item::model()->updateByPk($item_id, array('cover_image'=>$image_id));
			} else {
				echo "error $image_id";
			}
		}

	}
}
