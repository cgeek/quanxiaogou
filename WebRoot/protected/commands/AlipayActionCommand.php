<?php 
date_default_timezone_set('asia/shanghai');
class AlipayActionCommand extends CConsoleCommand
{
	public function actionIndex()
	{
		echo "Help:>>>> \n";

	}

	public function actionImport($fileName = '')
	{
		//$content = file_get_contents($fileName);
		//$content = iconv("gb2312","utf-8//IGNORE",$content);

		$line_count = 0;
		$recordArray = array();

		$file = fopen($fileName, "r") or exit("Unable to open file!");
		while(!feof($file))
		{
			//$line = fgets($file);
			$line = stream_get_line($file, 65535, "\n");
			$line = iconv("gb2312","utf-8//IGNORE",$line);
			$recordArray[] = $line;
		}
		fclose($file);

		$line_count = count($recordArray);
		/*
		echo $recordArray[0] . "\n";
		echo $recordArray[1] . "\n";
		echo $recordArray[2] . "\n";
		echo $recordArray[3] . "\n";
		echo $recordArray[4] . "\n";
		echo $recordArray[5] . "\n";
		echo "\n";
		*/
		for($i = 5; $i < $line_count - 5; $i++) {
			if(!empty($recordArray[$i])) {
				$record = explode(',', $recordArray[$i]);
				if(count($record) != 16) {
					echo count($record) . "\n";
					continue;
				}

				var_dump($record);

				//查询是否已经存在
				$new_record = new AlipayRecord;
				$new_record->alipay_order_no = trim($record[0]);
				$new_record->merchant_order_no = trim($record[1]);
				$new_record->create_time = trim($record[2]);
				$new_record->payment_time = trim($record[3]);
				$new_record->modified_time = trim($record[4]);
				$new_record->order_from = trim($record[5]);
				$new_record->order_type = trim($record[6]);
				$new_record->owner_user_id = trim($record[7]);
				$new_record->order_title = trim($record[8]);
				$total_amount = trim($record[9]);
				$new_record->total_amount = !empty($total_amount) ? $total_amount : '0';
				$new_record->in_out_type = trim($record[10]);
				$new_record->order_status = trim($record[11]);

				$new_record->ctime = date('Y-m-d H:i:s', time());
				//$new_record->mtime = date('Y-m-d H:i:s', time());
				if(!$new_record->save()) {
					echo $record[0] . "\n";
					var_dump($new_record->getErrors());
				}
			}
		}
	}

	public function actionOrder2Record()
	{
		$list = array();

		$criteria = new CDbCriteria;
		$criteria->addCondition("status>=0");
		$criteria->order = ' `ctime` DESC';
		$count = Order::model()->count($criteria);
		$orders = Order::model()->findAll($criteria);

		if(!empty($orders)) {
			foreach($orders as $order) {
				$order = $order->attributes;
				if(!empty($order['payout_trade_id'])) {
					$list[] = array('customer_id'=> $order['customer_id'], 'alipay_trade_id' => $order['payout_trade_id']);
				}
				if(!empty($order['payment_trade_id'])) {
					$list[] = array('customer_id'=> $order['customer_id'], 'alipay_trade_id' => $order['payment_trade_id']);
				}
			}
		}

		$count = count($list);
		foreach($list as $item) {
			$recode = null;
			$alipay_trade_id = trim($item['alipay_trade_id']);

			$criteria = new CDbCriteria;
			//$criteria->addSearchCondition('merchant_order_no', $alipay_trade_id);
			$criteria->addCondition("merchant_order_no Like '%$alipay_trade_id%'", "OR");
			$criteria->addCondition("alipay_order_no Like '%$alipay_trade_id%'", "OR");
			$record  = AlipayRecord::model()->find($criteria);

			if(!empty($record)) {
				$record->customer_id = $item['customer_id'];
				if($record->save()) {
					echo "修改成功， $alipay_trade_id \n";
				}
			} else {
				echo "修改失败,  $alipay_trade_id \n";
			}
		}
	}


	public function actionCustomer2Record()
	{
		$list = array();

		$criteria = new CDbCriteria;
		$customers = Customer::model()->findAll($criteria);

		if(!empty($customers)) {
			foreach($customers as $customer) {
				$customer = $customer->attributes;
				if(!empty($customer['taobao_trade_id'])) {
					$list[] = array('customer_id'=> $customer['id'], 'alipay_trade_id' => $customer['taobao_trade_id']);
				}
			}
		}

		$count = count($list);
		foreach($list as $item) {
			$recode = null;
			$alipay_trade_id = trim($item['alipay_trade_id']);

			$criteria = new CDbCriteria;
			//$criteria->addSearchCondition('merchant_order_no', $alipay_trade_id);
			$criteria->addCondition("merchant_order_no Like '%$alipay_trade_id%'", "OR");
			$criteria->addCondition("alipay_order_no Like '%$alipay_trade_id%'", "OR");
			$record  = AlipayRecord::model()->find($criteria);

			if(!empty($record)) {
				$record->customer_id = $item['customer_id'];
				if($record->save()) {
					echo "修改成功， $alipay_trade_id \n";
				}
			} else {
				echo "修改失败,  $alipay_trade_id \n";
			}
		}
	}


	public function actionCustomerMoney()
	{
		
		$criteria = new CDbCriteria;
		$criteria->addCondition('status >= 0');
		$customers = Customer::model()->findAll($criteria);
		if(empty($customers)) {
			echo "没有客户\n";die();
		}
		foreach($customers as $customer) {
			$order_margin = $this->_order_margin($customer->id);
			$record_margin = $this->_record_margin($customer->id);
			echo $customer->contact . "\t".$customer->admin_id. "\t". $customer->destination ."\t". $customer->service_fee ."\t$order_margin\t$record_margin\n"; 
		}


	}

	public function actionOrderMargin()
	{
		$order_margin = 0;
		$order_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("status>=0");
		$criteria->order = ' `ctime` DESC';
		$orders = Order::model()->findAll($criteria);
		if(!empty($orders)) {
			foreach($orders as $order) {
				$order = $order->attributes;
				$margin = 0;
				$margin = round(($order['total_fee'] - $order['payment']), 2);
				$order_margin += $margin;
				$order['margin'] = $margin;


				echo $order['id'] . "\t" . $order['agent'] . "\t"  . $order['title'] . "\t" . $order['total_fee'] . "\t" . $order['payment'] . "\t" . $order['margin'] . "\n"; 
			}
		}

	}


	private function _order_margin($customer_id = 0)
	{
		if(empty($customer_id)) {
			return 0;
		}
		$order_margin = 0;
		$order_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id=". $customer_id);
		$criteria->addCondition("status>=0");
		$criteria->order = ' `ctime` DESC';
		$orders = Order::model()->findAll($criteria);
		if(!empty($orders)) {
			foreach($orders as $order) {
				$order = $order->attributes;
				$margin = 0;
				$margin = round(($order['total_fee'] - $order['payment']), 2);
				$order_margin += $margin;
			}
		}
		return $order_margin;
	}

	private function _record_margin($customer_id)
	{
		if(empty($customer_id)) {
			return 0;
		}
		//交易流水
		$record_margin = 0;
		$record_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id='{$customer_id}'");
		$criteria->addCondition("status>=0");
		$criteria->order = ' `create_time` DESC';
		$count = AlipayRecord::model()->count($criteria);
		$records = AlipayRecord::model()->findAll($criteria);
		if(!empty($records)) {
			foreach($records as $record) {
				$record = $record->attributes;
				if($record['in_out_type'] == '收入') {
					$record_margin += $record['total_amount'];
				} else {
					$record_margin -= $record['total_amount'];
				}
				$record_list[] = $record;
			}
		}
		return $record_margin;
	}
}
