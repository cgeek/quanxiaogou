<?php
Yii::import('ext.sinaWeibo.SinaWeibo',true);
Yii::import('ext.SimpleHTMLDOM.SimpleHTMLDOM');
Yii::import('ext.upyun.QUpyun');

class PushActionCommand extends CConsoleCommand
{

	private $_fp = NULL;
	private $_error_time = 0;

	public $_templates = array(
		'QGL_QM_ERROR' => array(
			'message' => '抱歉，你现在使用的清迈攻略版本有问题，请到AppStore升级最新版本',
			'userInfo' => array('action' => 'app', 'appID' => '485600098')
		)
	);


	public function actionHelp()
	{

	}

	public function actionDoSendPush($clientName = '')
	{
		if(empty($clientName)) {

			die("请输入客户端名称，clientName \n");
		}
		$sql = "SELECT * FROM push_job WHERE status=0 AND client_name='{$clientName}';";
		$sql_command = Yii::app()->db->createCommand($sql);
		$job_queue = $sql_command->queryAll();
		if(empty($job_queue))
		{
			return ;
		}

		//初始化socket连接
		$this->_init_socket($clientName);
		foreach($job_queue as $job)
		{
			$cron_time = strtotime($job['cron_time']);
			$now = time();
			if($cron_time < $now) {
				PushJob::model()->updateByPk($job['id'], array('status'=>1));
				try {
					$r = $this->_do_push_job($job);
					$status = $r ? '2' : '-1';
				} catch (Exception $e) {
					var_dump($e);
					$status = -3;
				}
				PushJob::model()->updateByPk($job['id'], array('status'=>$status));
				sleep(2);
			}
		}
		//关闭socket连接
		$this->_close_socket();
	}

	public function actionSendPushByClient($clientName = '', $clientVersion = '0.0', $templateName = '')
	{

		if(empty($clientName) || empty($clientVersion) || empty($templateName)) {
			echo "请输入客户端名称（client_name） 和 客户端版本号（ client_vsersion ) 和信息模板名称\n";
			echo "usage: ./yiic pushAction sendPushByClient --clientName=QGL_QM --clientVersion=3.0 --templateName=QGL_QM_ERROR\n";
			die();
		}

		$templates = $this->_templates;
		$template = $templates[$templateName];
		if(empty($template)) {
			echo "没有找到模板，请重试\n";
			die();
		}

		$sql = "SELECT * FROM device WHERE client_name='{$clientName}' AND `client_version` LIKE '%{$clientVersion}%' AND `push_token` !='';";
		$sql_command = Yii::app()->db->createCommand($sql);
		$devices = $sql_command->queryAll();
		if(empty($devices))
		{
			echo "没有找到需要发push的用户";
			die();
		}

		$total = count($devices);
		echo "共找到  {$total} 个符合条件的用户 \n";
		foreach($devices as $device) {
			$criteria = new CDbCriteria;
			$criteria->addCondition("uuid='{$device['uuid']}'");
			$has_pushed = PushJob::model()->findAll($criteria);
			if(!empty($has_pushed)) {
				echo $device['uuid'] . "已经添加过任务，跳过... \n";
				continue;
			}
			$new_job = new PushJob;
			$new_job->uuid = $device['uuid'];
			$new_job->push_token = $device['push_token'];
			$new_job->client_name = $device['client_name'];
			$new_job->client_version = $device['client_version'];
			$new_job->message = $template['message'];
			$new_job->action  = $template['userInfo']['action'];
			$new_job->userInfo = json_encode($template['userInfo']);
			$new_job->ctime = date('Y-m-d H:i:s', time());
			$new_job->mtime = date('Y-m-d H:i:s', time());
			$new_job->cron_time = date('Y-m-d H:i:s', time());

			if($new_job->save()) {
				echo $device['uuid'] . "任务添加成功... \n";
			} else {
				$error = $new_job->getErrors();
				var_dump($error);
			}
		}

	}

	public function actionSendPushTest($client_name = NULL)
	{
		if(empty($client_name)) {
			echo "clientName empty\n";
			die();
		}
		$push_token = '4fa7d6392720cf211646ef665be5c441ea290748b10885d1481ddfcbb55c5f35';
		$info = $this->_templates['QGL_QM_ERROR'];
		$message = $info['message'];
		$userInfo = $info['userInfo'];
		$this->_send_push($push_token, $message, 1, $userInfo, $client_name);
	}

	private function _do_push_job($job)
	{
		if(empty($job)) {
			return false;
		}
		$push_token = $job['push_token'];
		$message = $job['message'];
		$userInfo = json_decode($job['userInfo'], true);
		$client_name = $job['client_name'];

		$this->_send_push($push_token, $message, 0, $userInfo, $client_name);
		
		return true;
	}

	private function _init_socket($client_name)
	{
		
		$passphrase = 'qmy110110';

		$ck_file = Yii::app()->basePath .'/../../script/push/ck_' . $client_name . ".pem";
		if(!file_exists($ck_file)) {
			die("证书无法找到，请检查！$ck_file \n");
		}

		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', $ck_file);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

			//'ssl://gateway.sandbox.push.apple.com:2195', $err,
		$this->_fp = stream_socket_client(
			'ssl://gateway.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$this->_fp) 
		{
			echo("Failed to connect: $err $errstr" . PHP_EOL . "\n");
			$this->_error_time ++;
			if($this->_error_time > 5) {
				die("连接错误超过5次，强制停止\n");
			}
		}


		echo "建立socket连接\n";
	}

	private function _close_socket()
	{
		if($this->_fp)
		{
			echo "关闭连接\n";
			fclose($this->_fp);
		}
	}

	private function _send_push($deviceToken = '', $message = '', $badge = 0, $userInfo = NULL, $client_name='') 
	{
		if(empty($deviceToken) || empty($message)) {
			return false;
		}

		if(!$this->_fp) {
			$this->_init_socket($client_name);
		}
		if(!$this->_fp) {
			return false;
		}
		/*
		$passphrase = 'qmy110110';

		$ck_file = Yii::app()->basePath .'/../../script/push/ck_' . $client_name . ".pem";
		if(!file_exists($ck_file)) {
			return false;
		}
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', $ck_file);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		$fp = stream_socket_client(
			//'ssl://gateway.sandbox.push.apple.com:2195', $err,
			'ssl://gateway.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
			//echo("Failed to connect: $err $errstr" . PHP_EOL);
			return false;
		 */

		$defaultAPSInfo = array(
			'alert' => $message,
			'badge' => $badge,
			'sound' => 'default'
		);

		if (is_null($userInfo)) {
			$body['aps'] = $defaultAPSInfo;
		} else {
			$body['aps'] = array_merge($defaultAPSInfo, $userInfo);
		}

		$payload = json_encode($body);

		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($this->_fp, $msg, strlen($msg));

		// Close the connection to the server
		// fclose($fp);
		$time = date('Y-m-d H:i:s', time());
		if (!$result) {
			echo "send push failed:$time, $deviceToken, $message \n";
			return false;
		} else {
			echo "send push success:$time, $deviceToken, $message \n";
			return true;
		}
	}
}
