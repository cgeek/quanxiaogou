<?php

class StatsCommand extends CConsoleCommand
{

	public function actionWeixin($d = 0)
	{
		if(empty($d))
		{
			$d = time() - 86400;
		}else
		{
			$d = strtotime($d);
		}

		$logFile =  '/var/log/httpd/quanxiaogou-access_*';

		$cmd = 'grep "GET /weixin/about" ' . $logFile . ' | grep "' . date('d/M/Y', $d) . '"';
		echo $cmd, "\n";

		exec($cmd, $logs);

		$count = 0;
		foreach ($logs as $log)
		{
			$patten = '/^\S+ \S+ \S+ \[[^\]]+\] "[A-Z]+ ([^ ]*) [^"]*" \d+ \d+$/m';

			preg_match($patten, $log, $matches); 
			$url = $matches[1];
			$query = parse_url($url);
			if(isset($query['query']))
			{
				parse_str($query['query'], $param);
			}
			if(isset($param['uuid']))
			{
				$count ++;
			}else{
				//just test
			}


		}

		$object = 'page-weixin-about';
		$date = date('Y-m-d 00:00:00', $d);
		$entry = Stats::model()->findByAttributes(array('object' => $object, 'date'=> $date, 'type' => 'day'));

		if(empty($entry))
		{
			$new_stats = new Stats;
			$new_stats->object = $object;
			$new_stats->count = $count;
			$new_stats->date = $date;
			$new_stats->type = 'day';
			$new_stats->save();
		}else{
			$entry->count = $count;
			$entry->save();
		}
	}

	public function actionAd($d = 0)
	{
		if(empty($d))
		{
			$d = time() - 86400;
		}else
		{
			$d = strtotime($d);
		}

		/*
		$logFile =  '/var/log/httpd/quanxiaogou-access_log';
		$logFile =  '/tmp/quanxiaogou-access_log';
		if(7 == date('N', $d))
		{
			$logFile =  '/tmp/quanxiaogou-access_log-'.date('Ymd', $d);
		}
		echo $logFile . "\n";
		 */
		$logFile =  '/var/log/httpd/quanxiaogou-access_*';

		$cmd = 'grep "GET /customer/ad" ' . $logFile . ' | grep "' . date('d/M/Y', $d) . '"';
		echo $cmd, "\n";

		exec($cmd, $logs);

		$stats = array();
		$stats['all'] = 0;
		foreach ($logs as $log)
		{
			$patten = '/^\S+ \S+ \S+ \[[^\]]+\] "[A-Z]+ ([^ ]*) [^"]*" \d+ \d+$/m';

			preg_match($patten, $log, $matches); 
			$url = $matches[1];
			$query = parse_url($url);
			if(isset($query['query']))
			{
				parse_str($query['query'], $param);
			}
			if(isset($param['client_name']))
			{
				if(!isset($stats[$param['client_name']]))
				{
					$stats[$param['client_name']] = 0;
				}
				$stats[$param['client_name']] ++;
			}

			$stats['all'] ++;

		}
		foreach($stats as $client_name=>$count)
		{
			$object = 'page-ad-client-' . $client_name;
			$date = date('Y-m-d 00:00:00', $d);
			$entry = Stats::model()->findByAttributes(array('object' => $object, 'date'=> $date, 'type' => 'day'));

			if(empty($entry))
			{
				$new_stats = new Stats;
				$new_stats->object = $object;
				$new_stats->count = $count;
				$new_stats->date = $date;
				$new_stats->type = 'day';
				$new_stats->save();
			}else{
				$entry->count = $count;
				$entry->save();
			}
			
		}


	}

}
