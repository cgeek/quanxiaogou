<?php
date_default_timezone_set('asia/shanghai');

class MetaActionCommand extends CConsoleCommand
{

	public function actionCrontab()
	{
		$sql = "SELECT * FROM meta_job WHERE status=0;";
		$sql_command = Yii::app()->db->createCommand($sql);
		$job_queue = $sql_command->queryAll();
		if(empty($job_queue))
		{
			return ;
		}
		foreach($job_queue as $job)
		{
			$cron_time = strtotime($job['cron_time']);
			$now = time();
			if($cron_time < $now) {
				MetaJob::model()->updateByPk($job['id'], array('status'=>1));
				$action_name = $job['meta_action'];
				try {
					$r = $this->$action_name($job['params']);
					$status = $r ? '2' : '-1';
				} catch (Exception $e) {
					$status = 3;
				}
				MetaJob::model()->updateByPk($job['id'], array('status'=>$status));
			}
		}
	}
	

	//异步抓取item的cover_image，比如淘宝导入保存的商品
	public function crawl_item_cover_image($params) 
	{
		$params = json_decode($params, TRUE);
		$item_id = isset($params['item_id']) ? $params['item_id'] : 0;
		if ($item_id <= 0) {
			return false;
		}
		$item = Item::model()->findByPk($item_id);
		if(empty($item))
		{
			return false;
		}
		$item = $item->attributes;
		if(empty($item['cover_image']) || !isValidURL($item['cover_image'])) {
			return false;
		}

		$url = $item['cover_image'];
		$image_str = curl_get($url);
		$image_info = get_image_info_from_string($image_str);
		if(empty($image_str) ||empty($image_info) || empty($image_info['image_id']))
		{
			echo "Crawl error: $url \n";
			return false;
		}
		$image_id = $image_info['image_id'];
		$r = Yii::app()->upyun->upload($image_id, $image_str);
		if(!empty($r)) {
			$new_image = new Image;
			$new_image->image_id = $image_id;
			$new_image->image_url = $url;
			$new_image->width = $image_info['width'];
			$new_image->height = $image_info['height'];
			$new_image->desc = '';
			$new_image->item_id = $item['item_id'];
			$new_image->in_article = 1;
			$new_image->in_gallery = 1;
			$new_image->is_avatar = 0;
			$new_image->user_id = $item['user_id'];
			$new_image->shop_id = $item['shop_id'];
			$new_image->ctime = time();
			$new_image->status = 0;

			if($new_image->save()) {
				Item::model()->updateByPk($item['item_id'], array('cover_image'=>$image_id));
			} else {
				//保存失败
				return false;
			}
		}

		return TRUE;
	}
	
	
	public function actionSendPushTest()
	{
		$params = '{"push_token":"4fa7d6392720cf211646ef665be5c441ea290748b10885d1481ddfcbb55c5f35","message":"cherrycha: \u56de\u590d\u6d4b\u8bd5","client_name":"Phuket","badge":1,"userInfo":{"action":"openURL","url":"local:\/\/id\/10005"}}';
		//$params = '{"push_token":"a81e72ea4b4c1f2ae5deb0df55f09b214645b1dcf875a87a56c984dd1c7ede83","message":"cherrycha: \u56de\u590d\u6d4b\u8bd5","client_name":"QGL_QM","badge":1,"userInfo":{"action":"poiDetail","pinID":"4487","url":"poiDetail:\/\/4\/pai\/4487"}}';
		$this->send_push($params);

	}

	public function send_push($params) 
	{
		$params = json_decode($params, TRUE);
		if(empty($params['push_token']) || empty($params['message']) || empty($params['client_name'])) {
			return FALSE;
		}
		$this->_send_push($params['push_token'], $params['message'], $params['badge'], $params['userInfo'], $params['client_name']);
		return TRUE;
	}

	public function send_weixin_alert($params) 
	{
		$params = json_decode($params, TRUE);
		if(empty($params['push_token']) || empty($params['message']) || empty($params['client_name'])) {
			return FALSE;
		}
		$this->_send_push($params['push_token'], $params['message'], $params['badge'], $params['userInfo'], $params['client_name']);
		return TRUE;
	}

	private function _send_push($deviceToken = '', $message = '', $badge = 1, $userInfo = NULL, $client_name='') 
	{
		if(empty($deviceToken) || empty($message)) {
			return false;
		}
		$passphrase = 'qmy110110';

		$ck_file = Yii::app()->basePath .'/../../push/ck_' . $client_name . ".pem";
echo $ck_file;die();
		if(!file_exists($ck_file)) {
			return false;
		}
		$ctx = stream_context_create();
		stream_context_set_option($ctx, 'ssl', 'local_cert', $ck_file);
		stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);
		
		$fp = stream_socket_client(
			//'ssl://gateway.sandbox.push.apple.com:2195', $err,
			'ssl://gateway.push.apple.com:2195', $err,
			$errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);
		if (!$fp)
			//echo("Failed to connect: $err $errstr" . PHP_EOL);
			return false;
		$defaultAPSInfo = array(
			'alert' => $message,
			'badge' => $badge,
			'sound' => 'default'
		);

		if (is_null($userInfo)) {
			$body['aps'] = $defaultAPSInfo;
		} else {
			$body['aps'] = array_merge($defaultAPSInfo, $userInfo);
		}

		$payload = json_encode($body);

		// Build the binary notification
		$msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

		// Send it to the server
		$result = fwrite($fp, $msg, strlen($msg));

		// Close the connection to the server
		fclose($fp);
		if (!$result) {
			echo "send push failed: $deviceToken, $message \n";
			return false;
		} else {
			echo "send push success: $deviceToken, $message \n";
			return true;
		}
	}
}
