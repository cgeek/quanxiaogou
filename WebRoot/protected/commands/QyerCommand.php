<?php

date_default_timezone_set('Asia/Shanghai');

class QyerCommand extends CConsoleCommand
{
    private $page = 1;
    private $post = array(
            'client_id' => 'qyer_android',
            'client_secret' => '9fcaae8aefc4f9ac4915',
        );

	public function actionTest()
	{
		echo 'test';
    }

    public function actionUpdateAllCountry()
    {
        $url = 'http://open.qyer.com/place/common/get_all_country';
        $r = http($url, $this->post, 'POST');
        if(empty($r)) {
            die('http error!');
        }

        $r = json_decode($r, true);
        if(empty($r) || empty($r['data'])) {
            die('no data error!');
        }

        $place_list = array();
        foreach($r['data'] as $k => $section) {
            if($k ==0) {
                continue;
            }
            foreach($section['hotcountrylist'] as $country) {
                $country['is_hot'] = 1;
                $country['continent'] = $section['catename'];
                $country['continent_en'] = $section['catename_en'];
                $place_list[$country['pid']] = $country;
            }
            foreach($section['countrylist'] as $country) {
                $country['continent'] = $section['catename'];
                $country['continent_en'] = $section['catename_en'];
                $place_list[$country['pid']] = $country;
            }
        }

        foreach($place_list as $place) {
            //先查询是否已经存在
            $place_db = Place::model()->findByPk($place['pid']);
            if(!empty($place_db)) {
                continue;
            }
            $new_place = new Place;
            $new_place->id = $place['pid'];
            $new_place->country_id = 0;
            $new_place->is_country = 1;
            $new_place->place_name = $place['catename'];
            if(!empty($place['catename_en'])) {
                $new_place->place_name_en = $place['catename_en'];
            }
            if(!empty($place['continent'])) {
                $new_place->continent = $place['continent'];
            }
            if(!empty($place['continent_en'])) {
                $new_place->continent_en = $place['continent_en'];
            }
            if(!empty($place['is_hot'])) {
                $new_place->is_hot = $place['is_hot'];
            }
            if(!empty($place['count'])) {
                $new_place->count = $place['count'];
            }
            if(!empty($place['flag'])) {
                $new_place->flag = $place['flag'];
            }

            if($new_place->save()) {
                echo "发现新目的地：{$place['catename']} {$place['pid']}\n";
            } else {
                echo "插入失败！新目的地：{$place['catename']} {$place['pid']}\n";
            }
        }
    }

    public function actionUpdateCountryInfo()
    {
        $url = 'http://open.qyer.com/place/country/get_country_info';

		$criteria = new CDbCriteria;
		$criteria->addCondition("is_country=1");
		$count = Place::model()->count($criteria);
		$place_list = Place::model()->findAll($criteria);
        if(!empty($place_list)) {
            foreach($place_list as $place) {
                if($place->flag != 1) {
                    continue;
                }
                $this->post['countryid'] = $place->id;
                $r = http($url, $this->post, 'POST');
                if(empty($r)) {
                    echo "http error! $place->id\n";
                    continue;
                }
                $r = json_decode($r, true);
                if(empty($r) || empty($r['data'])) {
                    echo "json error! $place->id\n";
                    continue;
                }
				$r = $r['data'];
                if(!empty($r['catename_en'])) {
                    $place->place_name_en = $r['catename_en'];
                }
                if(!empty($r['photo'])) {
                    $place->photos = json_encode($r['photo']);
                }
                if(!empty($r['photocount'])) {
                    $place->photocount = $r['photocount'];
                }
                if(!empty($r['overview_url'])) {
                    $place->overview_url = $r['overview_url'];
                }
                if(!empty($r['isguide'])) {
                    //$place->is_guide= intval($r['is_guide']);
                    $place->is_guide= intval($r['isguide']); //20160625修改
                }

                if($place->save()) {
                    echo "更新成功： $place->id , $place->place_name \n";
                } else {
                    echo "更新失败： $place->id , $place->place_name \n";
                }
            }
        }

    }

    public function actionUpdateCityList()
    {
        $url = 'http://open.qyer.com/place/city/get_city_list';

		$criteria = new CDbCriteria;
        $criteria->addCondition("flag=1");
        //$criteria->addInCondition('id', array(11));
		$count = Place::model()->count($criteria);
		$place_list = Place::model()->findAll($criteria);
        if(empty($place_list)) {
            echo "No place to get city! \n";
            die();
        }
        foreach($place_list as $country) {
            if($country->flag != 1) {
                continue;
            }
            $page = 1;
            $this->post['countryid'] = $country->id;
            while($page > 0) {
                $this->post['page'] = $page;
                $r = http($url, $this->post, 'POST');
                if(empty($r)) {
                    echo "http error!\n";
                    continue;
                }

                $r = json_decode($r, true);
                if(empty($r) ) {
                    echo "json error!\n";
                    continue;
                }
                if(empty($r['data'])) {
                    $page = -1;
                    continue;
                }
                foreach($r['data'] as $place) {
                    //先查询是否已经存在
                    $place_db = Place::model()->findByPk($place['id']);
                    if(!empty($place_db)) {
                        echo "已经存在：$place_db->id ; $place_db->place_name\n";
                        continue;
                    }
                    $new_place = new Place;
                    $new_place->id = $place['id'];
                    $new_place->country_id = $country->id;
                    $new_place->is_country = 0;
                    $new_place->place_name = $place['catename'];
                    if(!empty($place['catename_en'])) {
                        $new_place->place_name_en = $place['catename_en'];
                    }
                    $new_place->cover_image = $place['photo'];
                    $new_place->flag = 2;
                    if($place['ishot']) {
                        $new_place->is_hot = 1;
                    }
                    if($place['isguide']) {
                        $new_place->is_guide = 1;
                    }
                    if(!empty($place['lat'])) {
                        $new_place->lat = $place['lat'];
                    }
                    if(!empty($place['lng'])) {
                        $new_place->lng = $place['lng'];
                    }
                    if(!empty($place['representative'])) {
                        $new_place->representative = $place['representative'];
                    }
                    if(!empty($place['beennumber'])) {
                        $new_place->beennumber= $place['beennumber'];
                    }

                    if($new_place->save()) {
                        echo "发现新城市：{$place['catename']} \n";
                    }
                }
                $page++;
                if(count($r['data']) < 20) {
                    $page = -1;
                }
            }
        }

    }

    public function actionUpdateCityInfo()
    {
        echo "todo \n";
    }

    public function  actionUpdatePoiList($place_id = NULL)
    {
        $url = 'http://open.qyer.com/place/poi/get_poi_list';

        $criteria = new CDbCriteria;
        $criteria->addCondition("flag=2");
        if(!empty($place_id)) {
            $criteria->addCondition("id=$place_id");
        }
        //$criteria->addInCondition('id', array(50));
		$count = Place::model()->count($criteria);
		$place_list = Place::model()->findAll($criteria);
        if(empty($place_list)) {
            echo "No place to get poi! \n";
            die();
        }

        foreach($place_list as $place) {
            if($place->poi_crawled == 1 && empty($place_id)) {
                continue;
            }
            $page = 1;
            $this->post['cityid'] = $place->id;
            while($page > 0) {
                $this->post['page'] = $page;
                $r = http($url, $this->post, 'POST');
                if(empty($r)) {
                    echo "http error!\n";
                    continue;
                }

                $r = json_decode($r, true);
                if(empty($r) ) {
                    echo "json error!\n";
                    continue;
                }
                if(empty($r['data'])) {
                    $page = -1;
                    echo "没有更多了\n";
                    continue;
                }
                foreach($r['data'] as $poi) {
                    //先查询是否已经存在
                    $poi_db = Poi::model()->findByPk($poi['id']);
                    if(!empty($poi_db)) {
                        echo "已经存在：$poi_db->id ; $poi_db->firstname \n";
                        continue;
                    }
                    $new_poi = new Poi;
                    $new_poi->id = $poi['id'];
                    $new_poi->place_id = $place->id;
                    $new_poi->firstname = $poi['firstname'];
                    $new_poi->secnodname = $poi['secnodname'];
                    $new_poi->chinesename = $poi['chinesename'];
                    $new_poi->englishname = $poi['englishname'];
                    $new_poi->localname = $poi['localname'];
                    $new_poi->lat = $poi['lat'];
                    $new_poi->lng = $poi['lng'];
                    $new_poi->grade = $poi['grade'];
                    $new_poi->gradescores = $poi['gradescores'];
                    $new_poi->beennumber = $poi['beennumber'];
                    $new_poi->recommendnumber = $poi['recommendnumber'];
                    $new_poi->recommendstr = $poi['recommendstr'];
                    $new_poi->cover_image = $poi['photo'];
                    $new_poi->photo = $poi['photo'];

                    if($new_poi->save()) {
                        echo "插入成功：{$poi['firstname']} \n";
                    } else {
                        echo "插入失败：{$poi['firstname']} \n";
                    }

                }

                $page++;
                if(count($r['data']) < 20) {
                    $page = -1;
                }
                usleep('2000');
            }

            usleep('3000');
            $place->poi_crawled = 1;
            $place->save();
        }
    }
    
    public function actionUpdatePoiDetail($place_id = NULL)
    {
        $url = 'http://open.qyer.com/poi/get_one';

		$criteria = new CDbCriteria;
        if(!empty($place_id)) {
            $criteria->addCondition("place_id=$place_id");
        }
        $count = Poi::model()->count($criteria);
        //分页
        $pageSize = 100;
        $totalPage = intval($count / $pageSize) + 1;

        for($i = 0; $i < $totalPage; $i++) {
            $limit = $pageSize;
            $offset = $i * $limit;

		    $criteria = new CDbCriteria;
            $criteria->offset = $offset;
            $criteria->limit = $limit;
            //$criteria->addCondition("is_update=0");
            $criteria->addCondition("cateid=0");
            $poi_list = Poi::model()->findAll($criteria);
            if(empty($poi_list)) {
                continue;
            }
            foreach($poi_list as $poi) {
                if($poi->is_update == 1) {
                    echo $poi->id . "跳过\n"; 
                    continue;
                }
                echo $poi->qyerurl . "\n"; 
                $this->post['poi_id'] = $poi->id;
				$r = http($url, $this->post, 'POST');
                if(empty($r)) {
                    echo "http error!\n";
                    continue;
                }

                $r = json_decode($r, true);
                if(empty($r) ) {
                    echo "json error!\n";
                    continue;
                }
                if(empty($r['data'])) {
					//var_dump($r);
					echo $poi->id . "没有数据 \n"; 
                    continue;
                }

                $poi = $poi->attributes;
                $this->_update_poi_info($poi['id'], $poi, $r['data']);
            }
            echo $offset . "\n"; 
        }

    }

    private function _update_poi_info($poi_id, $poi, $new_poi)
	{
        $update = array();
        $update_key = array('firstname','secnodname', 'chinesename', 'englishname', 'cateid', 'cate_name', 'localname', 'lat', 'lng', 'mapstatus', 'grade', 'gradescores', 'introduction', 'address', 'site', 'phone', 'wayto', 'opentime', 'price', 'tips', 'updatetime', 'commentcounts', 'duration', 'img_count', 'recommendnumber', 'recommendstr', 'recommendscores', 'qyerurl');

        foreach($update_key as $key) {
            if(empty($poi[$key]) && !empty($new_poi[$key])) {
                $update[$key] = $new_poi[$key];
            }
        }
        if(empty($poi['photo']) && !empty($new_poi['photo'])) {
            $update['photo'] = $new_poi['photo'];
        }
        if(empty($poi['images']) && !empty($new_poi['img'])) {
            $update['images'] = json_encode($new_poi['img']);
        }
        if(empty($poi['comment_list']) && !empty($new_poi['comment_list'])) {
            $update['comment_list'] = json_encode($new_poi['comment_list']);
		}
		echo $new_poi['cateid'] . "  " . $new_poi['cate_name']  . "  - {$poi['cateid']} {$poi['cate_name']}";
        $update['is_update'] = 1;
        $update['last_crawl_time'] = date('Y-m-d H:i:s', time());
        Poi::model()->updateByPk($poi_id, $update);

        echo "更新成功：{$poi['firstname']} \n";
        //var_dump($update);
        //die();
    }

    public function actionUpdatePoiComments($place_id = NULL)
    {

        $url = 'http://open.qyer.com/poi/comment/get_list';

		$criteria = new CDbCriteria;
        if(!empty($place_id)) {
            $criteria->addCondition("place_id=$place_id");
        }
        $count = Poi::model()->count($criteria);

        //分页
        $pageSize = 100;
        $totalPage = intval($count / $pageSize) + 1;

        for($i = 0; $i < $totalPage; $i++) {
            $limit = $pageSize;
            $offset = $i * $limit;

		    $criteria = new CDbCriteria;
            if(!empty($place_id)) {
                $criteria->addCondition("place_id=$place_id");
            }
            $criteria->offset = $offset;
            $criteria->limit = $limit;
            $poi_list = Poi::model()->findAll($criteria);
            if(empty($poi_list)) {
                continue;
            }
            foreach($poi_list as $poi) {
                echo "poi:" . $poi->id . "\n"; 
                if($poi->comment_crawled == 1) {
                    echo "has crawled\n";
                    continue;
                }
                if($poi->commentcounts <= 0) {
                    echo "no comment\n";
                    continue;
                }
                $p = 0;
                $pg = intval($poi->commentcounts / 10) + 1;
                echo "\n pg: $pg\n" ;
                for($i = 1; $i <= $pg; $i++) {
                    $this->post['poiid'] = $poi->id;
                    $this->post['page'] = $i;
                    $r = http($url, $this->post, 'POST');
                    if(empty($r)) {
                        echo "http error!\n";
                        continue;
                    }
                    $r = json_decode($r, true);
                    if(empty($r) ) {
                        echo "json error!\n";
                        continue;
                    }
                    if(empty($r['data']) || empty($r['data']['list'])) {
                        echo "no comments \n";
                        continue;
                    }
                    foreach($r['data']['list'] as $comment) {
                        //var_dump($comment);die();
                        $this->_update_comment($poi->id, $comment);
                    }
                }
                Poi::model()->updateByPk($poi->id, array('comment_crawled'=> 1));
            }
            echo $offset . "\n"; 
        }

    }

    private function _update_comment($poi_id, $comment)
    {
        $exsit = Comment::model()->findByAttributes(array('object' => 'poi', 'object_id'=>$poi_id, 'source' =>'qyer', 'source_id'=> $comment['id']));
        if(!empty($exsit)) {
            echo "评论已经存在，{$comment['id']} \n";
            return ;
        }
        
        $new_comment = new Comment;
        $new_comment->content = $comment['comment'];
        $new_comment->star = $comment['star'];
        $new_comment->source = 'qyer';
        $new_comment->source_id = $comment['id'];
        $new_comment->ctime = date('Y-m-d H:i:s', $comment['datetime']);
        $new_comment->mtime = date('Y-m-d H:i:s', time());
        $new_comment->object = 'poi';
        $new_comment->object_id = $poi_id;
        $new_comment->status = 0;

        if($new_comment->save()) {
            echo "插入评论成功，{$comment['id']}\n";
        } else {
            echo "插入评论失败，{$comment['id']}\n";

        }

        //插入用户
        $qyer_user = $comment['user'];
        if(empty($qyer_user)) {
            echo "no user \n";
            return ;
        }

        $user = QyerUser::model()->findByPk($qyer_user['uid']);
        if(!empty($user)) {
            //echo "user exist {$qyer_user['email']}\n";
            return ;
        }

        $new_user = new QyerUser;
        $new_user->uid = $qyer_user['uid'];
        $new_user->username = $qyer_user['username'];
        $new_user->gender = $qyer_user['gender'];
        $new_user->email = $qyer_user['email'];
        $new_user->bio = $qyer_user['bio'];
        $new_user->avatar = $qyer_user['avatar'];

        if($new_user->save()) {
            echo "插入用户成功 {$qyer_user['email']} \n";
        } else {
            var_dump($new_user->getErrors());
            echo "插入用户失败 {$qyer_user['email']} \n";
        }

    }


    public function actionUpdatePoiPhotos($place_id = NULL)
    {

        $url = 'http://open.qyer.com/poi/get_pic_list';

		$criteria = new CDbCriteria;
        if(!empty($place_id)) {
            $criteria->addCondition("place_id=$place_id");
        }
        $count = Poi::model()->count($criteria);

        //分页
        $pageSize = 100;
        $totalPage = intval($count / $pageSize) + 1;

        for($i = 0; $i < $totalPage; $i++) {
            $limit = $pageSize;
            $offset = $i * $limit;

		    $criteria = new CDbCriteria;
            if(!empty($place_id)) {
                $criteria->addCondition("place_id=$place_id");
            }
            $criteria->offset = $offset;
            $criteria->limit = $limit;
            $poi_list = Poi::model()->findAll($criteria);
            if(empty($poi_list)) {
                continue;
            }
            foreach($poi_list as $poi) {
                echo $poi->id . "\n"; 
                $this->post['poi_id'] = $poi->id;
                $r = http($url, $this->post, 'POST');
                if(empty($r)) {
                    echo "http error!\n";
                    continue;
                }

                $r = json_decode($r, true);
                if(empty($r) ) {
                    echo "json error!\n";
                    continue;
                }
                if(empty($r['data']) || empty($r['data']['photo_list'])) {
                    echo "no photos \n";
                    continue;
                }
                foreach($r['data']['photo_list'] as $photo) {
                    $this->_update_photo($poi->id, $photo);
                 //   var_dump($photo);die();
                }

            }
            echo $offset . "\n"; 
        }

    }

    private function _update_photo($poi_id, $photo)
    {
        //判断photo是否已经存在
        if(empty($photo['url'])) {
            return ;
        }
        $url = $photo['url'];
        //正则替换url : http://pic.qyer.com/album/user/734/27/RkpRQB0EYw/index/       
        $match = "/^http:\/\/pic.qyer.com\/[\w+|\/]+index\//";
        preg_match($match, $photo['url'], $arr);
        if(!empty($arr[0])) {
            $url = $arr[0];
        }
        $exsit = Image::model()->findByAttributes(array('object' => 'poi', 'object_id'=>$poi_id, 'source_hash' => md5($url)));
        if(!empty($exsit)) {
            echo "已经存在，{$photo['url']} \n";
            return ;
        }
        
        $new_image = new Image;
        $new_image->source_url = $url;
        $new_image->source_hash = md5($url);
        $new_image->ctime = date('Y-m-d H:i:s', $photo['created']);
        $new_image->mtime = date('Y-m-d H:i:s', time());
        $new_image->status = 0;
        $new_image->object = 'poi';
        $new_image->object_id = $poi_id;

        if($new_image->save()) {
            echo "插入成功，{$photo['url']}\n";
        }
        //$image_str = http($pic_url);
        //var_dump($image_str);
        //$image_info = get_image_info($image_str);
        //var_dump($image_info);
        //die();       
    }


    public function actionDownloadPoiCoverImage($place_id = NULL)
    {
		$criteria = new CDbCriteria;
        if(!empty($place_id)) {
            $criteria->addCondition("place_id=$place_id");
        }
        $count = Poi::model()->count($criteria);

        //分页
        $pageSize = 100;
        $totalPage = intval($count / $pageSize) + 1;

        for($i = 0; $i < $totalPage; $i++) {
            $limit = $pageSize;
            $offset = $i * $limit;

		    $criteria = new CDbCriteria;
            if(!empty($place_id)) {
                $criteria->addCondition("place_id=$place_id");
            }
            $criteria->offset = $offset;
            $criteria->limit = $limit;
            $poi_list = Poi::model()->findAll($criteria);
            if(empty($poi_list)) {
                continue;
            }
            foreach($poi_list as $poi) {
                $poi_id = $poi->id;
                if(empty($poi->cover_image)) {
                    continue;
                }
                $url = $poi->cover_image;
                $match = "/^http:\/\/pic.qyer.com\/[\w+|\/]+index\//";
                preg_match($match, $url, $arr);
                if(!empty($arr[0])) {
                    $url = $arr[0];
                } else {
                    //地址格式不正确
                    echo "地址格式不正确: $poi_id: $url \n";
                    continue;
                }
                $exist = Image::model()->findByAttributes(array('object' => 'poi', 'object_id'=>$poi_id, 'source_hash' => md5($url)));
                if(empty($exist)) {
                    echo $url . "\n";
                    $new_image = new Image;
                    $new_image->source_url = $url;
                    $new_image->source_hash = md5($url);
                    $new_image->ctime = date('Y-m-d H:i:s', time());
                    $new_image->mtime = date('Y-m-d H:i:s', time());
                    $new_image->status = 0;
                    $new_image->object = 'poi';
                    $new_image->object_id = $poi_id;

                    if($new_image->save()) {
                        echo "插入成功，{$url}\n";
                        $exist = Image::model()->findByAttributes(array('object' => 'poi', 'object_id'=>$poi_id, 'source_hash' => md5($url)));
                    }
                }
                if(empty($exist)) {
                    continue;
                }

                $path = Yii::app()->basePath . '/../upload/';
                if(!empty($exist->image_hash) && file_exists($path . $exist->image_hash)) {
                    //更新poi coverimage
                    Poi::model()->updateByPk($poi_id, array('cover_image' => $exist->image_hash));
                } else {
                    //下载
                    try {
                        $image_info = $this->_downloadImage($url. 'w1080');
                        if($image_info == false) {
                            echo "下载失败：$poi_id, {$image_info['image_hash']} \n";
                            continue;
                        }
                        if(file_exists($path . $image_info['image_hash'])) {
                            echo "下载成功：$poi_id, {$image_info['image_hash']} \n";
                            Image::model()->updateByPk($exist->id, $image_info);
                            Poi::model()->updateByPk($poi_id, array('cover_image' => $image_info['image_hash']));
                        } else {
                            echo "下载失败：$poi_id, {$image_info['image_hash']} \n";
                        }
                    } catch (Exception $e) {
                        echo 'Caught exception: ',  $e->getMessage(), "\n";
                    }
                }
                //die();

            }
        }
        
    }

    private function _downloadImage($url)
    {
        if(empty($url)) {
            return false;
        }
        $r = http($url, '', 'GET', 'http://place.qyer.com');
        $hash = md5($r);
        $file = Yii::app()->basePath . '/../upload/' . $hash;

        if(strstr($r, 'error')) {
            echo "地址错误：$url \n";
            return false;
        }
        file_put_contents($file, $r);
        $image_info = get_image_info($r);
        //var_dump($image_info);
        return $image_info;
    }

}
