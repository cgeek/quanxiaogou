<?php
/**
 * Controller is the customized base controller class.
 * All controller classes for this application should extend from this base class.
 */
class ApiController extends CController
{
	public $_data = array();

	public $layout='//layouts/column2';

	private $format = 'json';

	public function init()
	{
		
	}

}
