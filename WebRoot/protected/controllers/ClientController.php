<?php

class ClientController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	// -----------------------------------------------------------
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/

	public function actionNewAdminPushToken()
	{

		$token = Yii::app()->request->getPost('push_token');
		$user = Yii::app()->request->getPost('admin');

		$return_data = array();

		if (!is_null($token)) {
			
			$adminToken = AdminPushToken::model()->find("push_token=:token", array(":token" => $token));

			if(is_null($adminToken)) {
				$adminToken = new AdminPushToken();
			}

			$adminToken->push_token = $token;
			if (!is_null($user)) {
				$adminToken->admin = $user;
			}

			if (!$adminToken->save()) {
				http_response_code(500);
				error_log($adminToken->getErrors());
			}

		}else {
			http_response_code(400);
		}

		header('Content-Type: application/json');

		Yii::app()->end();
	}
}