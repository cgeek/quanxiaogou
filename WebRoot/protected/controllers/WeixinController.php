<?php

class WeixinController extends Controller
{
    private $TOKEN = 'qxglx';

    //private $AppID = 'wx0bbb13955c7dbdf9';
    //private $AppSecret = '1f4ba0ba9bde6028c0e73312bf049e00';

	private $AppID = 'wxefa13c47da9734fe';
	private $AppSecret = 'ddd07d481d6929ebe02bbe93156cbba2';
	
	private  $EncodingAESKey = 'Y991ZohOMFuOgk1jH5uEWkEA7ApzWssI9sgB0JLkKkS';
	
    private $AccessToken = '';

	public function actionIndex()
	{
		echo "it's working !";
	}

	public function actionNewCustomer()
	{
		$source = Yii::app()->request->getParam('source');
		$source = empty($source) ? 'weixin' : $source;
		$this->_data['source'] = $source;

		$this->_data['weixin_uuid'] = Yii::app()->request->getParam('weixin_uuid');

		$this->renderPartial('new_customer', $this->_data);
	}

	public function actionAbout()
	{
		$content = WeixinContent::model()->findByPk(1)->attributes;
		
		$this->_data['content'] = $content;

		$this->renderPartial('article', $this->_data);
	}

	public function actionTeam()
	{
		$content = WeixinContent::model()->findByPk(2)->attributes;
		
		$this->_data['content'] = $content;

		$this->renderPartial('article', $this->_data);
	}

	/**
	 * weixin 消息接收处理
	 *
	 */
	public function actionMessage()
	{
		//get post data, May be due to the different environments
		$postStr = isset($GLOBALS["HTTP_RAW_POST_DATA"]) ? $GLOBALS["HTTP_RAW_POST_DATA"] : '';

		//extract post data
		if (!empty($postStr)){
			$postObj = simplexml_load_string($postStr, 'SimpleXMLElement', LIBXML_NOCDATA);
			$message['fromUsername'] = $postObj->FromUserName;
			$message['toUsername'] = $postObj->ToUserName;
			$message['CreateTime'] = $postObj->CreateTime;
			$message['msgType'] = $msgType = $postObj->MsgType;

			$this->_saveMessage($postObj);

			$resultStr = '';
			if($msgType == 'event') {
				//定义发送默认消息
				$message['event'] = $postObj->Event;
				if($message['event'] == 'subscribe') {
					$resultStr = $this->_responseWelcome($message);
					$this->_insertPushMeta('有新用户关注');
				}

			} else if($msgType == 'text') {
				//自动回复用户文字消息
				$message['content'] = trim($postObj->Content);
				$resultStr = $this->_responseText($message);

				$this->_insertPushMeta($message['content']);
			} else if($msgType == 'location') {
				//自动回复用户地理信息消息
				$message['lat'] = $postObj->Location_X;
				$message['lon'] = $postObj->Location_Y;
				$message['scale'] = $postObj->Scale;
				$message['label'] = $postObj->Label;
				$resultStr = $this->_responseLocation($message);

				$this->_insertPushMeta('用户发来一个地理位置分享');
			} else if($msgType == 'image') {
				//自动回复用户图片消息
				$message['picUrl'] = $postObj->PicUrl;
				$resultStr = $this->_responseText($message);

				$this->_insertPushMeta('用户发来一张图片');

			}
			//error_log($resultStr);
			echo $resultStr;
		} else {
			echo "";
			exit;
		}
	}


	private function _responseText($message)
	{
		if($message['content'] == 'welcome') {
			return $this->_responseWelcome($message);
		}
	}

	/**
	 * 发送文本消息 
	 */
	private function _sendText($message, $content)
	{
		$resutlStr = '';
		$textTpl = "<xml>
						<ToUserName><![CDATA[%s]]></ToUserName>
						<FromUserName><![CDATA[%s]]></FromUserName>
						<CreateTime>%s</CreateTime>
						<MsgType><![CDATA[%s]]></MsgType>
						<Content><![CDATA[%s]]></Content>
						<FuncFlag>0</FuncFlag>
						</xml>";
		$resultStr = sprintf($textTpl, $message['fromUsername'], $message['toUsername'], time(), 'text', $content);
		return $resultStr;

	}

	/**
	 * 发送图文列表 
	 */
	private function _sendNews($message, $news_list) {

		$items = '<ArticleCount>' .  count($news_list) . '</ArticleCount>';
		$items .= '<Articles>';
		foreach($news_list as $key=>$news) {
			$items .= '<item>';
			$items .= "<Title><![CDATA[" . cut_str($news['content'], 20) . "]]></Title>";
			$items .= "<Description><![CDATA[" . cut_str($news['content'],20) . "]]></Description>";
			$items .= "<PicUrl><![CDATA[" . $news['image_url'] . "]]></PicUrl>";
			$items .= "<Url><![CDATA[" . $news['link'] . "]]></Url>";
			$items .= '</item>';
		}
		$items .= '</Articles>';
		$LocationTpl = "<xml>
						<ToUserName><![CDATA[%s]]></ToUserName>
						<FromUserName><![CDATA[%s]]></FromUserName>
						<CreateTime>%s</CreateTime>
						<MsgType><![CDATA[%s]]></MsgType>";
		$LocationTplFooter = "
						<FuncFlag>0</FuncFlag>
					</xml>";
		$resultStr = sprintf($LocationTpl, $message['fromUsername'], $message['toUsername'], time(), 'news') . $items . $LocationTplFooter;
		return $resultStr;
	}

	/**
	 *  回应用户个人信息页面链接
	 */
	private function _responseMyInfo($message)
	{
		$weixin_uuid = $message['fromUsername'];
		$customer = Customer::model()->findByAttributes(array('weixin_uuid' => $weixin_uuid));
		if(empty($customer)) {
			$content = '你还没有申请定制服务，回复“定制”，联系我们的私人旅游顾问';
			return $this->_sendText($message, $content);
		}
		if(empty($customer['trip_id'])) {
			$content = '你的私人旅行顾问正在加紧为你制定行程中呢，联系我们的私人旅游顾问';
			return $this->_sendText($message, $content);
		}

		$welcome_list = array();
		$welcome_list[] = array('content' => '查看我的行程', 
			//'link' => 'http://www.quanxiaogou.com/weixin/myInfo?weixin_uuid=' . $weixin_uuid,
			'link' => 'http://www.quanxiaogou.com/plan/export/app?trip_id=' . $customer['trip_id'],
			'image_url' => 'http://www.quanxiaogou.com/images/flight.jpg'
		);

		return $this->_sendNews($message, $welcome_list);
	}

	public function actionMyInfo()
	{
		$weixin_uuid = Yii::app()->request->getParam('weixin_uuid');
		$customer = Customer::model()->findByAttributes(array('weixin_uuid' => $weixin_uuid));
		if(empty($customer)) {
			return $this->_responseNotfind();
		}
		$customer = $customer->attributes;

		$this->_data['customer'] = $customer;

		$this->pageTitle =  $customer['contact'] ;

		$order_margin = 0;
		$order_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id=". $customer['id']);
		$criteria->addCondition("status=0");
		$criteria->order = ' `ctime` DESC';
		$orders = Order::model()->findAll($criteria);
		if(!empty($orders)) {
			foreach($orders as $order) {
				$order = $order->attributes;
				$order_list[] = $order;
			}
		}

		$this->_data['order_list'] = $order_list;

		$traveler_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id=". $customer['id']);
		$criteria->addCondition("status=0");
		$criteria->order = ' `ctime` ASC';
		$count = Traveler::model()->count($criteria);
		$travelers = Traveler::model()->findAll($criteria);
		if(!empty($travelers)) {
			foreach($travelers as $traveler) {
				$traveler = $traveler->attributes;
				$traveler_list[] = $traveler;
			}
		}
		$this->_data['traveler_list'] = $traveler_list;

		$this->render('my_info', $this->_data);
	}

	private function _responseLocation($message, $keyword = '')
	{
		return '';
	}

	private function _responseImage($message)
	{
		return '';
	}

	private function _responseWelcome($message)
    {
        $welcome_text = "Hi~欢迎关注世界游官方微信~mo-呲牙
            世界游致力于提供国内外最新的旅游资讯、优惠信息，最优的旅行线路、最好的旅游服务支持。
            世界游咨询电话：400-7280-727（9:00-18:00）";

        return $this->_sendText($message, $welcome_text);
	}

	/**
	 * weixin 接口前面校验
	 *
	 */
	//public function actionMessage()
	public function actionValid()
    {
        $echoStr = $_GET["echostr"];
        //valid signature , option
        if($this->checkSignature()){
        	echo $echoStr;
        	exit;
        }
	}


	private function checkSignature()
	{
        $signature = $_GET["signature"];
        $timestamp = $_GET["timestamp"];
        $nonce = $_GET["nonce"];	
        		
		$token = $this->TOKEN;
		$tmpArr = array($token, $timestamp, $nonce);
		sort($tmpArr, SORT_STRING);
		$tmpStr = implode( $tmpArr );
		$tmpStr = sha1( $tmpStr );
		
		if( $tmpStr == $signature ){
			return true;
		}else{
			return false;
		}
	}

	public function actionMessageTest()
	{
		$message = new WeixinMessage;
		$message->FromUserName = 'aaa';
		$message->ToUserName = 'aaa';
		$message->MsgType = 'text';
		$message->save();

		var_dump($message->getErrors());

	}

	private function _saveMessage($postObj)
	{
		if(empty($postObj)) {
			return ;
		}
		$message = new WeixinMessage;

		$message->MsgType  = $msgType = $postObj->MsgType;
        $message->FromUserName = $postObj->FromUserName;
        $message->ToUserName = $postObj->ToUserName;
        $message->CreateTime = $postObj->CreateTime;
        $message->MsgId = $postObj->MsgId;

		if($msgType == 'text') {
			$message->Content = $postObj->Content;
		}
        $message->save();

        //check user info

	}

	private function _insertPushMeta($pushMessage = NULL)
	{
		$adminPushTokens = $this->_findAllAdminPushToken();

		foreach ($adminPushTokens as $admin) {
			$metaJob = new MetaJob;
			$metaJob->meta_action = 'send_weixin_alert';
			
			$metaJob->ctime = time();

			$metaJob->cron_time = date("y-m-d h:i:s", time() + 60);

			$params = array(
					"push_token" => $admin->push_token,
					"message" => $pushMessage,
					"client_name" => "QGLAdmin",
					"userInfo" => array(),
					"badge" => 1
				);

			$metaJob->params = CJSON::encode($params);

			if (!$metaJob->save()) {
				echo $metaJob->getErrors();
			}

			$metaJob->save();
		}
	}

	private function _findAllAdminPushToken()
	{
		$criteria = new CDbCriteria;
		$criteria->select = "push_token";
		$criteria->condition = "push_token is not NULL";
		$adminPushTokens = AdminPushToken::model()->findAll($criteria);

		return $adminPushTokens;
	}

	public function actionTestPushTokens()
	{
		$all = $this->_insertPushMeta("test");

		Yii::app()->end();
	}

    public function actionSyncUserList()
    {
        $api = 'https://api.weixin.qq.com/cgi-bin/user/get?access_token=';

        $access_token = $this->_getAccessToken();

        $api .= $access_token;
        $r = curl_get($api);
        $r = json_decode($r, true);

        if(isset($r['errcode']) && $r['errcode'] == '40001') {
            $api .= $this->_getAccessTokenRemote();
            $r = curl_get($api);
            $r = json_decode($r, true);
        }
        if(empty($r['data']) || empty($r['data']['openid'])) {
            error_log('weixin : no user list');
            return false;
        }
        foreach($r['data']['openid'] as $openid) {
            $this->_check_weixin_user($openid, $access_token);
        }

    }

    private function _check_weixin_user($openid, $access_token) {
        $weixin_user = WeixinUser::model()->findByAttributes(array('openid'=>$openid));
        if(empty($weixin_user)) {
            $weixin_user_remote = $this->_getWeixinUserRemote($openid, $access_token);

            $new_weixin_user = new WeixinUser;
            $new_weixin_user->openid = $weixin_user_remote['openid'];
            $new_weixin_user->nickname = $weixin_user_remote['nickname'];
            $new_weixin_user->sex = $weixin_user_remote['sex'];
            $new_weixin_user->language = $weixin_user_remote['language'];
            $new_weixin_user->city = $weixin_user_remote['city'];
            $new_weixin_user->province = $weixin_user_remote['province'];
            $new_weixin_user->country = $weixin_user_remote['country'];
            $new_weixin_user->headimgurl = $weixin_user_remote['headimgurl'];
            $new_weixin_user->subscribe_time = $weixin_user_remote['subscribe_time'];
            $new_weixin_user->subscribe= $weixin_user_remote['subscribe'];
            $new_weixin_user->remark = $weixin_user_remote['remark'];

            $r = $new_weixin_user->save();
            if(empty($r)) {
                error_log('new weixin user error:');
                //var_dump($new_weixin_user->getErrors());
                return false;
            }
        }

        return true;
    }


    private function _getAccessToken()
    {
        $access_token = '';
        $access = SystemOptions::model()->findByAttributes(array('key'=>'weixin_access_token'));
        if( empty($access) || empty($access->value) || (time() - strtotime($access->mtime) > 7200)) {
            $access_token = $this->_getAccessTokenRemote();
        } else {
            $access_token = $access->value;
        }

        return $access_token;
    }


    private function _getAccessTokenRemote()
    {   
        $api = "https://api.weixin.qq.com/cgi-bin/token?grant_type=client_credential&appid=" . $this->AppID ."&secret=$this->AppSecret";

        $r = curl_get($api);
        $r = json_decode($r, TRUE);

        $token = $r['access_token'];
        if(empty($token)) {
            error_log('weixin can not get weixin access_token');
            return '';
        }
        $access_token = SystemOptions::model()->findByAttributes(array('key'=>'weixin_access_token'));
        $access_token->value = $token;
        $access_token->save();

        return $token;
    }

    private function _getWeixinUserRemote($openid = '', $access_token = '')
    {
        $user = array();
        if(empty($openid)) {
            return $user;
        }
        if(empty($access_token)) {
            $access_token = $this->_getAccessToken();
        }

        $api = "https://api.weixin.qq.com/cgi-bin/user/info?access_token=$access_token&openid=$openid&lang=zh_CN";
        $r = curl_get($api);
        $user = json_decode($r, true);

        return $user;
    }


    public function actionMessageList()
    {
        $page = Yii::app()->request->getParam('page');
        $openid = Yii::app()->request->getParam('openid');
        $msgType = Yii::app()->request->getParam('msgType');


		$message_list = array();

		$pageSize = 20;
		$page = !empty($page) ? intval($page) : 1;
        $offset = ($page - 1) * $pageSize;


		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;
        $criteria->order = ' `CreateTime` DESC';
        if(!empty($openid)) {
		    $criteria->addCondition("FromUserName='{$openid}'");
        }
		    $criteria->addCondition("MsgType='text'");
		$count = WeixinMessage::model()->count($criteria);
		$messages = WeixinMessage::model()->findAll($criteria);
		if(!empty($messages)) {
			foreach($messages as $message) {
                $message = $message->attributes;
                $weixin_user = array();
                $user = WeixinUser::model()->findByAttributes(array('openid'=>$message['FromUserName']));
                if(!empty($user)) {
                    $weixin_user = $user->attributes;
                }
                $message['user'] = $weixin_user;
				$message_list[] = $message;
			}
		}
		$this->_data['message_list'] = $message_list;
		$this->_data['count'] = $count;
		ajax_response('200', 'message_list', $this->_data);
    }

    public function actionReplyMessage()
    {
        
        $openid = Yii::app()->request->getParam('openid');
        $content = Yii::app()->request->getParam('content');

        $access_token = $this->_getAccessToken();
        $api = "https://api.weixin.qq.com/cgi-bin/message/custom/send?access_token=".$access_token;

        $reply = array();
        $reply['touser'] = $openid;
        $reply['msgtype'] = 'text';
        $reply['text'] = array('content'=>"$content");

        $reply_str = json_encode($reply);

        //echo $reply_str; die();
        $r = curl_post($api, $reply_str);
        $r = json_decode($r, true);

        if(isset($r['errcode']) && $r['errcode'] == 0) {
            ajax_response(200, '回复成功');
        } else {
            ajax_response(500, '回复成功');
        }
    }

}
