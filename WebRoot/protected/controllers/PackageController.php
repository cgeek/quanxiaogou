<?php

class PackageController extends Controller
{
	public $_data;

	public function actionIndex()
	{
		$this->pageTitle = '产品';

		$package_list = array();

		$criteria = new CDbCriteria;
		$criteria->offset = 0;
		$criteria->limit = 20;
		$criteria->addCondition("status=0");
		$criteria->addCondition("type='hotel'");
		$criteria->order = ' `ctime` DESC';
		$count = Package::model()->count($criteria);
		$packages = Package::model()->findAll($criteria);
		if(!empty($packages)) {
			foreach($packages as $package) {
				$package = $package->attributes;
				$package_list[] = $package;
			}
		}
		//$this->_data['package_list'] = $package_list;

		$this->_data['hotel_rows'] = $this->_format_rows($package_list);

		//echo json_encode($this->_data['hotel_rows']);die();
		$this->renderPartial('index', $this->_data);
	}

	private function _format_rows($package_list)
	{
		$rows = array();
		if(empty($package_list)) {
			return $rows;
		}
		$grid = array(
					'total' => 8,
					'row_count' => array(2,5,7),
					'type' => array(
						array('row' => 1, 'col' => 2, 'col_width' => array(1,2)),
						array('row' => 1, 'col' => 3, 'col_width' => array(1,1,1)),
						//array('row' => 2, 'col' => 2, 'col_width' => array(2,1,1))
						array('row' => 1, 'col' => 3, 'col_width' => array(1,1,1))
					)
				);

		$row_id = $col_num = 0;
		foreach($package_list as $key => $package) {
			if($key > $grid['total']) {
				continue;
			}
			if(in_array($key, $grid['row_count'])) {
				$row_id++;
			}

			$rows[$row_id]['row'] = $grid['type'][$row_id]['row'];
			$rows[$row_id]['col'] = $grid['type'][$row_id]['col'];

			//计算宽度,高度
			$col_pos = ($row_id <= 0) ? $key : intval($key - $grid['row_count'][$row_id - 1]);
			$col_num = $grid['type'][$row_id]['col_width'][$col_pos];

			//$package['col_width'] = intval($col_num) == 2 ? 'double' : '';
			$package['col_width'] = intval($col_num);
			if($col_num == 2 && $grid['type'][$row_id]['row'] == 2) {
				$package['col_height'] = 'double';
			} else {
				$package['col_height'] = '';
			}

			$rows[$row_id]['packages'][] = $package;
		}
		return $rows;
	}

	public function actionDetail($id = NULL) 
	{
		$this->renderPartial('detail', $this->_data);
	}

}
