<?php

class CustomerController extends Controller
{
	public function actionIndex()
	{
		$this->render('index');
	}

	public function actionNew()
	{
		$this->render('new');
	}

	private function _fillAgentInfoByClientName($client_name)
	{
		$agents = $this->_getAgentsInfo();

		if(empty($_GET['a']))
		{
			switch($_GET['client_name'])
			{
			case 'Taiwan':
				$agent = 'cherrycha';
				$weibo_name = '台湾攻略';
				$weibo_url = 'http://weibo.com/u/2848043012';
				$place_name = '台湾';
				break;
			case 'TaiBei':
				$agent = 'cherrycha';
				$weibo_name = '台湾攻略';
				$weibo_url = 'http://weibo.com/u/2848043012';
				$place_name = '台湾';
				break;

			case 'Bangkok':
				$agent = 'lizzy';
				$weibo_name = '曼谷攻略';
				$weibo_url = 'http://weibo.com/u/2848090432';
				$place_name = '曼谷';
				break;
			case 'QGL_QM':
				$agent = 'lizzy';
				$weibo_name = '清迈攻略';
				$weibo_url = 'http://weibo.com/chiangmaiguide';
				$place_name = '清迈';
				break;
			case 'Phuket':
				$agent = 'lizzy';
				$weibo_name = '普吉岛攻略';
				$weibo_url = 'http://weibo.com/phuketguide';
				$place_name = '普吉岛';
				break;
			case 'Krabi':
				$agent = 'lizzy';
				$weibo_name = '甲米岛攻略';
				$weibo_url = 'http://weibo.com/krabiguide';
				$place_name = '甲米';
				break;
			case 'KohChangIsland':
				$agent = 'lizzy';
				$weibo_name = '清迈攻略';
				$weibo_url = 'http://weibo.com/chiangmaiguide';
				$place_name = '清迈';
				break;
			case 'Samui':
				$agent = 'lizzy';
				$weibo_name = '苏梅岛攻略';
				$weibo_url = 'http://weibo.com/sametguide';
				$place_name = '苏梅岛';
				break;

			case 'KualaLumpur':
				$agent = 'vivian';
				$weibo_name = '吉隆坡攻略';
				$weibo_url = 'http://weibo.com/u/2845742090';
				$place_name = '吉隆坡';
				break;
			case 'Malacca':
				$agent = 'vivian';
				$weibo_name = '马六甲攻略';
				$weibo_url = 'http://weibo.com/u/3535276854';
				$place_name = '马六甲';
				break;
			case 'Sabah':
			default:
				$agent = 'vivian';
				$weibo_name = '马来西亚沙巴攻略';
				$weibo_url = 'http://m.weibo.cn/u/2845764850';
				$place_name = '沙巴';
				break;

			}

		$this->_data['weibo_name'] = $weibo_name;
		$this->_data['weibo_url'] = $weibo_url;
		$this->_data['place_name'] = $place_name;

		} else {
			$agent = $_GET['a'];
			$this->_data['weibo_name'] = $agents[$agent]['default_weibo_name'];
			$this->_data['weibo_url'] = $agents[$agent]['default_weibo_url'];
			$this->_data['place_name'] = $agents[$agent]['default_place_name'];
		}

		$this->_data['agent'] = $agent;

		$this->_data['agent_avatar'] = $agents[$agent]['avatar'];
		$this->_data['agent_name'] = $agents[$agent]['name'];
		$this->_data['agent_phone'] = $agents[$agent]['phone'];
		$this->_data['agent_weixin'] = $agents[$agent]['weixin'];
		$this->_data['agent_qq'] = $agents[$agent]['qq'];


	}

	private function _getAgentsInfo()
	{
		$agents = array(
			'vivian' => array(
				/*
				'name' => '小Vivian',
				'avatar' => 'http://tp4.sinaimg.cn/1810769607/180/5606137678/0',
				'phone' => '18658863069',
				'qq' => '376006397',
				'weixin' => 'vivian872845',
				'default_weibo_name' => '马来西亚沙巴攻略',
				'default_weibo_url' => 'http://m.weibo.cn/u/2845764850',
				'default_place_name' => '沙巴',
				 */
				'name' => 'Cherry Cha',
				'avatar' => 'http://qimages.b0.upaiyun.com/20328908279967b094d1f23ec6393f5c_small.jpg',
				'phone' => '13738173952',
				'qq' => '2829010407',
				'weixin' => 'zhouxiaocha123',
				'default_weibo_name' => '马来西亚沙巴攻略',
				'default_weibo_url' => 'http://m.weibo.cn/u/2845764850',
				'default_place_name' => '沙巴',
			),
			'cherrycha' => array(
				'name' => 'Cherry Cha',
				'avatar' => 'http://qimages.b0.upaiyun.com/20328908279967b094d1f23ec6393f5c_small.jpg',
				'phone' => '13738173952',
				'qq' => '2829010407',
				'weixin' => 'zhouxiaocha123',
				'default_weibo_name' => '台湾攻略',
				'default_weibo_url' => 'http://weibo.com/u/2848043012',
				'default_place_name' => '台湾',
			),
			'lizzy' => array(
				'name' => 'Lizzy',
				'avatar' => 'http://xiaogoulvxing.b0.upaiyun.com/403a515f27fe9e67b0756e1b041b8539_160x160.jpg',
				'phone' => '13064707763',
				'qq' => '2047644107',
				'weixin' => 'yuqian269644',
				'default_weibo_name' => '清迈攻略',
				'default_weibo_url' => 'http://weibo.com/chiangmaiguide',
				'default_place_name' => '清迈',
			),
		);
		return $agents;
	}

	public function actionAd()
	{
		$this->_fillAgentInfoByClientName($_GET['client_name']);

		//params source=banner&client_name=Samui&UUID=42373b67588d444bbafa14c7b439ac2a3315aa33&client_version=3.6.0
		$source = Yii::app()->request->getParam('source');
		$client_name = Yii::app()->request->getParam('client_name');
		$client_version = Yii::app()->request->getParam('client_version');
		$uuid = Yii::app()->request->getParam('UUID');

		$this->_data['source'] = $client_name . '-' . $client_version . '-' . $source;
		$this->_data['platform'] = 'client';
		$this->_data['client_uuid'] = $uuid;

		$this->renderPartial('ad_new', $this->_data);
	}

	public function actionNewAd()
	{
		$this->_fillAgentInfoByClientName($_GET['client_name']);

		//params source=banner&client_name=Samui&UUID=42373b67588d444bbafa14c7b439ac2a3315aa33&client_version=3.6.0
		$source = Yii::app()->request->getParam('source');
		$client_name = Yii::app()->request->getParam('client_name');
		$client_version = Yii::app()->request->getParam('client_version');
		$uuid = Yii::app()->request->getParam('UUID');

		$this->_data['source'] = $client_name . '-' . $client_version . '-' . $source;
		$this->_data['platform'] = 'client';
		$this->_data['client_uuid'] = $uuid;

		$this->renderPartial('ad_new', $this->_data);
	}



	public function actionOnlineContact()
	{
		$agent = empty($_GET['a']) ? 'vivian' : $_GET['a'];
		$agents = $this->_getAgentsInfo();

		$this->_data['agent_avatar'] = $agents[$agent]['avatar'];
		$this->_data['agent_name'] = $agents[$agent]['name'];

		$this->_data['source'] = Yii::app()->request->getParam('source');
		$this->_data['platform'] = 'client';
		$this->_data['client_uuid'] = $uuid;

		$this->renderPartial('online_contact', $this->_data);
	}

	public function actionFaq()
	{
		$this->renderPartial('faq');
	}

	public function actionSubmit()
	{
		$this->render('submit');
	}

	public function actionSaveCustomer()
	{
		$accept_fields = array('contact',  'sex', 'num_of_people', 'start_date', 'end_date', 'has_booked_airline_ticket', 'has_booked_hotel','no_booking','remark', 'mobile', 'qq', 'weixin', 'email', 'platform', 'source', 'weixin_uuid', 'uuid');
		$data = array();
		foreach($accept_fields as $field) {
			if(isset($_POST[$field])) {
				$data[$field] = Yii::app()->request->getParam("{$field}");
			} 
		}
		if(empty($data['has_booked_airline_ticket'])) {
			$data['has_booked_airline_ticket'] = 0;
		}
		if(empty($data['has_booked_hotel'])) {
			$data['has_booked_hotel'] = 0;
		}
		if(empty($data['no_booking'])) {
			$data['no_booking'] = 0;
		}

		$this->_new_customer($data);
	}

	private function _new_customer($data)
	{
		$new_customer = new Customer;
		$new_customer->contact = $data['contact'];
		$new_customer->sex = isset($data['sex']) ? $data['sex'] : '';
		$new_customer->start_date = isset($data['start_date']) ? $data['start_date'] : '0000-00-00';
		$new_customer->end_date = isset($data['end_date']) ? $data['end_date'] : '';
		$new_customer->num_of_people = isset($data['num_of_people']) ? $data['num_of_people'] : '';
		$new_customer->qq = isset($data['qq']) ? $data['qq'] : '';
		$new_customer->remark = $data['remark'];
		$new_customer->mobile = isset($data['mobile']) ? $data['mobile'] :'';

		$new_customer->platform = isset($data['platform']) ? $data['platform'] :'';
		$new_customer->source = isset($data['source']) ? $data['source'] :'';

		$new_customer->weixin_uuid = isset($data['weixin_uuid']) ? $data['weixin_uuid'] :'';

		$new_customer->ctime = date('Y-m-d H:i:s', time());
		$new_customer->status = 0;
		if($new_customer->save()) {
			//sendSMS('18626859311', '有新客户来啦：' . $data['contact']);
			//sendSMS('18626880110', '有新客户来啦：' . $data['contact']);

			pushAdminMessage("新订单来了，冲啊！！！");

			ajax_response('200', '');
		} else {

			ajax_response('500', '');
		}

	}

	public function actionAllJson()
	{
		$customer_list = array();

		$pageSize = 100;
		$page = isset($_GET['page']) ? intval($_GET['page']) : 1;
		$offset = ($page - 1) * $pageSize;

		$criteria = new CDbCriteria;
		$criteria->offset = $offset;
		$criteria->limit = $pageSize;

		if(isset($_GET['status']) && $_GET['status']!='') {
			$criteria->addCondition("status={$_GET['status']}");
		} else {
			$criteria->addCondition("status>0");
		}
		$criteria->order = ' `ctime` DESC';
		$count = Customer::model()->count($criteria);
		$customers = Customer::model()->findAll($criteria);
		if(!empty($customers)) {
			foreach($customers as $customer) {
				$customer = $customer->attributes;
				$customer_list[] = array('customer_id'=> $customer['id'], 'name'=>$customer['contact']);
			}
		}
		$this->_data['customer_list'] = $customer_list;

		ajax_response(200, '', $this->_data);
	}

	public function actionTravelersJson()
	{
		$customer_id = Yii::app()->request->getParam('customer_id');
		$traveler_list = array();
		$criteria = new CDbCriteria;
		$criteria->addCondition("customer_id=". $customer_id);
		$criteria->addCondition("status>=0");
		$criteria->order = ' `id` ASC';
		$count = Traveler::model()->count($criteria);
		$travelers = Traveler::model()->findAll($criteria);

		if(!empty($travelers)) {
			foreach($travelers as $traveler) {
				$traveler = $traveler->attributes;
				
				$new_traveler = array(
					'first_name' => $traveler['first_name'],
					'last_name' => $traveler['last_name'],
					'day' => intval(date('d', strtotime($traveler['birthday']))),
					'month' => intval(date('m', strtotime($traveler['birthday']))),
					'year' => intval(date('Y', strtotime($traveler['birthday']))),
					'sex' => $traveler['sex']
				);
				if($traveler['age'] > 12) {
					$new_traveler['sex'] = ($traveler['sex'] == 'male') ? 'MR' : 'MS';
				} else {
					$new_traveler['sex'] = ($traveler['sex'] == 'male') ? 1 : 2;
				}
				$traveler_list[] = $new_traveler;
			}
		}

		$this->_data['traveler_list'] = $traveler_list;

		ajax_response(200, '', $this->_data);

	}

}
