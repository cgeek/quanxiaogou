<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title><?=isset($content) ? $content['title'] : '妙妙行程管家';?></title>
	<meta name="viewport" content="initial-scale=1.0, maximum-scale=1.0">
	<meta name="apple-mobile-web-app-capable" content="yes">
	<meta name="apple-mobile-web-app-status-bar-style" content="black">
	<meta name="format-detection" content="telephone=no">
	<style>
		html{background:#FFF;color:#000}
		body,div,dl,dt,dd,h1,h2,h3,h4,h5,h6,pre,code,form,fieldset,legend,input,textarea,p,blockquote,th,td{margin:0;padding:0}
		table{border-collapse:collapse;border-spacing:0}
		fieldset,img{border:0}
		h1 {
			font-size:24px;
			font-weight:bold;
		}
		p, div, h5 {
			font-family:Optima;
		}
		body {
			padding:10px;
		}
		p {
			margin:0px;
			padding:0px;
		}
		a.button {
			color: #fff;
			margin-top: 20px;
			margin-bottom: 20px;
			margin-left: 50px;
			text-decoration: none;
			background-color: #5cb85c;
			border-color: #4cae4c;
			display: inline-block;
			padding: 6px 12px;
			font-size: 14px;
			font-weight: normal;
			line-height: 1.428571429;
			text-align: center;
			white-space: nowrap;
			vertical-align: middle;
			cursor: pointer;
			border: 1px solid transparent;
			border-radius: 4px;
		}
		div.agent {
			border:1px solid #ccc;
			background-color: #eee;
			height:82px;
			margin-top:5px;
			margin-bottom:5px;
		}
		div.agent-avatar {
			float:left;
			padding-left:5px;
			padding-right:10px;
		}
		div.agent-avatar img {
			margin:9px 4px;
			border-radius:50%;
			width:60px;
			height:60px;
		}
		div.agent-info {
		}

		div.agent-info h5 {
			margin:2px; 
			margin-top:4px;
			margin-bottom:4px;
		}
		div.agent-info p {
			font-size:12px; 
			margin:0px;
		}

	</style>
	<script src="http://lib.sinaapp.com/js/jquery/1.9.0/jquery.min.js"></script>
</head>
<body>
	 <div class="page-bizinfo">
		<div class="header">
			<h1><?=$content['title'];?></h1>
			<!--p class="activity-info">
				<span id="post-date" class="activity-meta no-extra">2013-11-20</span>
				<span class="activity-meta">妙妙行程管家</span>
				<span id="post-date" class="activity-meta">妙妙行程管家</span>
			</p-->
		</div>
	</div>
	<div class="page-content" style="margin:10px 0;">
		<div class="text">
			<?=$content['content'];?>
		</div>
	</div>

<script style="text/javascript">
$.getUrlParam = function(name)
{
	var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
	var r = window.location.search.substr(1).match(reg);
	if (r!=null) return unescape(r[2]); return null; 
}

$(document).ready(function(){
	var weixin_uuid = $.getUrlParam('weixin_uuid');
	if(weixin_uuid != null) {
		$('a').each(function(){
			url = $(this).attr('href');

			url += ((url.indexOf('?') == -1) ? '?' : '&');
			url += "weixin_uuid=" + weixin_uuid;

			$(this).attr('href', url);
		});
	}
});
</script>
</body>
</html>
