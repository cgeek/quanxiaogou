<!DOCTYPE html>
<html>
<head>
	<title>妙妙行程管家</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家定制" />
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/main.css" />
	
	<script src="/assets/seajs/dist/sea.js"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->

</head>
<body style="margin:0px; padding:0px;">
	<div class="content">
		<div class="container" id="online-contact-form">
			<div class="row" style="margin-top:10px; margin-bottom:10px;">
				<div class="col-xs-11 col-sm-6 col-md-6 col-lg-6" style="padding-left:15px;">
					<div class="pull-left" style="height:60px;">
						<h3 style="margin-top:10px;">我要定制</h3>
						填完后，我们的旅游专家会尽快联系你。
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-0 col-lg-2">
				</div>
				<div class="customer-info-form col-xs-12 col-lg-8">
					<form id="customer-form" class="form-horizontal" role="form" method="POST">
						<input type="hidden" name="source" value="<?=isset($source) ? $source : '';?>">
						<input type="hidden" name="weixin_uuid" value="<?=isset($weixin_uuid) ? $weixin_uuid : '';?>">
						<input type="hidden" name="platform" value="weixin">

						<div class="form-group" style="margin-bottom:2px;">
							<label for="inputContact" class="col-xs-12 col-lg-2 control-label">姓名</label>
							<div class="col-xs-6 col-lg-2">
								<input type="text" name="contact" class="form-control" id="inputContact" placeholder="">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group" style="margin-bottom:2px;">
							<label for="inputMobile" class="col-xs-12 col-lg-2 control-label">手机号</label>
							<div class="col-xs-8 col-lg-3">
								<input type="text" name="mobile" class="form-control" id="inputMobile" placeholder="">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group" style="margin-bottom:2px;">
							<label for="inputMobile" class="col-xs-12 col-lg-2 control-label">QQ（最好填QQ，我们会优先用QQ联系）</label>
							<div class="col-xs-8 col-lg-5">
								<input type="text" name="qq" class="form-control" id="inputQQ" placeholder="选填">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group" style="margin-bottom:2px;">
							<label for="inputRemark" class="col-xs-12 col-lg-3 control-label">需求（目的地、人数、出行日期等）</label>
							<div class="col-xs-0 col-lg-5"></div>
							<div class="col-xs-0 col-lg-2"></div>
							<div class="col-xs-12 col-lg-6">
								<textarea class="form-control" placeholder="如：一家三口，打算明年三月份去普吉，已定机票，求设计行程、代订酒店活动" name="remark" id="inputRemark" rows="3"></textarea>
								<span class="help-block"><span>
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" id="submit-btn" class="col-xs-4 col-lg-2 btn btn-primary">提交</button>
								<span class="help-block"><span>
									</div>
								</div>
							</form>

						</div>
						<div class="col-xs-0 col-lg-2">
						</div>
					</div>
				</div>

				<div class="container" id="post-success-box" style="display:none; margin-top:20px;">
					<div class="alert alert-success clearfix">
						<div class="col-xs-4 col-lg-6" style="padding-right:10px;">
							<img src="http://qimages.b0.upaiyun.com/20328908279967b094d1f23ec6393f5c_small.jpg" class="img-circle pull-right" style="width:60px;">
							<div style="font-size:11px;padding-left:4px;">Cherry Cha</div>
						</div>
						<div class="col-xs-8 col-lg-6 pull-left clearfix" style="padding-left:5px; height:60px; padding-top:5px; padding-right:0px;">
							<h4>提交成功</h4>
							我已经收到了你的信息，会尽快联系你。
						</div>
					</div>
					<div class="col-xs-12 col-lg-6 pull-left clearfix" style="padding-left:0px;margin-top:20px;margin-bottom:10px;">
						如果你比较着急，也可以直接联系我：	
					</div>

					<table class="table table-striped table-bordered">
						<tbody>
							<tr>
								<td><span class="pull-right">姓名：</span></td>
								<td>CherryCha</td>
							</tr>
							<tr>
								<td><span class="pull-right">电话：</span></td>
								<td>13738173952</td>
							</tr>
							<tr>
								<td><span class="pull-right">微信：</span></td>
								<td>xiaogoulvxing</td>
							</tr>
							<tr>
								<td><span class="pull-right">QQ：</span></td>
								<td>
									9139263
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>



<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('customer');
	});
</script>
<script>
document.addEventListener('WeixinJSBridgeReady', function onBridgeReady() { 
	WeixinJSBridge.call('hideToolbar'); 
	//WeixinJSBridge.call('hideOptionMenu'); 
});

</script>


</body>
</html>
