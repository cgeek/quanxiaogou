<div class="container">
	你好，<?=$customer['contact'];?>
	<div class="customer-detail table-responsive">
		<h5>基本资料</h5>
		<table class="table table-bordered">
			<thead>
				<tr style="background:#eee;">
					<th>姓名</th>
					<th>手机号码</th>
					<th>邮箱</th>
					<th>人数</th>
					<th>出发日期</th>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td rowspan="2" style="vertical-align:middle;"><?=$customer['contact'];?></td>
					<td><?=$customer['mobile'];?></td>
					<td><?=$customer['email'];?></td>
					<td><?=$customer['num_of_people'];?></td>
					<td><?=$customer['start_date'];?></td>
				</tr>
			</tbody>
		</table>
		<!-- traveler list-->
		<h5>同行旅客资料</h5>
		<table class="table table-bordered traveler-list-table">
<?php if(!empty($traveler_list)):?>
			<thead>
				<tr style="background:#eee;">
					<th>姓名</th>
					<th>英文名</th>
					<th>性别</th>
					<th>出生日期</th>
					<th>护照号</th>
					<th>护照有效期</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach($traveler_list as $traveler):?>
				<tr>
					<td><?=($traveler['is_customer'] == 1) ? '<strong>' . $traveler['name'] . '</strong>' : $traveler['name'] ;?></td>
					<td><?=$traveler['last_name'];?>  <?=$traveler['first_name'];?></td>
					<td><?=($traveler['sex'] == 'male') ? '男' :'女';?></td>
					<td><?=$traveler['birthday'];?></td>
					<td><?=$traveler['passport_no'];?></td>
					<td><?=$traveler['passport_validity'];?></td>
				</tr>
	<?php endforeach;?>
			</tbody>
<?php else:?>
			<tbody>
				<tr>
					<td colspan=12>暂无旅客信息</td>
				</tr>
			</tbody>
<?php endif;?>
		</table>

		<!-- order-list -->
		<h5>代订项目列表</h5>
		<table class="table table-bordered order-list-table">
<?php if(!empty($order_list)): ?>
			<thead>
				<tr style="background:#eee;">
					<th>代订类型</th>
					<th>代订项目名称</th>
					<th>修改时间</th>
				</tr>
			</thead>
			<tbody>
	<?php foreach($order_list as $key=>$order):?>
				<tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?>>
					<td><?=orderTypeCN($order['type']);?></td>
					<td><strong><?=$order['title'];?></strong></td>
					<td rowspan=2><?=date('Y-m-d',strtotime($order['mtime']));?></td>
				</tr>
	<?php endforeach;?>
			</tbody>
<?php else:?>
			<tbody>
				<tr>
					<td colspan=5>暂无代订项目</td>
				</tr>
			</tbody>
<?php endif;?>
		</table>
	</div>
</div>
