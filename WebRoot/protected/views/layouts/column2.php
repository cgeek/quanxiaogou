<?php $this->beginContent('//layouts/main'); ?>
    <style>
        .sidebar {
            display: none;
        }
        @media (min-width: 768px) {
            .sidebar {
                position: fixed;
                top: 51px;
                bottom: 0;
                left: 0;
                z-index: 1000;
                display: block;
                padding: 20px;
                overflow-x: hidden;
                overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
                background-color: #f5f5f5;
                border-right: 1px solid #eee;
            }
        }
        .main {
            padding: 20px;
        }
        @media (min-width: 768px) {
            .main {
                padding-right: 20px;
                padding-left: 20px;
            }
        }
        .main .page-header {
            margin-top: 0;
        }
    </style>
    <div class=" container-fluid">
        <div class="row" style="margin:0;">
            <div class="col-sm-3 col-md-2 sidebar">

                <?php if($this->id == 'customer' || $this->id == 'customers'):?>
                    <ul class="nav nav-list">
                        <li class="nav-header">流量统计</li>
                        <li class="nav-sub"><a href="/admin/default/stats">客户端Ad浏览量</a></li>
                        <li class="nav-sub"><a href="/admin/default/stats?type=weixin">微信页面浏览量</a></li>
                        <li class="nav-sub"><a href="/admin/default/stats?type=order">客户日增</a></li>
                    </ul>
                    <ul class="nav nav-list">
                        <li class="nav-header">账户管理</li>
                        <li class="nav-sub"><a href="/admin/user/changeInfo">修改资料</a></li>
                        <li class="nav-sub"><a href="/admin/user/changePassword">修改密码</a></li>
                    </ul>
                    <!--ul class="nav nav-list">
                        <li class="nav-header">系统相关</li>
                        <li class="nav-sub"><a target="_blank" href="/admin/oauth/taobao">淘宝后台授权</a></li>
                    </ul-->

                <?php elseif($this->id == 'package'):?>


                <?php endif;?>
                <ul class="nav nav-sidebar">
                    <li class="active"><a href="/user/addCustomer">添加客户 <span class="sr-only">(current)</span></a></li>
                    <li><a href="/user/customers">客户列表</a></li>
                </ul>
            </div>
            <div class="col-sm-9 col-sm-offset-3 col-md-10 col-md-offset-2 main">
                <?php echo $content; ?>
            </div>
        </div>
    </div>



<?php $this->endContent(); ?>


