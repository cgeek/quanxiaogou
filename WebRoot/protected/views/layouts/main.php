<!DOCTYPE html>
<html>
<head>
	<title>妙妙行程管家 - 你的私人旅行顾问！</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/base.css" />
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/seajs/dist/sea.js"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<div id="topbar" class="navbar navbar-inverse navbar-fixed-top">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="/">行程定制ERP</a>
        </div>
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <li class="<?=($this->id == 'dashboard') ? 'active' : '';?>"><a href="/user/dashboard">首页</a></li>
                <li class="<?=($this->id == 'dashboard') ? 'active' : '';?>"><a href="/user/customers">客户</a></li>
                <!--li class="<?=($this->id == 'dashboard') ? 'active' : '';?>"><a href="/user/trips">POI</a></li>
                <li class="<?=($this->id == 'dashboard') ? 'active' : '';?>"><a href="/user/trips">攻略</a></li-->
                <li class="<?=($this->id == 'dashboard') ? 'active' : '';?>"><a href="/user/trips">行程</a></li>
                <li class="<?=($this->id == 'dashboard') ? 'active' : '';?>"><a href="/user/settings">设置</a></li>
            </ul>
        </div>
	</div>

    <div id="content" class="clearfix">
        <?php echo $content; ?>
    </div>

	<!--div class="footer">
		<div class="container">
			<p>妙妙行程管家 © 2013 XiaoGouLvXing.com 版权所有</p>
			<div class="links">合作伙伴:
				<ul>
                    <li><a target="_blank" href="http://www.qiugonglue.com/">求攻略</a></li>
					<li><a target="_blank" href="http://www.blueplanet.cn/">蓝色行星</a></li>
				</ul>
			</div>
		</div>
	</div-->
	<div style="display:none;">
<script type="text/javascript">
var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F53f78afdb90bf194696a6823b9c5aac8' type='text/javascript'%3E%3C/script%3E"));
</script>
	</div>
</body>
</html>
