<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="renderer" content="webkit" />
    <meta name="keywords" content="妙妙行程管家" />
    <meta name="description" content="" />
    <!-- 触发双核浏览器使用 chrome 内核 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="renderer" content="webkit" />
    <link rel="icon" href="http://www.blueplanet.cn/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="http://www.blueplanet.cn/favicon.ico" type="image/x-icon" />
    <link rel="bookmark" href="http://www.blueplanet.cn/favicon.ico" type="image/x-icon"  />

    <![if gte IE 9]>
    <script src="//cdn.bootcss.com/jquery/2.2.3/jquery.js"></script>
    <![endif]>

    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/r29/html5.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.js"></script>
    <script src="//cdn.bootcss.com/jquery/2.2.3/jquery.js"></script>
    <![endif]-->
    <script src="//cdn.bootcss.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link href="//cdn.bootcss.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/magicsuggest/2.1.4/magicsuggest-min.css" rel="stylesheet">
    <script src="//cdn.bootcss.com/magicsuggest/2.1.4/magicsuggest-min.js"></script>
    <link href="//cdn.bootcss.com/normalize/4.2.0/normalize.min.css" rel="stylesheet">

    <link href="/css/index.css?v=20160501" rel="stylesheet"  />
    <title>妙妙行程管家 - 专注行程定制专家</title>
    <link rel="stylesheet" href="/css/header.css?v=20160414" />
</head>
<body>
<div class="top">
    <div class="head">
        <div class="icon">
            <a href="index.html">
                <img src="http://www.blueplanet.cn/img/logo2.png" height="50" title="妙妙行程管家" alt="妙妙行程管家">
            </a>
        </div>
        <div class="h_menu">
            <div class="link">
                <a href="#plan">我要定制</a>
            </div>

            <div class="link" style="padding: 0px;">
                |
            </div>
            <div class="link">
                <a href="/user/login" data-toggle="modal" onclick="loginFlag('2');">登录</a>
            </div>
            <input type="hidden" id="loginFlag">
        </div>
    </div>
</div>
<div class="landingContainer ng-scope">
    <div class="landingContent">
        <div class="filter"></div>
        <div class="slogan">
            <div class="booking-area">
                <div class="book-btn-row">
                    <a href="#plan"><div class="mask_button">预约定制</div></a>
                </div>
                <div class="title">
                    已有<span id="service_count" class="timer" data-form="0" data-to="89379" data-speed="1100">89379</span>位朋友在妙妙行程管家享受到贴心服务
                </div>
            </div>
        </div>
        <video loop="loop" preload="auto" poster="http://public.tratao.com/a2dfca0eec7744db44d8670ac107a53c.jpg" autoplay="">
            <source src="http://public.tratao.com/e581e5d3a84f4df65415e3188e94ce5b.webm" type="video/webm">
            <source src="http://public.tratao.com/94243527ef1843325f542477d0f03053.mp4" type="video/mp4">
            <p>This browser does not support the video element.</p>
        </video>


    </div>
</div>
<div class="plan-create" id="plan">
    <div class="page_content">
        <form action="/customer/add" method="post" id="form_plan">
            <input type="hidden" id="needCode" class="insum" value="0">
            <ul id="slider" style="position: relative; transition: none; width: 960px; left: 0px;">
                <li style="float: left; display: inline; width: 480px;">
                    <div class="page" id="page2">
                        <div class="page_title">在线预约</div>
                        <div class="tr">
                            <div class="name username">
                                <input type="text" class="large realname" name="realName" id="realName" placeholder="您的姓名">

                            </div>
                            <div class="error" id="realName_warn">
                                请输入您的姓名
                            </div>
                        </div>
                        <div class="tr">
                            <div class="mobile">
                                <input type="text" class="large" name="mobile" id="mobile" placeholder="手机号码">
                            </div>
                            <div class="error" id="mobile_warn">
                                请输入手机号
                            </div>
                        </div>
                        <div class="tr">
                            <div class="weixin">
                                <input type="text" class="large" name="weixin" id="weixin" placeholder="微信号">
                            </div>
                        </div>
                        <div class="tr">
                            <textarea class="land" name="note" placeholder="备注" id="remark" placeholder="如：一家三口，打算明年三月份去台湾，已定机票，求规划行程、推荐酒店、当地活动"></textarea>
                        </div>
                        <div class="tr">
                            <div style="width:240px" class="next nextBut" id="submit">提交</div>
                        </div>
                    </div>
                </li>
                <li style="float: left; display: inline; width: 480px;">
                    <div class="page" id="page3">
                        <div class="tr">
                            <div class="look"></div>
                        </div>
                        <div class="tr t1">
                            您的需求已经提交成功。
                        </div>
                        <div class="tr t2">
                            我们将24小时内通过电话联系您。<br>
                            <div style="text-align: left;padding-left: 50px;">
                                1 北京 010-5949 2787<br>
                                2 日本 +81 70 1592 8598
                            </div>
                        </div>
                        <div class="tr">
                        </div>
                        <div class="tr">
                            <a href="index.html">
                                <div class="success">随便逛逛</div>
                            </a>
                        </div>
                    </div>
                </li>
                <!-- Powered by TouchSlider v2.0.1  https://github.com/qiqiboy/touchslider --></ul>
        </form>
    </div>
</div>

<div class="info_service">
    <div class="t"></div>
    <div class="service_content">
        <div class="td">
            <img src="http://xianbei.cc/res/3.0/img/service_aq.png" title="" alt="">
            <div class="title">安全保障</div>
            <div class="description">百里挑一的严格导游审查， </div>
            <div class="description">全天候的台湾当地团队。</div>
            <div class="description">车导信息完整备案，</div>
            <div class="description"> 500万车险保您出行安全。</div>
        </div>
        <div class="td">
            <img src="http://xianbei.cc/res/3.0/img/service_mx.png" title="" alt="">
            <div class="title">明星服务</div>
            <div class="description">管家式私导服务，</div>
            <div class="description">舒适座驾专车接送。</div>
            <div class="description">全程陪同翻译，细致景点讲解，</div>
            <div class="description">让您宾至如归。</div>
        </div>
        <div class="td">
            <img src="http://xianbei.cc/res/3.0/img/service_zy.png" title="" alt="">
            <div class="title">专业团队</div>
            <div class="description">最资深的当地人导游，</div>
            <div class="description">最专业的行程顾问。</div>
            <div class="description">一对一贴心服务，</div>
            <div class="description">带给您最有深度的自由行之旅。 </div>
        </div>
        <div class="td">
            <img src="http://xianbei.cc/res/3.0/img/service_jg.png" title="" alt="">
            <div class="title">价格实惠</div>
            <div class="description">不跟团，不自助，</div>
            <div class="description">无任何隐形消费。</div>
            <div class="description">行业内价格最低，服务最优，</div>
            <div class="description">让您物超所值。 </div>
        </div>
    </div>
</div>

<div class="footer_content" style="line-height:100px;">
    <div class="content">


        <div class="clear"></div>
        <div class="copyright">
            Copyright©2014  沪ICP备14047867号&nbsp;&nbsp; <a href="/">妙妙行程管家</a>&nbsp;&nbsp;|&nbsp;&nbsp;服务时间：9:00~19:00

        </div>

    </div>
</div>