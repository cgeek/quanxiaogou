<div style="margin-top:44px;">
    <ol class="breadcrumb">
        <li><a href="/user/dashboard">管理首页</a></li>
        <li><a href="/user/customers">客户列表</a></li>
        <li class="active">我的客户</li>
    </ol>

    <div class="customer_list clearfix">
        <div class="filter-bar clearfix" style="margin-bottom:10px;">
            <form class="form-inline" role="form">
                <div class="pull-left">
                    <!--状态过滤:
                    <a href="/user/customers?user_id=<?=isset($user_id) ? $user_id : '';?>&filter_key=<?=isset($filter_key) ? $filter_key : '';?>" class="btn <?=$status0_btn_class?>" style="padding-top:3px; padding-bottom:3px;">所有</a>
                    <a href="/user/customers?user_id=<?=isset($user_id) ? $user_id : '';?>&filter_key=<?=isset($filter_key) ? $filter_key : '';?>&status=1" class="btn <?=$status1_btn_class?>" style="padding-top:3px; padding-bottom:3px;">进行中</a>
                    <a href="/user/customers?user_id=<?=isset($user_id) ? $user_id : '';?>&filter_key=<?=isset($filter_key) ? $filter_key : '';?>&status=5" class="btn <?=$status5_btn_class?>" style="padding-top:3px; padding-bottom:3px;">已结束</a>
                    <a href="/user/customers?user_id=<?=isset($user_id) ? $user_id : '';?>&filter_key=<?=isset($filter_key) ? $filter_key : '';?>&status=-2" class="btn <?=$status_2_btn_class?>" style="padding-top:3px; padding-bottom:3px;">已取消</a>
                    &nbsp;
                    -->
                    关键词搜索:
                    <div class="form-group">
                        <input type="text" name="filter_key" class="form-control" id="filter_key" placeholder="姓名、微信或者手机" value="<?=isset($filter_key) ? $filter_key : '';?>"/>
                        <input type="hidden" name="status" value="<?=isset($status) ? $status : '';?>"/>
                    </div>
                    <button type="submit" class="btn btn-sm btn-primary">查找</button>
                    &nbsp;

                </div>

                <a href="/customer/add" class="btn btn-success btn-sm pull-right">添加客户</a>
                <!--div class="btn-group">
                    <button type="button" class="btn btn-primary">我的客户</button>
                    <button type="button" class="btn btn-primary">所有客户</button>
                    <button type="button" class="btn btn-primary">已经完成客户</button>
                </div-->
            </form>
        </div>
        <div class="table-responsive clearfix" >
            <table class="table table-bordered customer-list-table ">
                <thead>
                <tr style="background:#eee;">
                    <th>姓名</th>
                    <th width="35%">行程需求</th>
                    <th>联系方式</th>
                    <th>出发</th>
                    <th>目的</th>
                    <th>申请时间</th>
                    <th>操作</th>
                </tr>
                </thead>
                <tbody>
                <?php foreach($customer_list as $key=>$customer):?>
                    <tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?> customer_id="<?=$customer['id'];?>">
                        <td rowspan="2" style="vertical-align:middle;min-width:100px;">
                            <a target="_blank" href="/user/customer?id=<?=$customer['id'];?>"><?=$customer['contact'];?></a>
                        </td>
                        <td rowspan="2">
                            <?php if(mb_strlen($customer['remark'], 'utf-8') > 150):?>
                                <div id='c-<?=$customer['id'];?>-partial'>
                                    <?=nl2br(mb_substr($customer['remark'], 0, 150, 'utf-8'));?>...<br/>
                                    <a href="#", onClick="javascript:document.getElementById('c-<?=$customer['id'];?>-partial').style.display='none';document.getElementById('c-<?=$customer['id'];?>-all').style.display='';return false;">show all</a>
                                </div>
                                <div id='c-<?=$customer['id'];?>-all' style="display:none;">
                                    <?=nl2br($customer['remark']);?><br/>
                                    <a href="#", onClick="javascript:document.getElementById('c-<?=$customer['id'];?>-partial').style.display='';document.getElementById('c-<?=$customer['id'];?>-all').style.display='none';return false;">hide all</a>
                                </div>
                            <?php else:?>
                                <?=nl2br($customer['remark']);?>
                                <?php if(!empty($customer['trip_id'])):?>
                                    <br/>
                                    <a class="glyphicon glyphicon-pencil" target="_blank" href="/plan/create/edit?trip_id=<?=$customer['trip_id'];?>"></a>
                                    <a class="glyphicon glyphicon-file" target="_blank" href="/plan/export/excel?trip_id=<?=$customer['trip_id'];?>"></a>
                                <?php endif;?>
                            <?php endif;?>
                        </td>
                        <td rowspan="2" style="min-width:130px;font-family:Courier New;">
                            M：<?=$customer['mobile'];?><br/>
                            Q：<?=$customer['qq'];?><br/>
                            W：<?=$customer['wangwang'];?><br/>

                        </td>
                        <td style="min-width:100px;"><?=$customer['origin'];?></td>
                        <td><?=$customer['destination'];?></td>
                        <td style="min-width:100px;"><?=date('m-d H:i', strtotime($customer['ctime']));?></td>
                        <td><a class="btn btn-default btn-xs"  href="/user/customer?id=<?=$customer['id'];?>">查看详情</a></td>
                    </tr>
                    <tr <?php if($key % 2 == 1) :?> style="background:#f1f1f1;"<?php endif;?>>
                        <td>
                            <?php if($customer['start_date'] !='0000-00-00'):?>
                                <?=$customer['start_date'];?><br/>
                                <span style="font-size:12px;color:#666;"><?=human_time_admin_hp(strtotime($customer['start_date']))?></span>
                            <?php endif;?>
                        </td>
                        <td>
                            <?php if($customer['end_date'] !='0000-00-00'):?>
                                <?=$customer['end_date'];?><br/>
                                <span style="font-size:12px;color:#666;"><?=human_time_admin_hp(strtotime($customer['end_date']))?>
                                <?php if(strtotime($customer['start_date']) > strtotime($customer['end_date'])):?>
                                    <span class="text-danger">（!）</span></span>
                                <?php endif;?>
                            <?php endif;?>
                        </td>
                        <td><?=date('m-d H:i', strtotime($customer['mtime']));?></td>
                        <td style="width:100px;word-break:break-all;">
                            <a class="btn btn-default btn-xs" href="/plan/create/step1?customer_id=<?=$customer['id'];?>">编辑行程</a>
                        </td>
                    </tr>
                <?php endforeach;?>
                </tbody>
            </table>

            <?php
            $this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false));
            ?>

        </div>
    </div>

</div>

<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('admin/customer_index');
    });
</script>
