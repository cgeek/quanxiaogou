<div class="clearfix" style="padding-top:50px;">
    <ol class="breadcrumb">
        <li><a href="/user">管理首页</a></li>
        <li><a href="/user/customers">客户列表</a></li>
        <li class="active"><?=$customer['contact'];?></li>
    </ol>
    <div class="customer-detail table-responsive">
        <!-- Nav tabs -->
        <ul class="nav nav-tabs">
            <li class="active" ><a href="#base_info" data-toggle="tab">基本信息</a></li>
            <!--li><a href="#traveler" data-toggle="tab">同伴资料(<?=count($traveler_list);?>)</a></li-->
        </ul>
        <!-- Tab panes -->
        <div class="tab-content">
            <div class="tab-pane active" id="base_info">
                <table class="table table-bordered customer-list-table " style="margin-top:10px;">
                    <thead>
                    <tr style="background:#eee;">
                        <th>姓名</th>
                        <th>人数</th>
                        <td>出发地</td>
                        <td>目的地</td>
                        <td>出发日期</td>
                        <td>回程日期</td>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td rowspan="9" style="min-width:50px;vertical-align:middle;">
                            <?=$customer['contact'];?>
                            <?php if(!empty($customer['admin'])):?>
                                <p>【<a href="/admin/customer?admin_id=<?=$customer['admin_id'];?>"><?=$customer['admin']['nick_name'];?></a>】</p>
                            <?php endif;?>
                            <p>(<?=customerStatusCn($customer['status']);?>)</p>
                            <p>
                            </p>
                        </td>
                        <td><?=$customer['num_of_people'];?></td>
                        <td><?=$customer['origin'];?></td>
                        <td><?=$customer['destination'];?></td>
                        <td><?=$customer['start_date'];?></td>
                        <td><?=$customer['end_date'];?></td>
                    </tr>
                    <tr style="background:#eee;">
                        <th>手机号码</th>
                        <th>QQ号</th>
                        <th>邮箱</th>
                        <th>微信</th>
                        <td>创建时间</td>
                    </tr>
                    <tr>
                        <td><?=$customer['mobile'];?></td>
                        <td><?=$customer['qq'];?></td>
                        <td><?=$customer['email'];?></td>
                        <td><?=$customer['weixin'];?></td>
                        <td><?=date('Y-m-d',strtotime($customer['ctime']));?></td>
                    </tr>

                    <tr style="background:#eee;">
                        <td colspan="9">相关链接</td>
                    </tr>
                    <tr>
                        <td colspan="9">
                            <?php if(!empty($customer['journey_url'])):?>
                                <a class="btn btn-default" target="_blank" href="/admin/journey/google?customer_id=<?=$customer['id'];?>">查看行程单</a>
                            <?php endif;?>
                            <?php if(!empty($customer['trip_id'])):?>
                                <a class="btn btn-primary btn-sm" target="_blank" href="/plan/create/edit?trip_id=<?=$customer['trip_id'];?>">修改行程</a>
                                <a class="btn btn-success btn-sm" target="_blank" href="/plan/export/excel?trip_id=<?=$customer['trip_id'];?>">查看行程单</a>
                            <?php else:?>
                                <a class="btn btn-primary btn-sm" target="_blank" href="/admin/journey">创建行程</a>
                            <?php endif;?>

                            <div class="pull-right">
                                <!--button class="check_list_btn btn btn-sm btn-info" href="javascript:void(0);">待办事项(<?=$customer['progress'];?>%)</button>
                                <a class="btn btn-sm btn-warning" href="/admin/order/add?customer_id=<?=$customer['id'];?>" target="_blank">添加代订</a-->
                                <a class="btn btn-sm btn-primary" href="/user/customerEdit?id=<?=$customer['id'];?>">修改客户资料</a>
                            </div>
                        </td>
                    </tr>
                    <tr style="background:#eee;">
                        <td colspan="9">备注信息</td>
                    </tr>
                    <tr>
                        <td colspan="9"><?=nl2br($customer['remark']);?></td>
                    </tr>
                    <tr customer_id="<?=$customer['id'];?>">
                        <td colspan="10">
                            <input type="hidden" name="customer_id" value="<?=$customer['id'];?>">
                            <div class="btn-group" data-toggle="buttons">
                                <button class="btn btn-sm btn-success" <?php if($customer['status']>=1):?> disabled<?php endif;?> status=1 >已指派</button>
                                <button class="btn btn-sm btn-success update-status-btn" <?php if($customer['status']>=2):?> disabled<?php endif;?> status=2 >第一次联系客户</button>
                                <button class="btn btn-sm btn-success update-status-btn" <?php if($customer['status']>=3):?> disabled<?php endif;?> status=3 >沟通需求完成</button>
                                <button class="btn btn-sm btn-success update-status-btn" <?php if($customer['status']>=4):?> disabled<?php endif;?> status=4 >初步行程规划完成</button>
                                <button class="btn btn-sm btn-success update-status-btn" <?php if($customer['status']>=5):?> disabled<?php endif;?> status=5 >详细行程规划完成</button>
                                <button class="btn btn-sm btn-primary update-status-btn" <?php if($customer['status']==6):?> disabled<?php endif;?> status=6 >旅行结束已反馈</button>
                                <button class="btn btn-sm btn-warning update-status-btn" <?php if($customer['status'] == -2):?> disabled<?php endif;?> status=-2 >客户取消服务</button>
                                <button class="btn btn-sm btn-danger update-status-btn" <?php if($customer['status'] == -1):?> disabled<?php endif;?>  status=-1 >删除客户</button>
                            </div>

                        </td>
                    </tr>
                    </tbody>
                </table>




                <div class="col-lg-6 col-sm-6 col-md-6 pull-left" style="background:#eee; border-radius:5px; margin-top:10px;">
                    <h4>客户动态</h4>
                    <form class="clearfix" method="POST" id="comment-form">
                        <input type="hidden" name="object" value="customer-log">
                        <input type="hidden" name="object_id" value="<?=$customer['id'];?>">
                        <input type="hidden" name="admin_id" value="<?=Yii::app()->adminUser->id;?>">
                        <textarea class="form-control" rows="3" name="content" placeholder="可以记录任何关于这个客户的情况"></textarea>
                        <button class="btn btn-sm btn-primary pull-right" id="comment-submit-btn" style="margin-top:10px;">提交</button>
                    </form>
                    <ul class="comment-list list-unstyled">
                        <?php if(!empty($comment_list)):?>
                            <?php foreach($comment_list as $comment):?>
                                <li><?=nl2br($comment['content']);?> <p style="text-align:right; color:#999;"><?=$comment['ctime'];?></p></li>
                            <?php endforeach;?>
                        <?php endif;?>
                    </ul>
                </div>

            </div>

        </div>
    </div>
</div>

<script>
    seajs.use('/assets/js/router.js', function(router){
        router.load('admin/customer_detail');
    });
</script>
