<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0" name="viewport" />
    <meta content="telephone=no, address=no" name="format-detection" />
    <meta name="apple-mobile-web-app-capable" content="yes" />
    <meta name="apple-mobile-web-app-status-bar-style" content="black-translucent" />
    <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon" />
    <!-- 新 Bootstrap 核心 CSS 文件 -->
    <link rel="stylesheet" href="//cdn.bootcss.com/bootstrap/3.3.5/css/bootstrap.min.css" />
    <link rel="stylesheet" href="/css/index.css" />
    <link rel="stylesheet" href="/css/user_login.css" />
    <link rel="stylesheet" href="/css/header.css?v=20160414" />

    <script src="//cdn.bootcss.com/jquery/2.1.4/jquery.min.js"></script>
    <script src="//cdn.bootcss.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/lib/jquery.js"></script>
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/jquery.validate.min.js"></script>
    <script src="http://static.runoob.com/assets/jquery-validation-1.14.0/dist/localization/messages_zh.js"></script>

    <title>妙妙行程管家</title>
</head>
<body class="login-bg">


<div class="top top-fixed">
    <div class="head">
        <div class="icon">
            <a href="/taiwan">
                <img src="http://www.blueplanet.cn/img/logo2.png" height="50" title="妙妙行程管家" alt="妙妙行程管家">
            </a>
        </div>
        <div class="h_menu">

        </div>
    </div>
</div>

<div >
    <div class="container w-1100">
        <div class="main">
            <form method="post" id="loginForm" action="/account/login">
                <h4>网站登录</h4>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input name="username" type="text" class="form-control" id="inputEmail3" placeholder="请输入用户名" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <input name="password" type="password" class="form-control" id="inputPassword3" placeholder="密码" />
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <div class="checkbox">
                            <label class="reg-tips">
                                <input type="checkbox" checked="checked"> 记住密码
                            </label>
                        </div>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-12">
                        <button type="submit" style="width:100%" class="btn btn-success">登&nbsp;录</button>
                    </div>
                </div>
                <p id="hint" style="display:none;color:red">账号不存在或密码错误！</p>
                <!--p class="reg-tips">您还没有账号？<a href="/account/register">点击注册</a></p-->
            </form>
        </div>
    </div>
</div>

<script>
    $.validator.setDefaults({
        submitHandler: function() {
            var data = $('#loginForm').serialize();
            $.post('/user/login', data, function (r) {
                if (r.code == 200) {
                    window.location.href = "/user/dashboard";

                } else {
                    $("p#hint").show();
                }
            })
        }
    });
    $().ready(function() {

        $("#loginForm").validate({
            rules: {
                email: {
                    required: true,
                },
                password: {
                    required: true,
                }
            },
            messages: {
                email: {
                    required: '请输入用户名或邮箱！'
                },
                password: {
                    required: '请输入密码！'
                }
            },
        });
    })
</script>




</body>
</html>