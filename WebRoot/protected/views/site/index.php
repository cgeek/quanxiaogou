<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="renderer" content="webkit" />
    <meta name="keywords" content="妙妙行程管家" />
    <meta name="description" content="" />
    <!-- 触发双核浏览器使用 chrome 内核 -->
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <meta name="renderer" content="webkit" />
    <link rel="icon" href="http://www.blueplanet.cn/favicon.ico" type="image/x-icon" />
    <link rel="shortcut icon" href="http://www.blueplanet.cn/favicon.ico" type="image/x-icon" />
    <link rel="bookmark" href="http://www.blueplanet.cn/favicon.ico" type="image/x-icon"  />

    <![if gte IE 9]>
    <script src="//cdn.bootcss.com/jquery/2.2.3/jquery.js"></script>
    <![endif]>

    <!--[if lt IE 9]>
    <script src="//cdn.bootcss.com/html5shiv/r29/html5.js"></script>
    <script src="//cdn.bootcss.com/respond.js/1.4.2/respond.js"></script>
    <script src="//cdn.bootcss.com/jquery/2.2.3/jquery.js"></script>
    <![endif]-->
    <script src="//cdn.bootcss.com/bootstrap/3.2.0/js/bootstrap.min.js"></script>
    <link href="//cdn.bootcss.com/bootstrap/3.2.0/css/bootstrap.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/font-awesome/4.6.3/css/font-awesome.min.css" rel="stylesheet">
    <link href="//cdn.bootcss.com/magicsuggest/2.1.4/magicsuggest-min.css" rel="stylesheet">
    <script src="//cdn.bootcss.com/magicsuggest/2.1.4/magicsuggest-min.js"></script>
    <link href="//cdn.bootcss.com/normalize/4.2.0/normalize.min.css" rel="stylesheet">

    <link href="/css/index.css?v=20160501" rel="stylesheet"  />
    <title>妙妙行程管家 - 专注行程定制专家</title>
    <link rel="stylesheet" href="/css/header.css?v=20160414" />
</head>
<body>

<nav class="navbar navbar-inverse">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a  href="/" data-egret="p=home" style="color:#737373;">
                <img src="/images/welcome/logo.png" style="margin:10px;" title="妙妙行程管家" alt="妙妙行程管家">
                | &nbsp;蓝色行星旗下产品
            </a>

        </div>
        <div id="navbar" class="collapse navbar-collapse">
            <ul class="nav navbar-nav navbar-right">
                <li class="hidden-xs hidden-sm"><img src="/images/welcome/phone.png" width="150px" style="padding-top: 8px;"></li>
                <li ><a id="loginBtn" href="#/user/login">登录</a></li>
            </ul>
        </div>
    </div>
</nav>

<div class="landingContainer">
    <div class="landingContent">
        <div class="filter"></div>
    </div>
</div>

<style>
    .intro_list {margin-top:40px;}
    .intro_list img {margin-bottom: 20px;}
    .intro_list h4 { color:#000; margin-top:10px;}
    .intro_list p {  line-height: 30px;}
</style>
<div class="container" style="background: #fff;">
    <div class="row intro_list">
        <div class="col-md-4">
            <img src="/images/welcome/intro_1.png" width="80" title="" alt="">
            <h4>简单高效，微信分享</h4>
            <p>
                鼠标拖到 + 模板复制, <br />
                10分钟即可完成行程， <br />
                支持微信分享行程 <br />
            </p>
        </div>
        <div class="col-md-4">
            <img src="/images/welcome/intro_2.png" width="80" title="" alt="">
            <h4>收集需求，多客户管理</h4>
            <p>
                发送需求收集链接, <br />
                节省初步沟通时间， <br />
                客户信息卡片式管理，一目了然 <br />
            </p>
        </div>
        <div class="col-md-4">
            <img src="/images/welcome/intro_3.png" width="80" title="" alt="">
            <h4>操作问题，专人指导</h4>
            <p>
                行程制作过程中有问题, <br />
                一对一专人指导<br />
            </p>
        </div>
    </div>
</div>

<div class="footer">
    <div class="container">
        <div class="row" style="max-width: 600px; margin:0 auto;" >
            <div class="contact-info col-sm-9">
                <i class="contact-icon"><img src="/images/welcome/footer_connect.png"></i>
                <p>上海蓝色行星技术有限公司</p>
                <p>Copyright©2016 蓝色行星 版权所有 沪ICP备14047867号</p>
            </div>
            <div class="qr_code col-sm-3">
                <img src="/images/welcome/qr_code.png">
            </div>
        </div>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-body" style="padding:0;">
                <div class="left col-sm-4 hidden-xs" style="height: 300px;background: #D7DFE1;display: inline-block;float: left;">
                    <div class="" style="padding:20px;">
                        <p>&nbsp;没有账号？</p>
                        <p style="font-size:12px;">
                            内测期间，账号申请待开放
                            <!--a href="#" class="btn btn-default" style="border:1px solid #7f7f7f; background: none;">申请试用</a-->
                        </p>
                    </div>
                </div>
                <div class="right col-sm-8" style="height: 300px;display: inline-block;float: left;background: #fff;padding:0;">
                    <button type="button"  style="padding:10px;" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>

                    <form role="form" id="loginForm" style="margin:50px;">
                        <h4>登录</h4>
                        <div class="form-group">
                            <input name="username" style="border: 0; padding:0; border-bottom: 1px solid #7F7F7F;border-radius: 0;box-shadow: none;" type="email" class="form-control" id="exampleInputEmail1" placeholder="邮箱">
                        </div>
                        <div class="form-group">
                            <label for="exampleInputPassword1"></label>
                            <input name="password" type="password" class="form-control" id="exampleInputPassword1" placeholder="密码"  style="padding:0; border: 0;border-bottom: 1px solid #7F7F7F;border-radius: 0;box-shadow: none;" >
                        </div>
                        <p id="hint" style="display:none;color:red">账号不存在或密码错误！</p>
                        <button id="loginSubmit" type="submit" class="btn btn-default" style="background:#24C9C2; color:#fff;border:0;width:200px;margin-top:10px;">登录</button>
                    </form>


                </div>
            </div>

        </div>
    </div>
</div>

<script>

    $(document).ready(function(){

        $('#loginBtn').click(function(){
            $('#myModal').modal('show')
        });

        $('body').delegate('#loginSubmit', 'click', function(){
            var data = $('#loginForm').serialize();
            $.post('<?=Yii::app()->request->hostInfo;?>/user/login', data, function (r) {
                if (r.code == 200) {
                    window.location.href = "/user/";
                } else {
                    $("p#hint").show();
                }
            })

            console.log(data);
            return false;
        })
    });
</script>