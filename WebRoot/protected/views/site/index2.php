<!DOCTYPE html>
<html>
<head>
	<title>妙妙行程管家 - 你的私人旅行顾问！</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/base.css" />
	
	<script src="<?php echo Yii::app()->request->baseUrl; ?>/assets/seajs/dist/sea.js"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->

</head>
<body>
	<!--div id="topbar" class="navbar navbar-inverse navbar-fixed-top">
		<div class="container">
			<div class="navbar-header">
				<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
				<a class="navbar-brand" href="/">妙妙行程管家</a>
			</div>
			<div class="collapse navbar-collapse">
				<ul class="nav navbar-nav">
					<li><a href="/">我要定制</a></li>
				</ul>
			</div>
		</div>
	</div-->
	<header class="header">
		<div class="container">
			<!--div class="header-logo">
				<img src="/images/xiaogou-logo.png">
			</div-->
			<div class="header-nav">
				<ul>
					<li><a href="#about">关于我们</a></li>
					<li><a href="#demo">经典行程</a></li>
					<li><a href="#app">手机客户端</a></li>
				</ul>
			</div>
			<div class="pull-right">
				<img src="/images/home/top_slogan.png" >
				<a href="#submit" ><img src="/images/home/top_button.png"></a>
			</div>
			<div style="margin:0 auto; width:110px; height:110px;">
				<a href="/">
					<img src="/images/home/xiaogou-logo.png" class="logo-center">
				</a>
			</div>
		</div>
	</header>

	<div class="content home_page">
		<div class="layout banner" id="about">
			<div class="container" style="position:relative; height:100%;">
				<div><img src="/images/home/topbanner_liucheng.png"></div>
				<a href="#submit" class="submit_btn"><img src="/images/home/topbanner_button.png"></a>
				<a href="" style="position:absolute;bottom:0; right:10px;"><img src="/images/home/fee.png"></a>
			</div>
		</div>
		<div class="layout demo" id="demo">
			<div class="container">
				<img src="/images/home/banner2.png">
			</div>
		</div>
		<div class="layout mobile" id="app">
			<div class="container">
				<img src="/images/home/banner3.png">
			</div>
		</div>

		<div class="layout product">
			<div class="container">
				<img src="/images/home/banner4.png">
			</div>
		</div>

		<div class="layout submit" id="submit">
			<div class="container">
				<div class="" style="text-align:center; margin-top:20px;">
					<img src="/images/home/tijiao.png">
				</div>
				<div class="submit_box">
					<form method="post" action="/customer/add" class="form-horizontal" id="customer-form">
						<input type="hidden" name="platform" value="web" >
						<input type="hidden" name="source" value="home_page" >
						<div class="form-group">
							<label for="destination" class="col-lg-3 control-label">目的地</label>
							<div class="col-lg-7">
								<select class="form-control" name="destination">
									<option value="马来西亚">马来西亚</option>
									<option value="泰国">泰国</option>
								</select>
							</div>
						</div>
						<div class="form-group">
							<label for="contact" class="col-lg-3 control-label">联系人</label>
							<div class="col-lg-7">
								<input type="text" name="contact" class="form-control" id="contact" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="mobile" class="col-lg-3 control-label">手机号</label>
							<div class="col-lg-7">
								<input type="text" name="mobile" class="form-control" id="mobile" placeholder="请填写真实的手机号，方便联系">
							</div>
						</div>
						<div class="form-group">
							<label for="email" class="col-lg-3 control-label">邮箱</label>
							<div class="col-lg-7">
								<input type="text" name="email" class="form-control" id="email" placeholder="">
							</div>
						</div>
						<div class="form-group">
							<label for="remark" class="col-lg-3 control-label">需求</label>
							<div class="col-lg-7">
								<textarea name="remark" class="form-control" rows=5 placeholder="如：一家三口，打算明年三月份去普吉，已定机票，求规划行程、推荐酒店、当地活动"></textarea>
							</div>
						</div>
						<div class="form-group">
							<label class="col-lg-3 control-label"></label>
							<div class="col-lg-7 tips">妙妙行程管家的管家服务为收费服务，针对一次旅行收取100元服务费，点击<a href="">查看详细条款</a></div>
						</div>
							
						<div class="form-group">
							<label class="col-lg-3 control-label"></label>
							<div class="col-lg-7">
								<button class="btn btn-default pull-right" id="submit-btn">好了，给我来个管家</button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="footer">
		<div class="container">
			<p>妙妙行程管家 © 2013 XiaoGouLvXing.com 版权所有</p>
			<div class="links">合作伙伴:
				<ul>
					<li><a target="_blank" href="http://www.qiugonglue.com/">求攻略</a></li>
					<li><a target="_blank" href="http://u.ctrip.com/union/CtripRedirect.aspx?TypeID=2&Allianceid=11795&sid=328252&OUID=&jumpUrl=http://www.ctrip.com">携程</a></li>
					<li><a target="_blank" href="http://www.agoda.com.cn/?cid=1616993">Agoda</a></li>
				</ul>
			</div>
		</div>
	</div>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('customer');
	});
</script>

	<div style="display:none;">
<script type="text/javascript">
var _bdhmProtocol = (("https:" == document.location.protocol) ? " https://" : " http://");
document.write(unescape("%3Cscript src='" + _bdhmProtocol + "hm.baidu.com/h.js%3F53f78afdb90bf194696a6823b9c5aac8' type='text/javascript'%3E%3C/script%3E"));
</script>
	</div>
</body>
</html>
