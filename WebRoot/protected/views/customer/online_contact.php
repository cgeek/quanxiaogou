<!DOCTYPE html>
<html>
<head>
	<title>妙妙行程管家定制</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家定制" />
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/main.css" />
	
	<script src="/assets/seajs/dist/sea.js"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->

</head>
<body style="margin:0px; padding:0px;">
<div class="content">
	<div class="container" id="online-contact-form">
		<div class="row" style="margin-top:10px; margin-bottom:10px;">
			<div class="col-xs-4 col-sm-6 col-md-6 col-lg-6" style="padding-right:10px;">
				<img src="<?=$agent_avatar;?>" class="img-circle pull-right" style="width:60px;">
			</div>
			<div class="col-xs-8 col-sm-6 col-md-6 col-lg-6" style="padding-left:5px;">
				<div class="pull-left" style="height:60px;">
					<h4 style="margin-top:10px;">顾问：<a href="#"><?=$agent_name;?></a></h4>
					提交后我会尽快联系你。
				</div>
			</div>
		</div>
		<div class="row">
			<div class="col-xs-0 col-lg-2">
			</div>
			<div class="customer-info-form col-xs-12 col-lg-8">
					<form id="customer-form" class="form-horizontal" role="form" method="POST">
						<input type="hidden" name="source" value="<?=isset($source)? $source: '';?>">
						<input type="hidden" name="platform" value="<?=isset($platform)? $platform : '';?>">

						<div class="form-group" style="margin-bottom:2px;">
							<label for="inputContact" class="col-xs-12 col-lg-2 control-label">姓名</label>
							<div class="col-xs-6 col-lg-2">
								<input type="text" name="contact" class="form-control" id="inputContact" placeholder="">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group" style="margin-bottom:2px;">
							<label for="inputMobile" class="col-xs-12 col-lg-2 control-label">手机号</label>
							<div class="col-xs-8 col-lg-3">
								<input type="text" name="mobile" class="form-control" id="inputMobile" placeholder="">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group" style="margin-bottom:2px;">
							<label for="inputMobile" class="col-xs-12 col-lg-2 control-label">QQ（最好填QQ，我们会优先用QQ联系）</label>
							<div class="col-xs-8 col-lg-5">
								<input type="text" name="qq" class="form-control" id="inputQQ" placeholder="选填">
								<span class="help-block"></span>
							</div>
						</div>
						<div class="form-group" style="margin-bottom:2px;">
							<label for="inputRemark" class="col-xs-12 col-lg-3 control-label">需求（目的地、人数、出行日期等）</label>
							<div class="col-xs-0 col-lg-5"></div>
							<div class="col-xs-0 col-lg-2"></div>
							<div class="col-xs-12 col-lg-6">
								<textarea class="form-control" placeholder="如：一家三口，打算明年三月份去普吉，已定机票，求设计行程、代订酒店活动" name="remark" id="inputRemark" rows="3"></textarea>
								<span class="help-block"><span>
							</div>
						</div>

						<div class="form-group">
							<div class="col-lg-offset-2 col-lg-10">
								<button type="submit" id="submit-btn" class="btn btn-primary">提交</button>
								<span class="help-block"><span>
							</div>
						</div>
					</form>

			</div>
			<div class="col-xs-0 col-lg-2"></div>
		</div>
	</div>
	
	<div class="container" id="post-success-box" style="display:none; margin-top:20px;">
		<div class="alert alert-success clearfix">
			<div class="col-xs-4 col-sm-6 col-md-6 col-lg-6" style="padding-right:10px;">
				<img src="<?=$avatar;?>" class="img-circle pull-right" style="width:60px;">
			</div>
			<div class="col-xs-8 col-sm-6 col-md-6 col-lg-6 pull-left clearfix" style="padding-left:5px;">
					<h4>提交成功</h4>
					请保持电话畅通，我会尽快联系你。
			</div>
		</div>
	</div>
	
</div>



<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('customer');
	});
</script>

</body>
</html>
