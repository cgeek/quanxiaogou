<!DOCTYPE html>
<html>
<head>
	<title>妙妙行程管家定制</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家定制" />
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
	<link rel="stylesheet" type="text/css" href="/css/main.css" />
	
	<script src="/assets/seajs/dist/sea.js"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->
	<style>
		div.question{
			font-family:Optima;
			font-weight:bold;
			font-size:16px;
			padding:3px;
		}
		div.answer{
			font-family:Optima;
			font-size:16px;
			padding:3px;
			margin-bottom:20px;
		}
	</style>

</head>
<body style="margin:0px; padding:0px;">
	<div class="container">
		<h3>常见问题问答</h3>
		<div class="question">
			问：请简单介绍一下你自己吧。
		</div>
		<div class="answer">
			答：咿我刚才不是介绍过了ma？
		</div>
		<div class="question">
			问：你们只开通了沙巴么？我想去普吉岛哎...
		</div>
		<div class="answer">
			答：<strike>Sorry 我们目前只开通了沙巴。</strike>现在开通了沙巴和泰国清迈。
		</div>
		<div class="question">
			问：100元一个人么？
		</div>
		<div class="answer">
			答：不，目前100元一次，可以两个大人 + 一个小孩；代订酒店、机票、活动等；人多的话，主要是定机票太麻烦了，尤其是亚航的系统你们懂的，如果机票你已经定好了，那不管是几个人，都收费100，厚道极了。
		</div>
		<div class="question">
			问：是先交100块钱再设计行程、预定项目，还是先设计好行程预定好项目最后交这100元。
		</div>
		<div class="answer">
			答：走淘宝，先拍一个服务费商品交100快钱，所有都搞定了之后再确认收货。
		</div>
		<div class="question">
			问：你帮忙设计的行程和旅行社跟团的那种行程有什么不同。
		</div>
		<div class="answer">
			答：你问的这个问题太好了，我就喜欢回答这样的问题。<br/>
			我设计的行程都是自由行行程，所以每个都不一样。有的人想住香格里拉，有的人想住便宜的Gaya Center酒店；有的人想每天晚上去吃海鲜，有的人就喜欢去看最美日落。所以我给每个人设计的行程都是跟每个人沟通后修正的结果。另外我做的行程里面有一些旅行社行程肯定不会有的地方，比如我可以安排沙巴本地的中国留学生去接机，然后跟你聊一路他眼里的沙巴那里最好玩。另外，有些旅行社推荐住的酒店，我是绝对不会推荐的（比如给旅行社很大折扣的Ming Garden酒店）。还有，有旅行社会帮你定亚航的机票么？（亚航都是直销，不进旅行社那个定机票的系统的）总之，都是行程，一个是批量售卖，我这个是单独定制，基本没有一样的地方。。。<br/>
			这个问题是不是回答的太长了。。。
		</div>
		<div class="question">
			问：你是一个人还是一个公司？
		</div>
		<div class="answer">
			答：其实我们是一个小型的创业团队，每个人都酷爱旅行，我们在手机上做了一些很好的攻略（沙巴、清迈、台湾等），攻略很受大家欢迎但赚不到钱，所以我们现在开展了这个业务，能赚一点是一点。目前沙巴攻略主要是由<a href="/customer/ad?a=vivian">@小Vivian</a>和<a href="/customer/ad?a=cherrycha">@CherryCha</a>来维护。也主要是由我们两个帮大家制作行程、各种预定。
		</div>
		<div class="question">
			问：你自己有没有觉得，100块钱太便宜了？
		</div>
		<div class="answer">
			答：是唉，这个业务开展了一个月之后，我就发现我们提供的这个服务实在是太超值了，我发现我们的很多客户如果没有我们的帮忙的话真的会花很多冤枉钱的，关键玩的也不会太爽。内什么，问这个问题的你放心，如果很多人这么说，我们会涨价的。。。
		</div>
	</div>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('customer');
	});
</script>

</body>
</html>
