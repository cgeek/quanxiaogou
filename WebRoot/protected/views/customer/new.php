<div class="content">
	<div class="container">
		<h3 style="text-align:center;color:#999;padding-bottom:10px;">提交私人定制服务申请</h3>
		<div class="customer-info-form col-lg-12">
			<form id="zixun-form" class="form-horizontal" role="form" action="/customer/saveZixun" method="POST">
				<div class="form-group">
					<label for="inputContact" class="col-lg-2 control-label">联系人姓名</label>
					<div class="col-lg-2">
						<input type="text" name="contact" class="form-control" id="inputContact" placeholder="">
					</div>
				</div>
				
				<!--
				<div class="form-group">
					<label for="inputContact" class="col-lg-2 control-label">性别</label>
					<div class="col-lg-2">
						<select name="sex" class="form-control">
							<option value="female">女士</option>
							<option value="male">先生</option>
						</select>
					</div>
				</div>
				-->
				
				<!--div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">目的地</label>
					<div class="col-lg-2">
						<input type="text" class="form-control" id="inputPassword1" placeholder="">
					</div>
				</div-->
				
				<div class="form-group">
					<label for="inputStartDate" class="col-lg-2 control-label">预计出发日期</label>
					<div class="col-lg-2">
						<input type="text" name="start_date" class="form-control" id="inputStartDate" placeholder="如：2014-5-20">
					</div>
					<!--
					<div class="col-lg-2">
						<input type="text" name="end_date" class="form-control" id="inputEndDate" placeholder="结束日期">
					</div>
					-->
				</div>
				<div class="form-group">
					<label for="peopleNums" class="col-lg-2 control-label">出行人数</label>
					<div class="col-lg-3">
						<input type="text" name="num_of_people" class="form-control" id="peopleNums" placeholder="如：2大1小（小孩4岁）">
					</div>
				</div>
				<!--
				<div class="form-group">
					<label class="col-lg-2 control-label">预定情况</label>
					<div class="col-lg-5">
						<label class="checkbox-inline">
							<input type="checkbox" name="has_booked_airline_ticket" id="inlineCheckbox1" value="1"> 已订好机票
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" name="has_booked_hotel" id="inlineCheckbox2" value="1"> 已订好酒店
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" name="no_booking" id="inlineCheckbox3" value="1"> 什么都没订
						</label>
					</div>
				</div>
				-->
				
				<div class="form-group">
					<label for="inputRemark" class="col-lg-2 control-label">个性需求</label>
					<div class="col-lg-5">
						<textarea class="form-control" placeholder="简单介绍一下你的情况或者你的需求" name="remark" id="inputRemark" rows="5"></textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputMobile" class="col-lg-2 control-label">手机号码</label>
					<div class="col-lg-2">
						<input type="text" name="mobile" class="form-control" id="inputMobile" placeholder="">
					</div>
				</div>
				<!--
				<div class="form-group">
					<label for="inputQQ" class="col-lg-2 control-label">QQ号码</label>
					<div class="col-lg-2">
						<input type="text" name="qq" class="form-control" id="inputQQ" placeholder="">
					</div>
				</div>
				-->
				
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">提交申请</button>
						<span class="help-block"><span>
					</div>
				</div>
			</form>

			<div class="post-success-box alert alert-success " style="text-align:center; display:none;">
				<h3>恭喜你，你的申请已经提交成功！</h3>
				<p>我们的客服会尽快联系你，为你定制详细的行程计划！</p>
				<p>你也可以通过QQ或者邮箱联系我们的客服</p>
			</div>
		</div>
	</div>
</div>



<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('customer');
	});
</script>
