<div class="banner">
	<div class="container">
		<h1>提交用户信息</h1>
		<p>为了方便我们给你预定酒店、机票、活动,需要你提供以下信息</p>
	</div>
</div>
<div class="content">
	<div class="container">
		<div class="col-lg-3">
			<h4>已添加好友信息</h4>
			<ul class="list-group">
			  <li class="list-group-item">周晓茶</li>
			  <li class="list-group-item">缪华武</li>
			</ul>
		</div>
		<div class="customer-info-form col-lg-9">
			<h4>填写新好友信息</h4>
			<form class="form-horizontal" role="form">
				<div class="form-group">
					<label for="inputEmail1" class="col-lg-2 control-label">中文姓名</label>
					<div class="col-lg-3">
						<input type="text" class="form-control" id="inputEmail1" placeholder="中文姓名">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">英文名</label>
					<div class="col-lg-3">
						<input type="text" class="form-control" id="inputPassword1" placeholder="姓名的拼音">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">性别</label>
					<div class="col-lg-3">	
						<select class="form-control">
						  <option>请选择</option>
						  <option value="male">男</option>
						  <option value="female">女</option>
						</select>
						
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">出生日期</label>
					<div class="col-lg-3">
						<input type="text" class="form-control" id="inputPassword1" placeholder="">
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">护照号</label>
					<div class="col-lg-2">
						<input type="text" class="form-control" id="inputPassword1" placeholder="">
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">护照有效期</label>
					<div class="col-lg-2">
						<input type="text" class="form-control" id="inputPassword1" placeholder="2021-09-01">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">身份证号码</label>
					<div class="col-lg-3">
						<input type="text" class="form-control" id="inputPassword1" placeholder="如：330812XXXXXXXX">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">航班信息</label>
					<div class="col-lg-5">
						<textarea class="form-control" rows="3" placeholder="如果还没有预定机票，则不需填写"></textarea>
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">常用邮箱</label>
					<div class="col-lg-3">
						<input type="text" class="form-control" id="inputPassword1" placeholder="Email">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">QQ号码</label>
					<div class="col-lg-3">
						<input type="text" class="form-control" id="inputPassword1" placeholder="QQ">
					</div>
				</div>
				<div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">微信号</label>
					<div class="col-lg-3">
						<input type="text" class="form-control" id="inputPassword1" placeholder="微信">
					</div>
				</div>
				<div class="form-group">
					<div class="col-lg-offset-2 col-lg-10">
						<button type="submit" class="btn btn-default">保存信息</button>
					</div>
				</div>
			</form>
		</div>	
	</div>
</div>
<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('customer');
	});
</script>
