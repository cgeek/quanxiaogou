<div class="banner">
	<div class="container">
		<h1>我们提供什么样的服务？</h1>
		<p>￥100 = 个性化定制行程 + 代订旅行产品 + 旅行疑问解答</p>
	</div>
</div>
<!--
<div class="intro" style="padding:20px 0; background:#f7f7f6;" >
	<div class="container">
		<div class="col-lg-4">
			<h4> + 个性化定制行程， 制作攻略</h4>
			<p>这是我们的核心服务，我们拥有经验丰富的旅行顾问，能够根据你的需求为你定制出最符合你自己的行程安排。</p>
		</div>
		<div class="col-lg-4">
			<h4> + 代订酒店，活动，机票</h4>
			<p>我们有专业的团队每天观察，追踪最新的特价机票，酒店信息，并且了解各种预定方式和代理商的优劣。</p>
		</div>
		<div class="col-lg-4">
			<h4> + 解答旅途中各种问题 + 合理建议 </h4>
			<p>你的私人旅行顾问会为你解答行前准备和旅途中的任何问题，并且给你合理的建议</p>
		</div>
	</div>
</div>
-->
<div class="content">
	<div class="container">
		<h3 style="text-align:center;color:#999;padding-bottom:10px;">提交私人定制服务申请</h3>
		<div class="customer-info-form">
			<form id="customer-form" class="form-horizontal" role="form" action="/customer/saveZixun" method="POST">
				<div class="form-group">
					<label for="inputContact" class="col-sm-2 col-md-2 col-lg-2 control-label">联系人姓名</label>
					<div class="col-sm-2 col-md-2 col-lg-2">
						<input type="text" name="contact" class="form-control" id="inputContact" placeholder="">
					</div>
					<!--
					<div class="col-lg-2">
						<select name="sex" class="form-control">
							<option value="female">女士</option>
							<option value="male">先生</option>
						</select>
					</div>
					-->
				</div>
				
				<!--
				<div class="form-group">
					<label for="inputContact" class="col-lg-2 control-label">性别</label>
					<div class="col-lg-2">
						<select name="sex" class="form-control">
							<option value="female">女士</option>
							<option value="male">先生</option>
						</select>
					</div>
				</div>
				-->
				
				<!--div class="form-group">
					<label for="inputPassword1" class="col-lg-2 control-label">目的地</label>
					<div class="col-lg-2">
						<input type="text" class="form-control" id="inputPassword1" placeholder="">
					</div>
				</div-->
				
				<div class="form-group">
					<label for="inputStartDate" class="col-sm-2 col-md-2 col-lg-2 control-label">预计出发日期</label>
					<div class="col-sm-2 col-md-2 col-lg-2">
						<input type="text" name="start_date" class="form-control" id="inputStartDate" placeholder="如：2014-5-20">
					</div>
					<!--
					<div class="col-lg-2">
						<input type="text" name="end_date" class="form-control" id="inputEndDate" placeholder="结束日期">
					</div>
					-->
				</div>
				<div class="form-group">
					<label for="peopleNums" class="col-sm-2 col-md-2 col-lg-2 control-label">出行人数</label>
					<div class="col-sm-3 col-md-3 col-lg-3">
						<input type="text" name="num_of_people" class="form-control" id="peopleNums" placeholder="如：2大1小（小孩4岁）">
					</div>
				</div>
				<!--
				<div class="form-group">
					<label class="col-lg-2 control-label">预定情况</label>
					<div class="col-lg-5">
						<label class="checkbox-inline">
							<input type="checkbox" name="has_booked_airline_ticket" id="inlineCheckbox1" value="1"> 已订好机票
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" name="has_booked_hotel" id="inlineCheckbox2" value="1"> 已订好酒店
						</label>
						<label class="checkbox-inline">
							<input type="checkbox" name="no_booking" id="inlineCheckbox3" value="1"> 什么都没订
						</label>
					</div>
				</div>
				-->
				
				<div class="form-group">
					<label for="inputRemark" class="col-sm-2 col-md-2 col-lg-2 control-label">个性需求</label>
					<div class="col-sm-5 col-md-5 col-lg-5">
						<textarea class="form-control" placeholder="简单介绍一下你的情况或者你的需求" name="remark" id="inputRemark" rows="5"></textarea>
					</div>
				</div>
				
				<div class="form-group">
					<label for="inputMobile" class="col-sm-2 col-md-2 col-lg-2 control-label">手机号码</label>
					<div class="col-sm-2 col-md-2 col-lg-2">
						<input type="text" name="mobile" class="form-control" id="inputMobile" placeholder="">
					</div>
				</div>
				<!--
				<div class="form-group">
					<label for="inputQQ" class="col-lg-2 control-label">QQ号码</label>
					<div class="col-lg-2">
						<input type="text" name="qq" class="form-control" id="inputQQ" placeholder="">
					</div>
				</div>
				-->
				
				<div class="form-group">
					<div class="col-sm-offset-2 col-md-offset-2 col-lg-offset-2 col-lg-10 col-sm-10 col-md-10">
						<button type="submit" id="submit-btn" class="btn btn-primary">提交申请</button>
						<span class="help-block"><span>
					</div>
				</div>
			</form>

			<div class="post-success-box alert alert-success " style="text-align:center; display:none;">
				<h3>恭喜你，你的申请已经提交成功！</h3>
				<p>我们的客服会尽快联系你，为你定制详细的行程计划！</p>
				<p>你也可以通过QQ或者邮箱联系我们的客服</p>
			</div>
		</div>
	</div>
</div>


<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('customer');
	});
</script>
