<!DOCTYPE html>
<html>
<head>
	<title>妙妙行程管家定制</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0 maximum-scale=1.0, user-scalable=no">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="zh" />
	<meta name="description" content="妙妙行程管家定制" />
	<link rel="stylesheet" type="text/css" href="/css/bootstrap.min.css" />
	<!--link rel="stylesheet" type="text/css" href="/css/main.css" /-->

	<style>
		body {font-family:Optima; background:#F6F6F6;}
		.banner { text-align:center; padding-bottom:10px; /*background-image:url('/images/xiaogou_qiugonglue_banner.png'); */background-repeat:no-repeat;background-position:center; height:225px; width:100%;}
		.banner h3 { background:#382158; padding:10px 20px; margin-bottom:15px; color:#ffd200; font-size:23; border-radius:5px; text-align:center; font-family:Optima;}
		.banner p { color: #cabce1; font-size:14px; text-align:center; margin-bottom:5px; font-family:Optima;}
	</style>
	<script src="/assets/seajs/dist/sea.js"></script>
	<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
	<!--[if lt IE 9]>
		<script src="../../assets/libs/html5shiv.js"></script>
		<script src="../../assets/libs/respond.min.js"></script>
	<![endif]-->

</head>
<body style="margin:0px; padding:0px;">

	<img src="/images/xiaogou_qiugonglue_banner.png" width="100%">
<!--div class="banner client_banner">
	<div class="container">
		<h3>￥100 定制行程试运行</h3>
		<p>￥100 = 行程设计 + 定制攻略 + 代订所有项目</p>
		<p>省钱、省心、玩的好！</p>
	</div>
</div-->
<div class="content">
	<div class="container" style="margin-top:10px;">
		<div class="row">
			<div class="col-xs-0 col-sm-2 col-md-3 col-lg-4">
			</div>
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
				<div class="" style="font-family:optima;font-size:14px;">
					<p><img width="100" height=100 src="http://qimages.b0.upaiyun.com/efd9d19ecf17a3eeaff4da28036a5334_small.jpg" class="img-circle pull-left" style="padding:15px; width:100px;">Hi，我们是妙妙行程管家团队，本<?=$place_name;?>攻略作者。</p>
				<p>	如果你有旅行上的困惑，我们将作为助手帮你搞定这次旅行，并收取100元服务费。</p>
			<p>
其中包括：<br />
a、根据你的具体情况制定一份合理的行程 + 攻略。<br />
b、根据您的需求推荐酒店、航班、活动等项目，并进行预定。<br />
c、全程跟踪：包括出行前提醒、全程答疑等。<br />
</p>
<p>
无论旅行长短和人数，整个旅行只收一次100元的服务费。<br />
</p>

					<!--p>在微博上，每天都有人问我们能否订酒店、订活动什么的，后来我们就开展这样一个业务：收费100元，帮你制定行程攻略、代订酒店和各种旅游项目。</p>
					<p>一句话，我作为助手帮你搞定这次旅行，收费100元。</p>
					<p>作为<?=$place_name;?>攻略的作者，根据你的具体情况制定一份合理的行程 + 攻略是我们最擅长的。</p>
					<p>预定项目价格方面，由于定的比较多，逐渐摸索出各种项目在哪里预定性价比最高、服务最好。有的走Agoda，有的走淘宝，有的要找<?=$place_name;?>本地的旅行社，如果你是新手初次来<?=$place_name;?>，一般来说找我们预定可以帮你省几百到一千元不等。</p-->
					<!--p>
						<a href="/customer/faq">常见问题和回答</a>，感兴趣请联系：
					</p-->
				</div>
			</div>
			<div class="col-xs-0 col-sm-2 col-md-3 col-lg-4">
			</div>
		</div>

		<!--div class="row">
			<div class="col-xs-0 col-sm-2 col-md-3 col-lg-4"></div>
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
				<div class="" style="background-color:bisque;font-family:optima;font-size:14px;margin-top:10px; margin-bottom:10px; padding:5px;">
				小伙伴们，我在1月26号至2月8号(春节期间)休假哦，这段时间需要找我的亲，请直接点下面的“在线提交需求”，休假结束我就马上会联系你的哦。
				</div>
			</div>
			<div class="col-xs-0 col-sm-2 col-md-3 col-lg-4"></div>
		</div-->


		<div class="row">
			<div class="col-xs-0 col-sm-2 col-md-3 col-lg-4">
			</div>
			<div class="col-xs-12 col-sm-8 col-md-6 col-lg-4">
			<table class="table table-striped table-bordered">
				<tr>
					<td><span class="pull-right">在线联系：</span></td>
					<td>
					<a href="/customer/onlinecontact?a=<?=$agent;?>&source=<?=$source;?>&platform=<?=$platform;?>&uuid=<?=$client_uuid;?>">猛击这里提交需求</a>
					</td>
				</tr>
				<!--tr>
					<td><span class="pull-right">电话：</span></td>
					<td>
						<?=$agent_phone;?>
					</td>
				</tr-->
				<tr>
					<td><span class="pull-right">微信：</span></td>
					<td>
						xiaogoulvxing<br>(订阅号：妙妙行程管家)
					</td>
				</tr>
				<tr>
					<td><span class="pull-right">淘宝：</span></td>
					<td>
						<a href="http://shop102147639.taobao.com/index.htm" target="_blank">妙妙行程管家</a>
					</td>
				</tr>
			</table>
			</div>
			<div class="col-xs-0 col-sm-2 col-md-3 col-lg-4">
			</div>
		</div>


		<div class="row" style="height:10px;">
		</div>
	</div>
</div>
</body>
</html>
