<!DOCTYPE html>
<html>
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=9" />
	<meta content="酒店预订,精品酒店,高端酒店,设计酒店,热销酒店,度假酒店,酒店精选,旅行套餐,给我灵感,旅行博客,海外婚礼,高端旅游,定制旅游,度假村,浪漫假期,目的地攻略,蜜月旅行,奢侈旅行,Spa,水疗,订制旅行,非凡旅程" name="keywords">
	<meta content="妙妙行程管家，你的私人旅行顾问，帮助你完成第一次出国旅游的梦想。成为我们的会员，我们将为你量身打造你的行程安排。" name="description">
	<link rel="shortcut icon" type="image/x-icon" href="/images/favicon.png" />
	<title>酒店精选 - 专业的旅行顾问帮助你筛选的性价比最高的目的地酒店，值得推荐 -  妙妙行程管家</title>
	<link rel="stylesheet" type="text/css" href="/css/site.css" />
	<!--[if lt IE 9]>
		<script src="/themes/zanaduV1/assets/js/third_party/html5.js"></script>
	<![endif]-->
</head>
<body>
	<div class="header">
		<div class="container">
			<div class="logo">
				<a href="/">妙妙行程管家</a>
				<div class="clearfix"></div>
			</div>
			<ul class="site-nav">
				<li><a href="/">首页</a></li>
				<li><a href="/package">推荐酒店</a></li>
				<li><a href="/bundle">旅行套餐</a></li>
				<div class="clearfix"></div>
			</ul>
		</div>
	</div>
	<section class="bg-color-light-blue section-padding">
		<article class="container">
<?php if(!empty($hotel_rows)):?>
			<div class="thumb-section">
				<div class="col3 thumb-section-title">
					<h2 class="left">精选酒店</h2>
					<ul class="left">
						<li>
							<a href="">沙巴</a>
						</li>
						<li>
							<a href="">吉隆坡</a>
						</li>
					</ul>
					<div class="clearfix"></div>
				</div>
		<?php foreach($hotel_rows as $row):?>
				<div class="thumb-row margin-bottom-20">
				<?php foreach($row['packages'] as $package):?>
					<div class="col<?=$package['col_width'];?>">
						<div class="item <?=$package['col_height'];?>">
							<a href="/package/<?=$package['id'];?>" class="link">
								<img src="<?=upimage($package['cover_image'], 'org');?>" class="main-img" width="320" height="240">
								<div class="detail">
									<div class="caption">
									<h4 class="title" data-translation="The White House Hotel"><?=$package['title'];?></h4>
									<div class="sub-title" data-translation="GuiLin, China"><?=$package['subtitle'];?></div>
									</div>
									<div class="icons">
										<div class="clearfix"></div>
									</div>
								</div>
							</a>
						</div>
					</div>
				<?php endforeach;?>
					<div class="clearfix"></div>
				</div>
			<?php endforeach;?>
			</div>
<?php endif;?>
<!--
					<div class="col2" style="display:block; height:350px;">
						<div class="item">
							<a href="" class="link">
								<img src="http://zanadu.cn/resources/zanadu/bundle/71/thumb/cms_21_711_bundle.jpg" class="main-img" width="660" height="240">
								<div class="detail">
									<div class="caption">
										<h4 class="title" data-translation="The White House Hotel">中国桂林白公馆</h4>
										<div class="sub-title" data-translation="GuiLin, China">中国, 桂林</div>
									</div>
									<div class="icons">
										<div class="clearfix"></div>
									</div>
								</div>
							</a>
						</div>
					</div>
					<div class="clearfix"></div>
				</div>

				<div class="thumb-row margin-bottom-20">
<?php for($i = 0; $i < 3; $i++):?>
					<div class="col1" style="display:block; height:350px;">
						<div class="item">
							<a href="" class="link">
								<img src="http://www.zanadu.cn/resources/zanadu/hotel/61/thumb/cms_11_618_5055162.jpg" class="main-img" width="320" height="240">
								<div class="detail">
									<div class="caption">
										<h4 class="title" data-translation="The White House Hotel">中国桂林白公馆</h4>
										<div class="sub-title" data-translation="GuiLin, China">中国, 桂林</div>
									</div>
									<div class="icons">
										<div class="clearfix"></div>
									</div>
								</div>
							</a>
						</div>
					</div>
<?php endfor;?>
					<div class="clearfix"></div>
				</div>


				<div class="thumb-row margin-bottom-20">
					<div class="col2" >
						<div class="item double">
							<a href="" class="link">
								<img src="http://zanadu.cn/resources/zanadu/hotel/12/thumb/cms_22_129.jpg" class="main-img" width="660" height="610">
								<div class="detail">
									<div class="caption">
										<h4 class="title" data-translation="The White House Hotel">中国桂林白公馆</h4>
										<div class="sub-title" data-translation="GuiLin, China">中国, 桂林</div>
									</div>
									<div class="icons">
										<div class="clearfix"></div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div class="col1">
						<div class="item margin-bottom-20">
							<a href="" class="link">
								<img src="http://www.zanadu.cn/resources/zanadu/hotel/61/thumb/cms_11_618_5055162.jpg" class="main-img" width="320" height="240">
								<div class="detail">
									<div class="caption">
										<h4 class="title" data-translation="The White House Hotel">中国桂林白公馆</h4>
										<div class="sub-title" data-translation="GuiLin, China">中国, 桂林</div>
									</div>
									<div class="icons">
										<div class="clearfix"></div>
									</div>
								</div>
							</a>
						</div>

						<div class="item">
							<a href="" class="link">
								<img src="http://www.zanadu.cn/resources/zanadu/hotel/61/thumb/cms_11_618_5055162.jpg" class="main-img" width="320" height="240">
								<div class="detail">
									<div class="caption">
										<h4 class="title" data-translation="The White House Hotel">中国桂林白公馆</h4>
										<div class="sub-title" data-translation="GuiLin, China">中国, 桂林</div>
									</div>
									<div class="icons">
										<div class="clearfix"></div>
									</div>
								</div>
							</a>
						</div>
					</div>

					<div class="clearfix"></div>
				</div>
-->
			</div>
		</article>
	</section>
</body>
</html>
