<!doctype html>
<html ng-app="app" lang="zh-CN">
 <head>
	<meta charset="utf-8">
	<title>合作伙伴 - 妙妙行程管家</title>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<meta name="description" content="">
	<meta name="author" content="">
	<link type="text/css" rel="stylesheet" href="/css/bootstrap.min.css">
	<link type="text/css" rel="stylesheet" href="/css/partner/base.css">
	<link type="text/css" rel="stylesheet" href="/css/partner/xeditable.css">
	<script src="/assets/libs/angular.min.js"></script>
	<script src="https://code.angularjs.org/1.2.14/angular-route.js"></script>
	<script src="/assets/advisor/advisor.js"></script>
</head>
<body>
	<div ui-view></div>
	<div growl></div>
</body>
</html>
