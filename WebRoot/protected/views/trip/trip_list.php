<div class="trip_list clearfix">
	<ol class="breadcrumb">
		<li><a href="/admin">管理首页</a></li>
		<li class="active">行程列表</li>
	</ol>
	<div class="filter-bar clearfix">
		<form class="form-inline" role="form" action="/admin/journey">
			<div class="clearfix">
			
				<div class="pull-left">
					<div class="btn-group" >
						<a class="btn btn-sm btn-default <?php if(!isset($_GET['admin_id'])):?>active <?php endif?>" href="/admin/journey">全部</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['admin_id']) && $_GET['admin_id'] == 2):?>active <?php endif?>" href="/admin/journey?admin_id=2">CherryCha</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['admin_id']) && $_GET['admin_id'] == 4):?>active <?php endif?>" href="/admin/journey?admin_id=4">Vivian</a>
						<a class="btn btn-sm btn-default <?php if(isset($_GET['admin_id']) && $_GET['admin_id'] == 5):?>active <?php endif?>" href="/admin/journey?admin_id=5">Lizzy</a>
					</div>
				</div>

				<div class="pull-left">
					&nbsp;&nbsp;搜索:
					<div class="form-group">
						<input type="text" name="keyword" class="form-control" id="filter_key" placeholder="关键词" value="<?=isset($keyword) ? $keyword : '';?>"/>
					</div>
					<button type="submit" class="btn btn-sm btn-primary">查找</button>
					&nbsp;
				</div>
			</div>
		</form>
	</div>

	<div class="table-responsive" >
		<table class="table table-btriped trip-list-table">
			<thead>
				<tr style="background:#eee;">
					<th>#</th>
					<th>标题</th>
					<th>客服</th>
					<th>客户id</th>
					<th>出发日期</th>
					<th>天数</th>
					<th>修改时间</th>
					<th>操作</th>
				</tr>
			</thead>
			<tbody>
<?php if(!empty($trip_list)): ?>
	<?php foreach($trip_list as $trip):?>
				<tr trip_id="<?=$trip['id'];?>">
					<td><?=$trip['id'];?></td>
					<td><a href="/plan/export/excel?trip_id=<?=$trip['id'];?>" target="_blank"><?=$trip['title'];?></a></td>
					<td><?=$trip['admin_name'];?></td>
					<td><input type="text" class="trip_customer_id_input" value="<?=$trip['customer_id'];?>" style="width:60px;"></td>
					<td><?=$trip['start_time'];?></td>
					<td><?=$trip['days'];?>天</td>
					<td><?=$trip['ctime'];?></td>
					<td>
						<a class="btn btn-xs btn-default" target="_blank" href="/plan/export/pdf?trip_id=<?=$trip['id'];?>&admin_id=<?=Yii::app()->adminUser->id;?>">PDF版本</a>
						<a class="btn btn-xs btn-default copy-btn" data-toggle="modal" data-target=".modal-copy-trip">复制</a>
						<a href="/plan/create/edit?trip_id=<?=$trip['id'];?>" class="btn btn-xs btn-primary" target="_blank">修改</a>
						<a href="javascript:void(0);" class="btn btn-xs btn-danger delete_trip_btn" >删除</a>
					</td>
				</tr>
	<?php endforeach;?>
<?php else:?>
				<tr>
					<td colspan=11>暂没有相关行程列表</td>
				</tr>
<?php endif;?>
			</tbody>
		</table>
		<?php 
			$this->widget('LinkPager', array('pages' => $pages, 'prevPageLabel'=>'«','nextPageLabel'=>'»','cssFile'=>false, 'header'=>false)); 
		?>
	</div>

</div>

<div class="modal fade modal-copy-trip" tabindex="-1" >
    <div class="modal-dialog ">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                <h4 class="modal-title">复制行程</h4>
            </div>
            <div class="modal-body">
                <form role="form" id="copy-form">
                    <div class="form-group">
                        <label for="sourceId">当前行程ID</label>
                        <input name="sourceId" type="text" class="form-control" id="sourceId" value="" readonly>
                    </div>
                    <div class="form-group">
                        <label for="inputTitle">复制后的行程标题</label>
                        <input name="targetTitle" type="text" class="form-control" id="inputTitle" placeholder="行程标题">
                    </div>
                    <div class="form-group">
                        <label for="inputCustomerId">复制给的目标用户ID</label>
                        <input name="targetCustomerId" type="text" class="form-control" id="inputCustomerId" placeholder="用户ID">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary copy-submit-btn">确定复制</button>
                <button type="button" class="btn btn-default" data-dismiss="modal">取消</button>
            </div>
        </div>
    </div>
</div>

<script>
	seajs.use('/assets/js/router.js', function(router){
		router.load('admin/journey_list');
	});
</script>
