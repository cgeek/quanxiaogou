<?php

class Taobao extends CApplicationComponent {
	
	public $appkey, $secretKey, $topClient = '';

	public function init() {
		define("TOP_SDK_WORK_DIR", Yii::app()->runtimePath . '/top/');

		Yii::import('application.vendors.taobao.RequestCheckUtil');
		Yii::import('application.vendors.taobao.LtLogger');
		Yii::import('application.vendors.taobao.TopClient');
		Yii::import('application.vendors.taobao.request.*');

		$appkey = $this->appkey;
		$secretKey = $this->secretKey;

		if(!$appkey || !$secretKey){
			Yii::app()->end();
		}

		$this->topClient = new TopClient();
		$this->topClient->appkey = $appkey;
		$this->topClient->secretKey = $secretKey;
		$this->topClient->format = 'json';
	}

	public function getAccessToken($url)
	{
		$postfields= array(
			'grant_type'    => 'authorization_code',
			'client_id'     => $this->appkey,
			'client_secret' => $this->secretKey,
			'code'          => $_REQUEST['code'],
			'redirect_uri'  => $url
		);
		$access_token_url = 'https://oauth.taobao.com/token';
		$token = json_decode($this->topClient->curl($access_token_url,$postfields));
		return $token;
	}

	public function getAuthorizeURL($url, $response_type = 'code', $state = 1)
	{
		//https://oauth.taobao.com/authorize?response_type=code&client_id=12382619&redirect_uri=http://127.0.0.1/loginDemo/oauthLogin.php&state=1
		$params = array();
		$params['client_id'] = $this->appkey;
		$params['redirect_uri'] = $url;
		$params['response_type'] = $response_type;
		$params['state'] = $state;
		return "https://oauth.taobao.com/authorize?". http_build_query($params);
	}

	public function getUserInfo($nickname,$sessionKey='')
	{
		$req = new UserGetRequest;
		$req->setFields("user_id,uid,nick,sex");
		$req->setNick($nickname);
		$resp = $this->topClient->execute($req,$sessionKey);
		return $resp;
	}

	public function getSellerInfo($sessionKey)
	{
		$req = new UserSellerGetRequest;
		$req->setFields("user_id,nick,sex,seller_credit,type,has_more_pic,item_img_num,item_img_size,prop_img_num,prop_img_size,auto_repost,promoted_type,status,alipay_bind,consumer_protection,avatar,liangpin,sign_food_seller_promise,has_shop,is_lightning_consignment,has_sub_stock,is_golden_seller,vip_info,magazine_subscribe,vertical_market,online_gaming");
		$resp = $this->topClient->execute($req, $sessionKey);
		return $resp;
	}

	public function getShopInfo($shopname)
	{
		$req = new ShopGetRequest;
		$req->setFields("sid,cid,title,nick,desc,bulletin,pic_path,created,modified");
		$req->setNick($shopname);
		$resp = $this->topClient->execute($req);
		return $resp;
	}

	public function getTradesSold($sessionKey, $status = '', $page_no = 1, $page_size=100)
	{
		$req = new TradesSoldGetRequest;
		$req->setFields("seller_nick,buyer_nick,title,type,created,sid,tid,seller_rate,buyer_rate,status,payment,discount_fee,adjust_fee,post_fee,total_fee,pay_time,end_time,modified,consign_time,buyer_obtain_point_fee,point_fee,real_point_fee,received_payment,commission_fee,pic_path,num_iid,num_iid,num,price,cod_fee,cod_status,shipping_type,receiver_name,receiver_state,receiver_city,receiver_district,receiver_address,receiver_zip,receiver_mobile,receiver_phone,orders.title,orders.pic_path,orders.price,orders.num,orders.iid,orders.num_iid,orders.sku_id,orders.refund_status,orders.status,orders.oid,orders.total_fee,orders.payment,orders.discount_fee,orders.adjust_fee,orders.sku_properties_name,orders.item_meal_name,orders.buyer_rate,orders.seller_rate,orders.outer_iid,orders.outer_sku_id,orders.refund_id,orders.seller_type");
		if(!empty($status)) {
			$req->setStatus($status);
		}
		$req->setPageSize($page_size);
		$req->setPageNo($page_no);
		$resp = $this->topClient->execute($req, $sessionKey);
		return $resp;
	}

	public function getItemsOnsale($sessionKey)
	{
		$req = new ItemsOnsaleGetRequest;
		$req->setFields("approve_status,num_iid,title,nick,type,cid,pic_url,num,props,valid_thru,list_time,price,has_discount,has_invoice,has_warranty,has_showcase,modified,delist_time,postage_id,seller_cids,outer_id");
		$resp = $this->topClient->execute($req, $sessionKey);
		return $resp;
	}

	public function getWangwangEserviceReceivenum($nick, $start_date, $end_date, $sessionKey)
	{
		$req = new WangwangEserviceReceivenumGetRequest;
		$req->setServiceStaffId("$nick");
		$req->setStartDate("$start_date");
		$req->setEndDate("$end_date");
		$resp = $this->topClient->execute($req, $sessionKey, true);
		return $resp;
	}

	public function getTradeFullinfo($tid, $sessionKey) 
	{
		$req = new TradeFullinfoGetRequest;
		$req->setFields("seller_nick,buyer_nick,title,type,created,sid,tid,seller_rate,buyer_rate,status,payment,discount_fee,adjust_fee,post_fee,total_fee,pay_time,end_time,modified,consign_time,buyer_obtain_point_fee,point_fee,real_point_fee,received_payment,commission_fee,pic_path,num_iid,num_iid,num,price,cod_fee,cod_status,shipping_type,receiver_name,receiver_state,receiver_city,receiver_district,receiver_address,receiver_zip,receiver_mobile,receiver_phone,orders.title,orders.pic_path,orders.price,orders.num,orders.iid,orders.num_iid,orders.sku_id,orders.refund_status,orders.status,orders.oid,orders.total_fee,orders.payment,orders.discount_fee,orders.adjust_fee,orders.sku_properties_name,orders.item_meal_name,orders.buyer_rate,orders.seller_rate,orders.outer_iid,orders.outer_sku_id,orders.refund_id,orders.seller_type");
		$req->setTid($tid);
		$resp = $this->topClient->execute($req, $sessionKey);
		return $resp;
	}

	/*====== get item ======*/
	public function getItemDetail($id, $sessionKey = NULL)
	{
		$req = new ItemGetRequest;
		$req->setFields("detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,approve_status,postage_id,product_id,auction_point,property_alias,item_img,prop_img,sku,video,outer_id,is_virtual");
		$req->setNumIid($id);
		$resp = $this->topClient->execute($req, $sessionKey);
		return $resp;

	}

	public function getItemsList($ids_str = NULL, $sessionKey = NULL)
	{
		if($ids_str == NULL || empty($ids_str)) {
			return NULL;
		}
		$req = new ItemsListGetRequest;
		$req->setFields("detail_url,num_iid,title,nick,type,cid,seller_cids,props,input_pids,input_str,desc,pic_url,num,valid_thru,list_time,delist_time,stuff_status,location,price,post_fee,express_fee,ems_fee,has_discount,freight_payer,has_invoice,has_warranty,has_showcase,modified,increment,approve_status,postage_id,product_id,auction_point,property_alias,item_img,prop_img,sku,video,outer_id,is_virtual");
		$req->setNumIids("$ids_str");
		$resp = $this->topClient->execute($req, $sessionKey);
		return $resp;
	}

	public function getSkusByItem($id = NULL, $sessionKey = NULL)
	{
		if($id == NULL || empty($id))  {
			return NULL;
		}
		$req = new ItemSkusGetRequest;
		$req->setFields('sku_id,num_iid, price, properties_name, properties');
		$req->setNumIids($id);

		$resp = $this->topClient->execute($req, $sessionKey);
		return $resp;
		
	}
}
