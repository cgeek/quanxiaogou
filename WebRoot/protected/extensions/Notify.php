<?php



class Notify
{

    function call($remote_server, $post_string)
    {
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $remote_server . '/notify');
		
        curl_setopt($ch, CURLOPT_POSTFIELDS, $post_string);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);

        $data = curl_exec($ch);

        curl_close($ch);

        return $data;
    }


}
