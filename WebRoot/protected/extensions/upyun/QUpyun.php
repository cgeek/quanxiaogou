<?php

include_once("upyun.class.php");
class QUpyun extends CApplicationComponent 
{
	
	public $user = 'cgeek';
	public $password = 'miaomiao';
	public $bucket = 'miaomiao';

	private $upyun;
	
	public function __call($name,$paramaters){
		$domain = array_shift($paramaters);
		$upyun = $this->upyun;
		if(method_exists($upyun,$name)){
			return call_user_func_array(array($upyun,$name),$paramaters);
		}
		return parent::__call($name,$paramaters);
	}
	
	public function init()
	{
		$this->upyun = new UpYun($this->bucket, $this->user, $this->password);
	}

	public function upload($filename, $data)
	{
		$r = $this->upyun->writeFile("/$filename", $data, False);   // 上传图片，自动创建目录
		return $r;
	}

}

